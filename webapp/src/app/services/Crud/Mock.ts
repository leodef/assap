import { DTO } from '../../types/DTO';
import { CrudService as ICrudService } from './Crud'

class CrudService extends ICrudService {
  static items: any = {};
  constructor () {
    super()
    CrudService.items[this.path()] =
      CrudService.items[this.path()] || []
  }

  private get items (): Array<any> {
    return CrudService.items[this.path()]
  }

  private set items (items: Array<any>) {
    CrudService.items[this.path()] = items
  }

  fetch (params?: any): Promise<any> {
    /*
    const {
      filter,
      pagination,
      fields,
      sort
    } = (params || {} as any)
    */
    return Promise.resolve({ items: this.items })
  }

  create (value: any, parent?: any): Promise<any> {
    value._id = this.generateId()
    this.items.push(value)
    return Promise.resolve(value)
  }

  remove (value: any, parent?: any): Promise<any> {
    const _id = this.getId(value)
    let selected = null
    this.items = this.items.filter(item => {
      if (_id === this.getId(item)) {
        selected = item
        return false
      }
      return true
    })
    return Promise.resolve(selected)
  }

  update (value: any, parent?: any): Promise<any> {
    const _id = value._id
    const selected = null
    this.items = this.items.map(item => {
      if (_id === this.getId(item)) {
        return { ...item, ...value }
      }
      return item
    })
    return Promise.resolve(selected)
  }

  find (value: any, parent?: any): Promise<any> {
    const _id = this.getId(value)
    return this.items.filter(item => _id.indexOf(this.getId(item)) >= 0)[0]
  }

  all (filter: any, parent?: any): Promise<Array<any>> {
    return Promise.resolve(this.items)
  }

  private getId (value: any, id?: string): string {
    id = id || value._id
    if (typeof value === 'string' && !id) {
      id = value
    }
    return id || ''
  }

  private generateId () {
    return DTO.generateId()
  }
}
export { CrudService }
export default new CrudService()
