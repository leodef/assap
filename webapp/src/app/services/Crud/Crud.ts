import api from '../Api'
import _ from 'lodash'

export class CrudService {
  protected defaultPath (parent?: any) {
    return '/'
  }

  protected get api () {
    return api
  }

  protected loadRequestBody (value: any) {
    return value
  }

  protected loadResponseBody (value: any) {
    return value
  }

  protected loadResponse (value: any) {
    return Array.isArray(value)
      ? value.map(val => this.loadResponseBody(val))
      : this.loadResponseBody(value)
  }

  protected loadRequestFilter (value: any) {
    return value
  }

  protected path (parent?: any): string {
    return this.defaultPath(parent)
  }

  protected pathSubPath (val?: string, parent?: any) {
    return (val ? `${this.path(parent)}${val}/` : this.path())
  }

  protected pathId (item: any, parent?: any): string {
    return `${this.path(parent)}${item._id || item}/`
  }

  private cleanRequestFilter (body: any) {
    return _.pickBy(body, _.identity)
  }

  fetch (params?: any): Promise<any> {
    const {
      filter,
      pagination,
      fields,
      sort,
      parent
    } = (params || {})

    return api.post(
      this.pathSubPath('fetch', parent),
      this.cleanRequestFilter({
        filter,
        pagination,
        fields,
        sort
      })
    ).then((response: any) =>
      new CollectionWithPaginationResponse(response)
    )
  }

  create (value: any, parent?: any): Promise<any> {
    const url = this.path(parent)
    return api
      .post(url, this.loadRequestBody(value))
      .then(response => this.loadResponse(response))
  }

  remove (item: any, parent?: any): Promise<any> {
    if (!item || !item._id) {
      return Promise.resolve(null)
    }
    const _id = item._id
    const url = this.pathId(_id, parent)
    return api
      .delete(url)
      .then(response => this.loadResponse(response))
  }

  update (item: any, parent?: any): Promise<any> {
    if (!item || !item._id) {
      return Promise.resolve(null)
    }
    const _id = item._id
    const url = this.pathId(_id, parent)
    return api
      .put(url, this.loadRequestBody(item))
      .then(response => this.loadResponse(response))
  }

  find (id: any, parent?: any): Promise<any> {
    const url = this.pathId(id, parent)
    return api
      .get(url)
      .then(response => this.loadResponse(response))
  }

  all (filter: any, parent?: any): Promise<Array<any>> {
    const url = this.path(parent)
    return api
      .get(url)
      .then(response => this.loadResponse(response))
  }

  fetchOptions (params?: any): Promise<any> {
    const {
      filter,
      pagination,
      fields,
      sort,
      parent
    } = (params || {})

    return api.post(
      this.pathSubPath('fetchOptions', parent),
      this.cleanRequestFilter({
        filter,
        pagination,
        fields,
        sort
      })
    ).then((response: any) =>
      new CollectionWithPaginationResponse(response)
    )
  }

}

export class Pagination {
  count?: number;
  limit?: number;
  page?: number;
  pages?: number;
  constructor (response?: any) {
    const { count, limit, page, pages } = (response || {})
    this.count = count || 0
    this.limit = limit || 0
    this.page = page || 0
    this.pages = pages || 0
  }
}
export class CollectionWithPaginationResponse {
  items: Array<any> = [];
  pagination?: Pagination = new Pagination();

  constructor (response?: any) {
    const { items, pagination } = (response || {})
    this.items = items || []
    this.pagination = new Pagination(pagination)
  }
}
