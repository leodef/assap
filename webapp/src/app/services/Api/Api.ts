
// eslint-disable-next-line no-unused-vars
import axios, { AxiosRequestConfig, AxiosInstance } from 'axios'
import { stringToDate, dateToString } from '../../types/Config'
import { UrlUtils } from '../../utils/UrlUtils'
// import type { Axios } from 'axios';

const API_ROOT = 'http://localhost:3000/api'

export class ApiService {
  private static authToken: string
  static setAuthToken (authToken: string) {
    ApiService.authToken = authToken
    return authToken
  }

  static getAuthToken () {
    return ApiService.authToken
  }

  static utils () {
    return ApiUtils
  }

  get client (): AxiosInstance {
    const authToken = ApiService.authToken
    return axios.create({
      baseURL: API_ROOT,
      timeout: 10000,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        ...(!authToken ? {} : {
          Authorization: `Bearer ${authToken}`
        })
      }
    } as AxiosRequestConfig)
  }

  get (path: string, params?: any, config?: any): Promise<any> | Promise<any> {
    return this.client.get(
      this.loadUrlParams(path, params),
      config
    ).then((response: any) => this.validRequest(response))
  }

  post (path: string, payload: any, config?: any): Promise<any> {
    return this.client.post(path, payload, config).then((response: any) => this.validRequest(response))
  }

  patch (path: string, payload: any, config?: any): Promise<any> {
    return this.client.patch(path, payload, config).then((response: any) => this.validRequest(response))
  }

  put (path: string, payload: any, config?: any): Promise<any> {
    return this.client.put(path, payload).then((response: any) => this.validRequest(response))
  }

  delete (path: string, config?: any): Promise<any> {
    return this.client.delete(path, config).then((response: any) => this.validRequest(response))
  }

  loadUrlParams (path: string, obj?: any) {
    return UrlUtils.loadUrlParams(path, obj)
  }

  validRequest (response: any) {
    const data = response.data
    const { error, ...dataInfo } = data
    if (error) {
      throw dataInfo
    }
    return data
  }
}

class ApiUtils {
  static dateFormat = 'YYYY-mm-ddTHH:MM:ssZ'

  static dateToString = (date: any) => {
    return dateToString(date, ApiUtils.dateFormat)
  }

  static stringToDate = (date: any) => {
    return stringToDate(date, ApiUtils.dateFormat)
  }
}

export default new ApiService()
