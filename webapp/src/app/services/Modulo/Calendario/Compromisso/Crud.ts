import { CrudService } from '../../../Crud'
import { Compromisso } from '../../../../types/API/Calendario/Compromisso'

 /**
 * Serviço para acionar a serviços da api para gerenciar Compromisso
 * 
 * @name CompromissoCrudService
 * @module service/Modulo/Calendario/Compromisso/Crud
 * @category Serviço
 * @subcategory Compromisso
 */
export class CompromissoCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/calendario/compromisso/'
  }
  
  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): Compromisso {
    const dto = new Compromisso().fromJson(value)
    return dto
  }

  protected loadRequestBody (value: any) {
    return new Compromisso().fromJson(value).toDTO()
  }
}

export default new CompromissoCrudService()
