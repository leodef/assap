import { QuantidadeElemento } from '../../../../types/API/CalcCompQuim/QuantidadeElemento'
import { CrudService } from '../../../Crud'

 /**
 * Serviço para acionar a serviços da api para gerenciar QuantidadeElemento
 * 
 * @name QuantidadeElementoCrudService
 * @module service/Modulo/CalcCompQuim/QuantidadeElemento/Crud
 * @category Serviço
 * @subcategory QuantidadeElemento
 */
export class QuantidadeElementoCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/calc-comp-quim/quantidade-elemento/'
  }
  
  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): QuantidadeElemento {
    return new QuantidadeElemento().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new QuantidadeElemento().fromJson(value).toDTO()
  }
}

export default new QuantidadeElementoCrudService()
