import { QuantidadeSubstancia } from '../../../../types/API/CalcCompQuim/QuantidadeSubstancia'
import { CrudService } from '../../../Crud'

 /**
 * Serviço para acionar a serviços da api para gerenciar QuantidadeSubstancia
 * 
 * @name QuantidadeSubstanciaCrudService
 * @module service/Modulo/CalcCompQuim/QuantidadeSubstancia/Crud
 * @category Serviço
 * @subcategory QuantidadeSubstancia
 */
export class QuantidadeSubstanciaCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/calc-comp-quim/quantidade-substancia/'
  }

  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): QuantidadeSubstancia {
    return new QuantidadeSubstancia().fromJson(value)
  }
  
  protected loadRequestBody (value: any) {
    return new QuantidadeSubstancia().fromJson(value).toDTO()
  }
}

export default new QuantidadeSubstanciaCrudService()
