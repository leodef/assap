import { Substancia } from '../../../../types/API/CalcCompQuim/Substancia'
import { CrudService } from '../../../Crud'

 /**
 * Serviço para acionar a serviços da api para gerenciar Substancia
 * 
 * @name SubstanciaCrudService
 * @module service/Modulo/CalcCompQuim/Substancia/Crud
 * @category Serviço
 * @subcategory Substancia
 */
export class SubstanciaCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/calc-comp-quim/substancia/'
  }
  
  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): Substancia {
    return new Substancia().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new Substancia().fromJson(value).toDTO()
  }
}

export default new SubstanciaCrudService()
