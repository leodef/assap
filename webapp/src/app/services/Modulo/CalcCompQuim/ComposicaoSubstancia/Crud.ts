import { ComposicaoSubstancia } from '../../../../types/API/CalcCompQuim/ComposicaoSubstancia'
import { CrudService } from '../../../Crud'

 /**
 * Serviço para acionar a serviços da api para gerenciar ComposicaoSubstancia
 * 
 * @name ComposicaoSubstanciaCrudService
 * @module service/Modulo/CalcCompQuim/ComposicaoSubstancia/Crud
 * @category Serviço
 * @subcategory ComposicaoSubstancia
 */
export class ComposicaoSubstanciaCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/calc-comp-quim/composicao-substancia/'
  }
  
  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): ComposicaoSubstancia {
    return new ComposicaoSubstancia().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new ComposicaoSubstancia().fromJson(value).toDTO()
  }
}

export default new ComposicaoSubstanciaCrudService()
