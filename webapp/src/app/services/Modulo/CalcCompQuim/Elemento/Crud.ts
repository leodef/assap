import { Elemento } from '../../../../types/API/CalcCompQuim/Elemento'
import { CrudService } from '../../../Crud'

 /**
 * Serviço para acionar a serviços da api para gerenciar Elemento
 * 
 * @name ElementoCrudService
 * @module service/Modulo/CalcCompQuim/Elemento/Crud
 * @category Serviço
 * @subcategory Elemento
 */
export class ElementoCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/calc-comp-quim/elemento/'
  }
  
  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): Elemento {
    return new Elemento().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new Elemento().fromJson(value).toDTO()
  }
}

export default new ElementoCrudService()
