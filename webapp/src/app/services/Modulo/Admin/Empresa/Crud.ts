import { Empresa } from '../../../../types/API/Empresa/Empresa'
import { CrudService } from '../../../Crud'

 /**
 * Serviço para acionar a serviços da api para gerenciar Empresa
 * 
 * @name EmpresaCrudService
 * @module service/Modulo/Admin/Empresa/Crud
 * @category Serviço
 * @subcategory Empresa
 */
export class EmpresaCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/admin/empresa/'
  }

  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): any {
    return new Empresa().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new Empresa().fromJson(value).toDTO()
  }
}

export default new EmpresaCrudService()
