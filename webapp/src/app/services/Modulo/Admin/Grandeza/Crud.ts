import { Grandeza } from '../../../../types/API/Grandeza'
import { CrudService } from '../../../Crud'

 /**
 * Serviço para acionar a serviços da api para gerenciar Grandeza
 * 
 * @name GrandezaCrudService
 * @module service/Modulo/Admin/Grandeza/Crud
 * @category Serviço
 * @subcategory Grandeza
 */
export class GrandezaCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/admin/grandeza/'
  }

  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): any {
    return new Grandeza().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new Grandeza().fromJson(value).toDTO()
  }
}

export default new GrandezaCrudService()
