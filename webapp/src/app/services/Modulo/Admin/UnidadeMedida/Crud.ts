import { CrudService } from '../../../Crud'
import { findParentByPrefix } from '../../../../contexts/ParentCrudContext'
import { UnidadeMedida } from '../../../../types/API/UnidadeMedida'

 /**
 * Serviço para acionar a serviços da api para gerenciar UnidadeMedida
 * 
 * @name UnidadeMedidaCrudService
 * @module service/Modulo/Admin/UnidadeMedida/Crud
 * @category Serviço
 * @subcategory UnidadeMedida
 */
export class UnidadeMedidaCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   *   de acordo com o contexto, se tem alguem objeto relacionado ou não
   * 
   * @returns {String}
   * 
  */
  protected path (parent?: any): string {
    const grandezaParent = findParentByPrefix('grandeza', parent)
    if (grandezaParent && grandezaParent.item) {
      return this.grandezaPath(grandezaParent.item)
    }
    return this.defaultPath()
  }

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   *   quando for relacionado a um outro objeto
   * 
   * @returns {String}
   * 
  */
  protected grandezaPath (item: any): string {
    return `modulo/admin/grandeza/${item._id}/unidade-medida/`
  }


  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/admin/unidade-medida/'
  }
  
  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): UnidadeMedida {
    return new UnidadeMedida().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new UnidadeMedida().fromJson(value).toDTO()
  }
}

export default new UnidadeMedidaCrudService()
