import { TipoContatoInfo } from '../../../../types/API/Contato/TipoContatoInfo'
import { CrudService } from '../../../Crud'

 /**
 * Serviço para acionar a serviços da api para gerenciar TipoContatoInfo
 * 
 * @name TipoContatoInfoCrudService
 * @module service/Modulo/Admin/TipoContatoInfo/Crud
 * @category Serviço
 * @subcategory TipoContatoInfo
 */
export class TipoContatoInfoCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/admin/tipo-contato-info/'
  }
  
  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): TipoContatoInfo {
    return new TipoContatoInfo().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new TipoContatoInfo().fromJson(value).toDTO()
  }
}

export default new TipoContatoInfoCrudService()
