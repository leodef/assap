import { Produto } from '../../../../types/API/Produto'
import { CrudService } from '../../../Crud'

 /**
 * Serviço para acionar a serviços da api para gerenciar Produto
 * 
 * @name ProdutoCrudService
 * @module service/Modulo/Admin/Produto/Crud
 * @category Serviço
 * @subcategory Produto
 */
export class ProdutoCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/admin/produto/'
  }
  
  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): Produto {
    return new Produto().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new Produto().fromJson(value).toDTO()
  }
}

export default new ProdutoCrudService()
