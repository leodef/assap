import { CrudService } from '../../../Crud'
import { findParentByPrefix } from '../../../../contexts/ParentCrudContext'
import { Veiculo } from '../../../../types/API/Veiculo'

 /**
 * Serviço para acionar a serviços da api para gerenciar Veiculo
 * 
 * @name VeiculoCrudService
 * @module service/Modulo/DadosEmpresa/Veiculo/Crud
 * @category Serviço
 * @subcategory Veiculo
 */
export class VeiculoCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   *   de acordo com o contexto, se tem alguem objeto relacionado ou não
   * 
   * @returns {String}
   * 
  */
  protected path (parent?: any): string {
    const dadosEmpresaParent = findParentByPrefix('empresa', parent)
    if (dadosEmpresaParent && dadosEmpresaParent.item) {
      return this.dadosEmpresaPath(dadosEmpresaParent.item)
    }
    return this.defaultPath()
  }

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   *   quando for relacionado a um outro objeto
   * 
   * @returns {String}
   * 
  */
  protected dadosEmpresaPath (item: any): string {
    return `modulo/dados-empresa/${item.dados}/veiculo/`
  }


  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/dados-empresa/veiculo/'
  }
  
  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): Veiculo {
    return new Veiculo().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new Veiculo().fromJson(value).toDTO()
  }

}

export default new VeiculoCrudService()
