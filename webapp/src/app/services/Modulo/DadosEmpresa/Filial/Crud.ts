import { findParentByPrefix } from "../../../../contexts/ParentCrudContext";
import { Filial } from "../../../../types/API/Filial";
import { CrudService } from "../../../Crud";

/**
 * Serviço para acionar a serviços da api para gerenciar Filial
 *
 * @name FilialCrudService
 * @module service/Modulo/DadosEmpresa/Filial/Crud
 * @category Serviço
 * @subcategory Filial
 */
export class FilialCrudService extends CrudService {
  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   *   de acordo com o contexto, se tem alguem objeto relacionado ou não
   *
   * @returns {String}
   *
   */
  protected path(parent?: any): string {
    const parceiroParent = findParentByPrefix('parceiro', parent)
    if (parceiroParent && parceiroParent.item) {
      return this.parceiroPath(parceiroParent.item);
    }
    const dadosEmpresaParent = findParentByPrefix('empresa', parent)
    if (dadosEmpresaParent && dadosEmpresaParent.item) {
      return this.dadosEmpresaPath(dadosEmpresaParent.item);
    }
    return this.defaultPath();
  }

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   *   quando for relacionado a um outro objeto
   *
   * @returns {String}
   *
   */
  protected dadosEmpresaPath(item: any): string {
    return `modulo/dados-empresa/${item.dados}/filial/`;
  }
  protected parceiroPath(item: any): string {
    return `modulo/parceiro/${item._id}/filial/`;
  }

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   *
   * @returns {String}
   *
   */
  protected defaultPath(): string {
    return "modulo/dados-empresa/filial/";
  }

  /**
   * Converte o retorno da API
   *
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody(value: any): Filial {
    return new Filial().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new Filial().fromJson(value).toDTO()
  }
}

export default new FilialCrudService();
