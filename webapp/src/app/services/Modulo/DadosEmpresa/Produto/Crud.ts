import { CrudService } from '../../../Crud'
import { findParentByPrefix } from '../../../../contexts/ParentCrudContext'
import { Produto } from '../../../../types/API/Produto'

 /**
 * Serviço para acionar a serviços da api para gerenciar Produto
 * 
 * @name ProdutoCrudService
 * @module service/Modulo/DadosEmpresa/Produto/Crud
 * @category Serviço
 * @subcategory Produto
 */
export class ProdutoCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   *   de acordo com o contexto, se tem alguem objeto relacionado ou não
   * 
   * @returns {String}
   * 
  */
  protected path (parent?: any): string {
    const dadosEmpresaParent = findParentByPrefix('empresa', parent)
    if (dadosEmpresaParent && dadosEmpresaParent.item) {
      return this.dadosEmpresaPath(dadosEmpresaParent.item)
    }
    return this.defaultPath()
  }

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   *   quando for relacionado a um outro objeto
   * 
   * @returns {String}
   * 
  */
  protected dadosEmpresaPath (item: any): string {
    return `modulo/dados-empresa/${item.dados}/produto/`
  }


  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   * 
   * @returns {String}
   * 
  */
  protected defaultPath (): string {
    return 'modulo/dados-empresa/produto/'
  }
  
  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): Produto {
    return new Produto().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    return new Produto().fromJson(value).toDTO()
  }
}

export default new ProdutoCrudService()
