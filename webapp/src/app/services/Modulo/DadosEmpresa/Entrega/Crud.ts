import {
  findParentByPrefix
} from '../../../../contexts/ParentCrudContext'
import {
  Entrega
} from '../../../../types/API/Logistica/Entrega'
import {
  CalculoRotaResponse,
  calculoRotaResponseToRota,
  Rota,
  rotaToCalculoRotaRequest
} from '../../../../types/API/Logistica/Rota'
import {
  CrudService
} from '../../../Crud'

 /**
 * Serviço para acionar a serviços da api para gerenciar Entrega
 * 
 * @name EntregaCrudService
 * @module service/Modulo/DadosEmpresa/Entrega/Crud
 * @category Serviço
 * @subcategory Entrega
 */
export class EntregaCrudService extends CrudService {

  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   *   de acordo com o contexto, se tem alguem objeto relacionado ou não
   * 
   * @returns {String}
   * 
  */
 protected path (parent?: any): string {
  const dadosEmpresaParent = findParentByPrefix('empresa', parent)
  if (dadosEmpresaParent && dadosEmpresaParent.item) {
    return this.dadosEmpresaPath(dadosEmpresaParent.item)
  }
  return this.defaultPath()
}

/**
 * Retorna o inicio do caminho para o acesso dos serviços especifico da API
 *   quando for relacionado a um outro objeto
 * 
 * @returns {String}
 * 
*/
protected dadosEmpresaPath (item: any): string {
  return `modulo/dados-empresa/${item.dados}/entrega/`
}


/**
 * Retorna o inicio do caminho para o acesso dos serviços especifico da API
 * 
 * @returns {String}
 * 
*/
protected defaultPath (): string {
  return 'modulo/dados-empresa/entrega/'
}
  
  /**
   * Converte o retorno da API
   *
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): Entrega {
    return new Entrega().fromJson(value)
  }
  
  protected loadRequestBody (value: any) {
    const loadedRequest = new Entrega().fromJson(value).toDTO()
    return loadedRequest
  }

  /**
   * Acessa o serviço de calculo de rota da API
   *
   * @param {any} item Parametro para o calculo de rota
   * @param {any} rows Retorno da APi de calculo de distancia
   * @param {any} parent Objeto relacionado
   * 
   * @returns {String}
   * 
  */
  calculoRota (item: any, config: any, parent?: any): Promise<Rota> {

    const api = this.api
    const request = rotaToCalculoRotaRequest(item as Rota, config)
    return api.post(
      this.pathSubPath('calculo-rota', parent),
      request
    ).then((response) => {
      const result = calculoRotaResponseToRota(
        response as CalculoRotaResponse,
        config,
        item)
        return result
      })
  }

  /*
    create (value: any, parent?: any): Promise<any> {
      return Promise.resolve({value, _id: DTO.generateId()})
    }

    remove (item: any, parent?: any): Promise<any> {
      return Promise.resolve(item)
    }

    update (item: any, parent?: any): Promise<any> {
      return Promise.resolve(item)
    }
  */

}

export default new EntregaCrudService()
