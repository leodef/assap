import { findParentByPrefix } from '../../../../contexts/ParentCrudContext'
import { Funcionario } from '../../../../types/API/Funcionario'
import { CrudService } from '../../../Crud'

 /**
 * Serviço para acionar a serviços da api para gerenciar Funcionario
 * 
 * @name FuncionarioCrudService
 * @module service/Modulo/DadosEmpresa/Funcionario/Crud
 * @category Serviço
 * @subcategory Funcionario
 */
export class FuncionarioCrudService extends CrudService {


  /**
   * Retorna o inicio do caminho para o acesso dos serviços especifico da API
   *   de acordo com o contexto, se tem alguem objeto relacionado ou não
   * 
   * @returns {String}
   * 
  */
 protected path (parent?: any): string {
  const dadosEmpresaParent = findParentByPrefix('empresa', parent)
  if (dadosEmpresaParent && dadosEmpresaParent.item) {
    return this.dadosEmpresaPath(dadosEmpresaParent.item)
  }
  return this.defaultPath()
}

/**
 * Retorna o inicio do caminho para o acesso dos serviços especifico da API
 *   quando for relacionado a um outro objeto
 * 
 * @returns {String}
 * 
*/
protected dadosEmpresaPath (item: any): string {
  return `modulo/dados-empresa/${item.dados}/funcionario/`
}


/**
 * Retorna o inicio do caminho para o acesso dos serviços especifico da API
 * 
 * @returns {String}
 * 
*/
protected defaultPath (): string {
  return 'modulo/dados-empresa/funcionario/'
}

  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponseBody (value: any): Funcionario {
    return new Funcionario().fromJson(value)
  }

  protected loadRequestBody (value: any) {
    const request = new Funcionario().fromJson(value).toDTO()
    return request
  }
}

export default new FuncionarioCrudService()
