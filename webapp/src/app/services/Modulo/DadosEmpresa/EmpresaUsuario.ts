import { Empresa } from "../../../types/API/Empresa/Empresa"
import api from '../../Api'


 /**
 * Serviço para acionar a serviços da api para gerenciar Empresa
 * 
 * @name EmpresaUsuarioCrudService
 * @module service/Modulo/DadosEmpresa/Empresa/Crud
 * @category Serviço
 * @subcategory Empresa
 */
export class EmpresaUsuarioCrudService {

  protected get api () {
    return api
  }

  /**
   * Retorna o caminho para os endpoins a serem acessados
   * 
   *
   * @returns {String}
   */
  protected path (): string {
    return 'modulo/dados-empresa/empresa-usuario/'
  }

  /**
   * Converte o retorno da API
   * 
   * @param {any} value Retorno da api
   *
   * @returns {any}
   */
  protected loadResponse (value: any): any {
    return new Empresa().fromJson(value)
  }
  
  protected loadRequestBody (value: any) {
    return new Empresa().fromJson(value).toDTO()
  }
  /**
   * Acessa o endpoint para retorna todos os registros
   * 
   * @param {any} filter Filtro da requisição
   *
   * @returns {any}
   */
  fetch (filter: any): Promise<Array<any>> {
    const url = this.path()
    return api
      .get(url)
      .then(response => this.loadResponse(response))
  }
}

export default new EmpresaUsuarioCrudService()
