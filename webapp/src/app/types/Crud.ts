
export class CrudType {


  constructor (
    public config: any = null,
    public name: string = 'ITEM',
    public OPTIONS_ITEM: string = 'OPTIONS_ITEM',
    public SET_ACTION_ITEM: string = 'SET_ACTION_ITEM',
    public SET_PAGINATION_ITEM: string = 'SET_PAGINATION_ITEM',
    public SET_FILTER_ITEM: string = 'SET_FILTER_ITEM',
    public SET_FILTER_OPTIONS_ITEM: string = 'SET_FILTER_OPTIONS_ITEM',
    public SET_FIELDS_ITEM: string = 'SET_FIELDS_ITEM',
    public SET_FIELDS_OPTIONS_ITEM: string = 'SET_FIELDS_OPTIONS_ITEM',
    public SET_SORT_ITEM: string = 'SET_SORT_ITEM',
    public CLEAR_ITEM: string = 'CLEAR_ITEM',
    public CLEAR_OPTIONS_ITEM: string = 'CLEAR_OPTIONS_ITEM',
    

    // FETCH CREATE DELETE UPDATE
    public FETCH_OPTIONS_ITEM: string = 'FETCH_OPTIONS_ITEM',
    public FETCH_ITEM: string = 'FETCH_ITEM',
    public CREATE_ITEM: string = 'CREATE_ITEM',
    public DELETE_ITEM: string = 'DELETE_ITEM',
    public UPDATE_ITEM: string = 'UPDATE_ITEM',
    public FIND_ITEM: string = 'FIND_ITEM',

    // PENDING - FETCH CREATE DELETE UPDATE
    public FETCH_OPTIONS_ITEM_PENDING: string = 'FETCH_OPTIONS_ITEM_PENDING',
    public FETCH_ITEM_PENDING: string = 'FETCH_ITEM_PENDING',
    public CREATE_ITEM_PENDING: string = 'CREATE_ITEM_PENDING',
    public DELETE_ITEM_PENDING: string = 'DELETE_ITEM_PENDING',
    public UPDATE_ITEM_PENDING: string = 'UPDATE_ITEM_PENDING',
    public FIND_ITEM_PENDING: string = 'FIND_ITEM_PENDING',

    // SUCCESS - FETCH CREATE DELETE UPDATE
    public FETCH_OPTIONS_ITEM_SUCCESS: string = 'FETCH_OPTIONS_ITEM_SUCCESS',
    public FETCH_ITEM_SUCCESS: string = 'FETCH_ITEM_SUCCESS',
    public CREATE_ITEM_SUCCESS: string = 'CREATE_ITEM_SUCCESS',
    public DELETE_ITEM_SUCCESS: string = 'DELETE_ITEM_SUCCESS',
    public UPDATE_ITEM_SUCCESS: string = 'UPDATE_ITEM_SUCCESS',
    public FIND_ITEM_SUCCESS: string = 'FIND_ITEM_SUCCESS',

    // FAILURE - FETCH CREATE DELETE UPDATE
    public FETCH_OPTIONS_ITEM_FAILURE: string = 'FETCH_OPTIONS_ITEM_FAILURE',
    public FETCH_ITEM_FAILURE: string = 'FETCH_ITEM_FAILURE',
    public CREATE_ITEM_FAILURE: string = 'CREATE_ITEM_FAILURE',
    public DELETE_ITEM_FAILURE: string = 'DELETE_ITEM_FAILURE',
    public UPDATE_ITEM_FAILURE: string = 'UPDATE_ITEM_FAILURE',
    public FIND_ITEM_FAILURE: string = 'FIND_ITEM_FAILURE'

  ) {
    if (config) {
      this.load(config)
    }
  }

  load (config: any = {}) {
    let name = 'ITEM'
    if (config && typeof config === 'string') {
      name = config.toUpperCase()
      config = { name }
    }
    this.name = name
    this.config = config
    this.SET_ACTION_ITEM = (config.SET_ACTION_ITEM || `SET_ACTION_${name}`)
    this.SET_PAGINATION_ITEM = (config.SET_ACTION_ITEM || `SET_PAGINATION_${name}`)
    this.SET_FILTER_ITEM = (config.SET_FILTER_ITEM || `SET_FILTER_${name}`)
    this.SET_FILTER_OPTIONS_ITEM = (config.SET_FILTER_OPTIONS_ITEM || `SET_FILTER_OPTIONS_${name}`)
    this.SET_FIELDS_ITEM = (config.SET_FIELDS_ITEM || `SET_FIELDS_${name}`)
    this.SET_FIELDS_OPTIONS_ITEM = (config.SET_FIELDS_OPTIONS_ITEM || `SET_FIELDS_OPTIONS_${name}`)
    this.SET_SORT_ITEM = (config.SET_SORT_ITEM || `SET_SORT_${name}`)
    this.CLEAR_ITEM = (config.CLEAR_ITEM || `CLEAR_${name}`)
    this.CLEAR_OPTIONS_ITEM = (config.CLEAR_OPTIONS_ITEM || `CLEAR_OPTIONS_${name}`)

    // FETCH CREATE DELETE UPDATE
    this.FETCH_OPTIONS_ITEM = (config.FETCH_OPTIONS_ITEM || `FETCH_OPTIONS_${name}`)
    this.FETCH_ITEM = (config.FETCH_ITEM || `FETCH_${name}`)
    this.CREATE_ITEM = (config.CREATE_ITEM || `CREATE_${name}`)
    this.DELETE_ITEM = (config.DELETE_ITEM || `DELETE_${name}`)
    this.UPDATE_ITEM = (config.UPDATE_ITEM || `UPDATE_${name}`)
    this.FIND_ITEM = (config.FIND_ITEM || `FIND_${name}`)

    // PENDING - FETCH CREATE DELETE UPDATE 
    this.FETCH_OPTIONS_ITEM_PENDING = (config.FETCH_OPTIONS_ITEM_PENDING || `FETCH_OPTIONS_${name}_PENDING`)
    this.FETCH_ITEM_PENDING = (config.FETCH_ITEM_PENDING || `FETCH_${name}_PENDING`)
    this.CREATE_ITEM_PENDING = (config.CREATE_ITEM_PENDING || `CREATE_${name}_PENDING`)
    this.DELETE_ITEM_PENDING = (config.DELETE_ITEM_PENDING || `DELETE_${name}_PENDING`)
    this.UPDATE_ITEM_PENDING = (config.UPDATE_ITEM_PENDING || `UPDATE_${name}_PENDING`)
    this.FIND_ITEM_PENDING = (config.FIND_ITEM_PENDING || `FIND_${name}_PENDING`)
    
    // SUCCESS - FETCH CREATE DELETE UPDATE
    this.FETCH_OPTIONS_ITEM_SUCCESS = (config.FETCH_OPTIONS_ITEM_SUCCESS || `FETCH_OPTIONS_${name}_SUCCESS`)
    this.FETCH_ITEM_SUCCESS = (config.FETCH_ITEM_SUCCESS || `FETCH_${name}_SUCCESS`)
    this.CREATE_ITEM_SUCCESS = (config.CREATE_ITEM_SUCCESS || `CREATE_${name}_SUCCESS`)
    this.DELETE_ITEM_SUCCESS = (config.DELETE_ITEM_SUCCESS || `DELETE_${name}_SUCCESS`)
    this.UPDATE_ITEM_SUCCESS = (config.UPDATE_ITEM_SUCCESS || `UPDATE_${name}_SUCCESS`)
    this.FIND_ITEM_SUCCESS = (config.FIND_ITEM_SUCCESS || `FIND_${name}_SUCCESS`)

    // FAILURE - FETCH CREATE DELETE UPDATE
    this.FETCH_OPTIONS_ITEM_FAILURE = (config.FETCH_OPTIONS_ITEM_FAILURE || `FETCH_OPTIONS_${name}_FAILURE`)
    this.FETCH_ITEM_FAILURE = (config.FETCH_ITEM_FAILURE || `FETCH_${name}_FAILURE`)
    this.CREATE_ITEM_FAILURE = (config.CREATE_ITEM_FAILURE || `CREATE_${name}_FAILURE`)
    this.DELETE_ITEM_FAILURE = (config.DELETE_ITEM_FAILURE || `DELETE_${name}_FAILURE`)
    this.UPDATE_ITEM_FAILURE = (config.UPDATE_ITEM_FAILURE || `UPDATE_${name}_FAILURE`)
    this.FIND_ITEM_FAILURE = (config.FIND_ITEM_FAILURE || `FIND_${name}_FAILURE`)

    return this
  }
}

export enum CollectionTypeEnum {
  'LIST', 'TABLE'
}
export enum ActionTypeEnum {
  'LIST', 'REMOVE', 'SHOW', 'EDIT', 'NEW'
}