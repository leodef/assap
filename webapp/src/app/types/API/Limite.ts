import * as Yup from 'yup'
import { DTO } from '../DTO'

 /**
 * DTO
 * 
 * @name Limite
 * @module types/API/Limite
 * @category Tipos
 * @subcategory Limite
 */
export class Limite extends DTO {
  peso?: Number
  volume?: Number
  tara?: Number

  
  static transporteToLimite(transporte: any) {
    return transporteToLimite(transporte);
  }

  static compare(limiteA: any, limiteB: any) {
    return compare(limiteA, limiteB);
  }

  
  static accum(limiteA: any, limiteB: any) {
    return accum(limiteA, limiteB);
  }

}

 /**
 * Esquema para formulário
 * 
 * @name LimiteSchema
 * @module types/API/Limite
 * @category Tipos
 * @subcategory Limite
 */
export const LimiteSchema = Yup.object().shape({
  peso: Yup.number(),
  volume: Yup.number(),
  tara: Yup.number()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Limite
 * @category Tipos
 * @subcategory Limite
 */
export const initialValues = {
  peso: 0,
  volume: 0,
  tara: 0
}

export function accum(limiteA: any, limiteB: any) {
  return limiteA && limiteB ? ({
    peso: (limiteA.peso + limiteB.peso),
    volume: (limiteA.volume + limiteB.volume),
    tara: (limiteA.tara + limiteB.tara)
  }) : {peso: 0, volume: 0, tara: 0}
}

export function compare(limiteA: any, limiteB: any) {
  return limiteA && limiteB && ( 
    limiteA.peso === limiteB.peso &&
    limiteA.volume === limiteB.volume &&
    limiteA.tara === limiteB.tara)
}

export function transporteToLimite(transporte: any) {
  if(!transporte){
    return initialValues
  }
  const carreta = transporte.carreta
  const cavalo = transporte.cavalo || transporte.veiculo
  const limiteCavalo = cavalo ? cavalo.limite : 0
  const limiteCarreta = carreta ? carreta.limite : 0
  const pesoCarreta = limiteCarreta ? limiteCarreta.peso : 0
  const volumeCarreta = limiteCarreta ? limiteCarreta.volume : 0 
  const taraCarreta = limiteCarreta ? limiteCarreta.tara : 0
  const pesoCavalo = limiteCavalo ? limiteCavalo.peso : 0
  const volumeCavalo = limiteCavalo ? limiteCavalo.volume : 0 
  const taraCavalo = limiteCavalo ? limiteCavalo.tara : 0
  const result = ({
    tara: (taraCarreta + taraCavalo),
    peso: (0 < pesoCarreta && pesoCarreta < pesoCavalo
      ? pesoCarreta
      : pesoCavalo),
    volume: (0 < volumeCarreta ? volumeCarreta : volumeCavalo)
  })
  return result
}

/**
 * Converte a entidade Rota para a rota utilizado em calculo de rota
 * 
 * @param {Rota} rota Rota a ser convertida
 * @param {{ elements: Array<any> }} rows Retorno da calculo de distancia da Api do google
 * @param {CalculoRotaRequest} result Rota a ser preenchida com as informações
 *
 * @returns {CalculoRotaRequest}
 */
export function limiteToCalculoRotaLimit (
  limite?: Limite,
  result = new CalculoRotaRequestLimit()
): CalculoRotaRequestLimit {
  if(!limite){ return result }
  result.peso = limite.peso
  result.volume = limite.volume
  return result
}

 /**
 * DTO limite do calculo de rotas
 * 
 * @name CalculoRotaRequestLimit
 * @module types/Modulo/Logistica/Limite
 * @category Tipos
 * @subcategory Limite
 */
export class CalculoRotaRequestLimit {
  peso?: Number
  volume?: Number
}