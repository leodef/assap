import _ from 'lodash'
import * as Yup from 'yup'
import {
  CrudType
} from '../../Crud'
import {
  DTO
} from '../../DTO'
import {
  TipoContatoInfo, TipoContatoInfoSchema
} from './TipoContatoInfo'

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/ContatoInfo
 * @category Tipos
 * @subcategory ContatoInfo
 */
export const crudType = new CrudType('TIPO_UNIDADE_MEDIDA')

 /**
 * DTO
 * 
 * @name ContatoInfo
 * @module types/API/ContatoInfo
 * @category Tipos
 * @subcategory ContatoInfo
 */
export class ContatoInfo extends DTO {
  valor?: String
  tipo?: TipoContatoInfo
  
  toDTO() {
    const obj = _.clone(this) as any
    obj.tipo = (this.tipo ? this.tipo._id || this.tipo : null)
    return obj
  }
}

 /**
 * Esquema para formulário
 * 
 * @name ContatoInfoSchema
 * @module types/API/ContatoInfo
 * @category Tipos
 * @subcategory ContatoInfo
 */
export const ContatoInfoSchema = Yup.object().shape({
  valor: Yup.string(),
  tipo: TipoContatoInfoSchema.nullable(true)
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/ContatoInfo
 * @category Tipos
 * @subcategory ContatoInfo
 */
export const initialValues = {
  valor: '',
  tipo: null
}
