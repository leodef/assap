import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { DTO } from '../../DTO'


 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/TipoContatoInfo
 * @category Tipos
 * @subcategory TipoContatoInfo
 */
export const crudType = new CrudType('TIPO_UNIDADE_MEDIDA')


 /**
 * DTO
 * 
 * @name TipoContatoInfo
 * @module types/API/TipoContatoInfo
 * @category Tipos
 * @subcategory TipoContatoInfo
 */
export class TipoContatoInfo extends DTO {
  titulo?: String
  desc?: String
  mask?: String
}

 /**
 * Esquema para formulário
 * 
 * @name TipoContatoInfoSchema
 * @module types/API/TipoContatoInfo
 * @category Tipos
 * @subcategory TipoContatoInfo
 */
export const TipoContatoInfoSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  desc: Yup.string(),
  mask: Yup.string()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/TipoContatoInfo
 * @category Tipos
 * @subcategory TipoContatoInfo
 */
export const initialValues = {
  titulo: '',
  desc: '',
  mask: ''
}
