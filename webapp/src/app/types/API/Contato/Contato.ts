import _ from 'lodash'
import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { DTO } from '../../DTO'
import { ContatoInfo, ContatoInfoSchema } from './ContatoInfo'

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/Contato/Contato
 * @category Tipos
 * @subcategory Contato
 */
export const crudType = new CrudType('TIPO_UNIDADE_MEDIDA')

 /**
 * DTO
 * 
 * @name Contato
 * @module types/API/Contato/Contato
 * @category Tipos
 * @subcategory Contato
 */
export class Contato extends DTO {
  titulo?: String
  desc?: String
  infos?: Array<ContatoInfo>

    
  toDTO() {
    const obj = _.clone(this) as any
    obj.tipo = (this.infos || []).map(
      (val: any) => new ContatoInfo().fromJson(val).toDTO()
    )
    return obj
  }
}

 /**
 * Esquema para formulário
 * 
 * @name ContatoSchema
 * @module types/API/Contato/Contato
 * @category Tipos
 * @subcategory Contato
 */
export const ContatoSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  desc: Yup.string(),
  infos: Yup.array().of(ContatoInfoSchema)
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Contato/Contato
 * @category Tipos
 * @subcategory Contato
 */
export const initialValues = {
  titulo: '',
  desc: '',
  infos: []
}
