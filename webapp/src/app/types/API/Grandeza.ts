import * as Yup from 'yup'
import { CrudType } from '../Crud'
import { DTO } from '../DTO'

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/Grandeza
 * @category Tipos
 * @subcategory Grandeza
 */
export const crudType = new CrudType('GRANDEZA')

 /**
 * DTO
 * 
 * @name Grandeza
 * @module types/API/Grandeza
 * @category Tipos
 * @subcategory Grandeza
 */
export class Grandeza extends DTO {
    titulo?: String
    desc?: String
}

 /**
 * Esquema para formulário
 * 
 * @name GrandezaSchema
 * @module types/API/Grandeza
 * @category Tipos
 * @subcategory Grandeza
 */
export const GrandezaSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  desc: Yup.string().nullable(true)
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Grandeza
 * @category Tipos
 * @subcategory Grandeza
 */
export const initialValues = {
  titulo: '',
  desc: ''
}
