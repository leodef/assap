import * as Yup from 'yup'
import { CrudType } from '../Crud'
import { DTO } from '../DTO'

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/Produto
 * @category Tipos
 * @subcategory Produto
 */
export const crudType = new CrudType('PRODUTO')

 /**
 * DTO
 * 
 * @name Produto
 * @module types/API/Produto
 * @category Tipos
 * @subcategory Produto
 */
export class Produto extends DTO {
  titulo?: String
  desc?: String
  tipo?: String
  marca?: String
  densidade?: Number
}

 /**
 * Esquema para formulário
 * 
 * @name ProdutoSchema
 * @module types/API/Produto
 * @category Tipos
 * @subcategory Produto
 */
export const ProdutoSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  desc: Yup.string(),
  tipo: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  marca: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  densidade: Yup.number()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Produto
 * @category Tipos
 * @subcategory Produto
 */
export const initialValues = {
  titulo: '',
  desc: '',
  tipo: '',
  marca: '',
  densidade: 0
}
