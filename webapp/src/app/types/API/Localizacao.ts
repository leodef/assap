import * as Yup from 'yup'
import { DTO } from '../DTO'

 /**
 * DTO
 * 
 * @name Localizacao
 * @module types/API/Localizacao
 * @category Tipos
 * @subcategory Localizacao
 */
export class Localizacao extends DTO {
  titulo?: String
  endereco?: String
  lat?: Number
  long?: Number
  pais?: String
  estado?: String
  cidade?: String
  area?: String
  codigoPostal?: String
  numero?: String
  complemento?: String
}

 /**
 * Esquema para formulário
 * 
 * @name LocalizacaoSchema
 * @module types/API/Localizacao
 * @category Tipos
 * @subcategory Localizacao
 */
export const LocalizacaoSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .nullable(true),
  endereco: Yup.string()
    .min(2, 'Too Short!'),
  lat: Yup.number()
    .min(-90, 'Too Short!')
    .max(90, 'Too Long!'),
  long: Yup.number()
    .min(-180, 'Too Short!')
    .max(180, 'Too Long!'),
  pais: Yup.string(),
  estado: Yup.string(),
  cidade: Yup.string(),
  area: Yup.string(),
  codigoPostal: Yup.string(),
  numero: Yup.string(),
  complemento: Yup.string()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Localizacao
 * @category Tipos
 * @subcategory Localizacao
 */
export const initialValues = {
  titulo: '',
  endereco: '',
  lat: 0,
  long: 0,
  pais: '',
  estado: '',
  cidade: '',
  area: '',
  codigoPostal: '',
  numero: '',
  complemento: ''
}
