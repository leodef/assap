import _ from 'lodash'
import * as Yup from 'yup'
import { CrudType } from '../Crud'
import { DTO } from '../DTO'

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/UnidadeMedida
 * @category Tipos
 * @subcategory UnidadeMedida
 */
export const crudType = new CrudType('UNIDADE_MEDIDA')

 /**
 * DTO
 * 
 * @name UnidadeMedida
 * @module types/API/UnidadeMedida
 * @category Tipos
 * @subcategory UnidadeMedida
 */
export class UnidadeMedida extends DTO {
  grandeza?: any
  titulo?: String
  desc?: String
  valor?: Number
  
  toDTO() {
    const obj = _.clone(this)
    obj.grandeza = (this.grandeza._id || this.grandeza)
    return obj
  }
}

 /**
 * Esquema para formulário
 * 
 * @name UnidadeMedidaSchema
 * @module types/API/UnidadeMedida
 * @category Tipos
 * @subcategory UnidadeMedida
 */
export const UnidadeMedidaSchema = Yup.object().shape({
  grandeza: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .uppercase()
    .required('Required'),
  desc: Yup.string(),
  valor: Yup.number()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/UnidadeMedida
 * @category Tipos
 * @subcategory UnidadeMedida
 */
export const initialValues = {
  grandeza: '',
  titulo: '',
  desc: '',
  valor: 0
}
