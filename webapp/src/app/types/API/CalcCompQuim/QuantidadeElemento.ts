import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { DTO } from '../../DTO'

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/CalcCompQuim/QuantidadeElemento
 * @category Tipos
 * @subcategory QuantidadeElemento
 */
export const crudType = new CrudType('QUANTIDADE_ELEMENTO')

 /**
 * DTO
 * 
 * @name QuantidadeElemento
 * @module types/API/CalcCompQuim/QuantidadeElemento
 * @category Tipos
 * @subcategory QuantidadeElemento
 */
export class QuantidadeElemento extends DTO {
  elemento?: String
  quantidade?: Number
}

 /**
 * Esquema para formulário
 * 
 * @name QuantidadeElementoSchema
 * @module types/API/CalcCompQuim/QuantidadeElemento
 * @category Tipos
 * @subcategory QuantidadeElemento
 */
export const QuantidadeElementoSchema = Yup.object().shape({
  elemento: Yup.string()
    .min(2, 'Too Short!')
    .uppercase()
    .required('Required'),
  quantidade: Yup.number()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/CalcCompQuim/QuantidadeElemento
 * @category Tipos
 * @subcategory QuantidadeElemento
 */
export const initialValues = {
  elemento: '',
  quantidade: 0
}
