import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { DTO } from '../../DTO'

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/CalcCompQuim/Elemento
 * @category Tipos
 * @subcategory Elemento
 */
export const crudType = new CrudType('ELEMENTO')

 /**
 * DTO
 * 
 * @name Elemento
 * @module types/API/CalcCompQuim/Elemento
 * @category Tipos
 * @subcategory Elemento
 */
export class Elemento extends DTO {
    titulo?: String
    desc?: String
}

 /**
 * Esquema para formulário
 * 
 * @name ElementoSchema
 * @module types/API/CalcCompQuim/Elemento
 * @category Tipos
 * @subcategory Elemento
 */
export const ElementoSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(1, 'Too Short!')
    .max(10, 'Too Long!')
    .required('Required'),
  desc: Yup.string()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/CalcCompQuim/Elemento
 * @category Tipos
 * @subcategory Elemento
 */
export const initialValues = {
  titulo: '',
  desc: ''
}
