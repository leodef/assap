import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { DTO } from '../../DTO'

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/CalcCompQuim/Substancia
 * @category Tipos
 * @subcategory Substancia
 */
export const crudType = new CrudType('SUBSTANCIA')

 /**
 * DTO
 * 
 * @name Substancia
 * @module types/API/CalcCompQuim/Substancia
 * @category Tipos
 * @subcategory Substancia
 */
export class Substancia extends DTO {
    titulo?: String
    desc?: String
    valor?: Number
}

 /**
 * Esquema para formulário
 * 
 * @name SubstanciaSchema
 * @module types/API/CalcCompQuim/Substancia
 * @category Tipos
 * @subcategory Substancia
 */
export const SubstanciaSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  desc: Yup.string()
    .max(600, 'Too Long!'),
  valor: Yup.number()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/CalcCompQuim/Substancia
 * @category Tipos
 * @subcategory Substancia
 */
export const initialValues = {
  titulo: '',
  desc: '',
  valor: 0
}
