import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { DTO } from '../../DTO'


 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/CalcCompQuim/ComposicaoSubstancia
 * @category Tipos
 * @subcategory ComposicaoSubstancia
 */
export const crudType = new CrudType('COMPOSICAO_SUBSTANCIA')

 /**
 * DTO
 * 
 * @name ComposicaoSubstancia
 * @module types/API/CalcCompQuim/ComposicaoSubstancia
 * @category Tipos
 * @subcategory ComposicaoSubstancia
 */
export class ComposicaoSubstancia extends DTO {
    titulo?: String
    desc?: String
    valor?: Number
    // substancias: Array<QuantidadeSubstancia> = [];
}

 /**
 * Esquema para formulário
 * 
 * @name ComposicaoSubstanciaSchema
 * @module types/API/CalcCompQuim/ComposicaoSubstancia
 * @category Tipos
 * @subcategory ComposicaoSubstancia
 */
export const ComposicaoSubstanciaSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .uppercase()
    .required('Required'),
  desc: Yup.string(),
  valor: Yup.number()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/CalcCompQuim/ComposicaoSubstancia
 * @category Tipos
 * @subcategory ComposicaoSubstancia
 */
export const initialValues = {
  titulo: '',
  desc: '',
  valor: 0
}
