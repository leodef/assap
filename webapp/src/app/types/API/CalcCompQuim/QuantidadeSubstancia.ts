import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { DTO } from '../../DTO'
 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/CalcCompQuim/QuantidadeSubstancia
 * @category Tipos
 * @subcategory QuantidadeSubstancia
 */
export const crudType = new CrudType('QUANTIDADE_SUBSTANCIA')

 /**
 * DTO
 * 
 * @name QuantidadeSubstancia
 * @module types/API/CalcCompQuim/QuantidadeSubstancia
 * @category Tipos
 * @subcategory QuantidadeSubstancia
 */
export class QuantidadeSubstancia extends DTO {
    substancia?: String
    quantidade?: Number
}

 /**
 * Esquema para formulário
 * 
 * @name QuantidadeSubstanciaSchema
 * @module types/API/CalcCompQuim/QuantidadeSubstancia
 * @category Tipos
 * @subcategory QuantidadeSubstancia
 */
export const QuantidadeSubstanciaSchema = Yup.object().shape({
  substancia: Yup.string()
    .min(2, 'Too Short!')
    .uppercase()
    .required('Required'),
  quantidade: Yup.number()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/CalcCompQuim/QuantidadeSubstancia
 * @category Tipos
 * @subcategory QuantidadeSubstancia
 */
export const initialValues = {
  substancia: '',
  quantidade: 0
}
