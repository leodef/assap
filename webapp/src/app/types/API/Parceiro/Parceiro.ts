import _ from 'lodash'
import * as Yup from 'yup'
import { TIPO_PESSOA } from '../../../enums/tipo-pessoa'
import { CrudType } from '../../Crud'
import { DTO } from '../../DTO'
import {
  Filial,
  FilialSchema
} from '../Filial'
import {
  ContatoParceiro,
  ContatoParceiroSchema
} from './ContatoParceiro'

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/Parceiro/Parceiro
 * @category Tipos
 * @subcategory Parceiro
 */
export const crudType = new CrudType('PARCEIRO')

 /**
 * DTO
 * 
 * @name Parceiro
 * @module types/API/Parceiro/Parceiro
 * @category Tipos
 * @subcategory Parceiro
 */
export class Parceiro extends DTO {
  titulo?: String
  desc?: String
  identificacao?: String
  tipoPessoa?: String
  filiais?: Array<Filial>
  contatos?: Array<ContatoParceiro>

  fromJson (json: any) {
    if (!json) { return this }
    super.fromJson(json)
    this.filiais = (json.filiais || []).map( (filial: any) =>
      new Filial().fromJson(filial)
    )
    this.contatos = (json.contatos || []).map( (contato: any) =>
      new ContatoParceiro().fromJson(contato)
    )
    return this
  }

  toDTO() {
    const obj = _.clone(this)
    obj.filiais = (this.filiais || [])
    obj.contatos = (this.contatos || []).map(
      (val: any) => new ContatoParceiro().fromJson(val).toDTO()
    )
    return obj
  }
}

 /**
 * Esquema para formulário
 * 
 * @name ParceiroSchema
 * @module types/API/Parceiro/Parceiro
 * @category Tipos
 * @subcategory Parceiro
 */
export const ParceiroSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  desc: Yup.string(),
  identificacao: Yup.string(),
  tipoPessoa:  Yup.string().oneOf(Object.keys(TIPO_PESSOA)),
  filiais: Yup.array().of(FilialSchema.nullable(true)).nullable(true),
  contatos: Yup.array().of(ContatoParceiroSchema.nullable(true)).nullable(true)
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Parceiro/Parceiro
 * @category Tipos
 * @subcategory Parceiro
 */
export const initialValues = {
  titulo: '',
  desc: '',
  identificacao: '',
  tipoPessoa: '',
  filiais: [],
  contatos: []
}
