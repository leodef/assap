import _ from 'lodash'
import * as Yup from 'yup'
import { DTO } from '../../DTO'
import {
  Contato,
  ContatoSchema,
  initialValues as contatoInitialValues
} from '../Contato/Contato'
import { Filial, FilialSchema } from '../Filial'

 /**
 * DTO
 * 
 * @name ContatoParceiro
 * @module types/API/Parceiro/ContatoParceiro
 * @category Tipos
 * @subcategory ContatoParceiro
 */
export class ContatoParceiro extends DTO {
  filial?: Filial
  contato?: Contato

  fromJson (json: any) {
    const result = super.fromJson(json)
    if (!result) { return result }
    this.filial = json.filial
      ? new Filial().fromJson(json.filial)
      : null
    this.contato = json.contato
      ? new Contato().fromJson(json.contato)
      : null
    return this
  }

  toDTO() {
    const obj = _.clone(this) as any
    obj.filial = (
      this.filial
        ? this.filial._id ||
          this.filial._temp ||
          this.filial
        : undefined
      )
    obj.contato = this.contato
      ? new Contato().fromJson(this.contato).toDTO()
      : null
    return obj
  }
}

 /**
 * Esquema para formulário
 * 
 * @name ContatoParceiroSchema
 * @module types/API/Parceiro/ContatoParceiro
 * @category Tipos
 * @subcategory ContatoParceiro
 */
export const ContatoParceiroSchema = Yup.object().shape({
  filial: FilialSchema.nullable(true),
  contato: ContatoSchema.nullable(true)
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Parceiro/ContatoParceiro
 * @category Tipos
 * @subcategory ContatoParceiro
 */
export const initialValues = {
  filial: null,
  contato: contatoInitialValues
}
