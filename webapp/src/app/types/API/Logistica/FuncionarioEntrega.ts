import _ from 'lodash'
import * as Yup from 'yup'
import { TIPO_FUNCIONARIO_ENTREGA } from '../../../enums/tipo-funcionario-entrega'
import { DTO } from '../../DTO'
import { Funcionario, FuncionarioSchema } from '../Funcionario'

 /**
 * DTO
 * 
 * @name FuncionarioEntrega
 * @module types/API/Logistica/FuncionarioEntrega
 * @category Tipos
 * @subcategory FuncionarioEntrega
 */
export class FuncionarioEntrega extends DTO {
  funcionario?: Funcionario
  tipo?: String

  fromJson (json: any) {
    if (!json) { return this }
    super.fromJson(json)
    this.funcionario = json.funcionario
      ? new Funcionario().fromJson(json.funcionario)
      : null
    return this
  }

  toJson () {
    return JSON.stringify(this)
  }

  toDTO() {
    const obj = _.clone(this) as any
    obj.funcionario = (this.funcionario
      ? this.funcionario._id || this.funcionario
      : undefined)
    return obj
  }

}

 /**
 * Esquema para formulário
 * 
 * @name FuncionarioEntregaSchema
 * @module types/API/Logistica/FuncionarioEntrega
 * @category Tipos
 * @subcategory FuncionarioEntrega
 */
export const FuncionarioEntregaSchema = Yup.object().shape({
  funcionario: FuncionarioSchema.nullable(true),
  tipo: Yup.string().oneOf(Object.keys(TIPO_FUNCIONARIO_ENTREGA))
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Logistica/FuncionarioEntrega
 * @category Tipos
 * @subcategory FuncionarioEntrega
 */
export const initialValues = {
  funcionario: null,
  tipo: TIPO_FUNCIONARIO_ENTREGA.GERAL
}
