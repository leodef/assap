import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import {
  // eslint-disable-next-line no-unused-vars
  Localizacao,
  initialValues as localizacaoInitialValues,
  LocalizacaoSchema
} from '../Localizacao'
import {
  initialValues as limiteInitialValues,
  // eslint-disable-next-line no-unused-vars
  Limite,
  LimiteSchema,
  limiteToCalculoRotaLimit
} from '../Limite'
import {
  STATUS_ENTREGA
} from '../../../enums/status-entrega'
import {
  DTO
} from '../../DTO'
import {
  TIPO_FORMACAO
} from '../../../enums/tipo-formacao'
import {
  Filial, FilialSchema
} from '../Filial'
import {
  Parceiro, ParceiroSchema
} from '../Parceiro/Parceiro'
import _ from 'lodash'
const cleanLabel = DTO.cleanLabel

 /**
 * Gera o titulo do item
 * 
 * @name generateTitulo
 * @module types/API/Logistica/PontoRota
 * @category Tipos
 * @subcategory PontoRota
 */

export const generateTitulo = (obj: any) => {
  if(!obj) {
    return ''
  }
  const { localizacao, filial, parceiro } = obj
  const limiteTitulo = obj.limite && obj.limite.peso
    ? `${obj.limite.peso > 0 ? 'Carga' : 'Descarga' }: ${Math.abs(obj.limite.peso)}`
    : ''
  const parceiroTitulo = parceiro
    ? parceiro.titulo
    : null
  const filialTitulo = filial
    ? filial.titulo
    : null

  const localizacaoTitulo = localizacao
    ? localizacao.endereco || ` lat: ${localizacao.lat}, lng: ${localizacao.long} `
    : null
  const descTitulo = [
    parceiroTitulo,
    filialTitulo,
    limiteTitulo,
    localizacaoTitulo
  ].filter((val) => Boolean(val)).join(' ')

  return cleanLabel(`#${obj.ordem} ${descTitulo}`)
}

 /**
 * Gera a descrição do item
 * @module types/API/Logistica/PontoRota
 * @category Tipos
 * @subcategory PontoRota
 */
export const generateDesc = (obj: any) => {
  if(!obj) {
    return ''
  }
  const {
    limite,
    localizacao,
    parceiro,
    filial
  } = obj;

  const parceiroFilialDesc =  parceiro || filial
    ? [
        (parceiro && parceiro.titulo
          ? 'Parceiro: ' + parceiro.titulo
          : ''),
        (filial && filial.titulo
          ? 'Filial: ' + filial.titulo
          : '')
      ].filter(val => Boolean(val)).join(', ')+'\n'
    : ''

  const cargaDescargaDesc = limite
    ? `${
        limite.peso > 0
          ? 'Carga'
          : 'Descarga'
      }( ${
        limite.peso
        ? 'Peso: ' + limite.peso
        : ''
      } ${
        limite.volume
          ? 'Volume: ' + limite.volume
          : ''
      })\n`
    : ''
  const trajetoAnteriorSpace = '  '
  const trajetoAnteriorDesc = (obj.distanciaDesc || obj.distancia)
    ? `${
      'Trajeto da parada anterior até a atual:\n'
      }${[
        (obj.distanciaDesc || obj.distancia
          ? trajetoAnteriorSpace + 'Distância: ' + (obj.distanciaDesc || obj.distancia)
          : null),
        (obj.duracaoDesc || obj.duracao
          ? trajetoAnteriorSpace + 'Duração: ' + (obj.duracaoDesc || obj.duracao)
          : null),
        (obj.duracaoComTrafegoDesc || obj.duracaoComTrafego
          ? trajetoAnteriorSpace + 'Duração com Tráfego: ' + (obj.duracaoComTrafegoDesc || obj.duracaoComTrafego) 
          : null)
      ].filter(val => Boolean(val)).join(', ')
    }`
    : ''
 
  const localizacaoSpace = '  '
  const localizacaoDesc = localizacao ? `${
      localizacaoSpace + 'Localização:\n'
    }${
      localizacao.endereco
      ? 'Endereço ' + localizacao.endereco
      : ''
    }\n${
      localizacao.lat || localizacao.long
        ? `Lat / Lng: ${localizacao.lat} / ${localizacao.long}`
        : ''
    }\n${
      localizacao.pais
      ? 'Pais: ' + localizacao.pais
      : ''
    }, ${
      localizacao.estado
        ? 'Estado: ' + localizacao.estado
        : ''
    }, ${
      localizacao.cidade
        ? 'Cidade: ' + localizacao.cidade
        : ''
    }\n${
      localizacao.area
        ? 'Bairro: ' + localizacao.area
        : ''
    }, ${
        localizacao.numero
          ? 'Número ' + localizacao.numero
          : ''
    }, ${
      localizacao.complemento
        ? 'Complemento: ' + localizacao.complemento
        : ''
    }\n${
      localizacao.codigoPostal
        ? 'Código Postal ' + localizacao.codigoPostal
        : ''
    }\n` : ''
  
  return cleanLabel(`${
      'Parada Rota #' + obj.ordem + ':\n'
    }${
      trajetoAnteriorDesc
    }${
      obj.anterior
        ? 'Anterior: ' + obj.anterior
        .map((ant: any) => ant ? ant.titulo : null)
        .filter((val: any) => Boolean(val))
        .join(', ') + '\n' 
        : ''
    }${
      cargaDescargaDesc
    }${
      obj.status
        ? 'Status: ' + obj.status + '\n' 
        : ''
    }${
      obj._id || obj._temp
        ? 'ID: ' + (obj._id || obj._temp) + '\n' 
        : ''
    }${
      parceiroFilialDesc
    }${
      localizacaoDesc
    }`)
}



export const getSubTitulo = (obj: any): string => {

  if(obj && obj.tipoFormacaoTitulo === TIPO_FORMACAO.MANUAL) {
    const limiteTitulo = obj.limite && obj.limite.peso
    ? `${obj.limite.peso > 0 ? 'Carga' : 'Descarga' }: ${Math.abs(obj.limite.peso)}`
    : ''
    return `${obj.titulo ? obj.titulo.substring(0, 20) : ''} ${limiteTitulo}`
  }
  return generateTitulo({
      ...obj,
      localizacao: {
        ...(obj && obj.localizacao
            ? {
              ...obj.localizacao,
              endereco: obj.localizacao.endereco
                ? obj.localizacao.endereco.substring(0, 20)
                : ''
            }
            : {}
        )
      }
    })
}
 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/Logistica/PontoRota
 * @category Tipos
 * @subcategory PontoRota
 */
export const crudType = new CrudType('PONTO_ROTA')

 /**
 * DTO
 * 
 * @name PontoRota
 * @module types/API/Logistica/PontoRota
 * @category Tipos
 * @subcategory PontoRota
 */
export class PontoRota extends DTO {
  titulo?: String
  tipoFormacaoTitulo?: String
  desc?: String
  tipoFormacaoDesc?: String
  ordem?: Number;
  distancia?: Number;
  duracao?: Number;
  duracaoComTrafego?: Number;
  distanciaDesc?: String;
  duracaoDesc?: String;
  duracaoComTrafegoDesc?: String;
  localizacao?: Localizacao;
  limite?: Limite;
  status?: String;
  anterior: Array<any> = [];
  _temp?: String;
  usarFilial?: Boolean;
  parceiro?: Parceiro;
  filial?: Filial;

  static getPontoRotaId (ponto: any){
    return getPontoRotaId(ponto)
  }

  static loadOrdem (value: any, pontos: Array<any>) {
    const length = pontos.length
    if (!value.ordem) { value.ordem = pontos ? length : 0 }
    value.ordem = value.ordem || length
    return value
  }

  static atualizarOrdemRota (ponto: any, ordem: number, pontos: Array<any> = []): Array<any> {
    if (!ponto || !ponto.ordem || ponto.ordem === ordem) {
      return pontos
    }
    const nextPonto = pontos.find((pto: any, index: number) =>
      pto.ordem === ordem)
    return (nextPonto
      ? PontoRota.atualizarOrdemRota(nextPonto, (nextPonto.ordem + 1), pontos)
      : pontos)
      .map((pto: any, index: number) =>
        pto.ordem === ponto.ordem ? { ...ponto, ordem } : pto)
      .sort((a: any, b: any) =>
        a.ordem - b.ordem)
  }

  static compare (value: any, other: any, index?: number): boolean {
    const result = (
      Boolean(other) &&
      Boolean(value) &&
      (
        (Boolean(other._id) && other._id === value._id)
        || (Boolean(other._temp) && other._temp === value._temp)
        // || (other.ordem && other.ordem === value.ordem) ||
        //  || (Boolean(index) && !other.ordem && value.ordem === index)
      )
    )
    return result
  }

  static generateTitulo(obj: any): string {
    return generateTitulo(obj)
  }

  static generateDesc(obj: any): string {
    return generateDesc(obj)
  }

  static getSubTitulo(obj: any): string  {
    return getSubTitulo(obj)
  }

  getPontoRotaId (){
    return PontoRota.getPontoRotaId(this)
  }

  generateTitulo(): string  {
    return generateTitulo(this)
  }

  generateDesc(): string  {
    return generateDesc(this)
  }

  getSubTitulo(other?: any): string  {
    return getSubTitulo(this)
  }

  fromJson (json: any) {
    if (!json) { return this }
    super.fromJson(json)
    this.localizacao = json.localizacao
      ? new Localizacao().fromJson(json.localizacao)
      : null
    this.filial = json.filial
      ? new Filial().fromJson(json.filial)
      : null
    this.parceiro = json.parceiro
      ? new Parceiro().fromJson(json.parceiro)
      : undefined
    this.anterior = (json.anterior || []).map(
      (pontoAnterior: any) => new PontoRota().fromJson(pontoAnterior)
    )
    return this
  }

  toJson () {
    return JSON.stringify(this)
  }
      
  toDTO() {
    const obj = _.clone(this) as any
    obj.localizacao = new Localizacao().fromJson(this.localizacao).toDTO()
    obj.parceiro = (
      this.parceiro
        ? this.parceiro._id || this.parceiro
        : undefined
      )
    obj.filial = (
      this.filial
        ? this.filial._id || this.filial
        : undefined
      )
    obj.anterior = (this.anterior || [])
      .map( (pontoAnterior: any) => (
        pontoAnterior
          ? pontoAnterior._id ||
            pontoAnterior._temp ||
            pontoAnterior
          : undefined
        ))
      .filter((val: any) => Boolean(val))
    return obj;
  }

  compare (value: any): boolean {
    return PontoRota.compare(this, value)
  }
}

 /**
 * Esquema para formulário
 * 
 * @name PontoRotaSchema
 * @module types/API/Logistica/PontoRota
 * @category Tipos
 * @subcategory PontoRota
 */
export const PontoRotaSchema = Yup.object().shape({
  titulo: Yup.string().nullable(true),
  tipoFormacaoTitulo: Yup.string(),
  desc: Yup.string().nullable(true),
  tipoFormacaoDesc: Yup.string(),
  ordem: Yup.number(),
  localizacao: LocalizacaoSchema.nullable(true),
  limite: LimiteSchema.nullable(true),
  status: Yup.string().oneOf(Object.keys(STATUS_ENTREGA)),
  _temp: Yup.string().nullable(true),
  anterior: Yup.array().nullable(true),
  usarFilial: Yup.boolean(),
  parceiro: ParceiroSchema.nullable(true),
  filial: FilialSchema.nullable(true)
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Logistica/PontoRota
 * @category Tipos
 * @subcategory PontoRota
 */
export const initialValues = {
  titulo: '',
  tipoFormacaoTitulo: TIPO_FORMACAO.AUTOMATIZADO,
  desc: '',
  tipoFormacaoDesc: TIPO_FORMACAO.AUTOMATIZADO,
  ordem: 0,
  localizacao: localizacaoInitialValues,
  limite: limiteInitialValues,
  status: STATUS_ENTREGA.PENDENTE,
  _temp: '',
  usarFilial: false,
  parceiro: null,
  filial: null
}

// #region CalculoRota
export const getPontoRotaId = (ponto: any) => {
  return ponto ? (ponto._id || ponto._temp)  : null
}
 /**
 * DTO de ponto da rota para o calculo da rota
 * 
 * @name CalculoPontoRota
 * @module types/API/Logistica/PontoRota
 * @category Tipos
 * @subcategory PontoRota
 */
export class CalculoPontoRota extends DTO {
  id?: String
  x?: Number
  y?: Number
  count?: any
  value?: any;
  prev?: Array<Number> = [];
  pos?: Number
  distance?: Number
}

/**
 * Converte a entidade PontoRota para o ponto de rota utilizado em calculo de rota
 * 
 * @param {ponto} PontoRota Ponto a ser convertido
 * @param {result} CalculoPontoRota Ponto a ser preenchido com as informações
 *
 * @returns {CalculoPontoRota}
 */
export function pontoRotaToCalculoPontoRota (
  ponto: PontoRota,
  config: any,
  result = new CalculoPontoRota()
): CalculoPontoRota {
  const { map } = config || {} as any
  if (ponto.localizacao) {
    result.x = ponto.localizacao.lat
    result.y = ponto.localizacao.long
  }
  const id = getPontoRotaId(ponto)
  result.value = {
    _id: ponto._id,
    _temp: ponto._temp,
    ordem: ponto.ordem,
    titulo: ponto.titulo,
    index: map[id]
  }
  // result.prev
  result.pos = ponto.ordem
  result.distance = ponto.distancia
  result.count = limiteToCalculoRotaLimit(ponto.limite)
  result.prev = (ponto.anterior || [])
    .filter( (pto: any) => Boolean(pto))
    .map( (pto: any) => getPontoRotaId(pto))
  result.id = id

  return result
}

/**
 * Converte o ponto de rota utilizado em calculo de rota na entidade PontoRota para
 * 
 * @param {ponto} CalculoPontoRota a ser convertido
 * @param {result} PontoRota Ponto a ser preenchido com as informações
 *
 * @returns {PontoRota}
 */
export function calculoRotaPontoToPontoRota (
  ponto: CalculoPontoRota,
  result = new PontoRota()
  ): PontoRota {
  // result.titulo
  // result.desc
  result.ordem = ponto.pos
  result.localizacao = result.localizacao || new Localizacao()
  result.localizacao.lat = ponto.x
  result.localizacao.long = ponto.y
  result.distancia = ponto.distance
  // result.limite = ponto.count
  // result.status
  return result
}

// #endregion CalculoRota
