import _ from 'lodash'
import * as Yup from 'yup'
import { DTO } from '../../DTO'
import { ContatoParceiro, ContatoParceiroSchema } from '../Parceiro/ContatoParceiro'
import { Parceiro, ParceiroSchema } from '../Parceiro/Parceiro'

 /**
 * DTO
 * 
 * @name ParceiroEntrega
 * @module types/API/Logistica/ParceiroEntrega
 * @category Tipos
 * @subcategory ParceiroEntrega
 */
export class ParceiroEntrega extends DTO {
  parceiro?: Parceiro
  contato?: ContatoParceiro
  
  fromJson (json: any) {
    if (!json) { return this }
    super.fromJson(json)
    this.parceiro = json.parceiro
      ? new Parceiro().fromJson(json.parceiro)
      : undefined
    this.contato = json.contato
      ? new ContatoParceiro().fromJson(json.contato)
      : undefined
    return this
  }

  toJson () {
    return JSON.stringify(this)
  }

  toDTO() {
    const obj = _.clone(this) as any
    obj.parceiro = (this.parceiro
      ? this.parceiro._id || this.parceiro
      : undefined)
    obj.contato = (this.contato
      ? new ContatoParceiro().fromJson(this.contato).toDTO()
      : undefined)
      
    /*
    obj.contato = (this.contato
      ? this.contato._id || this.contato
      : undefined)
    */
    return obj
  }
}

 /**
 * Esquema para formulário
 * 
 * @name ParceiroEntregaSchema
 * @module types/API/Logistica/ParceiroEntrega
 * @category Tipos
 * @subcategory ParceiroEntrega
 */
export const ParceiroEntregaSchema = Yup.object().shape({
  parceiro: ParceiroSchema.nullable(true),
  contato: ContatoParceiroSchema.nullable(true)
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Logistica/ParceiroEntrega
 * @category Tipos
 * @subcategory ParceiroEntrega
 */
export const initialValues = {
  parceiro: null,
  contato: null
}
