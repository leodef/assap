import _ from 'lodash'
import * as Yup from 'yup'
import { DTO } from '../../DTO'
import { Veiculo, VeiculoSchema } from '../Veiculo'


 /**
 * DTO
 * 
 * @name Transporte
 * @module types/API/Logistica/Transporte
 * @category Tipos
 * @subcategory Transporte
 */
export class Transporte extends DTO {
  veiculo?: Veiculo
  carreta?: Veiculo

  fromJson (json: any) {
    if (!json) { return this }
    super.fromJson(json)
    this.veiculo = json.veiculo
      ? new Veiculo().fromJson(json.veiculo)
      : null
    this.carreta = json.carreta
      ? new Veiculo().fromJson(json.carreta)
      : null
    return this
  }

  toJson () {
    return JSON.stringify(this)
  }

  toDTO() {
    const obj = _.clone(this) as any
    obj.veiculo = (this.veiculo
      ? this.veiculo._id || this.veiculo
      : undefined)
    obj.carreta = (this.carreta
      ? this.carreta._id || this.carreta
      : undefined)
    return obj
  }
}

 /**
 * Esquema para formulário
 * 
 * @name TransporteSchema
 * @module types/API/Logistica/Transporte
 * @category Tipos
 * @subcategory Transporte
 */
export const TransporteSchema = Yup.object().shape({
  veiculo: VeiculoSchema.nullable(true),
  carreta: VeiculoSchema.nullable(true)
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Logistica/Transporte
 * @category Tipos
 * @subcategory Transporte
 */
export const initialValues = {
  veiculo: null,
  carreta: null
}
