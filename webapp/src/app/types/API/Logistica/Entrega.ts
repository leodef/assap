import * as Yup from 'yup'
import moment from 'moment'
import { CrudType } from '../../Crud'
import {
  TIPO_FORMACAO
} from '../../../enums/tipo-formacao'
import {
  STATUS_ENTREGA
} from '../../../enums/status-entrega'
import {
  DTO
} from '../../DTO'
import {
  TIPO_COMPROMISSO
} from '../../../enums/tipo-compromisso'
import {
  // eslint-disable-next-line no-unused-vars
  Rota,
  initialValues as rotaInitialValues,
  RotaSchema
} from './Rota'
import {
  ParceiroEntrega,
  initialValues as parceiroEntregaInitialValues,
  ParceiroEntregaSchema
} from './ParceiroEntrega'
import {
  FuncionarioEntrega,
  FuncionarioEntregaSchema
} from './FuncionarioEntrega'
import {
  Transporte,
  initialValues as transporteInitialValues,
  TransporteSchema
} from './Transporte'
import {
  Compromisso,
  initialValues as compromissoInitialValues,
  CompromissoSchema
} from '../Calendario/Compromisso'
import {
  Produto,
  ProdutoSchema
} from '../Produto'
import _ from 'lodash'
const cleanLabel = DTO.cleanLabel

 /**
 * Gera o titulo do item
 * 
 * @name generateTitulo
 * @module types/API/Logistica/Entrega
 * @category Tipos
 * @subcategory Entrega
 */

export const generateTitulo = (obj: any) => {
  if(!obj) {
    return ''
  }
  const { parceiroEntrega, transporte, produto } = obj
  let result = [
    (
      produto && produto.titulo
        ? produto.titulo
        : null
    ),
    (
      parceiroEntrega && parceiroEntrega.parceiro && parceiroEntrega.parceiro.titulo
        ? parceiroEntrega.parceiro.titulo
        : null
      ),
    (
      obj.data ? moment(obj.data).format('DD/MM/YYYY') : null
    ),
    (
      transporte && transporte.veiculo
        ? transporte.veiculo.placa
        : null
    )
  ].filter((val) => Boolean(val)).join(' ')
  return cleanLabel(result)
}



 /**
 * Gera a descrição do item
 * @module types/API/Logistica/Entrega
 * @category Tipos
 * @subcategory Entrega
 */
export const generateDesc = (obj: any) => {
  if(!obj) {
    return ''
  }
  const {
    rota,
    parceiroEntrega,
    transporte,
    funcionarios,
    compromisso
  } = obj;

  // pontoRota
  const pontoRotaDesc = (ponto: any, index: number) => {
    const parentSpace = '    '
    const space = '      '
    const comPontoAnterior = Boolean(ponto.distanciaDesc || ponto.distancia)
    return `${
      parentSpace + 'Parada Rota '+ ponto.ordem + ':\n'
    }${
      ponto.localizacao &&  ponto.localizacao.titulo
        ? space + 'Localização: ' + ponto.localizacao.titulo + '\n'
        : ''
    }${
      comPontoAnterior 
        ? space + 'Trajeto da parada anterior até a atual:\n'
        : ''
    }${
      comPontoAnterior
        ? space + 'Distância: ' + ponto.distanciaDesc || ponto.distancia + '\n'
        : ''
    }${
      comPontoAnterior && (ponto.duracaoDesc || ponto.duracao)
        ? space + 'Duração: ' + ponto.duracaoDesc || ponto.duracao + '\n'
        : ''
    }${
      comPontoAnterior && (ponto.duracaoComTrafegoDesc || ponto.duracaoComTrafego)
        ? space + 'Duração com Tráfego: ' + ponto.duracaoComTrafegoDesc || ponto.duracaoComTrafego + '\n'
        : ''
    }${
      ponto.anterior
        ? space + 'Anterior: ' + ponto.anterior.map( (ponto: any) => ponto.titulo).join(', ') + '\n'
        : ''
    }${
      ponto.status
        ? space + 'Status: ' + ponto.status + '\n'
        : ''
    }${
        ponto._id || ponto._temp
        ? space + 'ID: ' + (ponto._id || ponto._temp) + '\n'
        : ''
    }
    `
  }

  // data
  const dataDesc = obj.data? 'Data: '+moment(obj.data).format('DD/MM/YYYY') + '\n' : ''

  // rotaLimite
  const rotaLimiteSpace = '      '
  const rotaLimiteDesc = rota && rota.limite
    ? `${
        rota.limite.peso
          ? rotaLimiteSpace + 'Peso: ' + rota.limite.peso
          : ''
      }\n${
        rota.limite.volume
          ? rotaLimiteSpace + 'Volume: : ' + rota.limite.volume
          : ''
      }\n${
        rota.limite.tara
          ? rotaLimiteSpace + 'Peso de veículo: ' + rota.limite.tara
          : ''
      }\n`
    : ''

  // rota
  const rotaSpace = '    '
  const rotaPontoSpace = '      '
  const rotaComLimite = Boolean(rota.limite)
  const rotaDesc = rota
    ? `${
      'Rota:\n' 
    }${
        rota.algoritimo
          ? rotaSpace + 'Algoritimo: ' + rota.algoritimo + '\n'
          : ''
      }${
        rota.distancia
          ? rotaSpace + 'Distância: ' + rota.distancia + '\n'
          :  ''
      }${
        rota.duracao
          ? rotaSpace + 'Duração: ' + rota.duracao + '\n'
          : ''
      }${
        rota.duracaoComTrafego
          ? rotaSpace + 'Duração com Tráfego: ' + rota.duracaoComTrafego + '\n'
          : ''
      }${
        rota.calculo
            ? rotaSpace + 'Cálculo da rota realizado\n'
            : rotaSpace + 'Cálculo da rota não realizado\n'
      }${
        rota.pontos ? rotaSpace + 'Pontos: \n'
          + rotaPontoSpace
          +  '----\n'
          + (rota.pontos || [])
          .map(pontoRotaDesc)
          .join(rotaPontoSpace + '----\n') : ''
      }${
        rotaComLimite
          ? rotaSpace + 'Limite:\n'
          : ''
      }${
        rotaComLimite && rota.tipo
          ? rotaLimiteSpace + 'Tipo: ' + rota.tipo + '\n'
          : ''
      }${
        rotaComLimite
          ? rotaLimiteDesc + '\n'
          : ''
      }`
    : ''
  
  const parceiroEntregaSpace = '    '
  const parceiroEntregaDesc = parceiroEntrega
    ? `${
        'Parceiro comercial envolvido na entrega:\n'
      }${
        parceiroEntrega && parceiroEntrega.parceiro && parceiroEntrega.parceiro.titulo
          ? parceiroEntregaSpace + 'Parceiro: ' + parceiroEntrega.parceiro.titulo + '\n'
          : ''
      }${
        parceiroEntrega && parceiroEntrega.contato && parceiroEntrega.contato.titulo
          ? parceiroEntregaSpace + 'Contato: ' + parceiroEntrega.contato.titulo + '\n'
          : ''
      }`
    : ''

  const transporteSpace = '    '
  const transporteDesc = transporte
    ? `${
        'Transporte:\n'
      }${
        transporte.veiculo && transporte.veiculo.titulo
          ? transporteSpace + 'Veículo: ' + transporte.veiculo.titulo + '\n'
          : ''
      }${
        transporte.carreta && transporte.carreta.titulo
          ? transporteSpace + 'Carreta: ' + transporte.carreta.titulo + '\n'
          : ''
      }`
    : ''
  
  const funcionariosSpace = '    '
  const funcionariosDesc = funcionarios
    ? `${
        'Funcionários:\n'
      }${
        funcionarios.map(
          (func: any) => `${funcionariosSpace}${func.tipo}: ${func.funcionario
            ? func.funcionario.nome
            : ''}`).join(', ') + '\n'
      }` : ''
  const compromissoDesc = compromisso && compromisso.titulo
    ? `${
      'Evento no calendário:\n'
    }${
      compromisso.titulo
        ? 'Compromisso: ' + compromisso.titulo + '\n'
        : ''}
  ` : ''
    
  const result = `${
      'Entrega:\n'
    }${
      obj.status
        ? 'Status: ' + obj.status + '\n'
        : ''
    }${
      obj.produto
        ? 'Produto: ' + (obj.produto.titulo || obj.produto) + '\n'
        : ''
    }${
      dataDesc
    }${
      rotaDesc
    }${
      parceiroEntregaDesc
    }${
      transporteDesc
    }${
      funcionariosDesc
    }${
      compromissoDesc
    }`
  return cleanLabel(result)
}

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name EntregaCrudType
 * @module types/API/Calendario/Compromisso
 * @category Tipos
 * @subcategory Compromisso
 */
export class EntregaCrudType extends CrudType {
  CALCULO_ROTA = 'CALCULO_ROTA_ENTREGA'
  CALCULO_ROTA_PENDING = 'CALCULO_ROTA_ENTREGA_PENDING'
  CALCULO_ROTA_SUCCESS = 'CALCULO_ROTA_ENTREGA_SUCCESS'
  CALCULO_ROTA_FAILURE = 'CALCULO_ROTA_ENTREGA_FAILURE'

  constructor () {
    super('ENTREGA')
  }
}

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name EntregaCrudType
 * @module types/API/Logistica/EntregaCrudType
 * @category Tipos
 * @subcategory Entrega
 */
export const crudType = new EntregaCrudType()

 /**
 * DTO
 * 
 * @name Entrega
 * @module types/API/Logistica/Entrega
 * @category Tipos
 * @subcategory Entrega
 */
export class Entrega extends DTO {
    titulo?: String
    tipoFormacaoTitulo?: String
    desc?: String
    tipoFormacaoDesc?: String
    status?: String
    data?: Date
    produto?: Produto
    rota?: Rota
    parceiroEntrega?: ParceiroEntrega
    transporte?: Transporte
    funcionarios?: Array<FuncionarioEntrega> = []
    compromisso?: Compromisso
    criarCompromisso?: Boolean

    static generateTitulo(obj: any): string {
      return generateTitulo(obj)
    }

    static generateDesc(obj: any): string {
      return generateDesc(obj)
    }

    generateTitulo(): string {
      return generateTitulo(this)
    }

    generateDesc(): string {
      return generateDesc(this)
    }
  
    fromJson (json: any) {
      if (!json) { return this }
      super.fromJson(json)
      this.data = json.data
        ? new Date(json.data)
        : undefined
      this.produto = json.produto
        ? new Produto().fromJson(json.produto)
        : undefined
      this.rota = json.rota
        ? new Rota().fromJson(json.rota)
        : undefined
      this.parceiroEntrega = json.parceiroEntrega
        ? new ParceiroEntrega().fromJson(json.parceiroEntrega)
        : undefined
      this.transporte = json.transporte
        ? new Transporte().fromJson(json.transporte)
        : undefined
      this.funcionarios = (json.funcionarios || []).map(
        (func: any) =>  new FuncionarioEntrega().fromJson(func)
      )
      this.compromisso = json.compromisso
        ? new Compromisso().fromJson(
          _.defaults(json.compromisso, compromissoEntregaInitialValues)
        )
        : null
      return this
    }

    toJson () {
      return JSON.stringify(this)
    }

    toDTO() {
      const obj = _.clone(this) as any
      obj.produto = this.produto
        ? this.produto._id || this.produto
        : null
      obj.parceiroEntrega = this.parceiroEntrega
        ? new ParceiroEntrega().fromJson(this.parceiroEntrega).toDTO()
        : null
      obj.rota = this.rota
        ? new Rota().fromJson(this.rota).toDTO()
        : null
      obj.transporte =  this.transporte
        ? new Transporte().fromJson(this.transporte).toDTO()
        : null
      obj.funcionarios = (this.funcionarios || []).map(
        (val: any) => new FuncionarioEntrega().fromJson(val).toDTO()
      )
      obj.compromisso = this.criarCompromisso && this.compromisso
        ? new Compromisso().fromJson(
          _.defaults(this.compromisso, compromissoEntregaInitialValues)
        ).toDTO()
        : null
      return obj
    }
}

 /**
 * Esquema para formulário
 * 
 * @name EntregaSchema
 * @module types/API/Logistica/Entrega
 * @category Tipos
 * @subcategory Entrega
 */
export const EntregaSchema = Yup.object().shape({
  titulo: Yup.string().nullable(true),
  tipoFormacaoTitulo: Yup.string().nullable(true),
  desc: Yup.string().nullable(true),
  tipoFormacaoDesc: Yup.string().nullable(true),
  data: Yup.date(),
  produto: ProdutoSchema.nullable(true),
  rota: RotaSchema.nullable(true),
  status: Yup.string().oneOf(Object.keys(STATUS_ENTREGA)),
  parceiroEntrega: ParceiroEntregaSchema.nullable(true),
  transporte: TransporteSchema.nullable(true),
  funcionarios:  Yup.array().of(
    FuncionarioEntregaSchema.nullable(true)
  ).nullable(true),
  compromisso: CompromissoSchema.nullable(true),
  criarCompromisso: Yup.boolean()
})

export const compromissoEntregaInitialValues = {
  ...compromissoInitialValues,
  tipo: TIPO_COMPROMISSO.entrega,
}
 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Logistica/Entrega
 * @category Tipos
 * @subcategory Entrega
 */
export const initialValues = {
  titulo: '',
  tipoFormacaoTitulo: TIPO_FORMACAO.AUTOMATIZADO,
  desc: '',
  tipoFormacaoDesc: TIPO_FORMACAO.AUTOMATIZADO,
  data: new Date(),
  produto: null,
  rota: rotaInitialValues,
  status: STATUS_ENTREGA.PENDENTE,
  parceiroEntrega: parceiroEntregaInitialValues,
  transporte: transporteInitialValues,
  funcionarios: [],
  compromisso: compromissoEntregaInitialValues,
  criarCompromisso: false
}
