import * as Yup from 'yup'
// eslint-disable-next-line no-unused-vars
import {
  // eslint-disable-next-line no-unused-vars
  PontoRota,
  pontoRotaToCalculoPontoRota,
  calculoRotaPontoToPontoRota,
  // eslint-disable-next-line no-unused-vars
  CalculoPontoRota
} from './PontoRota'
import { ALGORITIMO_ROTA } from '../../../enums/algoritimo-rota'
import { DTO } from '../../DTO'
import {
  // eslint-disable-next-line no-unused-vars
  Limite,
  initialValues as limiteInitialValues,
  LimiteSchema, limiteToCalculoRotaLimit
} from '../Limite'
import { TIPO_LIMITE_ROTA } from '../../../enums/tipo-limite-rota'
import _ from 'lodash'
// eslint-disable-next-line no-unused-vars

 /**
 * DTO
 * 
 * @name Rota
 * @module types/API/Logistica/Rota
 * @category Tipos
 * @subcategory Rota
 */
export class Rota extends DTO {
  algoritimo?: String;
  calculo?: Boolean = false;
  distancia?: Number;
  duracao?: Number;
  duracaoComTrafego?: Number;
  limite?: Limite;
  pontos?: Array<PontoRota> = [];

  fromJson (json: any) {
    if (!json) { return this }
    super.fromJson(json)
    this.limite = json.limite
      ? new Limite().fromJson(json.limite)
      : null
    this.pontos = (json.pontos || []).map(
      (pto: any) => new PontoRota().fromJson(pto))
    return this
  }

  toJson () {
    return JSON.stringify(this)
  }
    
  toDTO() {
    const obj = _.clone(this) as any
    obj.contato = this.limite
      ? new Limite().fromJson(this.limite).toDTO()
      : null
    obj.pontos = (this.pontos || []).map(
      (pto) => new PontoRota().fromJson(pto).toDTO()
    )
    return obj
  }
}

 /**
 * Esquema para formulário
 * 
 * @name RotaSchema
 * @module types/API/Logistica/Rota
 * @category Tipos
 * @subcategory Rota
 */
export const RotaSchema = Yup.object().shape({
  algoritimo: Yup.string().oneOf(Object.keys(ALGORITIMO_ROTA)),
  calculo: Yup.boolean(),
  pontos: Yup.array(),
  limite: LimiteSchema.nullable(true),
  tipoLimite: Yup.string().oneOf(Object.keys(TIPO_LIMITE_ROTA)),
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Logistica/Rota
 * @category Tipos
 * @subcategory Rota
 */
export const initialValues = {
  algoritimo: 'naive',
  calculo: false,
  pontos: [],
  limite: limiteInitialValues,
  tipoLimite: TIPO_LIMITE_ROTA.INDEFINIDO
}

// #region CalculoRota

/**
 * Converte a entidade Rota para a rota utilizado em calculo de rota
 * 
 * @param {Rota} rota Rota a ser convertida
 * @param {{ elements: Array<any> }} rows Retorno da calculo de distancia da Api do google
 * @param {CalculoRotaRequest} result Rota a ser preenchida com as informações
 *
 * @returns {CalculoRotaRequest}
 */
export function rotaToCalculoRotaRequest (
  rota: Rota,
  config?: any,
  result = new CalculoRotaRequest()
): CalculoRotaRequest {
  if(!rota){ return result }
  const { rows } = config || {} as any
  result.config = rotaToCalculoRotaRequestConfig(rota)
  result.points = (rota.pontos || []).map((ponto, index) => pontoRotaToCalculoPontoRota(ponto, config))
  result.distanceMatrix = rows ? rowsToDistanceMatrix(rows) : undefined
  return result
}


/**
 * Converte a entidade Rota para a rota utilizado em calculo de rota
 * 
 * @param {Rota} rota Rota a ser convertida
 * @param {{ elements: Array<any> }} rows Retorno da calculo de distancia da Api do google
 * @param {CalculoRotaRequest} result Rota a ser preenchida com as informações
 *
 * @returns {CalculoRotaRequest}
 */
export function rotaToCalculoRotaRequestConfig (
  rota: Rota,
  result = new CalculoRotaRequestConfig()
): CalculoRotaRequestConfig {
  if(!rota){ return result }
  result.algorithm = rota.algoritimo
  if(rota.limite) {
    result.limit = limiteToCalculoRotaLimit(rota.limite)
  }
  return result
}

/**
 * Converte o retorno da calculo de distancia da Api do google, para 
 *    a matriz de distancias utilizada em calculo
 * 
 * @param {{ elements: Array<any> }} rows Retorno da calculo de distancia da Api do google
 * @param {Array<any>} result Matriz a ser preenchida com as informações
 *
 * @returns {Array<Array<Number>>}
 */
export function rowsToDistanceMatrix (
  rows?: Array<{ elements: Array<any> }>,
  result = [] as Array<any>
): Array<Array<Number>> | undefined {
  if (!rows) { return }
  for (const rowIndex in rows) {
    result[rowIndex] = []
    const row = rows[rowIndex]
    if(!row || !row.elements) {
      continue; 
    }
    const elements = row.elements
    for (const elementIndex in elements) {
      const element = elements[elementIndex]
      result[rowIndex][elementIndex] = (rowIndex !== elementIndex)
        ? Number(element.distance.value)
        : null
    }
  }
  return result
  /*
    rows: 0: elements: 0:
      distance: {text: "1 m", value: 0}
      duration: {text: "1 min", value: 0}
      duration_in_traffic: {text: "1 min", value: 30}
      status: "OK"
  */
}

/**
 * Converte a rota retornada do calculo de rota na entidade Rota para
 * 
 * @param {CalculoRotaResponse} response Resposta a ser convertida em entidade
 * @param {{ elements: Array<any> }} rows Retorno da calculo de distancia da Api do google
 * @param {Rota} result Rota a ser preenchida com as informações
 *
 * @returns {Rota}
 */
export function calculoRotaResponseToRota (
  response: CalculoRotaResponse,
  config?: any,
  result = new Rota()
): Rota {
  result.algoritimo = response.algorithm
  // result.limite = response.limit
  result.distancia = response.distance
  result.calculo = true
  result.pontos = (
    response.path ||
    response.points ||
    []).map((ponto) => calculoRotaPontoToPontoRota(
      ponto,
      result.pontos
        ? result.pontos.find(
            (pontoRota) => 
            pontoRota._id === ponto.id ||
            pontoRota._temp === ponto.id ||
            PontoRota.compare(pontoRota, ponto.value)) || undefined
        : undefined
    )
  )
  result = loadRows(result, config)
  result.distancia = (result.pontos || []).reduce(
    (prev, curr) => (
      Number(prev || 0) +
      Number(curr.distancia || 0)
    ), 0)
  result.duracao = (result.pontos || []).reduce(
    (prev, curr) => (
      Number(prev || 0) +
      Number(curr.duracao || 0)
    ), 0)
  result.duracaoComTrafego = (result.pontos || []).reduce(
    (prev, curr) => (
      Number(prev || 0) +
      Number(curr.duracaoComTrafego || 0)
    ), 0)
  return result
}

/**
 * Carrega o retorno do calculo de distancias da Api do Goolge na
 *   entidade entidade Rota
 * 
 * @param {Rota} result Rota a ser preenchida com as informações
 * @param {{ elements: Array<any> }} rows Retorno da calculo de distancia da Api do google
 *
 * @returns {Rota}
 */
export function loadRows (
  result = new Rota(),
  config?: any
): Rota {
  const {rows, map} = config || {} as any
  if (!result || !result.pontos) {
    return result
  }
  if(!rows || rows.length === 0) {
    /*
    result.pontos 
    result.pontos.map( (ponto: any) => ({
      ponto.distancia = 
      ponto.duracao
      ponto.duracaoComTrafego
      ponto.distanciaDesc
      ponto.duracaoDesc
      ponto.duracaoComTrafegoDesc
    }))
    */
    return result
  }
  result.pontos = result.pontos.map((ponto: PontoRota, i: number) => {
    const key = PontoRota.getPontoRotaId(ponto)
    const index = map ? map[key] : i - 1
    if (index === 0) {
      return ponto
    }
    const element = rows[index -1].elements[index]
    if (!element) {
      return ponto
    }

    ponto.distancia = (element && element.distance)
      ? element.distance.value
      : 0
    ponto.duracao = (element && element.duration)
      ? element.duration.value
      : 0
    ponto.duracaoComTrafego = (element && element.duration_in_traffic)
      ? element.duration_in_traffic.value
      : 0
    ponto.distanciaDesc = (element && element.distance)
      ? element.distance.text
      : undefined
    ponto.duracaoDesc = (element && element.duration)
      ? element.duration.text
      : undefined
    ponto.duracaoComTrafegoDesc = (element && element.duration_in_traffic)
      ? element.duration_in_traffic.text
      : undefined
    return ponto
  })

  return result
}


 /**
 * DTO configuração do calculo de rotas
 * 
 * @name CalculoRotaRequestConfig
 * @module types/API/Logistica/Rota
 * @category Tipos
 * @subcategory Rota
 */
export class CalculoRotaRequestConfig {
  algorithm?: String
  limit?: any
}

 /**
 * DTO requisição do calculo de rotas
 * 
 * @name CalculoRotaRequestConfig
 * @module types/API/Logistica/Rota
 * @category Tipos
 * @subcategory CalculoRotaRequest
 */
export class CalculoRotaRequest extends DTO {
  config: CalculoRotaRequestConfig = new CalculoRotaRequestConfig()
  points?: Array<CalculoPontoRota> = [];
  distanceMatrix?: Array<Array<Number>>;
}

 /**
 * DTO resposta do calculo de rotas
 * 
 * @name CalculoRotaResponse
 * @module types/API/Logistica/Rota
 * @category Tipos
 * @subcategory CalculoRotaRequest
 */
export class CalculoRotaResponse extends DTO {
  distance?: Number
  path?: Array<CalculoPontoRota> = [];
  count?: any;
  algorithm?: String
  limit?: Number
  points?: Array<CalculoPontoRota> = [];
  distancePath?: Array<Number> = [];
}

// #endregion CalculoRota
