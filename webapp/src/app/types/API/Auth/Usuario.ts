import * as Yup from "yup";
import { TIPO_USUARIO } from "../../../enums/tipo-usuario";
import { CrudType } from "../../Crud";
import { DTO } from "../../DTO";

/**
 * Constantes para gerenciamento de entidades
 *
 * @name crudType
 * @module types/API/Auth/Usuario
 * @category Tipos
 * @subcategory Usuario
 */
export const crudType = new CrudType("USUARIO");


/**
 * DTO
 *
 * @name Usuario
 * @module types/API/Auth/Usuario
 * @category Tipos
 * @subcategory Usuario
 */
export class Usuario extends DTO {
  usuario?: String;
  senha?: String;
  nome?: String;
  tipo?: String; 
  status?: String;
  email?: String;
}

/**
 * Esquema para formulário
 *
 * @name UsuarioSchema
 * @module types/API/Auth/Usuario
 * @category Tipos
 * @subcategory Usuario
 */

export const UsuarioSchema = Yup.object().shape({
  usuario: Yup.string(),
  senha: Yup.string(),
  nome: Yup.string(),
  tipo: Yup.string().oneOf(Object.keys(TIPO_USUARIO)),
  status: Yup.string(),
  email: Yup.string()
});


/**
 * Valores iniciais para formulário
 *
 * @name initialValues
 * @module types/API/Auth/Usuario
 * @category Tipos
 * @subcategory Usuario
 */
export const initialValues = {
  usuario: "",
  senha: "",
  nome: "",
  tipo: TIPO_USUARIO.PUBLICO,
  status: "",
  email: ""
};
