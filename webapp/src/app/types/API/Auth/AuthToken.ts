import * as Yup from "yup";
import { CrudType } from "../../Crud";
import { DTO } from "../../DTO";
import { Usuario } from "./Usuario";

/**
 * Constantes para gerenciamento de entidades
 *
 * @name crudType
 * @module types/API/Auth/AuthToken
 * @category Tipos
 * @subcategory AuthToken
 */
export const crudType = new CrudType("AUTH_TOKEN");

/**
 * DTO
 *
 * @name AuthToken
 * @module types/API/Auth/AuthToken
 * @category Tipos
 * @subcategory AuthToken
 */
export class AuthToken extends DTO {
  validade?: Date;
  usuario?: Usuario;
}

/**
 * Esquema para formulário
 *
 * @name AuthTokenSchema
 * @module types/API/Auth/AuthToken
 * @category Tipos
 * @subcategory AuthToken
 */
export const AuthTokenSchema = Yup.object().shape({
  validade: Yup.string(),
  usuario: Yup.string(),
});

/**
 * Valores iniciais para formulário
 *
 * @name initialValues
 * @module types/API/Auth/AuthToken
 * @category Tipos
 * @subcategory AuthToken
 */
export const initialValues = {
  validade: new Date(),
  usuario: "",
};
