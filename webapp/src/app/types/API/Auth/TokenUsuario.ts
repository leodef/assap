import * as Yup from "yup";
import { TIPO_TOKEN_USUARIO } from "../../../enums/tipo-token-usuario";
import { CrudType } from "../../Crud";
import { DTO } from "../../DTO";
import { Usuario, UsuarioSchema } from "./Usuario";

/**
 * Constantes para gerenciamento de entidades
 *
 * @name crudType
 * @module types/API/Auth/TokenUsuario
 * @category Tipos
 * @subcategory TokenUsuario
 */
export const crudType = new CrudType("TOKEN_USUARIO");

/**
 * DTO
 *
 * @name TokenUsuario
 * @module types/API/Auth/TokenUsuario
 * @category Tipos
 * @subcategory TokenUsuario
 */
export class TokenUsuario extends DTO {
  validade?: Date;
  tipo?: String;
  usuario?: Usuario;
}

/**
 * Esquema para formulário
 *
 * @name TokenUsuarioSchema
 * @module types/API/Auth/TokenUsuario
 * @category Tipos
 * @subcategory TokenUsuario
 */
export const TokenUsuarioSchema = Yup.object().shape({
  validade: Yup.date(),
  tipo: Yup.string().oneOf(Object.keys(TIPO_TOKEN_USUARIO)),
  usuario: UsuarioSchema.nullable(true),
});

/**
 * Valores iniciais para formulário
 *
 * @name initialValues
 * @module types/API/Auth/TokenUsuario
 * @category Tipos
 * @subcategory TokenUsuario
 */
export const initialValues = {
  validade: new Date(),
  tipo: TIPO_TOKEN_USUARIO.CONFIRMAR_CONTA,
  usuario: null,
};
