import {
  AppointmentModel,
  SchedulerDateTime,
} from "@devexpress/dx-react-scheduler";
import _ from "lodash";
import * as Yup from "yup";
import {
  TIPO_COMPROMISSO,
  TIPO_COMPROMISSO_LABEL,
} from "../../../enums/tipo-compromisso";
import { CrudType } from "../../Crud";
import { DTO } from "../../DTO";

/**
 * Constantes para gerenciamento de entidades
 *
 * @name crudType
 * @module types/API/Calendario/Compromisso
 * @category Tipos
 * @subcategory Compromisso
 */
export const crudType = new CrudType("COMPROMISSO");

/**
 * DTO
 *
 * @name Compromisso
 * @module types/API/Calendario/Compromisso
 * @category Tipos
 * @subcategory Compromisso
 */
export class Compromisso extends DTO {
  //_id: Schema.Types.ObjectId,
  titulo?: String;
  desc?: String;
  inicio?: Date;
  fim?: Date;
  recursos?: Object;
  diaInteiro: Boolean = false;
  tipo?: String;

  toDTO() {
    const obj = _.clone(this) as any;
    return obj;
  }
}

/**
 * Esquema para formulário
 *
 * @name CompromissoSchema
 * @module types/API/Calendario/Compromisso
 * @category Tipos
 * @subcategory Compromisso
 */
export const CompromissoSchema = Yup.object().shape({
  titulo: Yup.string().nullable(true),
  tipoFormacaoTitulo: Yup.string().nullable(true),
  desc: Yup.string().nullable(true),
  tipoFormacaoDesc: Yup.string().nullable(true),
  inicio: Yup.date(),
  fim: Yup.date().nullable(true),
  recursos: Yup.object().nullable(true),
  tipo: Yup.string().oneOf(Object.keys(TIPO_COMPROMISSO)),
});

/**
 * Valores iniciais para formulário
 *
 * @name initialValues
 * @module types/API/Calendario/Compromisso
 * @category Tipos
 * @subcategory Compromisso
 */
export const initialValues = {
  titulo: "",
  desc: "",
  inicio: new Date(),
  fim: new Date(),
  recursos: null,
  tipo: TIPO_COMPROMISSO.geral,
};

//#region Scheduler

/**
 * Tipo de recursos principal, para a separação de compromissos
 *   para ser exibido no calendário
 *
 */
export const mainResourceName = "type";

/**
 * Tipos de recursos disponiveis, para a separação de compromissos
 *   para ser exibido no calendário
 *
 * @todo Com novos recursos cadastraveis, consultar de um serviço
 */
export const resources = [
  {
    fieldName: "type",
    title: "Tipo",
    instances: [
      { id: TIPO_COMPROMISSO.geral, text: TIPO_COMPROMISSO_LABEL.geral },
      { id: TIPO_COMPROMISSO.entrega, text: TIPO_COMPROMISSO_LABEL.entrega },
    ],
  },
];

export const getResources = (values: any) => {
  const { veiculos, funcionarios, entregas } =
    values ||
    ({
      veiculos: [],
      funcionarios: [],
      entregas: [],
    } as any);
  return [
    ...resources,
    {
      fieldName: "veiculos",
      title: "Veículo",
      allowMultiple: true,
      instances: (veiculos || []).map((obj: any) => ({
        id: obj._id,
        text: obj.titulo,
      })),
    },
    {
      fieldName: "funcionarios",
      title: "Funcionários",
      allowMultiple: true,
      instances: (funcionarios || []).map((obj: any) => ({
        id: obj._id,
        text: obj.nome,
      })),
    },
    {
      fieldName: "entrega",
      title: "Entrega",
      instances: (entregas || []).map((obj: any) => ({
        id: obj._id,
        text: obj.titulo,
      })),
    },
  ];
};
/**
 * Compromisso para ser exibido no calendário
 *
 * @name SchedulerItem
 * @module types/API/Calendario/SchedulerItem
 * @category Tipos
 * @subcategory Compromisso
 */
export class SchedulerItem extends DTO implements AppointmentModel {
  [propertyName: string]: any;
  startDate: SchedulerDateTime = Date();
  endDate: SchedulerDateTime = Date();
  title?: string | undefined;
  // https://docs.devexpress.com/CoreLibraries/DevExpress.XtraScheduler.Appointment.AllDay
  allDay?: boolean | undefined;
  id?: string | number | undefined;
  rRule?: string | undefined;
  exDate?: string | undefined;
  //_id: Schema.Types.ObjectId,
  desc?: String;
  resources: any;
  type?: String;
}

/**
 * Converte a entidade compromisso para o compromisso a ser exibido no calendário
 *
 * @param {Compromisso | Array<Compromisso>} compromissos Compromisso(s) a ser convertido
 *
 * @returns {SchedulerItem | Array<SchedulerItem>}
 */
export const compromissoToSchedulerItem = (
  compromissos: Compromisso | Array<Compromisso>
): AppointmentModel | Array<AppointmentModel> => {
  if (Array.isArray(compromissos)) {
    return compromissos.map(
      (compromisso: Compromisso) =>
        compromissoToSchedulerItem(compromisso) as AppointmentModel
    ) as Array<AppointmentModel>;
  }
  const result = new SchedulerItem();
  result.title = String(compromissos.titulo);
  result.desc = compromissos.desc;
  result.startDate = compromissos.inicio as SchedulerDateTime;
  result.endDate = compromissos.fim as SchedulerDateTime;
  result.id = String(compromissos._id);
  const resources = _.cloneDeep(compromissos.recursos);
  result.resources = resources
  result.type = compromissos.tipo;
  result.allDay = Boolean(compromissos.diaInteiro);
  return { ...result, ...resources } as AppointmentModel;
};

//#endregion Scheduler
