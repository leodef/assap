import * as Yup from 'yup'
import { TIPO_FORMACAO } from '../../enums/tipo-formacao'
import { TIPO_FUNCAO_VEICULO } from '../../enums/tipo-funcao-veiculo'
import { CrudType } from '../Crud'
import { DTO } from '../DTO'
import {
  Limite,
  LimiteSchema,
  initialValues as limiteInitialValues
} from './Limite'
const cleanLabel = DTO.cleanLabel

 /**
 * Gera o titulo do item
 * 
 * @name generateTitulo
 * @module types/API/Veiculo
 * @category Tipos
 * @subcategory Veiculo
 */

export const generateTitulo = (obj: any) => {
  if(!obj) {
    return ''
  }
  const result = [
    (obj.placa || null),
    (obj.modelo || null),
    (obj.marca || null),
    (obj.ano || null)
  ].filter(val => Boolean(val)).join(', ')
  return cleanLabel(result)
}

 /**
 * Gera a descrição do item
 * @module types/API/Veiculo
 * @category Tipos
 * @subcategory Veiculo
 */
export const generateDesc = (obj: any) => {
  if(!obj) {
    return ''
  }
  const limitDesc = obj.limite ? [
      'Limite:\n',
      (obj.limite.tara
        ? 'Peso do veículo: ' + obj.limite.tara 
        : null),
      (obj.limite.peso
        ? 'Peso: ' + obj.limite.peso
        : null),
      (obj.limite.volume
        ? 'Volume: ' + obj.limite.volume
        : null)
  ].filter(val => Boolean(val)).join(', ') : ''
  const compatibilidadeDesc = obj.compatibilidade
  ? 'Carretas:\n' + obj.compatibilidade
    .map((c: any) => c ? '    '+c.titulo : null)
    .filter((val: any) => Boolean(val))
    .join('\n') 
  : ''

  return cleanLabel(`${
    'Veiculo:\n'
  }${
    obj.placa ? 'Placa: ' + obj.placa : ''
  } ${
    obj.marca ? 'Marca: ' + obj.marca : ''
  } ${
      obj.modelo ? 'Modelo: ' + obj.modelo : ''
  } ${
    obj.tipo ? 'Tipo: ' + obj.tipo : ''
  } ${
    obj.ano ? 'Ano: ' + obj.ano : ''
  }\n${
    obj.autonomo
      ? 'Veículo é autônomo para carga'
      : 'Veículo não é autônomo para carga'
  }\n${
    obj.trator 
      ? 'Veículo tem tração própria'
      : 'Veículo não tem tração própria'
  }\n${
    limitDesc
  }\n${
    compatibilidadeDesc
  }`)
}



 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/Veiculo
 * @category Tipos
 * @subcategory Veiculo
 */
export const crudType = new CrudType('VEICULO')

 /**
 * DTO
 * 
 * @name Veiculo
 * @module types/API/Veiculo
 * @category Tipos
 * @subcategory Veiculo
 */
export class Veiculo extends DTO {
  titulo?: String
  tipoFormacaoTitulo?: String
  desc?: String
  tipoFormacaoDesc?: String
  placa?: String
  marca?: String
  modelo?: String
  tipo?: String
  ano?: Number
  limite?: Limite
  autonomo?: Boolean // Capaz de transportar carga sozinho
  trator?: Boolean // Com tração própria
  // Veiculos que pode carregar
  compatibilidade?: Array<String>
  funcao?: String

  static generateTitulo(obj: any) {
    return generateTitulo(obj)
  }

  static generateDesc(obj: any) {
    return generateDesc(obj)
  }

  generateTitulo(): string {
    return generateTitulo(this)
  }

  generateDesc(): string {
    return generateDesc(this)
  }

}

 /**
 * Esquema para formulário
 * 
 * @name VeiculoSchema
 * @module types/API/Veiculo
 * @category Tipos
 * @subcategory Veiculo
 */

export const SubVeiculoSchema = Yup.object().shape({
  titulo: Yup.string(),
  tipoFormacaoTitulo: Yup.string(),
  desc: Yup.string(),
  tipoFormacaoDesc: Yup.string(),
  placa: Yup.string(),
  marca: Yup.string(),
  modelo: Yup.string(),
  tipo: Yup.string(),
  ano: Yup.number(),
  limite: LimiteSchema.nullable(true),
  autonomo: Yup.boolean(), // Capaz de transportar carga sozinho
  trator: Yup.boolean(), // Com tração própria
  // Veiculos que pode carregar
  funcao: Yup.string()
})

export const VeiculoSchema = Yup.object().shape({
  titulo: Yup.string().nullable(true),
  tipoFormacaoTitulo: Yup.string(),
  desc: Yup.string().nullable(true),
  tipoFormacaoDesc: Yup.string(),
  placa: Yup.string(),
  marca: Yup.string(),
  modelo: Yup.string(),
  tipo: Yup.string(),
  ano: Yup.number(),
  limite: LimiteSchema.nullable(true),
  autonomo: Yup.boolean(), // Capaz de transportar carga sozinho
  trator: Yup.boolean(), // Com tração própria
  // Veiculos que pode carregar
  compatibilidade: Yup.array().of(
    SubVeiculoSchema.nullable(true)
    ).nullable(true),
  funcao: Yup.string()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Veiculo
 * @category Tipos
 * @subcategory Veiculo
 */
export const initialValues = {
  titulo: '',
  tipoFormacaoTitulo: TIPO_FORMACAO.AUTOMATIZADO,
  desc: '',
  tipoFormacaoDesc: TIPO_FORMACAO.AUTOMATIZADO,
  placa: '',
  marca: '',
  modelo: '',
  tipo: '',
  ano: 0,
  limite: limiteInitialValues,
  autonomo: false, // Capaz de transportar carga sozinho
  trator: false, // Com tração própria
  // Veiculos que pode carregar
  compatibilidade: [],
  funcao: TIPO_FUNCAO_VEICULO.CARRETA
}
