import _ from 'lodash';
import * as Yup from 'yup'
import { CrudType } from '../Crud';
import { DTO } from '../DTO';
import { Usuario, UsuarioSchema } from './Auth/Usuario';
import { EmpresaSchema } from './Empresa/Empresa';
import { Filial, FilialSchema } from './Filial';

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/Funcionario
 * @category Tipos
 * @subcategory Funcionario
 */
export const crudType = new CrudType('FUNCIONARIO')

 /**
 * DTO
 * 
 * @name Funcionario
 * @module types/API/Funcionario
 * @category Tipos
 * @subcategory Funcionario
 */
export class Funcionario extends DTO {
  nome?: String
  identificacao?: String
  posicoes?: Array<String>
  filiais?: Array<Filial>
  empresa?: any
  criarUsuario: boolean = false
  usuario?: Usuario
  
  toDTO() {
    const obj = _.clone(this) as any
    obj.filiais = (this.filiais || []).map((val: any) => val._id)
    obj.empresa = (this.empresa ? this.empresa._id || this.empresa : null)
    return obj
  }

}

 /**
 * Esquema para formulário
 * 
 * @name FuncionarioSchema
 * @module types/API/Funcionario
 * @category Tipos
 * @subcategory Funcionario
 */
export const FuncionarioSchema = Yup.object().shape({
  nome: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  identificacao: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  posicoes: Yup.array().of(Yup.string()).nullable(true),
    // filiais: Yup.array().of(Yup.string()),
  filiais: Yup.array().of(FilialSchema.nullable(true)).nullable(true),
  empresa: EmpresaSchema.nullable(true),
  criarUsuario: Yup.boolean(),
  usuario: UsuarioSchema.nullable(true),
  
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Funcionario
 * @category Tipos
 * @subcategory Funcionario
 */
export const initialValues = {
  nome: '',
  identificacao: '',
  posicoes: [],
  filiais: [],
  empresa: null,
  criarUsuario: Boolean,
  usuario: null,
}
