import { DTO } from '../../DTO'
import { Filial } from '../Filial'
import { Funcionario } from '../Funcionario'
import { Entrega } from '../Logistica/Entrega'
import { Parceiro } from '../Parceiro/Parceiro'
import { Produto } from '../Produto'
import { Veiculo } from '../Veiculo'

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/Empresa/DadosEmprresa
 * @category Tipos
 * @subcategory DadosEmprresa
 */
export const crudType = {
  
}

 /**
 * DTO
 * 
 * @name DadosEmprresa
 * @module types/API/Empresa/DadosEmprresa
 * @category Tipos
 * @subcategory DadosEmprresa
 */
export class DadosEmprresa extends DTO {
  filiais?: Array<Filial>
  parceiros?: Array<Parceiro>
  veiculos?: Array<Veiculo>
  funcionarios?: Array<Funcionario>
  produtos?: Array<Produto>
  entregas?: Array<Entrega>
}

