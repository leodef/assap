import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { DTO } from '../../DTO'

 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/Empresa/Empresa
 * @category Tipos
 * @subcategory Empresa
 */
export const crudType = new CrudType('EMPRESA')

 /**
 * DTO
 * 
 * @name Empresa
 * @module types/API/Empresa/Empresa
 * @category Tipos
 * @subcategory Empresa
 */
export class Empresa extends DTO {
  nomeFantasia?: String
  razaoSocial?: String
  desc?: String
  identificacao?: String
}

 /**
 * Esquema para formulário
 * 
 * @name EmpresaSchema
 * @module types/API/Empresa/Empresa
 * @category Tipos
 * @subcategory Empresa
 */
export const EmpresaSchema = Yup.object().shape({
  nomeFantasia: Yup.string(),
  razaoSocial: Yup.string(),
  desc: Yup.string().nullable(true),
  identificacao: Yup.string()
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Empresa/Empresa
 * @category Tipos
 * @subcategory Empresa
 */
export const initialValues = {
  nomeFantasia: '', 
  razaoSocial: '', 
  desc: '', 
  identificacao: ''
}
