import * as Yup from 'yup'
import { CrudType } from '../Crud'
import { DTO } from '../DTO'
import {
  Localizacao,
  LocalizacaoSchema,
  initialValues as localizacaoInitialValues
} from './Localizacao'


 /**
 * Constantes para gerenciamento de entidades
 * 
 * @name crudType
 * @module types/API/Filial
 * @category Tipos
 * @subcategory Filial
 */
export const crudType = new CrudType('FILIAL')

 /**
 * DTO
 * 
 * @name Filial
 * @module types/API/Filial
 * @category Tipos
 * @subcategory Filial
 */
export class Filial extends DTO {
  titulo?: String
  desc?: String
  localizacao?: Localizacao

  
  static compare (value: any, other: any, index?: number): boolean {
    const result = (
      Boolean(other) &&
      Boolean(value) &&
      (
        (Boolean(other._id) && other._id === value._id)
        || (Boolean(other._temp) && other._temp === value._temp)
        // || (other.ordem && other.ordem === value.ordem) ||
        //  || (Boolean(index) && !other.ordem && value.ordem === index)
      )
    )
    return result
  }

  compare (value: any): boolean {
    return Filial.compare(this, value)
  }
}

 /**
 * Esquema para formulário
 * 
 * @name FilialSchema
 * @module types/API/Filial
 * @category Tipos
 * @subcategory Filial
 */
export const FilialSchema = Yup.object().shape({
  _temp: Yup.string().nullable(true),
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  desc: Yup.string().nullable(true),
  localizacao: LocalizacaoSchema.nullable(true)
})

 /**
 * Valores iniciais para formulário
 * 
 * @name initialValues
 * @module types/API/Filial
 * @category Tipos
 * @subcategory Filial
 */
export const initialValues = {
  _temp: '',
  titulo: '',
  desc: '',
  localizacao: localizacaoInitialValues,
}
