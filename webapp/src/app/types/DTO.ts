import _ from 'lodash'
import * as mongoose from 'mongoose'

export const cleanLabel = (result: string) => {
  result = result.trim()
  if(result.startsWith('-')) {
    result = result.substring(1).trim()
  }
  if(result.endsWith('-')) {
    result = result.substring(0, result.length-1).trim()
  }
  if(result.startsWith('/')) {
    result = result.substring(1).trim()
  }
  if(result.endsWith('/')) {
    result = result.substring(0, result.length-1).trim()
  }
  return result;
}

export const generateId = () => {
  const id = mongoose.Types.ObjectId().toString();
  return id
}

 
export const generateTempId = () => {
  return '_'+DTO.generateId()
}

export class DTO {
  _id?: String
  _temp?: String
  __v?: Number
  createdAt?: String
  updatedAt?: String

  static cleanLabel (result: string) {
    return cleanLabel(result)
  }

  static generateId(){
    return generateId()
  }

  static generateTempId(){
    return generateTempId()
  }

  static apply (obj: any, method: any, params?: any) {
    if (!obj || !obj[method]) {
      return null
    }
    return obj[method].apply(obj, params)
  }

  static fromJson (obj: any, json: any) {
    return DTO.apply(obj, 'fromJson', [json]) || JSON.parse(json)
  }

  static toJson (obj: any) {
    return DTO.apply(obj, 'toJson') || JSON.stringify(obj)
  }

  static fromForm (obj: any, json: any) {
    return DTO.apply(obj, 'fromForm', [json]) || JSON.parse(json)
  }

  static toForm (obj: any) {
    return DTO.apply(obj, 'toForm') || JSON.stringify(obj)
  }

  static isId(val: any) {
    return mongoose.Types.ObjectId.isValid(val)
  }

  assign (target: any, source: any, config?: any): any {
    target = target || this as any
    // Object.assign(target, source)
    Object.keys(source).forEach(key => (target[key] = source[key]))
    return target
  }

  fromJson (json: any): any {
    if (!json) { return this }
    try{
      json = (typeof json === 'string') ? JSON.parse(json) : json
      this.assign(this, json)
    } catch(err) {
      if(DTO.isId(json)) {
        this._id = json
      }
    }
    return this
  }

  toJson (value?: any): any {
    return JSON.stringify(value || this)
  }

  fromForm (form: any): any {
    return this.fromJson(form)
  }

  toForm (): any {
    return this.toJson()
  }

  toDTO(): any {
    return _.clone(this) as any
  }
}
