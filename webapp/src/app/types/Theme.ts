
export enum ThemeType {
    SET = 'SET_THEME',
    SET_DARK = 'SET_THEME_DARK',
    SET_LIGHT = 'SET_THEME_LIGHT',
    LOAD = 'LOAD_THEME',
    LOAD_SUCCESS = 'LOAD_THEME_SUCCESS',
    TOGGLE = 'TOGGLE_THEME'
}


export enum ThemeValue {
    light = 'light',
    dark = 'dark'
}
