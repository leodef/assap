/* eslint-disable no-unused-vars */

export enum MessageType {
    SHOW_MESSAGE = 'SHOW_MESSAGE',
    ADD_MESSAGE = 'ADD_MESSAGE',
    REMOVE_MESSAGE = 'REMOVE_MESSAGE'
}

export enum MessageVariant {
    error = 'error',
    info = 'info',
    success = 'success',
    warning = 'warning'
}

/*
   primary = 'primary',
    secondary = 'secondary',
    success = 'success',
    danger = 'danger',
    warning = 'warning',
    info = 'info',
    light = 'light',
    dark = 'dark',
 */

export const getMessagesList = (props: any): Array<any>  => {
    const list = (props.messages || []);
    return list.filter((message: any) => message && !message.local && message.message);
  }
  
export const compare = (msgA: any, msgB: any) => {
    return msgA._id && msgA._id === msgB._id
  }