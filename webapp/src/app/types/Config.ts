
import moment from 'moment'

export const dateFormat = 'DD/MM/YYYY'
export const dateFormatInput = 'YYYY-MM-DD'
export const dateFormatApi = 'YYYY-MM-DDTHH:mm:ss.sssZ'

export const minDataInput = '1700-04-01'
export const maxDataInput = '2100-12-30'

export const dataInput = {
  format: dateFormatInput,
  min: minDataInput,
  max: maxDataInput
}

export const dateToString = (date?: any, format: any = dateFormat) => {
  return moment(date).format(format)
}

export const dateToStringInput = (date?: any) => {
  return dateToString(date, dateFormatInput)
}

export const stringToDate = (date?: any, format: any = dateFormat) => {
  return moment(date, format).toDate()
}

export const stringToDateInput = (date?: any) => {
  return stringToDate(date, dateFormatInput)
}
