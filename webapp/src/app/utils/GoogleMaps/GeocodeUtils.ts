import Geocode from 'react-geocode'
import { GoogleLocation } from './GoogleLocation'
// eslint-disable-next-line no-unused-vars
import { Localizacao } from '../../types/API/Localizacao'
import { getGoogleApiKey } from './GoogleUtils'

export class GeocodeUtils {
  static load () {
    const googleApiKey = getGoogleApiKey()
    if (googleApiKey) { Geocode.setApiKey(googleApiKey) }
    if (process.env.NODE_ENV !== 'production') { Geocode.enableDebug() }
  }

  /**
   * Get the area and set the area input value to the one selected
   *
   * @param {any} lat 48.8583701
   * @param {any} lng 2.2922926
   * @return {Location}
   */
  static fromLatLng (lat: any, lng: any): Promise<Localizacao> {
    return Geocode.fromLatLng(lat, lng).then(
      (response: any) => (GoogleLocation.getInstance(response).toSchema())
    )
  }

  /**
   * Get the area and set the area input value to the one selected
   *
   * @param {any} address Eiffel Tower
   * @param {any} lng 2.2922926
   * @return {Location}
   */
  static fromAddress (address: any): Promise<Localizacao> {
    return Geocode.fromAddress(address).then(
      (response: any) => (GoogleLocation.getInstance(response).toSchema())
    )
  }
  // Get latidude & longitude from address.
}
