import moment from 'moment'
// eslint-disable-next-line no-unused-vars
import {
  Entrega
} from '../../../types/API/Logistica/Entrega'
// eslint-disable-next-line no-unused-vars
import {
  PontoRota
} from '../../../types/API/Logistica/PontoRota'
import {
  UrlUtils
} from '../../UrlUtils'
import {
  getGoogleMapsLinkHost,
  getLatLng,
  getWaypoint
} from '../GoogleUtils'

// https://developers.google.com/maps/documentation/javascript/reference/directions#DirectionsRequest
export class DirectionsRequest {
  // Controle
  _google: any
  _locations: Array<any> = []
  _center: any
  // Padrão
  travelMode: any
  optimizeWaypoints: boolean = false
  // Entrega
  drivingOptions: any = { departureTime: new Date() };
  // Pontos
  origin: any;
  destination: any;
  waypoints: any;

  static getInstance (google: any) {
    return new DirectionsRequest(google)
  }

  /**
   * Initialize object with api response
   *
   * @param {any} response
   * @return {string}
   */
  static loadEntrega (
    entrega: Entrega,
    directionsRequest: DirectionsRequest,
    config?: any) {
    // Padrão
    directionsRequest.travelMode = directionsRequest._google.maps.TravelMode.DRIVING

    // Config
    DirectionsRequest.loadConfig(
      config,
      directionsRequest)

    // Entrega
    if (!entrega) { return }
    DirectionsRequest.loadDataEntrega(entrega, directionsRequest)

    // Rota
    const rota = entrega.rota
    if (!rota) { return }
    if (rota.pontos) {
      DirectionsRequest.loadPontos(
        rota.pontos as Array<PontoRota>,
        directionsRequest)
    }

    return directionsRequest
  }

  static loadDataEntrega (
    entrega: Entrega,
    directionsRequest: DirectionsRequest
  ) {
    directionsRequest.drivingOptions = (directionsRequest.drivingOptions || {} as any)
    const dataEntrega = entrega.data
    const dataAtual = new Date()
    directionsRequest.drivingOptions.departureTime = (
      !dataAtual ||
      moment(dataEntrega).isBefore(dataAtual)) ? dataAtual : dataEntrega
    return directionsRequest
  }

  static loadConfig (
    config: any,
    directionsRequest: DirectionsRequest
  ) {
    if (!config) { return }
    directionsRequest.travelMode = config.travelMode || directionsRequest.travelMode
    directionsRequest.optimizeWaypoints = config.optimizeWaypoints || directionsRequest.optimizeWaypoints
    return directionsRequest
  }

  static loadPontos (
    pontos: Array<PontoRota> = [],
    directionsRequest: DirectionsRequest) {
    const google = directionsRequest._google
    const locations = pontos
      .map(DirectionsRequest.pontoToLocation)
      .filter((direction: any) => Boolean(direction))
    const waypoints = [...locations]
    const origin = waypoints.shift()
    const destination = waypoints.pop()
    const center = locations.reduce((prev: any, curr: any, currIndex: number) => {
      const { lat, lng } = curr
      return currIndex === 0
        ? { lat, lng }
        : ({ lat: (prev.lat + lat) / 2, lng: (prev.lng + lng) / 2 })
    }, ({ lat: 0, lng: 0 }))

    // Control
    directionsRequest._locations = locations
    directionsRequest._center = center
    // Request
    directionsRequest.waypoints = waypoints.map((location: any) => getWaypoint(google, location))
    directionsRequest.origin = getLatLng(google, origin)
    directionsRequest.destination = getLatLng(google, destination)
    // directionsRequest.travelMode = google.maps.TravelMode.DRIVING
    // directionsRequest.waypoints = config.waypoints ? config.waypoints.map(getWaypoint) : null
    return directionsRequest
  }

  static pontoToLocation (ponto: any, index: number) {
    return ponto.localizacao
      ? ({
        name: ponto.titulo || ponto.endereco || index.toString(),
        lat: ponto.localizacao.lat || 0,
        lng: ponto.localizacao.long || 0
      })
      : null
  }

  get google () {
    return this._google
  }

  get locations () {
    return this._locations
  }

  get center () {
    return this._center
  }

  constructor (google: any) {
    this._google = google
  }

  toRequest () {
    const {
      travelMode,
      optimizeWaypoints,
      drivingOptions,
      origin,
      destination,
      waypoints
    } = this
    return {
      travelMode,
      optimizeWaypoints,
      drivingOptions,
      origin,
      destination,
      waypoints
    }
  }

  getLocationUrlParam (location: any) {
    location = location.location || location
    return `${location.lat()},${location.lng()}`
  }

  toUrl () {
    const google = this._google
    const host = getGoogleMapsLinkHost()
    // 'https://www.google.com/maps/dir/'
    // 'http://maps.google.com/maps'
    const {
      origin,
      destination,
      travelMode,
      waypoints
    } = this
    const params = {
      api: 1,
      origin: this.getLocationUrlParam(origin),
      destination: this.getLocationUrlParam(destination),
      travelmode: travelMode || google.maps.TravelMode.DRIVING,
      waypoints: waypoints.map(this.getLocationUrlParam).join('|')
    }
    return UrlUtils.loadUrlParams(host, params)
  }
}
