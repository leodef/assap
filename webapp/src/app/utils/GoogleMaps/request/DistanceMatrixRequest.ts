import moment from 'moment'
// eslint-disable-next-line no-unused-vars
import { Entrega } from '../../../types/API/Logistica/Entrega'
// eslint-disable-next-line no-unused-vars
import { PontoRota } from '../../../types/API/Logistica/PontoRota'

// https://developers-dot-devsite-v2-prod.appspot.com/maps/documentation/javascript/reference/distance-matrix#DistanceMatrixRequest
export class DistanceMatrixRequest {
  // Controle
  _google: any
  _locations: Array<any> = []
  _map: any = {}
  // Padrão
  travelMode: any
  transitOptions: any = null // TransitOptions
  unitSystem: any = null // UnitSystem
  avoidHighways: boolean = false
  avoidTolls: boolean = false
  // Entrega
  drivingOptions: any = { departureTime: new Date() };
  // Pontos
  origins: Array<any> = [];
  destinations: Array<any> = [];

  static getInstance (google: any) {
    return new DistanceMatrixRequest(google)
  }

  /**
   * Initialize object with api response
   *
   * @param {any} response
   * @return {string}
   */
  static loadEntrega (
    entrega: Entrega,
    directionsRequest: DistanceMatrixRequest,
    config?: any) {
    // Padrão
    directionsRequest.travelMode = directionsRequest._google.maps.TravelMode.DRIVING
    // Config
    DistanceMatrixRequest.loadConfig(
      config,
      directionsRequest)

    // Entrega
    if (!entrega) { return }
    DistanceMatrixRequest.loadDataEntrega(entrega, directionsRequest)

    // Rota
    const rota = entrega.rota
    if (!rota) { return }
    if (rota.pontos) {
      DistanceMatrixRequest.loadPontos(
        rota.pontos as Array<PontoRota>,
        directionsRequest)
    }
    return directionsRequest
  }

  static loadDataEntrega (
    entrega: Entrega,
    directionsRequest: DistanceMatrixRequest
  ) {
    directionsRequest.drivingOptions = (directionsRequest.drivingOptions || {} as any)
    const dataEntrega = entrega.data
    const dataAtual = new Date()
    directionsRequest.drivingOptions.departureTime = (
      !dataAtual ||
      moment(dataEntrega).isBefore(dataAtual)) ? dataAtual : dataEntrega
    return directionsRequest
  }

  static loadConfig (
    config: any,
    directionsRequest: DistanceMatrixRequest
  ) {
    if (!config) { return }
    directionsRequest.travelMode = config.travelMode || directionsRequest.travelMode
    directionsRequest.transitOptions = config.transitOptions || directionsRequest.transitOptions // TransitOptions
    directionsRequest.unitSystem = config.unitSystem || directionsRequest.unitSystem // UnitSystem
    directionsRequest.avoidHighways = config.avoidHighways || directionsRequest.avoidHighways
    directionsRequest.avoidTolls = config.avoidTolls || directionsRequest.avoidTolls
    return directionsRequest
  }

  static loadPontos (
    pontos: Array<PontoRota> = [],
    directionsRequest: DistanceMatrixRequest) {
    const filterPontos = pontos.filter((pto: any) => Boolean(pto) && Boolean(pto.localizacao))
    const locations = filterPontos.map(DistanceMatrixRequest.pontoToLocation)
    directionsRequest._locations = locations
    const _map = {} as any;
    filterPontos.forEach( (ponto, index) => {
      let key = String(ponto._id || ponto._temp).toString()
      _map[key] =  index
    })
    directionsRequest._map = _map
    // Request
    directionsRequest.origins =
        directionsRequest.destinations =
            locations
    // directionsRequest.travelMode = google.maps.TravelMode.DRIVING
    // directionsRequest.waypoints = config.waypoints ? config.waypoints.map(getWaypoint) : null
    return directionsRequest
  }

  static pontoToLocation (ponto: any, index: number) {
    return ponto.localizacao
      ? ({
        name: ponto.titulo || ponto.endereco || index.toString(),
        lat: ponto.localizacao.lat || 0,
        lng: ponto.localizacao.long || 0
      })
      : null
  }

  get google () {
    return this._google
  }

  get locations () {
    return this._locations
  }

  constructor (google: any) {
    this._google = google
  }

  toRequest () {
    const {
      travelMode,
      transitOptions,
      unitSystem,
      avoidHighways,
      avoidTolls,
      drivingOptions,
      origins,
      destinations
    } = this
    return {
      travelMode,
      transitOptions,
      unitSystem,
      avoidHighways,
      avoidTolls,
      drivingOptions,
      origins,
      destinations
    }
  }
}
