import { Localizacao } from '../../types/API/Localizacao'

export class GoogleLocation {
  country?: String
  address?: String
  city?: String
  area?: String
  state?: String
  lat?: Number
  lng?: Number
  postalCode?: String
  streetNumber?: String
  complement?: String

  static getInstance (response: any) {
    return new GoogleLocation(response)
  }

  constructor (partial?: any) {
    if (partial) {
      this.load(partial)
    }
  }

  toSchema (result = new Localizacao()): Localizacao {
    result.titulo = result.titulo || this.address
    result.endereco = this.address
    result.lat = this.lat
    result.long = this.lng
    result.pais = this.country
    result.estado = this.state
    result.cidade = this.city
    result.area = this.area
    result.codigoPostal = this.postalCode
    result.numero = this.streetNumber
    result.complemento = this.complement
    return result
  }

  fromSchema (schema: Localizacao): GoogleLocation {
    // titulo
    this.address = schema.endereco
    this.lat = schema.lat
    this.lng = schema.long
    this.country = schema.pais
    this.state = schema.estado
    this.city = schema.cidade
    this.area = schema.area
    this.postalCode = schema.codigoPostal
    this.country = schema.pais
    this.streetNumber = schema.numero
    this.complement = schema.complemento
    return this
  }

  /**
   * Initialize object with api response
   *
   * @param {any} response
   * @return {string}
   */
  load (response: any) {
    const result = (response.results ? response.results[0] : response)
    const addressArray = result.address_components
    this.address = result.formatted_address
    this.lat = result.geometry.location.lat // lat()
    this.lng = result.geometry.location.lng // lng()
    this.country = this.getCountry(addressArray)
    this.state = this.getState(addressArray)
    this.city = this.getCity(addressArray)
    this.area = this.getArea(addressArray)
    this.postalCode = this.getPostalCode(addressArray)
    this.streetNumber = this.getStreetNumber(addressArray)
    this.complement = this.getComplement(addressArray)
    return this
  }

  /**
   * Get the city and set the city input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getCity (addressArray: Array<any>) {
    let city = ''
    for (let i = 0; i < addressArray.length; i++) {
      if (addressArray[i].types[0] && addressArray[i].types[0] === 'administrative_area_level_2') {
        city = addressArray[i].long_name
        return city
      }
    }
    return undefined
  };

  /**
   * Get the area and set the area input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getArea (addressArray: Array<any>) {
    let area = ''
    for (let i = 0; i < addressArray.length; i++) {
      if (addressArray[i].types[0]) {
        for (let j = 0; j < addressArray[i].types.length; j++) {
          if (addressArray[i].types[j] === 'sublocality_level_1' || addressArray[i].types[j] === 'locality') {
            area = addressArray[i].long_name
            return area
          }
        }
      }
    }
    return undefined
  };

  /**
   * Get the address and set the address input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getState (addressArray: Array<any>) {
    let state = ''
    for (let i = 0; i < addressArray.length; i++) {
      for (let i = 0; i < addressArray.length; i++) {
        if (addressArray[i].types[0] && addressArray[i].types[0] === 'administrative_area_level_1') {
          state = addressArray[i].long_name
          return state
        }
      }
    }
    return undefined
  }

  /**
   * Get the address and set the address input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getCountry (addressArray: Array<any>) {
    let state = ''
    for (let i = 0; i < addressArray.length; i++) {
      for (let i = 0; i < addressArray.length; i++) {
        if (addressArray[i].types[0] && addressArray[i].types[0] === 'country') {
          state = addressArray[i].long_name
          return state
        }
      }
    }
    return undefined
  }

  /**
   * Get the address and set the address input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getPostalCode (addressArray: Array<any>) {
    let state = ''
    for (let i = 0; i < addressArray.length; i++) {
      for (let i = 0; i < addressArray.length; i++) {
        if (addressArray[i].types[0] && addressArray[i].types[0] === 'postal_code') {
          state = addressArray[i].long_name
          return state
        }
      }
    }
    return undefined
  }

  /**
   * Get the address and set the address input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getStreetNumber (addressArray: Array<any>) {
    let state = ''
    for (let i = 0; i < addressArray.length; i++) {
      for (let i = 0; i < addressArray.length; i++) {
        if (addressArray[i].types[0] && addressArray[i].types[0] === 'street_number') {
          state = addressArray[i].long_name
          return state
        }
      }
    }
    return undefined
  }

  getComplement (addressArray: Array<any>) {
    let state = ''
    for (let i = 0; i < addressArray.length; i++) {
      for (let i = 0; i < addressArray.length; i++) {
        if (addressArray[i].types[0] && (
          addressArray[i].types[0] === 'floor' ||
              addressArray[i].types[0] === 'establishment' ||
              addressArray[i].types[0] === 'room'
        )
        ) {
          state += ` ${addressArray[i].types[0]}: ${addressArray[i].long_name} `
        }
      }
    }
    return state
  }
}
