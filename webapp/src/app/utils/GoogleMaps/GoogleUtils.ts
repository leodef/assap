import { UrlUtils } from "../UrlUtils"
import { DistanceMatrixRequest } from "./request/DistanceMatrixRequest"
const haversine = require('haversine')

export const getGoogleApiKey = () => process.env.REACT_APP_GOOGLE_MAPS_API_KEY

export const getGoogleMapsApiUrl = () => `https://maps.googleapis.com/maps/api/js?key=${
    getGoogleApiKey()
  }&v=3.exp&libraries=geometry,drawing,places`

export const getGoogleMapsLinkHost = () => 'https://www.google.com/maps/dir/'

export const getLocationGoogleMapsLink = (point: any) => {
  const host = getGoogleMapsLinkHost()
  const lat = point.lat
  const lng = point.lng || point.long
  const params = {
    z: 12,
    t: 'm',
    q: `loc:${lat}+${lng}`
  }
  return UrlUtils.loadUrlParams(host, params)
}

export const getLatLng = (google: any, obj: any, wrapper?: any, key?: any): any => {
  if (!google || !obj) {
    return null
  }
  const latLng = new google.maps.LatLng(obj.lat, obj.lng)
  return wrapper && key ? ({ ...wrapper, [key]: latLng }) : latLng
}

export const getWaypoint = (google: any, direction: any): any => {
  return getLatLng(google, direction, { stopover: true }, 'location')
}

export const route = (
  directionsParams: any,
  callback: any
): void => {
  const directionsService = new window.google.maps.DirectionsService()
  if (!directionsParams) { return }
  directionsService.route(
    directionsParams,
    (result: any, status: any) => {
    // https://developers.google.com/maps/documentation/javascript/reference/directions#DirectionsResult
      if (status === window.google.maps.DirectionsStatus.OK) {
        if (callback) {
          callback(result)
        }
      } else {
        console.error(
          `DirectionsService.route error: ${result}`
        )
      }
    })
}

export const getDistanceMatrix = (
  distanceMatrixRequest: any,
  callback: any
): void => {
  var distanceMatrixService = new window.google.maps.DistanceMatrixService()
  if (!distanceMatrixRequest) { return }
  distanceMatrixService.getDistanceMatrix(
    distanceMatrixRequest,
    (result: any, status: any) => {
      if (status === window.google.maps.DistanceMatrixStatus.OK) {
        if (callback) {
          callback(result, status)
        }
      } else {
        console.error(
          `DistanceMatrixService.getDistanceMatrix error: ${result} ${status}`
        )
        callback(null, status)
      }
    })
}

export const getLocalDistanceMatrix = (request: DistanceMatrixRequest) => {
  const { origins, destinations } = request
  const response = [] as Array<any>
  for(const originIndex in origins) {
    const origin = origins[originIndex]
    response[originIndex] = ({elements: []})
    for(const destinationIndex in destinations) {
      const destination = destinations[destinationIndex]
      response[
        originIndex
      ].elements[
        destinationIndex
      ] = getLocalDistanceMatrixElement(origin, destination)
    }
  }
  return response
}

export const getLocalDistanceMatrixElement = (origin: any, destination: any) => {
  const estimatedDistance = getEstimatedDistance(origin, destination)
  const estimatedDistanceDesc = getDistanceDesc(estimatedDistance)

  const estimatedDuration = getEstimatedDuration(estimatedDistance)
  const estimatedDurationDesc = getDurationDesc(estimatedDuration)

  const estimatedDurationInTraffic = getEstimatedDurationInTraffic(estimatedDistance)
  const estimatedDurationInTrafficDesc = getDurationDesc(estimatedDurationInTraffic)
  const element = ({
    distance: {
      text: estimatedDistanceDesc,
      value: estimatedDistance
    },
    duration: {
      text: estimatedDurationDesc,
      value: estimatedDuration
    },
    duration_in_traffic: {
      text: estimatedDurationInTrafficDesc,
      value: estimatedDurationInTraffic
    },
    status: "OK"
  })
  return element
}

export const getDistanceUnit = (value: number) => {
  let unit = 'm'
  if (value > 1000) {
    value /= 1000
    unit = 'Km'
  }
  return { unit, value }
}

export const getDistanceDesc = (val: number) => {
  const { unit, value } = getDistanceUnit(val)
  return `${ value.toFixed(2)} ${unit}`
}

export const getDurationUnit = (value: number) => {
  let unit = 's'
  if (value > 60) {
    value /= 60
    unit = 'min'
    if (value > 60) {
      value /= 60
      unit = 'h'
    }
  }
  return { unit, value }
}

export const getDurationDesc = (val: number) => {
  const { unit, value } = getDurationUnit(val)
  return `${ value.toFixed(2)} ${unit}`
}

export const getEstimatedDuration = (distance: number , traffic = false) => {
  // km
  distance = distance / 1000
  // 80 km / h
  const speed = traffic ? 56 : 80
  let durationH = distance / speed
  // % de paradas e limites
  const stopPerCent = traffic ? 60 : 50
  durationH = (durationH + ( (durationH / 100) * stopPerCent) )
  // s - segundos
  return durationH * 3600
}

export const getEstimatedDurationInTraffic = (distance: number) => {
  return getEstimatedDuration(distance, true)
}

export const getEstimatedDistance = (origin: any, destination: any) => {
  const distance = getDistance(origin, destination)
  // + 15% de curvas e quadras
  return (distance + ( (distance / 100) * 15) )
}

export const getDistance = (origin: any, destination: any) => {
  const distance =  haversine(
    [origin.lat, origin.lng],
    [destination.lat, destination.lng],
    { unit: 'meter', format: '[lat,lon]' }
  )
  return distance
  /*
    return Math.sqrt(
      Math.pow(Math.abs((a.x || 0) - (b.x || 0)), 2) +
      Math.pow( Math.abs((a.y || 0) - (b.y || 0)), 2)
    );
  */
}