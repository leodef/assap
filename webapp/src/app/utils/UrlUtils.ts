export class UrlUtils {
  static loadUrlParams (path: string, obj?: any) {
    if (!obj) {
      return path
    }
    const keys = Object
      .keys(obj).filter(key => obj[key] !== undefined && obj[key] !== null)
    if (keys.length === 0) {
      return path
    }
    const params = encodeURI(keys
      .map(key => `${key}=${obj[key]}`)
      .join('&'))
    return `${path}?${params}`
  }
}
