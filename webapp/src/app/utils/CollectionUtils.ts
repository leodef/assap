import _ from 'lodash'

export class CollectionUtils {
// Verificar texto presente no item
  static contains (item: any, filter: any, key?: any) {
    if (!filter) { return false }
    for (const index in item) {
      const obj = item[index]
      if (!obj) {
        continue
      }
      let filterVal = ((typeof filter === 'string') ? filter : filter[index])
      if (!filterVal) {
        continue
      }
      filterVal = filterVal.toString().toUpperCase()
      if (obj.toString().toUpperCase().includes(filterVal)) {
        return true
      }
    }
    return false
  }

  // Aplicar paginação
  static applyPagination (items: any, pagination?: any): any {
    if (!pagination) {
      return items
    }
    let { page, limit } = pagination
    page = page || 1
    limit = limit || 1
    if (!limit) {
      return items
    }
    let itemsArray = (items.items || items)
    const start = (page - 1) * limit
    const end = start + limit
    const count = itemsArray.length
    const pages = Math.ceil(count / limit)
    itemsArray = itemsArray.slice(start, end)
    return {
      items: itemsArray,
      pagination: {
        count,
        limit,
        page,
        pages
      }
    }
  }

  // Aplicar o filtro
  static applyFilter (items: any, filter?: any): any {
    if (!filter || filter === '') {
      return items
    }
    let itemsArray = (items.items || items)
    itemsArray = itemsArray.filter((item: any) => CollectionUtils.contains(item, filter))
    if (items.items) {
      items.items = itemsArray
    } else {
      items = itemsArray
    }
    return items
  }

  // Selecionar campos
  static applyFields (items: any, fields?: any): any {
    if (!fields) { return items }
    let itemsArray = (items.items || items)
    itemsArray = itemsArray.map((item: any) => {
      const obj = {} as any
      for (const key in fields) {
        const val = fields[key]
        if (val) {
          obj[key] = val
        }
      }
      return obj
    })
    if (items.items) {
      items.items = itemsArray
    } else {
      items = itemsArray
    }
    return items
  }

  // Ordernar lista
  static applySort (items: any, sort?: any): any {
    if (!sort) { return items }
    try {
      sort = JSON.parse(sort)
    } catch (err) {}
    let itemsArray = (items.items || items)
    sort = (typeof sort === 'string') ? [sort] : sort
    sort = (Array.isArray(sort)) ? sort : Object.keys(sort)
    itemsArray = _.sortBy(items, sort)
    if (items.items) {
      items.items = itemsArray
    } else {
      items = itemsArray
    }
    return items
  }

  static resolveCollection (items: any, params: any) {
    const {
      filter,
      pagination,
      fields,
      sort
    } = params
    // Aplicar o filtro
    items = CollectionUtils.applyFilter(items, filter)
    // Selecionar campos
    items = CollectionUtils.applyFields(items, fields)
    // Ordernar lista
    items = CollectionUtils.applySort(items, sort)
    // Aplicar paginação
    items = CollectionUtils.applyPagination(items, pagination)
    return items
  }
}
