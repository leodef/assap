export class SelectOption {
  public value?: string | number;
  public label?: string | number;
  // eslint-disable-next-line no-useless-constructor
  constructor (option: any) {
    if (option) {
      if (typeof option === 'string' || typeof option === 'number') {
        this.value = option
        this.label = option
        return
      }
      if (Array.isArray(option)) {
        if (typeof option[0] === 'string' || typeof option[0] === 'number') {
          this.value = option[0]
          this.label = option[1]
          return
        }
      }
      this.value = option.value || option.label
      this.label = option.label || option.value
    }
  }

  static load (obj: any): SelectOption | Array<SelectOption> {
    if (Array.isArray(obj)) {
      return obj.map((val: any, index: number) => this.load(val) as SelectOption)
    }
    return new SelectOption(obj)
  }

  static loadEnum (obj: any): Array<SelectOption> {
    return Object.keys(obj)
      .filter((key) => !!key)
      .map(
        (key) =>
          new SelectOption({
            value: key.startsWith('_') ? key.substr(1) : key,
            label: obj[key]
          })
      )
  }
}
