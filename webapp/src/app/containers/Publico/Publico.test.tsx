/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Publico } from './Publico'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Publico />, div)
  ReactDOM.unmountComponentAtNode(div)
})
