import React from 'react'
import { Switch, Route } from 'react-router-dom'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core/styles'
import Login from './Login/Login'
import { Navbar } from '../../components/Publico/Navbar/Navbar'
import './Publico.scss'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      flexGrow: 1
    }
  })
)

/**
 * Roteamento para telas abertas do sistema
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com roteamento para telas abertas do sistema
 */
export const Publico = (props: any) => {
  const classes = useStyles()
  return (
    <React.Fragment>
      <main>
        <Navbar />
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/login" component={Login} />
        </Switch>
      </main>
    </React.Fragment>)
}
export default Publico

/*
<AppBar
        position="relative">
        <Toolbar>
          <Typography
            variant="h6"
            className={classes.title}>
            Planning Route
          </Typography>
          <Button
            color="inherit">
              Login
          </Button>
        </Toolbar>
      </AppBar>
*/