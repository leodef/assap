import React, { useState, useMemo, useEffect, useCallback } from 'react'
import moment from 'moment'
import {
  MuiPickersUtilsProvider
} from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import { LoadScript } from '@react-google-maps/api'
import { Publico } from './Publico/Publico'
import { Modulo } from './Modulo/Modulo'
import { GeocodeUtils } from '../utils/GoogleMaps/GeocodeUtils'
import { getGoogleApiKey } from '../utils/GoogleMaps/GoogleUtils'
import { useDispatch, useSelector } from 'react-redux'
import { AuthType } from '../types/Auth'
import { Loading } from '../components/Shared/App/Loading/Loading'
import { Message } from '../components/Shared/App/Message/Message'
import './App.scss'
import { createMuiTheme, CssBaseline, ThemeProvider } from '@material-ui/core'
import { ThemeType, ThemeValue } from '../types/Theme'

/**
 * Componente principal da aplicação
 *   Faz a validação se o usuario esta autenticado para acessar o componente correto
 * @param {any}  props Propriedades
 * @return {React.Component} Componente que envolve toda a aplicação
 */
export const App = (props: any) => {
  // GoogleApi
  const googleApiKey = getGoogleApiKey()
  useEffect(() => GeocodeUtils.load(), [])

  const token = useSelector((state: any) => (state.auth || {}).token)
  const isAuth = useMemo(() => Boolean(token), [token])
  // dispatch
  const dispatch = useDispatch()
  // Carregamento de dados iniciais
  const loadAuth = useCallback(() =>
    dispatch({
      type: AuthType.LOAD
    }), [dispatch])

  const loadTheme = useCallback(() =>
    dispatch({
      type: ThemeType.LOAD
    }), [dispatch])
  
  useEffect(() => {
    loadAuth()
    loadTheme()
  }, [loadAuth, loadTheme])

  // usar um serviço para poder setar essa configuração de qualquer lugar
  const [locale, setLocale] = useState('br')
  const selectLocale = useCallback((loc: any) => {
    moment.locale(loc)
    setLocale(loc)
  }, [setLocale])
  useEffect(() => {
    selectLocale('br')
  }, [selectLocale])

  return (
    <LoadScript
      googleMapsApiKey={googleApiKey}
      libraries={['places']}
    >
      <MuiPickersUtilsProvider
        libInstance={moment}
        utils={MomentUtils}
        locale={locale}>
        <ThemeApp isAuth={isAuth}/>
      </MuiPickersUtilsProvider>
    </LoadScript>)
}


export const ThemeApp = (props: any) => {
  const isAuth = useMemo(() => props.isAuth, [props.isAuth])
  const body = useMemo(() => (isAuth ? (<Modulo />) : (<Publico />)), [isAuth])
  const themeValue = useSelector((state: any) => state.theme.value)
  const theme = useMemo(
    () =>
      createMuiTheme({
        palette: {
          type: themeValue
        },
      }),
    [themeValue],
  );
  return <ThemeProvider theme={theme}>
      <CssBaseline/>
      {body}
      <Loading />
      <Message />
    </ThemeProvider>

}

export default App
