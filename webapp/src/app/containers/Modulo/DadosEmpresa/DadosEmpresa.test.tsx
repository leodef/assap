/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
/* eslint-disable no-undef */
import { DadosEmpresa } from './DadosEmpresa'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<DadosEmpresa />, div)
  ReactDOM.unmountComponentAtNode(div)
})
