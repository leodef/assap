import React, { useMemo, useCallback } from 'react'
import {
  Veiculo as VeiculoComponent
} from '../../../../components/Modulo/DadosEmpresa/Veiculo/Veiculo'
import { crudType as types } from '../../../../types/API/Veiculo'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Veiculo.scss'

/**
 * Tela de gerenciameto de Veiculo
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Veiculo
 */
export const Veiculo = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.veiculo.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'veiculo'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <VeiculoComponent collection={collection} />
  </CrudContext.Provider>)
}
