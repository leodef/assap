import React, { useMemo, useCallback } from 'react'
import { Entrega as EntregaComponent } from '../../../../components/Modulo/Logistica/Entrega/Entrega'
import { crudType as types } from '../../../../types/API/Logistica/Entrega'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Entrega.scss'

/**
 * Tela de gerenciameto de Entrega
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Entrega
 */
export const Entrega = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.entrega.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'entrega'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <EntregaComponent collection={collection} />
  </CrudContext.Provider>)
}
