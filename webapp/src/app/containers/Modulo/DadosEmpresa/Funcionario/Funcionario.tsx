import React, { useMemo, useCallback } from 'react'
import {
  Funcionario as FuncionarioComponent
} from '../../../../components/Modulo/DadosEmpresa/Funcionario/Funcionario'
import { crudType as types } from '../../../../types/API/Funcionario'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Funcionario.scss'

/**
 * Tela de gerenciameto de Funcionário
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Funcionário
 */
export const Funcionario = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.funcionario.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'funcionario'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <FuncionarioComponent collection={collection} />
  </CrudContext.Provider>)
}
