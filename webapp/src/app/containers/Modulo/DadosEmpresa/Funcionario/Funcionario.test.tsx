/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Funcionario } from './Funcionario'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Funcionario />, div)
  ReactDOM.unmountComponentAtNode(div)
})
