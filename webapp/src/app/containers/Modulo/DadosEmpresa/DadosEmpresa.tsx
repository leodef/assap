import React, { useContext, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { ProtectedRoute } from '../../../components/Shared/App/ProtectedRoute/ProtectedRoute'
import {
  getParentContextValue,
  Parent,
  ParentCrudContext
} from '../../../contexts/ParentCrudContext'
import { getState } from '../../../types/EmpresaUsuario'
import { Entrega } from './Entrega/Entrega'
import { Filial } from './Filial/Filial'
import { Funcionario } from './Funcionario/Funcionario'
import { Parceiro } from './Parceiro/Parceiro'
import { Produto } from './Produto/Produto'
import { Veiculo } from './Veiculo/Veiculo'
import './DadosEmpresa.scss'
import { ActionTypeEnum } from '../../../types/Crud'

/**
 * Roteamento para telas do modulo admin
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com roteamento para telas do modulo admin
 */
export const DadosEmpresa = React.memo((props: any) => {
  DadosEmpresa.displayName = 'DadosEmpresa'
  const { item } = useSelector((state: any) => getState(state))
  const hidden = useMemo(() => !Boolean(item), [item]);
  const action = ActionTypeEnum.LIST
  const prefix = 'empresa'
  const parentValue = useMemo(
  () => new Parent(
    item,
    action,
    prefix
  ), [item, action, prefix])
const historyValue = useContext(ParentCrudContext)
const parentCrudContextValue = useMemo(
  () => getParentContextValue(parentValue, historyValue)
  , [parentValue, historyValue])
  return hidden ? null : (
    <ParentCrudContext.Provider value={parentCrudContextValue}>
      <ProtectedRoute exact path="/dados-empresa/entrega" component={Entrega} />
      <ProtectedRoute exact path="/dados-empresa/entrega/:action/:id" component={Entrega} />
      <ProtectedRoute exact path="/dados-empresa/filial" component={Filial} />
      <ProtectedRoute exact path="/dados-empresa/filial/:action/:id" component={Filial} />
      <ProtectedRoute exact path="/dados-empresa/funcionario" component={Funcionario} />
      <ProtectedRoute exact path="/dados-empresa/funcionario/:action/:id" component={Funcionario} />
      <ProtectedRoute exact path="/dados-empresa/parceiro" component={Parceiro} />
      <ProtectedRoute exact path="/dados-empresa/parceiro/:action/:id" component={Parceiro} />
      <ProtectedRoute exact path="/dados-empresa/produto" component={Produto} />
      <ProtectedRoute exact path="/dados-empresa/produto/:action/:id" component={Produto} />
      <ProtectedRoute exact path="/dados-empresa/veiculo" component={Veiculo} />
      <ProtectedRoute exact path="/dados-empresa/veiculo/:action/:id" component={Veiculo} />
    </ParentCrudContext.Provider>
  )
})

