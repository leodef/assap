import React, { useMemo, useCallback } from 'react'
import {
  Produto as ProdutoComponent
} from '../../../../components/Modulo/DadosEmpresa/Produto/Produto'
import { crudType as types } from '../../../../types/API/Produto'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Produto.scss'

/**
 * Tela de gerenciameto de Produto
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Produto
 */
export const Produto = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.produto.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'produto'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <ProdutoComponent collection={collection} />
  </CrudContext.Provider>)
}
