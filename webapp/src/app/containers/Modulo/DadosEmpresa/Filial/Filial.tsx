import React, { useMemo, useCallback } from 'react'
import {
  Filial as FilialComponent
} from '../../../../components/Modulo/DadosEmpresa/Filial/Filial'
import {
  crudType as types
} from '../../../../types/API/Filial'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Filial.scss'

/**
 * Tela de gerenciameto de Filial
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Filial
 */
export const Filial = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.filial.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'filial'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <FilialComponent collection={collection} />
  </CrudContext.Provider>)
}
