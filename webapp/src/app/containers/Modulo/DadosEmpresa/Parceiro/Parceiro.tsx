import React, { useMemo, useCallback } from 'react'
import {
  Parceiro as ParceiroComponent
} from '../../../../components/Modulo/DadosEmpresa/Parceiro/Parceiro'
import { crudType as types } from '../../../../types/API/Parceiro/Parceiro'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Parceiro.scss'

/**
 * Tela de gerenciameto de Parceiro
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Parceiro
 */
export const Parceiro = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.parceiro.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'parceiro'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <ParceiroComponent collection={collection} />
  </CrudContext.Provider>)
}
