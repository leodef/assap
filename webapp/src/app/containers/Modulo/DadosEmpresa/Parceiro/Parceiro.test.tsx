/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Parceiro } from './Parceiro'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Parceiro />, div)
  ReactDOM.unmountComponentAtNode(div)
})
