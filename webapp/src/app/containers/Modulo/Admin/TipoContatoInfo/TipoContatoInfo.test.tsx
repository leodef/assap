/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { TipoContatoInfo } from './TipoContatoInfo'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<TipoContatoInfo />, div)
  ReactDOM.unmountComponentAtNode(div)
})
