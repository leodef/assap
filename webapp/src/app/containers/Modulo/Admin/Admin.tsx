import React from 'react'
import { ProtectedRoute } from '../../../components/Shared/App/ProtectedRoute/ProtectedRoute'
import { Empresa } from './Empresa/Empresa'
import { Grandeza } from './Grandeza/Grandeza'
import { TipoContatoInfo } from './TipoContatoInfo/TipoContatoInfo'
import { UnidadeMedida } from './UnidadeMedida/UnidadeMedida'
import './Admin.scss'

/**
 * Roteamento para telas do modulo admin
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com roteamento para telas do modulo admin
 */
export const Admin = React.memo((props: any) => {
  Admin.displayName = 'Admin'
  return (
    <React.Fragment>
      <ProtectedRoute exact path="/admin/empresa" component={Empresa} />
      <ProtectedRoute exact path="/admin/empresa/:action/:id" component={Empresa} />
      <ProtectedRoute exact path="/admin/grandeza" component={Grandeza} />
      <ProtectedRoute exact path="/admin/grandeza/:action/:id" component={Grandeza} />
      <ProtectedRoute exact path="/admin/tipo-contato-info" component={TipoContatoInfo} />
      <ProtectedRoute exact path="/admin/tipo-contato-info/:action/:id" component={TipoContatoInfo} />
      <ProtectedRoute exact path="/admin/unidade-medida" component={UnidadeMedida} />
      <ProtectedRoute exact path="/admin/unidade-medida/:action/:id" component={UnidadeMedida} />
    </React.Fragment>
  )
})
