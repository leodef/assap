import React, { useMemo, useCallback } from 'react'
import { Grandeza as GrandezaComponent } from '../../../../components/Modulo/Admin/Grandeza/Grandeza'
import { crudType as types } from '../../../../types/API/Grandeza'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Grandeza.scss'

/**
 * Tela de gerenciameto de Grandeza
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Grandeza
 */
export const Grandeza = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.grandeza.crud, [])
  const actions = React.useMemo(() => { return { unidadeMedida: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'grandeza'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <GrandezaComponent collection={collection} />
  </CrudContext.Provider>)
}
