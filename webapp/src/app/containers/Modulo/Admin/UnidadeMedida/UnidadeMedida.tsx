import React, { useMemo, useCallback } from 'react'
import { UnidadeMedida as UnidadeMedidaComponent } from '../../../../components/Modulo/Admin/UnidadeMedida/UnidadeMedida'
import { crudType as types } from '../../../../types/API/UnidadeMedida'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './UnidadeMedida.scss'

/**
 * Tela de gerenciameto de UnidadeMedida
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de UnidadeMedida
 */
export const UnidadeMedida = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.unidadeMedida.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'unidadeMedida'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <UnidadeMedidaComponent collection={collection} />
  </CrudContext.Provider>)
}
