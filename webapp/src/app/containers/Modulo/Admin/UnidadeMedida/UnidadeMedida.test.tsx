/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { UnidadeMedida } from './UnidadeMedida'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<UnidadeMedida />, div)
  ReactDOM.unmountComponentAtNode(div)
})
