import React, { useMemo, useCallback } from 'react'
import { Empresa as EmpresaComponent } from '../../../../components/Modulo/Admin/Empresa/Empresa'
import { crudType as types } from '../../../../types/API/Empresa/Empresa'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Empresa.scss'

/**
 * Tela de gerenciameto de Empresa
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Empresa
 */
export const Empresa = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.empresa.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'empresa'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <EmpresaComponent collection={collection} />
  </CrudContext.Provider>)
}
