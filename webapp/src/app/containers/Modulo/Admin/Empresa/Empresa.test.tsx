/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Empresa } from './Empresa'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Empresa />, div)
  ReactDOM.unmountComponentAtNode(div)
})
