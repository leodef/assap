import {
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  makeStyles,
  MenuItem,
  Paper,
  Select,
  Theme,
  withStyles
} from "@material-ui/core";
import {
  RvHookup as EntregaIcon, // Entrega
  People as FuncionarioIcon, // Funcionarios
  LocalShipping as VeiculoIcon, // Veiculos
  More as SpeedDialIcon
} from '@material-ui/icons';

import moment from "moment";
import React,
{
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState
} from "react";
import {
  useDispatch,
  useSelector
} from "react-redux";
import {
  AppointmentModel,
  ViewState
} from "@devexpress/dx-react-scheduler";
// import {
//   pink, purple, teal, amber, deepOrange
// } from '@material-ui/core/colors'
import {
  Scheduler,
  DayView,
  WeekView,
  MonthView,
  Appointments,
  AppointmentTooltip,
  // AppointmentForm,
  Toolbar,
  ViewSwitcher,
  DateNavigator,
  TodayButton,
  CurrentTimeIndicator,
  AllDayPanel,
  Resources,
} from "@devexpress/dx-react-scheduler-material-ui";
import {
  crudType,
  mainResourceName as defaultMainResourceName,
  compromissoToSchedulerItem,
  getResources
} from "../../../types/API/Calendario/Compromisso";
import {
  crudType as crudTypeVeiculo,
} from "../../../types/API/Veiculo";
import {
  crudType as crudTypeEntrega,
} from "../../../types/API/Logistica/Entrega";
import {
  crudType as crudTypeFuncionario,
} from "../../../types/API/Funcionario";
import { 
  dateFormatApi
} from "../../../types/Config";
import "./Calendario.scss";
import {
  getParentContextValue,
  Parent,
  ParentCrudContext
} from "../../../contexts/ParentCrudContext";
import { ActionTypeEnum } from "../../../types/Crud";
import { SpeedDial, SpeedDialAction } from "@material-ui/lab";
import { NavService } from "../../../services/Nav";
import { push } from "connected-react-router";

/**
 * Calendário
 * @param {any}  props Propriedades
 * @return {React.Component} Componente
 */

export const EmpresaCalendario = React.memo((props) => {
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.empresaUsuario, [])
  const { item } = useSelector((state: any) => getState(state));
  const action = ActionTypeEnum.LIST
  const prefix = "empresa"
  const parentValue = useMemo(
    () =>
      new Parent(
        item, // item
        action,
        prefix // prefix
      ),
    [item, action, prefix]
  );

  const historyValue = useContext(ParentCrudContext);

  const parentCrudContextValue = useMemo(
    () => getParentContextValue(parentValue, historyValue),
    [parentValue, historyValue]
  );

  return (<ParentCrudContext.Provider value={parentCrudContextValue}>
      <Calendario {...props} />
    </ParentCrudContext.Provider>)
})

export const Calendario = React.memo((props: any) => {
  Calendario.displayName = "Calendario";
  const parentCrudContextValue = useContext(ParentCrudContext)
  const currentDate = moment().format(dateFormatApi); // '2018-11-01'
  const locale = useMemo(() => "pt-BR", []);
  const items = useSelector((state: any) =>
    state.modulo.calendario.compromisso.crud.items)
  const appointments = useMemo(
    () => compromissoToSchedulerItem(items) as Array<AppointmentModel>
  , [items])
  // dispatch
  const dispatch = useDispatch();
  // Metodo de consulta
  const fetch = useCallback(
    (params?: any) =>
      dispatch({
        type: crudType.FETCH_ITEM,
        payload: ({
          parent: parentCrudContextValue
        }),
      }),
    [dispatch, parentCrudContextValue]
  );

  const fetchVeiculo = useCallback(
    (params?: any) =>
      dispatch({
        type: crudTypeVeiculo.FETCH_OPTIONS_ITEM,
        payload: ({
          fields: ['titulo'],
          parent: parentCrudContextValue
        }),
      }),
    [dispatch, parentCrudContextValue]
  );

  const fetchEntrega = useCallback(
    (params?: any) =>
      dispatch({
        type: crudTypeEntrega.FETCH_OPTIONS_ITEM,
        payload: ({
          fields: ['titulo'],
          parent: parentCrudContextValue
        }),
      }),
    [dispatch, parentCrudContextValue]
  );

  const fetchFuncionario = useCallback(
    (params?: any) =>
      dispatch({
        type: crudTypeFuncionario.FETCH_OPTIONS_ITEM,
        payload: ({
          fields: ['nome'],
          parent: parentCrudContextValue
        }),
      }),
    [dispatch, parentCrudContextValue]
  );

  const veiculos = useSelector((state: any) =>
  state.modulo.dadosEmpresa.veiculo.options.items)
  const entregas = useSelector((state: any) =>
  state.modulo.dadosEmpresa.entrega.options.items)
  const funcionarios = useSelector((state: any) =>
  state.modulo.dadosEmpresa.funcionario.options.items)

  useEffect(() => {
    fetch();
    fetchVeiculo();
    fetchEntrega();
    fetchFuncionario();
  }, [
    fetch,
    fetchVeiculo,
    fetchEntrega,
    fetchFuncionario
  ]);


  const resources = useMemo( ()=> getResources({
    veiculos,
    entregas,
    funcionarios
  } as any), [
    veiculos,
    entregas,
    funcionarios
  ])

  const [mainResourceName, setMainResourceName] = useState(defaultMainResourceName)

  // https://codesandbox.io/s/ed1g3?file=/demo.js:2024-2187
  return (<React.Fragment>
    <ResourceSwitcher
      resources={resources}
      mainResourceName={mainResourceName}
      onChange={setMainResourceName}
    />

    <Paper>
      <Scheduler
        data={appointments}
        locale={locale}>
        <ViewState
          defaultCurrentDate={currentDate}
          defaultCurrentViewName="Week"
        />
        <DayView
          displayName="Visualizar dia"
          cellDuration={60} />
        <WeekView
          displayName="Visualizar semana"
          cellDuration={60} />
        <MonthView
          displayName="Visualizar mês" />
        <AllDayPanel
          messages={{allDay: 'Dia todo'}} />
        <Toolbar />
        <DateNavigator />
        <TodayButton
          messages={{ today: "Hoje" }} />
        <ViewSwitcher />
        <Appointments />
        <AppointmentTooltip headerComponent={
          (headerProps) => <Header
            {...headerProps}
            entregas={entregas}
            veiculos={veiculos}
            funcionarios={funcionarios}/>
        }/>
        <Resources data={resources} mainResourceName={mainResourceName} />
        {/* https://devexpress.github.io/devextreme-reactive/react/scheduler/docs/reference/resources/#resource */}
        <CurrentTimeIndicator shadePreviousCells={true} />
      </Scheduler>
    </Paper>
  </React.Fragment>);
});

export const ResourceSwitcher = React.memo((params: any) => {
  ResourceSwitcher.displayName = 'ResourceSwitcher'
  const {mainResourceName, onChange, resources} = params
    return (<Grid container spacing={1} justify="flex-end">
      <Grid item xs={4} >
        <FormControl fullWidth>
          <InputLabel>Recursos: </InputLabel>
          <Select
            fullWidth
            value={mainResourceName}
            onChange={e => onChange(e.target.value)}
            label="Recursos:"
          >
            {resources.map( (resource: any) => (
              <MenuItem key={resource.fieldName} value={resource.fieldName}>
                {resource.title}
              </MenuItem>
            ))}
        </Select>
      </FormControl>
    </Grid>
</Grid>)
})



export const Header = React.memo((params: any) => {
  Header.displayName = 'Header'
  const classes = useStylesHeader();
  const {
    children,
    appointmentData,
    entregas,
    veiculos,
    funcionarios,
    ...restProps
  } = params
  const [open, setOpen] = React.useState(false);

  // dispatch
  const dispatch = useDispatch();

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClick = (action: any) => {
    handleClose();
    dispatch(push(action.to))
  };

  const actions = [
    ...(appointmentData.entrega ?
      [appointmentData.entrega]
        .map( (id: any) => entregas.find((obj: any) => obj._id === id))
        .filter((val: any) => Boolean(val))
        .map( (entrega: any) => ({
          id: entrega._id,
          icon: (<EntregaIcon />),
          name: entrega.titulo,
          to: `/dados-empresa/entrega/${ActionTypeEnum.EDIT}/${entrega._id}`
        })) : []),
      ...(appointmentData.veiculos ? appointmentData.veiculos
          .map((id: any) => veiculos.find((obj: any) => obj._id === id))
          .filter((val: any) => Boolean(val))
          .map( (veiculo: any) => ({
        id: veiculo._id,
        icon: (<VeiculoIcon />),
        name: veiculo.titulo,
        to: `/dados-empresa/veiculo/${ActionTypeEnum.EDIT}/${veiculo._id}`
      })) : []),
      ...(appointmentData.funcionarios ? appointmentData.funcionarios
        .map((id: any) => funcionarios.find((obj: any) => obj._id === id))
        .filter((val: any) => Boolean(val))
        .map( (funcionario: any) => ({
        id: funcionario._id,
        icon: (<FuncionarioIcon />),
        name: funcionario.nome,
        to: `/dados-empresa/funcionario/${ActionTypeEnum.EDIT}/${funcionario._id}`
      })) : [])
  ]
  
  return(
  <AppointmentTooltip.Header
    {...restProps}
    appointmentData={appointmentData}
  >
     <SpeedDial
      ariaLabel="SpeedDial tooltip example"
      icon={<SpeedDialIcon />}
      onClose={handleClose}
      onOpen={handleOpen}
      open={open}
      direction="left"
      className={classes.speedDial}
    >
      {actions.map((action: any) => (
        <SpeedDialAction
          key={action.id}
          icon={action.icon}
          tooltipTitle={(action.name ? action.name.substr(0, 40) : null)}
          tooltipPlacement="bottom-end"
          onClick={() => handleClick(action)}
        />
      ))}
    </SpeedDial>
  </AppointmentTooltip.Header>
)
})

const useStylesHeader = makeStyles((theme: Theme) => ({
  root: {
    height: 380,
    transform: 'translateZ(0px)',
    flexGrow: 1,
  },
  speedDial: {
    //position: 'absolute',
    zIndex: 99999,
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
}));