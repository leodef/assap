import React from 'react'
import { ProtectedRoute } from '../../../components/Shared/App/ProtectedRoute/ProtectedRoute'
import { Entrega } from '../DadosEmpresa/Entrega/Entrega'
import './Logistica.scss'

/**
 * Roteamento para telas do modulo admin
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com roteamento para telas do modulo admin
 */
export const Logistica = React.memo((props: any) => {
  Logistica.displayName = 'Logistica'
  return (
    <React.Fragment>
      <ProtectedRoute exact path="/logistica/entrega" component={Entrega} />
    </React.Fragment>
  )
})
