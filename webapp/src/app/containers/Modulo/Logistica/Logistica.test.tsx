/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
/* eslint-disable no-undef */
import { Entrega } from './Logistica'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Entrega />, div)
  ReactDOM.unmountComponentAtNode(div)
})
