/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Modulo } from './Modulo'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Modulo />, div)
  ReactDOM.unmountComponentAtNode(div)
})
