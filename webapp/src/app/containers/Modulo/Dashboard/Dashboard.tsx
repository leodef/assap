import React from 'react'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core/styles'
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography
} from '@material-ui/core'
import {
  ExpandMore as ExpandMoreIcon
} from '@material-ui/icons/'
import { AdminGrid } from '../../../components/Modulo/Dashboard/AdminGrid/AdminGrid'
// import { CalcCompQuimGrid } from '../../../components/Modulo/Dashboard/CalcCompQuimGrid/CalcCompQuimGrid'
import { DadosEmpresaGrid } from '../../../components/Modulo/Dashboard/DadosEmpresaGrid/DadosEmpresaGrid'
import './Dashboard.scss'

const panelUseStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%'
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular
    }
  })
)

/**
 * Painel de controle para modulos
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com painel de controle para modulos
 */
export const Dashboard = React.memo((props: any) => {
  Dashboard.displayName = 'Dashboard'
  const classes = panelUseStyles()

  return (
    <div className={classes.root}>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>
            Área Administrativa
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <AdminGrid />
        </AccordionDetails>
      </Accordion>
      {/*<Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>
            Calculo de composição quimica
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <CalcCompQuimGrid />
        </AccordionDetails>
      </Accordion>
      */}
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>
            Dados da Empresa
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <DadosEmpresaGrid />
        </AccordionDetails>
      </Accordion>
    </div>
  )
})
