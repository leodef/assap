import React from 'react'
import { Switch } from 'react-router-dom'
import { ProtectedRoute } from '../../components/Shared/App/ProtectedRoute/ProtectedRoute'
import { Dashboard } from './Dashboard/Dashboard'
import { Navbar } from '../../components/Modulo/Navbar/Navbar'
import { Sidebar } from '../../components/Modulo/Sidebar/Sidebar'
import { Admin } from './Admin/Admin'
import { CalcCompQuim } from './CalcCompQuim/CalcCompQuim'
import { EmpresaCalendario } from './Calendario/Calendario'
import { DadosEmpresa } from './DadosEmpresa/DadosEmpresa'
import './Modulo.scss'

/**
 * Roteamento para modulos do sistema
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com roteamento para modulos do sistema
 */
export const Modulo = React.memo((props: any) => {
  Modulo.displayName = 'Modulo'
  return (
    <React.Fragment>
      <Sidebar />
      <main>
        <Navbar />
        <Switch>
          <ProtectedRoute exact path="/" component={Dashboard} />
          <ProtectedRoute path="/dashboard" component={Dashboard} />
          <ProtectedRoute path="/calendario" component={EmpresaCalendario} />
          <React.Fragment>
            <Admin />
            <CalcCompQuim />
            <DadosEmpresa />
          </React.Fragment>
        </Switch>
      </main>
    </React.Fragment>
  )
})
export default Modulo
