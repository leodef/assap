import React from 'react'
import { ProtectedRoute } from '../../../components/Shared/App/ProtectedRoute/ProtectedRoute'
import { Elemento } from './Elemento/Elemento'
import { Substancia } from './Substancia/Substancia'
import { QuantidadeElemento } from './QuantidadeElemento/QuantidadeElemento'
import { ComposicaoSubstancia } from './ComposicaoSubstancia/ComposicaoSubstancia'
import { QuantidadeSubstancia } from './QuantidadeSubstancia/QuantidadeSubstancia'
import './CalcCompQuim.scss'

export const CalcCompQuim = React.memo((props: any) => {
  CalcCompQuim.displayName = 'CalcCompQuim'
  return (
    <React.Fragment>
      <ProtectedRoute exact path="/calc-comp-quim/elemento" component={Elemento} />
      <ProtectedRoute exact path="/calc-comp-quim/substancia" component={Substancia} />
      <ProtectedRoute exact path="/calc-comp-quim/quantidade-elemento" component={QuantidadeElemento} />
      <ProtectedRoute exact path="/calc-comp-quim/composicao-substancia" component={ComposicaoSubstancia} />
      <ProtectedRoute exact path="/calc-comp-quim/quantidade-substancia" component={QuantidadeSubstancia} />
    </React.Fragment>
  )
})
