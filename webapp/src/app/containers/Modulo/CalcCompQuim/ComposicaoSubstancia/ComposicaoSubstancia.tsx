import React, { useMemo, useCallback } from 'react'
import { ComposicaoSubstancia as ComposicaoSubstanciaComponent } from '../../../../components/Modulo/CalcCompQuim/ComposicaoSubstancia/ComposicaoSubstancia'
import { crudType as types } from '../../../../types/API/CalcCompQuim/ComposicaoSubstancia'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './ComposicaoSubstancia.scss'

/**
 * Tela de gerenciameto de ComposicaoSubstancia
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de ComposicaoSubstancia
 */
export const ComposicaoSubstancia = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.calcCompQuim.composicaoSubstancia.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'grandeza'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <ComposicaoSubstanciaComponent collection={collection} />
  </CrudContext.Provider>)
}
