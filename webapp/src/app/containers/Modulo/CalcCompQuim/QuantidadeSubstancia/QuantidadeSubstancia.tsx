import React, { useMemo, useCallback } from 'react'
import { QuantidadeSubstancia as QuantidadeSubstanciaComponent } from '../../../../components/Modulo/CalcCompQuim/QuantidadeSubstancia/QuantidadeSubstancia'
import { crudType as types } from '../../../../types/API/CalcCompQuim/QuantidadeSubstancia'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './QuantidadeSubstancia.scss'

/**
 * Tela de gerenciameto de QuantidadeSubstancia
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de QuantidadeSubstancia
 */
export const QuantidadeSubstancia = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.calcCompQuim.quantidadeSubstancia.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'grandeza'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <QuantidadeSubstanciaComponent collection={collection} />
  </CrudContext.Provider>)
}
