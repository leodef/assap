/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { QuantidadeSubstancia } from './QuantidadeSubstancia'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<QuantidadeSubstancia />, div)
  ReactDOM.unmountComponentAtNode(div)
})
