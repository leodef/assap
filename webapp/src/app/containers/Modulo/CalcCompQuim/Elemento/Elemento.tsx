import React, { useMemo, useCallback } from 'react'
import { Elemento as ElementoComponent } from '../../../../components/Modulo/CalcCompQuim/Elemento/Elemento'
import { crudType as types } from '../../../../types/API/CalcCompQuim/Elemento'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Elemento.scss'

/**
 * Tela de gerenciameto de Elemento
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Elemento
 */
export const Elemento = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.calcCompQuim.elemento.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'grandeza'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <ElementoComponent collection={collection} />
  </CrudContext.Provider>)
}
