/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { CalcCompQuim } from './CalcCompQuim'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<CalcCompQuim />, div)
  ReactDOM.unmountComponentAtNode(div)
})
