/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Substancia } from './Substancia'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Substancia />, div)
  ReactDOM.unmountComponentAtNode(div)
})
