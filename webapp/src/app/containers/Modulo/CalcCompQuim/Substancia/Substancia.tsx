import React, { useMemo, useCallback } from 'react'
import { Substancia as SubstanciaComponent } from '../../../../components/Modulo/CalcCompQuim/Substancia/Substancia'
import { crudType as types } from '../../../../types/API/CalcCompQuim/Substancia'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Substancia.scss'

/**
 * Tela de gerenciameto de Substancia
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Substancia
 */
export const Substancia = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.calcCompQuim.substancia.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'grandeza'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <SubstanciaComponent collection={collection} />
  </CrudContext.Provider>)
}
