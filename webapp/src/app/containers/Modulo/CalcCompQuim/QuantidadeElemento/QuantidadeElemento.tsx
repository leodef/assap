import React, { useMemo, useCallback } from 'react'
import { QuantidadeElemento as QuantidadeElementoComponent } from '../../../../components/Modulo/CalcCompQuim/QuantidadeElemento/QuantidadeElemento'
import { crudType as types } from '../../../../types/API/CalcCompQuim/QuantidadeElemento'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './QuantidadeElemento.scss'

/**
 * Tela de gerenciameto de QuantidadeElemento
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de QuantidadeElemento
 */
export const QuantidadeElemento = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.calcCompQuim.quantidadeElemento.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'grandeza'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 3 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <QuantidadeElementoComponent collection={collection} />
  </CrudContext.Provider>)
}
