/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { QuantidadeElemento } from './QuantidadeElemento'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<QuantidadeElemento />, div)
  ReactDOM.unmountComponentAtNode(div)
})
