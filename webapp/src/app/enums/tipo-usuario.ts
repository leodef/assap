/**
 * Enum de opções de tipo de usuário
 * 
 * @enum
 * @mame TIPO_USUARIO
 */
/* eslint-disable no-unused-vars */
export enum TIPO_USUARIO {
  ADMIN = 'ADMIN',
  ADMIN_EMPRESA = 'ADMIN_EMPRESA',
  FUNCIONARIO = 'FUNCIONARIO',
  PUBLICO = 'PUBLICO'
}

export enum TIPO_USUARIO_FUNCIONARIO {
  ADMIN_EMPRESA = 'ADMIN_EMPRESA',
  FUNCIONARIO = 'FUNCIONARIO'
}

export enum TIPO_USUARIO_LABEL {
  ADMIN = 'Administrador',
  ADMIN_EMPRESA = 'Administrador da empresa',
  FUNCIONARIO = 'Funcionário da empresa',
  PUBLICO = 'Sem acesso ao sistema'
}
export enum TIPO_USUARIO_FUNCIONARIO_LABEL {
  ADMIN_EMPRESA = 'Administrador da empresa',
  FUNCIONARIO = 'Funcionário da empresa'
}