
/**
 * Enum de opções de tipo de função do veiculo
 * 
 * @enum
 * @mame TIPO_FUNCAO_VEICULO
 */
/* eslint-disable no-unused-vars */
export enum TIPO_FUNCAO_VEICULO {
  CAVALO = 'CAVALO',
  CARRETA = 'CARRETA'
}

export enum TIPO_FUNCAO_VEICULO_LABEL {
  CAVALO = 'Caminhão',
  CARRETA = 'Carreta'
}

