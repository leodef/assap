
/**
 * Enum de opções de tipo de pessoa
 * 
 * @enum
 * @mame TIPO_PESSOA
 */
/* eslint-disable no-unused-vars */
export enum TIPO_PESSOA {
  FISICA = 'FISICA',
  JURIDICA = 'JURIDICA'
}

export enum TIPO_PESSOA_LABEL {
  FISICA = 'Física',
  JURIDICA = 'Jurídica'
}
