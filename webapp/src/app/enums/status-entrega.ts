
/**
 * Enum de opções de status de entrega
 * 
 * @enum
 * @mame STATUS_ENTREGA
 */
/* eslint-disable no-unused-vars */
export enum STATUS_ENTREGA {
  PENDENTE = 'PENDENTE',
  INICIADO = 'INICIADO',
  FINALIZADO = 'FINALIZADO'
}
export enum STATUS_ENTREGA_LABEL {
  PENDENTE = 'Pendente',
  INICIADO = 'Iniciado',
  FINALIZADO = 'Finalizado'
}
