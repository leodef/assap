
/**
 * Enum de opções de tipo de ligação do funcionário com a entrega
 * 
 * @enum
 * @mame TIPO_FUNCIONARIO_ENTREGA
 */
/* eslint-disable no-unused-vars */
export enum TIPO_FUNCIONARIO_ENTREGA {
  VENDEDOR = 'VENDEDOR',
  MOTORISTA = 'MOTORISTA',
  AUXILIAR = 'AUXILIAR',
  GERAL = 'GERAL'
}

export enum TIPO_FUNCIONARIO_ENTREGA_LABEL {
  VENDEDOR = 'Vendedor',
  MOTORISTA = 'Motorista',
  AUXILIAR = 'Auxiliar',
  GERAL = 'Geral'
}