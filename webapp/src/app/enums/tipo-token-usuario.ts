/**
 * Enum de opções de tipo de token de usuario
 * 
 * @enum
 * @mame TIPO_TOKEN_USUARIO
 */
/* eslint-disable no-unused-vars */
export enum TIPO_TOKEN_USUARIO {
  RESETAR_SENHA = 'RESETAR_SENHA',
  CONFIRMAR_CONTA = 'CONFIRMAR_CONTA'
}

export enum TIPO_TOKEN_USUARIO_LABEL {
  RESETAR_SENHA = 'Resetar senha',
  CONFIRMAR_CONTA = 'Confirmar conta'
}