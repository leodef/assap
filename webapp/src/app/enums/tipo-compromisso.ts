
/**
 * Enum de opções de tipo de compromisso
 * 
 * @enum
 * @mame TIPO_COMPROMISSO
 */
/* eslint-disable no-unused-vars */
export enum TIPO_COMPROMISSO {
  geral = 'geral',
  entrega = 'entrega' 
}

/**
 * Enum de opções de tipo de texto para o compromisso
 * 
 * @enum
 * @mame TIPO_COMPROMISSO_LABEL
 */
export enum TIPO_COMPROMISSO_LABEL {
  geral = 'Compromisso',
  entrega = 'Entrega' 
}