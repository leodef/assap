
/**
 * Enum de opções de tipo de formação do valor / origem
 * 
 * @enum
 * @mame TIPO_FORMACAO
 */
/* eslint-disable no-unused-vars */
export enum TIPO_FORMACAO {
  MANUAL = 'MANUAL',
  AUTOMATIZADO = 'AUTOMATIZADO',
  INDEFINIDO = 'INDEFINIDO' 
}
export enum TIPO_FORMACAO_LABEL {
  MANUAL = 'Manual',
  AUTOMATIZADO = 'Automatizado',
  INDEFINIDO = 'Indefinido' 
}
