
/**
 * Enum de opções de algoritimo de rota
 * 
 * @enum
 * @mame ALGORITIMO_ROTA
 */
/* eslint-disable no-unused-vars */
export enum ALGORITIMO_ROTA {
  nearestNeighbor = 'nearestNeighbor',
  naive = 'naive'
}
export enum ALGORITIMO_ROTA_LABEL {
  nearestNeighbor = 'Vizinho próximo',
  naive = 'Força bruta'
}
