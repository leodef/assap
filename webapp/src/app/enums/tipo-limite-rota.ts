
/**
 * Enum de opções de tipo de limite de rota
 * 
 * @enum
 * @mame TIPO_LIMITE_ROTA
 */
/* eslint-disable no-unused-vars */
export enum TIPO_LIMITE_ROTA {
  MANUAL = 'MANUAL',
  TRANSPORTE = 'TRANSPORTE',
  INDEFINIDO = 'INDEFINIDO'
}

export enum TIPO_LIMITE_ROTA_LABEL {
  MANUAL = 'Manual',
  TRANSPORTE = 'Transporte',
  INDEFINIDO = 'Indefinido'
}
