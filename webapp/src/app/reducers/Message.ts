import { MessageType } from '../types/Message'

export const initialState: any = {
  messages: []
}

export default function reduce (state = initialState, action: any) {
  switch (action.type) {
    case MessageType.ADD_MESSAGE: {
      return { messages: [...state.messages, action.payload] }
    }
    case MessageType.REMOVE_MESSAGE:
      // ##!!
      // const messages = state.messages.filter( (message: any) => action.payload.id !== message.id)
      return { messages: [] }
    default:
      return state
  }
}
