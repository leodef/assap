import { AuthType } from '../types/Auth'

// AuthType.AUTH_PENDING
// AuthType.AUTH_SUCCESS
// AuthType.AUTH_FAILURE

export default function reduce (state = {
  error: null,
  loading: true,
  token: null,
  usuario: null,
  funcionario: null
}, action: any) {
  switch (action.type) {
    case AuthType.LOAD:
    case AuthType.AUTH:
      return state // saga actions
    case AuthType.AUTH_PENDING:
      return {
        ...state,
        error: null,
        loading: true,
        token: null,
        user: null
      }
    case AuthType.LOAD_SUCCESS:
    case AuthType.AUTH_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
        token: (action.payload ? action.payload.token : null),
        user: (action.payload ? action.payload.user : null)
      }
      // FAILURE
    case AuthType.AUTH_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false,
        token: null,
        user: null
      }
    case AuthType.LOGOUT:
      return {
        ...state,
        loading: false,
        token: null,
        user: null
      }
    default:
      return state
  }
}
