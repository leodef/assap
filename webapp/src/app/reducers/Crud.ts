import { CrudType, ActionTypeEnum } from '../types/Crud'

export interface CrudReducerState {
  items: Array<any>;
  item: any;
  filter: any;
  pagination: any;
  fields: any;
  sort: any;
  action: ActionTypeEnum;
  loading: boolean;
  error: string | null;
  fetchLoading: boolean;
  updateLoading: boolean;
  createLoading: boolean;
  deleteLoading: boolean;
  findLoading: boolean;
  fetched: boolean;
}
export const initialState: CrudReducerState = {
  items: [],
  item: null,
  filter: null,
  pagination: null,
  fields: null,
  sort: null,
  action: ActionTypeEnum.LIST,
  loading: false,
  error: null,
  fetchLoading: false,
  updateLoading: false,
  createLoading: false,
  deleteLoading: false,
  findLoading: false,
  fetched: false
}

export class CrudReducer {
  config: CrudType = new CrudType()
  constructor (config: CrudType = new CrudType()) {
    this.config = config
  }

  static getReduce (config?: CrudType) {
    const val = new CrudReducer(config)
    return val.getReduce()
  }

  getReduce () {
    return this.reduce.bind(this)
  }

  reduce (state: CrudReducerState = initialState, action: any): CrudReducerState {
    const config = this.config
    const actionType = action.type
    switch (actionType) {
      case config.CLEAR_ITEM:
        return initialState
      case config.SET_ACTION_ITEM:
        return {
          ...state,
          action: (
            (
              !action.payload.action &&
              action.payload.action !== 0
            )
              ? state.action
              : action.payload.action),
          item: action.payload.item
        }
      case config.SET_PAGINATION_ITEM:
        return {
          ...state,
          pagination: action.payload.pagination
        }
      case config.SET_FILTER_ITEM:
        return {
          ...state,
          filter: action.payload.filter
        }
      case config.SET_FIELDS_ITEM:
        return {
          ...state,
          fields: action.payload.fields
        }
      case config.SET_SORT_ITEM:
        return {
          ...state,
          sort: action.payload.sort
        }
      // FETCH CREATE DELETE UPDATE
      case config.FIND_ITEM:
      case config.FETCH_ITEM:
      case config.CREATE_ITEM:
      case config.DELETE_ITEM:
      case config.UPDATE_ITEM:
        return state // saga actions
      // PENDING - FETCH CREATE DELETE UPDATE
      case config.FIND_ITEM_PENDING:
        return {
          ...state,
          error: null,
          loading: true,
          findLoading: true
        }
      case config.FETCH_ITEM_PENDING:
        return {
          ...state,
          error: null,
          loading: true,
          fetchLoading: true
        }
      case config.DELETE_ITEM_PENDING:
        return {
          ...state,
          error: null,
          loading: true,
          deleteLoading: true
        }
      case config.CREATE_ITEM_PENDING:
        return {
          ...state,
          error: null,
          loading: true,
          createLoading: true
        }
      case config.UPDATE_ITEM_PENDING:
        return {
          ...state,
          error: null,
          loading: true,
          updateLoading: true
        }
      // SUCCESS - FETCH CREATE DELETE UPDATE
      case config.FIND_ITEM_SUCCESS:
        return {
          ...state,
          item: (action.payload.item || state.item),
          error: null,
          loading: false,
          findLoading: false
        }
      case config.FETCH_ITEM_SUCCESS:
        return {
          ...state,
          items: (action.payload.items || []),
          pagination: (action.payload.pagination || state.pagination),
          error: null,
          loading: false,
          fetchLoading: false,
          fetched: true
        }
      case config.CREATE_ITEM_SUCCESS:
        return {
          ...state,
          error: null,
          loading: false,
          createLoading: false
        }
      case config.DELETE_ITEM_SUCCESS:
        return {
          ...state,
          error: null,
          loading: false,
          deleteLoading: false
        }
      case config.UPDATE_ITEM_SUCCESS:
        return {
          ...state,
          error: null,
          loading: false,
          updateLoading: false
        }
      // FAILURE - FETCH CREATE DELETE UPDATE
      case config.FIND_ITEM_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          loading: false,
          findLoading: false
        }
      case config.FETCH_ITEM_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          loading: false,
          fetchLoading: false
        }
      case config.CREATE_ITEM_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          loading: false,
          createLoading: false
        }
      case config.DELETE_ITEM_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          loading: false,
          deleteLoading: false
        }
      case config.UPDATE_ITEM_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          loading: false,
          updateLoading: false
        }
      default:
        return state
    }
  }
}

export interface CrudPropsActions {
  set: (item: any) => void;
  save: (item: any, id?: string) => void;
  create: (item: any) => void;
  remove: (item: any, id?: string) => void;
  update: (item: any, id?: string) => void;
  fetch: (filter: any) => void;
}

export interface CrudProps extends CrudReducerState, CrudPropsActions {
  match?: any;
  auth?: any;
}

export const getMapStateToProps: (state: any) => any = (getState: (state: any) => any) => {
  return (state: any) => {
    const config = getState ? getState(state) : state
    return config
  }
}

export const getMapDispatchToProps: (dispatch: any) => any = (config: CrudType) => {
  return (dispatch: any) => {
    return getDispatchs(dispatch, config)
  }
}

export function getDispatchs (dispatch: any, config: CrudType) {
  const obj = {
    set: (item: any) => dispatch({ type: config.SET_ACTION_ITEM, payload: item }),
    save: (item: any, id?: string) => {
      id = id || item._id || item.id
      if (id) {
        obj.update(item, id)
      } else {
        obj.create(item)
      }
    },
    create: (item: any) => dispatch({ type: config.CREATE_ITEM, payload: item }),
    remove: (item: any, id?: string) => dispatch({ type: config.DELETE_ITEM, payload: item, id }),
    update: (item: any, id?: string) => dispatch({ type: config.UPDATE_ITEM, payload: item, id }),
    fetch: (filter: any) => dispatch({ type: config.FETCH_ITEM, payload: filter })
  }
  return obj
}
