import { combineReducers } from 'redux'

import entregaReducer from './Entrega'
import filialReducer from './Filial'
import funcionarioReducer from './Funcionario'
import parceiroReducer from './Parceiro'
import produtoReducer from './Produto'
import veiculoReducer from './Veiculo'
import empresaUsuarioReducer from './EmpresaUsuario'

const dadosEmpresaReducer = combineReducers({
  entrega: entregaReducer,
  filial: filialReducer,
  funcionario: funcionarioReducer,
  parceiro: parceiroReducer,
  produto: produtoReducer,
  veiculo: veiculoReducer,
  empresaUsuario: empresaUsuarioReducer
})

export default dadosEmpresaReducer
