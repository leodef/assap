import { EmpresaUsuarioType } from "../../../types/EmpresaUsuario"

export interface EmpresaUsuarioState {
    item: any,
    fix: boolean
    options: Array<any>,
    error: any,
    loading: boolean
}

export const initialState: EmpresaUsuarioState = {
    item: null as any,
    fix: false,
    options: [],
    error: null as any,
    loading: false
}

export default (state: EmpresaUsuarioState = initialState, action: any): EmpresaUsuarioState => {
  const actionType = action.type
  switch (actionType) {
    case EmpresaUsuarioType.SET:
      return {
        ...state,
        item: action.payload,
        fix: false
      }
    case EmpresaUsuarioType.SET_FIX:
      return {
        ...state,
        item: action.payload,
        fix: true
      }
    case EmpresaUsuarioType.CLEAR:
      return initialState
    case EmpresaUsuarioType.FETCH:
      return state // saga actions
    case EmpresaUsuarioType.FETCH_PENDING:
      return {
        ...state,
        error: null,
        loading: true
      }
    case EmpresaUsuarioType.FETCH_SUCCESS:
      return {
        ...state,
        options: (action.payload || []),
        error: null,
        loading: false
      }
    case EmpresaUsuarioType.FETCH_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false
      }
    default:
      return state
  }
}
