// eslint-disable-next-line no-unused-vars
import {
    CrudReducer,
    CrudReducerState,
    initialState as crudInitialState
} from '../../../Crud'
// eslint-disable-next-line no-unused-vars
import {
    crudType,
    EntregaCrudType
} from '../../../../types/API/Logistica/Entrega'

export interface EntregaCrudReducerState extends CrudReducerState {
  calculoRota: Boolean
  calculoRotaLoading: boolean;
}
export const initialState: EntregaCrudReducerState = {
  ...crudInitialState,
  calculoRota: false,
  calculoRotaLoading: false
}

export class EntregaCrudReducer extends CrudReducer {
  static getReduce () {
    const val = new EntregaCrudReducer()
    return val.getReduce()
  }

  constructor () {
    super(crudType)
  }

  getReduce () {
    return this.reduce.bind(this)
  }

  reduce (state: EntregaCrudReducerState = initialState, action: any): EntregaCrudReducerState {
    const config = this.config as EntregaCrudType
    const actionType = action.type
    const {
      calculoRotaLoading,
      ...crudState
    } = state
    switch (actionType) {
      case config.SET_ACTION_ITEM:
        return {
          ...state,
          action: (
            (
              !action.payload.action &&
              action.payload.action !== 0
            )
              ? state.action
              : action.payload.action),
          item: action.payload.item,
          calculoRota: false
        }
      case config.FIND_ITEM_SUCCESS:
        return {
          ...state,
          item: (action.payload.item || state.item),
          calculoRota: false,
          error: null,
          loading: false,
          findLoading: false
        }
      case config.CALCULO_ROTA:
        return state // saga actions
        // PENDING - FETCH CREATE DELETE UPDATE
      case config.CALCULO_ROTA_PENDING:
        return {
          ...state,
          error: null,
          loading: true,
          calculoRotaLoading: true
        }
        // SUCCESS - FETCH CREATE DELETE UPDATE
      case config.CALCULO_ROTA_SUCCESS:
        return {
          ...state,
          error: null,
          loading: false,
          item: (action.payload.item || state.item),
          calculoRota: true,
          calculoRotaLoading: false
        }
        // FAILURE - FETCH CREATE DELETE UPDATE
      case config.CALCULO_ROTA_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          loading: false,
          calculoRotaLoading: false,
          calculoRota: false
        }
      default:
        return {
          ...state,
          ...super.reduce(crudState as CrudReducerState, action)
        } as EntregaCrudReducerState
    }
  }
}

export default EntregaCrudReducer.getReduce()