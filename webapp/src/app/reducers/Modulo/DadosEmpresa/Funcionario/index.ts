import { combineReducers } from 'redux'

import optionsReducer from './Options'
import crudReducer from './Crud'

const dadosEmpresaReducer = combineReducers({
  options: optionsReducer,
  crud: crudReducer
})

export default dadosEmpresaReducer
