import { OptionsReducer } from '../../../Options'
import { crudType } from '../../../../types/API/CalcCompQuim/QuantidadeSubstancia'
export default OptionsReducer.getReduce(crudType)
