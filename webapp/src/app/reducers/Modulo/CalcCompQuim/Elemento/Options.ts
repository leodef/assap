import { OptionsReducer } from '../../../Options'
import { crudType } from '../../../../types/API/CalcCompQuim/Elemento'
export default OptionsReducer.getReduce(crudType)
