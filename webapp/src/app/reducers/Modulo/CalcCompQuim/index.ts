import { combineReducers } from 'redux'

import composicaoSubstanciaReducer from './ComposicaoSubstancia'
import elementoReducer from './Elemento'
import quantidadeElementoReducer from './QuantidadeElemento'
import quantidadeSubstanciaReducer from './QuantidadeSubstancia'
import substanciaReducer from './Substancia'

const adminReducer = combineReducers({
  composicaoSubstancia: composicaoSubstanciaReducer,
  elemento: elementoReducer,
  quantidadeElemento: quantidadeElementoReducer,
  quantidadeSubstancia: quantidadeSubstanciaReducer,
  substancia: substanciaReducer
})

export default adminReducer
