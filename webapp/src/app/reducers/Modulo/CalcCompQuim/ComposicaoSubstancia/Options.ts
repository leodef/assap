import { OptionsReducer } from '../../../Options'
import { crudType } from '../../../../types/API/CalcCompQuim/ComposicaoSubstancia'
export default OptionsReducer.getReduce(crudType)
