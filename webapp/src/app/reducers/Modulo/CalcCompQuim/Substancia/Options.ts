import { OptionsReducer } from '../../../Options'
import { crudType } from '../../../../types/API/CalcCompQuim/Substancia'
export default OptionsReducer.getReduce(crudType)
