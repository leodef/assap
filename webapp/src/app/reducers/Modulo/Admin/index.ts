import { combineReducers } from 'redux'

import empresaReducer from './Empresa'
import grandezaReducer from './Grandeza'
import produtoReducer from './Produto'
import tipoContatoInfoReducer from './TipoContatoInfo'
import unidadeMedidaReducer from './UnidadeMedida'

const adminReducer = combineReducers({
  empresa: empresaReducer,
  grandeza: grandezaReducer,
  produto: produtoReducer,
  tipoContatoInfo: tipoContatoInfoReducer,
  unidadeMedida: unidadeMedidaReducer
})

export default adminReducer
