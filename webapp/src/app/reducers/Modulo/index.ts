import { combineReducers } from 'redux'

import adminReducer from './Admin'
import calcCompQuimReducer from './CalcCompQuim'
import dadosEmpresaReducer from './DadosEmpresa'
import calendarioReducer from './Calendario'

const moduloReducer = combineReducers({
  admin: adminReducer,
  calcCompQuim: calcCompQuimReducer,
  dadosEmpresa: dadosEmpresaReducer,
  calendario: calendarioReducer
})

export default moduloReducer
