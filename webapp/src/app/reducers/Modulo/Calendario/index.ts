import { combineReducers } from 'redux'

import compromissoReducer from './Compromisso'

const adminReducer = combineReducers({
  compromisso: compromissoReducer
})

export default adminReducer
