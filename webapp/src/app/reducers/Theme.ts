import { ThemeType, ThemeValue } from '../types/Theme'

export const initialState: any = {
  value: ThemeValue.light
}

export default function reduce (state = initialState, action: any) {
  switch (action.type) {
    case ThemeType.SET:
    case ThemeType.LOAD_SUCCESS:
      return {
        value: action.payload || ThemeValue.light
      }
    case ThemeType.SET_DARK:
    case ThemeType.SET_LIGHT:
    case ThemeType.TOGGLE:
    case ThemeType.LOAD:
      return state
    default:
      return state
  }
}
