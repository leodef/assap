import React from 'react'
const collectionContextDefaultValue = {
  config: null as any,
  actions: null as any,
  filter: null as any,
  pagination: null as any,
  sort: null as any,
  setFilter: null as any,
  setPagination: null as any,
  setSort: null as any,
  types: null as any,
  items: [] as Array<any>,
  length: 0,
  loading: false
}
const CollectionContext = React.createContext(collectionContextDefaultValue)
//  Provider Consumer
export { CollectionContext }
