import React from 'react'

export class Parent {
  completePrefix: string = '';

  static load (parent: Parent, history: Array<Parent> = []) {
    return new Parent(
      parent.item,
      parent.action,
      parent.prefix,
      history
    )
  }

  constructor (
    public item: any,
    public action: any,
    public prefix: string = '',
    history: Array<Parent> = []) {
    this.completePrefix = this.loadPrefixHistory(prefix, history)
  }

  loadPrefixHistory (prefix: string, history: Array<Parent>) {
    return history
      .filter((item: Parent) => item && item.prefix !== prefix)
      .reduce((previous, current) =>
        previous.concat(current.prefix, '/').replace('//', '/')
      , '').concat(prefix)
  }
}

export class ParentCrudContextValue {
  parent: Parent | null = null;
  history: Array<Parent> = [];
  path: string = '';
}
const parentCrudContextValueDefault: ParentCrudContextValue = {
  parent: null as Parent | null,
  history: [] as Array<Parent>,
  path: ''
}
export const findParentByPrefix = (prefix: string, previousValue: any = null): Parent | null => {
  if (!previousValue || !prefix) {
    return null
  }
  if (previousValue.parent &&
      previousValue.parent.prefix === prefix 
    ) {
    return previousValue.parent
  }
  if (!previousValue.history) {
    return null
  }
  return previousValue.history.find(
    (item: any, index: number) => item && item.prefix === prefix)
}
export const loadPath = (history: Array<Parent>) => {
  return history
    .filter((parent: Parent) => parent)
    .reduce((previous, current: Parent) =>
      previous
        .concat(current.prefix,'/')
        .concat(current.item ? current.item._id : null, '/')
        .replace('//', '/')
    , '')
}
export const getParentContextValue = (parent: Parent | null, previousValue: any = null): ParentCrudContextValue => {
  if (!parent) {
    return previousValue
  }
  previousValue = previousValue || parentCrudContextValueDefault
  if (previousValue.parent && previousValue.history) {
    let valIndex = null
    for (const index in previousValue.history) {
      const val = previousValue.history[index]
      if (val && val.prefix === parent.prefix) {
        valIndex = index
      }
    }
    if (valIndex == null) {
      previousValue.history.push(previousValue.parent)
    } else {
      previousValue.history[valIndex] = previousValue.parent
    }
  }
  previousValue.parent = Parent.load(parent, previousValue.history)
  previousValue.path = loadPath(previousValue.history)
  return previousValue
}

const ParentCrudContext = React.createContext(parentCrudContextValueDefault)

export { ParentCrudContext }

/*
const parentValue = new Parent(
  item, // item
  action, // action
  prefix// prefix
);
const parentCrudContextValue = getParentContextValue(parentValue, useContext(ParentCrudContext))
(<ParentCrudContext.Provider>
  <ChildCrud />
</ParentCrudContext.Provider>)
*/
