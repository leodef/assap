import React, { useMemo } from 'react'
import {
  Link as RouterLink,
  // eslint-disable-next-line no-unused-vars
  LinkProps as RouterLinkProps
} from 'react-router-dom'
import {
  ListItem,
  ListItemIcon,
  ListItemText
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import { Omit } from '@material-ui/types'
import './ListItemLink.scss'

interface ListItemLinkProps {
  icon?: React.ReactElement;
  primary: string;
  to: string;
  className: string;
  onClick?: (event: any) => void;
}

/**
 * Item de uma lista
 *   Usando o visual do framework Material-UI e o framework react-router-dom para redirecionamento
 * @param {ListItemLinkProps} props Propriedades
 * @return {React.Component} Componente item de uma lista
 */
export const ListItemLink = (props: ListItemLinkProps) => {
  const { icon, primary, to, className, onClick } = props

  const renderLink = useMemo(() => {
    const body = (React.forwardRef<any, Omit<RouterLinkProps, 'to'>>((itemProps, ref) => {
      return (<RouterLink to={to} ref={ref} {...itemProps} />)
    }))
    body.displayName = 'ListItemLink.forwardRef'
    return body
  },
  [to]
  )
  renderLink.displayName = 'renderLink'

  return (
    <li>
      <ListItem
        button
        component={renderLink}
        className={className}
        onClick={onClick}>
        {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  )
}
