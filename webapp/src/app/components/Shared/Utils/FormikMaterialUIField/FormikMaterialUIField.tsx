import React, { useCallback, useMemo } from 'react'
import {
  Select,
  FormControl,
  InputLabel,
  makeStyles, MenuItem
} from '@material-ui/core'
import moment from 'moment'
// eslint-disable-next-line no-unused-vars
import { Field, useField } from 'formik'
// eslint-disable-next-line no-unused-vars
import {
  KeyboardTimePicker,
  KeyboardDatePicker
} from '@material-ui/pickers'
import MuiTextField from '@material-ui/core/TextField'
import { SelectOption } from '../../../../utils/SelectOption'
import { dateFormat } from '../../../../types/Config'
import './FormikMaterialUIField.scss'

// https://github.com/stackworx/formik-material-ui
// https://codesandbox.io/s/915qlr56rp
/**
 * Metodo para obter o nome do campo consierando o caminho no formulário
 *   inserindo o prefixed antes do nome do campo
 * @param {string} nome Nome do campo
 * @param {any} props Propriedades
 * @return {string} Nome completo do campo
 */
export function getFieldName (name: number | string | null, props: any) {
  return [
    props.name,
    (name ? String(name) : null)
  ].filter((val) => !!val).join('.') || ''
}

/**
 * Metodo para obter o nome do fomulário consierando o caminho no formulário
 * @param {any} props Propriedades
 * @return {string} Nome completo do formulário
 */
export function getFormName (props: any) {
  return getFieldName(null, props)
}

/**
 * Campo genérico
 * @param {TextFieldProps} props Propriedades
 * @return {React.Component} Componente com campo genérico
 */
export const TextField = React.memo((props: any) => {
  TextField.displayName = 'UpperCasingTextField'
  const [field] = useField(props)
  const others = {} as any
  if (props.uppercase || props.lowercase || props.formatter) {
    others.onChange = (event: any) => {
      const formatter = props.formatter || ((val: any) => val)
      const value = event.target.value
      event.target.value = value
        ? formatter(
          props.uppercase
            ? value.toString().toUpperCase()
            : value.toString().toLowerCase()
        )
        : ''
      field.onChange(event)
    }
  }
  return <MuiTextField
    {...props}
    {...field}
    {...others} />
})

/**
 * Campo genérico contendo obrigatóriamente somente letras maiusculas
 * @param {TextFieldProps} props Propriedades
 * @return {React.Component} Componente com campo genérico contendo obrigatóriamente somente letras maiusculas
 */
export const UppercaseInput = React.memo((props: any) => {
  UppercaseInput.displayName = 'UppercaseInput'

  const {
    form: { setFieldValue },
    field: { name }
  } = props

  const onChange = React.useCallback(
    (event) => {
      const { value } = event.target
      setFieldValue(name, value ? value.toUpperCase() : '')
    },
    [name, setFieldValue]
  )
  return <Field
    {...props}
    onChange={onChange} />
})
/**
 * Caixa de seleção genérica
 * @param {TextFieldProps} props Propriedades
 * @return {React.Component} Componente com caixa de seleção genérica
 */
export const SelectInput = React.memo((props: any) => {
  SelectInput.displayName = 'SelectInput'
  const classes = useStyles()
  /*
    const options = [ { value: '', label: '' } ]
    return (
    <SelectInput
      labelId="demo-simple-select-label"
      id="demo-simple-select"
      value={options[0].value}
      onChange={handleChange}
      options={options}/>);
  */
  let { enumOptions, options, ...inputProps } = useMemo(() => props, [props])
  const { name, label, fullWidth } = useMemo(() => props, [props])
  options = useMemo(
    () =>
      enumOptions
        ? SelectOption.loadEnum(enumOptions)
        : (SelectOption.load(options) as Array<SelectOption>),
    [enumOptions, options]
  )
  const randomId = useMemo(() => Math.abs(Math.random() * 100), [])
  const loabelId = useMemo(
    () =>
      label ? props.id || `select-label-${name}-${randomId}` : null,
    [name, randomId, props.id, label]
  )
  const [field] = useField(props)
  return (
    <FormControl
      fullWidth={fullWidth}
      className={classes.formControl}>
      {label ? (<InputLabel id={loabelId}>{label}</InputLabel>) : null}
      <Select
        {...inputProps}
        {...field}
        labelId={loabelId}
        fullWidth={fullWidth}>
        {options.map(
          (
            option: { value: string; label: string },
            index: number
          ) => (
            <MenuItem value={option.value} key={index}>
              {option.label}
            </MenuItem>
          )
        )}
      </Select>
    </FormControl>
  )
})

/**
 * Fabrica de campo da biblioteca formik encapsulando o campo da biblioteca visual Material-UI
 * @param {any} props Propriedades
 * @return {React.Component} Componente com fabrica de campo da biblioteca formik encapsulando o campo da biblioteca visual Material-UI
 */
export const FormikMaterialUIField = (config: any) => {
  const FormikMaterialUIField = React.memo((props: any) => {
    FormikMaterialUIField.displayName = 'FormikMaterialUIField'
    const onChange = (e: any) => {
      if (props.field && props.field.onChange) {
        props.field.onChange(e)
      }
      if (config.onChange) {
        config.onChange(e)
      }
    }
    const onBlur = (e: any) => {
      if (!!props.field && !!props.field.onBlur) {
        props.field.onBlur(e)
      }
      if (config.onBlur) {
        config.onBlur(e)
      }
    }
    return (
      <MuiTextField
        {...(props.field || props)}
        {...config}
        onBlur={onBlur}
        onChange={onChange}
      />
    )
  })
  return FormikMaterialUIField
}

export function DatePicker (props: any) {
  const [, meta, helpers] = useField(props)
  const { label, button, ...others } = useMemo(
    () => props,
    [props])
  const format = useMemo(
    () => (props.format || dateFormat), 
    [props.format])
  const value = useMemo(
    () => moment(meta.value).toDate(),
    [meta.value])

  const onChange = useCallback(
    (val: any) => helpers.setValue(moment(val).toDate()),
    [helpers]
  )
  /*
    variant="inline"
    id="date-picker-inline"
    label="Date picker inline"

    --- input type=date
    InputLabelProps={{
      shrink: true
    }}
  */
  return (
    <KeyboardDatePicker
      disableToolbar
      format={format}
      margin="normal"
      label={label}
      value={value}
      onChange={onChange}
      KeyboardButtonProps={{
        'aria-label': label,
        ...(button || {})
      }}
      {...others}
    />
  )
}

export function TimePicker (props: any) {
  const [, meta, helpers] = useField(props)
  const { label, button, ...others } = useMemo(
    () => props,
    [props])
  const value = useMemo(
    () => moment(meta.value).toDate(),
    [meta.value])
  const onChange = useCallback(
    (val: any) => helpers.setValue(
      moment(val).toDate()
    ),
    [helpers]
  )
  /*
    variant="inline"
    id="time-picker"
    label="Time picker"
  */
  return (
    <KeyboardTimePicker
      disableToolbar
      margin="normal"
      {...others}
      label={label}
      value={value}
      onChange={onChange}
      KeyboardButtonProps={{
        'aria-label': label,
        ...(button || {})
      }}
    />
  )
}

export const useStyles = makeStyles((theme) => ({
  fullWidthFormControl: {
    margin: theme.spacing(1),
    width: '100%',
    minWidth: 120
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
  multiline: {
    width: '100%',
    maxHeight: 80,
    overflowY: 'auto'
  }
}))
export default FormikMaterialUIField
