import React from 'react'
import {
  Button
} from '@material-ui/core'
import clsx from 'clsx'
// eslint-disable-next-line no-unused-vars
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import { green } from '@material-ui/core/colors'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrapper: {
      // margin: theme.spacing(1),
      position: 'relative'
    },
    buttonSuccess: {
      backgroundColor: green[500],
      '&:hover': {
        backgroundColor: green[700]
      }
    },
    buttonProgress: {
      color: green[500],
      position: 'absolute',
      top: '50%',
      left: '50%',
      marginTop: -12,
      marginLeft: -12
    }
  })
)

/**
 * Copyright informando proprietário do sistema
 * @param {any} props Propriedades
 * @return {React.Component} Componente com copyright informando proprietário do sistema
 */
export const LoadingButton = React.memo((props: any) => {
  LoadingButton.displayName = 'LoadingButton'
  const classes = useStyles()
  const { groupButton, ...others } = props

  return (groupButton
    ? <LoadingButtonContent {...others} />
    : <div className={classes.wrapper}>
      <LoadingButtonContent {...others} />
    </div>
  )
})

export const LoadingButtonContent = React.memo((props: any) => {
  LoadingButton.displayName = 'LoadingButton'
  const classes = useStyles()
  const {
    loading,
    success,
    button,
    progress,
    children,
    ...others
  } = props

  const buttonClassname = clsx({
    [classes.buttonSuccess]: success
  })
  // className={classes.wrapper}
  return (
    <React.Fragment >
      <Button
        className={buttonClassname}
        disabled={loading}
        {...button || {}}
        {...others}>
        {children}
      </Button>
      {
        loading &&
        <CircularProgress
          size={24}
          className={classes.buttonProgress}
          {...progress || {} }/>
      }
    </React.Fragment>
  )
})
