import React, { useMemo } from 'react'
import {
  Tooltip as MuiTooltip
} from '@material-ui/core'
import './Tooltip.scss'

/**
 * Tooltip com tratamento para botão desabilitado
 * @param {any} props Propriedades
 * @return {React.Component} Componente
 */
export const Tooltip = React.memo(React.forwardRef((props: any, ref: any) => {
  Tooltip.displayName = 'Tooltip'
  const {
    children,
    disabled,
    title,
    open,
    onClose,
    onOpen,
    tooltipRef,
    ...others
  } = useMemo(() => props, [props])
  const childrenElements = useMemo(
    () => (React.cloneElement(children, { ...others, ref })),
    [children, others, ref]
  )
  return (
    <MuiTooltip
      ref={tooltipRef}
      title={title}
      open={open}
      onClose={onClose}
      onOpen={onOpen}>
      {
        (disabled
          ? (<span>{childrenElements}</span>)
          : childrenElements)
      }
    </MuiTooltip>
  )
}))
