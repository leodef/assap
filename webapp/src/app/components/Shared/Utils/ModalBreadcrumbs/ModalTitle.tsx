import React from "react";
import {
  Breadcrumbs,
  makeStyles,
  Typography
} from "@material-ui/core";
import './ModalTitle.scss'
/**
 * Componente informando o caminho até o modal atual
 * @param {any} props Propriedades
 * @return {React.Component} Componente informando o caminho até o modal atual
 * @example <ModalTitle modalHistory={['a', 'b']} title="c"/>
 */
export const ModalTitle = React.memo((props: any) => {
  const classes = useStyles();
  const { modalHistory, title, children } = props;
  return modalHistory ? (
    <Breadcrumbs
      maxItems={3}
      aria-label="breadcrumb">
      {modalHistory.map((value: any, index: number) => (
        <Typography
          key={`modal_title_item_${value}_${index}`}
          color="textSecondary">{value}</Typography>
      ))}
      {title
        ? (<Typography
            color="textSecondary">{title}</Typography>)
        : null
      }
    </Breadcrumbs>
  ) : title ? (
    <Typography
      className={classes.title}
      color="textSecondary"
      gutterBottom>
      {title}
    </Typography>
  ) : (
    children || null
  );
});

const useStyles = makeStyles({
  title: {
    fontSize: 14,
  }
});
