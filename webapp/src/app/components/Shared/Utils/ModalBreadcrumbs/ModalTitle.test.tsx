/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ModalTitle } from './ModalTitle'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ModalTitle />, div)
  ReactDOM.unmountComponentAtNode(div)
})
