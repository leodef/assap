import React,
{
  useContext,
  useCallback,
  useMemo,
  useEffect,
  useState
} from 'react'
import _ from 'lodash'
import {
  useSelector, useDispatch
} from 'react-redux'
import MuiAutocomplete from '@material-ui/lab/Autocomplete'
import {
  TextField,
  CircularProgress,
  makeStyles,
  createStyles,
  Theme
} from '@material-ui/core'
import {
  AutocompleteContext
} from '../../../contexts/AutocompleteContext'
import {
  CollectionUtils
} from '../../../utils/CollectionUtils'
import {
  ParentCrudContext
} from '../../../contexts/ParentCrudContext'
import './Autocomplete.scss'

/**
 * Autocomplete genérico
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com autocomplete genérico
 */
export const Autocomplete = (props: any) => {
  const classes = useStyles()
  const {
    getOptionSelected,
    onChange,
    value,
    disabled,
    InputProps,
    exclude,
    options,
    noLabel,
    fieldProps,
    onInputChange,
    getOptionDisabled,
    ...otherProps
  } = useMemo(() => props, [props])
  // CrudContext
  let {
    getState,
    types,
    getOptionLabel,
    getOptionId,
    compare,
    fields,
    label,
    resolve
  } = useContext(AutocompleteContext)
  const [autocompleteValue, setAutocompleteValue ] = useState(value as any)
  const parentCrudContextValue = useContext(ParentCrudContext)
  getOptionId = getOptionId || ((option: any) => (option._id || option))
  compare = compare || ((item: any, option: any) => (getOptionId(item) === getOptionId(option)))
  const excludeList = exclude ? (Array.isArray(exclude) ? exclude : [exclude]) : []
  // selector
  const {
    filter,
    items,
    loading,
    fetched
  } = useSelector((state: any) => {
    const {
      filter,
      items,
      loading,
      fetched
    } = getState(state)
    return {
      filter,
      items,
      loading,
      fetched
    }
  })

  // dispatch
  const dispatch = useDispatch()

  // Metodo de consulta
  const fetch = useCallback((params?: {filter?: any, fields?: any, parent?: any}) => {
    dispatch({
      type: types.FETCH_OPTIONS_ITEM,
      payload: params
    })
  }, [dispatch, types])

  // Metodo para atualizar informações do filtro
  const setFilter = useCallback((pFilter: any) => {
    if (!pFilter || _.isEmpty(pFilter)) { return }
    dispatch({
      type: types.SET_FILTER_OPTIONS_ITEM,
      payload: {
        filter: pFilter
      }
    })
  }, [dispatch, types])

  const filteredItems = useMemo(() => {
    if(options) {
      return options
    }
    if (resolve === 'FRONT') {
      const list = CollectionUtils.resolveCollection(
        items,
        {
          filter,
          pagination: null,
          fields,
          sort: null
        })
      return list
    } else {
      return items
    }
  }, [
    items,
    resolve,
    filter,
    fields,
    options
  ])

  // lifecycle
  // Executar metodo de consulta
  useEffect(() => {
    if (resolve === 'FRONT' && !fetched && !options) {
      fetch()
    }
  }, [
    fetch,
    resolve,
    fetched,
    options
  ])

  useEffect(() => {
    if ((!resolve || resolve === 'BACK' ) && !options) {
      fetch({
        filter: filter,
        fields: fields,
        parent: parentCrudContextValue
      })
    }
  }, [
    fetch,
    resolve,
    filter,
    fields,
    options,
    parentCrudContextValue
  ])
  // label={label}
  return (<MuiAutocomplete
    {...otherProps}
    getOptionSelected={(option: any, value: any) => {
      if(getOptionSelected) {
        return Boolean(getOptionSelected(option, value))
      }
      return Boolean(compare(option, value))
    }}
    getOptionDisabled={(option: any) =>
      getOptionDisabled
        ? Boolean(getOptionDisabled(option))
        : Boolean(excludeList.find((value: any) => compare(option, value)))
    }
    onInputChange={(event: any, newInputValue: any) => {
      if(onInputChange) {
        onInputChange(event, newInputValue)
      }
      setFilter(newInputValue)
    }}
    onChange={(event:any, inputValue: any) => {
      setAutocompleteValue(inputValue)
      if(onChange) {
        onChange(inputValue)
      }
    }}
    value={autocompleteValue}
    getOptionLabel={getOptionLabel}
    options={filteredItems}
    loading={loading}
    renderInput={(params: any) => {
      return (
      <TextField
        className={classes.autocomplete}
        variant="outlined"
        label={ noLabel ? null : label }
        disabled={disabled}
        {...params}
        {...(fieldProps || {})}
        InputProps={{
          ...params.InputProps,
          ...(InputProps || []),
          endAdornment: (
            <React.Fragment>
              {InputProps ? InputProps.endAdornment : null }
              {loading ? <CircularProgress color="inherit" size={20} /> : null}
              {params.InputProps ? params.InputProps.endAdornment : null}
            </React.Fragment>
          )
        }}
      />
    )}}
  />)
}


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    autocomplete: {
      maxHeight: 100,
      //overflowY: 'auto'
    }
  })
)
