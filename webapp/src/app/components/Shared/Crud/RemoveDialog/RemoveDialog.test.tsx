/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { RemoveDialog } from './RemoveDialog'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<RemoveDialog />, div)
  ReactDOM.unmountComponentAtNode(div)
})
