/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { TituloDescFieldset } from './TituloDescFieldset'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<TituloDescFieldset />, div)
  ReactDOM.unmountComponentAtNode(div)
})
