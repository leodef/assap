import
  React
from "react";
import {
  TituloFieldset
} from "../TituloFieldset/TituloFieldset";
import {
  DescFieldset
} from "../DescFieldset/DescFieldset";
import "./TituloDescFieldset.scss";

/**
 * Area de inputs para titulo e descrição
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para titulo e descrição
 */
export const TituloDescFieldset = (props: any) => {
  return (
    <React.Fragment>
      <TituloFieldset {...props} />
      <DescFieldset {...props} />
    </React.Fragment>
  );
};
