/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { TituloFieldset } from './TituloFieldset'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<TituloFieldset />, div)
  ReactDOM.unmountComponentAtNode(div)
})
