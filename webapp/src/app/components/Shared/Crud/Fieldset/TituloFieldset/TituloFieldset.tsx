import
React,
{
  useCallback,
  useEffect,
  useMemo
} from "react";
import {
  FormControlLabel,
  Grid,
  Switch,
  TextField
} from "@material-ui/core";
// eslint-disable-next-line no-unused-vars
import {
  getFieldName
} from "../../../Utils/FormikMaterialUIField/FormikMaterialUIField";
import {
  useField
} from "formik";
import {
  TIPO_FORMACAO
} from "../../../../../enums/tipo-formacao";
import "./TituloFieldset.scss";

/**
 * Area de inputs para titulo
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para titulo
 */
export const TituloFieldset = (props: any) => {
  const { disabled, labels, hidden } = useMemo(() => props || {}, [props]);
  const automated = useMemo(
    () =>
      (props.automated ? props.automated.titulo || props.automated : null) ||
      (props.getTituloAutomated ? props.getTituloAutomated(props) : null) 
  , [props])
  const placeholder = useMemo(
    () =>
      (props.placeholder ? props.placeholder.titulo || props.placeholder : null) ||
      (props.getTituloPlaceholder ? props.getTituloPlaceholder(props) : null) ||
      automated,
    [
      props,
      automated
    ]
  )

  const fields = useMemo(() => {
    return {
      titulo: {
        name: getFieldName("titulo", props),
        label: labels && labels.titulo ? labels.titulo : "Título",
      },
      tipoFormacaoTitulo: {
        name: getFieldName("tipoFormacaoTitulo", props),
        label:
          labels && labels.tipoFormacaoTitulo
            ? labels.tipoFormacaoTitulo
            : "Tipo de formação do título",
      },
    };
  }, [props, labels]);

  // titulo
  const [, tituloMeta, tituloHelpers] = useField(
    fields.titulo.name
  );
  const titulo = useMemo(
    () => tituloMeta.value || '',
  [tituloMeta]);
  const setTitulo = useCallback(
    (value) => tituloHelpers.setValue(value),
    [tituloHelpers]
  );

  // tipoFormacaoTitulo
  const [, tipoFormacaoTituloMeta, tipoFormacaoTituloHelpers] = useField(
    fields.tipoFormacaoTitulo.name
  );
  const tipoFormacaoTitulo = useMemo(
    () => tipoFormacaoTituloMeta.value,
    [tipoFormacaoTituloMeta]);
  const formacaoTituloManual = useMemo(
      () => tipoFormacaoTitulo === TIPO_FORMACAO.MANUAL,
      [tipoFormacaoTitulo]
    );
  const formacaoTituloAutomatizado = useMemo(
    () => tipoFormacaoTitulo !== TIPO_FORMACAO.MANUAL,
    [tipoFormacaoTitulo]
  );

  const formacaoTituloLabel = useMemo(
    () =>
      tipoFormacaoTitulo === TIPO_FORMACAO.MANUAL
        ? "Título manual"
        : "Informar título manualmente",
    [tipoFormacaoTitulo]
  );

  useEffect(() => {
    if(
      automated &&
      formacaoTituloAutomatizado &&
      automated !== titulo
    ) {
      setTitulo(automated)
    }
  }, [
    automated,
    setTitulo,
    formacaoTituloAutomatizado,
    titulo
  ])

  const setTipoFormacaoTitulo = useCallback(
    (event) => {
      const checked = event.target.checked
      if(checked) {
        setTitulo('')
      } else if(automated) {
        setTitulo(automated)
      }
      tipoFormacaoTituloHelpers.setValue(
        checked === true
          ? TIPO_FORMACAO.MANUAL
          : TIPO_FORMACAO.AUTOMATIZADO
      )
    },
    [
      tipoFormacaoTituloHelpers,
      setTitulo,
      automated
    ]
  );

  return (
    <React.Fragment>
      <Grid item xs={12}>
        <TextField
          value={titulo}
          onChange={(event) => setTitulo(event.target.value)}
          type={ hidden ? 'hidden' : 'text'}
          label={ hidden ? null : fields.titulo.label}
          placeholder={ placeholder || null}
          disabled={disabled || !formacaoTituloManual}
          fullWidth
        />
      </Grid>
      {hidden ? null :
      (<Grid item xs={12}>
        <FormControlLabel
          control={
            <Switch
              checked={formacaoTituloManual}
              onChange={setTipoFormacaoTitulo}
              color="primary"
              size="small"
            />
          }
          label={formacaoTituloLabel}
        />
      </Grid>)
      }
    </React.Fragment>
  );
};
