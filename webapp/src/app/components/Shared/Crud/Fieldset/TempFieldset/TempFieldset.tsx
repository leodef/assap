import
  React,
  {
    useCallback,
    useEffect,
    useMemo
  }
from 'react'
import {
  getFieldName,
  TextField
} from '../../../Utils/FormikMaterialUIField/FormikMaterialUIField'
import { useField } from 'formik'
import { DTO } from '../../../../../types/DTO'
import './TempFieldset.scss'

/**
 * Area de inputs para id temporatio
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para id temporatio
 */
export const TempFieldset = React.memo((props: any) => {
  TempFieldset.displayName = 'TempFieldset'
  // const { _temp } = useMemo(() => props, [props])
  const { _temp, disabled } = useMemo(() => props, [props])

  const fields = useMemo(() => {
    return {
      _temp: {
        name: getFieldName('_temp', props)
      },
    }
  }, [props])
  const [, _tempMeta, _tempHelpers] = useField(
    fields._temp.name
    );
  const generatedId = useMemo(
    () => (_temp ? DTO.generateTempId() : null),
    [_temp])
  const tempValue = useMemo(
    () => (_tempMeta.value),
    [_tempMeta])
  
  const setTempValue = useCallback(
      (value: any) => _tempHelpers.setValue(value), [_tempHelpers],
    )

  useEffect(() => {
    if(generatedId && (!tempValue || tempValue === '')) {
      setTempValue(generatedId)
    }
  }, [setTempValue, generatedId, tempValue])

  return (<React.Fragment>
    <TextField
          name={fields._temp.name}
          type="hidden"
          disabled={disabled}
        />
  </React.Fragment>);
})
