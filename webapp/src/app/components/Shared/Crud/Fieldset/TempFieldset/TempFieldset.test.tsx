/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { TempFieldset } from './TempFieldset'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<TempFieldset />, div)
  ReactDOM.unmountComponentAtNode(div)
})
