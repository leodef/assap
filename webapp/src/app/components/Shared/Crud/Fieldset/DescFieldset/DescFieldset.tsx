import
React,
{
  useCallback,
  useEffect,
  useMemo
} from "react";
import { 
  FormControlLabel,
  Grid,
  Switch,
  TextField
} from "@material-ui/core";
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  useStyles as fieldsUseStyles
} from "../../../Utils/FormikMaterialUIField/FormikMaterialUIField";
import { 
  useField
} from "formik";
import { 
  TIPO_FORMACAO
} from "../../../../../enums/tipo-formacao";
import "./DescFieldset.scss";

/**
 * Area de inputs para descrição
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para descrição
 */
export const DescFieldset = (props: any) => {
  const fieldsClasses = fieldsUseStyles();
  const { disabled, labels, hidden } = useMemo(() => props || {}, [props]);
  const automated = useMemo(
    () =>
      (props.automated ? props.automated.desc || props.automated : null) ||
      (props.getDescAutomated ? props.getDescAutomated(props) : null) 
  , [props])
  const placeholder = useMemo(
    () =>
      (props.placeholder ? props.placeholder.desc || props.placeholder : null) ||
      (props.getDescPlaceholder ? props.getDescPlaceholder(props) : null) ||
      automated,
    [
      props,
      automated
    ]
  )

  const fields = useMemo(() => {
    return {
      desc: {
        name: getFieldName("desc", props),
        label: labels && labels.desc ? labels.desc : "Descrição",
      },
      tipoFormacaoDesc: {
        name: getFieldName("tipoFormacaoDesc", props),
        label:
          labels && labels.tipoFormacaoDesc
            ? labels.tipoFormacaoDesc
            : "Tipo de formação do descrição",
      },
    };
  }, [props, labels]);

  // desc
  const [, descMeta, descHelpers] = useField(
    fields.desc.name
  );
  const desc = useMemo(
    () => descMeta.value || '',
  [descMeta]);
  const setDesc = useCallback(
    (value) => descHelpers.setValue(value),
    [descHelpers]
  );

  // tipoFormacaoDesc
  const [, tipoFormacaoDescMeta, tipoFormacaoDescHelpers] = useField(
    fields.tipoFormacaoDesc.name
  );
  const tipoFormacaoDesc = useMemo(
    () => tipoFormacaoDescMeta.value,
    [tipoFormacaoDescMeta]);
    const formacaoDescManual = useMemo(
      () => tipoFormacaoDesc === TIPO_FORMACAO.MANUAL,
      [tipoFormacaoDesc]
    );
  const formacaoDescAutomatizado = useMemo(
    () => tipoFormacaoDesc !== TIPO_FORMACAO.MANUAL,
    [tipoFormacaoDesc]
  );

  const formacaoDescLabel = useMemo(
    () =>
      tipoFormacaoDesc === TIPO_FORMACAO.MANUAL
        ? "Descrição manual"
        : "Informar descrição manualmente",
    [tipoFormacaoDesc]
  );

  useEffect(() => {
    if(
      automated &&
      formacaoDescAutomatizado &&
      automated !== desc
    ) {
      setDesc(automated)
    }
  }, [
    automated,
    setDesc,
    formacaoDescAutomatizado,
    desc
  ])

  const setTipoFormacaoDesc = useCallback(
    (event) => {
      const checked = event.target.checked
      if(checked) {
        setDesc('')
      } else if(automated) {
        setDesc(automated)
      }
      tipoFormacaoDescHelpers.setValue(
        checked === true
          ? TIPO_FORMACAO.MANUAL
          : TIPO_FORMACAO.AUTOMATIZADO
      )
    },
    [
      tipoFormacaoDescHelpers,
      setDesc,
      automated
    ]
  );
  
  return (
    <React.Fragment>
      {/* Desc */}
      <Grid item xs={12}>
        <TextField
          value={desc}
          onChange={(event: any) => setDesc(event.target.value)}
          type={ hidden ? 'hidden' : 'text'}
          label={ hidden ? null : fields.desc.label}
          placeholder={ placeholder || null}
          disabled={disabled || !formacaoDescManual}
          multiline
          className={fieldsClasses.multiline}
        />
      </Grid>

      {hidden ? null :
      (<Grid item xs={12}>
        <FormControlLabel
          control={
            <Switch
              checked={formacaoDescManual}
              onChange={setTipoFormacaoDesc}
              color="primary"
              size="small"
            />
          }
          label={formacaoDescLabel}
        />
      </Grid>)
      }
    </React.Fragment>
  );
};
