/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { DescFieldset } from './DescFieldset'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<DescFieldset />, div)
  ReactDOM.unmountComponentAtNode(div)
})
