import React, { useContext, useCallback, useMemo, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import {
  Card,
  CardActions,
  CardContent,
  Grid,
  Button, ButtonGroup
} from '@material-ui/core'
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  ArrowBack as ArrowBackIcon
} from '@material-ui/icons'
import { CrudContext } from '../../../../contexts/CrudContext'
import { ActionTypeEnum } from '../../../../types/Crud'
import { ParentCrudContext } from '../../../../contexts/ParentCrudContext'
import { ModalTitle } from '../../Utils/ModalBreadcrumbs/ModalTitle'
import './Show.scss'

const useStyles = makeStyles({
  root: {
    minWidth: 300, // 275,
    minHeight: 300
  },
  title: {
    fontSize: 14
  }
})

/**
 * Visualização de item
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item
 */
export const Show = (props: any) => {
  const {
    actions,
    title,
    modalHistory
  } = useMemo(() => props, [props])
  const classes = useStyles()
  // selector
  return (
    <Card className={classes.root}>
      <CardContent>
        <ModalTitle title={title} modalHistory={modalHistory}/>
        <ShowBody
          {...props} />
      </CardContent>
      <CardActions>
        { actions ? actions(props) : <ShowActions
          {...props} />}
      </CardActions>
    </Card>
  )
}

const ShowBody = (props: any) => {
  const { body, fixed, ...bodyProps } = useMemo(() => props, [props])
  // CrudContext
  const { getState, types } = useContext(CrudContext)
  const parentCrudContextValue = useContext(ParentCrudContext)
  // selector
  const { item, action } = useSelector((state: any) => {
    const { item, action } = getState(state)
    return { item, action }
  })
  // dispatch
  const dispatch = useDispatch()

  // Metodo de consulta
  const find = useCallback((pItem: any) =>
    dispatch({
      type: types.FIND_ITEM,
      payload: {
        item: pItem,
        parent: parentCrudContextValue
      }
    }), [dispatch, types, parentCrudContextValue])

  const itemId = useMemo(() => (item ? item._id : null), [item])
  const fixedId = useMemo(() => (fixed ? fixed.id : null), [fixed])
  const id = useMemo(() => (fixedId || itemId), [fixedId, itemId])
  // lifecycle
  // Executar metodo de consulta
  useEffect(() => {
    if (action === ActionTypeEnum.SHOW && id) {
      find({ _id: id })
    }
  }, [id, action, find])

  return (<React.Fragment>{body({ ...bodyProps, item })}</React.Fragment>)
}

const ShowActions = (props: any) => {
  // CrudContext
  const { getState, actions, types } = useContext(CrudContext)
  // selector
  const item = useSelector((state: any) => getState(state).item)
  // dispatch
  const dispatch = useDispatch()

  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action,
        item
      }
    }), [dispatch, types])

  const toEdit = useCallback((pItem: any) => {
    setAction(ActionTypeEnum.EDIT, pItem)
  }, [setAction])

  const toRemove = useCallback((pItem: any) => {
    setAction(ActionTypeEnum.REMOVE, pItem)
  }, [setAction])

  const back = useCallback(() => {
    setAction(ActionTypeEnum.LIST, null)
  }, [setAction])

  const removeButton = useMemo(() => {
    return (actions.toRemove)
      ? (<Button
        size="small"
        variant="contained"
        color="primary"
        startIcon={<DeleteIcon />}
        onClick={(event: any) => toRemove(item)}>
        Remover
      </Button>) : null
  }, [actions, toRemove, item])

  const editButton = useMemo(() => {
    return actions.toEdit
      ? (<Button
        size="small"
        aria-label="Editar"
        variant="contained"
        color="primary"
        startIcon={<EditIcon />}
        onClick={() => toEdit(item)}>
          Editar
      </Button>)
      : null
  }, [actions, toEdit, item])

  const backButton = useMemo(() => {
    return actions.toBack
      ? (<Button
        size="small"
        variant="contained"
        color="primary"
        startIcon={<ArrowBackIcon />}
        onClick={(event: any) => back()}>
          Voltar
      </Button>)
      : null
  }, [actions, back])

  return (
    <Grid container>
      <Grid item>
        <ButtonGroup
          size="small"
          variant="contained"
          color="primary">
          {removeButton}
          {editButton}
          {backButton}
        </ButtonGroup>
      </Grid>
    </Grid>
  )
}
