import React,
{
  useEffect,
  useMemo,
  useCallback,
  useContext
} from 'react'
import {
  useDispatch,
  useSelector
} from 'react-redux'
import {
  Modal,
  Dialog,
  makeStyles,
  Theme,
  createStyles
} from '@material-ui/core'
import * as _ from 'lodash'
import { CrudContext } from '../../../contexts/CrudContext'
import {
  CollectionTypeEnum,
  ActionTypeEnum
} from '../../../types/Crud'
import { Show } from './Show/Show'
import { Form } from './Form/Form'
import { Collection } from './Collection/Collection'
import { RemoveDialog } from './RemoveDialog/RemoveDialog'
import { useHistory, useParams } from 'react-router-dom'
import './Crud.scss'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      minWidth: 200,
      minHeight: 200,
      // boxShadow: theme.shadows[5],
      // padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    }
  })
)

/**
 * Tela de gerenciameto de Crud
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Crud
 */
export const Crud = React.memo((props: any) => {
  Crud.displayName = 'Crud'
  const {
    form,
    show,
    remove,
    child,
    useUrlParams
  } = useMemo(() => props, [props])
  const params = useParams<any>()

  const fixed = useMemo(() => (
    (useUrlParams ? params : props.fixed) || {}
  ), [params, props.fixed, useUrlParams])

  const classes = useStyles()
  // CrudContext
  const { types, getState } = useContext(CrudContext)

  // collection merge defaults values
  const collection = useMemo(() => {
    const result = {
      type: CollectionTypeEnum.TABLE,
      pagination: { limit: 10 }
    }
    if (props.collection) {
      return _.defaultsDeep(props.collection, result)
    }
    return result
  }, [props.collection])

  const stateAction = useSelector(
    (state: any) => getState(state).action)
  const action = useMemo(() => (
    fixed.action ? fixed.action : stateAction
  ), [fixed.action, stateAction])

  const dispatch = useDispatch()
  const toList = useCallback(() =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action: ActionTypeEnum.LIST
      }
    }), [dispatch, types])
  const clear = useCallback(() =>
    dispatch({
      type: types.CLEAR_ITEM
    }), [dispatch, types])
  // dialog
  const onClose = useCallback((event: any) => toList(), [toList])

  const isShowModalOpen = useMemo(() =>
    (action === ActionTypeEnum.SHOW), [action])
  const isFormModalOpen = useMemo(() =>
    (action === ActionTypeEnum.NEW || action === ActionTypeEnum.EDIT), [action])
  const isRemoveDialogOpen = useMemo(() =>
    (action === ActionTypeEnum.REMOVE), [action])
  
  const history = useHistory()
  const unlisten = useMemo(
    () => history.listen((location: any, action: any) =>  clear())
  ,[history, clear])

  // lifecycle
  useEffect(() => {
    return () => {
      unlisten()
      clear()
    }
  }, [clear, unlisten])
  
  // render
  const ChildBody = useMemo(() => ((child && child.body) ? child.body : null), [child])
  const CollectionContent = useMemo(() => (<Collection {...collection} />), [collection])
  const FormModal = useMemo(() => (<Modal
    open={isFormModalOpen}
    onClose={onClose}
    aria-labelledby="Formulário"
    aria-describedby="Formulário">
    <div className={classes.paper}>
      <Form
        fixed={fixed}
        {...form}
      />
      {ChildBody}
    </div>
  </Modal>), [
      isFormModalOpen,
      onClose,
      form,
      classes,
      ChildBody,
      fixed
    ])
  const ShowModal = useMemo(() => (<Modal
    open={isShowModalOpen}
    onClose={onClose}
    aria-labelledby="Visualizar"
    aria-describedby="Visualizar">
    <div className={classes.paper}>
      <Show
        fixed={fixed}
        {...show} />
      {ChildBody}
    </div>
  </Modal>), [
    isShowModalOpen,
    onClose,
    show,
    classes,
    ChildBody,
    fixed
  ])
  const Remove = useMemo(() => (<Dialog
    open={isRemoveDialogOpen}
    aria-labelledby="Remover"
    aria-describedby="Remover">
    <RemoveDialog
      fixed={fixed}
      action={action}
      {...remove}/>
  </Dialog>), [
    isRemoveDialogOpen,
    remove,
    fixed,
    action
  ])
  return (<React.Fragment>

    {CollectionContent}

    {FormModal}

    {ShowModal}

    {Remove}

  </React.Fragment>)
})
/*
  collection: {
    type,
    config
  }
  show: {
    title,
    body
  },
  form: {
    title,
    body
  },
  remove: {
    title,
    body
  },
  child: {
    body
  }
 */
