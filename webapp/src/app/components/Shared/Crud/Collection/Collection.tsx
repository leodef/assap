import React, { useMemo, useContext, useCallback, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import * as _ from 'lodash'
import { CrudContext } from '../../../../contexts/CrudContext'
import { ActionTypeEnum, CollectionTypeEnum } from '../../../../types/Crud'
import { CollectionContext } from '../../../../contexts/CollectionContext'
import { List } from '../../Collection/List/List'
import { Table } from '../../Collection/Table/Table'
import { CollectionUtils } from '../../../../utils/CollectionUtils'
import { ParentCrudContext } from '../../../../contexts/ParentCrudContext'
import './Collection.scss'
import { useParams } from 'react-router-dom'

/**
 * Coleção de item
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com coleção de item
 */
export const Collection = (props: any) => {
  // prop
  const { type, config, resolve } = useMemo(() => props, [props])

  
  // CrudContext
  const { getState, actions, types} = useContext(CrudContext)

  // selector
  const {
    filter,
    pagination,
    fields,
    sort,
    items,
    action,
    fetchLoading,
    fetched
  } = useSelector((state: any) => {
    const {
      filter,
      pagination,
      fields,
      sort,
      items,
      action,
      fetchLoading,
      fetched
    } = getState(state)
    return {
      filter,
      pagination,
      fields,
      sort,
      items,
      action,
      fetchLoading,
      fetched
    }
  })
  const parentCrudContextValue = useContext(ParentCrudContext)

  const params = useParams<any>()
  // dispatch
  const dispatch = useDispatch()
    
  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action,
        item
      }
    }), [dispatch, types])

  // Metodo de consulta
  const fetch = useCallback((params?: {filter?: any, pagination?: any, fields?: any, sort?: any, parent?: any}) =>
    dispatch({
      type: types.FETCH_ITEM,
      payload: params
    }), [dispatch, types])

  // Metodo para atualizar informações do filtro
  const setFilter = useCallback((pFilter: any) =>
    dispatch({
      type: types.SET_FILTER_ITEM,
      payload: {
        filter: pFilter
      }
    }), [dispatch, types])

  const setFields = useCallback((pFields: any) =>
    dispatch({
      type: types.SET_FIELDS_ITEM,
      payload: {
        fields: pFields
      }
    }), [dispatch, types])

  // Metodo para atualizar informações da paginação
  const setPagination = useCallback((pPagination) =>
    dispatch({
      type: types.SET_PAGINATION_ITEM,
      payload: {
        pagination: _.defaults(pPagination, pagination)
      }
    }), [dispatch, types, pagination])

  const setSort = useCallback((pSort) =>
    dispatch({
      type: types.SET_SORT_ITEM,
      payload: {
        sort: pSort
      }
    }), [dispatch, types])

  const completeFilter = useMemo(
    () => (filter || props.filter) ? (filter || props.filter) : null,
    [filter, props.filter])
  const completePagination = useMemo(
    () => (pagination || props.pagination) ? _.defaults(pagination || {}, props.pagination) : null,
    [pagination, props.pagination])
  const completeFields = useMemo(
    () => (fields || props.fields) ? _.defaults(fields || {}, props.fields) : null,
    [fields, props.fields])
  const completeSort = useMemo(
    () => (sort || props.sort) ? (sort || props.sort) : null,
    [sort, props.sort])
  const localItems = useMemo(() => {
    if (resolve !== 'FRONT') { return null }
    return CollectionUtils.resolveCollection(
      items,
      {
        filter: completeFilter,
        pagination: completePagination,
        fields: completeFields,
        sort: completeSort
      })
  }, [completeFilter, completePagination, completeFields, completeSort, items, resolve])

  const localPagination = useMemo(() =>
    localItems ? localItems.pagination : null,
  [localItems])
  const filteredItems = useMemo(
    () => {
      return (
        (
          (resolve === 'FRONT') ? (
            localItems ? localItems.items : []
          ) : items
        ) || []).filter((item: any) => Boolean(item))
    }, [
      items,
      resolve,
      localItems
    ])

  const { page, limit } = useMemo(() => completePagination, [completePagination])

  // lifecycle
  // Setar o campo fields caso seja atualizado
  useEffect(() => {
    if (completeFields) {
      setFields(completeFields)
    }
  }, [completeFields, setFields])

  // Executar metodo de consulta
  useEffect(() => {
    if (
      action === ActionTypeEnum.LIST &&
      resolve === 'FRONT' &&
      !fetched
    ) {
      fetch({ parent: parentCrudContextValue })
    }
  }, [
    fetch,
    resolve,
    fetched,
    action,
    parentCrudContextValue])

  useEffect(() => {
    if (
      action === ActionTypeEnum.LIST &&
      (
        (
          !resolve ||
          resolve === 'BACK'
        ) &&
        !fetched
      )
    ) {
      fetch({
        filter: completeFilter,
        pagination: { page, limit },
        fields: completeFields,
        sort: completeSort,
        parent: parentCrudContextValue
      })
    }
  }, [
    fetched,
    fetch,
    resolve,
    completeFilter,
    page,
    limit,
    completeFields,
    completeSort,
    action,
    parentCrudContextValue])

  const loadUrlParams = useCallback(
    () => {
      if(params && params.action && params.id && fetched && filteredItems) {
        const item = filteredItems.find((item: any) => (item._id === params.id))
        if(item) {
          const action = Number(params.action)
          switch (action) {
            case Number(ActionTypeEnum.SHOW):
              setAction(ActionTypeEnum.SHOW, item)
              break
            case Number(ActionTypeEnum.EDIT):
              setAction(ActionTypeEnum.EDIT, item)
              break
            case Number(ActionTypeEnum.REMOVE):
              setAction(ActionTypeEnum.REMOVE, item)
              break
          }
        }
      }
    },
    [params, filteredItems, setAction, fetched],
  )
  
  useEffect(() => {
    loadUrlParams()
  }, [loadUrlParams])

  // CollectionContext
  const collectionContextValue = useMemo(() => {
    return {
      actions,
      types,
      items: filteredItems,
      filter: completeFilter,
      pagination: (localPagination || completePagination),
      fields: completeFields,
      sort: completeSort,
      setFilter,
      setPagination,
      setFields,
      setSort,
      length: items.length,
      loading: fetchLoading,
      config
    } as any
  }, [
    actions,
    types,
    filteredItems,
    completeFilter,
    localPagination,
    completePagination,
    completeFields,
    completeSort,
    setFilter,
    setPagination,
    setFields,
    setSort,
    items,
    fetchLoading,
    config
  ])
  const collectionContent = useMemo(() =>
    type === CollectionTypeEnum.TABLE
      ? (<Table />)
      : (<List />),
  [type])
  return (
    <CollectionContext.Provider
      value={collectionContextValue}>
      {collectionContent}
    </CollectionContext.Provider>)
}
