import React, { useCallback, useContext, useMemo, useEffect } from 'react'
import {
  Grid,
  Button,
  Card,
  CardActions,
  CardContent,
  makeStyles, ButtonGroup
} from '@material-ui/core'
import {
  Delete as DeleteIcon,
  ArrowBack as ArrowBackIcon,
  Send as SendIcon
} from '@material-ui/icons'
import { useDispatch, useSelector } from 'react-redux'
import { withFormik } from 'formik'
import _ from 'lodash'
import { CrudContext } from '../../../../contexts/CrudContext'
import { ActionTypeEnum } from '../../../../types/Crud'
import { LoadingButton } from '../../Utils/LoadingButton/LoadingButton'
import { ParentCrudContext } from '../../../../contexts/ParentCrudContext'
import { ModalTitle } from '../../Utils/ModalBreadcrumbs/ModalTitle'
import './Form.scss'


/**
 * Formulário
 * @param {FormProps} props Propriedades
 * @return {React.Component} Componente com formulário
 */
export const Form = (props: any) => {
  const { initialValues, schema, fixed } = useMemo(() => props, [props])

  // CrudContext
  const { getState, types } = useContext(CrudContext)
  const parentCrudContextValue = useContext(ParentCrudContext)

  // selector
  const {
    item,
    action
  } = useSelector((state: any) => {
    const { item, action } = getState(state)
    return { item, action }
  })

  const formInitialValues = useMemo(() =>
    (initialValues && initialValues[action])
      ? initialValues[action]
      : initialValues,
  [initialValues, action])
  const formSchema = useMemo(() =>
    (schema && schema[action])
      ? schema[action]
      : schema,
  [schema, action])

  // dispatch
  const dispatch = useDispatch()

  // Metodo de consulta
  const find = useCallback((pItem: any) =>
    dispatch({
      type: types.FIND_ITEM,
      payload: {
        item: pItem,
        parent: parentCrudContextValue
      }
    }), [dispatch, types, parentCrudContextValue])

  const itemId = useMemo(() => item ? item._id : null, [item])
  const fixedId = useMemo(() => (fixed ? fixed.id : null), [fixed])
  const id = useMemo(() => (fixedId || itemId), [fixedId, itemId])

  // lifecycle
  // Executar metodo de consulta
  useEffect(() => {
    if (
      (
        action === ActionTypeEnum.NEW ||
        action === ActionTypeEnum.EDIT
      ) && id) {
      find({ _id: id })
    }
  }, [id, action, find])

  // dispatch
  const save = useCallback((pItem: any) => {
    const type = pItem._id ? types.UPDATE_ITEM : types.CREATE_ITEM
    dispatch({
      type,
      payload: {
        item: pItem,
        parent: parentCrudContextValue
      }
    })
  }, [dispatch, types, parentCrudContextValue])

  const convertToFormData = useCallback((values: any) => {
    const formData = new FormData()
    for (const key in values) {
      const obj = values[key]
      formData.append(key, obj)
    }
    return formData
  }, [])

  const convertSaveValue = useCallback((values: any) => {
    const val = props.convertSaveValue
      ? props.convertSaveValue(values)
      : values
    return props.convertToFormData ? convertToFormData(val) : val
  }, [props, convertToFormData])
  const itemInitialValues = useMemo(
    () => _.defaults(item, formInitialValues),
    [item, formInitialValues]
  )
  const WithFormik = withFormik({
    mapPropsToValues: (props: any) => itemInitialValues,
    validationSchema: formSchema,
    handleSubmit: (values, params) => {
      if (props.handleSubmit) {
        return props.handleSubmit(values, params)
      }
      const { setSubmitting } = params
      setSubmitting(false)
      save(
        convertSaveValue(values)
      )
    },
    displayName: 'CrudForm'
  })(FormBody)
  const body = useMemo(
    () => (<WithFormik
      {...props}
      name={null}
      item={item} />), [item, props])
  return body
}

const FormBody = React.memo((props: any) => {
  FormBody.displayName = 'FormBody'
  const classes = useStyles()
  const {
    title,
    formProps,
    handleSubmit,
    actions,
    showButtons,
    modalHistory
  } = useMemo(() => props, [props])
  const { body, ...bodyProps } = useMemo(() => props, [props])
  return (<form {...(formProps || {})} onSubmit={handleSubmit}>
    <Card className={classes.root}>
      <CardContent>
        <ModalTitle title={title} modalHistory={modalHistory}/>
        {
          body(bodyProps)
        }
      </CardContent>

      {showButtons !== false
        ? (<CardActions>
          { actions ? actions(props)
            : <FormActions
              {...props} />}
        </CardActions>) : null}
    </Card>
  </form>)
})

export const RemoveButton = React.memo((props: any) => {
  RemoveButton.displayName = 'RemoveButton'
  const { actions, itemId, toRemove, item } = props
  return (actions.toRemove && itemId)
    ? (<Button
      size="small"
      variant="contained"
      color="primary"
      startIcon={<DeleteIcon />}
      onClick={(event: any) => toRemove(item)}>
      Remover
    </Button>) : null
})

export const SubmitButton = React.memo((props: any) => {
  SubmitButton.displayName = 'SubmitButton'
  const { loading, actions } = props
  const button = {
    size: 'small',
    type: 'submit',
    variant: 'contained',
    color: 'primary',
    startIcon: (<SendIcon />)
  }
  return actions.submit
    ? (<LoadingButton
      loading={loading}
      button={button}>
      Salvar
    </LoadingButton>) : null
})

export const BackButton = React.memo((props: any) => {
  BackButton.displayName = 'BackButton'
  const { actions, back } = props
  return actions.toBack
    ? (<Button
      size="small"
      variant="contained"
      color="primary"
      startIcon={<ArrowBackIcon />}
      onClick={(event: any) => back()}>
        Voltar
    </Button>) : null
})

export const FormActions = (props: any) => {
  // CrudContext
  const { getState, actions, types } = useContext(CrudContext)
  // selector
  const { item, createLoading, updateLoading } = useSelector((state: any) => {
    const { item, createLoading, updateLoading } = getState(state)
    return { item, createLoading, updateLoading }
  })
  const itemId = useMemo(() => item ? item._id : null, [item])

  const loading = useMemo(() =>
    (createLoading || updateLoading),
  [createLoading, updateLoading])
  // dispatch
  const dispatch = useDispatch()
  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action,
        item
      }
    }), [dispatch, types])
  const toRemove = useCallback((item: any) => {
    setAction(ActionTypeEnum.REMOVE, item)
  }, [setAction])
  const back = useCallback(() => {
    setAction(ActionTypeEnum.LIST, null)
  }, [setAction])

  // render
  return (
    <Grid container>
      <Grid item>
        <ButtonGroup
          size="small"
          variant="contained"
          color="primary">
          <RemoveButton
            actions={actions}
            itemId={itemId}
            toRemove={toRemove}
            item={item}
          />
          <SubmitButton
            actions={actions}
            loading={loading}
          />
          <BackButton
            actions={actions}
            back={back}
          />
        </ButtonGroup>
      </Grid>
    </Grid>
  )
}

const useStyles = makeStyles({
  root: {
    minWidth: 300, // 275,
    minHeight: 300
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
})