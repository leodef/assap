import React,
{
  useContext,
  useMemo,
  useCallback
} from 'react'
import {
  Table as TableMui,
  TableBody as TableBodyMui,
  TableCell,
  TableContainer,
  TableHead as TableHeadMui,
  TableRow as MuiTableRow,
  Paper,
  IconButton,
  // Fab,
  TableSortLabel,
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  Grid,
  TextField,
  InputAdornment,
  createStyles,
  Button
} from '@material-ui/core'
import {
  Edit as EditIcon,
  Visibility as VisibilityIcon,
  Delete as DeleteIcon,
  Search as SearchIcon,
  AddBox as AddBoxIcon
} from '@material-ui/icons'
import Skeleton from '@material-ui/lab/Skeleton'
import { CollectionContext } from '../../../../contexts/CollectionContext'
import { ActionTypeEnum } from '../../../../types/Crud'
import { useDispatch } from 'react-redux'
import { Pagination } from '../Pagination/Pagination'
import './Table.scss'

function getFieldsObj (fields: Array<any>): Array<TablePropsField> {
  return fields
    .filter((val: TablePropsField | String) => !!val)
    .map((val: TablePropsField | String) => {
      if (val instanceof TablePropsField) {
        return val
      } else {
        return new TablePropsField(val.toString())
      }
    })
}

export class TablePropsField {
  name: string | null = null;
  label: string | null = null;
  type: string = 'text';
  callback?: any;
  limit: number = 200

  constructor (
    pName: string | any,
    pLabel?: string,
    pType: string = 'text',
    pCallback?: any
  ) {
    if (typeof pName === 'string') {
      this.name = pName as string
      this.label = pLabel || pName || null
      this.type = pType
      this.callback = pCallback || this.callbackCallback.bind(this)
    } else {
      const {
        name,
        label,
        type,
        callback
      } = pName
      this.name = name
      this.label = label
      this.type = type
      this.callback = callback || this.callbackCallback.bind(this)
    }
  }

  callbackCallback (item: any, index: number, field: TablePropsField) {
    const { name } = (this || field)
    if (!name || !item) {
      return null
    }
    let result = item
    name.split('.').forEach((cField) => {
      result = result ? result[cField] : null
    })
    return result.toString()
  }

  getVal (item: any, index: number) {
    const callback = this.callback || this.callbackCallback
    const val = callback(item, index, this)
    return val && val.length > this.limit
      ? val.substr(0, this.limit - 3) + '...'
      : val
  }
}

/**
 * Tabela genérica para exibir coleção de elementos
 *   Table
 * @param {TableProps} props Propriedades
 * @return {React.Component} Componentes com tabela genérica para exibir coleção de elementos
 */
export const Table = (props: any) => {
  const classes = useStyles()
  const context = useContext(CollectionContext)

  const {
    disabled
  } = useMemo(() => props, [props])
  const {
    config,
    items,
    actions,
    types,
    loading,
    // pagination,
    sort,
    setSort,
    filter,
    setFilter
  } = useMemo(() => context, [context])

  const { label, align, fields, actionsLabel } = useMemo(
    () => config,
    [config])
  
  /*
  const limit = useMemo(() => pagination ? pagination.limit : null
    , [pagination])
  */
    // dispatch
  const dispatch = useDispatch()
  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action,
        item
      }
    }), [dispatch, types])

  const toNew = useCallback(() => {
    setAction(ActionTypeEnum.NEW, {})
  }, [setAction])

  const isFieldSorted = useCallback((field: any) => (!!sort && !!field && !!field.name)
    ? Object.keys(sort)[0] === field.name
    : false,
  [sort])

  const filterInput = useMemo(() => filter ? Object.values(filter)[0] || '' : '', [filter])

  const setFilterInput = useCallback((event: any) => {
    const filterInputVal = (event.target.value)
    const filterVal = {} as any
    fields.forEach((field: any) => {
      filterVal[field.name] = filterInputVal
    })
    setFilter(filterVal)
  }, [setFilter, fields])

  const setSortField = useCallback((field: any) => {
    const sortVal = (sort || {})
    const fieldName = field.name
    const value = ((sortVal[fieldName] || -1) * -1)
    // const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof Data) => {
    const newSort = {} as any
    newSort[fieldName] = value
    setSort(newSort)
  }, [sort, setSort])

  const order = useMemo(() => {
    if (!sort) { return undefined }
    const val = Number(Object.values(sort)[0])
    if (!val) { return undefined }
    return val < 0 ? 'desc' : 'asc'
  }, [sort])

  const fieldsObj = useMemo(() => getFieldsObj(fields), [fields])
  const showActions = useMemo(() => (
    actions && (
      actions.toRemove ||
      actions.toEdit ||
      actions.toShow
    )
  ), [actions])

  console.log('Table values', 'items', items, 'loading', loading)
  const body = useMemo(() => (
    loading
      ? (
        <React.Fragment>{
          Array.from({ length: 3 }, (val: any, key: number) =>
            (<WaitingTableRow
              key={key}
              fieldsObj={fieldsObj}
              showActions={showActions} />)
          )}
        </React.Fragment>
      )
    : (<React.Fragment>
      {items.filter((item: any) => Boolean(item)).map((item: any, key: number) =>
        (<TableRow
          item={item}
          showActions={showActions}
          fieldsObj={fieldsObj}
          align={align}
          key={key} />)
      )}
    </React.Fragment>) 
  ), [fieldsObj, items, loading, showActions, align])
  


  return (
    <TableContainer component={Paper} className={classes.root}>
      <Grid
        container
        spacing={1}
        direction="row"
        justify="flex-end"
        className={classes.filterRoot}
        >
        {(setFilter)
          ? (<Grid item>
            <TextField
              id="input-with-icon-grid"
              value={filterInput}
              onChange={(event: any) => setFilterInput(event) }
              disabled={disabled}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                )
              }} />
          </Grid>)
          : null
        }
        {(actions.toNew)
          ? (<Grid item>
            <Button
              aria-label="Novo"
              variant="contained"
              color="primary"
              onClick={() => toNew()}
              startIcon={<AddBoxIcon />} >
              Novo
            </Button>
          </Grid>)
          : null
        }
      </Grid>
      <TableMui aria-label={label}>
        <TableHeadMui>
          <MuiTableRow>
            { /* Colunas */}
            {fieldsObj.map((field, fieldIndex) =>
              (<TableCell
                key={fieldIndex}
                align={align}
                sortDirection={isFieldSorted(field) ? order : false}>
                <TableSortLabel
                  active={isFieldSorted(field)}
                  direction={isFieldSorted(field) ? order : 'asc'}
                  onClick={(event: any) => setSortField(field)}>
                  {field.label}
                  {isFieldSorted(field) ? (
                    <span className={classes.visuallyHidden}>
                      {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                    </span>
                  ) : null}
                </TableSortLabel>
              </TableCell>)
            )}
            { /* Coluna actions */}
            {(showActions)
              ? (<TableCell align={align}>
                {actionsLabel}
              </TableCell>)
              : null
            }
          </MuiTableRow>
        </TableHeadMui>

        <TableBodyMui>
          {body}
        </TableBodyMui>

      </TableMui>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center" >
        <Grid item >
          <Pagination />
        </Grid>
      </Grid>
      {/* (actions.toNew)
        ? (<Fab color="primary" size="medium" aria-label="add" onClick={() => toNew() }>
          <AddIcon />
        </Fab>)
        : null
      */}
    </TableContainer>
  )
}

const TableRow = React.memo((props: {
  item: any,
  key?: any,
  showActions: boolean,
  fieldsObj: Array<any>,
  align: any
}) => {
  TableRow.displayName = 'TableRow'
  const {
    item,
    showActions,
    fieldsObj,
    align
  } = useMemo(() => props, [props])

  return (<MuiTableRow>
    { /* Colunas */}
    {
      fieldsObj.map((field: TablePropsField, fieldIndex: number) => (
        <TableCell align={align} key={fieldIndex} >
          {field.getVal(item, fieldIndex)}
        </TableCell>
      ))
    }
    { /* Coluna actions */}
    {
      (showActions)
        ? (
          <TableCell align={align}>
            <ActionColumn
              item={item} />
          </TableCell>
        ) : null
    }
  </MuiTableRow>)
})

const WaitingTableRow = React.memo((props: {
  showActions: boolean,
  key?: any,
  fieldsObj: Array<any>
}) => {
  WaitingTableRow.displayName = 'WaitingTableRow'
  const {
    showActions,
    fieldsObj
  } = props
  const cellNum = useMemo(() => {
    const len = fieldsObj.length
    return showActions ? len + 1 : len
  }, [showActions, fieldsObj])
  return (<MuiTableRow>
    {
      Array.from({ length: cellNum }, (item: any, key: number) => (
        <TableCell align="center" key={key}>
          <Skeleton animation="wave" height={10} width="80%" />
        </TableCell>
      ))
    }
  </MuiTableRow>)
})
/**
 * Botões para linha da tabela genérica para exibir coleção de elementos
 *   Table.TableBody.Row.ActionColumn
 * @param {row: any, config: TableConfig, key: any } props Propriedades
 * @return {React.Component} Componente com botões para linha da tabela genérica para exibir coleção de elementos
 */
const ActionColumn = (props: { item: any }) => {
  // CollectionContext
  const { actions, types } = useContext(CollectionContext)
  // item
  const { item } = useMemo(() => props, [props])
 
  const dispatch = useDispatch()

  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action,
        item
      }
    }), [dispatch, types])

  const toShow = useCallback((item: any) => {
    setAction(ActionTypeEnum.SHOW, item)
  }, [setAction])

  const toEdit = useCallback((item: any) => {
    setAction(ActionTypeEnum.EDIT, item)
  }, [setAction])

  const toRemove = useCallback((item: any) => {
    setAction(ActionTypeEnum.REMOVE, item)
  }, [setAction])


  const showButton = useMemo(() => actions.toShow
    ? (<IconButton
      aria-label="Visualizar"
      onClick={() => toShow(item)}>
      <VisibilityIcon />
    </IconButton>)
    : null, [toShow, actions.toShow, item])

  const editButton = useMemo(() => actions.toEdit
    ? (<IconButton
      aria-label="Editar"
      onClick={() => toEdit(item)}>
      <EditIcon />
    </IconButton>)
    : null, [toEdit, actions.toEdit, item])

  const removeButton = useMemo(() => actions.toRemove
    ? (<IconButton
      aria-label="Remover"
      onClick={() => toRemove(item)}>
      <DeleteIcon />
    </IconButton>)
    : null, [toRemove, actions.toRemove, item])

  return (
    <React.Fragment>
      {showButton}
      {editButton}
      {removeButton}
    </React.Fragment>
  )
}


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: theme.spacing(1)
    },
    filterRoot: {
      width: '100%',
      marginTop: theme.spacing(1)
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1
    }
  })
)
