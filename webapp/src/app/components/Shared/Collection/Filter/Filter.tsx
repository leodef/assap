import React, { useMemo, useContext } from 'react'
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
  Grid,
  TextField
} from '@material-ui/core'
import {
  ExpandMore as ExpandMoreIcon
} from '@material-ui/icons'
import { CollectionContext } from '../../../../contexts/CollectionContext'
import './Filter.scss'
/**
 * Formulário genérico para filtar uma coleção de items
 * @param {any} props Propriedades
 * @return {React.Component} Componente com formulário genérico para filtar uma coleção de items
 */
export const Filter = (props: any) => {
  const { filter, label, disabled } = useMemo(() => props, [props])
  const { setFilter } = useContext(CollectionContext)
  return (
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="table-filter-content"
        id="table-filter-header">
        <Typography>
          {label}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <form noValidate autoComplete="off">
              <TextField
                id="filter"
                label={label}
                name="filter"
                value={filter || ''}
                disabled={disabled}
                onChange={(event: any) => setFilter(event, props)} />
            </form>
          </Grid>
        </Grid>
      </AccordionDetails>
    </Accordion>
  )
}
export default Filter
