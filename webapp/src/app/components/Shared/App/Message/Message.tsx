import React,
{
  useCallback,
  useMemo
} from "react"
import
  Snackbar
from "@material-ui/core/Snackbar"
import
  MuiAlert
from "@material-ui/lab/Alert"
import {
  useDispatch,
  useSelector
} from "react-redux"
import {
  getMessagesList,
  compare,
  MessageType
} from "../../../../types/Message"
import "./Message.scss"


/**
 * Encapsulamento dos alertas em tela
 * @param {any} props Propriedades
 * @return {React.Component} Componente com encapsulamento dos alertas em tela
 */
export const Message = (props: any) => {
  const dispatch = useDispatch()
  const messageState = useSelector((state: any) => state.message);
  const messages = useMemo(() => getMessagesList(messageState), [messageState])

  const removeMessage = useCallback((msg: any) =>
    dispatch({
      type: MessageType.REMOVE_MESSAGE,
      payload: msg
    }), [dispatch])
  const isOpen = useCallback(
    (message: any, index: number) => {
      return Boolean(
        messages.find(
          (msg) => compare(message, msg))
        )
    },
    [messages]
  )
  const handleClose = (message: any, index: any, event: any, reason: any) => {
    removeMessage(message)
  }

  return (
    <React.Fragment>
      {messages.map((message: any, index: number) => (
      <Snackbar
        key={`message_alert_${index}`}
        id={`message_alert_${index}`}
        open={ isOpen(message, index)}
        autoHideDuration={1000}
        onClose={
          (event: any, reason: any) => handleClose(message, index, event, reason)
        }> 
          <MuiAlert
            severity={message.variant}
            elevation={3}
            variant="filled"
          >
            {message.message.toString()}
          </MuiAlert>
        </Snackbar>
      ))}
    </React.Fragment>
  );
};
