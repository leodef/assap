import React, { useMemo } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { ProtectedComponent } from '../ProtectedComponent/ProtectedComponent'

/**
 * Mostra componentes filhos apenas se o usuário autenticado tive permissão
 * @param {any} props Propriedades
 * @return {React.Component} Componentes com redirecionamento para tela de login
 */
function RedirectToLogin (props: any) {
  return (<Redirect to='/login' />)
}
/**
 * Mostra rota apenas se o usuário autenticado tive permissão para acesala
 * @param {any} props Propriedades
 * @return {React.Component} Componentes com rota passada via propriedade child
 */
export function ProtectedRoute (props: any) {
  // ##!! const{ usuario } = useSelector( (state: any) => state.auth );
  const {
    component,
    ...routeProps
  } = useMemo(() => props, [props])
  return (
    <ProtectedComponent
      {...routeProps}
      other={
        <Route {...routeProps} component={RedirectToLogin}/>
      }>
      <Route {...routeProps} component={component}/>
    </ProtectedComponent>

  )
}
