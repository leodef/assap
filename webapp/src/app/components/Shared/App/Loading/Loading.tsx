import React, { useMemo } from 'react'
import { Backdrop, CircularProgress, makeStyles, Theme } from '@material-ui/core'
import { useSelector } from 'react-redux'
import './Loading.scss'

function getMainTasks (props: any): Array<any> {
  const tasks = props.tasks || []
  return tasks.filter((task: any) => !!task && !task.local)
}

/**
 * Informativo de progresso em tela
 * @param {any} props Propriedades
 * @return {React.Component} Componente com informativo de progresso
 */
export const Loading = (props: any) => {
  const classes = useStyles()
  const loading = useSelector((state: any) => state.loading)
  const tasks = useMemo(() => getMainTasks(loading), [loading])
  const open = useMemo(() => tasks.length > 0, [tasks])
  return (<Backdrop className={classes.backdrop} open={open} >
        <CircularProgress color="secondary"/>
      </Backdrop>)
}

const useStyles = makeStyles((theme: Theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

export default Loading
