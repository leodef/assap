import React, { useCallback } from 'react'
import {
  Link as RouterLink
} from 'react-router-dom'
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  IconButton,
  makeStyles,
  createStyles,
  Theme
} from '@material-ui/core'
import {
  Brightness7 as LightIcon,
  Brightness4 as DarkIcon
} from '@material-ui/icons'
import { ThemeType, ThemeValue } from '../../../types/Theme'
import { useDispatch, useSelector } from 'react-redux'

/**
 * Barra de navegação superior para usuários não logados
 * @param {any} props Propriedades
 * @return {React.Component} Componente com barra de navegação superior para usuários não logados
 */
export const Navbar = React.memo((props: any) => {
  Navbar.displayName = 'Navbar'
  const classes = useStyles()
  const dispatch = useDispatch()
  const tema = useSelector((state: any) => state.theme.value)
  const onClickThemeButton = useCallback( (event: any) => dispatch({
      type: ThemeType.TOGGLE 
    }),
    [dispatch]
  )
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" noWrap className={classes.title}>
          Planning Route
        </Typography>
        <div className={classes.grow} />
        <div className={classes.sectionDesktop}>
          {/* Icone tema */}
          <IconButton aria-label="Tema" color="inherit" onClick={onClickThemeButton}>
              {
                tema === ThemeValue.dark
                ? (<LightIcon />)
                : (<DarkIcon />)
              }
          </IconButton>
          <Button color="inherit" component={RouterLink} to="/">
              Login
          </Button>
        </div>

       
      </Toolbar>
    </AppBar>
  )
})

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block'
      }
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex'
      }
    }
  })
)

export default Navbar
