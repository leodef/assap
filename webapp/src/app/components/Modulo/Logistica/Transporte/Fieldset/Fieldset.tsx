import
React,
{
  useCallback,
  useMemo,
  useState
} from 'react'
import {
  Grid,
} from '@material-ui/core'
import {
  useField
} from 'formik'
import {
  getFieldName
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  Autocomplete as AutocompleteVeiculo
} from '../../../DadosEmpresa/Veiculo/Autocomplete/Autocomplete'
import {
  LocalAutocomplete as LocalAutocompleteVeiculo
} from '../../../DadosEmpresa/Veiculo/LocalAutocomplete/LocalAutocomplete'
import { TIPO_FUNCAO_VEICULO } from '../../../../../enums/tipo-funcao-veiculo'
import './Fieldset.scss'

/**
 * Area de inputs para Funcionario
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Funcionario
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  const { disabled } = useMemo(() => props, [props])
  const fields = useMemo(() => {
    return {
      veiculo: {
        name: getFieldName('veiculo', props),
        label: 'Veículo'
      },
      carreta: {
        name: getFieldName('carreta', props),
        label: 'Carreta'
      }
    }
  }, [props])

  // carreta
  const [, carretaMeta, carretaHelpers] = useField(
    fields.carreta.name
    )
  const [ carretaInputValue, setCarretaInputValue] = useState('')
  const onCarretaInputValueChange = useCallback(
    (event: object, value: string, reason: string) =>
      setCarretaInputValue(value),
    [setCarretaInputValue]
  )
  const setCarretaValue = useCallback(
    (value: any) => carretaHelpers.setValue(value),
    [carretaHelpers]
  )
  const carretaValue = useMemo(
    () => carretaMeta.value || null,
    [carretaMeta]
  )

  // veiculo
  const [, veiculoMeta, veiculoHelpers] = useField(
    fields.veiculo.name
    )
  const setVeiculo = useCallback(
    (value: any) => {
      veiculoHelpers.setValue(value)
      setCarretaValue(null)
      setCarretaInputValue('')
    },
    [
      veiculoHelpers,
      setCarretaValue,
      setCarretaInputValue
    ]
  )
  const veiculo = useMemo(
    () => veiculoMeta.value || null,
    [veiculoMeta]
  )

  // carretas
  const carretas = useMemo(
    () => ((veiculo && veiculo.compatibilidade)
      ? veiculo.compatibilidade
      : []),
      [veiculo])

  //  veiculo / carreta
  const carreta = useMemo(
    () => carretas && carretaValue
      ? carretas.find((val: any) => (val._id || val) === (carretaValue._id || carretaValue))
      : null,
    [carretaValue, carretas]
  )

  // filter cavalo
  const filterOptionsCavalo = useCallback(
    (option) => 
      option ? option.filter((opt: any) => (
        opt && opt.funcao === TIPO_FUNCAO_VEICULO.CAVALO
      )) : [],
    []
  )

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={6}>
        <AutocompleteVeiculo
          id="veiculo-entrega-autocomplete-veiculo"
          size="small"
          fullWidth
          disabled={disabled}
          value={veiculo}
          onChange={setVeiculo}
          filterOptions={filterOptionsCavalo}
          />
      </Grid>

      <Grid item xs={12} sm={6}>
        <LocalAutocompleteVeiculo
          id="veiculo-entrega-autocomplete-carreta"
          size="small"
          label="Carreta"
          fullWidth
          disabled={disabled}
          value={carreta}
          onChange={setCarretaValue}
          inputValue={carretaInputValue}
          onInputChange={onCarretaInputValueChange}
          options={carretas}
        />
      </Grid>

    </Grid>
  )
})
