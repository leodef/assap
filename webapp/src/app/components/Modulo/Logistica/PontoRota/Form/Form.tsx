import React,
{
  useCallback, useMemo
} from 'react'
import {
  makeStyles,
  Card,
  CardContent,
  CardActions,
  Button,
  createStyles,
  // eslint-disable-next-line no-unused-vars
  Theme, ButtonGroup
} from '@material-ui/core'
import {
  Send as SendIcon
} from '@material-ui/icons'
// eslint-disable-next-line no-unused-vars
import {
  withFormik,
  // eslint-disable-next-line no-unused-vars
  FormikHelpers
} from 'formik'
import { Fieldset } from '../Fieldset/Fieldset'
import './Form.scss'
import {
  PontoRotaSchema as validationSchema,
  initialValues
} from '../../../../../types/API/Logistica/PontoRota'
import _ from 'lodash'
import { ModalTitle } from '../../../../Shared/Utils/ModalBreadcrumbs/ModalTitle'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 400, // 275
      minHeight: 400
    },
    title: {
      fontSize: 14
    }
  })
)

/**
 * Componente Form
 * @param {any} props Propriedades
 * @return {React.Component} Componente Form
 */
export const Form = (props: any) => {
  const { save, item } = props
  const mapPropsToValues = useCallback(
    (props: any) => _.defaults(item, initialValues),
    [item]
  )
  const WithFormik = withFormik({
    mapPropsToValues,
    validationSchema,
    handleSubmit: (values: any, formikHelpers: FormikHelpers<any>) => {
      const { setSubmitting } = formikHelpers
      setSubmitting(false)
      save(values)
    }
  })(FormBody)
  return (<WithFormik
    {...props}
    name={null}
    propsToValues={mapPropsToValues} />)
}

/**
 * Componente FormBody
 * @param {any} props Propriedades
 * @return {React.Component} Componente FormBody
 */
export const FormBody = (props: any) => {
  const classes = useStyles()
  const { onClose, modalHistory } = useMemo(() => props, [props])

  return (<form onSubmit={props.handleSubmit}>
    <Card className={classes.root}>
      <CardContent>
      <ModalTitle
          modalHistory={modalHistory}
          title="Parada" />
        <Fieldset
          {...props} />
      </CardContent>
      <CardActions>
        <ButtonGroup
          size="small"
          variant="contained"
          color="primary">
          <Button
            type="submit"
            size="small"
            variant="contained"
            color="primary"
            startIcon={<SendIcon />}>
          Salvar
          </Button>
          <Button
            size="small"
            variant="contained"
            color="primary"
            onClick={onClose}>
          Voltar
          </Button>
        </ButtonGroup>
      </CardActions>
    </Card>
  </form>)
}
