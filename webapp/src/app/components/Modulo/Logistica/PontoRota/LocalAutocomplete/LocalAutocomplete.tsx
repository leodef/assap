import
React,
{
  useCallback,
  useMemo
} from 'react'
import MuiAutocomplete from '@material-ui/lab/Autocomplete'
import { TextField } from '@material-ui/core'
import './LocalAutocomplete.scss'
import { PontoRota } from '../../../../../types/API/Logistica/PontoRota'

/**
 * Visualização de item do tipo ponto de rota
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo ponto de rota
 */
export const LocalAutocomplete = (props: any) => {
  const { value, onChange, disabled, ...otherProps } = useMemo(() => props, [props])

  const { label } = useMemo(() => {
    return { label: (props.label || 'Ponto de rota') }
  }, [props.label])
  const getOptionLabel = (option: any) => PontoRota.getSubTitulo(option)

  const getOptionSelected = useCallback(
    (option: any, value: any) => 
      Boolean(
        option &&
        value &&
        option._temp === value._temp
      ),
    [],
  )

  return (<MuiAutocomplete
    {...otherProps}
    label={label}
    onChange={(event:any, inputValue: any) => {
      if(onChange) {
        onChange(inputValue)
      }
    }}
    value={value}
    getOptionLabel={getOptionLabel}
    getOptionSelected={getOptionSelected}
    renderInput={(params: any) => {
      return (
      <TextField
        variant="outlined"
        label={label}
        disabled={disabled}
        {...params}
      />
    )}}
  />)
}
