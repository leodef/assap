/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { FilialFieldset } from './FilialFieldset'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<FilialFieldset />, div)
  ReactDOM.unmountComponentAtNode(div)
})
