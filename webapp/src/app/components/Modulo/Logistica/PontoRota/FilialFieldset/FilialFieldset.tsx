import React, { useCallback, useMemo, useState } from "react";
import {
  createStyles,
  FormControlLabel,
  Grid, 
  makeStyles, 
  Switch, 
  Theme} from "@material-ui/core";
import {
  useField
} from "formik";
import {
  getFieldName
} from "../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField";
import {
  LocalAutocomplete as LocalAutocompleteFilial
} from "../../../DadosEmpresa/Filial/LocalAutocomplete/LocalAutocomplete";
import {
  Autocomplete as AutocompleteFilial
} from "../../../DadosEmpresa/Filial/Autocomplete/Autocomplete";
import "./FilialFieldset.scss";
/*
      parceiroEntrega: params.depedencies.ParceiroEntrega.schema,
        parceiro: { type: Schema.Types.ObjectId, ref: 'Parceiro' },
        contato: { type: Schema.Types.ObjectId, ref: 'Contato' },
*/
/**
 * Area de inputs para Funcionario
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Funcionario
 */
export const FilialFieldset = (props: any) => {
  const classes = useStyles();
  const { disabled } = useMemo(() => props, [props]);
  const parceiroEntrega = useMemo(
    () => props.parceiroEntrega
      ? props.parceiroEntrega.parceiro
      : null, [props.parceiroEntrega])
  const [filialEmpresaInputValue, setFilialEmpresaInputValue] = useState('')
  const [filialParceiroInputValue, setFilialParceiroInputValue] = useState('')
  const fields = useMemo(() => {
    return {
      parceiro: {
        name: getFieldName("parceiro", props),
        label: "Parceiro",
      },
      filial: {
        name: getFieldName("filial", props),
        label: "Contato",
      },
      localizacao: {
        name: getFieldName("localizacao", props),
        label: "Localização",
      },
    };
  }, [props]);

  // Parceiro
  const [, parceiroMeta, parceiroHelpers] = useField(fields.parceiro.name);
  const setParceiroValue = useCallback(
    (value: any) => parceiroHelpers.setValue(value),
    [parceiroHelpers]
  );
  const parceiroValue = useMemo(
    () => parceiroMeta.value,
    [parceiroMeta.value]);

  // Localização
  const [, , localizacaoHelpers] = useField(fields.localizacao.name);
  
  // Filial
  const [, filialMeta, filialHelpers] = useField(fields.filial.name);
  const setFilialValue = useCallback(
      (value: any) => {
        filialHelpers.setValue(value)
        if(value && value.localizacao) {
          localizacaoHelpers.setValue(value.localizacao)
        }
      },
      [filialHelpers, localizacaoHelpers]
    );
  const filialValue = useMemo(
      () => filialMeta.value,
      [filialMeta.value]);
  const filialParceiroValue = useMemo(
      () => parceiroValue ? filialValue : null,
      [parceiroValue, filialValue]);
  const filialEmpresaValue = useMemo(
      () => parceiroValue ?  null : filialValue,
      [parceiroValue, filialValue]);
  
  // onChange
  const onFilialValueChange = useCallback(
        (value: any) => {
          setFilialValue(value)
        },
        [setFilialValue]
      )
  const onParceiroValueChange = useCallback(
        (value: any) => {
          setFilialValue(null)
          setFilialEmpresaInputValue('')
          setFilialParceiroInputValue('')
          setParceiroValue(value)
        } , [
          setFilialEmpresaInputValue,
          setFilialParceiroInputValue,
          setFilialValue,
          setParceiroValue]
      )
  const filiais = useMemo(
      () => parceiroValue ? parceiroValue.filiais || [] : [],
      [parceiroValue])
  
  //

  const origemFilial = useMemo(
    () => Boolean(parceiroValue),
    [parceiroValue])
  const updateOrigemFilial = useCallback(
    () => onParceiroValueChange(parceiroValue ? null : parceiroEntrega),
    [onParceiroValueChange, parceiroValue, parceiroEntrega]
  )
  const origemFilialLabel = useMemo(
    () => (parceiroValue ? 'Filial do parceiro' : 'Filial da empresa'),
    [parceiroValue])


  return (
    <Grid container spacing={2}>

        <Grid item xs={12} sm={6}>
        <FormControlLabel
            id="ponto_rota_filial_origem"
            control={
              <Switch
                checked={origemFilial}
                onChange={updateOrigemFilial}
                color="primary"
              />
            }
            label={origemFilialLabel}
          />
      </Grid>

      <Grid item xs={12} sm={6}>
        <LocalAutocompleteFilial
          className={parceiroValue ? classes.show: classes.hidden }
          id="ponto-rota-filial-autocomplete-filial"
          size="small"
          fullWidth
          onChange={onFilialValueChange}
          value={filialParceiroValue}
          disabled={disabled}
          options={filiais}
          inputValue={filialParceiroInputValue}
          onInputChange={
            (event: any, value: string) => setFilialParceiroInputValue(value)
          }
          placeholder="Empresa / Parceiro"
        />
        <AutocompleteFilial
          className={parceiroValue ? classes.hidden : classes.show}
          id="funcionario-autocomplete-filial"
          size="small"
          fullWidth
          onChange={onFilialValueChange}
          value={filialEmpresaValue}
          inputValue={filialEmpresaInputValue}
          onInputChange={
            (event: any, value: string) => setFilialEmpresaInputValue(value)}
          placeholder="Filiais da empresa"
        />
      </Grid>
    </Grid>);
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxHeight: 600
    },
    hidden: {
      display: "none",
    },
    show: {
      display: "block",
    }
  })
);
