import React, { useCallback, useMemo, useState } from "react";
import {
  Grid,
  makeStyles,
  createStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  BottomNavigationAction,
  BottomNavigation,
  AccordionSummary,
  Typography,
  Accordion,
  AccordionDetails,
  Checkbox,
  FormControlLabel
} from "@material-ui/core";
import {
  EditLocation as EditLocationIcon,
  Settings as SettingsIcon,
  ExpandMore as ExpandMoreIcon,
} from "@material-ui/icons";
import { useField } from "formik";
import {
  TextField,
  getFieldName,
  getFormName,
} from "../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField";
import {
  Fieldset as LocalizacaoFieldset
} from "../../../Admin/Localizacao/Fieldset/Fieldset";
import {
  TempFieldset
} from "../../../../Shared/Crud/Fieldset/TempFieldset/TempFieldset";
import {
  TituloDescFieldset
} from "../../../../Shared/Crud/Fieldset/TituloDescFieldset/TituloDescFieldset";
import {
  FilialFieldset
} from "../FilialFieldset/FilialFieldset";
import {
  CargaDescargaFieldset
} from "../CargaDescargaFieldset/CargaDescargaFieldset";
import {
  LocalAutocomplete
} from "../LocalAutocomplete/LocalAutocomplete";
import {
  PontoRota
} from "../../../../../types/API/Logistica/PontoRota";
import "./Fieldset.scss";

/**
 * Componente Fieldset
 * @param {any} props Propriedades
 * @return {React.Component} Componente Fieldset
 */
export const Fieldset = (props: any) => {
  const classes = useStyles();
  const { item, disabled, pontos } = useMemo(() => props, [props]);
  const { localizacao, limite } = useMemo(() => item || {}, [item]);
  const [tab, setTab] = useState(0);
  // Automated
  const automated = useMemo(() => ({
    titulo: PontoRota.generateTitulo(props.values),
    desc: PontoRota.generateDesc(props.values)
  }),
  [props.values])

  // Expanded
  const [
    tituloDescAccordionExpanded,
    setTituloDescAccordionExpanded,
  ] = useState(false as boolean);
  const [
    cargaDescargaAccordionExpanded,
    setCargaDescargaAccordionExpanded,
  ] = useState(false as boolean);
  const [
    filialAccordionExpanded,
    setFilialAccordionExpanded
  ] = useState(false as boolean);

  const fields = useMemo(() => {
    return {
      pontoRota: {
        name: getFormName(props),
        label: "Ponto rota",
      },
      titulo: {
        name: getFieldName("titulo", props),
        label: "Título",
      },
      desc: {
        name: getFieldName("desc", props),
        label: "Descrição",
      },
      ordem: {
        name: getFieldName("ordem", props),
        label: "Ordem",
      },
      limite: {
        name: getFieldName("limite", props),
        label: "Limite",
      },
      localizacao: {
        name: getFieldName("localizacao", props),
        label: "Localização",
      },
      status: {
        name: getFieldName("status", props),
        label: "Status",
      },
      usarFilial: {
        name: getFieldName("usarFilial", props),
        label: "Usar filial",
      },
      filial: {
        name: getFieldName("filial", props),
        label: "Filial",
      },
      parceiro: {
        name: getFieldName("parceiro", props),
        label: "Parceiro",
      },
      anterior: {
        name: getFieldName("anterior", props),
        label: "Paradas anteriores",
      },
    };
  }, [props]);

  // Ponto Rota
  const pontoRota = useMemo(() => props.values, [props.values])
  
  // Filial
  const [, , filialHelpers] = useField(
    fields.filial.name
  );
  const [, , parceiroHelpers] = useField(
    fields.parceiro.name
  );
  const [, usarFilialMeta, usarFilialHelpers] = useField(
    fields.usarFilial.name
  );
  const setFilialValue = useCallback(
    (value: any) => filialHelpers.setValue(value),
    [filialHelpers]
  );
  const setParceiroValue = useCallback(
    (value: any) => parceiroHelpers.setValue(value),
    [parceiroHelpers]
  );
  const setUsarFilialValue = useCallback(
    (value: any) => {
      const val = Boolean(value);
      usarFilialHelpers.setValue(val);
      setFilialAccordionExpanded(val);
      if(!val) {
        setFilialValue(null)
        setParceiroValue(null)
      }

    },
    [
      usarFilialHelpers,
      setFilialAccordionExpanded,
      setFilialValue,
      setParceiroValue
    ]
  );
  const usarFilialValue = useMemo(
    () => Boolean(usarFilialMeta.value), [
    usarFilialMeta.value,
  ]);
  const filialDisabled = useMemo(
    () => !usarFilialValue, [usarFilialValue]);
  const filialLabel = useMemo(
    () =>
      filialDisabled
        ? "Selecionar a localização de uma filial ?"
        : "Uso da localização de uma filial",
    [filialDisabled]
  );
  const localizacaoDisabled = useMemo(() => usarFilialValue, [usarFilialValue]);

  // Anterior
  const [, anteriorMeta, anteriorHelpers] = useField(fields.anterior.name);
  const setAnteriorValue = useCallback(
    (value: any) => anteriorHelpers.setValue(value),
    [anteriorHelpers]
  );
  const anteriorValue = useMemo(
    () => anteriorMeta.value || [],
    [anteriorMeta.value]);
  const anteriorOptions = useMemo(
    () => (pontos || []).filter((pto: any) =>
      !PontoRota.compare(
      pontoRota,
      pto) 
  ), [
    pontos,
    pontoRota
  ]);

  return (
    <React.Fragment>
      {/*<Paper elevation={1} className={classes.paper}>*/}
      <Grid  container className={classes.root}>
        <Grid
        item
        xs={12}
        container
        className={classes.container}>
          <Grid
            container
            item
            spacing={1}
            className={tab === 0 ? classes.show : classes.hidden}
          >
            <Grid
              item
              container
              xs={12}
              spacing={1}>
              <Grid item xs={2}>
                <TextField
                  name={fields.ordem.name}
                  type="number"
                  label={fields.ordem.label}
                  disabled
                />
              </Grid>
              {/* Prev */}
              <Grid item xs={10}>
                <LocalAutocomplete
                  size="small"
                  id="pronto-rota-autocomplete-prev"
                  label={fields.anterior.label}
                  fullWidth
                  multiple
                  defaultValue={[]}
                  filterSelectedOptions
                  onChange={setAnteriorValue}
                  value={anteriorValue}
                  options={anteriorOptions}
                />
              </Grid>
            </Grid>

            {/* TituloDesc */}
            <Grid item xs={12}>
              <Accordion
                expanded={tituloDescAccordionExpanded}
                onChange={(event: any, isExpanded: boolean) =>
                  setTituloDescAccordionExpanded(!tituloDescAccordionExpanded)
                }
              >
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-label="Titulo e Descrição"
                  aria-controls="ponto-rota-fieldset-accord-titulo-desc-content"
                  id="ponto-rota-fieldset-accord-titulo-desc-header"
                >
                  <Typography className={classes.heading}>
                    Titulo e Descrição
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Grid container>
                    <TituloDescFieldset
                      {...props}
                      automated={automated}/>
                  </Grid>
                </AccordionDetails>
              </Accordion>
            </Grid>
            {/* Filial */}
            <Grid item xs={12}>
              <Accordion
                expanded={filialAccordionExpanded}
                onChange={(event: any, isExpanded: boolean) =>
                  setFilialAccordionExpanded(
                    !filialAccordionExpanded && !filialDisabled
                  )
                }
              >
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-label={filialLabel}
                  aria-controls="ponto-rota-fieldset-accord-filial-content"
                  id="ponto-rota-fieldset-accord-filial-header"
                  IconButtonProps={{ disabled: filialDisabled }}
                >
                  <FormControlLabel
                    aria-label={filialLabel}
                    onClick={(event) => event.stopPropagation()}
                    onFocus={(event) => event.stopPropagation()}
                    onChange={(event, checked) => setUsarFilialValue(checked)}
                    checked={usarFilialValue}
                    control={<Checkbox />}
                    label={filialLabel}
                  />
                </AccordionSummary>
                <AccordionDetails>
                  <FilialFieldset
                    {...props} />
                </AccordionDetails>
              </Accordion>
            </Grid>
            {/* Carga / Descarga */}
            <Grid item xs={12}>
              <Accordion
                expanded={cargaDescargaAccordionExpanded}
                onChange={(event: any, isExpanded: boolean) =>
                  setCargaDescargaAccordionExpanded(
                    !cargaDescargaAccordionExpanded
                  )
                }
              >
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-label="Carga / Descarga"
                  aria-controls="ponto-rota-fieldset-accord-limite-content"
                  id="ponto-rota-fieldset-accord-limite-header"
                >
                  Carga / Descarga
                </AccordionSummary>
                <AccordionDetails>
                  <CargaDescargaFieldset
                    {...props}
                    name={fields.limite.name}
                    item={limite} />
                </AccordionDetails>
              </Accordion>
            </Grid>
          </Grid>
          <Grid
            container
            item
            spacing={2}
            className={tab === 1 ? classes.show : classes.hidden}
          >
            <Grid item sm={12} >
              <LocalizacaoFieldset
                {...props}
                autocomplete
                name={fields.localizacao.name}
                item={localizacao}
                disabled={disabled}
              />
            </Grid>
          </Grid>
        </Grid>

        <BottomNavigation
          value={tab}
          onChange={(event: any, newValue: any) => setTab(newValue)}
          showLabels
          className={classes.bottomNavigation}
        >
          <BottomNavigationAction
            label="Configuração"
            icon={<SettingsIcon />}
          />
          <BottomNavigationAction
            label="Localização"
            disabled={localizacaoDisabled}
            icon={<EditLocationIcon />}
          />
        </BottomNavigation>
      {/* </Paper> */}
      </Grid>
      <TempFieldset {...props} _temp />
    </React.Fragment>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(1),
      textAlign: "center",
      color: theme.palette.text.secondary,
      overflow: "auto",
      maxWidth: 650,
    },
    root: {
      width: 700,
      height: 400
      // width: '100%'
    },
    container: {
      width: "100%",
      minHeight: 300,
      maxHeight: 400,
      overflowY: "auto",
      padding: theme.spacing(1)
    },
    bottomNavigation: {
      width: "100%",
    },
    hidden: {
      display: "none",
    },
    show: {
      display: "block",
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: "33.33%",
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
  })
);
