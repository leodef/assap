import React,
{
  useCallback, useMemo
} from 'react'
import {
  Modal as MuiModal,
  makeStyles,
  createStyles,
  // eslint-disable-next-line no-unused-vars
  Theme
} from '@material-ui/core'
import { Form } from '../Form/Form'
import './Modal.scss'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modalPaper: {
      position: 'absolute',
      minWidth: 300,
      minHeight: 300,
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
      // boxShadow: theme.shadows[5],
      // padding: theme.spacing(2, 4, 3),
    }
  })
)

/**
 * Componente Modal
 * @param {any} props Propriedades
 * @return {React.Component} Componente Modal
 */
export const Modal = (props: any) => {
  const classes = useStyles()
  let {
    save,
    setOpen,
    isOpen,
    setItem,
    item,
    setModalInfo,
    modalInfo
  } = useMemo(() => props, [props])
  const isOpenModalInfo = useMemo(
    () => modalInfo ? modalInfo.isOpen : isOpen,
    [modalInfo, isOpen])
  const itemModalInfo = useMemo(
    () => modalInfo ? modalInfo.item : item,
    [modalInfo, item])

  const open = useMemo(() => Boolean(isOpenModalInfo), [isOpenModalInfo])
  const onClose = useCallback(() => {
    if (setOpen) { setOpen(false) }
    if (setItem) { setItem(null) }
    if (setModalInfo) { setModalInfo({ open: false, item: null }) }
  }, [setOpen, setItem, setModalInfo])
  const salvarPontoEntrega = useCallback((values: any) => {
    if (setOpen) { setOpen(false) }
    if (setItem) { setItem(values) }
    if (setModalInfo) { setModalInfo({ open: false,  values }) }
    save(values)
  }, [save, setOpen, setItem, setModalInfo])

  return (
    <MuiModal
      open={open}
      onClose={onClose}
      aria-labelledby="Parada"
      aria-describedby="Parada">
      <div className={classes.modalPaper}>
        <Form
          {...props}
          save={salvarPontoEntrega}
          onClose={onClose}
          item={itemModalInfo} />
      </div>
    </MuiModal>)
}
