import
React,
{
  useCallback,
  useMemo,
  useState
} from 'react'
import {
  FormControlLabel,
  Grid,
  InputAdornment,
  Switch,
  TextField as MuiTextField
} from '@material-ui/core'
import {
  useField
} from 'formik'
import {
  getFieldName,
  TextField
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'

import './CargaDescargaFieldset.scss'
/*
  switch  Carga e descarga
  limite: {
    tara: 0,
    peso: Input
    volume: Disabled  peso * produto.densidade 
  }
*/
/**
 * Area de inputs para carga e descarga
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para carga e descarga
 */
export const CargaDescargaFieldset = React.memo((props: any) => {
  CargaDescargaFieldset.displayName = 'CargaDescargaFieldset'
  const {
    disabled,
    produto,
    statusRota,
    limite
  } = useMemo(() => props, [props])

  const statusRotaLimite = useMemo(
    () => statusRota ? statusRota.limite : null,
    [statusRota])

  const helperText = useMemo(
    () => {
      const statusRotaPesoDesc = statusRotaLimite && statusRotaLimite.peso
        ? `${statusRotaLimite.peso > 0 ? 'Carregado' : 'Descarregado'}: ${statusRotaLimite.peso}`
        : null
      const statusRotaVolumeDesc = statusRotaLimite && statusRotaLimite.volume
      ? `${statusRotaLimite.volume > 0 ? 'Carregado' : 'Descarregado'}: ${statusRotaLimite.volume}`
        : null
      const limitePesoDesc = limite && limite.peso
        ? `Capacidade de ${limite.peso}`
        : null
      const limiteVolumeDesc = limite && limite.volume
      ? `Capacidade de ${limite.volume}`
        : null
      const pesoDesc = [
        statusRotaPesoDesc,
        limitePesoDesc
      ].filter(val => Boolean(val)).join(', ')
      const volumeDesc = [
        statusRotaVolumeDesc,
        limiteVolumeDesc
      ].filter(val => Boolean(val)).join(', ')
      return ({ peso: pesoDesc, volume: volumeDesc })
  },
  [
    statusRotaLimite,
    limite
  ])

  const max = useMemo(() => ({
    peso: limite && limite.peso ? limite.peso : null,
    volume: limite && limite.volume ? limite.volume : null
  }), [limite])

  const fields = useMemo(() => {
    return {
      tara: {
        name: getFieldName("tara", props),
      },
      peso: {
        name: getFieldName("peso", props),
        label: "Peso"
      },
      volume: {
        name: getFieldName("volume", props),
        label: "Volume"
      },
    }
  }, [props])

    // peso
    const [, pesoMeta, pesoHelpers] = useField(
      fields.peso.name
      )
    const peso = useMemo(
        () => pesoMeta.value || null,
        [pesoMeta]
      )
    // carregamento
    const [carregamento, setCarregamento] = useState(peso >= 0)
    const carregamentoLabel = useMemo(() => carregamento
      ? 'Carga'
      : 'Descarga',
      [carregamento])
    const carregamentoNum = useMemo(() => carregamento
      ? 1
      : -1,
      [carregamento])

  // volume
  const [, volumeMeta, volumeHelpers] = useField(
      fields.volume.name
      )
  const setVolume = useCallback(
      (value: any) => volumeHelpers.setValue(value),
      [volumeHelpers]
    )
  const setVolumeInput = useCallback(
      (event: any) => {
        const value = Number(event.target.value)
        setVolume(value * carregamentoNum)
      },
      [setVolume, carregamentoNum]
    )
  const volume = useMemo(
      () => volumeMeta.value || null,
      [volumeMeta]
    )
  const volumeInput = useMemo(
      () => Math.abs(Number(volume)),
      [volume]
    )
  const volumeDisabled = useMemo(
    () => disabled || Boolean( produto &&  produto.densidade),
    [disabled, produto])

  // peso
  const setPeso = useCallback(
    (value: any) => pesoHelpers.setValue(value),
    [pesoHelpers]
  )
  const setPesoInput = useCallback(
    (event: any) => {
      const value = Number(event.target.value)
      setPeso(value * carregamentoNum)
      if(produto && produto.densidade) {
        const val = value * produto.densidade
        if(val !== volume) {
          setVolume(val * carregamentoNum)
        }
      }
    },
    [
      setPeso,
      carregamentoNum,
      produto,
      setVolume,
      volume
    ]
  )
  const pesoInput = useMemo(
    () => Math.abs(Number(peso)),
    [peso]
  )

  const updateCarregamento = useCallback(
    (event) => {
      const checked = event.target.checked
      const num = checked ? 1 : -1
      setCarregamento(checked)
      setPeso(pesoInput * num)
      setVolume(volumeInput * num)
    },
    [
      setCarregamento,
      setVolume,
      setPeso,
      volumeInput,
      pesoInput
    ],
  )

  return (<React.Fragment>
    <TextField
      id="ponto_rota_carga_descarga_tara"
      name={fields.tara.name}
      type="hidden"
      value={0}
    />
    <Grid item container xs={12}>
      <Grid item xs={4}>
        <FormControlLabel
            id="ponto_rota_carga_descarga_carregamento"
            control={
              <Switch
                checked={carregamento}
                onChange={updateCarregamento}
                color="primary"
              />
            }
            label={carregamentoLabel}
          />
      </Grid>
      <Grid item xs={4}>
        <MuiTextField
          id="ponto_rota_carga_descarga_peso"
          type="number"
          label={fields.peso.label}
          helperText={helperText.peso}
          inputProps={{
            max: max.peso
          }}
          disabled={disabled}
          value={pesoInput}
          onChange={setPesoInput}
          InputProps={{
            startAdornment: <InputAdornment position="start">
              <i>Kg</i>
            </InputAdornment>,
          }}
        />
      </Grid>
      <Grid item xs={4}>
        <MuiTextField
          id="ponto_rota_carga_descarga_volume"
          type="number"
          label={fields.volume.label}
          helperText={helperText.volume}
          inputProps={{
            max: max.peso
          }}
          disabled={volumeDisabled}
          value={volumeInput}
          onChange={setVolumeInput}
          InputProps={{
            startAdornment: <InputAdornment position="start">
              <i>L</i>
            </InputAdornment>,
          }}
        />
      </Grid>
    </Grid>
  </React.Fragment>);
})
