/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { CargaDescargaFieldset } from './CargaDescargaFieldset'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<CargaDescargaFieldset />, div)
  ReactDOM.unmountComponentAtNode(div)
})
