import
React,
{
  useCallback,
  useState,
  useContext,
  useMemo,
  useEffect
} from 'react'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles,
  Stepper as MuiStepper,
  Step,
  StepButton,
  Grid
} from '@material-ui/core'
import { StepperActions } from './StepperActions/StepperActions'
import { StepperContent } from './StepperContent/StepperContent'
import './Stepper.scss'
import { useSelector } from 'react-redux'
import { CrudContext } from '../../../../../contexts/CrudContext'
/**
 * Area de inputs para Entrega
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Entrega
 */

export const Stepper = React.memo((props: any) => {
  Stepper.displayName = 'Stepper'
  const classes = useStyles()

  const { getState } = useContext(CrudContext)
  const calculoRota = useSelector(state => getState(state).calculoRota)
  const pontos = useMemo(() => props.values.rota ? props.values.rota.pontos || [] : [], [props.values.rota])
  const pontsLength = useMemo(() => pontos ? pontos.length || 0 : 0, [pontos])
  const steps = useMemo(() => ([
    { label: 'Configuração', disabled: false },
    { label: 'Rota', disabled: false },
    { label: 'Mapa', disabled: (pontsLength === 0) }
  ]), [pontsLength])
  /* hook:completed:Stepper.tsx:41 */
  const [completed, setCompleted] = useState({} as any)
  // const [disabled, setDisabled] = useState({} as any)
  const [activeStep, setActiveStep] = useState(calculoRota ? 1 : 0)
  // steps.findIndex((step: string, index: number) => !completed[index]) || 0)
  const updateActiveStep = useCallback(
    (val: number) => {
      localStorage.setItem('entrega_stepper', String(val))
      setActiveStep(val)
    },
    [setActiveStep],
  )
  
  useEffect(() => {
    const entregaStepper = localStorage.getItem('entrega_stepper');
    if(
      entregaStepper &&
      Number.isInteger(entregaStepper) &&
      Number(entregaStepper) !== activeStep) {
        setActiveStep(Number(entregaStepper))
    }
    return () => {
      localStorage.removeItem('entrega_stepper')
    }
  }, [setActiveStep, activeStep])
  /*
  useEffect(() => {
    if (Number(activeStep) >= 0 && calculoRota) {
      updateActiveStep(1)
    }
  }, [activeStep, calculoRota, updateActiveStep])
*/
  const nextButtonDisabeld = useMemo(
    () => activeStep === null || !steps[activeStep + 1] || steps[activeStep + 1].disabled,
    [steps, activeStep])

  const totalSteps = useMemo(() => {
    return steps.length
  }, [steps])

  const completedSteps = useMemo(() => {
    return Object.keys(completed).length
  }, [completed])

  const isLastStep = useMemo(() => {
    return activeStep !== null && activeStep === totalSteps - 1
  }, [activeStep, totalSteps])

  const allStepsCompleted = useMemo(() => {
    return completedSteps === totalSteps
  }, [completedSteps, totalSteps])

  const handleNext = useCallback(() => {
    const newActiveStep =
      isLastStep && !allStepsCompleted
        ? steps.findIndex((step, i) => !(i in completed))
        : activeStep + 1
    if (!steps[newActiveStep].disabled) {
      updateActiveStep(newActiveStep)
    }
  }, [
    isLastStep,
    completed,
    allStepsCompleted,
    steps,
    activeStep,
    updateActiveStep
  ])

  const handleBack = useCallback(() => {
    updateActiveStep(activeStep - 1)
  }, [activeStep, updateActiveStep])

  const handleStep = useCallback((step: number) => {
    return () => updateActiveStep(step)
  }, [updateActiveStep])

  const handleComplete = useCallback(() => {
    const newCompleted = completed
    newCompleted[activeStep] = true
    setCompleted(newCompleted)
    handleNext()
  }, [completed, activeStep, setCompleted, handleNext])

  const handleReset = useCallback(() => {
    updateActiveStep(0)
    setCompleted({})
  }, [updateActiveStep, setCompleted])

  const isCompleted = useCallback((index: number) => completed[index] && index !== activeStep, [completed, activeStep]
  )
  return (
    <div className={classes.root}>
      <MuiStepper nonLinear activeStep={activeStep || 0}>
        {steps.map((step: any, index: number) => (
          <Step key={step.label}>
            <StepButton
              onClick={handleStep(index)}
              completed={isCompleted(index)}
              disabled={step.disabled}>
              {step.label}
            </StepButton>
          </Step>
        ))}
      </MuiStepper>
      <Grid
        id="stepper_container"
        container
        className={classes.container}
        spacing={1}
      >
        <Grid item xs={12}
          id="stepper_content_container"
          className={classes.contentContainer}>
          { activeStep === null ? null : (
            <StepperContent
              {...props}
              step={activeStep}
              calculoRota={calculoRota}
              pontos={pontos} />) }
        </Grid>
        <Grid
          item
          container
          xs={12}
          className={classes.row}
          justify="space-around">
          <Grid item xs={12}>
            {activeStep === null ? null
              : (<StepperActions
                handleReset={handleReset}
                activeStep={activeStep}
                handleBack={handleBack}
                handleNext={handleNext}
                handleComplete={handleComplete}
                isLastStep={isLastStep}
                calculoRota={calculoRota}
                nextButtonDisabeld={nextButtonDisabeld} />)
            }
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
})

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    row: {
      width: '100%',
      maxWidth: '100%'
    },
    container: {
      width: '100%',
      maxWidth: '100%'
    },
    contentContainer: {
      width: '100%',
      maxWidth: '100%',
      height: 300,
      maxHeight: 300,
      overflowY: 'auto',
      overflowX: 'hidden'
    },
    root: {
      width: 700,
      height: 400
      // width: '100%'
    }
  })
)
