import React, { useMemo } from 'react'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core'
import {
  ConfigStep
} from './ConfigStep/ConfigStep'
import {
  DirectionsRequest
} from '../../../../../../utils/GoogleMaps/request/DirectionsRequest'
import {
  RotaStep
} from './RotaStep/RotaStep'
import {
  MapsStep
} from './MapsStep/MapsStep'
import './StepperContent.scss'
import { getFieldName } from '../../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import { useField } from 'formik'
/**
 * Area de inputs para Entrega
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Entrega
 */
export const StepperContent = (props: any) => {
  const classes = useStyles()
  const { google } = useMemo(() => window as any, [])
  const {
    step,
    values,
    item,
    disabled
  } = useMemo(() => props, [props])
  const { rota } = useMemo(() => item, [item])

  const directionsRequest = useMemo(() => DirectionsRequest.loadEntrega(
    values,
    DirectionsRequest.getInstance(google)
  ), [values, google])
  
  const fields = useMemo(() => {
    return {
      produto: {
        name: getFieldName("produto", props),
        label: "Produto",
      },
      parceiroEntrega: {
        name: getFieldName("parceiroEntrega", props),
        label: "Parceiro de Entrega",
      },
      transporte: {
        name: getFieldName("transporte", props),
        label: "Transporte",
      },
      funcionarios: {
        name: getFieldName("funcionarios", props),
        label: "Funcionários",
      }
    };
  }, [props])

  // produto
  const [, produtoMeta] = useField(
    fields.produto.name
  )
  const produto = useMemo(
    () => produtoMeta.value,
    [produtoMeta.value])
  // parceiroEntrega
  const [, parceiroEntregaMeta] = useField(
    fields.parceiroEntrega.name
  )
  const parceiroEntrega = useMemo(
    () => parceiroEntregaMeta.value,
    [parceiroEntregaMeta.value])
  // transporte
  const [, transporteMeta] = useField(
    fields.transporte.name
  )
  const transporte = useMemo(
    () => transporteMeta.value,
    [transporteMeta.value])
  // funcionarios
  const [, funcionariosMeta] = useField(
    fields.funcionarios.name
  )
  const funcionarios = useMemo(
    () => funcionariosMeta.value,
    [funcionariosMeta.value])  

  return (<div id="stepper_content_component">
    <div
      id="step_resume_fieldset"
      className={step === 0 ? classes.show : classes.hidden}>
      <ConfigStep
        {...props}
      />
    </div>
    <div
      id="step_rota_fieldset"
      className={step === 1 ? classes.show : classes.hidden}>
      <RotaStep
        {...props}
        name="rota"
        item={rota}
        produto={produto}
        parceiroEntrega={parceiroEntrega}
        transporte={transporte}
        funcionarios={funcionarios}
         />
    </div>
    { (step === 2) ? (<div
      id="step_maps_panel"
      className={step === 2 ? classes.show : classes.hidden}>
      <MapsStep
        disabled={disabled}
        directionsRequest={directionsRequest} />
    </div>) : null }
  </div>)
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    hidden: {
      display: 'none'
    },
    show: {
      display: 'block'
    }
  })
)
