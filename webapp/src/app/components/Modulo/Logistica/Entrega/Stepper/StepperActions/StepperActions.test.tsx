/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { StepperActions } from './StepperActions'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<StepperActions />, div)
  ReactDOM.unmountComponentAtNode(div)
})
