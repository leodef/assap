import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import ReactToPrint from 'react-to-print'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles,
  Grid,
  ButtonGroup,
  Button,
  Card,
  CardActions,
  CardContent,
  Modal,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select
} from '@material-ui/core'
import {
  Map as MapIcon,
  Room as RoomIcon,
  Print as PrintIcon,
  FileCopy as CopyIcon,
  Directions as DirectionsIcon
} from '@material-ui/icons'
import {
  Tooltip
} from '../../../../../../Shared/Utils/Tooltip/Tooltip'
import {
  DirectionsPanel
} from '../../../DirectionsPanel/DirectionsPanel'
import {
  Map
} from '../../../Map/Map'
import './MapsStep.scss'

/**
 * Area de inputs para Localização
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Localização
 */
export const MapsStep = (props: any) => {
  const classes = useStyles()
  const printRef = useRef(null as any)
  const printContentRef = useRef(null as any)
  const [viewConfig, setViewConfig] = useState({
    view: 'map' as 'map' | 'direction' | 'all',
    resolve: null as any
  })
  const updateViewConfig = useCallback(
    (view: 'map' | 'direction' | 'all') => { setViewConfig({ view, resolve: null }) }, [setViewConfig]
  )

  const [isPrintModalOpen, setPrintModalOpen] = useState(false)
  const onClosePrintModal = useCallback(() => setPrintModalOpen(false), [setPrintModalOpen])
  const openPrintModal = useCallback(() => setPrintModalOpen(true), [setPrintModalOpen])

  const panel = useMemo(() => 'entrega_stepper_google_map_directions_panel', [])
  const { disabled, directionsRequest } = useMemo(() => props, [props])
  const buttonAbrirDirecoesDisabled = useMemo(() => (disabled || viewConfig.view === 'direction'), [disabled, viewConfig.view])
  const buttonAbrirMapaDisabled = useMemo(() => (disabled || viewConfig.view === 'map'), [disabled, viewConfig.view])
  const showMapaView = useMemo(() => (viewConfig.view === 'map' || viewConfig.view === 'all'), [viewConfig.view])
  const showDirecoesView = useMemo(() => (viewConfig.view === 'direction' || viewConfig.view === 'all'), [viewConfig.view])
  const abrirImpressao = useCallback((resolve) => {
    setViewConfig({
      ...viewConfig,
      resolve
    })
  }, [setViewConfig, viewConfig])
  const abrirDirecoes = useCallback(() => updateViewConfig('direction'), [updateViewConfig])
  const abrirMapa = useCallback(() => updateViewConfig('map'), [updateViewConfig])
  const abrirMapaExterno = useCallback((event: any) =>
    window.open(directionsRequest.toUrl(), '_blank'),
  [directionsRequest])
  const imprimir = useCallback((event: any) => {
    onClosePrintModal()
    if (printRef.current) {
      printRef.current.handlePrint()
    }
  }, [onClosePrintModal])
  const copiar = useCallback(() => {
    document.execCommand(directionsRequest.toUrl())
  }, [directionsRequest]
  )

  useEffect(() => {
    if (viewConfig && viewConfig.resolve) {
      viewConfig.resolve()
    }
  }, [viewConfig])

  return (<React.Fragment>

    <ReactToPrint
      onAfterPrint={() => abrirMapa()}
      onBeforeGetContent={() =>
        new Promise((resolve) => {
          abrirImpressao(resolve)
        })
      }
      ref={printRef}
      content={() => printContentRef.current} />

    <Grid
      id="maps_panel_component_grid"
      container
      spacing={1}>
      <Grid
        id="maps_panel_component_button_grid"
        item
        xs={12}>

        <ButtonGroup
          size="small"
          variant="contained"
          color="primary">

          <Tooltip
            title="Mostrar direções"
            aria-label="Mostrar direções"
            disabled={buttonAbrirDirecoesDisabled}>
            <Button
              aria-label="Mostrar direções"
              size="small"
              disabled={buttonAbrirDirecoesDisabled}
              onClick={abrirDirecoes}>
              <DirectionsIcon fontSize="small" />
            </Button>
          </Tooltip>

          <Tooltip
            title="Mostrar mapa"
            aria-label="Mostrar mapa"
            disabled={buttonAbrirMapaDisabled}>

            <Button
              aria-label="Mostrar mapa"
              size="small"
              disabled={buttonAbrirMapaDisabled}
              onClick={abrirMapa}>
              <RoomIcon fontSize="small" />
            </Button>
          </Tooltip>

          <Tooltip
            title="Imprimir"
            aria-label="Imprimir"
            disabled={disabled}>

            <Button
              aria-label="Imprimir"
              size="small"
              disabled={disabled}
              onClick={openPrintModal}>
              <PrintIcon fontSize="small" />
            </Button>
          </Tooltip>

          <Tooltip
            title="Abrir no Google maps"
            aria-label="Abrir no Google maps"
            disabled={disabled}>

            <Button
              aria-label="Abrir no Google maps"
              size="small"
              disabled={disabled}
              onClick={abrirMapaExterno}>
              <MapIcon fontSize="small" />
            </Button>
          </Tooltip>

          <Tooltip
            title="Copiar"
            aria-label="Copiar"
            disabled={disabled}>

            <Button
              aria-label="Copiar"
              size="small"
              disabled={disabled}
              onClick={copiar}>
              <CopyIcon fontSize="small" />
            </Button>
          </Tooltip>

        </ButtonGroup>

      </Grid>

      <Grid
        id="maps_panel_component_container_grid"
        item
        xs={12}
        className={classes.container}
        ref={printContentRef}>
        <div
          id="maps_panel_component_map"
          className={(showMapaView ? classes.show : classes.hidden)}>
          <Map
            {...props}
            directionsRequest={directionsRequest}
            panel={panel} />
        </div>
        <div
          id="maps_panel_component_direction"
          className={(showDirecoesView ? classes.show : classes.hidden)}>
          <DirectionsPanel
            panel={panel} />
        </div>
      </Grid>
    </Grid>
    <Modal
      open={isPrintModalOpen}
      onClose={onClosePrintModal}
      aria-labelledby="Impressão"
      aria-describedby="Impressão"
      className={classes.modal}>
      <Card>
        <CardContent>
          <FormControl className={classes.formControl}>
            <InputLabel shrink id="demo-simple-select-placeholder-label-label">
              Tipo de impressão
            </InputLabel>
            <Select
              labelId="demo-simple-select-placeholder-label-label"
              id="demo-simple-select-placeholder-label"
              value={viewConfig.view}
              onChange={(event: any) => updateViewConfig(event.target.value)}
            >
              <MenuItem value="all">Mapa e Direções</MenuItem>
              <MenuItem value="map">Mapa</MenuItem>
              <MenuItem value="direction">Direções</MenuItem>
            </Select>
            <FormHelperText>Conteúdo a ser impresso</FormHelperText>
          </FormControl>
        </CardContent>
        <CardActions>
          <Button
            size="small"
            onClick={imprimir}> Imprimir </Button>
          <Button
            size="small"
            onClick={onClosePrintModal}> Voltar </Button>
        </CardActions>
      </Card>
    </Modal>
  </React.Fragment>)
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      flexGrow: 1,
      flex: 1,
      height: '100%'
    },
    hidden: {
      display: 'none'
    },
    show: {
      display: 'block'
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3)
    }
  })
)
