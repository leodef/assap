import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useField } from "formik";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Checkbox,
  createStyles,
  FormControlLabel,
  Grid,
  makeStyles,
  Theme,
  Typography,
} from "@material-ui/core";
import { KeyboardTimePicker } from "@material-ui/pickers";
import {
  ExpandMore as ExpandMoreIcon
} from "@material-ui/icons";
import {
  DatePicker,
  getFieldName
} from "../../../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField";
import {
  Autocomplete as AutocompleteProduto
} from "../../../../../DadosEmpresa/Produto/Autocomplete/Autocomplete";
import {
  Fieldset as LimiteFieldset
} from "../../../../Limite/Fieldset/Fieldset";
import {
  Fieldset as ParceiroEntregaFieldset
} from "../../../../ParceiroEntrega/Fieldset/Fieldset";
import {
  Fieldset as TransporteFieldset
} from "../../../../Transporte/Fieldset/Fieldset";
import {
  ArrayFieldset as FuncionarioEntregaArrayFieldset
} from "../../../../FuncionarioEntrega/ArrayFieldset/ArrayFieldset";
import {
  Fieldset as CompromissoFieldset
} from "../../../../../Calendario/Compromisso/Fieldset/Fieldset";
import {
  TituloDescFieldset
} from "../../../../../../Shared/Crud/Fieldset/TituloDescFieldset/TituloDescFieldset";
import {
  TIPO_LIMITE_ROTA
} from "../../../../../../../enums/tipo-limite-rota";
import {
  TIPO_COMPROMISSO
} from "../../../../../../../enums/tipo-compromisso";
import {
  Limite
} from "../../../../../../../types/API/Limite";
import {
  Entrega
} from "../../../../../../../types/API/Logistica/Entrega";
import {
  initialValues as compromissoInitialValues
} from "../../../../../../../types/API/Calendario/Compromisso";
import "./ConfigStep.scss";


/**
 * Componente ConfigStep
 * @param {any} props Propriedades
 * @return {React.Component} Componente ConfigStep
 */
export const ConfigStep = (props: any) => {
  const classes = useStyles();

  // #region Automated
  const automated = useMemo(() => ({
    titulo: Entrega.generateTitulo(props.values),
    desc: Entrega.generateDesc(props.values)
  }),
  [props.values])
  // #endregion Automated

  // #region Expanded
  const [
    tituloDescAccordionExpanded,
    setTituloDescAccordionExpanded,
  ] = useState(false as boolean);
  const [
    limiteAccordionExpanded,
    setLimiteAccordionExpanded
  ] = useState(false as boolean);
  const [
    funcionarioEntregaAccordionExpanded,
    setFuncionarioEntregaAccordionExpanded,
  ] = useState(false as boolean);
  const [
    compromissoAccordionExpanded,
    setCompromissoAccordionExpanded,
  ] = useState(false as boolean);
  const [
    transporteAccordionExpanded,
    setTransporteAccordionExpanded,
  ] = useState(false as boolean);
  const [
    parceiroEntregaAccordionExpanded,
    setParceiroEntregaAccordionExpanded,
  ] = useState(false as boolean);
  // #endregion Expanded

  // #region Item Form
  const { disabled, item } = useMemo(() => props || {}, [props]);
  const {
    limite,
    parceiroEntrega,
    transporte,
    funcionarios,
    compromisso,
  } = useMemo(() => item, [item]);

  const fields = useMemo(() => {
    return {
      data: {
        name: getFieldName("data", props),
        label: "Data",
        hora : {
          label: "Hora",
        }
      },
      produto: {
        name: getFieldName("produto", props),
        label: "Produto",
      },
      titulo: {
        name: getFieldName("titulo", props),
        label: "Título",
      },
      tipoFormacaoTitulo: {
        name: getFieldName("tipoFormacaoTitulo", props),
        label: "Tipo de formação do titulo",
      },
      desc: {
        name: getFieldName("desc", props),
        label: "Descrição",
      },
      tipoFormacaoDesc: {
        name: getFieldName("tipoFormacaoDesc", props),
        label: "Tipo de formação da descrição",
      },
      tipoLimite: {
        name: getFieldName("rota.tipoLimite", props),
        label: "Tipo de limite",
      },
      limite: {
        name: getFieldName("rota.limite", props),
        label: "Limite",
      },
      parceiroEntrega: {
        name: getFieldName("parceiroEntrega", props),
        label: "Parceiro de Entrega",
      },
      transporte: {
        name: getFieldName("transporte", props),
        label: "Transporte",
      },
      funcionarios: {
        name: getFieldName("funcionarios", props),
        label: "Funcionários",
      },
      compromisso: {
        name: getFieldName("compromisso", props),
        label: "Compromisso",
      },
      criarCompromisso: {
        name: getFieldName("criarCompromisso", props),
        label: "Criar compromisso",
      },
    };
  }, [props]);
  // #endregion Item Form

  // #region Data
  const [, dataMeta, dataHelpers] = useField(
    fields.data.name
  );
  const dataValue = useMemo(() => dataMeta.value, [dataMeta.value])
  const setDataValue = useCallback((value: any) => dataHelpers.setValue(value), [dataHelpers])
  // #endregion Data

  // #region Compromisso
  const [, criarCompromissoMeta, criarCompromissoHelpers] = useField(
    fields.criarCompromisso.name
  );
  const [, , compromissoHelpers] = useField(
    fields.compromisso.name
  );
  const setCompromisso = useCallback((val) => compromissoHelpers.setValue(val), [compromissoHelpers])
  const setCriarCompromissoValue = useCallback(
    (value: any) => {
      const val = Boolean(value);
      setCompromisso(
        compromissoInitialValues
      )
      criarCompromissoHelpers.setValue(val);
      setCompromissoAccordionExpanded(val);
    },
    [
      criarCompromissoHelpers,
      setCompromissoAccordionExpanded,
      setCompromisso]
  );
  const criarCompromissoValue = useMemo(() => Boolean(criarCompromissoMeta.value), [
    criarCompromissoMeta.value,
  ]);
  const compromissoDisabled = useMemo(() => !criarCompromissoValue, [
    criarCompromissoValue,
  ]);
  const compromissoLabel = useMemo(
    () =>
      compromissoDisabled
        ? "Criar evento no calendário"
        : "Evento no calendário",
    [compromissoDisabled]
  );
  const compromissoFixed = useMemo(() => ({
    diaInteiro: true,
    tipo: TIPO_COMPROMISSO.entrega,
    fim: null,
    inicio: dataValue
  }), [dataValue])
  const compromissoFieldset = useMemo(() =>
    criarCompromissoValue
      ? (<CompromissoFieldset
        {...props}
        name={fields.compromisso.name}
        item={compromisso}
        fixed={compromissoFixed}
        placeHolder={automated}
        automated={automated}
      />) : null, [
        props,
        criarCompromissoValue,
        fields,
        compromisso,
        compromissoFixed,
        automated
      ])
  // #endregion Compromisso

  // #region Produto
  const [, produtoMeta, produtoHelpers] = useField(fields.produto.name);
  const setProdutoValue = useCallback(
    (value: any) => produtoHelpers.setValue(value),
    [produtoHelpers]
  )
  const produtoValue = useMemo(() => produtoMeta.value, [
    produtoMeta.value
  ])
  // #endregion Produto

  // #region Transporte
  const [, transporteMeta] = useField(fields.transporte.name);
  const transporteValue = useMemo(() => transporteMeta.value, [
    transporteMeta.value
  ])
  // #endregion Transporte
  
  // #region Limite
  const [, limiteMeta, limiteHelpers] = useField(fields.limite.name);
  const setLimiteValue = useCallback(
    (value: any) => limiteHelpers.setValue(value),
    [limiteHelpers]
  )
  const limiteValue = useMemo(() => limiteMeta.value, [
    limiteMeta.value
  ])
  // #endregion Limite

  // #region tipoLimite
  const [, tipoLimiteMeta, tipoLimiteHelpers] = useField(
      fields.tipoLimite.name
    );
  const tipoLimiteValue = useMemo(() => tipoLimiteMeta.value, [tipoLimiteMeta]);
  const setTipoLimiteValue = useCallback(
      (value) => tipoLimiteHelpers.setValue(value),
      [tipoLimiteHelpers]
    );
  const updateTipoLimiteValue = useCallback(
      (checked) => {
        setLimiteAccordionExpanded(checked);
        setTipoLimiteValue(
          checked === true
            ? TIPO_LIMITE_ROTA.MANUAL
            : TIPO_LIMITE_ROTA.TRANSPORTE)
      },
      [
        setLimiteAccordionExpanded,
        setTipoLimiteValue
      ]
    );
  const tipoLimiteManual = useMemo(
        () => tipoLimiteValue === TIPO_LIMITE_ROTA.MANUAL, 
        [tipoLimiteValue])
  const tipoLimiteDisabled = useMemo(
        () => tipoLimiteManual,
        [tipoLimiteManual]
      );
  const tipoLimiteLabel = useMemo(
      () =>
        tipoLimiteValue === TIPO_LIMITE_ROTA.MANUAL
          ? "Limite manual"
          : "Informar limite manualmente",
      [tipoLimiteValue]
    );
  // #endregion tipoLimite

  // Atualizações
  // Carrega o limite com o veiculo
  useEffect(() => {
    if(!tipoLimiteManual && transporteValue) {
      const limite = Limite.transporteToLimite(transporteValue)
      if(!Limite.compare(limite, limiteValue)){
        setLimiteValue(limite)
        setTipoLimiteValue(TIPO_LIMITE_ROTA.TRANSPORTE)
      }
    }
  }, [
    tipoLimiteManual,
    transporteValue,
    limiteValue,
    setLimiteValue,
    setTipoLimiteValue
  ])

  return (
    <Grid container spacing={1}>

      {/* Data */}
      <Grid item container xs={12}>
        <Grid item xs={12} sm={4}>
          <DatePicker
            name={fields.data.name}
            label={fields.data.label}
            disabled={disabled}
          />

</Grid>
        <Grid item xs={12} sm={4}>
        <KeyboardTimePicker
          margin="normal"
          label={fields.data.hora.label}
          value={dataValue}
          onChange={(obj) => setDataValue(obj)}
          KeyboardButtonProps={{
            'aria-label': 'change time',
          }}
        />

        </Grid>

        <Grid item xs={12} sm={4}>

          <AutocompleteProduto
            id="config-step-autocomplete-produto"
            size="small"
            fullWidth
            onChange={setProdutoValue}
            value={produtoValue}
            disabled={disabled}
          />

        </Grid>
      </Grid>

      {/* TituloDesc */}
      <Grid item xs={12}>
        <Accordion
          expanded={tituloDescAccordionExpanded}
          onChange={(event: any, isExpanded: boolean) =>
            setTituloDescAccordionExpanded(!tituloDescAccordionExpanded)
          }
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label="Titulo e Descrição"
            aria-controls="entrega-config-step-accord-titulo-desc-content"
            id="entrega-config-step-accord-titulo-desc-header"
          >
            <Typography className={classes.heading}>
              Titulo e Descrição
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container>
              <TituloDescFieldset
                {...props}
                automated={automated} />
            </Grid>
          </AccordionDetails>
        </Accordion>
      </Grid>

      {/* ParceiroEntrega */}
      <Grid item xs={12}>
        <Accordion
          expanded={parceiroEntregaAccordionExpanded}
          onChange={(event: any, isExpanded: boolean) =>
            setParceiroEntregaAccordionExpanded(
              !parceiroEntregaAccordionExpanded
            )
          }
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label="Parceiro comercial"
            aria-controls="entrega-config-step-accord-parceiro-entrega-content"
            id="entrega-config-step-accord-parceiro-entrega-header"
          >
            <Typography className={classes.heading}>
              Parceiro comercial
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <ParceiroEntregaFieldset
              {...props}
              name={fields.parceiroEntrega.name}
              item={parceiroEntrega}
            />
          </AccordionDetails>
        </Accordion>
      </Grid>
    
      {/* Transporte */}
      <Grid item xs={12}>
        <Accordion
          expanded={transporteAccordionExpanded}
          onChange={(event: any, isExpanded: boolean) =>
            setTransporteAccordionExpanded(!transporteAccordionExpanded)
          }
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label="Transporte"
            aria-controls="entrega-config-step-accord-transporte-content"
            id="entrega-config-step-accord-transporte-header"
          >
            <Typography className={classes.heading}>Transporte</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <TransporteFieldset
              {...props}
              name={fields.transporte.name}
              item={transporte}
            />
          </AccordionDetails>
        </Accordion>
      </Grid>
      
      {/* FuncionarioEntrega */}
      <Grid item xs={12}>
        <Accordion
          expanded={funcionarioEntregaAccordionExpanded}
          onChange={(event: any, isExpanded: boolean) =>
            setFuncionarioEntregaAccordionExpanded(
              !funcionarioEntregaAccordionExpanded
            )
          }
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label="Funcionários"
            aria-controls="entrega-config-step-accord-funcionarios-content"
            id="entrega-config-step-accord-funcionarios-header"
          >
            <Typography className={classes.heading}>Funcionários</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <FuncionarioEntregaArrayFieldset
              {...props}
              name={fields.funcionarios.name}
              item={funcionarios}
            />
          </AccordionDetails>
        </Accordion>
      </Grid>
      
      {/* Compromisso */}
      <Grid item xs={12}>
        <Accordion
          expanded={compromissoAccordionExpanded}
          onChange={(event: any, isExpanded: boolean) =>
            setCompromissoAccordionExpanded(
              !compromissoAccordionExpanded && !compromissoDisabled
            )
          }
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label={compromissoLabel}
            aria-controls="entrega-config-step-accord-compromisso-content"
            id="entrega-config-step-accord-compromisso-header"
            IconButtonProps={{ disabled: compromissoDisabled }}
          >
            <FormControlLabel
              aria-label={compromissoLabel}
              onClick={(event: any) => event.stopPropagation()}
              onFocus={(event: any) => event.stopPropagation()}
              onChange={(event, checked) =>
                setCriarCompromissoValue(checked)}
              checked={criarCompromissoValue}
              control={<Checkbox />}
              label={compromissoLabel}
            />
          </AccordionSummary>
          <AccordionDetails>
            {compromissoFieldset}
          </AccordionDetails>
        </Accordion>
      </Grid>

      {/* Limite */}
      <Grid item xs={12}>
        <Accordion
          expanded={limiteAccordionExpanded}
          onChange={(event: any, isExpanded: boolean) =>
            setLimiteAccordionExpanded(
              !limiteAccordionExpanded && !tipoLimiteDisabled
            )
          }
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label={tipoLimiteLabel}
            aria-controls="entrega-config-step-accord-limite-content"
            id="entrega-config-step-accord-limite-header"
            IconButtonProps={{ disabled: tipoLimiteDisabled }}
          >
            <FormControlLabel
              aria-label={tipoLimiteLabel}
              onClick={(event) => event.stopPropagation()}
              onFocus={(event) => event.stopPropagation()}
              onChange={(event, checked) => updateTipoLimiteValue(checked)}
              control={<Checkbox />}
              checked={tipoLimiteManual}
              label={tipoLimiteLabel}
            />
          </AccordionSummary>
          <AccordionDetails>
            <LimiteFieldset
              {...props}
              name={fields.limite.name}
              item={limite}
            />
          </AccordionDetails>
        </Accordion>
      </Grid>
    
    </Grid>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: "33.33%",
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
  })
);
