import React, { useMemo } from 'react'
// eslint-disable-next-line no-unused-vars
import { Grid } from '@material-ui/core'
import { useField } from 'formik'
import {
  Fieldset
} from '../../../../Rota/Fieldset/Fieldset'
import {
  Chips as RotaChips
} from '../../../../Rota/Chips/Chips'
import {
  getFieldName
} from '../../../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import './RotaStep.scss'

/**
 * Componente RotaStep
 * @param {any} props Propriedades
 * @return {React.Component} Componente RotaStep
 */
export const RotaStep = (props: any) => {
  const fields = useMemo(() => {
    return {
      calculo: {
        name: getFieldName('calculo', props),
        label: 'Calculo'
      },
      limite: {
        name: getFieldName('limite', props),
        label: 'Limite'
      },
      tipoLimite: {
        name: getFieldName("tipoLimite", props),
        label: "Tipo de limite",
      },
    }
  }, [props])

  const modalHistory = useMemo(
    () => ([
      ...(props.modalHistory || []),
      "Entrega"
    ]), [props.modalHistory])

  // calculo
  const [, calculoMeta] = useField(
    fields.calculo.name
  )
  const calculo = useMemo(
    () => calculoMeta.value,
    [calculoMeta.value])
  const disabled = useMemo(
    () => calculo || props.disabled,
    [calculo, props.disabled])

  // limite
  const [, limiteMeta] = useField(
    fields.limite.name
  )
  const limite = useMemo(
    () => limiteMeta.value,
    [limiteMeta.value])
  
  // tipoLimite
  const [, tipoLimiteMeta] = useField(
    fields.tipoLimite.name
  )
  const tipoLimite = useMemo(
    () => tipoLimiteMeta.value,
    [tipoLimiteMeta.value])
  
  return (
    <Grid
      item
      container
      spacing={1}
      justify="flex-start">
      <Grid item xs={12}>
        <Fieldset
          {...props}
          disabled={disabled} />
      </Grid>
      <Grid item xs={12}>
        <RotaChips
          {...props}
          modalHistory={modalHistory}
          disabled={disabled}
          calculo={calculo}
          limite={limite}
          tipoLimite={tipoLimite} />
      </Grid>

    </Grid>
  )
}
