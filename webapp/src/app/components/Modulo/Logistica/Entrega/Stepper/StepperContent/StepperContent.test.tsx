/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { StepperContent } from './StepperContent'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<StepperContent/>, div)
  ReactDOM.unmountComponentAtNode(div)
})
