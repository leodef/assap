/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { MapsStep } from './MapsStep'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<MapsStep />, div)
  ReactDOM.unmountComponentAtNode(div)
})
