/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { RotaStep } from './RotaStep'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<RotaStep />, div)
  ReactDOM.unmountComponentAtNode(div)
})
