import React, { useCallback, useContext, useMemo } from 'react'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles,
  Button,
  Grid,
  ButtonGroup
} from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { LoadingButton } from '../../../../../Shared/Utils/LoadingButton/LoadingButton'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import { ActionTypeEnum } from '../../../../../../types/Crud'
import './StepperActions.scss'
/**
 * Area de inputs para Entrega
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Entrega
 */
export const StepperActions = (props: any) => {
  const dispatch = useDispatch()
  const {
    activeStep,
    handleBack,
    handleNext,
    handleComplete,
    isLastStep,
    nextButtonDisabeld
  } = props

  // CrudContext
  const {
    getState,
    actions,
    types
  } = useContext(CrudContext)
  // selector
  const {
    item,
    createLoading,
    updateLoading,
    calculoRotaLoading
  } = useSelector((state: any) => {
    const {
      item,
      createLoading,
      updateLoading,
      calculoRotaLoading
    } = getState(state)
    return {
      item,
      createLoading,
      updateLoading,
      calculoRotaLoading
    }
  })
  const itemId = useMemo(() => item ? item._id : null, [item])

  const loading = useMemo(() =>
    (createLoading || updateLoading || calculoRotaLoading),
  [createLoading, updateLoading, calculoRotaLoading])

  // dispatch
  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action,
        item
      }
    }), [dispatch, types])
  const toRemove = useCallback((item: any) => {
    setAction(ActionTypeEnum.REMOVE, item)
  }, [setAction])
  const back = useCallback(() => {
    setAction(ActionTypeEnum.LIST, null)
  }, [setAction])

  const onNext = (event: any) => {
    handleNext(event)
    handleComplete(event)
  }
  return (<Grid
    container
    spacing={2}
    direction="row"
    justify="space-around">
    <Grid item xs={4}>
      <ButtonGroup
        size="small"
        variant="contained"
        color="primary">
        <NextButton
          onNext={onNext}
          isLastStep={isLastStep}
          nextButtonDisabeld={nextButtonDisabeld} />

        <PrevButton
          activeStep={activeStep}
          handleBack={handleBack} />
      </ButtonGroup>
    </Grid>
    <Grid item xs={2}></Grid>
    <Grid item xs={6}>
      <ButtonGroup
        size="small"
        variant="contained"
        color="primary">

        <RemoveButton
          size="small"
          actions={actions}
          itemId={itemId}
          toRemove={toRemove}
          item={item}
        />
        <SubmitButton
          size="small"
          groupButton={true}
          actions={actions}
          loading={loading}
          isLastStep={isLastStep}
        />
        <ExitButton
          size="small"
          actions={actions}
          back={back}
        />
      </ButtonGroup>
    </Grid>
  </Grid>)
}

export const NextButton = React.memo((props: any) => {
  NextButton.displayName = 'NextButton'
  const classes = useStyles()
  const { onNext, isLastStep, nextButtonDisabeld } = props
  return (<Button
    size="small"
    variant="contained"
    color="primary"
    disabled={isLastStep || nextButtonDisabeld}
    onClick={onNext}
    className={classes.button}>
      Próximo
  </Button>)
})

export const PrevButton = React.memo((props: any) => {
  PrevButton.displayName = 'PrevButton'
  const classes = useStyles()
  const { activeStep, handleBack } = props
  return (<Button
    size="small"
    disabled={activeStep === 0}
    onClick={handleBack}
    className={classes.button}>
      Voltar
  </Button>)
})

export const RemoveButton = React.memo((props: any) => {
  RemoveButton.displayName = 'RemoveButton'
  const { itemId, toRemove, item, ...others } = useMemo(() => props, [props])
  /*
    variant="contained"
    color="primary"
  */
  return (itemId)
    ? (<Button
      {...others}
      onClick={(event: any) => toRemove(item)}>
      Remover
    </Button>) : null
})

export const SubmitButton = React.memo((props: any) => {
  SubmitButton.displayName = 'SubmitButton'
  const {
    isLastStep,
    actions,
    ...others
  } = props
  return (<LoadingButton
    {...others}
    type="submit"
    disabled={!isLastStep} >
      Salvar
  </LoadingButton>)
})

export const ExitButton = React.memo((props: any) => {
  ExitButton.displayName = 'ExitButton'
  const { back, ...others } = props
  /*
    variant="contained"
    color="primary"
  */
  return (<Button
    {...others}
    onClick={(event: any) => back()}>
        Sair
  </Button>)
})

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      marginRight: theme.spacing(1)
    }
  })
)
