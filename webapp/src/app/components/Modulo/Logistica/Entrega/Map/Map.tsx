import React, { useMemo, useState, useEffect, useCallback } from "react";
import { DirectionsRenderer, GoogleMap } from "@react-google-maps/api";
import "./Map.scss";

const containerStyle = {
  width: "100%",
  height: "200px",
};

/**
 * Area de inputs para Localização
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Localização
 */
export const Map = (props: any) => {
  // #region Map
  const [directions, setDirections] = useState(null as any);
  const { directionsRequest, panel } = useMemo(() => props, [props]);
  const directionsParams = useMemo(
    () => (directionsRequest ? directionsRequest.toRequest() : null),
    [directionsRequest]
  );

  const DirectionsService = useMemo(
    () => new window.google.maps.DirectionsService(),
    []
  );
  useEffect(() => {
    if (!directionsParams) {
      return;
    }
    DirectionsService.route(directionsParams, (result: any, status: any) => {
      // https://developers.google.com/maps/documentation/javascript/reference/directions#DirectionsResult
      if (status === window.google.maps.DirectionsStatus.OK) {
        setDirections(result);
      } else {
        console.error(`error fetching directions ${result}`);
      }
    });
  }, [directionsParams, DirectionsService]);
  // //#endregion Map

  // #region Map
  // const center = useMemo(() => directionsRequest ? directionsRequest._center : null, [directionsRequest])
  const point = useMemo(() => directionsRequest._center, [directionsRequest._center])
  const [map, setMap] = useState(null as any);
  const center = useMemo(() =>
    new window.google.maps.LatLng(point?.lat, point?.lng)
  , [point]);
  const zoom = useMemo(() => 16, []);

  const onLoad = useCallback(function callback(map) {
    const bounds = new window.google.maps.LatLngBounds();
    map.fitBounds(bounds);
    map.panTo(center);
    map.setZoom(zoom);
    setMap(map);
  }, [zoom, center]);

  const onUnmount = useCallback(function callback(map) {
    setMap(null);
  }, []);

  useEffect(() => {
    if(map) {
      map.panTo(center);
      map.setZoom(zoom);
    }
  }, [center, zoom, map])
  // #endregion Map


  return (
    <GoogleMap
      mapContainerStyle={containerStyle}
      center={center}
      zoom={zoom}
      onLoad={onLoad}
      onUnmount={onUnmount}
    >
      {directions ? (
        <DirectionsRenderer
          directions={directions}
          panel={document.getElementById(panel) as Element}
        />
      ) : null}
    </GoogleMap>
  );
};
