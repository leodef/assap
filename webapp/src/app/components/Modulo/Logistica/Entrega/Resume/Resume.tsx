
import moment from "moment";
import React, { useState } from "react";
import {
  BottomNavigation,
  BottomNavigationAction,
  createStyles,
  Grid,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Theme,
  Typography,
} from "@material-ui/core";
import {
  Timeline,
  TimelineConnector,
  TimelineContent,
  TimelineDot,
  TimelineItem,
  TimelineOppositeContent,
  TimelineSeparator
} from "@material-ui/lab";
import {
  Settings as SettingsIcon,
  Map as MapIcon,
  Event as EventIcon,
} from "@material-ui/icons";
import {
  Resume as CompromissoResume
} from "../../../Calendario/Compromisso/Resume/Resume"
import "./Resume.scss";

/**
 * Visualização de item do tipo Entrega
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Entrega
 */
export const Resume = (props: any) => {
  const classes = useStyles();
  const [tab, setTab] = useState(0);
  const { item } = props;
  const { compromisso } = item || {} as any;
  return (
    <Grid container spacing={1}>
      <Grid
        container
        item
        xs={12}
        justify="space-between"
        direction="row"
        className={tab === 0 ? classes.show : classes.hidden}
      >
        <Grid item xs={12}>
          <Typography variant="h5" component="h2">
            {item.titulo}
          </Typography>
        </Grid>
        <Grid item xs={4}>
          
          <strong>Data da entrega:</strong> {moment(item.data).format('DD/MM/YYY hh:mm:ss')}
          
        </Grid>
        {item.produto && item.produto.titulo ? (<Grid item xs={4}>
          
          <strong>Produto:</strong> {item.produto.titulo}
          
        </Grid>) : null}
        {item.status ? (<Grid item xs={4}>
          
          <strong>Status entrega:</strong> {item.status}
          
        </Grid>) : null}
        {item.parceiroEntrega && item.parceiroEntrega.parceiro && item.parceiroEntrega.parceiro.titulo ? (<Grid item xs={4}>
          
          <strong>Parceiro:</strong> {item.parceiroEntrega.parceiro.titulo}
          
        </Grid>) : null}
        {item.parceiroEntrega && item.parceiroEntrega.contato && item.parceiroEntrega.contato.titulo ? (<Grid item xs={4}>
          
          <strong>Contato do parceiro:</strong> {item.parceiroEntrega.contato.titulo}
          
        </Grid>) : null}
        {item.transporte && item.transporte.veiculo && item.transporte.veiculo.titulo ? (<Grid item xs={4}>
          
          <strong>Veículo:</strong> {item.transporte.veiculo.titulo}
          
        </Grid>) : null}
        {item.transporte && item.transporte.carreta && item.transporte.carreta.titulo ? (<Grid item xs={4}>
          
          <strong>Carreta:</strong> {item.transporte.carreta.titulo}
          
        </Grid>) : null}
        {item.funcionarios ? (<Grid item xs={12}><TableContainer>
            <Table
              aria-label="Funcionários"
              size="small"
              stickyHeader
              >
              <TableHead>
                <TableRow>
                  <TableCell align="center" colSpan={2}>
                    Funcionários
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {item.funcionarios.map((funcionario: any, index: number) => (
                  <TableRow  key={`entrega_funcionario_${index}`}>
                    <TableCell colSpan={2}>
                      {
                        funcionario && funcionario.funcionario && funcionario.funcionario.nome
                          ? funcionario.funcionario.nome
                          : null
                      }
                    </TableCell>
                    <TableCell colSpan={2}>
                    {
                        funcionario && funcionario.tipo
                          ? funcionario.tipo
                          : null
                      }
                    </TableCell>
                  </TableRow>)
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>) : null}

      </Grid>
      <Grid
        container
        item
        xs={12}
        className={tab === 1 ? classes.show : classes.hidden} >
        
        {item.rota && item.rota.algoritimo ? (<Grid item xs={4}>
          
          <strong>Algoritimo:</strong> {item.rota.algoritimo}
          
        </Grid>) : null}
        
        {item.rota && item.rota.calculo ? (<Grid item xs={4}>
          
            {item.rota.calculo ? 'Cálculo realizado' : 'Cálculo não realizado'}
          
        </Grid>) : null}

        {item.rota && item.rota.limite && item.rota.limite.peso ? (<Grid item xs={4}>
          
          <strong>Limite:</strong> {item.rota.limite.peso} kg
          
        </Grid>) : null}
      
        {item.rota && item.rota.limite && item.rota.limite.volume ? (<Grid item xs={4}>
          
            <strong>Volume:</strong> {item.rota.limite.volume} L
          
        </Grid>) : null}

        
        {item.rota && item.rota.tipoLimite ? (<Grid item xs={4}>
          
            <strong>Tipo de limite:</strong> {item.rota.tipoLimite}
          
        </Grid>) : null}

      {item.rota && item.rota.pontos ?
      
      (<Timeline align="alternate">
        {item.rota.pontos.map((ponto: any) => (
        <TimelineItem>
          <TimelineOppositeContent>
        <Typography color="textSecondary">{ponto.titulo}</Typography>
          </TimelineOppositeContent>
          <TimelineSeparator>
            <TimelineDot />
            <TimelineConnector />
          </TimelineSeparator>
          <TimelineContent>
            <Typography>{
              ponto.limite && ponto.limite.peso
              ? (ponto.limite.peso > 0 ? 'Carga: ' : 'Descarga: ') + Math.abs(ponto.limite.peso)
              : ponto.localizacao.lat || ponto.localizacao.long
                ? `Lat: ${ponto.localizacao.lat} \\ Lng: ${ponto.localizacao.long}`
                : `Ordem: ${ponto.ordem}`
            }</Typography>
          </TimelineContent>
        </TimelineItem>))}
      </Timeline>): null}
                    </Grid>
      <Grid
        container
        item
        xs={12}
        className={tab === 2 ? classes.show : classes.hidden}>
        
        <CompromissoResume {...props} item={compromisso} />
      </Grid>
      <Grid
        item
        xs={12}>    
      <BottomNavigation
        value={tab}
        onChange={(event: any, newValue: any) => setTab(newValue)}
        showLabels
        className={classes.bottomNavigation}
      >
        <BottomNavigationAction label="Configuração" icon={<SettingsIcon />} />
        <BottomNavigationAction label="Rota" icon={<MapIcon />} />
        <BottomNavigationAction label="Evento" icon={<EventIcon />} />
      </BottomNavigation>
      </Grid>
    </Grid>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
      overflow: "auto",
      width: 500,
    },
    container: {
      height: 300,
      width: "100%",
    },
    bottomNavigation: {
      width: "100%",
    },
    hidden: {
      display: "none",
    },
    show: {
      display: "block",
      maxHeight: 300,
      overflowY: "auto"
    },
  })
);
