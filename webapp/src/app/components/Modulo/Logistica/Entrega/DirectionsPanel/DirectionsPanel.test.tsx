/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { DirectionsPanel } from './DirectionsPanel'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<DirectionsPanel />, div)
  ReactDOM.unmountComponentAtNode(div)
})
