import React, { useMemo } from 'react'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core'
import './DirectionsPanel.scss'

/**
 * Area de inputs para Localização
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Localização
 */
export const DirectionsPanel = (props: any) => {
  const classes = useStyles()
  const { panel } = useMemo(() => props, [props])
  return (<div id={panel} className={classes.panelContainer}></div>)
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    panelContainer: {
      flexGrow: 1,
      flex: 1,
      width: '100%'
    }
  })
)
