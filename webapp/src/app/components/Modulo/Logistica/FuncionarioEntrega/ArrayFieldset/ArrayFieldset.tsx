import React, { useCallback, useMemo } from "react";
import { useField } from "formik";
import {
  createStyles,
  Fab,
  IconButton,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Theme,
} from "@material-ui/core";
import {
  Add as AddIcon,
  Delete as DeleteIcon
} from "@material-ui/icons";
import {
  Autocomplete as AutocompleteFuncionario
} from "../../../DadosEmpresa/Funcionario/Autocomplete/Autocomplete";
import {
  getFieldName,
  getFormName,
  SelectInput,
} from "../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField";
import "./ArrayFieldset.scss";
import {
  TIPO_FUNCIONARIO_ENTREGA_LABEL
} from "../../../../../enums/tipo-funcionario-entrega";
import { initialValues } from "../../../../../types/API/Logistica/FuncionarioEntrega";

/**
 * Componente ArrayFieldset
 * @param {any} props Propriedades
 * @return {React.Component} Componente ArrayFieldset
 */
export const ArrayFieldset = (props: any) => {
  const classes = useStyles();
  const { disabled } = useMemo(() => props, [props]);

  const fields = useMemo(() => {
    return {
      funcionarios: {
        name: getFormName(props),
        label: "Informação de contato",
      },
      funcionario: {
        getName: (index: number) =>
          getFieldName("funcionario", {
            ...props,
            name: getFieldName(String(index), props),
          }),
        name: getFieldName("funcionario", props),
        label: "Funcionário",
      },
      tipo: {
        getName: (index: number) =>
        getFieldName("tipo", {
          ...props,
          name: getFieldName(String(index), props),
        }),
        name: getFieldName("tipo", props),
        label: "Tipo",
      },
    };
  }, [props]);

  // funcionarios
  const [, funcionariosMeta, funcionariosHelpers] = useField(
    fields.funcionarios.name
  );
  const setFuncionarios = useCallback(
    (val: any) => funcionariosHelpers.setValue(val),
    [funcionariosHelpers]
  );
  const funcionarios = useMemo(
    () => (funcionariosMeta.value || []) as Array<any>,
    [funcionariosMeta]
  );

  // utils
  const updateFuncionario = useCallback(
    (funcionario: any, index: number) => {
      const values = [...funcionarios];
      const newValue = {
        ...(values[index] || {}),
        funcionario,
      } as any;
      values[index] = newValue;
      setFuncionarios(values);
    },
    [setFuncionarios, funcionarios]
  );
  const getFuncionario = useCallback(
    (index: number) => {
      const info = funcionarios[index];
      return info && info.funcionario
        ? info.funcionario
        : null;
    },
    [funcionarios]
  );
  const remove = useCallback(
    (index: number) => {
      setFuncionarios(
        funcionarios.filter((val: any, ind: number) => ind !== index)
      );
    },
    [setFuncionarios, funcionarios]
  );
  const create = useCallback(() => {
    setFuncionarios([
      ...funcionarios,
      initialValues
    ]);
  }, [setFuncionarios, funcionarios]);

  return (
    <TableContainer component={Paper} className={classes.container}>
      <Table aria-label="Funcionários da entega" size="small" stickyHeader>
        <TableHead>
          <TableRow>
            <TableCell size="small" colSpan={7} align="right">
              <Fab
                color="primary"
                size="small"
                aria-label="Novo"
                onClick={() => create()}
              >
                <AddIcon />
              </Fab>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {funcionarios.map((info: any, index: number) => (
            <TableRow key={`funcionario_entrega_array_fieldset_${index}`}>
              <TableCell colSpan={3} size="small">
                <AutocompleteFuncionario
                  id="funcionario-entrega-autocomplete-funcionario"
                  size="small"
                  fullWidth
                  disabled={disabled}
                  margin="normal"
                  noLabel
                  fieldProps={{
                    variant: "standard",
                    margon: "normal",
                  }}
                  onChange={(value: any) => updateFuncionario(value, index)}
                  value={getFuncionario(index)}
                />
              </TableCell>
              <TableCell colSpan={3} size="small">
                  <SelectInput
                    disabled={disabled}
                    name={fields.tipo.getName(index)}
                    label={fields.tipo.label}
                    enumOptions={TIPO_FUNCIONARIO_ENTREGA_LABEL}
                    fullWidth
                  />
              </TableCell>
              <TableCell align="right" size="small">
                <IconButton
                  size="small"
                  aria-label="Remover"
                  onClick={() => remove(index)}>
                  <DeleteIcon />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      position: "relative",
      overflow: "auto",
      maxHeight: 300,
    }
  })
);
