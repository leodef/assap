import React, { useMemo } from 'react'
import {
  List,
  ListItem,
  ListItemText,
  ListSubheader
} from '@material-ui/core'
import './ArrayResume.scss'

/**
 * Visualização de item do tipo Empresa
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Empresa
 */
export const ArrayResume = (props: any) => {
  const { item, label } = useMemo(() => props, [props])
  return (<List
    dense
    subheader={
      label ?
        (<ListSubheader>{label}</ListSubheader>) :
        undefined}
    >
    <ListItem dense>
      {
        (item || []).map((value: any) =>
          (<ListItemText
            primary={item.valor}
            secondary={item.tipo ? item.tipo.titulo : 'Contato'}
          />))
      }
      
    </ListItem>
    </List>)
}
