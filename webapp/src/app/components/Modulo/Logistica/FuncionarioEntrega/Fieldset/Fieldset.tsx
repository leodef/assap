import
React,
{
  useCallback,
  useMemo
} from 'react'
import {
  Grid
} from '@material-ui/core'
import { useField } from 'formik'
import {
  getFieldName,
  SelectInput
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  Autocomplete as AutocompleteFuncionario
} from '../../../DadosEmpresa/Funcionario/Autocomplete/Autocomplete'
import {
  TIPO_FUNCIONARIO_ENTREGA_LABEL
} from '../../../../../enums/tipo-funcionario-entrega'
import './Fieldset.scss'
/*
      funcionarios: params.depedencies.FuncionarioEntrega.schema,
        funcionario: { type: Schema.Types.ObjectId, ref: 'Funcionario' },
        tipo: { type: String, enum: TIPO_FUNCIONARIO_ENTREGA_LABEL }
*/
/**
 * Area de inputs para Funcionário
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para funcionário da entrega
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  const { disabled } = useMemo(() => props, [props])

  const fields = useMemo(() => {
    return {
      funcionario: {
        name: getFieldName('funcionario', props),
        label: 'Funcionário'
      },
      tipo: {
        name: getFieldName('tipo', props),
        label: 'Tipo'
      }
    }
  }, [props])
  
  const [, funcionarioMeta, funcionarioHelpers] = useField(
    fields.funcionario.name
    )

  const setFuncionario = useCallback(
    (value: any) => funcionarioHelpers.setValue(value),
    [funcionarioHelpers]
  )

  const funcionario = useMemo(
    () => funcionarioMeta.value || null,
    [funcionarioMeta]
  )

  return (
    <Grid container spacing={2}>

      <Grid item xs={12} sm={6}>

        <SelectInput
          disabled={disabled}
          name={fields.tipo.name}
          label={fields.tipo.label}
          enumOptions={TIPO_FUNCIONARIO_ENTREGA_LABEL}
          fullWidth
        />

      </Grid>

      <Grid item xs={12} sm={6}>
        <AutocompleteFuncionario
          id="funcionario-autocomplete-funcionario"
          size="small"
          fullWidth
          onChange={setFuncionario}
          value={funcionario}
          disabled={disabled}
          />
      </Grid>

    </Grid>
  )
})
