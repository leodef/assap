import React,
{
  useCallback,
  useEffect,
  useMemo, useState
} from 'react'
import {
  Checkbox,
  FormControlLabel,
  Grid
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  SelectInput
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  ALGORITIMO_ROTA, ALGORITIMO_ROTA_LABEL
} from '../../../../../enums/algoritimo-rota'
import { useField } from 'formik'
import './Fieldset.scss'

/**
 * Componente Fieldset
 * @param {any} props Propriedades
 * @return {React.Component} Componente Fieldset
 */
export const Fieldset = (props: any) => {
  const { disabled, pontos } = useMemo(() => props, [props])
  const [algoritimoHabilitado, setAlgoritimoHabilitado] = useState(false)
  const alterarEscolhaAlgoritimo = useCallback((event: any) => {
    setAlgoritimoHabilitado(event.target.checked)
  }, [setAlgoritimoHabilitado])

  const fields = useMemo(() => {
    return {
      limite: {
        name: getFieldName('limite', props),
        label: 'Limite'
      },
      calculo: {
        name: getFieldName('calculo', props),
        label: 'Calculo'
      },
      algoritimo: {
        name: getFieldName('algoritimo', props),
        label: 'Método de Rota'
      }
    }
  }, [props])

  const [, calculoMeta] = useField(
    fields.calculo.name
    )
  const [, algoritimoMeta, algoritimoHelper] = useField(
    fields.algoritimo.name
    )

  const calculo = useMemo(
    () => calculoMeta.value,
    [calculoMeta.value]
    )
  const defaultValueAlgoritimo = useMemo(
    () => (pontos && pontos.length < 3)
      ? ALGORITIMO_ROTA.naive
      : ALGORITIMO_ROTA.nearestNeighbor,
    [pontos])

  useEffect(() => {
    if (
      !algoritimoHabilitado &&
      algoritimoMeta.value !== defaultValueAlgoritimo &&
      !calculo) {
      algoritimoHelper.setValue(defaultValueAlgoritimo)
    }
  }, [
    defaultValueAlgoritimo,
    algoritimoHabilitado,
    algoritimoMeta,
    algoritimoHelper,
    calculo
  ])

  return (
    <Grid container
      spacing={2}>

      <Grid item xs={6}>
        <SelectInput
          disabled={disabled || !algoritimoHabilitado}
          name={fields.algoritimo.name}
          label={fields.algoritimo.label}
          enumOptions={ALGORITIMO_ROTA_LABEL}
        />
      </Grid>
      {disabled ? null : (<Grid item xs={6}>
        <FormControlLabel
          control={
            <Checkbox
              checked={algoritimoHabilitado}
              onChange={alterarEscolhaAlgoritimo}
              name="checkedB"
              color="primary"
            />
          }
          label={!algoritimoHabilitado
            ? 'Deseja escolher método de rota ?'
            : 'Escolha manual do método de rota'}

        />
      </Grid>)
      }
    </Grid>)
}
