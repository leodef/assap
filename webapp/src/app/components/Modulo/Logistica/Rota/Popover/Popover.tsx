import React, { useMemo } from 'react'
import {
  makeStyles,
  createStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  Popover as MuiPopover,
  List,
  ListItemText,
  ListItem,
  Divider
} from '@material-ui/core'
import { TIPO_FORMACAO } from '../../../../../enums/tipo-formacao'
import './Popover.scss'

/**
 * Componente representando informações de um ponto da rota em forma de Popover
 * @param {any} props Propriedades
 * @return {React.Component} Componente Popover
 */
export const Popover = (props: any) => {
  const classes = useStyles()
  const {
    id,
    open,
    anchorEl,
    pontoInfo,
    handlePopoverClose
  } = useMemo(() => props, [props])

  const { titulo, tipoFormacaoDesc, limite } = useMemo(
    () => pontoInfo || {}, [pontoInfo])
  const desc = useMemo(() =>
    tipoFormacaoDesc === TIPO_FORMACAO.MANUAL
      ? pontoInfo.desc
      : null,
    [
      tipoFormacaoDesc,
      pontoInfo
    ])

  const limiteTitulo = useMemo(
    () => limite && limite.peso
      ? limite.peso > 0 ? 'Carga' : 'Descarga'
      : null,
    [limite])

  const limiteDesc = useMemo(
    () => limite && limite.peso ? `${
      Math.abs(limite.peso)
    } Kg / ${
      Math.abs(limite.volume)
    } L` : null,
    [limite])
    // disableRestoreFocus
  return (<MuiPopover
    id={id}
    open={open}
    anchorEl={anchorEl}
    className={classes.popover}
    classes={{
      paper: classes.paper
    }}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right'
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right'
    }}
    onClose={handlePopoverClose}
    >
    { pontoInfo ? (
      <List
        dense
        className={classes.root}
        aria-labelledby="nested-list-subheader">
        <ListItem dense>
          <ListItemText
            primary={titulo}
            secondary={desc} />
        </ListItem>
        <Divider />
        <ListItem dense>
          <ListItemText
            primary="Ordem / Status / Distancia"
            secondary={`${
                pontoInfo.ordem || ''
              } / ${
                pontoInfo.status || ''
              } / ${
                pontoInfo.distancia || ''
              }`} />
        </ListItem>
        { limiteTitulo
          ? (<ListItem dense>
            <ListItemText
              primary={limiteTitulo}
              secondary={limiteDesc} />
          </ListItem>)
          : null
        }
        { (
          pontoInfo.distanciaDesc ||
          pontoInfo.duracaoDesc ||
          pontoInfo.duracaoComTrafegoDesc
        ) ? (<ListItem dense>
            <ListItemText
              primary="Distancia / Duracao / Duracao com tráfego"
              secondary={`${
                pontoInfo.distanciaDesc || ''
              } / ${
                pontoInfo.duracaoDesc || ''
              } / ${
                pontoInfo.duracaoComTrafegoDesc || ''
              }`} />
          </ListItem>) : null }
        { pontoInfo.localizacao ? (<React.Fragment>
          <Divider />
          <ListItem dense>
            <ListItemText
              primary="Localização" />
          </ListItem>
          { pontoInfo.localizacao.endereco ? (<ListItem dense>
            <ListItemText
              primary="Endereço"
              secondary={pontoInfo.localizacao.endereco} />
          </ListItem>) : null }
          <ListItem dense>
            <ListItemText
              primary="Lat / Long"
              secondary={`${
                pontoInfo.localizacao.lat || 0
              } / ${
                pontoInfo.localizacao.long || 0
              }`} />
          </ListItem>
        </React.Fragment>) : null
        }
      </List>) : null
    }
  </MuiPopover>)
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    popover: {
      pointerEvents: 'none'
    },
    paper: {
      padding: theme.spacing(0.5)
    },
    root: {
      width: '100%',
      maxWidth: '36ch',
      backgroundColor: theme.palette.background.paper
    }
  })
)
