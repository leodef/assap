/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Chip } from './Chip'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Chip />, div)
  ReactDOM.unmountComponentAtNode(div)
})
