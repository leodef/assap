import React,
{ useMemo, useCallback } from 'react'
import {
  Chip as MuiChip,
  Badge
} from '@material-ui/core'
import {
  Add as AddIcon,
  Room as RoomIcon
} from '@material-ui/icons'
import {
  PontoRota
} from '../../../../../types/API/Logistica/PontoRota'
import './Chip.scss'

/**
 * Componente representando um ponto da rota em forma de Chip
 * @param {any} props Propriedades
 * @return {React.Component} Componente Chips
 */
export const Chip = (props: any) => {
  const {
    ponto,
    index,
    ariaOwns,
    editar,
    novo,
    remover,
    handlePopoverOpen,
    handlePopoverClose,
    disabled,
    calculo
  } = props
  const color = useMemo(
    () => calculo ? 'secondary' : 'primary',
    [calculo]) as 'primary' | 'secondary'
  const size = useMemo(
    () => 'small',
    []) as 'small' | 'medium' | undefined
  // #region callbacks
  const handleDeletePontoEntrega = useCallback(
    (ponto, index) => !disabled ? remover(ponto, index) : null,
    [remover, disabled])
  const handleClickPontoEntrega = useCallback(
    (ponto, index) => !disabled ? editar(ponto, index) : null,
    [editar, disabled])
  const handleDeleteNovo = useCallback(
    () => !disabled ? novo() : null,
    [novo, disabled])
  const handleClickNovo = useCallback(
    () => !disabled ? novo() : null,
    [novo, disabled])
  const label = useMemo(
    () =>  PontoRota.getSubTitulo(ponto),
    [ponto])
  // #endregion  callbacks
  return ponto ? (
    <Badge
      badgeContent={
        ponto.distancia
          ? (parseFloat(ponto.distancia) / 1000).toFixed(2)
          : null}
      color="primary"
      onMouseEnter={
        (event: any) => disabled
          ? handlePopoverOpen(ponto, index)(event)
          : null
      }
      onMouseLeave={
        (event: any) => disabled
          ? handlePopoverClose(event)
          : null
      }
      >
      <MuiChip
        color={color}
        size={size}
        label={label}
        disabled={disabled}
        onDelete={
          (event: any) => handleDeletePontoEntrega(ponto, index)}
        onClick={
          (event: any) => handleClickPontoEntrega(ponto, index)}
        icon={<RoomIcon
          aria-owns={ariaOwns}
          aria-haspopup="true"
          onMouseEnter={
            handlePopoverOpen(ponto, index)
          }
          onMouseLeave={
            handlePopoverClose
          }
        />} />
    </Badge>
  ) : (
    <MuiChip
      color={color}
      size={size}
      label="Nova parada"
      deleteIcon={<AddIcon />}
      onDelete={handleDeleteNovo}
      onClick={handleClickNovo}
      disabled={disabled} />
  )
}
