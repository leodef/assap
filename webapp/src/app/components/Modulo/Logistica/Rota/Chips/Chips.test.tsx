/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Chips } from './Chips'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Chips />, div)
  ReactDOM.unmountComponentAtNode(div)
})
