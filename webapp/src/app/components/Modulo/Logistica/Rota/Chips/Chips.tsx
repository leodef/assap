import React,
{
  useMemo,
  useCallback,
  useState,
  useContext
} from 'react'
import {
  Button,
  Grid,
  makeStyles,
  Theme,
  createStyles, ButtonGroup, Typography
} from '@material-ui/core'
import {
  useField, useFormikContext
  // useFormikContext
} from 'formik'
import {
  useSelector, useDispatch
} from 'react-redux'
import {
  crudType
} from '../../../../../types/API/Logistica/Entrega'
import {
  PontoRota
} from '../../../../../types/API/Logistica/PontoRota'
import {
  CrudContext
} from '../../../../../contexts/CrudContext'
import {
  getFormName,
  getFieldName
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  getDistanceDesc,
  getDistanceMatrix,
  getDurationDesc,
  getLocalDistanceMatrix
} from '../../../../../utils/GoogleMaps/GoogleUtils'
import {
  DistanceMatrixRequest
} from '../../../../../utils/GoogleMaps/request/DistanceMatrixRequest'
import {
  Limite
} from "../../../../../types/API/Limite"
import {
  initialValues as limiteInitialValues
} from "../../../../../types/API/Limite"
import { ParentCrudContext } from '../../../../../contexts/ParentCrudContext'
import {
  Chip as PontoRotaChip
} from '../Chip/Chip'
import {
  Popover as PontoRotaPopover
} from '../Popover/Popover'
import {
  Modal as PontoRotaModal
} from '../../PontoRota/Modal/Modal'
import './Chips.scss'

/**
 * Componente representando os pontos da rota em forma de Chips
 * @param {any} props Propriedades
 * @return {React.Component} Componente Chips
 */
export const Chips = (props: any) => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const { getState } = useContext(CrudContext)
  const { google } = useMemo(() => window as any, [])
  const {
    disabled,
    modalHistory,
    limite,
    calculo
  } = useMemo(() => props, [props])

  const rotaItem = useSelector(state => {
    const entrega = getState(state)
    return entrega ? entrega.rota : null
  })

  // #region form
  const { values } = useFormikContext<any>()
  const fields = useMemo(() => {
    return {
      rota: {
        name: getFormName(props),
        label: 'Rota'
      },
      pontos: {
        name: getFieldName('pontos', props),
        label: 'Pontos'
      },
      calculo: {
        name: getFieldName('calculo', props),
        label: 'Calculo'
      },
      distancia: {
        name: getFieldName('distancia', props),
        label: 'Distância'
      },
      duracao: {
        name: getFieldName('duracao', props),
        label: 'Duração'
      },
      duracaoComTrafego: {
        name: getFieldName('duracaoComTrafego', props),
        label: 'Duracao com tráfego'
      }
    }
  }, [props])

  // calculo
  const [, rotaMeta] = useField(
    fields.rota.name
    )
  const [, , calculoHelpers] = useField(
    fields.calculo.name
    )
  const setCalculo = useCallback(
    (value: any) => calculoHelpers.setValue(value), [calculoHelpers])

  // pontos
  const [, pontosMeta, pontosHelpers] = useField(
    fields.pontos.name
    )
  const pontos = useMemo(
    () => (pontosMeta.value || []),
    [pontosMeta.value])
  const setPontos = useCallback(
    (value: any) => pontosHelpers.setValue(value),
    [pontosHelpers])
  const lastIndex = useMemo(
    () => (pontos.length + 1),
    [pontos])
  const chips = useMemo(
    () => [...pontos, null],
    [pontos])
  const statusRota = useMemo(
    () => pontos.reduce(
      (prev: any, curr: any) => 
        ({
          limite: Limite.accum(prev.limite, curr.limite)
        }),
        ({ limite: limiteInitialValues })
      ),
    [
      pontos
    ])
  // #endregion form

  const descLimiteRota  = useMemo(() => {
    return limite && limite.peso
        ? ` Capacidade de carga ${limite.peso} Kg, ${
          limite.volume
        } L ${
          limite.tara ? ' | Peso do veiculo: ' + limite.tara : null
        } Kg `
      : null
  }, [limite]) 

  const descStatusRotaRota  = useMemo(() => {
    return statusRota && statusRota.limite && statusRota.limite.peso
      ? `${
          statusRota.limite.peso > 0
            ? 'Carregado'
            : 'Descarregado'
        }: ${statusRota.limite.peso} Kg, ${
          statusRota.limite.volume
        } L`
      : null
  }, [statusRota]) 

  const rotaDuracaoDesc = useMemo(() => {
    const { distancia, duracao, duracaoComTrafego } = rotaMeta.value
    return [
      distancia
        ? `Distância estimada: ${getDistanceDesc(distancia)}`
        : null,
      duracao
        ? `Tempo estimado: ${getDurationDesc(duracao)}`
        : null,
      duracaoComTrafego
        ? `Tempo estimado com tráfego: ${getDurationDesc(duracaoComTrafego)}`
        : null
    ].filter(val => Boolean(val)).join(' | ')
  }, [rotaMeta.value])

  // #region popover
  const [id, setId] = useState(null as any)
  const [anchorEl, setAnchorEl] = useState(null as any)
  const [pontoInfo, setPontoInfo] = useState(null as any)
  const open = useMemo(() => Boolean(anchorEl), [anchorEl])
  const handlePopoverOpen = useCallback((ponto: any, index: number) => {
    return (event: any) => {
      setAnchorEl(event.currentTarget)
      setPontoInfo(ponto)
      setId(`ponto_desc_${index}`)
    }
  }, [setAnchorEl, setPontoInfo, setId])
  const handlePopoverClose = useCallback((event: any) => {
    setAnchorEl(null)
    setId(null)
  }, [setAnchorEl, setId])
  // #endregion popover

  // #region modal
  const [modalInfo, setModalInfo] = useState({ isOpen: false, item: null } as {isOpen: boolean, item: any})
  const salvarPonto = useCallback((value: any) => {
    let find = false
    let ptos = pontos.map((item: any, index: number) => {
      if (PontoRota.compare(value, item, (index + 1))) {
        find = true
        return value
      }
      return item
    })
    if (!find) {
      ptos = [...ptos, value]
    }
    setPontos(ptos)
  }, [pontos, setPontos])
  // #endregion  modal

  // #region functions
  const distanceMatrixRequest = useMemo(() =>
    DistanceMatrixRequest.loadEntrega(
      values,
      DistanceMatrixRequest.getInstance(google)
    )
  , [values, google])
  // Buttons
  // PontoRota.atualizarOrdemRota (ponto: any, ordem: number, pontos: Array<any> = [])
  const parentCrudContextValue = useContext(ParentCrudContext)
  const calcularRota = useCallback((item: any, event: any) => {
    const  { request, map } = distanceMatrixRequest ? ({
      request: distanceMatrixRequest.toRequest(),
      map: distanceMatrixRequest._map
    }) : ({}) as any
    if (!request) { return }
    getDistanceMatrix(
      request,
      (result: any) => {
        const rows = (
            result ?
            result.rows || null
            : null
          ) || getLocalDistanceMatrix(request)
        dispatch({
          type: crudType.CALCULO_ROTA,
          payload: {
            item,
            rows,
            map,
            parent: parentCrudContextValue
          }
        })
      }
    )
  }, [
    dispatch,
    distanceMatrixRequest,
    parentCrudContextValue
  ])

  const editarRota = useCallback((event: any) => {
    setCalculo(false)
    const novoPontos = pontos.map((ponto: any) => {
      const result = {
        ...ponto,
        distancia: null,
        distanciaDesc: null,
        duracao: null,
        duracaoComTrafego: null,
        duracaoComTrafegoDesc: null,
        duracaoDesc: null,
      }
      return result
    })
    setPontos(novoPontos)
  }, [setCalculo, setPontos, pontos])
  const limparRota = useCallback(() => setPontos(rotaItem ? rotaItem.pontos || [] : []),
  [setPontos, rotaItem])
  // Chips
  const editar = useCallback(
    (item: any, index: number) => setModalInfo({ item, isOpen: true }),
    [setModalInfo])
  const novo = useCallback(
    () => setModalInfo({ item: { ordem: lastIndex } as any, isOpen: true }),
    [setModalInfo, lastIndex])
  const remover = useCallback(
    (value: any, index: number) => {
      const ptos = pontos.filter(
        (item: any) => !PontoRota.compare(value, item))
          // Remove o elemento do filtro
          setPontos(ptos)
    },
    [pontos, setPontos])

  // #endregion  functions

  // utils
  const getPontoIndex = useCallback((ponto: any, index: number) => `ponto_key_${index}`, [])
  
  return (<React.Fragment>
    <Grid
      container
      spacing={1}>
      <Grid
        item
        xs={10}>
        <ButtonGroup
          size="small"
          variant="contained"
          color="primary">
          <Button
            size="small"
            disabled={!disabled}
            onClick={(event: any) => editarRota(event)} >
              Editar rota
          </Button>
          { calcularRota ? (
            <Button
              size="small"
              disabled={disabled}
              onClick={(event: any) => calcularRota(values, event) }>
              Calcular rota
            </Button>) : null
          }
          <Button
            size="small"
            disabled={disabled}
            onClick={limparRota}>
            Limpar rota
          </Button>
        </ButtonGroup>
      </Grid>

      <Grid
        item
        container
        xs={10}
        spacing={3}
        className={classes.container}>
        { chips.map((ponto: any, index: number) =>
          (<PontoRotaChip
            disabled={disabled}
            ariaOwns={id}
            ponto={ponto}
            index={index}
            key={getPontoIndex(ponto, index)}
            editar={editar}
            novo={novo}
            remover={remover}
            handlePopoverOpen={handlePopoverOpen}
            handlePopoverClose={handlePopoverClose}
            calculo={calculo} />))
        }
        <PontoRotaPopover
          id={id}
          anchorEl={anchorEl}
          pontoInfo={pontoInfo}
          open={open}
          handlePopoverClose={handlePopoverClose}
          disabled={disabled} />
      </Grid>

      <Grid item xs={12}>

        <Typography variant="caption" display="block" gutterBottom align="center">
          { descStatusRotaRota }
        </Typography>

        <Typography variant="caption" display="block" gutterBottom align="center">
          { descLimiteRota }
        </Typography>

        <Typography variant="caption" display="block" gutterBottom align="center">
          { rotaDuracaoDesc }
        </Typography>
      </Grid>

    </Grid>

    <PontoRotaModal
      {...props}
      {...modalInfo}
      save={salvarPonto}
      setModalInfo={setModalInfo}
      modalHistory={modalHistory} 
      disabled={disabled}
      pontos={pontos}
      statusRota={statusRota} />
  </React.Fragment>)
}
/*
    <PontoRotaModal
      {...props}
      {...modalInfo}
      save={salvarPonto}
      setModalInfo={setModalInfo}
      modalHistory={modalHistory} 
      disabled={disabled}
      pontos={pontos}

      produto={produto}
      parceiroEntrega={parceiroEntrega}
      transporte={transporte}
      funcionarios={funcionarios}
      limite={limite}
      tipoLimite={tipoLimite}
    />
*/

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      // color: theme.palette.text.secondary,
      '& > *': {
        margin: theme.spacing(1.5)
      },
      margin: theme.spacing(1),
      display: 'flex',
      justifyContent: 'center',
      flexWrap: 'wrap',
      overflowY: 'auto'
      /*
      flexGrow: 1,
      flex: 1,
      overflow: 'auto'
      */
    }
  })
)
