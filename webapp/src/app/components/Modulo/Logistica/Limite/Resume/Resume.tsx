import React, { useMemo } from "react";
import { createStyles, makeStyles, Theme, Typography } from "@material-ui/core";
import "./Resume.scss";

/**
 * Visualização de item do tipo Limite
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Limite
 */
export const Resume = (props: any) => {
  const classes = useStyles();
  const { item, labels } = useMemo(() => props || {}, [props]);
  return (
    <React.Fragment>
      <Typography variant="body2" component="p">
        {item.tara ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}>
              {labels && labels.tara ? labels.tara : "Tara"}:
            </strong>
            {item.tara}
            <i>Kg</i>
          </React.Fragment>
        ) : null}

        {item.peso ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}>
              - {labels && labels.peso ? labels.peso : "Peso"}:
            </strong>
            {item.peso}
            <i>Kg</i>
          </React.Fragment>
        ) : null}

        {item.volume ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}>
              - {labels && labels.volume ? labels.volume : "Volume"}:
            </strong>
            {item.volume}
            <i>L</i>
          </React.Fragment>
        ) : null}
      </Typography>
    </React.Fragment>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    resumeLabel: {
      marginRight: "0.35em",
      marginLeft: "0.35em",
    },
  })
);
