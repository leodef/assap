import React,
{
  useMemo
} from "react";
import {
  Grid,
  InputAdornment
} from "@material-ui/core";
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  TextField,
} from "../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField";
import "./Fieldset.scss";

/**
 * Componente Fieldset
 * @param {any} props Propriedades
 * @return {React.Component} Componente Fieldset
 */
export const Fieldset = (props: any) => {
  const {
    disabled,
    maxLimite,
    helperText,
    useTara,
    labels
  } = useMemo(
    () => props || {},
    [props]
  );
  const fields = useMemo(() => {
    return {
      tara: {
        name: getFieldName("tara", props),
        label: labels && labels.tara ? labels.tara : "Tara",
        helperText: helperText && helperText.tara ? helperText.tara : null,
        max: maxLimite ? maxLimite.tara : undefined,
      },
      peso: {
        name: getFieldName("peso", props),
        label: labels && labels.peso ? labels.peso : "Peso",
        helperText: helperText && helperText.peso ?
          helperText.peso : 
          (maxLimite ? `Max: ${maxLimite.peso}` : undefined),
        max: maxLimite ? maxLimite.peso : undefined,
      },
      volume: {
        name: getFieldName("volume", props),
        label: labels && labels.volume ? labels.volume : "Volume",
          helperText: helperText && helperText.volume ?
          helperText.volume : 
          (maxLimite ? `Max: ${maxLimite.volume}` : undefined),
        max: maxLimite ? maxLimite.volume : undefined,
      },
    };
  }, [props, helperText, maxLimite, labels]);
  return (
    <Grid item container xs={12}>
      {!useTara ? null : (
        <Grid item xs={4}>
          <TextField
            name={fields.tara.name}
            type="number"
            label={fields.tara.label}
            helperText={fields.tara.helperText}
            disabled={disabled}
            InputProps={{
              startAdornment: <InputAdornment position="start">
                <i>Kg</i>
              </InputAdornment>,
            }}
          />
        </Grid>
      )}
      <Grid item xs={useTara ? 4 : 6}>
        <TextField
          name={fields.peso.name}
          type="number"
          label={fields.peso.label}
          helperText={fields.peso.helperText}
          disabled={disabled}
          InputProps={{
            startAdornment: <InputAdornment position="start">
              <i>Kg</i>
            </InputAdornment>,
          }}
        />
      </Grid>
      <Grid item xs={useTara ? 4 : 6}>
        <TextField
          name={fields.volume.name}
          type="number"
          label={fields.volume.label}
          helperText={fields.volume.helperText}
          disabled={disabled}
          InputProps={{
            startAdornment: <InputAdornment position="start">
              <i>L</i>
            </InputAdornment>,
          }}
        />
      </Grid>
    </Grid>
  );
};
