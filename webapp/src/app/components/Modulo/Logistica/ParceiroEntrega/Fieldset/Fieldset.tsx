import
React,
{
  useCallback,
  useMemo,
  useState
} from 'react'
import {
  Grid,
} from '@material-ui/core'
import {
  useField
} from 'formik'
import {
  getFieldName
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  Autocomplete as AutocompleteParceiro
} from '../../../DadosEmpresa/Parceiro/Autocomplete/Autocomplete'
import {
  LocalAutocomplete as LocalAutocompleteContatoParceiro
} from '../../../DadosEmpresa/ContatoParceiro/LocalAutocomplete/LocalAutocomplete'
import './Fieldset.scss'
/*
      parceiroEntrega: params.depedencies.ParceiroEntrega.schema,
        parceiro: { type: Schema.Types.ObjectId, ref: 'Parceiro' },
        contato: { type: Schema.Types.ObjectId, ref: 'Contato' },
*/
/**
 * Area de inputs para Funcionario
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Funcionario
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  const { disabled } = useMemo(() => props, [props])

  const fields = useMemo(() => {
    return {
      parceiro: {
        name: getFieldName('parceiro', props),
        label: 'Parceiro'
      },
      contato: {
        name: getFieldName('contato', props),
        label: 'Contato'
      }
    }
  }, [props])
  
// contato
const [, contatoMeta, contatoHelpers] = useField(
  fields.contato.name
)
const [ contatoInputValue, setContatoInputValue] = useState('')
const onContatoInputValueChange = useCallback(
  (event: object, value: string, reason: string) =>
  setContatoInputValue(value),
  [setContatoInputValue],
)
const setContatoValue = useCallback(
  (value: any) => contatoHelpers.setValue(value),
  [contatoHelpers]
)
const contatoValue = useMemo(
  () => contatoMeta.value || null,
  [contatoMeta]
)


  // parceiro
  const [, parceiroMeta, parceiroHelpers] = useField(
    fields.parceiro.name
  )
  const setParceiro = useCallback(
    (value: any) => {
      parceiroHelpers.setValue(value)
      setContatoValue(null)
      setContatoInputValue('')
    },
    [parceiroHelpers, setContatoValue, setContatoInputValue]
  )
  const parceiro = useMemo(
    () => parceiroMeta.value || null,
    [parceiroMeta]
  )

  // filiais
  const filiais = useMemo(
    () => ((parceiro && parceiro.filiais)
      ? parceiro.filiais
      : []),
      [parceiro])

  // contatos
  const contatos = useMemo(
    () => ((parceiro && parceiro.contatos)
      ? parceiro.contatos
      : []),
      [parceiro])

  //  parceiro / contato
  const contato = useMemo(
    () => contatos && contatoValue
      ? contatos.find((val: any) => (val._id || val) === (contatoValue._id || contatoValue))
      : null,
    [contatoValue, contatos]
  )

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={6}>
        <AutocompleteParceiro
          id="parceiro-entrega-autocomplete-parceiro"
          size="small"
          fullWidth
          onChange={setParceiro}
          value={parceiro}
          disabled={disabled}
          />
      </Grid>

      <Grid item xs={12} sm={6}>
        <LocalAutocompleteContatoParceiro
          id="parceiro-entrega-autocomplete-contato"
          size="small"
          fullWidth
          disabled={disabled}
          value={contato}
          onChange={setContatoValue}
          inputValue={contatoInputValue}
          onInputChange={onContatoInputValueChange}
          options={contatos}
          filiais={filiais}
          />
      </Grid>

    </Grid>
  )
})
