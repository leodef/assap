import React, { useState } from 'react'
// eslint-disable-next-line no-unused-vars
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse
} from '@material-ui/core'
import {
  ExpandLess as ExpandLessIcon,
  ExpandMore as ExpandMoreIcon,
  SupervisorAccount as SupervisorAccountIcon
} from '@material-ui/icons'
import { ListItemLink } from '../../../Shared/Utils/ListItemLink/ListItemLink'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nested: {
      paddingLeft: theme.spacing(4)
    }
  })
)

/**
 * Menu com atalhos para telas do modulo CalcCompQuim
 * @param {any} props Propriedades
 * @return {React.Component} Componente com menu com atalhos para telas do modulo CalcCompQuim
 */
export const CalcCompQuimSideBarListItem = function (props: any) {
  const classes = useStyles()
  const { toggleSidebar } = props
  const [open, setOpen] = useState(false)
  const handleClick = () => {
    setOpen(!open)
  }

  return (
    <React.Fragment>
      <ListItem button onClick={handleClick}>
        <ListItemIcon>
          <SupervisorAccountIcon />
        </ListItemIcon>
        <ListItemText primary="Calculo de composição quimica" />
        {open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        { /* SubLista */ }
        <List component="div" disablePadding>
          <ListItemLink
            to="/calc-comp-quim/composicao-substancia"
            primary="Composição de substancia"
            className={classes.nested}
            onClick={() => toggleSidebar()}/>
          <ListItemLink
            to="/calc-comp-quim/elemento"
            primary="Elemento"
            className={classes.nested}
            onClick={() => toggleSidebar()}/>
          <ListItemLink
            to="/calc-comp-quim/substancia"
            primary="Substancia"
            className={classes.nested}
            onClick={() => toggleSidebar()}/>
        </List>
      </Collapse>
    </React.Fragment>
  )
}
export default CalcCompQuimSideBarListItem
