import
React,
{
  useMemo,
  useState
} from 'react'
import {
  useSelector
} from 'react-redux'
import {
  makeStyles,
  Theme,
  createStyles
} from '@material-ui/core/styles'
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse
} from '@material-ui/core'
import {
  ExpandLess as ExpandLessIcon,
  ExpandMore as ExpandMoreIcon,
  SupervisorAccount as SupervisorAccountIcon
} from '@material-ui/icons'
import {
  ListItemLink
} from '../../../Shared/Utils/ListItemLink/ListItemLink'
import {
  getState
} from '../../../../types/EmpresaUsuario'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nested: {
      paddingLeft: theme.spacing(4)
    }
  })
)

/**
 * Menu com atalhos para telas do modulo Admin
 * @param {any} props Propriedades
 * @return {React.Component} Componente com menu com atalhos para telas do modulo Admin
 */
export const DadosEmpresaSideBarListItem = function (props: any) {
  const classes = useStyles()
  const { item } = useSelector((state: any) => getState(state))
  const allowed = useMemo(() => Boolean(item), [item])
  const { toggleSidebar } = props
  const [open, setOpen] = useState(false)
  const handleClick = () => {
    if(!allowed) {
      setOpen(false)
    } else {
      setOpen(!open)
    }
  }

  return (
    <React.Fragment>
      <ListItem
        button
        onClick={handleClick}
        disabled={!allowed}>
        <ListItemIcon>
          <SupervisorAccountIcon />
        </ListItemIcon>
        <ListItemText primary="Dados da Empresa" />
        {open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        { /* SubLista */ }
        <List component="div" disablePadding>
          <ListItemLink
            to="/dados-empresa/entrega"
            primary="Entrega"
            className={classes.nested}
            onClick={() => toggleSidebar()}/>
          
          <ListItemLink
            to="/dados-empresa/filial"
            primary="Filial"
            className={classes.nested}
            onClick={() => toggleSidebar()}/>
          
          <ListItemLink
            to="/dados-empresa/funcionario"
            primary="Funcionário"
            className={classes.nested}
            onClick={() => toggleSidebar()}/>


          <ListItemLink
            to="/dados-empresa/parceiro"
            primary="Parceiro"
            className={classes.nested}
            onClick={() => toggleSidebar()}/>

          <ListItemLink
            to="/dados-empresa/produto"
            primary="Produto"
            className={classes.nested}
            onClick={() => toggleSidebar()}/>


          <ListItemLink
            to="/dados-empresa/veiculo"
            primary="Veiculo"
            className={classes.nested}
            onClick={() => toggleSidebar()}/>
        </List>
      </Collapse>
    </React.Fragment>
  )
}
export default DadosEmpresaSideBarListItem
