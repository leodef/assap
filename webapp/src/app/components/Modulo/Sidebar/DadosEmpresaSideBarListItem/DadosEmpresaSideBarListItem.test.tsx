/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import DadosEmpresaSideBarListItem from './DadosEmpresaSideBarListItem'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<DadosEmpresaSideBarListItem />, div)
  ReactDOM.unmountComponentAtNode(div)
})
