import React from 'react'
import {
  Typography
} from '@material-ui/core'
import './Resume.scss'

/**
 * Visualização de item do tipo QuantidadeElemento
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo QuantidadeElemento
 */
export const Resume = (props: any) => {
  /*
    QuantidadeElemento
      elemento: Elemento
      quantidade: number (%)
  */
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.elemento.titulo}
    </Typography>
    <Typography variant="body2" component="p">
      {item.quantidade}
    </Typography>
  </React.Fragment>)
}
