import
React, {
  useMemo, useCallback
} from 'react'
import {
  Assignment as AssignmentIcon
} from '@material-ui/icons'
import {
  CollectionTypeEnum
} from '../../../../types/Crud'
import { Crud } from '../../../Shared/Crud/Crud'
import { TablePropsField } from '../../../Shared/Collection/Table/Table'
import { Resume } from './Resume/Resume'
import { Fieldset } from './Fieldset/Fieldset'
import {
  QuantidadeElementoSchema,
  initialValues
} from '../../../../types/API/CalcCompQuim/QuantidadeElemento'
import './QuantidadeElemento.scss'

export const QuantidadeElemento = (props: any) => {
  const { collection, form, show, remove } = props
  const { type } = (collection || {})
  const fields = useMemo(() => [
    new TablePropsField('titulo', 'Título'),
    new TablePropsField('desc', 'Descrição')
  ], [])
  const tableConfig = useMemo(() => {
    return {
      label: 'QuantidadeElemento',
      align: 'center',
      actionsLabel: 'Ações',
      fields
    }
  }, [fields])

  const ShowBody = useCallback((props: any) => {
    return (<Resume
      {...props} />)
  }, [])
  const FormBody = useCallback((props: any) => {
    return (<Fieldset
      {...props} />)
  }, [])

  const listConfig = useMemo(() => {
    return {
      dense: true,
      subheader: 'QuantidadeElemento',
      fields,
      getListItem: (value: any, index: number) => {
        return {
          primary: (value.titulo ? value.titulo.toString() : ''),
          secondary: null,
          avatar: (value.titulo ? value.titulo.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
        }
      }
    }
  }, [fields])
  const config = (type === CollectionTypeEnum.LIST) ? listConfig : tableConfig
  const params = useMemo(() => {
    const params = {
      collection: {
        type,
        config,
        // resolve: 'FRONT' // 'BACK'
        ...(collection || {})
      },
      show: {
        title: 'QuantidadeElemento',
        // eslint-disable-next-line react/display-name
        body: ShowBody,
        ...(show || {})
      },
      form: {
        title: 'QuantidadeElemento',
        // eslint-disable-next-line react/display-name
        body: FormBody,
        initialValues,
        schema: QuantidadeElementoSchema,
        ...(form || [])
      },
      remove: {
        title: 'Deseja realmente remover o elemento ?',
        ...(remove || {})
      }
    }
    return params
  }, [
    collection,
    config,
    form,
    remove,
    show,
    type,
    FormBody,
    ShowBody
  ])
  return <Crud {...params} />
}
