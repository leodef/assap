import React, { useMemo } from 'react'
import {
  Grid
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import { getFieldName, TextField } from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import { Autocomplete as SubstanciaAutocomplete } from '../../Substancia/Autocomplete/Autocomplete'
import './Fieldset.scss'

/**
 * Area de inputs para QuantidadeSubstancia
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para QuantidadeSubstancia
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  const { disabled } = useMemo(() => props, [props])

  /*
    QuantidadeSubstancia:
      substancia:Substancia
      quantidade: number
  */

  const fields = useMemo(() => {
    return {
      quantidade: {
        name: getFieldName('quantidade', props)
      }
    }
  }, [props])

  return (

    <Grid container spacing={2} >

      <Grid item xs={12} sm={6}>
        <SubstanciaAutocomplete
          disabled={disabled} />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.quantidade.name}
          type="number"
          label="Quantidade"
          disabled={disabled}
        />
      </Grid>

    </Grid>
  )
})
