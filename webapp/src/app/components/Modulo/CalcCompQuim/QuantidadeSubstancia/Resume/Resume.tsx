import React from 'react'
import {
  Typography
} from '@material-ui/core'
import './Resume.scss'

/**
 * Visualização de item do tipo QuantidadeSubstancia
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo QuantidadeSubstancia
 */
export const Resume = (props: any) => {
  /*
    QuantidadeSubstancia:
      substancia:Substancia
      quantidade: number
  */
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.substancia.titulo}
    </Typography>
    <Typography variant="body2" component="p">
      {item.quantidade}
    </Typography>
  </React.Fragment>)
}
