/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ComposicaoSubstancia } from './ComposicaoSubstancia'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ComposicaoSubstancia />, div)
  ReactDOM.unmountComponentAtNode(div)
})
