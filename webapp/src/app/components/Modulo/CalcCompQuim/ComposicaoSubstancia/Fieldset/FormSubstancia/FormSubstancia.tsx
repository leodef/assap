import React,
{
  useMemo,
  useCallback
} from 'react'
import {
  Grid,
  Modal,
  makeStyles,
  Card,
  CardContent,
  Typography,
  CardActions,
  Button,
  createStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  Paper,
  ButtonGroup
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import {
  TextField
} from '../../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  Send as SendIcon
} from '@material-ui/icons'
// eslint-disable-next-line no-unused-vars
import {
  withFormik,
  // eslint-disable-next-line no-unused-vars
  FormikHelpers
} from 'formik'
import './FormSubstancia.scss'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 300, // 275
      minHeight: 300
    },
    title: {
      fontSize: 14
    },
    pos: {
      marginBottom: 12
    },
    modalPaper: {
      position: 'absolute',
      minWidth: 200,
      minHeight: 200,
      // boxShadow: theme.shadows[5],
      // padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
      overflow: 'auto'
    },
    button: {
      margin: theme.spacing(0.5, 0)
    }
  })
)

/**
 * Componente FormSubstanciaModal
 * @param {any} props Propriedades
 * @return {React.Component} Componente FormSubstanciaModal
 */
export const FormSubstanciaModal = (props: any) => {
  const classes = useStyles()
  const { salvarSubstancia, isOpen, setOpen, setValue } = props
  const onClose = useCallback(() => {
    setOpen(false)
    setValue(null)
  }, [setOpen, setValue])
  const save = useCallback((values: any) => {
    salvarSubstancia(values)
    setOpen(false)
    setValue(null)
  }, [salvarSubstancia, setOpen, setValue])
  return (
    <Modal
      open={isOpen}
      onClose={onClose}
      aria-labelledby="Substancia"
      aria-describedby="Substancia">
      <div className={classes.modalPaper}>
        <FormSubstancia
          {...props}
          save={save}
          onClose={onClose}/>
      </div>
    </Modal>)
}

/**
 * Componente FormSubstancia
 * @param {any} props Propriedades
 * @return {React.Component} Componente FormSubstancia
 */
export const FormSubstancia = (props: any) => {
  // salvarSubstancia - clicar no botao salver
  const { save, value } = props
  // mapPropsToValues: () => _.defaults(value, initialValues),
  const propsToValues = useMemo(() => (value || []), [value])
  const WithFormik = withFormik({
    mapPropsToValues: () => propsToValues,
    handleSubmit: (values: any, formikHelpers: FormikHelpers<any>) => {
      const { setSubmitting } = formikHelpers
      setSubmitting(false)
      save(values)
    }
  })(FormSubstanciaBody)
  return (<WithFormik
    {...props}
    name={null}
    propsToValues={propsToValues} />)
}

/**
 * Componente FormSubstanciaBody
 * @param {any} props Propriedades
 * @return {React.Component} Componente FormSubstanciaBody
 */
export const FormSubstanciaBody = (props: any) => {
  const classes = useStyles()
  const { propsToValues, onClose } = props
  return (<form onSubmit={props.handleSubmit}>
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
        Substancia
        </Typography>
        <Grid container spacing={2}>
          { propsToValues.map((val: any, index: number) =>
            (<Grid
              item
              key={index}>
              <FormSubstanciaFieldset
                {...props}
                index={index}
                item={val} />
            </Grid>)
          )}
        </Grid>
      </CardContent>
      <CardActions>
        <ButtonGroup
          size="small"
          variant="contained"
          color="primary">
          <Button
            size="small"
            type="submit"
            variant="contained"
            color="primary"
            startIcon={<SendIcon />}>
          Salvar
          </Button>
          <Button
            size="small"
            variant="contained"
            color="primary"
            onClick={onClose}>
          Voltar
          </Button>
        </ButtonGroup>
      </CardActions>
    </Card>
  </form>)
}
/**
 * Componente FormSubstanciaBody
 * @param {any} props Propriedades
 * @return {React.Component} Componente FormSubstanciaBody
 */
export const FormSubstanciaFieldset = (props: any) => {
  const classes = useStyles()
  const { index, item, disabled } = props
  const { substancia, quantidade } = item
  return (<Paper elevation={2} className={classes.paper}>
    <Grid container>

      <Grid item xs={12} sm={6}>
        <Typography color="primary">{substancia.titulo}</Typography>
        <TextField
          name={`${index}.substancia`}
          type="hidden"
          value={substancia}
          disabled={disabled}
        />
      </Grid>

      <Grid item xs={12} sm={6}>

        <TextField
          name={`${index}.quantidade`}
          type="number"
          label="Quantidade"
          value={quantidade}
          disabled={disabled}
        />
      </Grid>

    </Grid>
  </Paper>)
}
