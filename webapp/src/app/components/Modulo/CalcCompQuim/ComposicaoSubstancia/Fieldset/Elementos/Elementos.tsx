import React from 'react'
import './Elementos.scss'
import { ChipElemento } from '../ChipElemento/ChipElemento'

/**
 * Componente Elementos
 * @param {any} props Propriedades
 * @return {React.Component} Componente Elementos
 */
export const Elementos = (props: any) => {
  // Lista elementos acumulados / filtro
  // cinza - acumulado < filtro, primary - acumulado = filtro, warning = acumulado > filtro
  // label - elemento.titulo quantidade / quantidadeFiltro
  let { elementos } = props
  elementos = elementos || []
  return elementos.map((elemento: any, index: number) => (
    <ChipElemento
      {...props}
      quantidadeElemento={elemento}
      key={index} />)
  )
}
