import React, { useMemo } from 'react'
import {
  Chip,
  Avatar
} from '@material-ui/core'
import WarningIcon from '@material-ui/icons/Warning'
/*
import {
  Done as DoneIcon
} from '@material-ui/icons'
*/
import './ChipElemento.scss'

/**
 * Componente ChipElemento
 * @param {any} props Propriedades
 * @return {React.Component} Componente ChipElemento
 */
export const ChipElemento = (props: any) => {
  const { quantidadeElemento } = props
  const { quantidade, elemento, filtro } = (quantidadeElemento || {})

  // Se ultrapassar o filtro mostrar como warning,
  //   se for igual o filtro mostrar como sucesso,
  //   se nao tiver filtro ou for menor que o filtro mostrar sem cor
  const color = useMemo(() => {
    if (!filtro) {
      return 'secondary'
    } else if (filtro === quantidade) {
      return 'primary'
    } else if (filtro > quantidade) {
      return 'default'
    } else if (filtro < quantidade) {
      return undefined // 'inherit'
    }
    return 'default'
  }, [filtro, quantidade])

  const warning = useMemo(() => filtro && filtro < quantidade, [filtro, quantidade])
  const {
    onClick,
    onDelete
  } = props
  const label = useMemo(() => {
    const qtdLabel = filtro
      ? `${quantidade || 0} / ${filtro || 0}`
      : `${quantidade || 0}`
    return `${elemento.titulo || ''} ${qtdLabel}`
  }, [elemento, quantidade, filtro])
  // deleteIcon={ onDelete ? <DoneIcon /> : undefined}
  return (<Chip
    avatar={
      elemento.titulo && !warning ? (<Avatar>
        { elemento.titulo.substring(0, 2)}
      </Avatar>) : undefined }
    icon={warning ? <WarningIcon /> : undefined}
    label={label}
    clickable
    color={color}
    onClick={onClick ? (event: any) => onClick(quantidadeElemento) : undefined}
    onDelete={onDelete ? (event: any) => onDelete(quantidadeElemento) : undefined}
  />)
}
