/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ChipElemento } from './ChipElemento'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ChipElemento />, div)
  ReactDOM.unmountComponentAtNode(div)
})
