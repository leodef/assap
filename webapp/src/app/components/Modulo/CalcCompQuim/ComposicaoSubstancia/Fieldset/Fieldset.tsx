import React,
{
  useMemo,
  useState,
  useCallback,
  useContext
} from 'react'
import {
  // eslint-disable-next-line no-unused-vars
  Grid, makeStyles, Theme, createStyles, Paper
} from '@material-ui/core'
import {
  CrudContext
} from '../../../../../contexts/CrudContext'
import {
  useSelector
} from 'react-redux'
import './Fieldset.scss'
import {
  FiltroElementos
} from './FiltroElementos/FiltroElementos'
import {
  ListaQuantidadeSubstancia
} from './ListaQuantidadeSubstancia/ListaQuantidadeSubstancia'
import {
  AcoesTransferenciaDeLista
} from './AcoesTransferenciaDeLista/AcoesTransferenciaDeLista'
import {
  Elementos
} from './Elementos/Elementos'
import {
  FormFiltroElementoModal
} from './FormFiltroElemento/FormFiltroElemento'
import {
  FormSubstanciaModal
} from './FormSubstancia/FormSubstancia'
import {
  SelectList as SubstanciaSelectList
} from '../../Substancia/SelectList/SelectList'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
      overflow: 'auto'
    },
    title: {
      fontSize: 14
    },
    pos: {
      marginBottom: 12
    },
    button: {
      margin: theme.spacing(0.5, 0)
    }
  })
)

/**
 * Area de inputs para ComposicaoSubstancia
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para ComposicaoSubstancia
 */

export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  // const { getState } = useContext(CrudContext)

  // Selector
  const ComposicaoSubstanciasFieldsetComponent = useMemo(() => {
    return (<ComposicaoSubstanciasFieldset />)
  }, [])
  return (

    <Grid container>
      <Grid item>
        <span>Campos</span>
      </Grid>
      <Grid item>
        { ComposicaoSubstanciasFieldsetComponent }
      </Grid>
    </Grid>
  )
})

export const ComposicaoSubstanciasFieldset = (props: any) => {
  const { getState } = useContext(CrudContext)
  const classes = useStyles()

  // Selector
  const { item } = useSelector((state: any) => {
    const { item } = getState(state)
    return { item }
  })
  const itemSubstancias = useMemo(() => item.substancias || [], [item.substancias])
  const [filtroElementos, setFiltroElementos] = useState([] as Array<any>)
  //   (ListaQuantidadeSubstancia)
  //     item
  const [quantidadeSubstancias, setQuantidadeSubstancias] = useState(itemSubstancias)
  //     checked, setChecked
  const [quantidadeSubstanciasChecked, setQuantidadeSubstanciasChecked] = useState([] as Array<any>)
  //   (SubstanciaSelectList)
  //     checked, setChecked
  const [substanciasChecked, setSubstanciasChecked] = useState([] as Array<any>)
  //   (FormSubstanciaModal)
  //     isOpen, setOpen
  const [isSubstanciaModalOpen, setSubstanciaModalOpen] = useState(false)
  //     value, setValue
  const [substanciaModalValue, setSubstanciaModalValue] = useState(null)
  //   (FormFiltroElementoModal)
  //     isOpen, setOpen
  const [isFiltroElementoModalOpen, setFiltroElementoModalOpen] = useState(false)
  //     value, setValue
  const [filtroElementoModalValue, setFiltroElementoModalValue] = useState(null)

  // Memo
  //   (SubstanciaSelectList)
  //     selectedSubstancias
  const selectedSubstancias = useMemo(() => quantidadeSubstancias.map(
    (quantidadeSubstancia: any) => quantidadeSubstancia.substancia || {}),
  [quantidadeSubstancias])
  //   (Elementos)
  //     elementos
  const elementos = useMemo(() => {
    // Acumula elementos das substancias
    const result: any[] = []
    quantidadeSubstancias.forEach((quantidadeSubstancia: { quantidade: number; substancia: any }) => {
      const { substancia } = quantidadeSubstancia
      substancia.elementos.forEach((quantidadeElemento: { quantidade?: any; elemento?: any }) => {
        const { elemento } = quantidadeElemento

        let findElementoIndex = result.length
        const findElemento = result.find((elem: { elemento: { _id: any } }, index: any) => {
          if (elem.elemento._id === elemento._id) {
            findElementoIndex = index
            return true
          }
          return false
        })

        const quantidade = (
          (
            (
              quantidadeElemento.quantidade *
              quantidadeSubstancia.quantidade
            ) / 100
          ) + (
            findElemento ? findElemento.quantidade : 0
          )
        )
        result[findElementoIndex] = { quantidade, elemento }
      })
    })

    // Carrega os filtros para os elementos
    filtroElementos.forEach((filtroElemento) => {
      const { elemento } = filtroElemento
      let findFiltroElementoIndex = result.length
      result.find((elem: { elemento: { _id: any } }, index: any) => {
        if (elem.elemento._id === elemento._id) {
          findFiltroElementoIndex = index
          return true
        }
        return false
      })
      // filtroElemento
      result[findFiltroElementoIndex] = result[findFiltroElementoIndex] || {
        elemento,
        quantidade: 0
      }
      result[findFiltroElementoIndex].filtro = filtroElemento.quantidade
    })
    return result
  }, [
    filtroElementos,
    quantidadeSubstancias
  ])
  // Callback
  //   (FiltroElementos)
  //     editar
  const editarFiltroElemento = useCallback((value: any) => {
    // Abrir modal com formulario de filtro de elementos com o elemento selecionado
    setFiltroElementoModalValue(value)
    setFiltroElementoModalOpen(true)
  }, [setFiltroElementoModalValue, setFiltroElementoModalOpen])
  //     novo
  const novoFiltroElemento = useCallback(() => {
    // Abrir modal com formulario de filtro de elementos
    setFiltroElementoModalValue(null)
    setFiltroElementoModalOpen(true)
  }, [setFiltroElementoModalValue, setFiltroElementoModalOpen])
  //     remover
  const removerFiltroElmento = useCallback((value: any) => {
    // Remove o elemento do filtro
    setFiltroElementos(
      filtroElementos.filter((val: any) => value.elemento._id !== val.elemento._id)
    )
  }, [filtroElementos, setFiltroElementos])
  //   (AcoesTransferenciaDeLista)
  const novaQuantidadeSubstancia = useCallback((values: any) => {
    // Abrir modal com formulario de filtro de elementos
    const formValue = values.map(
      (substancia: any) => {
        return { substancia, quantidade: 0 }
      }
    )
    setSubstanciaModalValue(formValue)
    setSubstanciaModalOpen(true)
  }, [setSubstanciaModalValue, setSubstanciaModalOpen])
  //     remover
  const removerQuantidadeSubstancias = useCallback(() => {
    setQuantidadeSubstancias([])
    setQuantidadeSubstanciasChecked([])
    // Remove
  }, [setQuantidadeSubstancias, setQuantidadeSubstanciasChecked])
  //     adcionarSelecionados
  const adcionarSubstanciasSelecionadas = useCallback(() => {
    novaQuantidadeSubstancia(substanciasChecked)
  }, [novaQuantidadeSubstancia, substanciasChecked])
  //     removerSelecionados
  const removerQuantidadeSubstanciasSelecionadas = useCallback(() => {
    setQuantidadeSubstancias(
      quantidadeSubstancias.filter((val: any) => {
        return !quantidadeSubstanciasChecked.find((checked: any) => {
          return (checked.substancia._id === val.substancia._id)
        })
      })
    )
    setQuantidadeSubstanciasChecked([])
  }, [setQuantidadeSubstancias, quantidadeSubstancias, quantidadeSubstanciasChecked])
  //   (SubstanciaSelectList)
  //     exclude
  const excludeSubstanciaSelectList = useCallback((value: any) => {
    return Boolean(selectedSubstancias.find(
      (substancia: any) => substancia._id === value._id)
    )
  }, [selectedSubstancias])
  //   (FormSubstanciaModal)
  //     salvarSubstancia
  const salvarSubstancia = useCallback((value: any) => {
    setSubstanciasChecked([])
    setQuantidadeSubstancias([
      ...quantidadeSubstancias,
      ...value
    ])
  }, [quantidadeSubstancias, setQuantidadeSubstancias])
  //   (FormFiltroElementoModal)
  //     salvarFiltroElemento
  const salvarFiltroElemento = useCallback((value: any) => {
    setFiltroElementos([
      ...filtroElementos.filter((item: any) =>
        item.elemento._id !== value.elemento._id),
      value
    ])
  }, [filtroElementos, setFiltroElementos])

  return (<React.Fragment>
    <Grid
      container
      className={classes.root}>

      <Grid item xs={12}>

        <Paper className={classes.paper}>
          <FiltroElementos
            filtro={filtroElementos}
            editar={editarFiltroElemento}
            novo={novoFiltroElemento}
            remover={removerFiltroElmento}
          />
        </Paper>

      </Grid>

      <Grid item xs={12}>

        <Paper className={classes.paper}>

          <Grid
            container
            spacing={2}
            justify="center"
            alignItems="center">

            <Grid item>
              <ListaQuantidadeSubstancia
                items={quantidadeSubstancias}
                checked={quantidadeSubstanciasChecked}
                setChecked={(event: any) => setQuantidadeSubstanciasChecked(event)}
              />
            </Grid>

            <Grid item>
              <AcoesTransferenciaDeLista
                moverTudoDireita={
                  (event: any) => removerQuantidadeSubstancias()
                }
                moverSelecionadoDireita={
                  (event: any) => removerQuantidadeSubstanciasSelecionadas()
                }
                moverSelectionadoEsquerda={
                  (event: any) => adcionarSubstanciasSelecionadas()
                }
                itensEsquerda={
                  quantidadeSubstancias
                }
                itensSelecionadosDireita={
                  substanciasChecked
                }
                itensSelecionadosEsquerda={
                  quantidadeSubstanciasChecked
                }
              />
            </Grid>

            <Grid item>
              <SubstanciaSelectList
                checked={substanciasChecked}
                exclude={excludeSubstanciaSelectList}
                setChecked={(event: any) => setSubstanciasChecked(event)}
                filtroElementos={filtroElementos}
              />
            </Grid>

          </Grid>

        </Paper>

      </Grid>

      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <Elementos
            elementos={elementos}
          />
        </Paper>
      </Grid>
    </Grid>

    <FormFiltroElementoModal
      salvarFiltroElemento={salvarFiltroElemento}
      isOpen={isFiltroElementoModalOpen}
      setOpen={setFiltroElementoModalOpen}
      value={filtroElementoModalValue}
      setValue={setFiltroElementoModalValue}
    />
    <FormSubstanciaModal
      salvarSubstancia={salvarSubstancia}
      isOpen={isSubstanciaModalOpen}
      setOpen={setSubstanciaModalOpen}
      value={substanciaModalValue}
      setValue={setSubstanciaModalValue}
    />
  </React.Fragment>)
}
