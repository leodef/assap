/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { FiltroElementos } from './FiltroElementos'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<FiltroElementos />, div)
  ReactDOM.unmountComponentAtNode(div)
})
