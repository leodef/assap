/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { FormFiltroElemento } from './FormFiltroElemento'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<FormFiltroElemento />, div)
  ReactDOM.unmountComponentAtNode(div)
})
