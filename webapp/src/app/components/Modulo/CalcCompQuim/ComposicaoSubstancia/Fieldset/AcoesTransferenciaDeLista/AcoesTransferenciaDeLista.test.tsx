/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { AcoesTransferenciaDeLista } from './AcoesTransferenciaDeLista'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<AcoesTransferenciaDeLista />, div)
  ReactDOM.unmountComponentAtNode(div)
})
