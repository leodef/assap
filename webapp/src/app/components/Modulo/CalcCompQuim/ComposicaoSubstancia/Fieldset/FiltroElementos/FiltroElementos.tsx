import React,
{
  useCallback
} from 'react'
import {
  IconButton
} from '@material-ui/core'
import {
  Add as AddIcon
} from '@material-ui/icons'
import { ChipElemento } from '../ChipElemento/ChipElemento'
import './FiltroElementos.scss'

/**
 * Componente FiltroElementos
 * @param {any} props Propriedades
 * @return {React.Component} Componente FiltroElementos
 */
export const FiltroElementos = (props: any) => {
  // Lista de elementos para filtro
  // editarFiltroElemento - clicar no Chip do elemento
  // novoFiltroElemento - clicar no botão novo
  // removerFiltroElmento - clicar no icone x do Chip do elemento
  // label - elemento.titulo quantidade
  let {
    filtro,
    editar,
    remover,
    novo
  } = props
  const onClick = useCallback((values) => {
    editar(values)
  }, [editar])
  const onDelete = useCallback((values) => {
    remover(values)
  }, [remover])
  const onButtonClick = useCallback(() => {
    novo()
  }, [novo])
  filtro = filtro || []
  return (
    <React.Fragment>
      {filtro.map((elemento: any, index: any) => (
        <ChipElemento
          {...props}
          quantidadeElemento={elemento}
          onClick={onClick}
          onDelete={onDelete}
          key={index}/>)
      )}
      <IconButton color="inherit" onClick={onButtonClick} >
        <AddIcon/>
      </IconButton>
    </React.Fragment>)
}
