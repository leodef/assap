import React, { useCallback } from 'react'
import {
  SelectList as SharedSelectList
} from '../../../../../Shared/Crud/SelectList/SelectList'
// eslint-disable-next-line no-unused-vars
import './ListaQuantidadeSubstancia.scss'

/**
 * Componente ListaQuantidadeSubstancia
 * @param {any} props Propriedades
 * @return {React.Component} Componente ListaQuantidadeSubstancia
 */
export const ListaQuantidadeSubstancia = (props: any) => {
  const {
    items,
    checked,
    exclude,
    setChecked
  } = props

  const getLabel = useCallback(
    (option: any) => {
      if (!option) { return '' }
      const { substancia, quantidade } = option
      return `${
        substancia ? substancia.titulo : ''}
        -
        ${quantidade}`
    }, [])

  const getSecondary = useCallback(
    (option: any) => {
      const { quantidade, substancia } = option
      const { elementos } = (substancia || {})
      return (elementos || [])
        .filter((val: any) => val && val.elemento)
        .reduce(
          (prev: string, current: any) => {
            const result = prev.concat(
              current.elemento.titulo,
              '-',
              String((current.quantidade * quantidade) / 100),
              ', ')
            return result
          },
          '')
    },
    [])

  const getId = useCallback(
    (val: any) => val && val.substancia ? val.substancia._id : null,
    [])

  const compare = useCallback(
    (val: any, other: any) =>
      val && val.substancia && getId(val) === getId(other),
    [getId])

  const isChecked = useCallback(
    (value, index, checked) => Boolean(checked.find(
      (item: any) => compare(item, value)
    )),
    [compare])
  /* checked, items, exclude, isChecked, getLabel, setChecked */
  return (<SharedSelectList
    compare={compare}
    checked={checked}
    items={items}
    exclude={exclude}
    isChecked={isChecked}
    getLabel={getLabel}
    setChecked={setChecked}
    getSecondary={getSecondary}
  />)
}
