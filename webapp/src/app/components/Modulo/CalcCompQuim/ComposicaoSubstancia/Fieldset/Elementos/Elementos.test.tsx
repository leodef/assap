/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Elementos } from './Elementos'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Elementos />, div)
  ReactDOM.unmountComponentAtNode(div)
})
