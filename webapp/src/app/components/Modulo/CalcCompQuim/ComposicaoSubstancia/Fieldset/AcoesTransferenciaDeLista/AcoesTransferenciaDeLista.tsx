import React,
{
  useMemo
} from 'react'
import {
  makeStyles,
  Button,
  createStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  Grid
} from '@material-ui/core'
import './AcoesTransferenciaDeLista.scss'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      margin: theme.spacing(0.5, 0)
    }
  })
)

/**
 * Componente AcoesTransferenciaDeLista
 * @param {any} props Propriedades
 * @return {React.Component} Componente AcoesTransferenciaDeLista
 */
export const AcoesTransferenciaDeLista = (props: any) => {
  const classes = useStyles()
  const {
    // Metodos
    moverTudoEsquerda,
    moverTudoDireita,
    moverSelecionadoDireita,
    moverSelectionadoEsquerda,
    // Parametros
    itensDireita,
    itensEsquerda,
    itensSelecionadosDireita,
    itensSelecionadosEsquerda
  } = props

  /*
  Direita remove
  Esquerda adiciona
    //Metodos
    moverTudoEsquerda={}
    moverTudoDireita={}
    moverSelecionadoDireita={}
    moverSelectionadoEsquerda={}
    // Parametros
    itensDireita={}
    itensEsquerda={}
    itensSelecionadosDireita={}
    itensSelecionadosEsquerda={}
  */
  const moverTudoDireitaButton = useMemo(() =>
    (moverTudoDireita && itensEsquerda) ? (<Button
      variant="outlined"
      size="small"
      className={classes.button}
      onClick={(event:any) => moverTudoDireita(event)}
      disabled={itensEsquerda.length === 0}
      aria-label="Mover tudo para direita"
    >≫</Button>) : null
  , [moverTudoDireita, itensEsquerda, classes.button])

  const moverSelecionadoDireitaButton = useMemo(() =>
    (moverSelecionadoDireita && itensSelecionadosEsquerda) ? (<Button
      variant="outlined"
      size="small"
      className={classes.button}
      onClick={(event:any) => moverSelecionadoDireita(event)}
      disabled={itensSelecionadosEsquerda.length === 0}
      aria-label="Mover selecionados para direita"
    >&gt;</Button>) : null
  , [moverSelecionadoDireita, itensSelecionadosEsquerda, classes.button])

  const moverSelectionadoEsquerdaButton = useMemo(() =>
    (moverSelectionadoEsquerda && itensSelecionadosDireita) ? (<Button
      variant="outlined"
      size="small"
      className={classes.button}
      onClick={(event:any) => moverSelectionadoEsquerda(event)}
      disabled={itensSelecionadosDireita.length === 0}
      aria-label="Mover selecionados para esquerda"
    >&lt;</Button>) : null
  , [moverSelectionadoEsquerda, itensSelecionadosDireita, classes.button])

  const moverTudoEsquerdaButton = useMemo(() =>
    (moverTudoEsquerda && itensDireita) ? (<Button
      variant="outlined"
      size="small"
      className={classes.button}
      onClick={(event:any) => moverTudoEsquerda(event)}
      disabled={itensDireita.length === 0}
      aria-label="Mover tudo para esquerda"
    >≪</Button>) : null
  , [moverTudoEsquerda, itensDireita, classes.button])

  return (<Grid container direction="column" alignItems="center">
    {moverTudoDireitaButton}
    {moverSelecionadoDireitaButton}
    {moverSelectionadoEsquerdaButton}
    {moverTudoEsquerdaButton}
  </Grid>)
}
