import React,
{
  useCallback
} from 'react'
import {
  Grid,
  Modal,
  makeStyles,
  Card,
  CardContent,
  Typography,
  CardActions,
  Button,
  createStyles,
  // eslint-disable-next-line no-unused-vars
  Theme
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import {
  TextField
} from '../../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  Send as SendIcon
} from '@material-ui/icons'
import {
  Autocomplete as ElementoAutocomplete
} from '../../../Elemento/Autocomplete/Autocomplete'
// eslint-disable-next-line no-unused-vars
import {
  withFormik,
  // eslint-disable-next-line no-unused-vars
  FormikHelpers,
  useField
} from 'formik'
import _ from 'lodash'
import './FormFiltroElemento.scss'

/**
 * Componente FormFiltroElementoModal
 * @param {any} props Propriedades
 * @return {React.Component} Componente FormFiltroElementoModal
 */
export const FormFiltroElementoModal = (props: any) => {
  const classes = useStyles()
  const { salvarFiltroElemento, isOpen, setOpen, setValue } = props
  const onClose = useCallback(() => {
    setOpen(false)
    setValue(null)
  }, [setOpen, setValue])
  const save = useCallback((values: any) => {
    salvarFiltroElemento(values)
    setOpen(false)
    setValue(null)
  }, [salvarFiltroElemento, setOpen, setValue])
  return (
    <Modal
      open={isOpen}
      onClose={onClose}
      aria-labelledby="Substancia"
      aria-describedby="Substancia">
      <div className={classes.paper}>
        <FormFiltroElemento
          {...props}
          save={save}/>
      </div>
    </Modal>)
}

/**
 * Componente FormFiltroElementoModal
 * @param {any} props Propriedades
 * @return {React.Component} Componente FormFiltroElementoModal
 */
export const FormFiltroElemento = (props: any) => {
  // salvarFiltroElemento
  const initialValues = {
    elemento: '',
    quantidade: ''
  }
  const { save, value } = props
  const WithFormik = withFormik({
    mapPropsToValues: () => _.defaults(value, initialValues),
    handleSubmit: (values: any, formikHelpers: FormikHelpers<any>) => {
      const { setSubmitting } = formikHelpers
      setSubmitting(false)
      save(values)
    }
  })(FormFiltroElementoBody)
  return (<WithFormik
    {...props}
    name={null} />)
}

/**
 * Componente FormFiltroElementoModal
 * @param {any} props Propriedades
 * @return {React.Component} Componente FormFiltroElementoModal
 */
export const FormFiltroElementoBody = (props: any) => {
  const classes = useStyles()
  return (<form onSubmit={props.handleSubmit}>
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          Elemento para filtro
        </Typography>
        <FormFiltroElementoFieldset
          {...props} />
      </CardContent>
      <CardActions>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          startIcon={<SendIcon />}>
            Salvar
        </Button>
      </CardActions>
    </Card>
  </form>)
}

/**
 * Componente FormFiltroElementoModal
 * @param {any} props Propriedades
 * @return {React.Component} Componente FormFiltroElementoModal
 */
export const FormFiltroElementoFieldset = (props: any) => {
  const { values, disabled } = props
  const { elemento } = (values || {})
  const [, , elementoHelpers] = useField('elemento')
  const setElemento = useCallback(
    (elem?: any) => {
      // const elementoId = elemento ? elemento._id : null
      elementoHelpers.setValue(elem)
    }, // eslint-disable-next-line
    []
  )

  return (
    <Grid container spacing={2} >
      <Grid item xs={12} sm={6}>
        <ElementoAutocomplete
          onChange={(selected: any) => setElemento(selected)}
          value={elemento}/>
        <TextField
          name="elemento"
          type="hidden"
          disabled={disabled}
        />
      </Grid>

      <Grid item xs={12}>

        <TextField
          name="quantidade"
          type="number"
          label="Quantidade"
          disabled={disabled}
        />
      </Grid>

    </Grid>)
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 300, // 275
      minHeight: 300
    },
    title: {
      fontSize: 14
    },
    pos: {
      marginBottom: 12
    },
    paper: {
      position: 'absolute',
      minWidth: 200,
      minHeight: 200,
      // boxShadow: theme.shadows[5],
      // padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },
    button: {
      margin: theme.spacing(0.5, 0)
    }
  })
)
