/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ListaQuantidadeSubstancia } from './ListaQuantidadeSubstancia'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ListaQuantidadeSubstancia />, div)
  ReactDOM.unmountComponentAtNode(div)
})
