import React from 'react'
import {
  Typography
} from '@material-ui/core'
import './Resume.scss'

/**
 * Visualização de item do tipo ComposicaoSubstancia
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo ComposicaoSubstancia
 */
export const Resume = (props: any) => {
  /*
    ComposiçãoSubstancia
      titulo: string
      desc: string
      valor: number
      substancias: Array<QuantidadeSubstancia>
  */
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.titulo}
    </Typography>
    <Typography variant="body2" component="p">
      {item.desc}
    </Typography>
    <Typography variant="body2" component="p">
      {item.valor}
    </Typography>

    {/* CRUD - QuantidadeSubstancia */}

  </React.Fragment>)
}
