import React, { useCallback, useMemo } from 'react'
import { crudType as types } from '../../../../../types/API/CalcCompQuim/ComposicaoSubstancia'
import { AutocompleteContext } from '../../../../../contexts/AutocompleteContext'
import { Autocomplete as SharedAutocomplete } from '../../../../Shared/Autocomplete/Autocomplete'
import './Autocomplete.scss'

/**
 * Visualização de item do tipo ComposicaoSubstancia
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo ComposicaoSubstancia
 */
export const Autocomplete = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.calcCompQuim.composicaoSubstancia.options, [])
  const { fields, label } = useMemo(() => {
    return { fields: ['titulo'], label: (props.label || 'Composição de substancia') }
  }, [props.label])
  const getOptionLabel = (option: any) => option ? option.titulo : ''
  const getOptionId = (val: any) => val ? val._id : null
  const autocompleteContextValue = { getState, types, getOptionLabel, getOptionId, fields, label }

  return (<AutocompleteContext.Provider value={autocompleteContextValue}>
    <SharedAutocomplete
      {...props} />
  </AutocompleteContext.Provider>)
}
