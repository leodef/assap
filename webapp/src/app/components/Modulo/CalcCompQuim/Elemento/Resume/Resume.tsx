import React from 'react'
import {
  Typography
} from '@material-ui/core'
import './Resume.scss'

/**
 * Visualização de item do tipo Elemento
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Elemento
 */
export const Resume = (props: any) => {
  /*
    Elemento:
      titulo: string
      desc: string
  */
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.titulo}
    </Typography>
    <Typography variant="body2" component="p">
      {item.desc}
    </Typography>
  </React.Fragment>)
}
