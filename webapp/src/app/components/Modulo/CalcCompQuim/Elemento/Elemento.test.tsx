/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Elemento } from './Elemento'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Elemento />, div)
  ReactDOM.unmountComponentAtNode(div)
})
