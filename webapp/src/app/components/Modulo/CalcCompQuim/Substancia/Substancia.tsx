import
React, {
  useMemo, useCallback
} from 'react'
import {
  Assignment as AssignmentIcon
} from '@material-ui/icons'
import {
  CollectionTypeEnum
} from '../../../../types/Crud'
import { Crud } from '../../../Shared/Crud/Crud'
import { TablePropsField } from '../../../Shared/Collection/Table/Table'
import { Resume } from './Resume/Resume'
import { Fieldset } from './Fieldset/Fieldset'
import {
  SubstanciaSchema,
  initialValues
} from '../../../../types/API/CalcCompQuim/Substancia'
import './Substancia.scss'

export const Substancia = (props: any) => {
  const { collection, form, show, remove } = props
  const { type } = (collection || {})
  const fields = useMemo(() => [
    new TablePropsField('titulo', 'Título'),
    new TablePropsField('desc', 'Descrição'),
    new TablePropsField({
      name: 'elementos',
      label: 'Elementos',
      callback: (item: any) => {
        const { elementos } = (item || {})
        return (elementos || [])
          .filter((val: any) => val && val.elemento)
          .reduce(
            (prev: string, current: any) => {
              const result = prev.concat(
                current.elemento.titulo,
                '-',
                current.quantidade,
                ', ')
              return result
            },
            '')
      }
    })
  ], [])

  const ShowBody = useCallback((props: any) => {
    return (<Resume
      {...props} />)
  }, [])
  const FormBody = useCallback((props: any) => {
    return (<Fieldset
      {...props} />)
  }, [])

  const tableConfig = useMemo(() => {
    return {
      label: 'Substancia',
      align: 'center',
      actionsLabel: 'Ações',
      fields
    }
  }, [fields])
  const listConfig = useMemo(() => {
    return {
      dense: true,
      subheader: 'Substancia',
      fields,
      getListItem: (value: any, index: number) => {
        return {
          primary: (value.titulo ? value.titulo.toString() : ''),
          secondary: null,
          avatar: (value.titulo ? value.titulo.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
        }
      }
    }
  }, [fields])
  const config = (type === CollectionTypeEnum.LIST) ? listConfig : tableConfig
  const params = useMemo(() => {
    const params = {
      collection: {
        type,
        config,
        // resolve: 'FRONT' // 'BACK'
        ...(collection || {})
      },
      show: {
        title: 'Substancia',
        // eslint-disable-next-line react/display-name
        body: ShowBody,
        ...(show || {})
      },
      form: {
        title: 'Substancia',
        // eslint-disable-next-line react/display-name
        body: FormBody,
        initialValues,
        schema: SubstanciaSchema,
        ...(form || [])
      },
      remove: {
        title: 'Deseja realmente remover a substancia ?',
        ...(remove || {})
      }
    }
    return params
  }, [
    collection,
    config,
    form,
    remove,
    show,
    type,
    ShowBody,
    FormBody
  ])
  return <Crud {...params} />
}
