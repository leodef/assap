import
React,
{
  useCallback,
  useEffect,
  useMemo
} from 'react'
import {
  crudType as types
} from '../../../../../types/API/CalcCompQuim/Substancia'
import {
  SelectList as SharedSelectList
} from '../../../../Shared/Crud/SelectList/SelectList'
// eslint-disable-next-line no-unused-vars
import { useDispatch, useSelector } from 'react-redux'
import './SelectList.scss'

/**
 * Area de inputs para Substancia
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Substancia
 */
export const SelectList = (props: any) => {
  const {
    checked,
    exclude,
    setChecked,
    filtroElementos
  } = props
  const getState = useCallback(
    (state: any) => state.modulo.calcCompQuim.substancia.crud, [])

  const state = useSelector(
    state => getState(state)
  )
  const items = useMemo(() => state.items, [state])

  const getLabel = useCallback(
    (option: any) => option ? option.titulo : '',
    [])
  const getSecondary = useCallback(
    (option: any) => {
      const { elementos } = option
      return (elementos || [])
        .filter((val: any) => val && val.elemento)
        .reduce(
          (prev: string, current: any) => {
            const result = prev.concat(
              current.elemento.titulo,
              '-',
              current.quantidade,
              ', ')
            return result
          },
          '')
    },
    [])

  const getId = useCallback(
    (val: any) => val ? val._id : null,
    [])

  const compare = useCallback(
    (val: any, other: any) =>
      val && getId(val) === getId(other),
    [getId])

  const isChecked = useCallback(
    (value, index, checked) => checked.find(
      (item: any) => compare(item, value)
    ),
    [compare])

  // dispatch
  const dispatch = useDispatch()

  // Metodo de consulta
  const fetch = useCallback(
    (params?: {filter?: any}) =>
      dispatch({
        type: types.FETCH_ITEM,
        payload: params
      }
      ), [dispatch])

  // Executar metodo de consulta
  useEffect(() => {
    fetch({
      /* filter: {
        elementos: filtroElementos
      } */
    })
  }, [fetch, filtroElementos])

  /* checked, items, exclude, isChecked, getLabel, setChecked */
  return (<SharedSelectList
    checked={checked}
    items={items}
    exclude={exclude}
    isChecked={isChecked}
    getLabel={getLabel}
    getSecondary={getSecondary}
    setChecked={setChecked}
  />)
}
