import React,
{
  useCallback,
  useEffect,
  useMemo,
  useState
} from 'react'
import {
  useDispatch,
  useSelector
} from 'react-redux'
// eslint-disable-next-line no-unused-vars
import {
  makeStyles,
  Theme,
  createStyles
} from '@material-ui/core/styles'
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Badge,
  MenuItem,
  Menu,
  FormControl,
  Select
} from '@material-ui/core'
import {
  Menu as MenuIcon,
  AccountCircle,
  Notifications as NotificationsIcon,
  MoreVert as MoreIcon,
  Brightness7 as LightIcon,
  Brightness4 as DarkIcon
} from '@material-ui/icons'
import { AuthType } from '../../../types/Auth'
import { SidebarType } from '../../../types/Sidebar'
import { EmpresaUsuarioType, getState } from '../../../types/EmpresaUsuario'
import { ThemeType, ThemeValue } from '../../../types/Theme'
// import { EmpresaUsuarioType } from '../../../types/EmpresaUsuario'

/**
 * Barra de navegação superior para usuários logados
 * @param {any} props Propriedades
 * @return {React.Component} Componente com barra de navegação superior para usuários logados
 */
export const Navbar = function (props: any) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const toggleSidebar = () => dispatch({ type: SidebarType.TOGGLE_SIDEBAR })
  const logout = () => dispatch({ type: AuthType.LOGOUT })
  const tema = useSelector((state: any) => state.theme.value)
  const onClickThemeButton = useCallback( (event: any) => dispatch({
      type: ThemeType.TOGGLE 
    }),
    [dispatch]
  )

  
  // empresaSelectInput
  const [dadosEmpresaValue, setDadosEmpresaValue] = useState('' as string);
  const dadosEmpresa = useSelector(state => getState(state))
  
  const dadosEmpresaTitulo = useMemo(
    () => dadosEmpresa.item ? dadosEmpresa.item.nomeFantasia : null,
    [dadosEmpresa])
  const dadosEmpresaDisabled = useMemo(
    () => dadosEmpresa.fix,
    [dadosEmpresa])
  const setEmpresaUsuario = useCallback((_id: any) =>
    dispatch({
    type: EmpresaUsuarioType.SET,
    payload: dadosEmpresa.options.find((option: any) => option && option._id === _id)
    }), [dispatch, dadosEmpresa])

  const fetchEmpresaUsuario = useCallback(() => dispatch({
      type: EmpresaUsuarioType.FETCH
    }), [dispatch])

  const empresaSelectInputOptions = useMemo(() =>
    (dadosEmpresa ? dadosEmpresa.options || '' : [])
      .filter((val: any) => val)
      .map( (empresa: any) => ({label: empresa.nomeFantasia, value: empresa._id}) ),
    [dadosEmpresa])


  useEffect(() => {
    if(!dadosEmpresaDisabled) {
      fetchEmpresaUsuario()
    }
  }, [dadosEmpresaDisabled, fetchEmpresaUsuario])

  const empresaSelectInputDisabled = useMemo(() => false,
    [])
  const empresaSelectInputOnChange = useCallback((event: any) =>{
    const value = event.target.value
    if(value && value !== ''){
      setEmpresaUsuario(value)
    } else {
      setEmpresaUsuario(null)
    }
    setDadosEmpresaValue(value)
  },
    [setEmpresaUsuario])

  // Menu profile
  const [profileAnchorEl, setProfileAnchorEl] = React.useState<null | HTMLElement>(null)
  const isProfileMenuOpen = Boolean(profileAnchorEl)
  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setProfileAnchorEl(event.currentTarget)
  }
  const handleProfileMenuClose = () => {
    setProfileAnchorEl(null)
    handleMobileMenuClose()
  }
  const menuId = 'primary-search-account-menu'
  const renderMenu = (
    <Menu
      anchorEl={profileAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isProfileMenuOpen}
      onClose={handleProfileMenuClose}
    >
      <MenuItem onClick={handleProfileMenuClose}>Profile</MenuItem>
      <MenuItem onClick={logout}>Logout</MenuItem>
    </Menu>
  )

  // Menu vertical p tela pequena
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState<null | HTMLElement>(null)
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl)
  const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget)
  }
  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null)
  }
  const mobileMenuId = 'primary-search-account-menu-mobile'
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton aria-label="show 11 new notifications" color="inherit">
          <Badge badgeContent={11} color="secondary">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  )

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
          { /* Icone menu lateral */}
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
            onClick={() => toggleSidebar()}>
            <MenuIcon />
          </IconButton>
          { /* Título aplicação */}
          <Typography variant="h6" noWrap className={classes.title}>
            Planning Route
          </Typography>
          { /* Area de expansão */}
          <div className={classes.grow} />
          { /* Icones para telas grandes */}
          <div className={classes.sectionDesktop}>
            { /* Seleciona a empresa a ser alterada */ }
            { dadosEmpresaDisabled
            ? dadosEmpresaTitulo ?(<Typography variant="h6" noWrap className={classes.title}>
            <em>{dadosEmpresaTitulo}</em>
          </Typography>) : null
            : (<FormControl className={classes.formControl}>
            { /*<InputLabel
                id="empresaSelectInput">
                  Empresa
              </InputLabel>*/ }
              <Select
                disabled={empresaSelectInputDisabled}
                labelId="empresaSelectInput"
                onChange={empresaSelectInputOnChange}
                value={dadosEmpresaValue}
                defaultValue=""
                color="primary"
                disableUnderline>
                <MenuItem value="" key={0}>
                <Typography
                        variant="h6"
                        noWrap
                        color="textPrimary">
                        <em>Sem empresa</em>
                      </Typography>
                  
                </MenuItem>
                {empresaSelectInputOptions.map(
                  (option: { value: string; label: string }, index: number) => (
                    <MenuItem value={option.value} key={index+1}>
                      <Typography
                        variant="h6"
                        noWrap
                        color="textPrimary">
                        {option.label}
                      </Typography>
                    </MenuItem>
                  )
                )}
              </Select>
            </FormControl>)
                  }
            { /* Icone tema */}
            <IconButton aria-label="Tema" color="inherit" onClick={onClickThemeButton}>
                {
                  tema === ThemeValue.dark
                  ? (<LightIcon />)
                  : (<DarkIcon />)
                }
            </IconButton>
            { /* Icone opções de conta */}
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </div>
          { /* Icones para telas pequenas */}
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      { /* Menu usados na barra */}
      {renderMobileMenu}
      {renderMenu}
    </div>
  )
}


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 250
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block'
      }
    },
    selectInputLabel: {
      color: theme.palette.common.white
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex'
      }
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none'
      }
    }
  })
)

export default Navbar
