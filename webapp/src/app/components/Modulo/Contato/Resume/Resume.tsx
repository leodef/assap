import React, { useMemo } from "react";
import { Typography } from "@material-ui/core";
import { ArrayResume as ArrayResumeContatoInfo } from "../../ContatoInfo/ArrayResume/ArrayResume";
import "./Resume.scss";

/**
 * Visualização de item do tipo Filial
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Filial
 */
export const Resume = (props: any) => {
  const { item, index } = useMemo(() => props, [props]);
  const infos = useMemo(() => (item ? item.infos : null), [item]);

  return (
    <React.Fragment>
      <Typography variant="body2" component="p">
        {index ? `#${index}` : null}
        {item.titulo}
      </Typography>

      <Typography variant="body2" component="p">
        {item.desc}
      </Typography>

      <ArrayResumeContatoInfo item={infos} />
    </React.Fragment>
  );
};
