import React, { useMemo } from "react";
import { Card, CardContent, Grid } from "@material-ui/core";
import {
  getFieldName,
  TextField,
  useStyles as fieldsUseStyles,
} from "../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField";
import {
  ArrayFieldset as ArrayFieldsetContatoInfo
} from "../../ContatoInfo/ArrayFieldset/ArrayFieldset";
import "./Fieldset.scss";

/**
 * Area de inputs para Contato
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Contato
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = "Fieldset";
  const fieldsClasses = fieldsUseStyles();
  const {
    disabled,
    item,
    modalHistory
  } = useMemo(
    () => props,
    [props]);
  const desc = useMemo(
    () => (item ? item.desc : null),
    [item]);
  const infos = useMemo(
    () => (item ? item.infos : []),
    [item]);
  const fields = useMemo(() => {
    return {
      titulo: {
        name: getFieldName("titulo", props),
        label: "Título",
      },
      desc: {
        name: getFieldName("desc", props),
        label: "Descrição",
      },
      infos: {
        name: getFieldName("infos", props),
        label: "Informações para contato",
      },
    };
  }, [props]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.titulo.name}
          type="text"
          label={fields.titulo.label}
          disabled={disabled}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          name={fields.desc.name}
          type="text"
          label={fields.desc.label}
          disabled={disabled}
          value={desc}
          multiline
          className={fieldsClasses.multiline}
        />
      </Grid>
      <Grid item xs={12}>
        <Card>
          <CardContent>
            <ArrayFieldsetContatoInfo
              modalHistory={modalHistory}
              name={fields.infos.name}
              label={fields.infos.label}
              disabled={disabled}
              item={infos}
            />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
});
