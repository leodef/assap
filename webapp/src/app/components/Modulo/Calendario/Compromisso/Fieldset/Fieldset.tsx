import React,
{
  useCallback,
  useMemo
} from "react";
import {
  useField
} from "formik";
import {
  FormControlLabel,
  Grid,
  Switch
} from "@material-ui/core";
// eslint-disable-next-line no-unused-vars
import {
  DatePicker,
  getFieldName,
  SelectInput,
  TextField,
} from "../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField";
import {
  TIPO_COMPROMISSO
} from "../../../../../enums/tipo-compromisso";
import {
  TituloFieldset
} from "../../../../Shared/Crud/Fieldset/TituloFieldset/TituloFieldset";
import {
  DescFieldset
} from "../../../../Shared/Crud/Fieldset/DescFieldset/DescFieldset";
import "./Fieldset.scss";

/**
 * Area de inputs para TipoContatoInfo
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para TipoContatoInfo
 */
export const Fieldset = (props: any) => {
  const {
    disabled,
    labels,
    fixed,
    placeholder,
    automated
  } = useMemo(() => props || {}, [props]);
  const fields = useMemo(() => {
    return {
      titulo: {
        name: getFieldName("titulo", props),
        label: labels && labels.titulo ? labels.titulo : "Título",
      },
      desc: {
        name: getFieldName("desc", props),
        label: labels && labels.desc ? labels.desc : "Descrição",
      },
      inicio: {
        name: getFieldName("inicio", props),
        label: labels && labels.inicio ? labels.inicio : "Começo",
      },
      fim: {
        name: getFieldName("fim", props),
        label: labels && labels.fim ? labels.fim : "Fim",
      },
      recursos: {
        name: getFieldName("recursos", props),
        label: labels && labels.recursos ? labels.recursos : "Recursos",
      },
      diaInteiro: {
        name: getFieldName("diaInteiro", props),
        label: labels && labels.diaInteiro ? labels.diaInteiro : "Dia inteiro",
      },
      tipo: {
        name: getFieldName("tipo", props),
        label: labels && labels.tipo ? labels.tipo : "Tipo evento",
      }
    };
  }, [props, labels]);

  // diaInteiro
  const [, diaInteiroMeta, diaInteiroHelpers] = useField(
    fields.diaInteiro.name
  );
  const diaInteiro = useMemo(() => diaInteiroMeta.value, [diaInteiroMeta]);
  const setDiaInteiro = useCallback(
    (value: any) => diaInteiroHelpers.setValue(value),
    [diaInteiroHelpers]
  );
  const handleDiaInteiroChange = useCallback(
    (event) => setDiaInteiro(event.target.checket),
    [setDiaInteiro]
  );
  const diaInteiroLabel = useMemo(
    () =>
      diaInteiro
        ? "Evento dura o dia inteiro"
        : "Marcar evento para o dia inteiro ?",
    [diaInteiro]
  );

  const tituloInput = useMemo(
    () =>
      fixed && fixed.titulo ? (
        <TextField
          type="hidden"
          name={fields.titulo.name}
          value={fixed.titulo}
        />
      ) : (
        <Grid item xs={12} sm={6}>
        <TituloFieldset
          {...props}
          hidden={fixed && fixed.titulo}
          placeholder={placeholder ? placeholder.titulo : null}
          automated={automated ? automated.titulo : null}
          fixed={fixed.titulo}
        />
      </Grid>
      ),
    [
      props,
      fixed,
      fields,
      automated,
      placeholder
    ]
  );

  const tipoInput = useMemo(
    () =>
      fixed && fixed.tipo ? (
        <TextField
          type="hidden"
          name={fields.tipo.name}
          value={fixed.tipo}
        />
      ) : (
        <Grid item xs={12} sm={4}>
          <SelectInput
            disabled={disabled}
            name={fields.tipo.name}
            label={fields.tipo.label}
            enumOptions={TIPO_COMPROMISSO}
            fullWidth
          />
        </Grid>
      ),
    [fixed, fields, disabled]
  );

  const inicioInput = useMemo(
    () =>
    fixed && (fixed.inicio || fixed.inicio === null) ? (
        <TextField
          type="hidden"
          name={fields.inicio.name}
          value={fixed.inicio}
        />
      ) : (
        <Grid item xs={12} sm={6}>
        <DatePicker
          name={fields.inicio.name}
          label={fields.inicio.label}
          disabled={disabled}
        />
      </Grid>
      ),
    [fixed, fields, disabled]
  );

  const fimInput = useMemo(
    () =>
    diaInteiro || (fixed && (fixed.fim || fixed.fim === null)) ? (
        <TextField
          type="hidden"
          name={fields.fim.name}
          value={fixed.fim}
        />
      ) : (
        <Grid item xs={12} sm={6}
        >
          <DatePicker
            name={fields.fim.name}
            label={fields.fim.label}
            disabled={disabled}
          />
        </Grid>
      ),
    [
      diaInteiro,
      fixed,
      fields,
      disabled
    ]
  );

  const diaInteiroInput = useMemo(
    () =>
      fixed && fixed.diaInteiro ? (
        <TextField
          type="hidden"
          name={fields.diaInteiro.name}
          value={fixed.diaInteiro}
        />
      ) : (
      <Grid item xs={4}>
        <FormControlLabel
          disabled={disabled}
          control={
            <Switch
              checked={diaInteiro}
              onChange={handleDiaInteiroChange}
              color="primary"
              disabled={disabled}
            />
          }
          label={diaInteiroLabel}
        />
      </Grid>
      ),
    [
      fixed,
      fields,
      disabled,
      diaInteiro,
      diaInteiroLabel,
      handleDiaInteiroChange
    ]
  );

  const descInput = useMemo(
    () =>
      fixed && fixed.desc ? (
        <TextField
          type="hidden"
          name={fields.desc.name}
          value={fixed.desc}
        />
      ) : (
        <Grid item xs={12}>
          <DescFieldset
            {...props}
            hidden={fixed && fixed.desc}
            fixed={fixed.desc}
            automated={automated ? automated.desc : null}
            placeholder={placeholder ? placeholder.desc : null} />
        </Grid>
      ),
    [
      props,
      fixed,
      fields,
      automated,
      placeholder
    ]
  );


  return (
    <Grid container spacing={2}>
      {/* titulo */}
      {tituloInput}

      {/* tipo */}
      {tipoInput}

      {/* inicio */}
      {inicioInput}

      {/* fim */}
      {fimInput}

      {/* diaInteiro */}
      {diaInteiroInput}

      {/* desc */}
      {descInput}
      
    </Grid>
  );
};

/*
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    hidden: {
      display: "none",
    },
    show: {
      display: "block",
    },
  })
);
*/