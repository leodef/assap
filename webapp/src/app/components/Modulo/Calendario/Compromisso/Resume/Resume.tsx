
import moment from "moment";
import React from "react";
import {
  Grid,
  Typography
} from "@material-ui/core";
import "./Resume.scss";

/**
 * Visualização de item do tipo Compromisso
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Compromisso
 */
export const Resume = (props: any) => {
  const { item } = props;
  return (<Grid
        container
        item
        spacing={2}>
      {item && item.titulo ?
        (<Grid item xs={12}>
          <Typography variant="h5" component="h2">
            {item.titulo }
          </Typography>
        </Grid>): null}
      {item && item.inicio ?
        (<Grid item xs={4}>
          <Typography variant="body2" component="p">
          <strong>Inicio:</strong> {moment(item.inicio).format('DD/MM/YYY hh:mm:ss')}
          </Typography>
        </Grid>): null}
      
        {item && item.fim ?
        (<Grid item xs={4}>
          <Typography variant="body2" component="p">
          <strong>Fim:</strong> {moment(item.fim).format('DD/MM/YYY hh:mm:ss')}
          </Typography>
        </Grid>): null}

        {item && item.tipo ?
        (<Grid item xs={4}>
          <Typography variant="body2" component="p">
          <strong>Tipo de evento:</strong> {item.tipo}
          </Typography>
        </Grid>): null}

        {item && item.diaInteiro ?
        (<Grid item xs={4}>
          <Typography variant="body2" component="p">
          Dia reservado ao evento
          </Typography>
        </Grid>): null}

        {item && item.desc ?
        (<Grid item xs={4}>
          <Typography variant="body2" component="p">
          <strong>Descrição:</strong> {item.desc}
          </Typography>
        </Grid>): null}
      </Grid>)
};