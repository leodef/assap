import { useTheme } from '@material-ui/core'
import React, { useEffect, useMemo, useRef } from 'react'
import GooglePlacesAutocomplete from 'react-google-places-autocomplete'
import './Autocomplete.scss'

/**
 * Area de inputs para Localização
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Localização
 */
export const Autocomplete = (props: any) => {
  const ref = useRef(null as any)
  const theme = useTheme();
  // const classes = useStyles()
  // const { disabled } = useMemo(() => props, [props])
  const {
    value,
    onChange,
    defaultInputValue
  } = useMemo(() => props, [props])
  useEffect(() => {
    if (ref.current) {
      ref.current.setAddressText(defaultInputValue)
    }
  }, [defaultInputValue])
  /**
   * When the user types an address in the search box
   * @param place
   */
  const selectProps = useMemo(() => ({
    value,
    onChange,
    placeholder: defaultInputValue,
    styles: {
      input: (provided: any) => ({
        ...provided,
        color: theme.palette.secondary.light,
      }),
      option: (provided: any) => ({
        ...provided,
        color: theme.palette.secondary.dark,
      }),
      singleValue: (provided: any) => ({
        ...provided,
        color: theme.palette.primary.light,
      }),
    }
  }), [
    value,
    onChange,
    defaultInputValue,
    theme
  ])

  return (
    <GooglePlacesAutocomplete
      minLengthAutocomplete={6}
      debounce={600}
      selectProps={selectProps}
    />
  )
}
