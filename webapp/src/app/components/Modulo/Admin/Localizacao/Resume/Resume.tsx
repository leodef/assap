import React from "react";
import {
  Button,
  createStyles,
  makeStyles,
  Theme,
  Typography
} from "@material-ui/core";
import {
  getLocationGoogleMapsLink
} from "../../../../../utils/GoogleMaps/GoogleUtils";
import "./Resume.scss";

/**
 * Visualização de item do tipo TipoContatoInfo
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo TipoContatoInfo
 */
export const Resume = (props: any) => {
  const classes = useStyles();

  const { item } = props;
  const link = getLocationGoogleMapsLink(item)
  return (
    <React.Fragment>
      <Typography variant="h6" component="h6">
        {item.titulo || item.endereco}
      </Typography>

      <Typography variant="body2" component="p">
        {item.pais ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}>Pais:</strong>
            {item.pais}
          </React.Fragment>
        ) : null}
        {item.estado ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}> - Estado:</strong>
            {item.estado}
          </React.Fragment>
        ) : null}
        {item.cidade ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}> - Cidade:</strong>
            {item.cidade}
          </React.Fragment>
        ) : null}
        {item.pais ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}> - Bairro:</strong>
            {item.area}
          </React.Fragment>
        ) : null}
      </Typography>

      <Typography variant="body2" component="p">
        {item.codigoPostal ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}>Código postal:</strong>
            {item.codigoPostal}
          </React.Fragment>
        ) : null}
        {item.numero ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}> - Número:</strong>
            {item.numero}
          </React.Fragment>
        ) : null}
        {item.complemento ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}> - Complemento:</strong>
            {item.complemento}
          </React.Fragment>
        ) : null}
        {item.lat || item.long ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}> - (Lat / Lng):</strong>
            {item.lat} / {item.long}
          </React.Fragment>
        ) : null}
      </Typography>
      <Button
        variant="outlined"
        color="primary"
        rel="noopener"
        target="_blank"
        href={link}> Abrir Mapa
      </Button>
    </React.Fragment>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    resumeLabel: {
      marginRight: "0.35em",
      marginLeft: "0.35em",
    },
  })
);
