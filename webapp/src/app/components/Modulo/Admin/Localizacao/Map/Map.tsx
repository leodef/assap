import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Marker, GoogleMap } from "@react-google-maps/api";
import "./Map.scss";

const containerStyle = {
  width: "100%",
  height: "200px",
};

/**
 * Area de inputs para Localização
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Localização
 */
export const Map = (props: any) => {
  const { value, onChange, disabled } = useMemo(() => props, [props]);
  const point = useMemo(() => {
    return value ? { lat: value.lat || 0, lng: value.long || 0 } : null;
  }, [value]);

  const onDragEnd = useCallback(
    (coord) => {
      if (onChange && coord) {
        const { latLng } = coord;
        const lat = latLng.lat();
        const lng = latLng.lng();
        onChange({ lat, lng });
      }
    },
    [onChange]
  );

  // #region Map
  const [map, setMap] = useState(null as any);
  const center = useMemo(() =>
    new window.google.maps.LatLng(point?.lat, point?.lng)
  , [point]);
  const zoom = useMemo(() => 16, []);

  const onLoad = useCallback(function callback(map) {
    const bounds = new window.google.maps.LatLngBounds();
    map.fitBounds(bounds);
    map.panTo(center);
    map.setZoom(zoom);
    setMap(map);
  }, [zoom, center]);

  const onUnmount = useCallback(function callback(map) {
    setMap(null);
  }, []);

  useEffect(() => {
    if(map) {
      map.panTo(center);
      map.setZoom(zoom);
    }
  }, [center, zoom, map])

  // #endregion Map
  return (
    <GoogleMap
      mapContainerStyle={containerStyle}
      zoom={zoom}
      center={center}
      onLoad={onLoad}
      onUnmount={onUnmount}
    >
      {point ? (
        <Marker
        position={point}
        draggable={!disabled}
        onDragEnd={onDragEnd} />
      ) : null}
    </GoogleMap>
  );
};
