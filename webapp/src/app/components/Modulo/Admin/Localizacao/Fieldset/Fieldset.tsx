import React, { useCallback, useMemo, useState } from 'react'
import * as _ from 'lodash'
import {
  createStyles,
  FormControlLabel,
  Grid,
  makeStyles,
  Switch,
  // eslint-disable-next-line no-unused-vars
  Theme
} from '@material-ui/core'
import { useField } from 'formik'
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  getFormName,
  TextField
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import { GeocodeUtils } from '../../../../../utils/GoogleMaps/GeocodeUtils'
// eslint-disable-next-line no-unused-vars
import {
  // eslint-disable-next-line no-unused-vars
  Localizacao,
  initialValues
} from '../../../../../types/API/Localizacao'
import { Map } from '../Map/Map'
import { Autocomplete } from '../Autocomplete/Autocomplete'
import './Fieldset.scss'
/*
    Localizacao
      titulo: String,
      endereco: String,
      lat: Number,
      long:  Number,
      pais: String,
      estado: String,
      cidade: String,
      area: String
*/

/**
 * Area de inputs para Localização
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Localização
 */
export const Fieldset = (props: any) => {
  const classes = useStyles()
  const { disabled } = useMemo(() => props, [props])
  const propFormType = useMemo(() => props.formType, [props])

  const [formType, setFormType] = useState( (propFormType || 'google') as 'google' | 'manual')
  const checked = useMemo(() => formType === 'google', [formType])
  const formTypeLabel = useMemo(() => (checked ? 'Pesquisar localização' : 'Localização Manual'), [
    checked
  ])
  const handleFormTypeChange = useCallback(
    (event: any) => setFormType(event.target.checked ? 'google' : 'manual'),
    [setFormType]
  )

  const fields = useMemo(() => {
    return {
      localizacao: {
        name: getFormName(props),
        label: 'Localização'
      },
      titulo: {
        name: getFieldName('Título', props),
        label: 'Titulo'
      },
      endereco: {
        name: getFieldName('endereco', props),
        label: 'Endereço'
      },
      lat: {
        name: getFieldName('lat', props),
        label: 'Latitude'
      },
      long: {
        name: getFieldName('long', props),
        label: 'Longitude'
      },
      pais: {
        name: getFieldName('pais', props),
        label: 'Pais'
      },
      estado: {
        name: getFieldName('estado', props),
        label: 'Estado'
      },
      cidade: {
        name: getFieldName('cidade', props),
        label: 'Cidade'
      },
      area: {
        name: getFieldName('area', props),
        label: 'Bairro'
      },
      codigoPostal: {
        name: getFieldName('codigoPostal', props),
        label: 'Código postal'
      },
      numero: {
        name: getFieldName('numero', props),
        label: 'Número'
      },
      complemento: {
        name: getFieldName('complemento', props),
        label: 'Complemento'
      }
    }
  }, [props])

  const [, localizacaoMeta, localizacaoHelpers] = useField(
    fields.localizacao.name
  )
  const localizacao = useMemo(() => localizacaoMeta.value, [localizacaoMeta.value])

  const [enderecoValue, setEnderecoValue] = useState(null as any)
  const endereco = useMemo(() => (localizacao ? localizacao.endereco : null), [localizacao])
  const loadLocalizacaoFromGeocode = useCallback(
    (value: Localizacao, others: any) => {
      const newValue = {
        ...initialValues,
        ..._.pickBy(localizacaoMeta.value, _.identity),
        ...(value ? _.pickBy(value, _.identity) : {}),
        ...(others || {})
      }
      localizacaoHelpers.setValue(newValue)
      return newValue
    },
    [localizacaoMeta.value, localizacaoHelpers]
  )
  const loadAutocompleteResult = useCallback(
    (event: any) => {
      if (event && event.label) {
        GeocodeUtils.fromAddress(event.label).then((value: Localizacao) => {
          const others = (event.label ? { titulo: event.label } : {})
          loadLocalizacaoFromGeocode(value, others)
        })
      }
      setEnderecoValue(event)
    },
    [loadLocalizacaoFromGeocode]
  )

  const loadMapResult = useCallback(
    (event: any) => {
      if (event) {
        GeocodeUtils.fromLatLng(event.lat, event.lng).then((value: Localizacao) => {
          const others = (event.label ? { titulo: event.label } : {})
          loadLocalizacaoFromGeocode(value, others)
          setEnderecoValue(null)
        })
      }
    },
    [loadLocalizacaoFromGeocode]
  )
  return (
    <Grid container spacing={1}>
      {
        propFormType ? null : <Grid item sm={12}>
          <FormControlLabel
            control={
              <Switch
                checked={checked}
                onChange={handleFormTypeChange}
                color="primary"
              />
            }
            label={formTypeLabel}
          />
        </Grid>
      }

      <Grid
        item
        container
        xs={12}
        justify="center"
        className={
          formType === 'google'
            ? classes.show
            : classes.hidden
        }>
          <Grid item sm={12} >
            <Autocomplete
              value={enderecoValue}
              onChange={loadAutocompleteResult}
              defaultInputValue={endereco}
              disabled={disabled} />
          </Grid>
          <Grid item sm={12}>
            <Map
              value={localizacao}
              disabled={disabled}
              onChange={loadMapResult} />
          </Grid>
      </Grid>

      <Grid
        item
        sm={12}
        className={
          formType === 'manual'
            ? classes.show
            : classes.hidden}>

        <Grid container>
          <Grid item sm={12}>
            <TextField
              name={fields.endereco.name}
              type="text"
              label={fields.endereco.label}
              disabled={disabled}
              className={classes.enderecoInput}
            />
          </Grid>

          <Grid item sm={6}>
            <TextField
              name={fields.lat.name}
              type="number"
              label={fields.lat.label}
              disabled={disabled}
            />
          </Grid>

          <Grid item sm={6}>
            <TextField
              name={fields.long.name}
              type="number"
              label={fields.long.label}
              disabled={disabled}
            />
          </Grid>

          <Grid item sm={4}>
            <TextField
              name={fields.pais.name}
              type="text"
              label={fields.pais.label}
              disabled={disabled}
            />
          </Grid>

          <Grid item sm={4}>
            <TextField
              name={fields.estado.name}
              type="text"
              label={fields.estado.label}
              disabled={disabled}
            />
          </Grid>

          <Grid item sm={4}>
            <TextField
              name={fields.cidade.name}
              type="text"
              label={fields.cidade.label}
              disabled={disabled}
            />
          </Grid>

          <Grid item sm={4}>
            <TextField
              name={fields.area.name}
              type="text"
              label={fields.area.label}
              disabled={disabled}
            />
          </Grid>

          <Grid item sm={4}>
            <TextField
              name={fields.codigoPostal.name}
              type="text"
              label={fields.codigoPostal.label}
              disabled={disabled}
            />
          </Grid>

          <Grid item sm={4}>
            <TextField
              name={fields.numero.name}
              type="text"
              label={fields.numero.label}
              disabled={disabled}
            />
          </Grid>

          <Grid item sm={4}>
            <TextField
              name={fields.complemento.name}
              type="text"
              label={fields.complemento.label}
              disabled={disabled}
            />
          </Grid>
        </Grid>

      </Grid>

    </Grid>
  )
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    hidden: {
      display: 'none'
    },
    show: {
      display: 'block'
    },
    enderecoInput: {
      width: '100%'
    }
  })
)
