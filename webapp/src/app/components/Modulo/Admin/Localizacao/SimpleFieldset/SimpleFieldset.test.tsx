/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { SimpleFieldset } from './SimpleFieldset'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<SimpleFieldset />, div)
  ReactDOM.unmountComponentAtNode(div)
})
