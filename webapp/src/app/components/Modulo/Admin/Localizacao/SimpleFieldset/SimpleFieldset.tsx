import React, { useMemo } from 'react'
import {
  createStyles,
  Grid, makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import { getFieldName, TextField } from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import './SimpleFieldset.scss'

/**
 * Area de inputs para Localização
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Localização
 */
export const SimpleFieldset = React.memo((props: any) => {
  SimpleFieldset.displayName = 'SimpleFieldset'
  const classes = useStyles()
  const { disabled } = useMemo(() => props, [props])

  /*
    Localizacao
      titulo: String,
      endereco: String,
      lat: Number,
      long:  Number,
      pais: String,
      estado: String,
      cidade: String,
      area: String
  */
  const fields = useMemo(() => {
    return {
      titulo: {
        name: getFieldName('Título', props),
        label: 'titulo'
      },
      endereco: {
        name: getFieldName('endereco', props),
        label: 'Endereço'
      },
      lat: {
        name: getFieldName('lat', props),
        label: 'Latitude'
      },
      long: {
        name: getFieldName('long', props),
        label: 'Longitude'
      },
      pais: {
        name: getFieldName('pais', props),
        label: 'Pais'
      },
      estado: {
        name: getFieldName('estado', props),
        label: 'Estado'
      },
      cidade: {
        name: getFieldName('cidade', props),
        label: 'Cidade'
      },
      area: {
        name: getFieldName('area', props),
        label: 'Bairro'
      }
    }
  }, [props])

  return (

    <Grid container spacing={2} >

      <Grid item sm={12}>
        <TextField
          name={fields.endereco.name}
          type="text"
          label={fields.endereco.label}
          disabled={disabled}
          className={classes.enderecoInput}
        />
      </Grid>

      <Grid item sm={6}>
        <TextField
          name={fields.lat.name}
          type="number"
          label={fields.lat.label}
          disabled={disabled}
        />
      </Grid>

      <Grid item sm={6}>

        <TextField
          name={fields.long.name}
          type="number"
          label={fields.long.label}
          disabled={disabled}
        />
      </Grid>

      <Grid item sm={12}>
        <TextField
          name={fields.pais.name}
          type="text"
          label={fields.pais.label}
          disabled={disabled}
        />
      </Grid>

      <Grid item sm={12}>
        <TextField
          name={fields.estado.name}
          type="text"
          label={fields.estado.label}
          disabled={disabled}
        />
      </Grid>

      <Grid item sm={12}>
        <TextField
          name={fields.cidade.name}
          type="text"
          label={fields.cidade.label}
          disabled={disabled}
        />
      </Grid>
      <Grid item sm={12}>
        <TextField
          name={fields.area.name}
          type="text"
          label={fields.area.label}
          disabled={disabled}
        />
      </Grid>
    </Grid>
  )
})

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    enderecoInput: {
      width: '100%'
    }
  })
)
