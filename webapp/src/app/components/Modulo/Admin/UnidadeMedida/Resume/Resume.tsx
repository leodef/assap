import React from 'react'
import {
  Typography
} from '@material-ui/core'
import './Resume.scss'

/**
 * Visualização de item do tipo UnidadeMedida
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo UnidadeMedida
 */
export const Resume = (props: any) => {
  /*
    UnidadeMedida :
      grandeza: Grandeza,
      titulo: String,
      desc: String,
      valor: Number
  */
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.grandeza}
    </Typography>
    <Typography variant="h5" component="h2">
      {item.titulo}
    </Typography>
    <Typography variant="body2" component="p">
      {item.desc}
    </Typography>
    <Typography variant="body2" component="p">
      {item.valor}
    </Typography>
  </React.Fragment>)
}
