import React, { useMemo, useContext, useEffect, useCallback } from 'react'
import {
  Grid
} from '@material-ui/core'
import { useField } from 'formik'
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  TextField,
  useStyles as fieldsUseStyles
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import { Autocomplete as GrandezaAutocomplete } from '../../Grandeza/Autocomplete/Autocomplete'
import { ParentCrudContext, findParentByPrefix } from '../../../../../contexts/ParentCrudContext'
import './Fieldset.scss'

/**
 * Area de inputs para UnidadeMedida
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para UnidadeMedida
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  const fieldsClasses = fieldsUseStyles()
  const { disabled, item } = useMemo(() => props || {}, [props])
  const desc = useMemo(() => item ? item.desc : null, [item])

  const parentCrudContextValue = useContext(ParentCrudContext)
  const grandezaParent = useMemo(
    () => {
      const parent = findParentByPrefix('grandeza', parentCrudContextValue)
      return parent ? parent.item : null
    }, [parentCrudContextValue])
  /*
    UnidadeMedida :
      grandeza: Grandeza,
      titulo: String,
      desc: String,
      valor: Number
  */
  const fields = useMemo(() => {
    return {
      grandeza: {
        name: getFieldName('grandeza', props),
        label: 'Grandeza'
      },
      titulo: {
        name: getFieldName('titulo', props),
        label: 'Título'
      },
      desc: {
        name: getFieldName('desc', props),
        label: 'Descrição'
      },
      valor: {
        name: getFieldName('valor', props),
        label: 'Valor'
      }
    }
  }, [props])

  const [, , grandezaHelpers] = useField(fields.grandeza.name)

  const setGrandeza = useCallback(
    (grandeza?: any) => {
      const grandezaId = grandeza ? grandeza._id : null
      const parentId = (grandezaParent && grandezaParent._id)
        ? grandezaParent._id
        : null
      grandezaHelpers.setValue(parentId || grandezaId || '')
    }, // eslint-disable-next-line
    [grandezaParent]
  )
  useEffect(() => setGrandeza(), [grandezaParent, setGrandeza])

  return (

    <Grid container spacing={2} >

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.grandeza.name}
          type="text"
          disabled={disabled}
        />
      </Grid>

      <Grid item xs={12}>
        { /* hidden={!!grandezaParentItem} */ }
        <GrandezaAutocomplete
          onChange={(selected: any) => setGrandeza(selected)}
          value={grandezaParent}
          disabled={disabled} />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.titulo.name}
          type="text"
          label={fields.titulo.label}
          uppercase="true"
          disabled={disabled}
        />
      </Grid>

      <Grid item xs={12}>
        <TextField
          name={fields.desc.name}
          type="text"
          label={fields.desc.label}
          disabled={disabled}
          value={desc}
          multiline
          className={fieldsClasses.multiline}
        />
      </Grid>

      <Grid item xs={12} sm={6} >
        <TextField
          name={fields.valor.name}
          type="number"
          label={fields.valor.label}
          disabled={disabled}
        />
      </Grid>

    </Grid>
  )
})
