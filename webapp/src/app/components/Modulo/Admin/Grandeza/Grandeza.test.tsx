/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Grandeza } from './Grandeza'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Grandeza />, div)
  ReactDOM.unmountComponentAtNode(div)
})
