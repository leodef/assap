import
React, {
  useMemo, useCallback
} from 'react'
import {
  Assignment as AssignmentIcon
} from '@material-ui/icons'
import {
  CollectionTypeEnum
} from '../../../../types/Crud'
import { Crud } from '../../../Shared/Crud/Crud'
import { TablePropsField } from '../../../Shared/Collection/Table/Table'
import { Resume } from './Resume/Resume'
import { Fieldset } from './Fieldset/Fieldset'
import {
  GrandezaSchema,
  initialValues
} from '../../../../types/API/Grandeza'
import {
  // UnidadeMedidaModal,
  UnidadeMedida
} from './UnidadeMedida/UnidadeMedida'
import { Grid } from '@material-ui/core'
// import { UnidadeMedida } from './UnidadeMedida/UnidadeMedida'
import './Grandeza.scss'

export const Grandeza = (props: any) => {
  const { collection, form, show, remove } = props
  const { type } = (collection || {})
  const fields = useMemo(() => [
    new TablePropsField('titulo', 'Título'),
    new TablePropsField('desc', 'Descrição')
  ], [])

  const ShowBody = useCallback((props: any) => {
    return (<Grid container direction="column">
      <Grid item>
        <Resume
          {...props} />
      </Grid>
      <Grid item>
        <UnidadeMedida />
      </Grid>
    </Grid>)
  }, [])

  const FormBody = useCallback((props: any) => {
    return (<Grid container direction="column">
      <Grid item>
        <Fieldset
          {...props} />
      </Grid>
      <Grid item>
        <UnidadeMedida />
      </Grid>
    </Grid>)
  }, [])

  const tableConfig = useMemo(() => {
    return {
      label: 'Grandeza',
      align: 'center',
      actionsLabel: 'Ações',
      fields
    }
  }, [fields])
  const listConfig = useMemo(() => {
    return {
      dense: true,
      subheader: 'Grandeza',
      fields,
      getListItem: (value: any, index: number) => {
        return {
          primary: (value.titulo ? value.titulo.toString() : ''),
          secondary: null,
          avatar: (value.titulo ? value.titulo.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
        }
      }
    }
  }, [fields])
  const config = (type === CollectionTypeEnum.LIST) ? listConfig : tableConfig
  const params = useMemo(() => {
    const params = {
      /* child: {
        // eslint-disable-next-line react/display-name
        body: (props: any) => (<UnidadeMedida {...props} />)
      }, */
      collection: {
        type,
        config,
        // resolve: 'FRONT' // 'BACK'
        ...(collection || {})
      },
      show: {
        title: 'Grandeza',
        // eslint-disable-next-line react/display-name
        body: ShowBody,
        ...(show || {})
      },
      form: {
        title: 'Grandeza',
        // eslint-disable-next-line react/display-name
        body: FormBody,
        initialValues,
        schema: GrandezaSchema,
        ...(form || [])
      },
      remove: {
        title: 'Deseja realmente remover a grandeza ?',
        ...(remove || {})
      }
    }
    return params
  }, [
    collection,
    config,
    form,
    remove,
    show,
    type,
    FormBody,
    ShowBody
  ])
  return <Crud {...params} />
}
