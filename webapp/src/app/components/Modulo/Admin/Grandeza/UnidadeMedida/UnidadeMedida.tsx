import React, { useContext, useState, useMemo, useCallback } from 'react'
import { useSelector } from 'react-redux'
// eslint-disable-next-line no-unused-vars
import { Modal, makeStyles, Theme, createStyles, Button } from '@material-ui/core'
import BallotIcon from '@material-ui/icons/Ballot'
// eslint-disable-next-line no-unused-vars
import { Parent, getParentContextValue, ParentCrudContext } from '../../../../../contexts/ParentCrudContext'
import { CrudContext } from '../../../../../contexts/CrudContext'
import { UnidadeMedida as UnidadeMedidaCrudComponent } from '../../UnidadeMedida/UnidadeMedida'
import { crudType as types } from '../../../../../types/API/UnidadeMedida'
import { CollectionTypeEnum } from '../../../../../types/Crud'
import './UnidadeMedida.scss'

/**
 * Gerenciamento de unidades de medida
 * @param {any} props Propriedades
 * @return {React.Component} Componente com gerenciamento de unidades de medida
 */
export const UnidadeMedidaModal = (props: any) => {
  const { actions } = useContext(CrudContext)
  const classes = useStyles()
  const [isOpen, setOpen] = useState(false)
  const onClose = useCallback(() => {
    setOpen(false)
  }, [setOpen])
  const toggleOpen = useCallback(() => {
    setOpen(!isOpen)
  }, [setOpen, isOpen])

  const button = useMemo(() => {
    return actions.unidadeMedida
      ? (<Button
        variant="contained"
        color="primary"
        startIcon={<BallotIcon />}
        onClick={(event: any) => toggleOpen()}>
        Unidades de medida
      </Button>) : null
  }, [actions, toggleOpen])

  return (<React.Fragment>
    {button}
    <Modal
      open={isOpen}
      onClose={onClose}
      aria-labelledby="Unidades de medida"
      aria-describedby="Unidades de medida">
      <div className={classes.paper}>
        <UnidadeMedida
          {...props} />
      </div>
    </Modal>
  </React.Fragment>)
}

export const UnidadeMedidaCrud = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.admin.unidadeMedida.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'unidadeMedida'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  return (<CrudContext.Provider value={crudContextValue}>
    <UnidadeMedidaCrudComponent collection={collection}/>
  </CrudContext.Provider>)
}
export const UnidadeMedida = (props: any) => {
  const { getState, prefix } = useContext(CrudContext)
  const { item, action } = useSelector(state => {
    const { item, action } = getState(state)
    return { item, action }
  })
  const parentValue = useMemo(
    () => new Parent(
      item, // item
      action, // action
      prefix// prefix
    ), [item, action, prefix])
  const historyValue = useContext(ParentCrudContext)
  const parentCrudContextValue = useMemo(
    () => getParentContextValue(parentValue, historyValue)
    , [parentValue, historyValue])
  return useMemo(
    () => (item && item._id)
      ? (<ParentCrudContext.Provider value={parentCrudContextValue}>
        <UnidadeMedidaCrud />
      </ParentCrudContext.Provider>)
      : null,
    [item, parentCrudContextValue])
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      minWidth: 200,
      minHeight: 200,
      // boxShadow: theme.shadows[5],
      // padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    }
  })
)
