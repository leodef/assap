import React, { useCallback, useMemo } from 'react'
import { crudType as types } from '../../../../../types/API/Contato/TipoContatoInfo'
import { AutocompleteContext } from '../../../../../contexts/AutocompleteContext'
import { Autocomplete as SharedAutocomplete } from '../../../../Shared/Autocomplete/Autocomplete'
import './Autocomplete.scss'

/**
 * Visualização de item do tipo TipoContatoInfo
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo TipoContatoInfo
 */
export const Autocomplete = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.admin.tipoContatoInfo.options, [])
  const { fields, label } = useMemo(() => {
    return { fields: ['titulo'], label: (props.label || 'Tipo') }
  }, [props.label])
  const getOptionLabel = (option: any) => option ? option.titulo || '' : ''
  const getOptionId = (val: any) => val ? val._id || null : null
  const autocompleteContextValue = { getState, types, getOptionLabel, getOptionId, fields, label }

  return (<AutocompleteContext.Provider value={autocompleteContextValue}>
    <SharedAutocomplete
      {...props} />
  </AutocompleteContext.Provider>)
}
