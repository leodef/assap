import React, { useMemo } from 'react'
import {
  Grid
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  TextField,
  useStyles as fieldsUseStyles
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import './Fieldset.scss'

/**
 * Area de inputs para TipoContatoInfo
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para TipoContatoInfo
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  const fieldsClasses = fieldsUseStyles()
  const { disabled, item } = props
  const desc = useMemo( () => item ? item.desc : null, [item])

  /*
    TipoContatoInfo :
      titulo: string,
      desc:   string,
      mask: String
  */
  const fields = useMemo(() => {
    return {
      titulo: {
        name: getFieldName('titulo', props),
        label: 'Título'
      },
      desc: {
        name: getFieldName('desc', props),
        label: 'Descrição'
      },
      mask: {
        name: getFieldName('mask', props),
        label: 'Mascara'
      }
    }
  }, [props])

  return (

    <Grid container spacing={2} >

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.titulo.name}
          type="text"
          label={fields.titulo.label}
          disabled={disabled}
        />
      </Grid>

      <Grid item xs={12}>

        <TextField
          name={fields.desc.name}
          type="text"
          label={fields.desc.label}
          disabled={disabled}
          value={desc}
          multiline
          className={fieldsClasses.multiline}
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.mask.name}
          type="text"
          label={fields.mask.label}
          disabled={disabled}
        />
      </Grid>

    </Grid>
  )
})
