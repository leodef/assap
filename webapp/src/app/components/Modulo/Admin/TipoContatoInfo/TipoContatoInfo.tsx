import
React, {
  useMemo, useCallback
} from 'react'
import {
  Assignment as AssignmentIcon
} from '@material-ui/icons'
import {
  CollectionTypeEnum
} from '../../../../types/Crud'
import { Crud } from '../../../Shared/Crud/Crud'
import { TablePropsField } from '../../../Shared/Collection/Table/Table'
import { Resume } from './Resume/Resume'
import { Fieldset } from './Fieldset/Fieldset'
import {
  TipoContatoInfoSchema,
  initialValues
} from '../../../../types/API/Contato/TipoContatoInfo'
import './TipoContatoInfo.scss'

export const TipoContatoInfo = (props: any) => {
  const { collection, form, show, remove } = props
  const { type } = (collection || {})
  const fields = useMemo(() => [
    new TablePropsField('titulo', 'Título'),
    new TablePropsField('desc', 'Descrição')
  ], [])

  const ShowBody = useCallback((props: any) => {
    return (<Resume
      {...props} />)
  }, [])
  const FormBody = useCallback((props: any) => {
    return (<Fieldset
      {...props} />)
  }, [])

  const tableConfig = useMemo(() => {
    return {
      label: 'Tipo de informações para contato',
      align: 'center',
      actionsLabel: 'Ações',
      fields
    }
  }, [fields])
  const listConfig = useMemo(() => {
    return {
      dense: true,
      subheader: 'Tipo de informações para contato',
      fields,
      getListItem: (value: any, index: number) => {
        return {
          primary: (value.titulo ? value.titulo.toString() : ''),
          secondary: null,
          avatar: (value.titulo ? value.titulo.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
        }
      }
    }
  }, [fields])
  const config = (type === CollectionTypeEnum.LIST) ? listConfig : tableConfig
  const params = useMemo(() => {
    const params = {
      collection: {
        type,
        config,
        // resolve: 'FRONT' // 'BACK'
        ...(collection || {})
      },
      show: {
        title: 'Tipo de Informação para Contato',
        // eslint-disable-next-line react/display-name
        body: ShowBody,
        ...(show || {})
      },
      form: {
        title: 'Tipo de Informação para Contato',
        // eslint-disable-next-line react/display-name
        body: FormBody,
        initialValues,
        schema: TipoContatoInfoSchema,
        ...(form || [])
      },
      remove: {
        title: 'Deseja realmente remover o tipo de informação para contato ?',
        ...(remove || {})
      }
    }
    return params
  }, [
    collection,
    config,
    form,
    remove,
    show,
    type,
    FormBody,
    ShowBody
  ])
  return <Crud {...params} />
}
