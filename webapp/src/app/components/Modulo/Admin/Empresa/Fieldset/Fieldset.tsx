import React, { useMemo } from 'react'
import {
  Grid
} from '@material-ui/core'
import {
  getFieldName,
  TextField,
  useStyles as fieldsUseStyles
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import './Fieldset.scss'

/**
 * Area de inputs para Empresa
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Empresa
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  const fieldsClasses = fieldsUseStyles()
  const { disabled, item } = props
  const desc = useMemo( () => item ? item.desc : null, [item])

  const fields = useMemo(() => {
    return {
      nomeFantasia: {
        name: getFieldName('nomeFantasia', props),
        label: 'Nome Fantasia'
      },
      razaoSocial: {
        name: getFieldName('razaoSocial', props),
        label: 'Razão Social'
      },
      desc: {
        name: getFieldName('desc', props),
        label: 'Descrição'
      },
      identificacao: {
        name: getFieldName('identificacao', props),
        label: 'Identificação (CNPJ)'
      },
    }
  }, [props])
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.nomeFantasia.name}
          type="text"
          label={fields.nomeFantasia.label}
          disabled={disabled}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.razaoSocial.name}
          type="text"
          label={fields.razaoSocial.label}
          disabled={disabled}
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.identificacao.name}
          type="text"
          label={fields.identificacao.label}
          disabled={disabled}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          name={fields.desc.name}
          type="text"
          label={fields.desc.label}
          disabled={disabled}
          value={desc}
          multiline
          className={fieldsClasses.multiline}
        />
      </Grid>
    </Grid>
  )
})
