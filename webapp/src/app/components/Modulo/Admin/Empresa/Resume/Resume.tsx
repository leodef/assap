import React from 'react'
import {
  createStyles,
  makeStyles,
  Theme,
  Typography
} from '@material-ui/core'
import './Resume.scss'

/**
 * Visualização de item do tipo Empresa
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Empresa
 */
export const Resume = (props: any) => {
  const classes = useStyles()
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.titulo}
    </Typography>
    <Typography variant="body2" component="p">
      {item.desc}
    </Typography>
    <Typography variant="body2" component="p">
      <strong className={classes.resumeLabel}>Nome Fantasia</strong>{item.nomeFantasia}
    </Typography>
    <Typography variant="body2" component="p">
      <strong className={classes.resumeLabel}>Razão Social:</strong>{item.razaoSocial}
    </Typography>
    <Typography variant="body2" component="p">
      <strong className={classes.resumeLabel}>Identificação (CNPJ):</strong>{item.identificacao}
    </Typography>
  </React.Fragment>)
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    resumeLabel: {
      marginRight: '0.35em',
    }
  })
)
