import
  React,
  {
    useCallback,
    useMemo
  }
from "react";
import {
  useField
} from "formik";
import {
  createStyles,
  FormControl,
  IconButton,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Theme,
} from "@material-ui/core";
import {
  Add as AddIcon,
  Delete as DeleteIcon
} from "@material-ui/icons";
import {
  Autocomplete as AutocompleteTipoContatoInfo
} from "../../Admin/TipoContatoInfo/Autocomplete/Autocomplete";
import {
  getFieldName,
  getFormName,
  TextField,
} from "../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField";
import "./ArrayFieldset.scss";
import { initialValues } from "../../../../types/API/Contato/ContatoInfo";

/**
 * Componente ArrayFieldset
 * @param {any} props Propriedades
 * @return {React.Component} Componente ArrayFieldset
 */
export const ArrayFieldset = (props: any) => {
  const classes = useStyles();
  const { disabled } = useMemo(() => props, [props]);

  const fields = useMemo(() => {
    return {
      infos: {
        name: getFormName(props),
        label: "Informação de contato"
      },
      valor: {
        name: getFieldName("valor", props),
        label: "Valor",
        getName: (index: number) =>
          getFieldName("valor", {
            ...props,
            name: getFieldName(String(index), props),
          }),
      },
      tipo: {
        name: getFieldName("tipo", props),
        label: "Tipo",
        getName: (index: number) =>
          getFieldName("tipo", {
            ...props,
            name: getFieldName(String(index), props),
          }),
      },
    };
  }, [props]);

  const [, infosMeta, infosHelpers] = useField(
    fields.infos.name
  );
  const setInfos= useCallback(
    (val: any) => infosHelpers.setValue(val),
    [infosHelpers],
  )
  const infos = useMemo(
    () => (infosMeta.value || []) as Array<any>,
    [infosMeta]
  )

  const updateTipo = useCallback(
    (tipo: any, index: number) => {
      const values = [...infos];
      const newValue = {
        ...(values[index] || {}),
        tipo
       } as any
      values[index] = newValue
      setInfos(values);
    },
    [setInfos, infos]
  );

  const getTipo = useCallback(
    (index: number) => {
      const info = infos[index]
      return info ? info.tipo || null : null
    }, [
    infos,
  ]);
  

  const remove = useCallback(
    (index: number) => {
      setInfos(
        infos.filter((val: any, ind: number) => ind !== index)
      );
    },
    [setInfos, infos]
  );

  const create = useCallback(() => {
    setInfos([
      ...infos,
      initialValues
    ]);
  }, [setInfos, infos]);

  return (
    <TableContainer
      component={Paper}
      className={classes.container}
      >
      <Table
        aria-label="Informações para contato"
        size="small"
        stickyHeader
        >
        <TableHead>
          <TableRow >
            <TableCell
              colSpan={4}
              align="center">
                Informações para contato
            </TableCell>
            <TableCell align="right">
              <IconButton 
                color="primary"
                size="small"
                aria-label="Novo"
                onClick={
                  () => create()}>
                  <AddIcon />
              </IconButton>
            </TableCell>
            
          </TableRow>
        </TableHead>
        <TableBody>
          {infos.map((info: any, index: number) => (
            <TableRow
              key={`contao_info_array_fieldset_${index}`}
              >
              <TableCell colSpan={2}>
                <AutocompleteTipoContatoInfo
                  id="contato-info-autocomplete-tipo-contato-info"
                  margin="normal"
                  noLabel
                  className={classes.formField}
                  fieldProps={{
                    variant: "standard",
                    margon: "normal"
                  }}
                  onChange={
                    (value: any) => updateTipo(value, index)}
                  value={
                    getTipo(index)}
                />
              </TableCell>
              <TableCell colSpan={2}>
                <FormControl
                  className={classes.formFieldContainer} >
                  <TextField
                    name={fields.valor.getName(index)}
                    type="text"
                    label={fields.valor.label}
                    disabled={disabled}
                    className={classes.formField}
                  />
                </FormControl>
              </TableCell>
              <TableCell align="right">
                <IconButton
                  aria-label="Remover"
                  onClick={() => remove(index)}>
                  <DeleteIcon />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      position: 'relative',
      overflow: 'auto',
      maxHeight: 300
    },
    formField: {
      width: 200
    },
    formFieldContainer: {
      paddingBottom: 10
    }
  })
)