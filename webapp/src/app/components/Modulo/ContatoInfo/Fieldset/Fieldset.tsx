import React,
{
  useCallback, useMemo
} from 'react'
import {
  Grid
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import {
  useField
} from 'formik'
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  TextField
} from '../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  Autocomplete as AutocompleteTipoContatoInfo
} from '../../Admin/TipoContatoInfo/Autocomplete/Autocomplete'
import './Fieldset.scss'

/**
 * Componente Fieldset
 * @param {any} props Propriedades
 * @return {React.Component} Componente Fieldset
 */
export const Fieldset = (props: any) => {
  const { disabled } = useMemo(() => props, [props])

  const fields = useMemo(() => {
    return {
      valor: {
        name: getFieldName('valor', props),
        label: ''
      },
      tipo: {
        name: getFieldName('tipo', props),
        label: 'Tipo'
      }
    }
  }, [props])

   
  const [, tipoMeta, tipoHelpers] = useField(fields.tipo.name)

  const setTipo = useCallback(
    (value: any) => tipoHelpers.setValue(value),
    [tipoHelpers]
  )

  const tipo = useMemo(
    () => tipoMeta.value || null,
    [tipoMeta]
  )

  return (
    <Grid container spacing={2} >

      <Grid item xs={12} sm={6}>
        <AutocompleteTipoContatoInfo
          id="contato-info-autocomplete-tipo-contato-info"
          fullWidth
          onChange={setTipo}
          value={tipo}
          />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.valor.name}
          type="text"
          label={fields.valor.label}
          disabled={disabled}
        />
      </Grid>

    </Grid>)
}
