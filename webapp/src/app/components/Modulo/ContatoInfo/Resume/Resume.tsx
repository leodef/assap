import React, { useMemo } from 'react'
import {
  createStyles,
  makeStyles,
  Theme,
  Typography
} from '@material-ui/core'
import './Resume.scss'

/**
 * Visualização de item do tipo Empresa
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Empresa
 */
export const Resume = (props: any) => {
  const classes = useStyles()
  const { item, index } = useMemo(() => props, [props])
  return (<React.Fragment>
    <Typography variant="body2" component="p">
      <strong className={classes.resumeLabel}>
        {index ? `#${index}` : null}
        {item.tipo ? item.tipo.titulo : 'Contato'}:
      </strong>{item.valor}
    </Typography>
  </React.Fragment>)
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    resumeLabel: {
      marginRight: '0.35em',
    }
  })
)
