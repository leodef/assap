import React, { useMemo } from 'react'
import {
  createStyles,
  Divider,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  makeStyles,
  Theme
} from '@material-ui/core'
import './ArrayResume.scss'

/**
 * Visualização de item do tipo Empresa
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Empresa
 */
export const ArrayResume = (props: any) => {
  const classes = useStyles();
  const { item, label } = useMemo(() => props, [props])
  return (<List
    dense
    subheader={
      label ?
        (<ListSubheader>{label}</ListSubheader>) :
        undefined
    }
      className={classes.listContainer}>
    {
        (item || []).map((value: any, index: number) =>
        <React.Fragment  key={`contato_info_array_fieldset_${index}`}>
          <ListItem dense button>
            <ListItemText
              primary={
                `${
                  (value.tipo ? value.tipo.titulo : 'Contato')
                }: ${
                  value.valor
                }`}
            />
          </ListItem>
          <Divider />
        </React.Fragment>)
      }
    </List>)


}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listContainer: {
      width: '100%',
      // backgroundColor: theme.palette.background.paper,
      position: 'relative',
      overflow: 'auto',
    }
  })
);