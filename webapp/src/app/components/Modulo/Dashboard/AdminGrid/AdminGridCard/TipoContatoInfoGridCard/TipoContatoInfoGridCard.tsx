import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/Contato/TipoContatoInfo'
import { TipoContatoInfo } from '../../../../Admin/TipoContatoInfo/TipoContatoInfo'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './TipoContatoInfoGridCard.scss'

export const TipoContatoInfoGridCard = React.memo((props: any) => {
  TipoContatoInfoGridCard.displayName = 'TipoContatoInfoGridCard'
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.tipoContatoInfo.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'tipoContatoInfo'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="TI"
    title="Tipo de Informação para Contato"
    subheader="Gerenciamento de Tipos de Informações para Contato"
    content="Gerenciamento de Tipos de Informações para Contato"
    to="/admin/tipoContatoInfo"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <TipoContatoInfo collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
})
