import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/Empresa/Empresa'
import { Empresa } from '../../../../Admin/Empresa/Empresa'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './EmpresaGridCard.scss'

export const EmpresaGridCard = React.memo((props: any) => {
  EmpresaGridCard.displayName = 'EmpresaGridCard'
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.empresa.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'empresa'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="EM"
    title="Empresas"
    subheader="Gerenciamento de Empresas"
    content="Gerenciamento de Empresas"
    to="/admin/empresa"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Empresa collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
})
