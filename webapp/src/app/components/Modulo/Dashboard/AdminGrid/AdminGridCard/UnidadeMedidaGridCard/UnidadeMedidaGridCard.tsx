import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/UnidadeMedida'
import { UnidadeMedida } from '../../../../Admin/UnidadeMedida/UnidadeMedida'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './UnidadeMedidaGridCard.scss'

export const UnidadeMedidaGridCard = React.memo((props: any) => {
  UnidadeMedidaGridCard.displayName = 'UnidadeMedidaGridCard'
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.unidadeMedida.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'unidadeMedida'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="UM"
    title="Unidades de medida"
    subheader="Gerenciamento de Unidades de Medida"
    content="Gerenciamento de Unidades de Medida"
    to="/admin/unidade-medida"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <UnidadeMedida collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
})
