/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { TipoContatoInfoGridCard } from './TipoContatoInfoGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<TipoContatoInfoGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
