/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { EmpresaGridCard } from './EmpresaGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<EmpresaGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
