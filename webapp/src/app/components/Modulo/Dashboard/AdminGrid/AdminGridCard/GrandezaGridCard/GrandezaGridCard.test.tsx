/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { GrandezaGridCard } from './GrandezaGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<GrandezaGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
