/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { UnidadeMedidaGridCard } from './UnidadeMedidaGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<UnidadeMedidaGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
