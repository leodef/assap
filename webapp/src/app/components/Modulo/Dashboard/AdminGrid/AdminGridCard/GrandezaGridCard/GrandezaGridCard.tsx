import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/Grandeza'
import { Grandeza } from '../../../../Admin/Grandeza/Grandeza'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './GrandezaGridCard.scss'

export const GrandezaGridCard = React.memo((props: any) => {
  GrandezaGridCard.displayName = 'GrandezaGridCard'
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.grandeza.crud, [])
  const actions = React.useMemo(() => { return { unidadeMedida: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'grandeza'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="GR"
    title="Grandezas"
    subheader="Gerenciamento de Grandezas"
    content="Gerenciamento de Grandezas"
    to="/admin/grandeza"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Grandeza collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
})
