import React from 'react'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core/styles'
import {
  Grid
} from '@material-ui/core'
import { EmpresaGridCard } from './AdminGridCard/EmpresaGridCard/EmpresaGridCard'
// import { GrandezaGridCard } from './AdminGridCard/GrandezaGridCard/GrandezaGridCard'
import { TipoContatoInfoGridCard } from './AdminGridCard/TipoContatoInfoGridCard/TipoContatoInfoGridCard'
// import { UnidadeMedidaGridCard } from './AdminGridCard/UnidadeMedidaGridCard/UnidadeMedidaGridCard'
import './AdminGrid.scss'

const gridUseStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    }
  })
)
/** Grandeza
 * Área Administrativa do painel de controle
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com Área Administrativa do painel de controle
 */
export const AdminGrid = React.memo((props: any) => {
  AdminGrid.displayName = 'AdminGrid'
  const classes = gridUseStyles()
  return (
    <Grid container justify="center" className={classes.root} spacing={4}>
      { /* EM - Empresa */ }
      <Grid item>
        <EmpresaGridCard />
      </Grid>
      { /* GR - Grandezas 
      <Grid item>
        <GrandezaGridCard />
      </Grid>
      */ }
      { /* TC - Tipo contato info */ }
      <Grid item>
        <TipoContatoInfoGridCard />
      </Grid>
      { /* UM - Unidade de medida 
      <Grid item>
        <UnidadeMedidaGridCard />
      </Grid>
      */ }
    </Grid>
  )
})
