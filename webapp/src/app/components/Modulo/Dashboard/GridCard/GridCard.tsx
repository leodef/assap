import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core/styles'
import {
  Typography,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Avatar,
  IconButton,
  Collapse
} from '@material-ui/core'
import {
  OpenInBrowser as OpenInBrowserIcon,
  ExpandMore as ExpandMoreIcon
} from '@material-ui/icons/'
import clsx from 'clsx'
import './GridCard.scss'

function getCardUseStyles (color: any, theme?: Theme) {
  return makeStyles((theme: Theme) =>
    createStyles({
      
      avatar: {
        backgroundColor: color[500]
      }
    })
  )(theme)
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 400, // 345
      padding: theme.spacing(0.5),
    },
    media: {
      height: 0,
      paddingTop: '56.25%' // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest
      })
    },
    expandOpen: {
      transform: 'rotate(180deg)'
    },
    expandedContent: {
      padding: theme.spacing(0.5),
    }
  })
)

export interface GridCardProps {
  avatar: any;
  title: string;
  subheader: string;
  content: string;
  to: string;
  color: any;
  children?: any;
}

/**
 * Cartão padronizado para a painel do dashboard
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com cartão padronizado para a painel do dashboard
 */
export const GridCard = (props: GridCardProps) => {
  const { avatar, title, subheader, content, to, color } = props
  const cardClasses = getCardUseStyles(color)
  const classes = useStyles()

  const [expanded, setExpanded] = React.useState(false)
  const handleExpandClick = () => {
    setExpanded(!expanded)
  }

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={cardClasses.avatar}>
            {avatar}
          </Avatar>
        }
        title={title}
        subheader={subheader}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {content}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton
          aria-label={subheader}
          component={RouterLink}
          to={to}>
          <OpenInBrowserIcon />
        </IconButton>
        { props.children
          ? (<IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="Mostrar mais"
          >
            <ExpandMoreIcon />
          </IconButton>) : null
        }
      </CardActions>
      { props.children
        ? (<Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent className={classes.expandedContent}>
            {props.children}
          </CardContent>
        </Collapse>) : null
      }
    </Card>
  )
}
