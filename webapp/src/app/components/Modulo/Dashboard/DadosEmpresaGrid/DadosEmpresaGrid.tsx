import React, { useContext, useMemo } from "react";
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles,
} from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";
import { EntregaGridCard } from "./DadosEmpresaGridCard/EntregaGridCard/EntregaGridCard";
import { FilialGridCard } from "./DadosEmpresaGridCard/FilialGridCard/FilialGridCard";
import { FuncionarioGridCard } from "./DadosEmpresaGridCard/FuncionarioGridCard/FuncionarioGridCard";
import { ParceiroGridCard } from "./DadosEmpresaGridCard/ParceiroGridCard/ParceiroGridCard";
import { ProdutoGridCard } from "./DadosEmpresaGridCard/ProdutoGridCard/ProdutoGridCard";
import { VeiculoGridCard } from "./DadosEmpresaGridCard/VeiculoGridCard/VeiculoGridCard";
import "./DadosEmpresaGrid.scss";
import {
  getParentContextValue,
  Parent,
  ParentCrudContext,
} from "../../../../contexts/ParentCrudContext";
import { useSelector } from "react-redux";
import { getState } from "../../../../types/EmpresaUsuario";
import { ActionTypeEnum } from "../../../../types/Crud";

const gridUseStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
  })
);
/** DadosEmpresa
 * Area de dados da empresa do painel de controle
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com area de dados da empresa do painel de controle
 */
export const DadosEmpresaGrid = (props: any) => {
  const { item } = useSelector((state: any) => getState(state));
  const action = ActionTypeEnum.LIST
  const prefix = "empresa"
  const parentValue = useMemo(
    () =>
      new Parent(
        item, // item
        action,
        prefix // prefix
      ),
    [item, action, prefix]
  );

  const historyValue = useContext(ParentCrudContext);
  const hidden = useMemo(() => !Boolean(item), [item]);

  const parentCrudContextValue = useMemo(
    () => getParentContextValue(parentValue, historyValue),
    [parentValue, historyValue]
  );

  return hidden ? null : (
    <ParentCrudContext.Provider value={parentCrudContextValue}>
      <DadosEmpresaGridBody />
    </ParentCrudContext.Provider>
  );
};

export const DadosEmpresaGridBody = (props: any) => {
  const classes = gridUseStyles();
  return (
    <Grid container justify="center" className={classes.root} spacing={4}>
      {/* EN - Entrega */}
      <Grid item>
        <EntregaGridCard />
      </Grid>
      {/* FI - Filial */}
      <Grid item>
        <FilialGridCard />
      </Grid>
      {/* FU - Funcionário */}
      <Grid item>
        <FuncionarioGridCard />
      </Grid>
      {/* PA - Parceiro */}
      <Grid item>
        <ParceiroGridCard />
      </Grid>
      {/* PR - Produto */}
      <Grid item>
        <ProdutoGridCard />
      </Grid>
      {/* VI - Veiculo */}
      <Grid item>
        <VeiculoGridCard />
      </Grid>
    </Grid>
  );
};
