import React, { useCallback, useMemo } from 'react'
import {
  BusinessCenter as ProdutoIcon // Produtos
} from '@material-ui/icons'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/Produto'
import { Produto } from '../../../../DadosEmpresa/Produto/Produto'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './ProdutoGridCard.scss'

export const ProdutoGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.produto.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'produto'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar={(<ProdutoIcon />)}
    title="Produtos"
    subheader="Gerenciamento de Produtos"
    content="Gerenciamento de Produtos"
    to="/dados-empresa/produto"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Produto collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
