import React, { useCallback, useMemo } from 'react'
import {
  RvHookup as EntregaIcon // Entrega
} from '@material-ui/icons'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/Logistica/Entrega'
import { Entrega } from '../../../../Logistica/Entrega/Entrega'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './EntregaGridCard.scss'

export const EntregaGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.entrega.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'entrega'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
      avatar={(<EntregaIcon />)}
      title="Entregas"
      subheader="Gerenciamento de Entregas"
      content="Gerenciamento de Entregas"
      to="/dados-empresa/entrega"
      color={red}>
      <CrudContext.Provider value={crudContextValue}>
        <Entrega collection={collection}/>
      </CrudContext.Provider>
    </GridCard>)
}
