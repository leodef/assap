import React, { useCallback, useMemo } from 'react'
import {
  People as FuncionarioIcon // Funcionarios
} from '@material-ui/icons'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/Funcionario'
import { Funcionario } from '../../../../DadosEmpresa/Funcionario/Funcionario'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './FuncionarioGridCard.scss'

export const FuncionarioGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.funcionario.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'funcionario'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar={(<FuncionarioIcon />)}
    title="Funcionários"
    subheader="Gerenciamento de Funcionários"
    content="Gerenciamento de Funcionários"
    to="/dados-empresa/funcionario"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Funcionario collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
