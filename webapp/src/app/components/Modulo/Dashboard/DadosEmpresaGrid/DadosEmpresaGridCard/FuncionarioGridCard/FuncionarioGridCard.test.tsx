/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { FuncionarioGridCard } from './FuncionarioGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<FuncionarioGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
