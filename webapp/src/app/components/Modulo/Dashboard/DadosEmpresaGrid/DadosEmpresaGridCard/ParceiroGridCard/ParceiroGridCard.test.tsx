/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ParceiroGridCard } from './ParceiroGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ParceiroGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
