/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { VeiculoGridCard } from './VeiculoGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<VeiculoGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
