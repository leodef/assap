import React, { useCallback, useMemo } from 'react'
import {
  LocalShipping as VeiculoIcon // Veiculos
} from '@material-ui/icons'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/Veiculo'
import { Veiculo } from '../../../../DadosEmpresa/Veiculo/Veiculo'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './VeiculoGridCard.scss'

export const VeiculoGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.veiculo.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'veiculo'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar={(<VeiculoIcon />)}
    title="Veiculos"
    subheader="Gerenciamento de Veículos"
    content="Gerenciamento de Veículos"
    to="/dados-empresa/veiculo"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Veiculo collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
