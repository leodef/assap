import React, { useCallback, useMemo } from 'react'
import {
  SupervisedUserCircle as  ParceiroIcon // Parceiros
} from '@material-ui/icons'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/Parceiro/Parceiro'
import { Parceiro } from '../../../../DadosEmpresa/Parceiro/Parceiro'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './ParceiroGridCard.scss'

export const ParceiroGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.parceiro.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'parceiro'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar={(<ParceiroIcon />)}
    title="Parceiros Comerciais"
    subheader="Gerenciamento de Parceiros Comerciais"
    content="Gerenciamento de Parceiros Comerciais"
    to="/dados-empresa/parceiro"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Parceiro collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
