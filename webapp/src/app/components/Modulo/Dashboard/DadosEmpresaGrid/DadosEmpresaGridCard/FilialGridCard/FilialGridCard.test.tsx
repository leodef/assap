/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { FilialGridCard } from './FilialGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<FilialGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
