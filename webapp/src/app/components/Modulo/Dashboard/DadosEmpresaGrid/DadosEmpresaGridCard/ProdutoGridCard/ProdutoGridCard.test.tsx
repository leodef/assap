/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ProdutoGridCard } from './ProdutoGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ProdutoGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
