import React, { useCallback, useMemo } from 'react'
import {
  Business as FilialIcon // Filiais
} from '@material-ui/icons'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/Filial'
import { Filial } from '../../../../DadosEmpresa/Filial/Filial'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './FilialGridCard.scss'

export const FilialGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.filial.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'filial'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar={(<FilialIcon />)}
    title="Filiais"
    subheader="Gerenciamento de Filiais"
    content="Gerenciamento de Filiais"
    to="/dados-empresa/filial"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Filial collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
