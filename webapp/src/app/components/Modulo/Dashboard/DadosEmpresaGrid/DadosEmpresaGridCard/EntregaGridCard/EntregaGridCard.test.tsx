/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { EntregaGridCard } from './EntregaGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<EntregaGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
