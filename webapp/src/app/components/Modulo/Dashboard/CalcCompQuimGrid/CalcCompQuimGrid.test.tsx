/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { CalcCompQuimGrid } from './CalcCompQuimGrid'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<CalcCompQuimGrid />, div)
  ReactDOM.unmountComponentAtNode(div)
})
