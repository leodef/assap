import React from 'react'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core/styles'
import {
  Grid
} from '@material-ui/core'
import { ElementoGridCard } from './CalcCompQuimCard/ElementoGridCard/ElementoGridCard'
import { SubstanciaGridCard } from './CalcCompQuimCard/SubstanciaGridCard/SubstanciaGridCard'
import { ComposicaoSubstanciaGridCard } from './CalcCompQuimCard/ComposicaoSubstanciaGridCard/ComposicaoSubstanciaGridCard'
import './CalcCompQuimGrid.scss'

const gridUseStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    }
  })
)
/** Grandeza
 * Area calcCompQuimistrativa do painel de controle
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com area calcCompQuimistrativa do painel de controle
 */
export const CalcCompQuimGrid = React.memo((props: any) => {
  CalcCompQuimGrid.displayName = 'CalcCompQuimGrid'
  const classes = gridUseStyles()
  return (
    <Grid container justify="center" className={classes.root} spacing={4}>
      { /* EL - Elemento */ }
      <Grid item>
        <ElementoGridCard />
      </Grid>
      { /* SB - Substancia */ }
      <Grid item>
        <SubstanciaGridCard />
      </Grid>
      { /* CS - ComposicaoSubstancia */ }
      <Grid item>
        <ComposicaoSubstanciaGridCard />
      </Grid>
    </Grid>
  )
})
