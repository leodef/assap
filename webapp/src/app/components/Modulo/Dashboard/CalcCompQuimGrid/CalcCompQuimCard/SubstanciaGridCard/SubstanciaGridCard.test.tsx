/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { SubstanciaGridCard } from './SubstanciaGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<SubstanciaGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
