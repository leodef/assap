import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/CalcCompQuim/ComposicaoSubstancia'
import { ComposicaoSubstancia } from '../../../../../../components/Modulo/CalcCompQuim/ComposicaoSubstancia/ComposicaoSubstancia'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './ComposicaoSubstanciaGridCard.scss'

export const ComposicaoSubstanciaGridCard = React.memo((props: any) => {
  ComposicaoSubstanciaGridCard.displayName = 'ComposicaoSubstanciaGridCard'
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.calcCompQuim.composicaoSubstancia.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'composicao-substancia'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="CS"
    title="Composições de substancias"
    subheader="Gerenciamento de Composições de Substancias"
    content="Gerenciamento de Composições de Substancias"
    to="/calc-comp-quim/composicao-substancia"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <ComposicaoSubstancia collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
})
