import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/CalcCompQuim/Substancia'
import { Substancia } from '../../../../../../components/Modulo/CalcCompQuim/Substancia/Substancia'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './SubstanciaGridCard.scss'

export const SubstanciaGridCard = React.memo((props: any) => {
  SubstanciaGridCard.displayName = 'SubstanciaGridCard'
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.calcCompQuim.substancia.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'substancia'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="GR"
    title="Substancias"
    subheader="Gerenciamento de Substância"
    content="Gerenciamento de Substância"
    to="/calc-comp-quim/substancia"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Substancia collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
})
