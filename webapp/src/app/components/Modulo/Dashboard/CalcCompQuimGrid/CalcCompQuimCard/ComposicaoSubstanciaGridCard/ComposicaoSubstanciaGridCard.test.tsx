/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ComposicaoSubstanciaGridCard } from './ComposicaoSubstanciaGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ComposicaoSubstanciaGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
