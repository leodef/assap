/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ElementoGridCard } from './ElementoGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ElementoGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
