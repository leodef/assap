import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/API/CalcCompQuim/Elemento'
import { Elemento } from '../../../../../../components/Modulo/CalcCompQuim/Elemento/Elemento'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './ElementoGridCard.scss'

export const ElementoGridCard = React.memo((props: any) => {
  ElementoGridCard.displayName = 'ElementoGridCard'
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.calcCompQuim.elemento.crud, [])
  const actions = React.useMemo(() => { return { submit: true, toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true } }, [])
  const prefix = 'elemento'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="EL"
    title="Elementos"
    subheader="Gerenciamento de Elementos"
    content="Gerenciamento de Elementos"
    to="/calc-comp-quim/elemento"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Elemento collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
})
