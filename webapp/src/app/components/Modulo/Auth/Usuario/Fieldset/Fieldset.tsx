import React,
{
  useCallback,
  useEffect,
  useMemo
} from "react";
import { useField } from "formik";
import {
  Grid
} from "@material-ui/core";
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  SelectInput,
  TextField,
} from "../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField";
import {
  TIPO_USUARIO_LABEL
} from "../../../../../enums/tipo-usuario";
import "./Fieldset.scss";

/**
 * Area de inputs para TipoContatoInfo
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para TipoContatoInfo
 */
export const Fieldset = (props: any) => {
  // const classes = useStyles();
  const { disabled, labels, fixed, tipoOptions } = useMemo(() => props || {}, [
    props,
  ]);

  const fields = useMemo(() => {
    return {
      usuario: {
        name: getFieldName("usuario", props),
        label: labels && labels.usuario ? labels.usuario : "Nome de acesso",
      },
      senha: {
        name: getFieldName("senha", props),
        label: labels && labels.senha ? labels.senha : "Senha",
      },
      email: {
        name: getFieldName("email", props),
        label: labels && labels.email ? labels.email : "Email",
      },
      nome: {
        name: getFieldName("nome", props),
        label: labels && labels.nome ? labels.nome : "Nome",
      },
      tipo: {
        name: getFieldName("tipo", props),
        label: labels && labels.tipo ? labels.tipo : "Tipo do usuário",
      },
      status: {
        name: getFieldName("status", props),
        label: labels && labels.status ? labels.status : "Situação do usuário",
      },
    };
  }, [props, labels]);

  const [, usuarioMeta, usuarioHelpers] = useField(fields.usuario.name);
  const usuario = useMemo(() => usuarioMeta.value, [usuarioMeta]);
  const setUsuario = useCallback(
    (value: any) => usuarioHelpers.setValue(value),
    [usuarioHelpers]
  );

  const [, senhaMeta, senhaHelpers] = useField(fields.senha.name);
  const senha = useMemo(() => senhaMeta.value, [senhaMeta]);
  const setSenha = useCallback((value: any) => senhaHelpers.setValue(value), [
    senhaHelpers,
  ]);

  const [, emailMeta, emailHelpers] = useField(fields.email.name);
  const email = useMemo(() => emailMeta.value, [emailMeta]);
  const setEmail = useCallback((value: any) => emailHelpers.setValue(value), [
    emailHelpers,
  ]);

  const [, nomeMeta, nomeHelpers] = useField(fields.nome.name);
  const nome = useMemo(() => nomeMeta.value, [nomeMeta]);
  const setNome = useCallback((value: any) => nomeHelpers.setValue(value), [
    nomeHelpers,
  ]);

  const [, tipoMeta, tipoHelpers] = useField(fields.tipo.name);
  const tipo = useMemo(() => tipoMeta.value, [tipoMeta]);
  const setTipo = useCallback((value: any) => tipoHelpers.setValue(value), [
    tipoHelpers,
  ]);

  const [, statusMeta, statusHelpers] = useField(fields.status.name);
  const status = useMemo(() => statusMeta.value, [statusMeta]);
  const setStatus = useCallback((value: any) => statusHelpers.setValue(value), [
    statusHelpers,
  ]);

  useEffect(() => {
    if(fixed && fixed.usuario && fixed.usuario !== usuario) {
      setUsuario(fixed.usuario)
    }
    if(fixed && fixed.senha && fixed.senha !== senha){
      setSenha(fixed.senha)
    }
    if(fixed && fixed.email && fixed.email !== email){
      setEmail(fixed.email)
    }
    if(fixed && fixed.nome && fixed.nome !== nome){
      setNome(fixed.nome)
    }
    if(fixed && fixed.tipo && fixed.tipo !== tipo){
      setTipo(fixed.tipo)
    }
    if(fixed && fixed.status && fixed.status !== status){
      setStatus(fixed.status)
    }
  }, [
    fixed,
    usuario,
    setUsuario,
    senha,
    setSenha,
    email,
    setEmail,
    nome,
    setNome,
    tipo,
    setTipo,
    status,
    setStatus
  ]);

  const usuarioInput = useMemo(
    () =>
      fixed && fixed.usuario ? (
        <TextField
          type="hidden"
          name={fields.usuario.name}
          value={fixed.usuario}
        />
      ) : (
        <Grid item xs={12} sm={6}>
          <TextField
            name={fields.usuario.name}
            type="text"
            label={fields.usuario.label}
            disabled={disabled}
          />
        </Grid>
      ),
    [fixed, fields, disabled]
  );

  const senhaInput = useMemo(
    () =>
      fixed && fixed.senha ? (
        <TextField type="hidden" name={fields.senha.name} value={fixed.senha} />
      ) : (
        <Grid item xs={12} sm={6}>
          <TextField
            name={fields.senha.name}
            type="password"
            label={fields.senha.label}
            disabled={disabled}
          />
        </Grid>
      ),
    [fixed, fields, disabled]
  );

  const nomeInput = useMemo(
    () =>
      fixed && fixed.nome ? (
        <TextField type="hidden" name={fields.nome.name} value={fixed.nome} />
      ) : (
        <Grid item xs={12} sm={6}>
          <TextField
            name={fields.nome.name}
            type="text"
            label={fields.nome.label}
            disabled={disabled}
          />
        </Grid>
      ),
    [fixed, fields, disabled]
  );

  const emailInput = useMemo(
    () =>
      fixed && fixed.email ? (
        <TextField type="hidden" name={fields.email.name} value={fixed.email} />
      ) : (
        <Grid item xs={12} sm={6}>
          <TextField
            name={fields.email.name}
            type="text"
            label={fields.email.label}
            disabled={disabled}
          />
        </Grid>
      ),
    [fixed, fields, disabled]
  );

  const tipoInput = useMemo(
    () =>
      fixed && fixed.tipo ? (
        <TextField type="hidden" name={fields.tipo.name} value={fixed.tipo} />
      ) : (
        <Grid item xs={12} sm={6}>
          <SelectInput
            disabled={disabled}
            name={fields.tipo.name}
            label={fields.tipo.label}
            enumOptions={tipoOptions || TIPO_USUARIO_LABEL}
            fullWidth
          />
        </Grid>
      ),
    [fixed, fields, disabled, tipoOptions]
  );

  const statusInput = useMemo(
    () =>
      fixed && fixed.status ? (
        <TextField
          type="hidden"
          name={fields.status.name}
          value={fixed.status}
        />
      ) : (
        <Grid item xs={12} sm={6}>
          <TextField
            name={fields.status.name}
            type="text"
            label={fields.status.label}
            disabled={disabled}
          />
        </Grid>
      ),
    [fixed, fields, disabled]
  );

  return (
    <Grid container spacing={2}>
      {/* usuario */}
      {usuarioInput}

      {/* senha */}
      {senhaInput}

      {/* nome */}
      {nomeInput}

      {/* email */}
      {emailInput}

      {/* tipo */}
      {tipoInput}

      {/* status */}
      {statusInput}
    </Grid>
  );
};
