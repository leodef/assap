import React from 'react'
import {
  createStyles,
  makeStyles,
  Theme,
  Typography
} from '@material-ui/core'
import './Resume.scss'

/**
 * Visualização de item do tipo Funcionário
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Funcionário
 */
export const Resume = (props: any) => {
  const classes = useStyles()
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
    <strong className={classes.resumeLabel}>Nome:</strong>{item.nome}
    </Typography>
    <Typography variant="body2" component="p">
      <strong className={classes.resumeLabel}>Identificação:</strong>{item.identificacao}
    </Typography>
    { item.posicoes && item.posicoes.length > 0 ? (<Typography variant="body2" component="p">
      <strong className={classes.resumeLabel}>Posições:</strong>{
      item.posicoes.join(', ')
    }</Typography>) : null}
    {item.filiais && item.filiais.length > 0 ? (<Typography variant="body2" component="p">
      <strong className={classes.resumeLabel}>Filiais:</strong> {
        item.filiais.map((filial: any) => filial ? filial.titulo : '').join(', ')
      }</Typography>) : null}
    {item.empresa ? (<Typography variant="body2" component="p">
      <strong className={classes.resumeLabel}>Empresa:</strong>{
       item.empresa.nomeFantasia || item.empresa.razaoSocial
      }</Typography>) : null}
  </React.Fragment>)
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    resumeLabel: {
      marginRight: '0.35em',
    }
  })
)
