import React,
{
  useCallback,
  useEffect,
  useMemo,
  useState
} from 'react'
import {
  useSelector
} from 'react-redux'
import {
  useField
} from 'formik'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Checkbox,
  createStyles,
  Divider,
  FormControlLabel,
  Grid,
  makeStyles,
  TextField as MuiTextField,
  Theme
} from '@material-ui/core'
import {
  ExpandMore as ExpandMoreIcon
} from "@material-ui/icons";
import Autocomplete from '@material-ui/lab/Autocomplete';
import {
  getState
} from '../../../../../types/EmpresaUsuario'
import {
  initialValues as usuarioInitialValues
} from '../../../../../types/API/Auth/Usuario'
import {
  getFieldName,
  TextField
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  Autocomplete as AutocompleteFilial
} from '../../Filial/Autocomplete/Autocomplete'
import {
  Fieldset as FieldsetUsuario
} from '../../../Auth/Usuario/Fieldset/Fieldset'
import {
  TIPO_USUARIO,
  TIPO_USUARIO_FUNCIONARIO_LABEL
} from '../../../../../enums/tipo-usuario';
import './Fieldset.scss'

/**
 * Area de inputs para Funcionário
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Funcionário
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  const classes = useStyles();

  
  // #region Expanded
  const [
    usuarioAccordionExpanded,
    setUsuarioAccordionExpanded,
  ] = useState(false as boolean);
  // #endregion Expanded

  // #region Item Form
  const empresaUsuario = useSelector(state => getState(state).item || '' )
  const { disabled } = useMemo(() => props, [props])
  const fields = useMemo(() => {
    return {
      nome: {
        name: getFieldName('nome', props),
        label: 'Nome'
      },
      identificacao: {
        name: getFieldName('identificacao', props),
        label: 'Identificação'
      },
      posicoes: {
        name: getFieldName('posicoes', props),
        label: 'Posições'
      },
      filiais: {
        name: getFieldName('filiais', props),
        label: 'Filiais'
      },
      empresa: {
        name: getFieldName('empresa', props),
        label: 'Empresa'
      },
      criarUsuario: {
        name: getFieldName('criarUsuario', props),
        label: 'Criar '
      },
      usuario: {
        name: getFieldName('usuario', props),
        label: 'Usuário'
      }
    }
  }, [props])
  // #endregion Item Form
  
  // #region Empresa
  const [, empresaMeta, empresaHelpers] = useField(
    fields.empresa.name
  )
  const setEmpresa = useCallback(
    (value: any) => empresaHelpers.setValue(value),
    [empresaHelpers]
  )
  const empresa = useMemo(
    () => empresaMeta.value || [],
    [empresaMeta.value]
  )
  useEffect(() => {
    if(!empresa || (empresaUsuario && empresaUsuario._id !== empresa._id )){
    setEmpresa(empresaUsuario)
    }
  }, [empresa, empresaUsuario, setEmpresa])
  // #endregion Empresa

  // #region Filiais
  const [, filiaisMeta, filiaisHelpers] = useField(
    fields.filiais.name
  )
  const setFiliais = useCallback(
    (value: any) => filiaisHelpers.setValue(value),
    [filiaisHelpers]
  )
  const filiais = useMemo(
    () => filiaisMeta.value || [],
    [filiaisMeta.value]
  )
  // #endregion Filiais

  // #region Posições
  const [, posicoesMeta, posicoesHelpers] = useField(
    fields.posicoes.name
  )
  const setPosicoes = useCallback(
    (value: any) => posicoesHelpers.setValue(value),
    [posicoesHelpers]
  )
  const posicoes = useMemo(
    () => posicoesMeta.value || [],
    [posicoesMeta.value]
  )
  // #endregion Posições

  // #region Nome
  const [, nomeMeta] = useField(
    fields.nome.name
  )
  const nome = useMemo(
    () => nomeMeta.value || [],
    [nomeMeta.value]
  )
  // #endregion Nome  

  // #region Usuario
  const [, criarUsuarioMeta, criarUsuarioHelpers] = useField(
    fields.criarUsuario.name
  );
  const [, usuarioMeta, usuarioHelpers] = useField(
    fields.usuario.name
  );
  const setUsuario = useCallback(
      (value: any) => usuarioHelpers.setValue(value),
      [usuarioHelpers]
    );
  const usuario = useMemo(() => Boolean(usuarioMeta.value), [
    usuarioMeta.value,
  ]);
  const setCriarUsuario = useCallback(
      (value: any) => {
        const val = Boolean(value);
        setUsuario(
          usuarioInitialValues
        )
        criarUsuarioHelpers.setValue(val);
        setUsuarioAccordionExpanded(val);
      },
      [
        criarUsuarioHelpers,
        setUsuarioAccordionExpanded,
        setUsuario]
    );
  const criarUsuario = useMemo(() => Boolean(criarUsuarioMeta.value), [
    criarUsuarioMeta.value,
  ]);
  const usuarioDisabled = useMemo(() => !criarUsuario, [
    criarUsuario,
  ]);
  const usuarioLabel = useMemo(
    () =>
      !criarUsuario
        ? "Criar usuário no sistema"
        : "Usuário no sistema",
    [criarUsuario]
  )
  const usuarioFixed = useMemo(() => ({
    //usuario: 'String',
    //senha: 'String',
    //email: 'String',
    nome: nome,
    tipo:  TIPO_USUARIO.ADMIN_EMPRESA,
    status: 'ativo'

  }), [nome])
  const usuarioFieldset = useMemo(() =>
  criarUsuario
    ? (<FieldsetUsuario
      {...props}
      name={fields.usuario.name}
      item={usuario}
      tipoOptions={TIPO_USUARIO_FUNCIONARIO_LABEL}
      fixed={usuarioFixed}
    />) : null, [
      props,
      criarUsuario,
      fields,
      usuario,
      usuarioFixed
    ])

  
  // #endregion Usuario
  
  return (
    <Grid container spacing={2} className={classes.container}>
      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.nome.name}
          type="text"
          label={fields.nome.label}
          disabled={disabled}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.identificacao.name}
          type="text"
          label={fields.identificacao.label}
          disabled={disabled}
        />
      </Grid>
      <Grid item xs={12}>
        <Divider variant="middle" />
      </Grid>
      <Grid
        item
        container
        xs={12}>
         <Autocomplete
            id="funcionario-autocomplete-posicoes"
            freeSolo
            fullWidth
            multiple
            onChange={(event: any, value: any) => setPosicoes(value)}
            value={posicoes}
            defaultValue={[]}
            options={[]}
            renderInput={(params) => (
              <MuiTextField
                {...params}
                value={''}
                variant="outlined"
                label="Posição"
                placeholder="Posição" />
            )}
      />
      </Grid>
      
      <Grid item xs={12}>
        <Divider variant="middle" />
      </Grid>

      <Grid item xs={12}>
        <AutocompleteFilial
          id="funcionario-autocomplete-filial"
          fullWidth
          multiple
          defaultValue={[]}
          filterSelectedOptions
          onChange={setFiliais}
          value={filiais}
          />
      </Grid>

      <Grid item xs={12}>
        <Divider variant="middle" />
      </Grid>

      <Grid item xs={12}>

      <Accordion
          expanded={usuarioAccordionExpanded}
          onChange={(event: any, isExpanded: boolean) =>
            setUsuarioAccordionExpanded(
              !usuarioAccordionExpanded && !usuarioDisabled
            )
          }
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label={usuarioLabel}
            aria-controls="entrega-config-step-accord-usuario-content"
            id="entrega-config-step-accord-usuario-header"
            IconButtonProps={{ disabled: usuarioDisabled }}
          >
            <FormControlLabel
              aria-label={usuarioLabel}
              onClick={(event: any) => event.stopPropagation()}
              onFocus={(event: any) => event.stopPropagation()}
              onChange={(event: any, checked: boolean) =>
                setCriarUsuario(checked)}
              checked={criarUsuario}
              control={<Checkbox />}
              label={usuarioLabel}
            />
          </AccordionSummary>
          <AccordionDetails>
            {usuarioFieldset}
          </AccordionDetails>
        </Accordion>
      </Grid>

    </Grid>
  )
})

// Chips Container
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      position: 'relative',
      overflow: 'auto',
      maxHeight: 300
    }
    /*
      root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
      },
      chipContainer: {
        flexGrow: 1,
        flex: 1,
        display: 'flex',
        '& > *': {
          margin: theme.spacing(0.5)
        },
        padding: theme.spacing(0.5),
        justifyContent: 'center',
        flexWrap: 'wrap',
        height: 100,
        overflowY: 'auto'
      },
      input: {
        marginLeft: theme.spacing(1),
        flex: 1,
      },
      margin: {
        margin: theme.spacing(1),
      },
      iconButton: {
        padding: 10,
      },
      divider: {
        borderBottom: '0.1em solid black',
        padding: '0.5em'
      }
    */
  }),
);
