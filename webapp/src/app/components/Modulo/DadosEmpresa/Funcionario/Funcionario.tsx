import
React, {
  useMemo, useCallback
} from 'react'
import {
  Assignment as AssignmentIcon
} from '@material-ui/icons'
import {
  CollectionTypeEnum
} from '../../../../types/Crud'
import { Crud } from '../../../Shared/Crud/Crud'
import { TablePropsField } from '../../../Shared/Collection/Table/Table'
import { Resume } from './Resume/Resume'
import { Fieldset } from './Fieldset/Fieldset'
import {
  FuncionarioSchema,
  initialValues
} from '../../../../types/API/Funcionario'
import './Funcionario.scss'

export const Funcionario = (props: any) => {
  const { collection, form, show, remove } = props
  const { type } = (collection || {})
  const fields = useMemo(() => [
    new TablePropsField('nome', 'Nome'),
    new TablePropsField('identificacao', 'Identificação')
  ], [])
 
  const ShowBody = useCallback((props: any) => {
    return (<Resume
      {...props} />)
  }, [])

  const FormBody = useCallback((props: any) => {
    return (<Fieldset
      {...props} />)
  }, [])

  const tableConfig = useMemo(() => {
    return {
      label: 'Funcionário',
      align: 'center',
      actionsLabel: 'Ações',
      fields
    }
  }, [fields])
  const listConfig = useMemo(() => {
    return {
      dense: true,
      subheader: 'Funcionário',
      fields,
      getListItem: (value: any, index: number) => {
        return {
          primary: (value.nome ? value.nome.toString() : ''),
          secondary: (value.identificacao ? value.identificacao.toString() : ''),
          avatar: (value.nome ? value.nome.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
        }
      }
    }
  }, [fields])
  const config = (type === CollectionTypeEnum.LIST) ? listConfig : tableConfig
  const params = useMemo(() => {
    const params = {
      /* child: {
        // eslint-disable-next-line react/display-name
        body: (props: any) => (<UnidadeMedida {...props} />)
      }, */
      collection: {
        type,
        config,
        // resolve: 'FRONT' // 'BACK'
        ...(collection || {})
      },
      show: {
        title: 'Funcionário',
        // eslint-disable-next-line react/display-name
        body: ShowBody,
        ...(show || {})
      },
      form: {
        title: 'Funcionário',
        // eslint-disable-next-line react/display-name
        body: FormBody,
        initialValues,
        schema: FuncionarioSchema,
        ...(form || [])
      },
      remove: {
        title: 'Deseja realmente remover a funcionário ?',
        ...(remove || {})
      }
    }
    return params
  }, [
    collection,
    config,
    form,
    remove,
    show,
    type,
    FormBody,
    ShowBody
  ])
  return <Crud {...params} />
}
