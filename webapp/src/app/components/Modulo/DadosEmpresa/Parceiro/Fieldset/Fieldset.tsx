import
  React,
  {
    useCallback,
    useMemo,
    useState
  }
from "react";
import { useField } from "formik";
import {
  BottomNavigation,
  BottomNavigationAction,
  createStyles,
  Grid,
  makeStyles,
  Theme,
} from "@material-ui/core";
import {
  Contacts as ContactsIcon,
  Business as BusinessIcon
} from '@material-ui/icons';
import {
  TIPO_PESSOA_LABEL
} from "../../../../../enums/tipo-pessoa";
import {
  getFieldName,
  SelectInput,
  TextField,
  useStyles as fieldsUseStyles,
} from "../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField";
import {
  ArrayFieldset as ArrayFieldsetContatoParceiro
} from "../../ContatoParceiro/ArrayFieldset/ArrayFieldset";
import {
  ArrayFieldset as ArrayFieldsetFilial
} from "../../Filial/ArrayFieldset/ArrayFieldset";
import "./Fieldset.scss";
import { Filial } from "../../../../../types/API/Filial";

/**
 * Area de inputs para Empresa
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Empresa
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = "Fieldset";
  const classes = useStyles();
  const fieldsClasses = fieldsUseStyles();
  const { disabled, item } = useMemo(() => props, [props]);
  const modalHistory = useMemo(
    () => ([
      ...(props.modalHistory || []),
      "Parceiro"
    ]), [props.modalHistory])
    const [tab, setTab] = useState(0);
  const desc = useMemo(() => (item ? item.desc : null), [item]);
  const contatosItem = useMemo(() => (item ? item.contatos : []), [item]);
  const filiaisItem = useMemo(() => (item ? item.filiais : []), [item]);
  const fields = useMemo(() => {
    return {
      titulo: {
        name: getFieldName("titulo", props),
        label: "Título",
      },
      desc: {
        name: getFieldName("desc", props),
        label: "Descrição",
      },
      identificacao: {
        name: getFieldName("identificacao", props),
        label: "Identificação",
      },
      tipoPessoa: {
        name: getFieldName("tipoPessoa", props),
        label: "Tipo de pessoa",
      },
      filiais: {
        name: getFieldName("filiais", props),
        label: "Filiais",
      },
      contatos: {
        name: getFieldName("contatos", props),
        label: "Contatos",
      },
    };
  }, [props]);

  const [, filiaisMeta] = useField(
    fields.filiais.name
  );
  const filiais = useMemo(() => filiaisMeta.value || [], [filiaisMeta]);

  
  const [, contatosMeta, contatosHelpers] = useField(
    fields.contatos.name
  );
  const contatos = useMemo(
    () => contatosMeta.value || [],
  [contatosMeta]);
  const setContatos = useCallback(
    (val: any) => contatosHelpers.setValue(val),
    [contatosHelpers]
  )
  
  const onFilialChange = useCallback(
    (filial) => {
      if(contatos && filial) {
        setContatos(contatos.map( (obj: any) => {
          if(obj && obj.filial){
            obj.filial = Filial.compare(filial, obj.filial) 
              ? filial
              : obj.filial
          }
          return obj
        }))
      }
    },
    [contatos, setContatos]
  )
  
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={4}>
        <TextField
          name={fields.titulo.name}
          type="text"
          label={fields.titulo.label}
          disabled={disabled}
        />
      </Grid>

      <Grid item xs={12} sm={4}>
        <TextField
          name={fields.identificacao.name}
          type="text"
          label={fields.identificacao.label}
          disabled={disabled}
        />
      </Grid>

      <Grid item xs={12} sm={4}>
        <SelectInput
          disabled={disabled}
          name={fields.tipoPessoa.name}
          label={fields.tipoPessoa.label}
          enumOptions={TIPO_PESSOA_LABEL}
          fullWidth
        />
      </Grid>

      <Grid item xs={12}>
        <TextField
          name={fields.desc.name}
          type="text"
          label={fields.desc.label}
          disabled={disabled}
          value={desc}
          multiline
          className={fieldsClasses.multiline}
        />
      </Grid>

      <Grid
        item
        xs={12}
        className={tab === 0 ? classes.show : classes.hidden}>

            <ArrayFieldsetFilial
              modalHistory={modalHistory}
              name={fields.filiais.name}
              label={fields.filiais.label}
              disabled={disabled}
              item={filiaisItem}
              onFilialChange={onFilialChange}
              _temp
            />

      </Grid>

      <Grid
        item
        xs={12}
        className={tab === 1 ? classes.show : classes.hidden}>

            <ArrayFieldsetContatoParceiro
              modalHistory={modalHistory}
              name={fields.contatos.name}
              label={fields.contatos.label}
              disabled={disabled}
              item={contatosItem}
              filiais={filiais}
            />

      </Grid>
    
      <Grid item xs={12} >
      <BottomNavigation
        value={tab}
        onChange={(event: any, newValue: any) => setTab(newValue)}
        showLabels
        className={classes.bottomNavigation}
        >
          <BottomNavigationAction
            label="Filiais"
            icon={<BusinessIcon />} />
          <BottomNavigationAction
            label="Contatos"
            icon={<ContactsIcon />} />
        </BottomNavigation>
      </Grid>
    </Grid>
  );
});

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
      overflow: "auto",
      width: 500
    },
    container: {
      height: 300,
      width: '100%'
    },
    bottomNavigation: {
      width: "100%",
    },
    hidden: {
      display: 'none'
    },
    show: {
      display: 'block'
    }
  })
);
