import React, { useMemo } from "react";
import {
  Card,
  CardContent,
  createStyles,
  Grid,
  makeStyles,
  Theme,
  Typography,
} from "@material-ui/core";
import {
  ArrayResume as ArrayResumeContatoParceiro
} from "../../ContatoParceiro/ArrayResume/ArrayResume";
import "./Resume.scss";

/**
 * Visualização de item do tipo Parceiro
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Parceiro
 */
export const Resume = (props: any) => {
  const classes = useStyles();
  const { item } = useMemo(() => props, [props]);
  const filiais = useMemo(() => (item ? item.filiais : []), [item]);
  const contatos = useMemo(() => (item ? item.contatos : []), [item]);

  return (<Grid container>
    <Grid item xs={12} >
      <Typography variant="h5" component="h2">
        {item.titulo}
      </Typography>
      <Typography variant="body2" component="p">
        {item.desc}
      </Typography>
      <Typography variant="body2" component="p">
        <strong className={classes.resumeLabel}>Identificação:</strong>
        {item.identificacao}
        <strong className={classes.resumeLabel}>Tipo pessoa:</strong>
        {item.tipoPessoa}
      </Typography>
      {filiais && filiais.length > 0 ? (
        <Typography variant="body2" component="p">
          <strong className={classes.resumeLabel}>Filiais:</strong>{" "}
          {filiais
            .map((filial: any) => (filial ? filial.titulo : ""))
            .join(", ")}
        </Typography>
      ) : null}
      </Grid>
        <Grid item xs={12}>
          {contatos && contatos.length > 0 ? (
            <Card>
              <CardContent>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  gutterBottom
                >
                  Contatos
                </Typography>
                  <ArrayResumeContatoParceiro {...props} item={contatos}/>
                </CardContent>
            </Card>
          ) : null}
        </Grid>
      </Grid>
  )
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    resumeLabel: {
      marginRight: "0.35em",
      marginLeft: "0.35em",
    },
    title: {
      fontSize: 14,
    },
  })
);
