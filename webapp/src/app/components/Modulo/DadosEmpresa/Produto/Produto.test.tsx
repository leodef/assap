/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Produto } from './Produto'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Produto />, div)
  ReactDOM.unmountComponentAtNode(div)
})
