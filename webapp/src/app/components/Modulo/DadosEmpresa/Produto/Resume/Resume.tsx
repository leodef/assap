import React from "react";
import { createStyles, makeStyles, Theme, Typography } from "@material-ui/core";
import "./Resume.scss";

/**
 * Visualização de item do tipo Produto
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Produto
 */
export const Resume = (props: any) => {
  const classes = useStyles();
  const { item } = props;
  return (
    <React.Fragment>
      <Typography variant="h5" component="h2">
        {item.titulo}
      </Typography>
      <Typography variant="body2" component="p">
        {item.desc}
      </Typography>
      <Typography variant="body2" component="p">
        {item.tipo ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}>Tipo:</strong>
            {item.tipo}
          </React.Fragment>
        ) : null}

        {item.marca ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}>Marca:</strong>
            {item.marca}
          </React.Fragment>
        ) : null}

        {item.densidade ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}>Densidade:</strong>
            {item.densidade} <i>Kg/L</i>
          </React.Fragment>
        ) : null}
      </Typography>
    </React.Fragment>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    resumeLabel: {
      marginRight: "0.35em",
      marginLeft: "0.35em",
    },
  })
);
