import React, { useMemo } from 'react'
import {
  Grid, InputAdornment
} from '@material-ui/core'
import {
  getFieldName,
  TextField,
  useStyles as fieldsUseStyles
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import './Fieldset.scss'

/**
 * Area de inputs para Produto
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Produto
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  const fieldsClasses = fieldsUseStyles()
  const { disabled, item } = props
  const desc = useMemo( () => item ? item.desc : null, [item])

  const fields = useMemo(() => {
    return {
      titulo: {
        name: getFieldName('titulo', props),
        label: 'Título'
      },
      desc: {
        name: getFieldName('desc', props),
        label: 'Descrição'
      },
      tipo: {
        name: getFieldName('tipo', props),
        label: 'Tipo'
      },
      marca: {
        name: getFieldName('marca', props),
        label: 'Marca'
      },
      densidade: {
        name: getFieldName('densidade', props),
        label: 'Densidade'
      }
    }
  }, [props])

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.titulo.name}
          type="text"
          label={fields.titulo.label}
          disabled={disabled}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.tipo.name}
          type="text"
          label={fields.tipo.label}
          disabled={disabled}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.marca.name}
          type="text"
          label={fields.marca.label}
          disabled={disabled}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.densidade.name}
          type="number"
          label={fields.densidade.label}
          disabled={disabled}
          InputProps={{
            startAdornment: <InputAdornment position="start">
              <i>Kg/L</i>
              </InputAdornment>,
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          name={fields.desc.name}
          type="text"
          label={fields.desc.label}
          disabled={disabled}
          value={desc}
          multiline
          className={fieldsClasses.multiline}
        />
      </Grid>
    </Grid>
  )
})
