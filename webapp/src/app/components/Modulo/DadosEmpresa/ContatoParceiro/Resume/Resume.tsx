import React, { useMemo } from "react";
import { createStyles, makeStyles, Theme, Typography } from "@material-ui/core";
import { ArrayResume as ArrayResumeContatoInfo } from "../../../ContatoInfo/ArrayResume/ArrayResume";
import "./Resume.scss";

/**
 * Visualização de item do tipo Parceiro
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Parceiro
 */
export const Resume = (props: any) => {
  const classes = useStyles();
  const { item } = useMemo(() => props, [props]);
  const contato = useMemo(() => (item ? item.contato : null), [item]);
  return (
    <React.Fragment>
      <Typography variant="body2" component="p">
        <strong className={classes.resumeLabel}>
          {
          item.contato && item.contato.titulo
          ? item.contato.titulo
          : null
          }
        </strong>
        {item.filial ? (
          <React.Fragment>
            (<strong className={classes.resumeLabel}>Filial:</strong>
            {item.filial.titulo})
          </React.Fragment>
        ) : null}
      </Typography>
      {item.contato && item.contato.desc ? (
        <Typography variant="body2" component="p">
          {item.contato.desc}
        </Typography>
      ) : null}
      {contato.infos && contato.infos.length > 0 ? (
        <ArrayResumeContatoInfo item={contato.infos} />
      ) : null}
    </React.Fragment>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    resumeLabel: {
      marginRight: "0.35em",
      marginLeft: "0.35em",
    },
  })
);
