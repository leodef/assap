import React, { useMemo } from "react";
import {
  Button,
  createStyles,
  makeStyles,
  MobileStepper,
  Theme,
  useTheme,
} from "@material-ui/core";
import {
  KeyboardArrowLeft, KeyboardArrowRight
} from "@material-ui/icons";
import {
  Resume
} from "../Resume/Resume";
import "./ArrayResume.scss";

/**
 * Visualização de item do tipo Empresa
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Empresa
 */
export const ArrayResume = (props: any) => {
  const { item } = useMemo(() => props, [props]);
  const classes = useStyles();
  const theme = useTheme();

  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = item.length;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  return (
    <div className={classes.root}>
      <Resume item={item[activeStep]} />
      <MobileStepper
        steps={maxSteps}
        position="static"
        variant="text"
        activeStep={activeStep}
        nextButton={
          <Button
            size="small"
            onClick={handleNext}
            disabled={activeStep === maxSteps - 1}
          >
            Anterior
            {theme.direction === "rtl" ? (
              <KeyboardArrowLeft />
            ) : (
              <KeyboardArrowRight />
            )}
          </Button>
        }
        backButton={
          <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
            {theme.direction === "rtl" ? (
              <KeyboardArrowRight />
            ) : (
              <KeyboardArrowLeft />
            )}
            Proximo
          </Button>
        }
      />
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    header: {
      display: "flex",
      alignItems: "center",
      height: 50,
      paddingLeft: theme.spacing(4),
      backgroundColor: theme.palette.background.default,
    }
  })
);
