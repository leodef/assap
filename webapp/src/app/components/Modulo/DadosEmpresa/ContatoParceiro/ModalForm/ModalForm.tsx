import React,
{
  useCallback, useMemo
} from 'react'
import {
  Modal,
  makeStyles,
  Card,
  CardContent,
  CardActions,
  Button,
  createStyles,
  // eslint-disable-next-line no-unused-vars
  Theme
} from '@material-ui/core'
import {
  Send as SendIcon,
  ArrowBack as ArrowBackIcon
} from '@material-ui/icons'
// eslint-disable-next-line no-unused-vars
import {
  withFormik,
  // eslint-disable-next-line no-unused-vars
  FormikHelpers
} from 'formik'
import _ from 'lodash'
import { Fieldset } from '../Fieldset/Fieldset'
import {
  initialValues,
  ContatoParceiroSchema as formSchema
} from '../../../../../types/API/Parceiro/ContatoParceiro'
import './ModalForm.scss'
import { ModalTitle } from '../../../../Shared/Utils/ModalBreadcrumbs/ModalTitle'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 300, // 275
      minHeight: 300
    },
    title: {
      fontSize: 14
    },
    pos: {
      marginBottom: 12
    },
    paper: {
      position: 'absolute',
      minWidth: 200,
      minHeight: 200,
      // boxShadow: theme.shadows[5],
      // padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },
    button: {
      margin: theme.spacing(0.5, 0)
    }
  })
)

/**
 * Componente ModalForm
 * @param {any} props Propriedades
 * @return {React.Component} Componente ModalForm
 */
export const ModalForm = (props: any) => {
  const classes = useStyles()
  const {
    save,
    isOpen,
    setOpen,
    setValue
  } = useMemo( () => props, [props])
  const onClose = useCallback(() => {
    setOpen(false)
    setValue(null)
  }, [setOpen, setValue])
  const fSave = useCallback((values: any) => {
    save(values)
    setOpen(false)
    setValue(null)
  }, [save, setOpen, setValue])
  return (
    <Modal
      open={isOpen}
      onClose={onClose}
      aria-labelledby="Contato"
      aria-describedby="Contato">
      <div className={classes.paper}>
        <Form
          {...props}
          save={fSave}/>
      </div>
    </Modal>)
}

/**
 * Componente ModalForm
 * @param {any} props Propriedades
 * @return {React.Component} Componente ModalForm
 */
export const Form = (props: any) => {
  // salvar
  const { save, value } = useMemo(() => props, [props])
  const WithFormik = withFormik({
    mapPropsToValues: () => _.defaults(value, initialValues),
    validationSchema: formSchema,
    handleSubmit: (values: any, formikHelpers: FormikHelpers<any>) => {
      const { setSubmitting } = formikHelpers
      setSubmitting(false)
      save(values)
    }
  })(FormBody)
  return (<WithFormik
    {...props}
    name={null} />)
}

/**
 * Componente ModalForm
 * @param {any} props Propriedades
 * @return {React.Component} Componente ModalForm
 */
export const FormBody = (props: any) => {
  const classes = useStyles()
  const {
    value,
    back,
    modalHistory
  } = useMemo(() => props, [props])
  return (<form onSubmit={props.handleSubmit}>
    <Card className={classes.root}>
      <CardContent>
      <ModalTitle
          modalHistory={modalHistory}
          title="Contato do parceiro" />
        <Fieldset
          item={value}
          {...props} />
      </CardContent>
      <CardActions>
        <Button
          size="small"
          type="submit"
          variant="contained"
          color="primary"
          startIcon={<SendIcon />}>
            Salvar
        </Button>
        {back ? (<Button
          size="small"
          variant="contained"
          color="primary"
          startIcon={<ArrowBackIcon />}
          onClick={(event: any) => back()}>
            Voltar
        </Button>) : null}
      </CardActions>
    </Card>
  </form>)
}
