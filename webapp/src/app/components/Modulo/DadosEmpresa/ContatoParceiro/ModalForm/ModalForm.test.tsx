/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ModalForm } from './ModalForm'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ModalForm />, div)
  ReactDOM.unmountComponentAtNode(div)
})
