import React, { useCallback, useMemo } from 'react'
import { useField } from 'formik'
import {
  Grid
} from '@material-ui/core'
import {
  getFieldName
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  Autocomplete as AutocompleteFilial
} from '../../Filial/Autocomplete/Autocomplete'
import {
  Fieldset as FieldsetContato
} from '../../../Contato/Fieldset/Fieldset'
import './Fieldset.scss'

/**
 * Area de inputs para ContatoParceiro
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para ContatoParceiro
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  const {
    disabled,
    item,
    filiais
  } = useMemo( () => props, [props])

  const contato = useMemo(
    () => item ? item.contato : null,
    [item])

  const fields = useMemo(() => {
    return {
      filial: {
        name: getFieldName('filial', props),
        label: 'Filial'
      },
      contato: {
        name: getFieldName('contato', props),
        label: 'Contato'
      }
    }
  }, [props])

  const modalHistory = useMemo(
    () => ([
      ...(props.modalHistory || []),
      "Contato parceiro"
    ]), [props.modalHistory])

  const [, filialMeta, filialHelpers] = useField(
    fields.filial.name
  )
  
  const setFilial = useCallback(
    (value: any) => filialHelpers.setValue(value),
    [filialHelpers]
  )

  const filial = useMemo(
    () => filialMeta.value || null,
    [filialMeta]
  )
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <FieldsetContato
          modalHistory={modalHistory}
          name={fields.contato.name}
          label={fields.contato.label}
          disabled={disabled}
          item={contato}
        />
      </Grid>
      
      <Grid item xs={12}>
        <AutocompleteFilial
          id="funcionario-autocomplete-filial"
          fullWidth
          label="Filial do contato"
          onChange={setFilial}
          value={filial}
          options={filiais}
          />
      </Grid>
    </Grid>
  )
})
