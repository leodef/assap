import
React,
{
  useCallback,
  useMemo,
  useState
} from 'react'
import {
  useField
} from 'formik'
import {
  Container,
  createStyles,
  Divider,
  Fab,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  makeStyles,
  Menu,
  MenuItem,
  MenuProps,
  Theme,
  withStyles
} from '@material-ui/core'
import { Pagination } from '@material-ui/lab'
import {
  Add as AddIcon,
  Delete as DeleteIcon,
  MoreVert as MoreVertIcon,
  Edit as EditIcon
} from '@material-ui/icons'
// eslint-disable-next-line no-unused-vars
import {
  getFormName
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  ModalForm
} from '../ModalForm/ModalForm'
import './ArrayFieldset.scss'
/**
 * Componente ArrayFieldset
 * @param {any} props Propriedades
 * @return {React.Component} Componente ArrayFieldset
 */
export const ArrayFieldset = (props: any) => {
  const classes = useStyles();
  const { disabled, filiais } = useMemo(() => props, [props])
  const [value, setValue] = useState({} as any)
  const [itemIndex, setItemIndex] = useState(null as number | null)
  const [isOpen, setOpen] = useState(false as boolean)

  const fields = useMemo(() => {
    return {
      contatoParceiro: {
        name: getFormName(props),
        label: 'Contatos'
      }
    }
  }, [props])
  
  const [, contatoParceiroMeta, contatoParceiroHelpers] = useField(
    fields.contatoParceiro.name
  )
  const contatoParceiro = useMemo(() => (contatoParceiroMeta.value || []) as Array<any>, [contatoParceiroMeta])
  const back = useCallback(
    () => {
      setValue({})
      setOpen(false)
    },
    [setValue, setOpen]
  )
  
  const edit = useCallback(
    (value: any, index: number) => {
      setItemIndex(index)
      setValue(value)
      setOpen(true)
    },
    [setValue, setOpen]
  )

  const remove = useCallback((index: number) => {
    contatoParceiroHelpers.setValue(
      contatoParceiro.filter((val: any, ind: number) => ind !== index)
    )
  },[contatoParceiroHelpers, contatoParceiro ]
  )

  const create = useCallback(() => {
    setItemIndex(null)
    setValue(null)
    setOpen(true)
  }, []
  )

  const save = useCallback((value: any) => {
      if (itemIndex === null) {
        contatoParceiroHelpers.setValue([...contatoParceiro, value])
      } else {
        contatoParceiroHelpers.setValue(
          contatoParceiro.map((val: any, index: number) => 
          (index === itemIndex) ? value : val
          )
        )
      }
      setItemIndex(null)
    },
    [
      itemIndex,
      setItemIndex,
      contatoParceiroHelpers,
      contatoParceiro
    ]
  )

  //#region Pagination
  const [page, setPage] = useState(1 as number)
  const limit = 5
  const length = useMemo(
    () => (contatoParceiro.length || 0),
    [contatoParceiro])
  const pages = useMemo(
    () => Math.round(length / limit),
    [length, limit])
  const alterarPaginacao = useCallback(
    (event: React.ChangeEvent<unknown>, value: number) => {
      setPage(value)
  }, [setPage])
  const showPagination = useMemo(
    () => (pages > 1  && !disabled),
    [pages, disabled])

  return (
    <React.Fragment>
    <Container>
    <List
      dense
      aria-label="Lista de contatos do parceiro"
      className={classes.listContainer}>
      { contatoParceiro.map( (info: any, index: number) => (
        <React.Fragment  key={`contato_parceiro_array_fieldset_${index}`}>
          <ListItem dense button>
            <ListItemText
              primary={
                info.contato ? info.contato.titulo : null
              }
              secondary={
                info.filial ? info.filial.titulo : null
              } />
              { disabled ? null : (<ListItemActions
                index={index}
                item={info}
                edit={edit}
                remove={remove}
              />)}
          </ListItem>
          <Divider />
        </React.Fragment>)
      )}
    </List>
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="center" >
      <Grid item >
        { showPagination  ? 
          (<Pagination
            count={pages}
            page={page || 1}
            showFirstButton
            showLastButton
            onChange={alterarPaginacao} />) : null
        }
      </Grid>
    </Grid>
    <Fab
      color="primary"
      size="small"
      aria-label="Novo"
      onClick={() => create()}>
      <AddIcon />
    </Fab>
  </Container>
  <ModalForm
      {...props}
      filiais={filiais}
      back={back}
      save={save}
      value={value}
      setValue={setValue}
      isOpen={isOpen}
      setOpen={setOpen}
      />
  </React.Fragment>)
}

/**
 * Botões do item da lista genérica para coleção de items
 *   List.ListItem.ListItemActions
 * @param {item: ListPropsItem, index: number, config: ListConfig} props Propriedades
 * @return {React.Component} Componente com botões do item da lista genérica para coleção de items
 */
export const ListItemActions = (props: any) => {
  const { item, index, edit, remove } = useMemo(() => props, [props])
  // Menu control
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = useCallback(() => {
    setAnchorEl(null)
  }, [setAnchorEl])

  const editButton = useMemo(() => (
    <StyledMenuItem
      onClick={() => {
        handleClose();
        edit(item, index);
      }}>
      <ListItemIcon>
        <EditIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText primary="Editar" />
    </StyledMenuItem>), [handleClose, edit, item, index])

  const removeButton = useMemo(() => (
    <StyledMenuItem onClick={() => {
      handleClose();
      remove(index);
    }}>
      <ListItemIcon>
        <DeleteIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText primary="Remover" />
    </StyledMenuItem>), [handleClose, remove, index])

  return (
    <ListItemSecondaryAction>
      <div>
        <IconButton
          aria-label="more"
          aria-controls="long-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </IconButton>
        <StyledMenu
          id="contato-parceiro-array-fieldset-actions"
          anchorEl={anchorEl}
          keepMounted
          open={!!anchorEl}
          onClose={handleClose} >
          {editButton}
          {removeButton}
        </StyledMenu>
      </div>
    </ListItemSecondaryAction>
  )
}


/**
 * Menu customizado
 * @param {MenuProps} props Propriedades
 * @return {React.Component} Componente com menu customizado
 */
const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5'
  }
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center'
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center'
    }}
    {...props}
  />
))

/**
 * Fabrica de item de menu customizado
 * @param {any} theme Propriedades
 * @return {method} Componente com fabrica de item de menu customizado
 */
const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white
      }
    }
  }
}))(MenuItem)

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listContainer: {
      width: '100%',
      // backgroundColor: theme.palette.background.paper,
      position: 'relative',
      overflow: 'auto',
      maxHeight: 200,
    }
  })
);
