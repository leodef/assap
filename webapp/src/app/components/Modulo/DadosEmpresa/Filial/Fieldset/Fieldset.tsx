import React, { useMemo } from 'react'
import {
  Grid
} from '@material-ui/core'
import {
  getFieldName,
  TextField,
  useStyles as fieldsUseStyles
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  Fieldset as FieldsetLocalizacao
} from '../../../Admin/Localizacao/Fieldset/Fieldset'
import {
  TempFieldset
} from '../../../../Shared/Crud/Fieldset/TempFieldset/TempFieldset'
import './Fieldset.scss'

/**
 * Area de inputs para Filial
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Filial
 */
export const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  const fieldsClasses = fieldsUseStyles()
  const { disabled, item } = props
  const desc = useMemo( () => item ? item.desc : null, [item])
  const localizacao = useMemo( () => item ? item.localizacao : null, [item])

  const fields = useMemo(() => {
    return {
      titulo: {
        name: getFieldName('titulo', props),
        label: 'Título'
      },
      desc: {
        name: getFieldName('desc', props),
        label: 'Descrição'
      }
    }
  }, [props])

  return (<React.Fragment>

    <Grid container spacing={2}>

      <Grid item xs={12} sm={6}>

        <TextField
          name={fields.titulo.name}
          type="text"
          label={fields.titulo.label}
          disabled={disabled}
        />

      </Grid>

      <Grid item xs={12}>

        <TextField
          name={fields.desc.name}
          type="text"
          label={fields.desc.label}
          disabled={disabled}
          value={desc}
          multiline
          className={fieldsClasses.multiline}
        />
      </Grid>

      <Grid item xs={12}>

          <FieldsetLocalizacao
            {...props}
            item={localizacao}
            name="localizacao"/>

      </Grid>

    </Grid>

    <TempFieldset {...props} />
  </React.Fragment>)
})
