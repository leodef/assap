/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ArrayFieldset } from './ArrayFieldset'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ArrayFieldset />, div)
  ReactDOM.unmountComponentAtNode(div)
})
