import React, { useCallback, useMemo } from 'react'
import { crudType as types } from '../../../../../types/API/Filial'
import { AutocompleteContext } from '../../../../../contexts/AutocompleteContext'
import { Autocomplete as SharedAutocomplete } from '../../../../Shared/Autocomplete/Autocomplete'
import './Autocomplete.scss'

/**
 * Visualização de item do tipo Filial
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Filial
 */
export const Autocomplete = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.dadosEmpresa.filial.options, [])
  const { label } = useMemo(() => {
    return { label: (props.label || 'Filial') }
  }, [props.label])
  const getOptionLabel = (option: any) => option ? option.titulo || '' : ''
  const getOptionId = (val: any) => val ? val._id || null : null
  const autocompleteContextValue = { getState, types, getOptionLabel, getOptionId, label }

  return (<AutocompleteContext.Provider value={autocompleteContextValue}>
    <SharedAutocomplete
      {...props} />
  </AutocompleteContext.Provider>)
}
