/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Filial } from './Filial'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Filial />, div)
  ReactDOM.unmountComponentAtNode(div)
})
