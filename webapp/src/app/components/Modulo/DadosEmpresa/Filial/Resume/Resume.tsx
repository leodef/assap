import React, { useMemo } from "react";
import { Card, CardContent, createStyles, makeStyles, Theme, Typography } from "@material-ui/core";
import { Resume as ResumeLocalizacao } from "../../../Admin/Localizacao/Resume/Resume";
import "./Resume.scss";

/**
 * Visualização de item do tipo Filial
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Filial
 */
export const Resume = (props: any) => {
  const classes = useStyles();
  const { item, index } = useMemo(() => props, [props]);
  const localizacao = useMemo(() => (item ? item.localizacao : null), [item]);

  return (
    <React.Fragment>
      <Typography variant="h5" component="h2">
        {index ? `#${index}` : null}
        {item.titulo}
      </Typography>
      <Typography variant="body2" component="p">
        {item.desc}
      </Typography>

      <Card>
        <CardContent>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Localização:
          </Typography>

          <ResumeLocalizacao {...props} item={localizacao} name="localizacao" />
        </CardContent>
      </Card>
    </React.Fragment>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontSize: 14
    },
  })
)
