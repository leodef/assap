import
React,
{
  useCallback,
  useMemo
} from 'react'
import MuiAutocomplete from '@material-ui/lab/Autocomplete'
import { TextField } from '@material-ui/core'
import './LocalAutocomplete.scss'

/**
 * Visualização de item do tipo Funcionário
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Funcionário
 */
export const LocalAutocomplete = (props: any) => {
  const { value, onChange, disabled, ...otherProps } = useMemo(() => props, [props])

  const { label } = useMemo(() => {
    return { label: (props.label || 'Veículo') }
  }, [props.label])
  const getOptionLabel = (option: any) => option ? option.titulo || '' : ''
    
  const getOptionSelected = useCallback(
    (option: any, value: any) => 
      Boolean(
        option &&
        value &&
        option._id === value._id
      ),
    [],
  )

  return (<MuiAutocomplete
    {...otherProps}
    label={label}
    onChange={(event:any, inputValue: any) => {
      if(onChange) {
        onChange(inputValue)
      }
    }}
    value={value}
    getOptionLabel={getOptionLabel}
    getOptionSelected={getOptionSelected}
    renderInput={(params: any) => {
      return (
      <TextField
        variant="outlined"
        label={label}
        disabled={disabled}
        {...params}
      />
    )}}
  />)
}
