/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { LocalAutocomplete } from './LocalAutocomplete'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<LocalAutocomplete />, div)
  ReactDOM.unmountComponentAtNode(div)
})
