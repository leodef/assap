import React, { useCallback, useEffect, useMemo } from "react";
import { useField } from "formik";
import {
  createStyles,
  FormControlLabel,
  Grid,
  makeStyles,
  Switch,
  Theme,
} from "@material-ui/core";
import {
  getFieldName,
  SelectInput,
  TextField,
} from "../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField";
import {
  Autocomplete as AutocompleteVeiculo
} from "../../Veiculo/Autocomplete/Autocomplete";
import {
  Fieldset as FieldsetLimite
} from "../../../Logistica/Limite/Fieldset/Fieldset";
import {
  TituloFieldset
} from "../../../../Shared/Crud/Fieldset/TituloFieldset/TituloFieldset";
import {
  DescFieldset
} from "../../../../Shared/Crud/Fieldset/DescFieldset/DescFieldset";
import {
  TIPO_FUNCAO_VEICULO_LABEL
} from "../../../../../enums/tipo-funcao-veiculo";
import "./Fieldset.scss";
import { Veiculo } from "../../../../../types/API/Veiculo";

/**
 * Area de inputs para Veiculo
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Veiculo
 */
export const Fieldset = (props: any) => {
  const classes = useStyles();
  const {
    disabled,
    item,
    hideFuncao
  } = useMemo(() => props, [props]);

  const automated = useMemo(() => ({
    titulo: Veiculo.generateTitulo(props.values),
    desc: Veiculo.generateDesc(props.values)
  }),
  [props.values])

  const fields = useMemo(() => {
    return {
      funcao: {
        name: getFieldName("funcao", props),
        label: "Função do veículo",
      },
      placa: {
        name: getFieldName("placa", props),
        label: "Placa",
      },
      marca: {
        name: getFieldName("marca", props),
        label: "Marca",
      },
      modelo: {
        name: getFieldName("modelo", props),
        label: "Modelo",
      },
      tipo: {
        name: getFieldName("tipo", props),
        label: "Tipo",
      },
      ano: {
        name: getFieldName("ano", props),
        label: "Ano",
      },
      limite: {
        name: getFieldName("limite", props),
        label: "Limite",
      },
      autonomo: {
        name: getFieldName("autonomo", props),
        label: "Autonomo",
      },
      trator: {
        name: getFieldName("trator", props),
        label: "Tração própria",
      },
      compatibilidade: {
        name: getFieldName("compatibilidade", props),
        label: "Carretas Compatíveis",
      },
    };
  }, [props]);

  // funcao
  const [, funcaoMeta] = useField(
    fields.funcao.name
    );
  const funcao = useMemo(
    () => funcaoMeta.value,
    [funcaoMeta.value]);

  // compatibilidade
  const [, compatibilidadeMeta, compatibilidadeHelpers] = useField(
    fields.compatibilidade.name
  );
  const compatibilidade = useMemo(
    () => compatibilidadeMeta.value || [],
    [compatibilidadeMeta.value]
  );
  const setCompatibilidade = useCallback(
    (value: any) => compatibilidadeHelpers.setValue(value),
    [compatibilidadeHelpers]
  );

  // trator
  const [, tratorMeta, tratorHelpers] = useField(
    fields.trator.name
    );
  
  const setTrator = useCallback(
    (value: any) => tratorHelpers.setValue(value),
    [tratorHelpers]
  );

  const trator = useMemo(
    () => tratorMeta.value || false,
    [tratorMeta.value ]);

  // autonomo
  const [, autonomoMeta, autonomoHelpers] = useField(
    fields.autonomo.name
    );
  
  const autonomo = useMemo(
    () => autonomoMeta.value || false,
    [autonomoMeta.value ]);

  const autonomoLabel = useMemo(() => autonomo
  ? 'Veículo com capacidade própria de carga'
  : 'O Veículo tem capacidade própria de carga ?', [autonomo])
  
  const setAutonomo = useCallback(
    (value: any) => autonomoHelpers.setValue(value),
    [autonomoHelpers]
  );

  useEffect(() => {
    if (funcao === TIPO_FUNCAO_VEICULO_LABEL.CARRETA) {
      if (trator !== false) {
        setTrator(false);
      }
      if (autonomo !== true) {
        setAutonomo(true);
        setCompatibilidade([])
      }
      if(compatibilidade && compatibilidade.length > 0){
        setCompatibilidade([])
      }
    }
    if  (funcao === TIPO_FUNCAO_VEICULO_LABEL.CAVALO) {
      if (trator !== true) {
        setTrator(true);
      }
    }
  }, [funcao,
    trator,
    autonomo,
    compatibilidade,
    setTrator,
    setAutonomo,
    setCompatibilidade
  ]);

  // Cavalo pode ser autonomo, carrega é obrigatoriamente autonomo
  const hideAutonomo = useMemo(
    () => (funcao !== TIPO_FUNCAO_VEICULO_LABEL.CAVALO),
    [funcao]);
  // Carretas para o cavalo, se o cavalo e autonomo não precisa de carreta
  const hideCompatibilidade = useMemo(
      () => (funcao !== TIPO_FUNCAO_VEICULO_LABEL.CAVALO || autonomo),
      [funcao, autonomo]);
  const filterOptionsCompatibilidade = useCallback(
    (options) =>
      (options ? options.filter((opt: any) => {
        return (
        opt.funcao === TIPO_FUNCAO_VEICULO_LABEL.CARRETA &&
        (item && item._id && item._id !== opt._id)
      )}) : [])
    ,
    [item]
  ) 

  return (
      <Grid
        container
        spacing={1}
        className={classes.root}>
        
        {/* titulo */}
        <Grid item xs={12} sm={12}>
          <TituloFieldset
            {...props}
            automated={automated} />
        </Grid>

        {/* marca */}
        <Grid item xs={12} sm={4}>
          <TextField
            name={fields.marca.name}
            type="text"
            label={fields.marca.label}
            disabled={disabled}
          />
        </Grid>

        {/* modelo */}
        <Grid item xs={12} sm={4}>
          <TextField
            name={fields.modelo.name}
            type="text"
            label={fields.modelo.label}
            disabled={disabled}
          />
        </Grid>

        {/* tipo */}
        <Grid item xs={12} sm={4}>
          <TextField
            name={fields.tipo.name}
            type="text"
            label={fields.tipo.label}
            disabled={disabled}
          />
        </Grid>

        {/* ano */}
        <Grid item xs={12} sm={4}>
            <TextField
            name={fields.ano.name}
            type="number"
            label={fields.ano.label}
            disabled={disabled}
          />
        </Grid>

        {/* placa */}
        <Grid item xs={12} sm={4}>
          <TextField
            name={fields.placa.name}
            type="text"
            label={fields.placa.label}
            disabled={disabled}
          />
        </Grid>

        {/* funcao */}
        <Grid item xs={12} sm={4}
          className={hideFuncao ? classes.hidden : classes.show}
        >
          <SelectInput
            disabled={disabled}
            name={fields.funcao.name}
            label={fields.funcao.label}
            enumOptions={TIPO_FUNCAO_VEICULO_LABEL}
            fullWidth
          />
        </Grid>
        
        {/* autonomo */}
        <Grid item xs={12} sm={8}
          className={hideAutonomo ? classes.hidden : classes.show}
        >
          <FormControlLabel
            control={
              <Switch
                checked={autonomo}
                onChange={(event: any) =>
                  setAutonomo(event.target.checked)
                }
                name="autonomo"
                color="primary"
              />
            }
            label={autonomoLabel}
          />
        </Grid>


        {/* limite */}
        <Grid item xs={12}>
          <FieldsetLimite
            useTara
            labels={{
              tara: 'Peso do veículo',
              peso: 'Capacidade de peso',
              volume: 'Capacidade de volume' 
            }}
            name={fields.limite.name}
            label={fields.limite.label}
            disabled={disabled}
            item={item.limite}
          />
        </Grid>

        {/* veiculo */}
        <Grid item xs={12} sm={12}
          className={hideCompatibilidade ? classes.hidden : classes.show}>
          <AutocompleteVeiculo
            id="funcionario-autocomplete-filial"
            label={fields.compatibilidade.label}
            fullWidth
            multiple
            defaultValue={[]}
            filterSelectedOptions
            onChange={setCompatibilidade}
            value={compatibilidade}
            filterOptions={(option: any) => filterOptionsCompatibilidade(option)}
          />
        </Grid>

        {/* desc */}
        <Grid item xs={12}>
          <DescFieldset
            {...props}
            automated={automated} />
        </Grid>
      </Grid>
  );
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxHeight: 600
    },
    hidden: {
      display: "none",
    },
    show: {
      display: "block",
    }
  })
);
