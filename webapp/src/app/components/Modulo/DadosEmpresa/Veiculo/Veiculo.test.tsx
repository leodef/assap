/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Veiculo } from './Veiculo'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Veiculo />, div)
  ReactDOM.unmountComponentAtNode(div)
})
