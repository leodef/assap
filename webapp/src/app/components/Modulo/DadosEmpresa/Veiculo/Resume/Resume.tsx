import React from "react";
import {
  Card,
  CardContent,
  createStyles,
  makeStyles,
  Theme,
  Typography,
} from "@material-ui/core";
import { Resume as ResumeLimite } from "../../../Logistica/Limite/Resume/Resume";
import "./Resume.scss";

/**
 * Visualização de item do tipo Veiculo
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Veiculo
 */
export const Resume = (props: any) => {
  const classes = useStyles();
  const { item } = props;
  return (
    <React.Fragment>
      <Typography variant="h5" component="h2">
        {item.titulo}
      </Typography>

      <Typography variant="body2" component="p">
        {item.desc}
      </Typography>

      <Typography variant="body2" component="p">
        {item.placa ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}>Placa:</strong>
            {item.placa}
          </React.Fragment>
        ) : null}

        {item.marca ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}> - Marca:</strong>
            {item.marca}
          </React.Fragment>
        ) : null}

        {item.modelo ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}> - Modelo:</strong>
            {item.modelo}
          </React.Fragment>
        ) : null}

        {item.tipo ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}> - Tipo:</strong>
            {item.tipo}
          </React.Fragment>
        ) : null}

        {item.ano ? (
          <React.Fragment>
            <strong className={classes.resumeLabel}> - Ano:</strong>
            {item.ano}
          </React.Fragment>
        ) : null}
      </Typography>
      <Card>
        <CardContent>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            Capacidade do veículo:
          </Typography>
          <ResumeLimite
            labels={{ tara: "Peso do veículo" }}
            item={item.limite}
          />
        </CardContent>
      </Card>

      {item.autonomo ? (
        <Typography variant="body2" component="p">
          <strong className={classes.resumeLabel}>
            Veículo com capacidade de carga
          </strong>
        </Typography>
      ) : null}
      {item.trator ? (
        <Typography variant="body2" component="p">
          <strong className={classes.resumeLabel}>
            Veículo com tração própria
          </strong>
        </Typography>
      ) : null}

      {item.compatibilidade && item.compatibilidade.length > 0 ? (
        <Typography variant="body2" component="p">
          <strong className={classes.resumeLabel}>Compatibilidade:</strong>
          {item.compatibilidade.map((c: any) => c.titulo).join(", ")}
        </Typography>
      ) : null}
    </React.Fragment>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    resumeLabel: {
      marginRight: "0.35em",
      marginLeft: "0.35em",
    },
    title: {
      fontSize: 14,
    },
  })
);
