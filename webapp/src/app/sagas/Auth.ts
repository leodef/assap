import { takeLatest, put, call, all } from 'redux-saga/effects' // select
import { AuthType, authNav, logoutNav } from '../types/Auth'
import { AuthService } from '../services/Auth'
import { push } from 'connected-react-router'
import { LoadingType } from '../types/Loading'
import { MessageVariant, MessageType } from '../types/Message'
import { EmpresaUsuarioType } from '../types/EmpresaUsuario'

export class AuthSaga {
  static getSagasFunction () {
    const obj = new AuthSaga()
    return obj.getSagasFunction()
  }

  static getSagas () {
    const obj = new AuthSaga()
    return obj.getSagas()
  }

  // eslint-disable-next-line no-useless-constructor
  constructor (
    public service: AuthService = new AuthService()
  ) { }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    yield all([
      takeLatest(AuthType.AUTH, context.login.bind(context)),
      takeLatest(AuthType.LOAD, context.load.bind(context)),
      takeLatest(AuthType.LOGOUT, context.logout.bind(context)),
      takeLatest([
        AuthType.AUTH_SUCCESS,
        AuthType.AUTH_FAILURE
      ], context.msg.bind(context))
    ])
  }

  public * msg (action: any) {
    let msg = null
    let variant = MessageVariant.info
    switch (action.type) {
      case AuthType.AUTH_SUCCESS:
        msg = 'Carregado com sucesso'
        variant = MessageVariant.success
        break
      case AuthType.AUTH_FAILURE:
        msg = 'Erro ao carregar'
        variant = MessageVariant.error
        break
    }
    // chamar servico de mensagem
    const payload = { message: msg, variant }
    yield put({ type: MessageType.SHOW_MESSAGE, payload })
    yield call(console.log, msg)
  }

  public * login (action: any) {
    const context = this
    const service = context.service
    yield put({ type: LoadingType.START_LOADING, payload: 'login' })
    yield put({ type: AuthType.AUTH_PENDING, payload: action.payload })
    try {
      const user = action.payload
      const postsFromApi = yield call(service.login.bind(service), user)
      if(postsFromApi && postsFromApi.user && postsFromApi.user.empresa) {
        yield put({
          type: EmpresaUsuarioType.SET_FIX,
          payload: postsFromApi.user.empresa
        })
      } else {
        yield put({
          type: EmpresaUsuarioType.SET,
          payload: null
        })
      }
      console.log('postsFromApi', postsFromApi)
      yield put({ type: AuthType.AUTH_SUCCESS, payload: postsFromApi })
      // ##!! se admin
      yield put(push(authNav))
    } catch (error) {
      yield put({ type: AuthType.AUTH_FAILURE, payload: [], error })
      console.error(error); // eslint-disable-line
    } finally {
      yield put({ type: LoadingType.STOP_LOADING, payload: 'login' })
    }
  }

  public * logout (action: any) {
    const context = this
    const service = context.service
    yield call(service.logout.bind(service), action.payload)
    yield put({
      type: EmpresaUsuarioType.SET,
      payload: null
    })
    yield put(push(logoutNav))
  }

  public * load () {
    const context = this
    const service = context.service

    const data = yield call(service.load.bind(service))
    console.log('load data', data)
    if(data && data.user && data.user.empresa) {
      yield put({
        type: EmpresaUsuarioType.SET_FIX,
        payload: data.user.empresa
      })
    } else {
      yield put({
        type: EmpresaUsuarioType.SET,
        payload: null
      })
    }
    yield put({ type: AuthType.LOAD_SUCCESS, payload: data })
    if (data && data.token) {
      yield put(push(authNav))
    }
    return null
  }
}
export default AuthSaga.getSagasFunction()
