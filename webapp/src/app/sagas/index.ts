// import { all } from 'redux-saga/effects';
import moduloSaga from './Modulo'
import authSaga from './Auth'
import messageSaga from './Message'
import themeSaga from './Theme'

const rootSaga = [
  ...moduloSaga,
  authSaga,
  messageSaga,
  themeSaga
]
export default rootSaga
