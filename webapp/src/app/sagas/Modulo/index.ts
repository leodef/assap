import adminSaga from './Admin'
import calcCompQuimSaga from './CalcCompQuim'
import calendarioSaga from './Calendario'
import dadosEmpresaSaga from './DadosEmpresa'

const moduloSagas = [
  ...adminSaga,
  ...calcCompQuimSaga,
  ...calendarioSaga,
  ...dadosEmpresaSaga
]

export default moduloSagas
