import service from '../../../../services/Modulo/Admin/TipoContatoInfo/Crud'
import { crudType } from '../../../../types/API/Contato/TipoContatoInfo'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
