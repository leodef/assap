import service from '../../../../services/Modulo/Admin/Produto/Crud'
import { crudType } from '../../../../types/API/Produto'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
