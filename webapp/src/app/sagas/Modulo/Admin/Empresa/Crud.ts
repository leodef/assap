import service from '../../../../services/Modulo/Admin/Empresa/Crud'
import { crudType } from '../../../../types/API/Empresa/Empresa'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
