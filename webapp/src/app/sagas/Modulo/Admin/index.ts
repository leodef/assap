import empresaSaga from './Empresa'
import grandezaSaga from './Grandeza'
import produtoSaga from './Produto'
import tipoContatoInfoSaga from './TipoContatoInfo'
import unidadeMedidaSaga from './UnidadeMedida'

const adminSagas = [
  ...empresaSaga,
  ...grandezaSaga,
  ...produtoSaga,
  ...tipoContatoInfoSaga,
  ...unidadeMedidaSaga
]

export default adminSagas
