import service from '../../../../services/Modulo/Admin/UnidadeMedida/Crud'
import { crudType } from '../../../../types/API/UnidadeMedida'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
