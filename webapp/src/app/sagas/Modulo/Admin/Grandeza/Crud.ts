import service from '../../../../services/Modulo/Admin/Grandeza/Crud'
import { crudType } from '../../../../types/API/Grandeza'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
