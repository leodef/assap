import {
  takeLatest,
  put,
  call,
  all
} from 'redux-saga/effects' // select
import empresaUsuarioCrudService from '../../../services/Modulo/DadosEmpresa/EmpresaUsuario'
import {
  EmpresaUsuarioType
} from '../../../types/EmpresaUsuario'
import {
  LoadingType
} from '../../../types/Loading'
import {
  MessageType, MessageVariant
} from '../../../types/Message'

export class EmpresaUsuarioSaga  {

  constructor (
    private service = empresaUsuarioCrudService
  ) {
  }
  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    yield all(this.getSagasArray())
  }

  public getSagasArray () {
    const context = this
    return [
      takeLatest(EmpresaUsuarioType.FETCH, context.fetch.bind(context)),
      takeLatest([
        // SUCCESS
        EmpresaUsuarioType.FETCH_SUCCESS,
        // FAILURE
        EmpresaUsuarioType.FETCH_FAILURE
      ], context.msg.bind(context))
    ]
  }
  
  public * fetch (action: any) {
    const context = this
    const service = context.service
    yield put({ type: LoadingType.START_LOADING, payload: 'fetch' })
    yield put({ type: EmpresaUsuarioType.FETCH_PENDING })
    try {
      const postsFromApi = yield call(
        service.fetch.bind(service), action.payload)
      yield put({
        type: EmpresaUsuarioType.FETCH_SUCCESS,
        payload: postsFromApi ? postsFromApi.items || [] : []
      })
    } catch (error) {
      yield put({ type: EmpresaUsuarioType.FETCH_FAILURE, payload: error })
      console.error(error); // eslint-disable-line
    } finally {
      yield put({ type: LoadingType.STOP_LOADING, payload: 'fetch' })
    }
  }

  
  public * msg (action: any) {
    let variant = MessageVariant.info
    let msg = null
    switch (action.type) {
      case EmpresaUsuarioType.FETCH_SUCCESS:
        msg = 'Carregado com sucesso'
        variant = MessageVariant.success
        break
      // FAILURE
      case EmpresaUsuarioType.FETCH_FAILURE:
        msg = 'Erro ao carregar'
        variant = MessageVariant.error
        break
    }
    // chamar servico de mensagem
    const payload = { message: msg, variant }
    yield put({ type: MessageType.SHOW_MESSAGE, payload })
    yield call(console.log, msg)
  }

}


export default new EmpresaUsuarioSaga().getSagasFunction()
