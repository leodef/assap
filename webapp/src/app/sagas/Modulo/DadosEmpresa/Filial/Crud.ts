import service from '../../../../services/Modulo/DadosEmpresa/Filial/Crud'
import { crudType } from '../../../../types/API/Filial'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
