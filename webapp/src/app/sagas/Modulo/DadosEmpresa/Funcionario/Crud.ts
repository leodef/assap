import service from '../../../../services/Modulo/DadosEmpresa/Funcionario/Crud'
import { crudType } from '../../../../types/API/Funcionario'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
