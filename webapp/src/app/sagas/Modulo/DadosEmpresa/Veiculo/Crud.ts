import service from '../../../../services/Modulo/DadosEmpresa/Veiculo/Crud'
import { crudType } from '../../../../types/API/Veiculo'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
