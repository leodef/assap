
import { takeLatest, put, call } from 'redux-saga/effects'
import { CrudSaga } from '../../../Crud'
// eslint-disable-next-line no-unused-vars
import { crudType, EntregaCrudType } from '../../../../types/API/Logistica/Entrega'
// eslint-disable-next-line no-unused-vars
import service, { EntregaCrudService } from '../../../../services/Modulo/DadosEmpresa/Entrega/Crud'
import { LoadingType } from '../../../../types/Loading'

export class EntregaCrudSaga extends CrudSaga {
  static getSagasFunction (params: any) {
    const obj = new EntregaCrudSaga(params)
    return obj.getSagasFunction()
  }

  static getSagas (params: any) {
    const obj = new EntregaCrudSaga(params)
    return obj.getSagas()
  }

  constructor (params: any = null) {
    super(crudType, service, params)
  }

  public getSagasArray () {
    const context = this
    const config = context.config as EntregaCrudType
    return [
      ...super.getSagasArray(),
      takeLatest(config.CALCULO_ROTA, context.calculoRota.bind(context)),
      takeLatest([
        // SUCCESS
        config.CALCULO_ROTA_SUCCESS,
        // FAILURE
        config.CALCULO_ROTA_FAILURE
      ], context.msg.bind(context))
    ]
  }

  public * calculoRota (action: any) {
    const context = this
    const config = context.config as EntregaCrudType
    const service = context.service as EntregaCrudService
    yield put({ type: LoadingType.START_LOADING, payload: 'find' })
    yield put({ type: config.CALCULO_ROTA_PENDING })
    try {
      const { parent, ...conf } = action.payload
      const item = context.getItem(action)
      const rota = (item || {} as any).rota
      if (!rota) {
        throw new Error('Rota não definida para o calculo.')
      }
      const calculoRotaResult = yield call(service.calculoRota.bind(service), rota, conf, parent)
      yield put({
        type: config.CALCULO_ROTA_SUCCESS,
        payload: {
          item: {
            ...item,
            rota: calculoRotaResult
          },
          request: action.payload
        }
      })
    } catch (error) {
      yield put({ type: config.CALCULO_ROTA_FAILURE, payload: { error } })
      console.error(error) // eslint-disable-line
    } finally {
      yield put({ type: LoadingType.STOP_LOADING, payload: 'find' })
    }
  }
}

export default new EntregaCrudSaga().getSagasFunction()