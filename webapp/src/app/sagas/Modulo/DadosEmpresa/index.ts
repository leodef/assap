import entregaSaga from './Entrega'
import filialSaga from './Filial'
import funcionarioSaga from './Funcionario'
import parceiroSaga from './Parceiro'
import produtoSaga from './Produto'
import veiculoSaga from './Veiculo'
import empresaUsuarioSaga from './EmpresaUsuario'

const adminSagas = [
  ...entregaSaga,
  ...filialSaga,
  ...funcionarioSaga,
  ...parceiroSaga,
  ...produtoSaga,
  ...veiculoSaga,
  empresaUsuarioSaga
]

export default adminSagas
