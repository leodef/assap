import service from '../../../../services/Modulo/DadosEmpresa/Parceiro/Crud'
import { crudType } from '../../../../types/API/Parceiro/Parceiro'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
