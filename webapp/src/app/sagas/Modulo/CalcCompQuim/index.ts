import composicaoSubstanciaSaga from './ComposicaoSubstancia'
import elementoSaga from './Elemento'
import quantidadeElementoSaga from './QuantidadeElemento'
import quantidadeSubstanciaSaga from './QuantidadeSubstancia'
import substanciaSaga from './Substancia'

const adminSagas = [
  ...composicaoSubstanciaSaga,
  ...elementoSaga,
  ...quantidadeElementoSaga,
  ...quantidadeSubstanciaSaga,
  ...substanciaSaga
]

export default adminSagas
