import service from '../../../../services/Modulo/CalcCompQuim/QuantidadeElemento/Crud'
import { crudType } from '../../../../types/API/CalcCompQuim/QuantidadeElemento'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
