import service from '../../../../services/Modulo/CalcCompQuim/ComposicaoSubstancia/Crud'
import { crudType } from '../../../../types/API/CalcCompQuim/ComposicaoSubstancia'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
