import service from '../../../../services/Modulo/CalcCompQuim/Elemento/Crud'
import { crudType } from '../../../../types/API/CalcCompQuim/Elemento'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
