import service from '../../../../services/Modulo/CalcCompQuim/Substancia/Crud'
import { crudType } from '../../../../types/API/CalcCompQuim/Substancia'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
