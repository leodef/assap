import service from '../../../../services/Modulo/CalcCompQuim/QuantidadeSubstancia/Crud'
import { crudType } from '../../../../types/API/CalcCompQuim/QuantidadeSubstancia'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
