import compromissoSaga from './Compromisso'

const calendarioSagas = [
  ...compromissoSaga
]

export default calendarioSagas
