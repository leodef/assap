import service from '../../../../services/Modulo/Calendario/Compromisso/Crud'
import { crudType } from '../../../../types/API/Calendario/Compromisso'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
