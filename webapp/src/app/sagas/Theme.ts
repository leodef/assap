import { takeLatest, put, call, all, select } from 'redux-saga/effects' // select
import { ThemeType, ThemeValue } from '../types/Theme'
import { ThemeService } from '../services/Theme'

export class ThemeSaga {
  static getSagasFunction () {
    const obj = new ThemeSaga()
    return obj.getSagasFunction()
  }

  static getSagas () {
    const obj = new ThemeSaga()
    return obj.getSagas()
  }

  // eslint-disable-next-line no-useless-constructor
  constructor (
    public service: ThemeService = new ThemeService()
  ) { }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    yield all([
      takeLatest(ThemeType.LOAD, context.load.bind(context)),
      takeLatest([
        ThemeType.SET_DARK,
        ThemeType.SET_LIGHT,
        ThemeType.TOGGLE
      ], context.set.bind(context))
    ])
  }
  
  private getThemeType(action: any, state: any) {
    switch (action.type) {
      case ThemeType.SET_DARK:
        return ThemeValue.dark
      case ThemeType.SET_LIGHT:
        return ThemeValue.light
      case ThemeType.TOGGLE:
        return (state.value === ThemeValue.light
          ? ThemeValue.dark
          : ThemeValue.light
        )
      default:
        return action.payload
    }
  }

  public * set (action: any) {
    const context = this
    const service = context.service
    const state = yield select( state => state.theme);
    let themeType = this.getThemeType(action, state)
    const data = yield call(service.set.bind(service), themeType)
    yield put({ type: ThemeType.SET, payload: data })
    return null
  }

  public * load () {
    const context = this
    const service = context.service
    const data = yield call(service.load.bind(service))
    yield put({ type: ThemeType.LOAD_SUCCESS, payload: data })
    return null
  }
}
export default ThemeSaga.getSagasFunction()
