import {
  takeLatest,
  put,
  all
} from 'redux-saga/effects' // select, call
import {
  MessageType,
  MessageVariant
} from '../types/Message'

export class MessageSaga {
  static getSagasFunction () {
    const obj = new MessageSaga()
    return obj.getSagasFunction()
  }

  static getSagas () {
    const obj = new MessageSaga()
    return obj.getSagas()
  }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    yield all([
      takeLatest(MessageType.SHOW_MESSAGE, context.add.bind(context))
    ])
  }

  public * add (action: any) {
    const message = action.payload.message || action.payload
    const variant = action.payload.variant || MessageVariant.info
    const _id = new Date().getMilliseconds().toString()
    const payload = { message, _id, variant }
    yield put({ type: MessageType.ADD_MESSAGE, payload })
    /*
      try {
        yield put({ type: MessageType.ADD_MESSAGE, payload })
        yield delay(1000)
      } finally {
        yield put({ type: MessageType.REMOVE_MESSAGE, payload })
      }
    */
  }
}
export default MessageSaga.getSagasFunction()
