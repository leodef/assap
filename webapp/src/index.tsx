import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import * as dotenv from 'dotenv'
import * as serviceWorker from './serviceWorker'
import { App } from './app/containers/App'
import configureStore, { history } from './configureStore'
import './index.scss'

const preloadedState = {} as any
dotenv.config()
const element = document.getElementById('root')
if (!element) {
  throw new Error('couldn\'t find element with id root')
}

/*
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware))
  const store: Store = createStore(rootReducer, enhancer)
*/

const store = configureStore(preloadedState)
const render = () => {
  // const googleaApisLink = getGoogleMapsApiUrl()
  const fontLink = 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap'
  const materialIconLink = 'https://fonts.googleapis.com/icon?family=Material+Icons'

  ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <link
          rel="stylesheet"
          href={fontLink} />
        <link
          rel="stylesheet"
          href={materialIconLink} />
        {/* <link rel="text/javascript" href={googleaApisLink} /> */}
        <App />
      </ConnectedRouter>
    </Provider>,
    document.getElementById('root'))
}

render()

// Hot reloading
if ((module as any).hot && (module as any).accept) {
  // Reload components
  (module as any).accept('./App', () => {
    render()
  })
}
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
