
// API /empresa
export const MockEntrega = {
  nomeFantasia: 'String',
  razaoSocial: 'String',
  desc: 'String',
  cnpj: 'String',
  // API /dados-empresa
  dados: { // Schema DadosEntrega
  // API /dados-empresa/filial Reference
    filiais: [ // Ref Filial
      {
        titulo: 'String',
        desc: 'String',
        localizacao: { // Schema: Localizacao
            titulo: 'String',
            endereco: 'String',
            lat: { type: 'Number', min: -90 , max: 90 },
            long: { type: 'Number', min: -180 , max: 180 },
            pais: 'String',
            estado: 'String',
            cidade: 'String',
            area: 'String',
            codigoPostal: 'String',
            numero: 'String',
            complemento: 'String'
        } // Schema: Localizacao
      }
  ], // Ref Filial
  // API /dados-empresa/parceiro Reference
    parceiros: [ // Ref Parceiro
      {
        titulo: 'String',
        desc: 'String',
        identificacao: 'String',
    tipoPessoa: { type: 'String', enum: 'TIPO_PESSOA' },
    filiais: [ // Ref Filial
      {
        titulo: 'String',
        desc: 'String',
        localizacao: { // Schema: Localizacao
          titulo: 'String',
          endereco: 'String',
          lat: { type: 'Number', min: -90 , max: 90 },
          long: { type: 'Number', min: -180 , max: 180 },
          pais: 'String',
          estado: 'String',
          cidade: 'String',
          area: 'String',
          codigoPostal: 'String',
          numero: 'String',
          complemento: 'String'
        } // Schema: Localizacao
      }
        ], // Ref Filial
    contatos: [ // Schema ContatoParceiro
      {
            contato: {
        titulo: 'String',
        desc: 'String',
        infos: [
          {
            valor: 'String',
            tipo: {
              titulo: 'String',
              desc: 'String',
              mask: 'String'
            } // Ref 'TipoContatoInfo'
          } // Schema ContatoInfo
        ]
        },
        filial: {  // Ref Filial
      titulo: 'String',
      desc: 'String',
      localizacao: { // Schema: Localizacao
        titulo: 'String',
        endereco: 'String',
        lat: { type: 'Number', min: -90 , max: 90 },
        long: { type: 'Number', min: -180 , max: 180 },
        pais: 'String',
        estado: 'String',
        cidade: 'String',
        area: 'String',
        codigoPostal: 'String',
        numero: 'String',
        complemento: 'String'
      } // Schema: Localizacao
        } // Ref Filial
      }
        ] // Schema ContatoParceiro
      } // Ref Parceiro
  ],
  // API /dados-empresa/veiculo Reference
    veiculos: [ // Ref Veiculo
      { 
        titulo: 'String',
        desc: 'String',
        placa: 'String',
        marca: 'String',
        modelo: 'String',
        tipo: 'String',
        limite: { // Schema Limite
      peso: 'Number',
      volume: 'Number',
      tara: 'Number'
    }, // Schema Limite
        autonomo: 'Boolean', // Capaz de transportar carga sozinho
        trator: 'Boolean', // Com tração própria
        compatibilidade: [{}] // Ref Veiculo
      } // Ref Veiculo
  ],
  // API /dados-empresa/funcionario Reference
    funcionarios: [ // Ref Funcionario
      {
        nome: 'String',
        cpf: 'String',
        posicoes: [
            'String'
    ],
        filiais: [ // Ref Filial
      {
        titulo: 'String',
        desc: 'String',
        localizacao: { // Schema: Localizacao
          titulo: 'String',
          endereco: 'String',
          lat: { type: 'Number', min: -90 , max: 90 },
          long: { type: 'Number', min: -180 , max: 180 },
          pais: 'String',
          estado: 'String',
          cidade: 'String',
          area: 'String',
          codigoPostal: 'String',
          numero: 'String',
          complemento: 'String'
        } // Schema: Localizacao
      }
        ] // Ref Filial
      } // Ref Funcionario
    ],
  // API /dados-empresa/produto Reference
    produtos: [ // Ref Produto
      {
        nome: 'String',
        desc: 'String',
        tipo: 'String',
        marca: 'String',
        densidade: 'Number'
      } // Ref Produto
    ],
  // API /dados-empresa/entrega Reference
    entregas: [ // Ref Entrega
      {
        titulo: 'String',
        tipoFormacaoTitulo: { type: 'String', enum: 'TIPO_FORMACAO' },
        desc: 'String',
        tipoFormacaoDesc: { type: 'String', enum: 'TIPO_FORMACAO' }, 
        data: 'Date',
    rota: { // Schema Rota
      algoritimo: { type: 'String', enum: 'ALGORITIMO_ROTA' },
      distancia: 'Number',
      duracao: 'Number',
      duracaoComTrafego: 'Number',
      calculo: 'Boolean',
      pontos: [ // Schema PontoRota
        {
          titulo: 'String',
          desc: 'String',
          ordem: 'Number',
          distancia: 'Number',
          duracao: 'Number',
          duracaoComTrafego: 'Number',
          distanciaDesc: 'String',
          duracaoDesc: 'String',
          duracaoComTrafegoDesc: 'String',
          localizacao: { // Schema: Localizacao
            titulo: 'String',
            endereco: 'String',
            lat: { type: 'Number', min: -90 , max: 90 },
            long: { type: 'Number', min: -180 , max: 180 },
            pais: 'String',
            estado: 'String',
            cidade: 'String',
            area: 'String',
            codigoPostal: 'String',
            numero: 'String',
            complemento: 'String'
          }, // Schema: Localizacao,
          limite: { // Schema Limite
            peso: 'Number',
            volume: 'Number',
            tara: 'Number'
          }, // Schema Limite
          anterior: [
            'Number'
          ],
          status: { type: 'String', enum: 'STATUS_ENTREGA' }
        }
      ], // Schema PontoRota
      limite: { // Schema Limite
        peso: 'Number',
        volume: 'Number',
        tara: 'Number'
      }, // Schema Limite
        }, // Schema Rota
        status: { type: 'String', enum: 'STATUS_ENTREGA' },
        parceiroEntrega: { // Schema ParceiroEntrega
          parceiro: { // Ref Parceiro
        titulo: 'String',
        desc: 'String',
        identificacao: 'String',
        tipoPessoa: { type: 'String', enum: 'TIPO_PESSOA' },
        filiais: [ // Ref Filial
          {
            titulo: 'String',
            desc: 'String',
            localizacao: { // Schema: Localizacao
            titulo: 'String',
            endereco: 'String',
            lat: { type: 'Number', min: -90 , max: 90 },
            long: { type: 'Number', min: -180 , max: 180 },
            pais: 'String',
            estado: 'String',
            cidade: 'String',
            area: 'String',
            codigoPostal: 'String',
            numero: 'String',
            complemento: 'String'
            } // Schema: Localizacao
          }
        ], // Ref Filial
        contatos: [ // Schema ContatoParceiro
          {
            contato: {
            titulo: 'String',
            desc: 'String',
            infos: [
            {
              valor: 'String',
              tipo: {
              titulo: 'String',
              desc: 'String',
              mask: 'String'
              } // Ref 'TipoContatoInfo'
            } // Schema ContatoInfo
            ]
            },
            filial: {  // Ref Filial
            titulo: 'String',
            desc: 'String',
            localizacao: { // Schema: Localizacao
              titulo: 'String',
              endereco: 'String',
              lat: { type: 'Number', min: -90 , max: 90 },
              long: { type: 'Number', min: -180 , max: 180 },
              pais: 'String',
              estado: 'String',
              cidade: 'String',
              area: 'String',
              codigoPostal: 'String',
              numero: 'String',
              complemento: 'String'
            } // Schema: Localizacao
            } // Ref Filial
          }
        ] // Schema ContatoParceiro
      }, // Ref Parceiro
          contato: { // Ref Contato
        titulo: 'String',
        desc: 'String',
        infos: [ // Schema ContatoInfo
          {
            valor: 'String',
            tipo: { // Ref TipoContatoInfo
              titulo: 'String',
              desc: 'String',
              mask: 'String'
            } // Ref TipoContatoInfo
          }
        ] // Schema ContatoInfo
      } // Ref Contato
        }, // Schema ParceiroEntrega
        transporte: { // Schema Transporte
          veiculo: {  // Ref Veiculo
        titulo: 'String',
        desc: 'String',
        placa: 'String',
        marca: 'String',
        modelo: 'String',
        tipo: 'String',
        limite: { // Schema Limite
          peso: 'Number',
          volume: 'Number',
          tara: 'Number'
        }, // Schema Limite
        autonomo: 'Boolean', // Capaz de transportar carga sozinho
        trator: 'Boolean', // Com tração própria
        compatibilidade: [{}] // Ref Veiculo
      }, // Ref Veiculo
          carreta: { // Ref Veiculo
        titulo: 'String',
        desc: 'String',
        placa: 'String',
        marca: 'String',
        modelo: 'String',
        tipo: 'String',
        limite: { // Schema Limite
          peso: 'Number',
          volume: 'Number',
          tara: 'Number'
        }, // Schema Limite
        autonomo: 'Boolean', // Capaz de transportar carga sozinho
        trator: 'Boolean', // Com tração própria
        compatibilidade: [{}] // Ref Veiculo
      } // Ref Veiculo
        }, // Schema Transporte
        tipoLimite: { type: 'String', enum: 'TIPO_LIMITE_ENTREGA' },
    funcionarios: { // Schema FuncionarioEntrega
      functionario: {
        nome: 'String',
        cpf: 'String',
        posicoes: [
          'String'
        ],
        filiais: [ // Ref Filial
          {
            titulo: 'String',
            desc: 'String',
            localizacao: { // Schema: Localizacao
              titulo: 'String',
              endereco: 'String',
              lat: { type: 'Number', min: -90 , max: 90 },
              long: { type: 'Number', min: -180 , max: 180 },
              pais: 'String',
              estado: 'String',
              cidade: 'String',
              area: 'String',
              codigoPostal: 'String',
              numero: 'String',
              complemento: 'String'
            } // Schema: Localizacao
            }
        ] // Ref Filial
      }, // Ref Funcionario
      tipo: { type: 'String', enum: 'TIPO_FUNCIONARIO_ENTREGA' }
        } // Schema FuncionarioEntrega
      } // Ref Entrega
    ],
  } // Schema DadosEmpresa /dados-empresa
} // Empresa


/*
API
  /empresa
    CRUD (criar subdocumentos automaticamente)
  /dados-empresa
    Simples Rota
  /dados-empresa/:dadosEmpresa/filial
    Reference
  /dados-empresa/:dadosEmpresa/parceiro
    Reference
  /dados-empresa/:dadosEmpresa/veiculo
    Reference
  /dados-empresa/:dadosEmpresa/funcionario
      Reference
  /dados-empresa/:dadosEmpresa/produto
    Reference
  /dados-empresa/:dadosEmpresa/entrega
    Reference

Telas
  CRUD Empresa
  Renomear Dashboar para Administrativo
  Dashboard
    (Caso o usuario nao seja vinculado a empresa,
      mostra autocomplete para escolher uma empresa)
Componentes
  Empresa/Autocomplete
  CRUD Empresa
    Empresa/Fieldset

  CRUD Filial
    Filial/Fieldset
      (Localizacao/Fieldset)
  
  CRUD Parceiro
    Parceiro/Fieldset
      (tipoPessoa select TIPO_PESSOA)
      (Filial/ListAutocomplete)
      (Contato/Fieldset)
        (ContatoInfo/FormArray)
  //           (ContatoInfoType/Autocomplete)
  
  CRUD Veiculo
    Veiculo/Fieldset
      (Limit/Fieldset)
      (Veiculo/ListAutocomplete)

  CRUD Funcionario
    Funcionario/Fieldset
      (posicoes Chips)
      (Filial/ListAutocomplete)

  CRUD Produto
    Produto/Fieldset

  CRUD Entrega
    Entrega/Fieldset (DadosEmpresaParent)
*/