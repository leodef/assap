const { ModelFactory } = require('../commons/model-factory');

class BusinessAuthService {
  // API
  //login
  login(authToken, req, res, next) {
    const {
      usuario
    } = authToken
    const {
      senha,
      ...usuarioResult
    } = usuario
    console.log('business login authToken', authToken, 'usuario', usuario, 'usuarioResult', usuarioResult)
    const funcionarioWrapper = ModelFactory.getModel('Funcionario');
    if(!funcionarioWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo Funcionario.');
    }
    const funcionarioModel = funcionarioWrapper.model;
    return funcionarioModel
          .findOne({ usuario: (usuario._id || usuario) })
          .populate(
            'empresa',
            // 'empresa.tipo',
            {
              path: 'usuario',
              select: {
                '_id': 1,
                'usuario': 1,
                'nome': 1,
                'tipo': 1,
                'status': 1,
                'email': 1
              }
            }
          )
          .exec()
          .then(result =>
            result
              ? result.toObject()
              : ({ usuario: usuarioResult })
          )
  }

  //logout
  logout(logoutParams, req, res, next) {
    return logoutParams;
  }
}

  
const service = new BusinessAuthService();
module.exports = { BusinessAuthService, service };
