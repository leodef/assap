const {
  ModuloDocumentCrudService,
} = require('../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../commons/model-factory');
const {
  TravelingSalesman
} = require('../../../commons/travelling-salesman');
const {
  ModuloCompromossioService
} = require('../calendario/compromisso')

 /**
 * Serviço com funções padrões para CRUD da coleção Entrega
 * 
 * @name ModuloLogisticaEntregaService
 * @module commons/crud/service/impl
 * @category Serviço
 * @subcategory Entrega
 */
class ModuloLogisticaEntregaService extends ModuloDocumentCrudService {
  /**
   * Retorna a instancia de ModuloLogisticaEntregaService
   * 
   * @param {any} config Configuração da instancia
   *
   *  @returns {ModuloLogisticaEntregaService}
  */
  static getInstance(config) {
    return new ModuloLogisticaEntregaService(config);
  }

  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
   */
  modulo() {
    return "logistica";
  }

  /**
   * @constructor
   *
   * @param {any} config  Objeto de configuração da instancia
   * 
   * @returns ModuloLogisticaEntregaService
   */
  constructor(config) {
    const field = "entregas";
    const modelWrapper = ModelFactory.getModel("Entrega");
    if (!modelWrapper) {
      throw new Error(
        "Não foi possivel carregar a persistencia do modelo Entrega."
      );
    }
    const parentIdUrlName = "dadosEmpresa";
    const parentModelWrapper = ModelFactory.getModel("DadosEmpresa");
    if (!modelWrapper) {
      throw new Error(
        "Não foi possivel carregar a persistencia do modelo DadosEmpresa."
      );
    }
    super({
      ...config,
      parent: {
        persistence: parentModelWrapper,
        field,
      },
      persistence: modelWrapper,
      parentIdUrlName,
      // ...queries
    });
    this.pontoRotaPersistence = ModelFactory.getModel("PontoRota");
    this.compromissoService = ModuloCompromossioService.getInstance();
    this.travelingSalesman = TravelingSalesman.getInstance();
  }

  /**
   * Calcula a melhor rota entre pontos
   *   POST /calculo-rota
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   */
  calculoRota(req, res, next) {
    req.logger.silly(`${typeof this} - create`);
    const travelingSalesman = this.travelingSalesman;
    const body = this.getReqBody(req);
    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    const action = 'rota';
    return this.resolveTask(
      req,
      res,
      next,
      Promise
        .resolve(body)
        .then((values) => {
          const { points, distanceMatrix, config } = values;
          const params = { points, distanceMatrix, config };
          return travelingSalesman.resolve(params);
        }),
      action
    );
  }

  carregarCompromisso(compromisso, entrega) {
    if(compromisso.tipoFormacaoTitulo === 'AUTOMATIZADO') {
      compromisso.titulo = entrega.titulo
    }
    if(compromisso.tipoFormacaoDesc === 'AUTOMATIZADO') {
      compromisso.desc = entrega.desc
    }
    compromisso.inicio = entrega.data
    const hour = 360
    compromisso.fim = moment(
        entrega.data
      ).add(
        entrega.rota
          ? entrega.rota.duracaoComTrafego || entrega.rota.duracao || hour
          : hour
      , 's');
    compromisso.recursos = {
      funcionarios: (entrega.funcionarios
        ? entrega.funcionarios.map((func) => func.funcionario)
        : []
      ),
      veiculos: (entrega.transporte
        ? [ entrega.transporte.veiculo, entrega.transporte.carreta]
        : []),
      entrega: entrega._id
    }
    compromisso.diaInteiro = true
    compromisso.tipo = 'entrega'
    return compromisso
  }

  carregarPontoRota(entrega) {
    if (!entrega || !entrega.rota || !entrega.rota.pontos) {
      return entrega
    }
    const rota = entrega.rota
    const pontos = rota.pontos
    rota.pontos = pontos
      .filter(ponto => Boolean(ponto))
      .map(ponto => this.carregarValoresPontoRota(ponto))
      .map(ponto => {
        if(!ponto.anterior || ponto.anterior.length === 0) {
          return ponto;
        }
        const anterior =  ponto.anterior
        for(const pontoAnteriorIndex in anterior) {
            const pontoAnterior = anterior[pontoAnteriorIndex]
            const pontoAnteriorTemp = (
              pontoAnterior._temp ||
              pontoAnterior
            )
            const pontoAnteriorFindIndex = pontos.findIndex(
              (cp) =>
                cp &&
                cp._temp &&
                cp._temp === pontoAnteriorTemp
              )
            const pontoAnteriorFind = pontoAnteriorFindIndex >= 0
              ? pontos[pontoAnteriorFindIndex]
              : null
            if(pontoAnteriorFind && pontoAnteriorFind._id) {
              anterior[pontoAnteriorIndex] = pontoAnteriorFind._id
            }
        }
        ponto.anterior = anterior
        return ponto
      }).map(ponto => {
        ponto._temp = null
        return ponto
      })
    entrega.rota = rota
    return entrega
  }

  create(req, res, next) {
    const parentIdUrlName = this.parentIdUrlName;
    const body = this.getReqBody(req)

    // user
    const user = this.auth(req, res, next);

    // _id
    let _id = null
    const _parentId = this.searchReqParam(req, parentIdUrlName);

    if (!user) {
      return;
    }

    const task =
    this.createCompromisso(body)
    .then( (result) => {
      return this.createItem(_parentId, result)
    }).then((result) => {
      _id = result._id || result
      return this.carregarPontoRota(result)
    }).then((result) => {
        return this.updateItem(_parentId, _id, result)
    })
    
    const action = 'create';
    return this.resolveTask(
      req,
      res,
      next,
      task,
      action
    );
  }

  createCompromisso(entrega) {
    const compromissoService = this.compromissoService;
    const {
      criarCompromisso,
      compromisso
    } = entrega
    return (criarCompromisso && compromisso)
        ? compromissoService.createItem(
            this.carregarCompromisso(compromisso, entrega)
          )
          .then((result) => {
            entrega.compromisso = result ? result._id || result : null
            return entrega
          })
        : Promise.resolve(entrega)
  }

  update(req, res, next) {
    const parentIdUrlName = this.parentIdUrlName;
    const documentPersistence = this.documentPersistence
    const body = this.getReqBody(req)

    // user
    const user = this.auth(req, res, next);

    // _id
    const _id = this.searchReqParam(req, 'id');
    const _parentId = this.searchReqParam(req, parentIdUrlName);
  
    if (!user) {
      return;
    }

    // - Pesquisar Parceiro anteriormente
    const task = documentPersistence.find({ _id })
    .then((result) => {
      return this.updateCompromisso(body, result)
    }).then((result) => {
      return this.updateItem(_parentId, _id, body)
    }).then((result) => {
      return this.carregarPontoRota(result)
    }).then((result) => {
      return this.updateItem(_parentId, _id, body)
    })
    
    const action = 'update';
    return this.resolveTask(
      req,
      res,
      next,
      task,
      action
    );
  }

  updateCompromisso(entrega, find) {
    const compromissoService = this.compromissoService;
    const {
      criarCompromisso,
      compromisso
    } = entrega
    if(!criarCompromisso && find && find.compromisso) {
      return compromissoService.removeItem(
        find.compromisso._id
      )
      .then((result) => {
        entrega.compromisso = null
        return entrega
      })
    }
    if(criarCompromisso && compromisso) {
      return (find.compromisso
        ? compromissoService.updateItem(
            find.compromisso._id,
            this.carregarCompromisso(compromisso, entrega)
          )
        : compromissoService.createItem(
            this.carregarCompromisso(compromisso, entrega)
          )
        ).then((result) => {
          entrega.compromisso = result ? result._id : null
          return entrega
        })
    }
    return Promise.resolve(entrega)
  }

  remove(req, res, next) {
    const remove = super.remove.bind(this);
    let response = null

    // - Remover Parceiro
    remove(req, res, next)
    .then( (result) => {
      response = result;
      return result;
    }).then(
      (result) => this.removeCompromisso(result)
    ).then((result) => response)
  }

  removeCompromisso(result) {
    const compromissoService = this.compromissoService;
    const {
      compromisso
    } = body
    return (compromisso)
    ? compromissoService.removeItem(compromisso)
    : Promise.resolve(null)
  }


  carregarValores(obj) {
    const documentPersistence = this.documentPersistence
    const persistenceConfig = documentPersistence.persistence.config || {};
    const {
      generateTitulo,
      generateDesc
    } = persistenceConfig
    const {
      tipoFormacaoTitulo,
      tipoFormacaoDesc,
      _id
    } = obj

    return documentPersistence.find({ _id }).then((result) => {
      if(result && result.rota && result.rota.pontos){
        obj.rota = obj.rota || {}
        obj.rota.pontos = result.rota.pontos.map(ponto => carregarValoresPontoRota(ponto))
      }
      if(tipoFormacaoTitulo === 'AUTOMATIZADO' && generateTitulo) {
        obj.titulo = generateTitulo(result)
      }
      if(tipoFormacaoDesc === 'AUTOMATIZADO' && generateDesc) {
        obj.desc = generateDesc(result)
      }
      return obj
    })
    
  }

  carregarValoresPontoRota(obj) {
    const persistenceConfig = this.pontoRotaPersistence.config || {};
    const { 
      generateTitulo,
      generateDesc
    } = persistenceConfig
    const {
      tipoFormacaoTitulo,
      tipoFormacaoDesc
    } = obj
  
    if(tipoFormacaoTitulo === 'AUTOMATIZADO'  && generateTitulo) {
      obj.titulo = generateTitulo(obj)
    }
    if(tipoFormacaoDesc === 'AUTOMATIZADO'  && generateDesc) {
      obj.desc = generateDesc(obj)
    }
    return obj
  }

}

module.exports = { ModuloLogisticaEntregaService };
