const {
  ModuloDocumentCrudService,
} = require('../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../commons/model-factory');

class ModuloCompromossioService extends ModuloDocumentCrudService {
  /**
   * Retorna a instancia de ModuloCompromossioService
   * 
   * @param {any} config Configuração da instancia
   *
   *  @returns {ModuloCompromossioService}
  */
  static getInstance(config) {
    return new ModuloCompromossioService(config);
  }

  constructor(config) {
    const modelWrapper = ModelFactory.getModel('Compromisso');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo Compromisso.');
    }
    return super({
      persistence: modelWrapper,
    });
  }

  modulo() {
    return 'calendario';
  }

}

module.exports = { ModuloCompromossioService };
