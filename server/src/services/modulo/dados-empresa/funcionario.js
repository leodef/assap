const {
  ModuloReferencedDocumentCrudService,
} = require('../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../commons/model-factory');
const {
  ModuloUsuarioService
} = require('../auth/usuario')
class ModuloFuncionarioService extends ModuloReferencedDocumentCrudService {
  /**
   * Retorna a instancia de ModuloFuncionarioService
   * 
   * @param {any} config Configuração da instancia
   *
   *  @returns {ModuloFuncionarioService}
  */
  static getInstance(config) {
    return new ModuloFuncionarioService(config);
  }

  constructor(config) {
    const field = 'funcionarios'
    const modelWrapper = ModelFactory.getModel('Funcionario');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo Funcionario.');
    }
    const parentIdUrlName = 'dadosEmpresa'
    const parentModelWrapper = ModelFactory.getModel('DadosEmpresa');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo DadosEmpresa.');
    }
    super({
      ...config,
      parent: {
        persistence: parentModelWrapper,
        field
      },
      persistence: modelWrapper,
      parentIdUrlName
      // ...queries
     });
    this.usuarioService = ModuloUsuarioService.getInstance();
  }


  create(req, res, next) {
    const parentIdUrlName = this.parentIdUrlName;
    const body = this.getReqBody(req)

    // user
    const user = this.auth(req, res, next);

    // _id
    let _id = null
    const _parentId = this.searchReqParam(req, parentIdUrlName);

    if (!user) {
      return;
    }

    const task =
    this.createUsuario(body)
    .then( (result) => {
      return this.createItem(_parentId, result)
    })
    
    const action = 'create';
    return this.resolveTask(
      req,
      res,
      next,
      task,
      action
    );
  }

  createUsuario(body) {
    const usuarioService = this.usuarioService;
    const {
      criarUsuario,
      usuario
    } = body
    return (criarUsuario && usuario)
        ? usuarioService.createItem(usuario)
          .then((result) => {
            body.usuario = result ? result._id || result : null
            return body
          })
        : Promise.resolve(body)
  }

  update(req, res, next) {
    const parentIdUrlName = this.parentIdUrlName;
    const documentPersistence = this.documentPersistence
    const body = this.getReqBody(req)

    // user
    const user = this.auth(req, res, next);

    // _id
    const _id = this.searchReqParam(req, 'id');
    const _parentId = this.searchReqParam(req, parentIdUrlName);
  
    if (!user) {
      return;
    }

    // - Pesquisar Parceiro anteriormente
    const task = documentPersistence.find({ _id })
    .then((result) => {
      return this.updateUsuario(body, result)
    }).then((result) => {
      return this.updateItem(_parentId, _id, body)
    })
    
    const action = 'update';
    return this.resolveTask(
      req,
      res,
      next,
      task,
      action
    );
  }

  updateUsuario(body, find) {
    const usuarioService = this.usuarioService;
    const {
      criarUsuario,
      usuario
    } = body
    if(!criarUsuario && find && find.usuario) {
      return usuarioService.removeItem(
        find.usuario._id
      )
      .then((result) => {
        body.usuario = null
        return body
      })
    }
    if(criarUsuario && usuario) {
      return (find.usuario
        ? usuarioService.updateItem(
          find.usuario._id,
          usuario)
        : usuarioService.createItem(
          usuario)
        ).then((result) => {
          body.usuario = result ? result._id : null
          return body
        })
    }
    return Promise.resolve(body)
  }

  remove(req, res, next) {
    const remove = super.remove.bind(this);
    let response = null

    // - Remover Parceiro
    remove(req, res, next)
    .then( (result) => {
      response = result;
      return result;
    }).then(
      (result) => this.removeUsuario(result)
    ).then((result) => response)
  }

  removeUsuario(result) {
    const usuarioService = this.usuarioService;
    const {
      usuario
    } = body
    return (usuario)
    ? usuarioService.removeItem(usuario)
    : Promise.resolve(null)
  }

  modulo() {
    return 'dados-empresa';
  }

}

module.exports = { ModuloFuncionarioService };
