const {
  ModuloReferencedDocumentCrudService,
} = require('../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../commons/model-factory');


  /**
   * Gera a query para ser executada posterior a qualquer outra query na base
   *
   * @param {any} params Parametros para gerar a query
   * 
   * @returns {Array<any>}
   *
  */
const nextQuery = (params) => {
  return [
      {
        '$lookup': {
          'from': 'veiculos', 
          'localField': 'compatibilidade', 
          'foreignField': '_id', 
          'as': 'compatibilidade'
        }
      }
    ]
  }

class ModuloVeiculoService extends ModuloReferencedDocumentCrudService {
  /**
   * Retorna a instancia de ModuloVeiculoService
   * 
   * @param {any} config Configuração da instancia
   *
   *  @returns {ModuloVeiculoService}
  */
  static getInstance(config) {
    return new ModuloVeiculoService(config);
  }

  constructor(config) {
    const field = 'veiculos'
    const modelWrapper = ModelFactory.getModel('Veiculo');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo Veiculo.');
    }
    const parentIdUrlName = 'dadosEmpresa'
    const parentModelWrapper = ModelFactory.getModel('DadosEmpresa');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo DadosEmpresa.');
    }
    super({
      ...config,
      parent: {
        persistence: parentModelWrapper,
        field
      },
      persistence: modelWrapper,
      parentIdUrlName,
      nextQuery: nextQuery
      // ...queries
     });
  }

  carregarValores(obj) {
    const documentPersistence = this.documentPersistence
    const persistenceConfig = documentPersistence.persistence.config || {};
    const {
      generateTitulo,
      generateDesc
    } = persistenceConfig
    const {
      tipoFormacaoTitulo,
      tipoFormacaoDesc,
      _id
    } = obj

    return documentPersistence.find({ _id }).then((result) => {
      if(tipoFormacaoTitulo === 'AUTOMATIZADO' && generateTitulo) {
        obj.titulo = generateTitulo(result)
      }
      if(tipoFormacaoDesc === 'AUTOMATIZADO' && generateDesc) {
        obj.desc = generateDesc(result)
      }
      return obj
    })
    
  }


  /**
   * Informa o nome do modulo em que os serviços estão situados
   * 
   * @returns {String}
   * 
  */
 modulo() { return 'dados-empresa'; }


}

module.exports = { ModuloVeiculoService };
