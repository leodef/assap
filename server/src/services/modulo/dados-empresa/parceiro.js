const {
  ModuloReferencedDocumentCrudService,
} = require('../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../commons/model-factory');
const {
  ModuloFilialService
} = require('../admin/filial')

  /**
   * Gera a query para ser executada posterior a qualquer outra query na base
   *
   * @param {any} params Parametros para gerar a query
   * 
   * @returns {Array<any>}
   *
  */
 function nextQuery(params) {
  return [
      {
        '$lookup': {
          'from': 'filials', 
          'localField': 'filiais', 
          'foreignField': '_id', 
          'as': 'filiais'
        }
      }
    ]
  }

class ModuloParceiroService extends ModuloReferencedDocumentCrudService {
  static getInstance(config) {
    return new ModuloParceiroService(config);
  }

  constructor(config) {
    const field = 'parceiros'
    const modelWrapper = ModelFactory.getModel('Parceiro');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo Parceiro.');
    }
    const parentIdUrlName = 'dadosEmpresa'
    const parentModelWrapper = ModelFactory.getModel('DadosEmpresa');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo DadosEmpresa.');
    }
    super({
      ...config,
      parent: {
        persistence: parentModelWrapper,
        field
      },
      persistence: modelWrapper,
      parentIdUrlName,
      nextQuery
      // ...queries
    });
    // this.filialModelWrapper = ModelFactory.getModel('Filial');
    this.filialService = ModuloFilialService.getInstance();
  
  }

  modulo() {
    return 'dados-empresa';
  }

  create(req, res, next) {
    const create = super.create.bind(this);
    const filialService = this.filialService;
    const body = this.getReqBody(req);
    const filiais = body.filiais;
    let response = null;
    ( // - Criar / atualizar todos as Filiais 
      filiais ?
      filialService.createItem(filiais) : // createOrUpdateALl
      Promise.resolve(null)
     ).then( (filiais) => {
        // - Para cada contato caso a filial aponte para o id temporario de uma filial da lista, substitui pelo id
        body.filiais = (filiais || []).map(filial => filial._id);

        body.contatos = (body.contatos || []).map(
        ( contato, contatoIndex) => {

          const contatoFilialTemp = (contato.filial
            ? String(contato.filial._temp || contato.filial)
            : null)

          const contatoFilial = contatoFilialTemp
            ? filiais.find(
                (filial, filialIndex) => 
                  String(filial._temp) === contatoFilialTemp
              )
            : null;
          contato.filial = (contatoFilial
            ? contatoFilial._id
            : contato.filial) || null;
          return contato;
      });
      return body;
    }).then((result) => {
      // - Carrega o parceiro alterado no corpo da requisição
      return req.body = result;
    }).then((result) => {
      // - Salva o parceiro carregado
      create(req, res, next)
      return result
    }).then( (result) => {
      // - Carrega o retorno da função
      response = result;
      return result;
    }).then((result) =>
      // - Remover temp id das filiais e salvar 
      Promise.all(
        (response.filiais || []).map(
          (filial, index) => filialService.updateItem(
            filial._id,
            {
              ...filial,
              _temp: null
            })
        )
      )
    ).then((result) => {
      return response;
    })
  }

  update(req, res, next) {
    const update = super.update.bind(this);
    const documentPersistence = this.documentPersistence;
    const filialService = this.filialService;

    const _id = this.searchReqParam(req, 'id');
    const body = this.getReqBody(req);

    const filiais = body.filiais || []
    let response = null

    // - Pesquisar Parceiro anteriormente
    documentPersistence.find({ _id })
    .then( (old) => {
      // - Ver se alguma Filial esta faltando nos dados atuais do Parceiro
      const oldFiliais = (old ? old.filiais : [])
      const filtered = oldFiliais.filter(
        (oldFilial) => {
          return !Boolean(filiais.find(
            (filial) => {
              const oId = (oldFilial._id || oldFilial).toString()
              const fId = (filial._id || filial).toString()
              return (oId === fId)
            }))
        })
      return filtered
    }).then( (toRemoveFiliais) => {
      // - Remover filiais nao encontradas
      return filialService.removeItem(
        toRemoveFiliais) //##!! Remove all
    }).then( () => 
      // - Criar / atualizar todos as Filiais
      Promise.all( filiais.map(cFilial => 
        cFilial._id
          ? filialService.updateItem(cFilial._id, cFilial)
          : filialService.createItem(cFilial)
        ) )
    ).then( (filiais) => {
      // - Para cada contato caso a filial aponte para o id temporario de uma filial da lista, substitui pelo id
      body.filiais = (filiais || []).map(filial => filial._id);

      body.contatos = (body.contatos || []).map(
      ( contato, contatoIndex) => {

        const contatoFilialTemp = (contato.filial
          ? String(contato.filial._temp || contato.filial)
          : null)

        const contatoFilial = contatoFilialTemp
          ? filiais.find(
              (filial, filialIndex) => 
                String(filial._temp) === contatoFilialTemp
            )
          : null;
        contato.filial = (contatoFilial
          ? contatoFilial._id
          : contato.filial) || null;
        return contato;
      });
      return body;
    }).then((result) => {
      // - Carrega o parceiro alterado no corpo da requisição
      return req.body = result;
    }).then((result) => {
      // - Salva o parceiro carregado
      update(req, res, next)
      return result
    }).then( (result) => {
      // - Carrega o retorno da função
      response = result;
      return result;
    }).then((result) =>
      // - Remover temp id das filiais e salvar 
      Promise.all(
        (response.filiais || []).map(
          (filial, index) => filialService.updateItem(
            filial._id,
            {
              ...filial,
              _temp: null
            })
        )
      )
    ).then((result) => {
      return response;
    })
  }

  remove(req, res, next) {
    const remove = super.remove.bind(this);
    const filialService = this.filialService;
    let response = null

    // - Remover Parceiro
    remove(req, res, next)
    .then( (result) =>
      // - Carrega o retorno da função
      response = result
    ).then((result) =>
      // - Remover todas as filiais
      filialService.removeItem(result.filiais)
    ).then( (result) => response)
  }

}
module.exports = { ModuloParceiroService };
