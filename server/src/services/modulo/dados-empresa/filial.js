const {
  ModuloReferencedDocumentCrudService,
} = require('../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../commons/model-factory');

class ModuloFilialService extends ModuloReferencedDocumentCrudService {
  /**
   * Retorna a instancia de ModuloFilialService
   * 
   * @param {any} config Configuração da instancia
   *
   *  @returns {ModuloFilialService}
  */
  static getInstance(config) {
    return new ModuloFilialService(config);
  }

  constructor(config) {
    const field = 'filiais'
    const modelWrapper = ModelFactory.getModel('Filial');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo Filial.');
    }
    const parentIdUrlName = 'dadosEmpresa'
    const parentModelWrapper = ModelFactory.getModel('DadosEmpresa');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo DadosEmpresa.');
    }
    super({
      ...config,
      parent: {
        persistence: parentModelWrapper,
        field
      },
      persistence: modelWrapper,
      parentIdUrlName
      // ...queries
     });
  }

  modulo() {
    return 'dados-empresa';
  }

}

module.exports = { ModuloFilialService };
