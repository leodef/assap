const {
  ModuloDocumentCrudService,
} = require('../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../commons/model-factory');
const passwordUtilsService = require('../../../services/password-utils').service;

const resolveItem = (body, context) => {
  const {
    senha,
    ...value
  } = body || {}
  if(senha) {
    value.senha =  passwordUtilsService.cryptPassword(senha)
  }
  return value;
}

class ModuloUsuarioService extends ModuloDocumentCrudService {
  static getInstance(config) {
    return new ModuloUsuarioService(config);
  }

  constructor(config) {
    const modelWrapper = ModelFactory.getModel('Usuario');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo Usuario.');
    }
    super({
      persistence: modelWrapper,
      resolveItem
   });
  }

  modulo() {
    return 'auth';
  }

}

module.exports = { ModuloUsuarioService };
