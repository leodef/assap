const {
  ModuloDocumentCrudService,
} = require('../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../commons/model-factory');

class ModuloFilialService extends ModuloDocumentCrudService {
  static getInstance(config) {
    return new ModuloFilialService(config);
  }

  constructor(config) {
    const modelWrapper = ModelFactory.getModel('Filial');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo Filial.');
    }
    super({
      persistence: modelWrapper,
   });
  }

  modulo() {
    return 'admin';
  }

}

module.exports = { ModuloFilialService };
