const moment = require('moment');

const AUTHENTICATION_TOKEN_EXPIRY_DAYS = 6;

class AuthTokenService {
  getAuthTokenExpiryDate() {
    return moment().add(AUTHENTICATION_TOKEN_EXPIRY_DAYS, 'days').toDate();
  }
    
  getAuthTokenDTO(usuario) {
    return {
      usuario: usuario,
      validade: this.getAuthTokenExpiryDate(),
      };
  }
}

  
const service = new AuthTokenService();
module.exports = { AuthTokenService, service };

