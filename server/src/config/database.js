const mongoose = require('mongoose');
const env = require('./env');
const logger = require('./logger');

/**
 * Função de conexão com o banco de dados
 * @namespace
 */
const connect = () => {
  mongoose.Promise = require('bluebird');
  /**
   * Configuração da conexão
   * @memberof connect
   * @example
   * var config = {
   * URI: 'mongodb://mongodb-ip/magic',
   * OPTIONS: {
   *     user: 'tarcher',
   *     pass: 'SUPERSECRETPASSWORD',
   *     auth: {
   *       authdb: 'magic'
   *     }
   *   }
   * }
   * db.init(config.URI, config.OPTIONS);
   * const CONFIG = {
   *   useUnifiedTopology: true,
   *   useNewUrlParser: true
   *   useMongoClient: true, user: 'root',  pass: 'pass'
   * }
   */
  const CONFIG = {
    useUnifiedTopology: true,
    useNewUrlParser: true
  };

 /**
   * Conexão com a base de dados
   * @memberof connect
   * @example
   * mongoose.connect(
   *   'mongodb://user:password@sample.com:port/dbname',
   *   { useNewUrlParser: true })
   */
   const client = mongoose.connect(env.db, CONFIG);
  
   /**
   * Inicia o banco em modo debug
   * @memberof connect
   */
  if(env.env == 'development'){
    mongoose.set('debug', false);
  }

  /**
   * Função para ser executada quando a conexão e feita
   * @memberof connect
   */
  mongoose.connection.on('connected', function(){
    logger.info('mongoose connected');
  });

  /**
   * Função para ser executada quando a conexão e finalizada
   * @memberof connect
   */
  mongoose.connection.on('disconnected', function(){
    logger.info('mongoose disconnected');
  });
  
  /**
   * Função para ser executada quando houver erro de conexão
   * @memberof connect
   */
  mongoose.connection.on('error', function(err){
    logger.info('mongoose error: '+err);
  });
  
  /**
   * Finaliza a conexão com o banco quando o App é encerrado
   * @memberof connect
   */
  process.on('SIGINT', function(){
    mongoose.connection.close(function(){
      logger.info('Mongoose, desconnected by application');
    });
  });
  
  return { client };
};

module.exports = connect();