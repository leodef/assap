const mongoose = require('mongoose');
const moment = require('moment');
const passport = require('passport');
const { ModelFactory } = require('../commons/model-factory');
const { BusinessAuth } = require('./business-auth');

 /**
 * Estratégia de autenticação no sistema
 * 
 * @name Auth
 * @module config/auth
 * @category Config
 * @subcategory Auth
 */
class Auth {

  static validAuthToken(authToken) {
    if (!authToken) { throw 'Token não encontrado' }
    if(moment() >= authToken.expiryDate){
      // Remove token se expirado
      return authToken.remove(function(err, token){
        if(err) {
          throw err;
        }
        throw 'Sessão expirada';
      });
    } else {
      return authToken;
    }
  }

  // return cb(err);   return cb(authTokenValidate.err, false); 

  /**
   * Metodo para realizar a autenticação
   * 
   * @param {any} token Token enviado na requisição
   * @param {any} cb Callback para ser executado
   *   com dados da autenticação
   * @returns {any}
  */
  static authenticate(token, cb) {

    const authTokenWrapper = ModelFactory.getModel('AuthToken');
    if(!authTokenWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo AuthToken.');
    }
    const authTokenModel = authTokenWrapper.model;

    // Procura o token
    authTokenModel.findById(token).exec()
    .then((authToken) => {
      authToken = authToken
      return authToken
    })
    .then((authToken) => 
      Auth.validAuthToken(authToken)
    )
    .then((authToken) =>
      BusinessAuth.authenticate(authToken, cb)
      )
    .then((business) => cb(null, business))
    .catch(err => cb(err, false));// find account
    
  }// authenticate

  static authTask() {
    return passport.authenticate('bearer', { session: false })
  }

}// Authentication

module.exports = Auth;
