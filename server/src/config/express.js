const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const methodOverride = require('method-override');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const passport = require('passport');
const helmet = require('helmet');
const morgan = require('morgan');
const cors = require('cors');
const expressWs = require('express-ws');
const Strategy = require('passport-http-bearer').Strategy;
//config
const db = require('./database.js');
const env = require('./env');
const logger = require('./logger');
const routes = require('../routes');
const modelFactory = require('./model-factory')
// const date = require('./date');
const Auth = require('./auth');
const Account = require('./account');
const responseHandler = require('./response-handler');


/**
 * Carrega a configuração principal da aplicação retornando uma 
 *   encadeamento de funções a serem executadas no fluxo principal
 *   do sistema
 * @namespace
 */
const loadApp = () => {
  /**
   * 1- Função para criar o usuário principal,
   *   caso dele não exista
   * 
   * @memberof loadApp
   * @example Estratégia da aplicação (Usar)
   *   Account.init();
  */
  Account.init();

  /**
   * 2 - carrega a estratégia de autentincação
   * 
   * @memberof loadApp
   * 
   * @example Estratégia da aplicação (Usar)
   *   passport.use(new Strategy(Auth.authenticate));
   * 
  */
  passport.use(new Strategy(Auth.authenticate));
  
  //3- Cria Express
  
  /**
   * 3 - Cria objeto princiapl da aplicação para carregar
   *   a configuração e encadeamento de funções
   * 
   * @memberof loadApp
   * 
   * @example Estratégia da aplicação (Usar)
   *    const app = express();
   * 
   * @example Estratégia com uso de web-sockets
   *    const appExpressWs = expressWs(app);
   * 
  */
  const app = express();
  

  /**
   * 4 - Carraga as principais variaveis globais da aplicação
   * 
   * @memberof loadApp
   * 
   * @example Estratégia da aplicação (Usar)
   *   app.use(passport.initialize());
   *   app.use(passport.session());
   *   passport.authenticate('bearer', { session: false });
   * 
  */
  // app.use(passport.initialize());
  // app.use(passport.session());
  // passport.authenticate('bearer', { session: false });

  /**
   * 5 - Carraga as principais variaveis globais da aplicação
   *   public - Local de arquivos publicos para acesso da api
   *   title  - Titulo da aplicação
   *   env    - Configuração do ambiente da aplicação
   *   db     - Configuração da base de dados
   *   logger - Função para salvar logs da aplicação
   *   modelFactory - Função de gerenciamento de objetos de persistencia
   * 
   * @memberof loadApp
   * 
  */
  app.use('public', express.static(path.join(__dirname, 'public/')));
  app.set('title', 'Assap')
  app.set('env', env);
  app.set('db', db);
  app.set('logger', logger);
  app.set('modelFactory', modelFactory);

  /**
   * 6 - Configuração para lidar com ataques do tipo CORS
   *   (Compartilhamento de recursos de origem cruzada))
   * 
   * @memberof loadApp
   * 
   * @example Utilizando a configuração de ambiente (Usar)
   *   app.use(cors(env.corsOptions));
   * 
   * @example Configuração manual
   *   app.options('', (req, res, next) => {
   *   var headers = {};
   *   headers['Access-Control-Allow-Origin'] = '';
   *   headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS';
   *   headers['Access-Control-Allow-Credentials'] = false;
   *   headers['Access-Control-Max-Age'] = '86400'; // 24 hours
   *   headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization, cache-control';
   *   res.writeHead(200, headers);
   *   res.end();
   *   });
   * 
  */
  app.use(cors(env.corsOptions));

  /**
   * 7 - Estrátegia de automatização dos logs da comunicação HTTP
   * @memberof loadApp
   * 
  */
  app.use(morgan('tiny', { 'stream': logger.stream }));

  
  /**
   * 7 - Defini estratégia de conversão de requisição
   *    para objeto valido para o sistema
   *    - Conversão os parametros de URL
   *    - Conversão do corpo da requisição no formato JSON
   *    - Metodo antigo utilizado para definir um método HTTP
   * @memberof loadApp
   * 
  */
  app.use(bodyParser.urlencoded(env.bodyParser.urlencoded));
  app.use(bodyParser.json(env.bodyParser.json));
  app.use(methodOverride());

  /**
   * 8 - Realiza o parser do header de cookies da requisição
   *   populando req.cookies e armazena o IDdasessão
   * 
   * @memberof loadApp
   * 
  */
  app.use(cookieParser());//req.cookie

  /**
   * 9- cria por padrão a sessão do usuário em memória
   *       saveUninitialized -garante que as informações 
   *       da sessão serão acessíveis através de cookies a
   *       cada requisição
   * 
   * @memberof loadApp
   * 
  */
  app.use(session({//req.session
    secret: 'a4f8071f-c873-4447-8ee2',
    cookie: { maxAge: 2628000000 },
    resave: true,
    saveUninitialized: true
  }));

  /**
   * 10- Proteção da aplicação usando a lib helmet
   *   frameguard - impede que a pagina seja acessada por xframes
   *   noSniff - impede que o browser use scrpts ou links
   *      com arquivos que não sejam css ou js
   *
   * @memberof loadApp
   * 
   * @example
   *   app.disable('x-powerd-by');
  */
  app.use(helmet.ieNoOpen());
  app.use(helmet.frameguard());
  app.use(helmet.xssFilter());
  app.use(helmet.noSniff());
  app.use(helmet.hidePoweredBy({setTo:'PHP 5.5.14'}));

  /**
   * 11- Configuração do retorno em JSON
   * 
   * @memberof loadApp
   * 
   * @example
   *  app.set('json replacer', date.replacer.bind(date)); 
   *  app.set('json spaces', 2);
  */
  // app.set('json replacer', date.replacer.bind(date)); 
  // app.set('json spaces', 2);

  /**
   * 13- Carrega funções comuns no app para serem usadas
   *   pela aplicação
   * 
   * @memberof loadApp
   * 
  */
  app.use(responseHandler(app));

  /**
   * 14- Roteamento da requisição para os serviços
   *   pela aplicação
   * 
   * @memberof loadApp
   * 
  */
  app.use(routes());

  return app;
};

module.exports = loadApp();
