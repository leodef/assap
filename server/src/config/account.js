//frameworks
const bcrypt = require('bcrypt');
const _ = require('lodash');
const env = require('./env')
const {
  ModelFactory
} = require("../commons/model-factory");
const passwordUtilsService = require('../services/password-utils').service;

class Account{

    static init() {
        const accountDTO = Account.createDTO(env.adminAccount);
        const { usuario } = accountDTO
        
        const modelWrapper = ModelFactory.getModel('Usuario');
        if(!modelWrapper) {
          throw new Error('Não foi possivel carregar a persistencia do modelo Usuario.');
        }

        const { model } = modelWrapper;
        return model.findOneAndUpdate(
            { usuario },
            accountDTO,
            { new: true, upsert: true })
    } // init


    static createDTO(body){
        return {
            usuario: body.usuario, // usuario: String,
            senha: passwordUtilsService.cryptPassword(body.senha), // senha: String,
            nome: body.nome, //  nome: String,
            tipo: 'ADMIN', // tipo: { type: String, enum: TIPO_USUARIO }
            status: 'ativo', // status: String
            email: 'admin@admin.com'
        };// return
    }// createDTO
}// Account

 /**
 * Inicializa a base com o usuário master
 * 
 * @name Account
 * @module config/account
 * @category Config
 * @subcategory Account
 */
module.exports = Account;