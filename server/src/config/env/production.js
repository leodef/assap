/**
 * Ambiente de produção
 * @namespace
 * @name envProduction
 */
const env = {
  env: 'production',
  db: process.env.OPENSHIFT_MONGODB_DB_URL + 'assap',
  clientID: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  port: process.env.OPENSHIFT_NODEJS_PORT,
  address: process.env.OPENSHIFT_NODEJS_IP,
  domain: process.env.OPENSHIFT_APP_DNS,
  bodyParser: {
    urlencoded: { extended: false, limit: '50mb' },
    json: {limit: '50mb'}
  },
  datetime: {
    format: {
      date: 'YYYY/MM/DD',
      datetime: 'YYYY/MM/DD hh:mm:ss'
    }
  },
  auth: {
    tokenHeaderKey: 'x-auth-token',
    salt: 10
  },
  log: {
    file: 'server.log',
    combined: 'server.log',
    error: 'error.log'
  },
  urlConfig: {
    host: 'https://localhost:4200',
    confirmAccount: 'https://localhost:4200/confirmAccount/:token',
    forgotPassword: 'https://localhost:4200/forgotPassword/:token'
  },
  file: {
    clientImage: process.env.FILE_CONFIG_CLIENT_IMAGE ||
      `${appRoot}/assets/client-image`
  },
  corsOptions: {
    origin: process.env.FRONTEND_ORIGIN,//'http://example.com',
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
    credentials: true
  },
  adminAccount: {
    usuario: process.env.ADMIN_ACCOUNT_USERNAME,
    senha: process.env.ADMIN_ACCOUNT_PASSWORD,
    nome: process.env.ADMIN_ACCOUNT_NAME,
    tipo: 'ADMIN',
    status: 'ativo',
    email: process.env.ADMIN_ACCOUNT_EMAIL,
  },
  email: {
    transport: {
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false,
      auth: {
        user: 'gkwaway4abclejwv@ethereal.email',
        pass: 'tqaupaVdyqw1rGHgnJ',
      }
    },
    defaultOptions: {
      from: '"Controle de Produção 👻" <no-reply@assap.com>',
      //to: 'bar@example.com, baz@example.com',
      ///subject: 'Hello ✔', 
      //text: 'Hello world?', 
      //html: '<b>Hello world?</b>'
    }
  },
  recognition: {
    default: {
      state: 'default',
      prefix: 'default__'
    }
  }
};
module.exports = env;
