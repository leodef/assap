const appRoot = require('app-root-path');

/**
 * Ambiente de desenvolvimento
 * @namespace
 * @name envDevelopment
 */
const env = {
  env: 'development', 
  db: process.env.MONGODB_DB_URL || 'mongodb://localhost/assap',// mongodb://user:password@sample.com:port/dbname
  port: 3000,
  address: 'localhost',
  domain: 'localhost:3000',
  bodyParser: {
    urlencoded: { extended: false, limit: '50mb' },
    json: {limit: '50mb'}
  },
  datetime: {
    format: {
      date: 'YYYY/MM/DD',
      datetime: 'YYYY/MM/DD hh:mm:ss'
    }
  },
  auth: {
    tokenHeaderKey: 'x-auth-token',
    salt: 10
  },
  log: {
    file: 'server.log',
    combined: 'server.log',
    error: 'error.log'
  },
  urlConfig: {
    host: 'https://localhost:4200',
    confirmAccount: 'https://localhost:4200/confirmAccount/:token',
    forgotPassword: 'https://localhost:4200/forgotPassword/:token'
  },
  file: {
    clientImage: process.env.FILE_CONFIG_CLIENT_IMAGE ||
      `${appRoot}/assets/client-image`
  },
  corsOptions: {
    origin: 'http://localhost:8080',//'http://example.com',
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
    credentials: true,
    exposedHeaders: 'x-auth-token'
  },
  adminAccount: {
    usuario: 'admin',
    senha: 'admin31@admin23',
    nome: 'Admin',
    tipo: 'ADMIN',
    status: 'ativo',
    email: 'admin31@admin.com'
  },
  email: {
    transport: {
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false,
      auth: {
        user: 'gkwaway4abclejwv@ethereal.email',
        pass: 'tqaupaVdyqw1rGHgnJ',
      }
    },
    defaultOptions: {
      from: '"Controle de Produção 👻" <no-reply@assap.com>',
      //to: 'bar@example.com, baz@example.com',
      ///subject: 'Hello ✔', 
      //text: 'Hello world?', 
      //html: '<b>Hello world?</b>'
    }
  },
  recognition: {
    default: {
      state: 'default',
      prefix: 'default__'
    }
  }
};
module.exports = env;
