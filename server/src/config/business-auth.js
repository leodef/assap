const mongoose = require('mongoose');
const moment = require('moment');
const { ModelFactory } = require('../commons/model-factory');

class BusinessAuth {

  static authenticate(authToken, cb) {
    const {
      usuario
    } = (authToken || {})
    if(!usuario) {
      return Promise.resolve(
        { usuario: null, token: authToken }
      )
    }

    const funcionarioWrapper = ModelFactory.getModel('Funcionario');
    const { model } = funcionarioWrapper;

    return model
          .findOne({ usuario: (usuario._id || usuario) })
          .populate(
            'empresa',
            {
              path: 'usuario',
              select: {
                '_id': 1,
                'usuario': 1,
                'nome': 1,
                'tipo': 1,
                'status': 1,
                'email': 1
              }
            }
          ).then(result => (result || { usuario })
          ).then(result => ({ ...result, token: authToken }))
  }

}// Authentication

module.exports = { BusinessAuth };


