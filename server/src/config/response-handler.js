
/**
 * Funções comuns para construir o retorno da aplicação
 * @namespace
 */
const responseHandler = function(app){
  const method = function(req, res, next){
    const env = req.env = res.env = app.get('env');
    const db = req.db = res.db = app.get('db');
    const logger = req.logger = res.logger = app.get('logger');

  /**
   * Cria retorno com erro
   * @memberof responseHandler
   */
    res.handleError  = req.handleError = function(req, res, err, status = 500){
      if(!err){return false;}
      console.error(err.stack)
      const responseErr = {error:true};
      //if(typeof err == 'string'){
      responseErr.title = err.message || err.title || err;
      logger.error(err);
      logger.error(err.stack);
      logger.info(responseErr);
      if(!res.headersSent){
        res.status(status).json(responseErr);
      }
      return true;
    };
  /**
   * Cria retorno de sucesso
   * @memberof responseHandler
   */
    res.handleSuccess = req.handleSuccess = function(req, res, title, description, data = null, status = 200){
      res.status(status).json({
        title: title,
        description: description,
        data:data
      });
    };
  
  /**
   * Formata retorno genérico
   * @memberof responseHandler
   */
    res.handleResponse = req.handleResponse = function(req, res, data, status = 200){
      res.status(status).json(data); 
      /*
        1- JSON.stringify(object, replacer, space )
        2- res.send() 
      */
    };

  /**
   * Cria retorno com arquivo
   * @memberof responseHandler
   */
    res.handleFile = req.handleFile = function(req, res, data, type = 'image/jpeg', status = 200){
      res.writeHead(status, { 'Content-Type': type }); // 
      res.end(data); // Send the file data to the browser.
    };

  /**
   * Cria retorno com erro de acesso negado
   * @memberof responseHandler
   */
    res.forbiddenResponse = req.forbiddenResponse = function(req, res, msg){
      if(!msg) {msg = 'Authentication Falied'; }
      req.handleError(req, res, {title: msg}, 401);
      return false;
    };
    
  
  /**
   * Confirma a auntenticação,
   *   caso não exista,
   *   gera o retorno com erro 
   * @memberof responseHandler
   */
    res.confirmAuth = req.confirmAuth = function(req, res, next){
      if(!req.user){
        req.forbiddenResponse(req, res, 'Authentication Falied')
        return false;
      }
      return true;      
    };

  /**
   * Confirma a permissao do usuário,
   *   caso não exista,
   *   gera o retorno com erro 
   * @memberof responseHandler
   */
    res.confirmPermission = req.confirmPermission = function(req, res, user){
      if(!req.confirmAuth(req, res, next)){
        return false;
      }
      var userid;
      userid = user._id ? user._id : user;
      userid = user.id ? user.id : user;
      if(userid != req.user._id){
        req.handleError(req, res, {title: 'Wrong Permission'}, 403);
        return false;
      }
      return true;

    };
  
  /**
   * Confirma a existencia do corpo da requisição,
   *   caso não exista,
   *   gera o retorno com erro 
   * @memberof responseHandler
   */
    res.confirmBody = req.confirmBody = function(req, res, next){
      if(!req.body){
        req.handleError(req, res, {title: 'Empty body'});
        return false;
      }
      return true;      
    };

  /**
   * Confirma a existencia de um objeto,
   *   caso não exista,
   *   gera o retorno com erro 
   * @memberof responseHandler
   * @todo Separar confirmResult e confirmObjeto aonde um confirma
   *    se uma Promisse tem resultado, e o outro um objeto generico
   */
    res.confirmResult = req.confirmResult = function(req, res, result){
      if(!result){
        req.handleError(req, res, {title: 'Object Not Found'}, 404);
        return false;
      }
      return true;
    };
  /**
   * Valida a url da requisição,
   *   caso esteja invalida,
   *   gera o retorno com erro 
   * @memberof responseHandler
   */
    res.checkUrl = req.checkUrl = function(url){
      if(!url){return false;}
      return url.startsWith(env.urlConfig.host);
    };

    next();
  }
  return method;
}
module.exports = responseHandler;
