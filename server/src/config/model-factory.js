const {ModelFactory, ModelWrapper} = require('../commons/model-factory');
const schemas = require('../schemas');
const mongoose = require('mongoose');
const config = {list: schemas};
/**
 * Instancia um objeto de gerenciamento de objetos de persistencia
 * 
 */
const modelFactory = ModelFactory.getInstance(config);

module.exports = modelFactory;