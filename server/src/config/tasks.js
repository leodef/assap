var cron = require('node-cron');

/**
 * Configuração de agendamento de tarefas
 * @namespace
 */
function loadTasks(){
    
  /**
   * Configuração de agendamento de tarefas
   *   second  0-59
   *   minute  0-59
   *   hour  0-23
   *   day of month  1-31
   *   month  1-12 (or names)
   *   day of week  0-7 (or names, 0 or 7 are sunday)
   * 
   * @memberof loadTasks
   * 
   * @example
   *   const ExampleTask = () => {
   *    console.info('ExampleTask')
   *   };
   *   cron.schedule('* * 3 * * *', loadRecognitionStateTask);
   * 
   * @example executar tarefa 3 da manha
   *   cron.schedule('* * * * *', function() {
   *     logger.silly('running a task every minute');
   *  });
*/
  
  return true;
}

module.exports = loadTasks();