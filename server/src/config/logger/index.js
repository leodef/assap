const winston = require('winston');
const config = require('./config');
/**
 * Função que carrega o objeto de log
 */
const createLogger = () => {
    return winston.createLogger(config);
};

const logger = createLogger();

/**
 * Instancia um objeto de automatização de log
 * @namespace
 * @name logger
 */
logger.stream = {
    /**
     * Configuração da escrita do objeto
     * @memberof logger
     */
    write: function(message, encoding){
        logger.info(message);
    }
};

module.exports = logger;
