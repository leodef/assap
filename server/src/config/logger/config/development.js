const winston = require('winston');
/**
 * Configuração do objeto de log para desenvolvimento
 * @namespace
 * @name logConfigDevelopment
 * @example
 *     const files =  new winston.transports.File({ filename:  env.log.combined}),
 *     const console = new winston.transports.Console()
 *     logger
 *         .clear()
 *         .add(console)
 *         .add(files)
 *         .remove(console);
 * 
 */
const config = {
    format:winston.format.combine(   
      winston.format.colorize(),
            winston.format.simple()
            //winston.format.json()
        ),
        transports: [
            new winston.transports.Console({
                handleExceptions:true,
                json: false,
                colorize: true
            })
        ],
        exitOnError: false
};
module.exports = config;
