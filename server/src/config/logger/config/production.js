/**
 * Configuração do objeto de log para produção
 * @namespace
 * @name logConfigProduction
 * @example
 *     const files =  new winston.transports.File({ filename:  env.log.combined}),
 *     const console = new winston.transports.Console()
 *     logger
 *         .clear()
 *         .add(console)
 *         .add(files)
 *         .remove(console);
 * 
 */
const config = {
    format:winston.format.combine(   
      winston.format.colorize(),
            winston.format.simple()
            //winston.format.json()
        ),
        transports: [
            new winston.transports.Console({
                handleExceptions:true,
                json: false,
                colorize: true
            }),
            new winston.transports.File({ 
                filename:  env.log.combined,
                handleExceptions: true,
                json:true,
                colorize: false
            }),
            new winston.transports.File({ 
                    filename:  env.log.error,
                    handleExceptions: true,
                    json:true,
                    colorize: false,
                    
                })

        ],
        exitOnError: false
};
module.exports = config;
