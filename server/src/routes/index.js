const express = require('express');

/**
 * Rotas da API
 */
const apiRoutes = require('./api');

/**
 * Mapeamento de rotas do sistema
 * @namespace
 * @name routes
 */
const routes = () => {
  const router = express.Router({ mergeParams: true });

  /**
   * Mapeamento de rotas da API
   * @memberof routes
  */
  router.use('/api', apiRoutes());

  /**
   * Rota de teste
   * @memberof routes
  */
  router.post('/test', function (req, res, next) {
    res.send('test')
  });

  /**
   * Rota padrão, caso nenhuma seja requisitada
   * @memberof routes
  */
  router.use('/', function (req, res, next) {
    res.send('default');
  });

  return router;
};

module.exports = routes;
