const express = require('express');
// const passport = require('passport');
const service = require('../../services/auth');
const Auth = require('../../config/auth');
const authTask = Auth.authTask();

/**
 * Mapeamento de rotas para autenticação
 * @namespace
 * @name authRoutes
 * 
 * @example Rota para consulta de tipos de contas
 *     router.get('/accountTypes',service.accountTypes.bind(service));
 * @example Rota para confirmação da conta através do token
 *     router.post('/confirm-account/:token', service.confirmAccount.bind(service));
 * @example Rota para solicitar o token de redefinição da senha
 *     router.post('/forgot-password', service.forgotPassword.bind(service));
 * @example Rota para redefinir senha através do token
 *     router.post('/reset-password/:token', service.resetPassword.bind(service));
 * @example Rota para validar usuário  
 *     router.get('/check-username/:username', service.checkUsername.bind(service));
 * 
 */
const routes = () => {
  const router = express.Router();

  /**
   * Rota para autenticação no sistema
   * @memberof authRoutes
  */
  router.post(
    '/login',
    service.login.bind(service)
  );

  /**
   * Rota para remover a sessão atual
   * @memberof authRoutes
  */
  router.post(
    '/logout',
    [
      authTask,
      service.logout.bind(service)
    ]
  );
  return router;
};

module.exports = routes;
