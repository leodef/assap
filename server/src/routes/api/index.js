const express = require('express');
/**
 * Rotas de Modulos
 */
const moduloRoutes = require('./modulo');
/**
 * Rotas de autenticação
 */
const authRoutes = require('./auth');

/**
 * Mapeamento de rotas da api
 * @namespace
 * @name apiRoutes
 */
const routes = () => {

  const router = express.Router({ mergeParams: true });

  /**
   * Mapeamento de rotas de Modulos
   * @memberof apiRoutes
  */
  router.use('/modulo', moduloRoutes());

  /**
   * Mapeamento de rotas de autenticação
   * @memberof apiRoutes
  */
  router.use('/auth', authRoutes());

  /**
   * Rota padrão, caso nenhuma seja requisitada
   * @memberof apiRoutes
  */
  router.use('/', function (req, res, next) {
    res.send('api');
  });

  return router;
};

module.exports = routes;
