const express =  require('express');

/**
 * Mapeamento de rotas para testes
 * @namespace
 * @name testRoutes
 */
const routes = () => {
  const router = express.Router({mergeParams: true});
    
  /**
   * Rota padrão, caso nenhuma seja requisitada
   * @memberof apiRoutes
  */
  router.use('/', function(req, res){
      res.send('test');
  });

  return router;
};

module.exports = routes;