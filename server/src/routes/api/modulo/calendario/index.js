const express = require('express');
const { CompromissoRouter } = require('./compromisso');

/**
 * Mapeamento de rotas para Compromisso
 * @namespace
 * @name calendarioRoutes
 */
const routes = () => {
  const router = express.Router({ mergeParams: true });

  /**
   * Rotas do calendario Compromisso
   * @memberof calendarioRoutes
   */
  router.use('/compromisso', new CompromissoRouter().router());

  /**
   * Rota padrão, caso nenhuma seja requisitada
   * @memberof calendarioRoutes
  */
  router.use('/', function (req, res, next) {
    res.send('calendario');
  });

  return router;
};

module.exports = routes;
