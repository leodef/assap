const {
  ModuloCrudRouter
} = require('../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDadosEmpresaEntregaService
} = require('../../../../services/modulo/dados-empresa/entrega');


/**
 * Mapeamento de rotas para entrega
 *
 * @name EntregaRouter
 * @module routes/api/modulo/dados-empresa/entrega
 * @category Routes
 * @subcategory Admin
*/
class EntregaRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
  */
  modulo() {
    return 'dados-empresa';
  }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    return ModuloDadosEmpresaEntregaService.getInstance();
  }

  
  /**
   * Mapeia os serviços derivados
   *
   * @param {any} router Objeto de registro de mapemamento
   * 
   * @returns {any}
   *
  */
  loadChild(router) {
  const service = this.service();
  router.post.apply(router, [
    "/calculo-rota",
    ...this.getTaks(service.calculoRota.bind(service), "calculoRota"),
  ]);
  return router;
}



}

module.exports = { EntregaRouter };
