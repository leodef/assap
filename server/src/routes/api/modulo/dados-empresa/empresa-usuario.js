const {
  ModuloCrudRouter,
} = require("../../../../abstracts/router/modulo/modulo-crud-router");
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDocumentCrudService,
} = require("../../../../abstracts/service/modulo/modulo-crud-service");
const {
  ModelFactory
} = require("../../../../commons/model-factory");

/**
 * Mapeamento de rotas para dados da empresa
 *
 * @name EmpresaUsuarioRouter
 * @module routes/api/modulo/dados-empresa/
 * @category Routes
 * @subcategory Admin
 */
class EmpresaUsuarioRouter extends ModuloCrudRouter {
  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
   */
  modulo() {
    return 'dados-empresa';
  }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    const modelWrapper = ModelFactory.getModel("Empresa");
    if (!modelWrapper) {
      throw new Error(
        "Não foi possivel carregar a persistencia do modelo Empresa."
      );
    }
    return ModuloDocumentCrudService.getInstance({
      persistence: modelWrapper
    });
  }

  /**
   * Carrega rotas no objeto de registrar o roteamento
   *
   * @returns {any}
   */
  loadRouter() {
    const router = this._router || this.createRouter();
    const service = this.service();

    //find
    router.get.apply(router, [
      '/',
      ...this.getTaks(service.fetch.bind(service), 'fetch'),
    ]);

    //find
    router.post.apply(router, [
      '/fetch',
      ...this.getTaks(service.fetch.bind(service), 'fetch'),
    ]);

    // fetchOptions
    router.post.apply(router, [
      '/fetchOptions',
      ...this.getTaks(service.fetch.bind(service), 'fetch'),
    ]);

    return this.loadChild(router);
  }

}

module.exports = { EmpresaUsuarioRouter };
