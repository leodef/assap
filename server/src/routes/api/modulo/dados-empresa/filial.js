const {
  ModuloCrudRouter
} = require('../../../../abstracts/router/modulo/modulo-crud-router');
const {
  ModuloFilialService
} = require('../../../../services/modulo/dados-empresa/filial');
/**
 * Mapeamento de rotas para filial
 *
 * @name FilialRouter
 * @module routes/api/modulo/dados-empresa/filial
 * @category Routes
 * @subcategory Admin
*/
class FilialRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
  */
  modulo() { return 'dados-empresa'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    return ModuloFilialService.getInstance();
  }

    
}

module.exports = { FilialRouter };