const {
  ModuloCrudRouter
} = require('../../../../abstracts/router/modulo/modulo-crud-router');
const {
  ModuloFuncionarioService
} = require('../../../../services/modulo/dados-empresa/funcionario');

/**
 * Mapeamento de rotas para empresa
 * 
 * @name FuncionarioRouter
 * @module routes/api/modulo/dados-empresa/funcionario
 * @category Routes
 * @subcategory Admin
*/
class FuncionarioRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   * 
   * @returns {String}
   * 
  */
  modulo() { return 'dados-empresa'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    return ModuloFuncionarioService.getInstance();
  }
}

module.exports = { FuncionarioRouter };