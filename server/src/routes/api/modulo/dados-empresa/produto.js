const {
  ModuloCrudRouter
} = require('../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloReferencedDocumentCrudService
} = require('../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../commons/model-factory');

/**
 * Mapeamento de rotas para produtos
 *
 * @name ProdutoRouter
 * @module routes/api/modulo/dados-empresa/produto
 * @category Routes
 * @subcategory Admin
*/
class ProdutoRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
  */
  modulo() { return 'dados-empresa'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    const field = 'produtos'
    const modelWrapper = ModelFactory.getModel('Produto');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo Produto.');
    }
    const parentIdUrlName = 'dadosEmpresa'
    const parentModelWrapper = ModelFactory.getModel('DadosEmpresa');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo DadosEmpresa.');
    }
    return ModuloReferencedDocumentCrudService.getInstance({
      parent: {
        persistence: parentModelWrapper,
        field
      },
      persistence: modelWrapper,
      parentIdUrlName
      // ...queries
     });
  }

}

module.exports = { ProdutoRouter };