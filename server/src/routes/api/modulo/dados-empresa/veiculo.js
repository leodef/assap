const {
  ModuloCrudRouter
} = require('../../../../abstracts/router/modulo/modulo-crud-router');
const {
  ModuloVeiculoService
} = require('../../../../services/modulo/dados-empresa/veiculo');

/**
 * Mapeamento de rotas para veiculo
 * 
 * @name VeiculoRouter
 * @module routes/api/modulo/dados-empresa/veiculo
 * @category Routes
 * @subcategory Admin
*/
class VeiculoRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   * 
   * @returns {String}
   * 
  */
 modulo() { return 'dados-empresa'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    return ModuloVeiculoService.getInstance()
  }
}

module.exports = { VeiculoRouter };