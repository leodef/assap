const express = require("express");

const { EmpresaUsuarioRouter } = require("./empresa-usuario");
const { EntregaRouter } = require("./entrega");
const { FilialRouter } = require("./filial");
const { FuncionarioRouter } = require("./funcionario");
const { ParceiroRouter } = require("./parceiro");
const { ProdutoRouter } = require("./produto");
const { VeiculoRouter } = require("./veiculo");

/**
 * Mapeamento de rotas para gerenciamento de dados da empresa
 * @namespace
 * @name dadosEmpresaRoutes
 */
const routes = () => {
  const router = express.Router({ mergeParams: true });

  /**
   * Mapeamento de rotas para gerenciamento de entregas
   * @memberof dadosEmpresaRoutes
   */
  router.use(
    "/empresa-usuario",
    new EmpresaUsuarioRouter().router()
  );

  /**
   * Mapeamento de rotas para gerenciamento de entregas
   * @memberof dadosEmpresaRoutes
   */
  router.use("/:dadosEmpresa/entrega", new EntregaRouter().router());

  /**
   * Mapeamento de rotas para gerenciamento de filiais
   * @memberof dadosEmpresaRoutes
   */
  router.use("/:dadosEmpresa/filial", new FilialRouter().router());

  /**
   * Mapeamento de rotas para gerenciamento de funcionários
   * @memberof dadosEmpresaRoutes
   */
  router.use("/:dadosEmpresa/funcionario", new FuncionarioRouter().router());
  /**
   * Mapeamento de rotas para gerenciamento de parceiros
   * @memberof dadosEmpresaRoutes
   */
  router.use("/:dadosEmpresa/parceiro", new ParceiroRouter().router());

  /**
   * Mapeamento de rotas para gerenciamento de produtos
   * @memberof dadosEmpresaRoutes
   */
  router.use("/:dadosEmpresa/produto", new ProdutoRouter().router());

  /**
   * Mapeamento de rotas para gerenciamento de veiculos
   * @memberof dadosEmpresaRoutes
   */
  router.use("/:dadosEmpresa/veiculo", new VeiculoRouter().router());

  /**
   * Rota padrão, caso nenhuma seja requisitada
   * @memberof dadosEmpresaRoutes
   */
  router.use("/", function (req, res) {
    res.send("dadosEmpresa");
  });
  return router;
};

module.exports = routes;
