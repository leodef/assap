const {
  ModuloCrudRouter
} = require('../../../../abstracts/router/modulo/modulo-crud-router');
const {
  ModuloParceiroService
} = require('../../../../services/modulo/dados-empresa/parceiro');
/**
 * Mapeamento de rotas para parceiro
 * 
 * @name ParceiroRouter
 * @module routes/api/modulo/dados-empresa/parceiro
 * @category Routes
 * @subcategory Admin
*/
class ParceiroRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   * 
   * @returns {String}
   * 
  */
  modulo() { return 'dados-empresa'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    return ModuloParceiroService.getInstance();
  }

}

module.exports = { ParceiroRouter };