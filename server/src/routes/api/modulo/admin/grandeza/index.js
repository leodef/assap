const {
  ModuloCrudRouter
} = require('../../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDocumentCrudService
} = require('../../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../../commons/model-factory');
const { UnidadeMedidaRouter } = require('./unidade-medida');

/**
 * Mapeamento de rotas para grandeza
 * 
 * @name GrandezaRouter
 * @module routes/api/modulo/admin/grandeza
 * @category Routes
 * @subcategory Admin
*/
class GrandezaRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   * 
   * @returns {String}
   * 
  */
  modulo() { return 'admin'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   * 
  */
  service() {
    const modelWrapper = ModelFactory.getModel('Grandeza');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo Grandeza.');
    }
    return ModuloDocumentCrudService.getInstance({
      persistence: modelWrapper,
    });
  }

  /**
   * Mapeia os serviços derivados
   *
   * @param {any} router Objeto de registro de mapemamento
   * 
   * @returns {any}
   *
  */
  loadChild(router) {
    router.use(
      '/:grandeza/unidade-medida',
      new UnidadeMedidaRouter().router())
    return router;
  }
  
}

module.exports = { GrandezaRouter };