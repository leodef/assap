const {
  ModuloCrudRouter
} = require('../../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDocumentCrudService
} = require('../../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../../commons/model-factory');

/**
 * Mapeamento de rotas para unidade de medida
 *
 * @name UnidadeMedidaRouter
 * @module routes/api/modulo/admin/grandeza/unidade-medida
 * @category Routes
 * @subcategory Admin
*/
class UnidadeMedidaRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
  */
  modulo() { return 'admin'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    const modelWrapper = ModelFactory.getModel('UnidadeMedida');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo UnidadeMedida.');
    }
    return ModuloDocumentCrudService.getInstance({
      persistence: modelWrapper,
      urlFilters: {
        grandeza: { _id: 'grandeza' }
      }
    });
  }
    
}

module.exports = { UnidadeMedidaRouter };