const {
  ModuloCrudRouter
} = require('../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDocumentCrudService
} = require('../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../commons/model-factory');

/**
 * Mapeamento de rotas para tipo de informação de contato
 *
 * @name TipoContatoInfoRouter
 * @module routes/api/modulo/admin/tipo-contato-info
 * @category Routes
 * @subcategory Admin
*/
class TipoContatoInfoRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
  */
  modulo() { return 'admin'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    const modelWrapper = ModelFactory.getModel('TipoContatoInfo');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo TipoContatoInfo.');
    }
    return ModuloDocumentCrudService.getInstance({
      persistence: modelWrapper,
    });
  }
  
}

module.exports = { TipoContatoInfoRouter };