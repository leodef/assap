const {
  ModuloCrudRouter,
} = require("../../../../abstracts/router/modulo/modulo-crud-router");
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDocumentCrudService,
} = require("../../../../abstracts/service/modulo/modulo-crud-service");
const {
  ModelFactory
} = require("../../../../commons/model-factory");

/**
 * Mapeamento de rotas para dados da empresa
 *
 * @name EmpresaRouter
 * @module routes/api/modulo/admin/empresa
 * @category Routes
 * @subcategory Admin
 */
class EmpresaRouter extends ModuloCrudRouter {
  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
   */
  modulo() {
    return "admin";
  }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    const modelWrapper = ModelFactory.getModel("Empresa");
    const modelWrapperDadosEmpresa = ModelFactory.getModel("DadosEmpresa");
    if (!modelWrapper) {
      throw new Error(
        "Não foi possivel carregar a persistencia do modelo Empresa."
      );
    }
    const resolveItem = (item, documentPersistence) => {
      
    const modelDadosEmpresa = modelWrapperDadosEmpresa.model;
      if(item.dados) {
        return item;
      }
      return modelDadosEmpresa.create({}).then(
        (dados) => ({ ...item, dados })
      )
      
    }
    return ModuloDocumentCrudService.getInstance({
      persistence: modelWrapper,
      resolveItem
    });
  }

}

module.exports = { EmpresaRouter };
