const express =  require('express');

const { EmpresaRouter } = require('./empresa');
const { GrandezaRouter } = require('./grandeza');
const { TipoContatoInfoRouter } = require('./tipo-contato-info');

/**
 * Mapeamento de rotas para o modulo admin
 * @namespace
 * @name adminRoutes
 */
const routes = () => {
  const router = express.Router({mergeParams: true});


  /**
   * Mapeamento de rotas para gerenciamento de empresa
   * @memberof adminRoutes
  */
 router.use('/empresa', new EmpresaRouter().router());

  /**
   * Mapeamento de rotas para gerenciamento de grandeza
   * @memberof adminRoutes
  */
  router.use('/grandeza', new GrandezaRouter().router());

  /**
   * Mapeamento de rotas do modulo tipo de contato para informação
   * @memberof adminRoutes
  */
  router.use('/tipo-contato-info', new TipoContatoInfoRouter().router());

  /**
   * Rota padrão, caso nenhuma seja requisitada
   * @memberof adminRoutes
  */
  router.use('/', function(req, res){
    res.send('admin');
  });
  return router;
};

module.exports = routes;
