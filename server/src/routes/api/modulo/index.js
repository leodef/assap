const express = require('express');
const adminRoutes = require('./admin');
const calcCompQuimRoutes = require('./calc-comp-quim');
const calendarioRoutes = require('./calendario');
const dadosEmpresaRoutes = require('./dados-empresa');
const logisticaRoutes = require('./logistica');

/**
 * Mapeamento de rotas para Modulo
 * @namespace
 * @name moduloRoutes
 */
const routes = () => {
  const router = express.Router({ mergeParams: true });

  /**
   * Mapeamento de rotas do modulo Admin
   * @memberof moduloRoutes
  */
  router.use('/admin', adminRoutes());

  /**
   * Mapeamento de rotas do modulo CalcCompQuim
   * @memberof moduloRoutes
  */
  router.use('/calc-comp-quim', calcCompQuimRoutes());

  /**
   * Mapeamento de rotas do modulo Calendario
   * @memberof moduloRoutes
  */
  router.use('/calendario', calendarioRoutes());

  /**
   * Mapeamento de rotas para dados de empresa
   * @memberof adminRoutes
  */
  router.use('/dados-empresa', dadosEmpresaRoutes());

  /**
   * Rotas do modulo Logistica
   * @memberof moduloRoutes
   */
  router.use('/logistica', logisticaRoutes());

  /**
   * Rota padrão, caso nenhuma seja requisitada
   * @memberof moduloRoutes
  */
  router.use('/', function (req, res, next) {
    res.send('modulo');
  });

  return router;
};

module.exports = routes;
