const express =  require('express');

const { CCQComposicaoSubstanciaRouter } = require('./composicao-substancia');
const { CCQSubstanciaRouter } = require('./substancia');
const { CCQElementoRouter } = require('./elemento');
/**
 * Mapeamento de rotas para o modulo CalcCompQuim
 * @namespace
 * @name calcCompQuimRoutes
 */
const routes = () => {
  const router = express.Router({mergeParams: true});
  
  /**
   * Mapeamento de rotas para gerenciamento de composicao de substancia
   * @memberof adminRoutes
  */
  router.use('/composicao-substancia', new CCQComposicaoSubstanciaRouter().router());
  
  /**
   * Mapeamento de rotas para gerenciamento de substancia
   * @memberof adminRoutes
  */
  router.use('/substancia', new CCQSubstanciaRouter().router());
    
  /**
   * Mapeamento de rotas para gerenciamento de elemento
   * @memberof adminRoutes
  */
  router.use('/elemento', new CCQElementoRouter().router());

  /**
   * Rota padrão, caso nenhuma seja requisitada
   * @memberof adminRoutes
  */
  router.use('/', function(req, res){
    res.send('calc-comp-quim');
  });
  return router;
};

module.exports = routes;