const {
  ModuloCrudRouter
} = require('../../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDocumentCrudService
} = require('../../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../../commons/model-factory');
const {
  CCQQuantidadeSubstanciaRouter
} = require('./quantidade-substancia');

/**
 * Mapeamento de rotas para composição de substancia
 *
 * @name CCQComposicaoSubstanciaRouter
 * @module routes/api/modulo/calc-comp-quim/composicao-substancia
 * @category Routes
 * @subcategory Admin
*/
class CCQComposicaoSubstanciaRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
  */
  modulo() { return 'calcCompQuim'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
      const modelWrapper = ModelFactory.getModel('CCQComposicaoSubstancia');
      if(!modelWrapper) {
        throw new Error('Não foi possivel carregar a persistencia do modelo CCQComposicaoSubstancia.');
      }
      return ModuloDocumentCrudService.getInstance({
          persistence: modelWrapper,
          nextQuery: this.nextQuery.bind(this)
      });
  }

  /**
   * Mapeia os serviços derivados
   *
   * @param {any} router Objeto de registro de mapemamento
   * 
   * @returns {any}
   *
  */
  loadChild(router){
      router.use('/:composicao-substancia/substancia', new CCQQuantidadeSubstanciaRouter().router());
      return router;
  }

  /**
   * Gera a query para ser executada posterior a qualquer outra query na base
   *
   * @param {any} params Parametros para gerar a query
   * 
   * @returns {Array<any>}
   *
  */
  nextQuery(params) {
      return [
          {
            '$unwind': '$substancias'
          }, {
            '$lookup': {
              'from': 'ccqsubstancias', 
              'localField': 'substancias.substancia', 
              'foreignField': '_id', 
              'as': 'substancias.substancia'
            }
          }, {
            '$unwind': '$substancias.substancia'
          }, {
            '$group': {
              '_id': '$_id', 
              'root': {
                '$mergeObjects': '$$ROOT'
              }, 
              'substancias': {
                '$push': '$substancias'
              }
            }
          }, {
            '$replaceRoot': {
              'newRoot': {
                '$mergeObjects': [
                  '$root', '$$ROOT'
                ]
              }
            }
          }, {
            '$project': {
              'root': 0
            }
          }
        ]
  }
}

module.exports = { CCQComposicaoSubstanciaRouter };