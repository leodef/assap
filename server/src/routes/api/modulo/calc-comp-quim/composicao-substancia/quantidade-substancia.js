const {
  ModuloCrudRouter
} = require('../../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloNestedDocumentCrudService
} = require('../../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../../commons/model-factory');

/**
 * Mapeamento de rotas para quantidade de substancia
 *
 * @name CCQQuantidadeSubstanciaRouter
 * @module routes/api/modulo/calc-comp-quim/composicao-substancia/quantidade-substancia
 * @category Routes
 * @subcategory Admin
*/
class CCQQuantidadeSubstanciaRouter extends ModuloCrudRouter {
 
  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
  */
  modulo() { return 'calcCompQuim'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    const modelWrapper = ModelFactory.getModel('CCQQuantidadeSubstancia');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo CCQQuantidadeSubstancia.');
    }
    const parentodelWrapper = ModelFactory.getModel('CCQComposicaoSubstancia');
    if(!parentodelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo CCQComposicaoSubstancia.');
    }
    return ModuloNestedDocumentCrudService.getInstance({
      persistence: modelWrapper,
      parentIdUrlName: 'composicaoSubstancia',
      parent: {
        field: 'substancias',
        persistence: parentodelWrapper
      }
    });
  }
  
}

module.exports = { CCQQuantidadeSubstanciaRouter };