const {
  ModuloCrudRouter
} = require('../../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDocumentCrudService
} = require('../../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../../commons/model-factory');
const {
  CCQQuantidadeElementoRouter
} = require('./quantiade-elemento');

/**
 * Mapeamento de rotas para composição de substancia
 *
 * @name CCQSubstanciaRouter
 * @module routes/api/modulo/calc-comp-quim/substancia
 * @category Routes
 * @subcategory Admin
*/
class CCQSubstanciaRouter extends ModuloCrudRouter {
 
  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
  */
  modulo() { return 'calcCompQuim'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    const modelWrapper = ModelFactory.getModel('CCQSubstancia');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo CCQSubstancia.');
    }
    return ModuloDocumentCrudService.getInstance({
      persistence: modelWrapper,
      nextQuery: this.nextQuery.bind(this)
    });
  }

  /**
   * Mapeia os serviços derivados
   *
   * @param {any} router Objeto de registro de mapemamento
   * 
   * @returns {any}
   *
  */
  loadChild(router){
    router.use('/:substancia/elemento', new CCQQuantidadeElementoRouter().router());
    return router;
  }

  /**
   * Gera a query para ser executada posterior a qualquer outra query na base
   *
   * @param {any} params Parametros para gerar a query
   * 
   * @returns {Array<any>}
   *
  */
  nextQuery(params) {
    return [
      {
        '$unwind': '$elementos'
      }, {
        '$lookup': {
        'from': 'ccqelementos', 
        'localField': 'elementos.elemento', 
        'foreignField': '_id', 
        'as': 'elementos.elemento'
        }
      }, {
        '$unwind': '$elementos.elemento'
      }, {
        '$group': {
        '_id': '$_id', 
        'root': {
          '$mergeObjects': '$$ROOT'
        }, 
        'elementos': {
          '$push': '$elementos'
        }
        }
      }, {
        '$replaceRoot': {
        'newRoot': {
          '$mergeObjects': [
          '$root', '$$ROOT'
          ]
        }
        }
      }, {
        '$project': {
        'root': 0
        }
      }
    ]
  }
}

module.exports = { CCQSubstanciaRouter };