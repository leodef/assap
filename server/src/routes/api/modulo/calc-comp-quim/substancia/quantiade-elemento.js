const {
  ModuloCrudRouter
} = require('../../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloNestedDocumentCrudService
} = require('../../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../../commons/model-factory');

/**
 * Mapeamento de rotas para quantidade de elementos
 *
 * @name CCQQuantidadeElementoRouter
 * @module routes/api/modulo/calc-comp-quim/substancia/quantidade-elemento
 * @category Routes
 * @subcategory Admin
*/
class CCQQuantidadeElementoRouter extends ModuloCrudRouter {
 
  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
  */
  modulo() { return 'calcCompQuim'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    const modelWrapper = ModelFactory.getModel('CCQQuantidadeElemento');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo CCQQuantidadeElemento.');
    }
    const parentodelWrapper = ModelFactory.getModel('CCQSubstancia');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo CCQSubstancia.');
    }
    return ModuloNestedDocumentCrudService.getInstance({
      persistence: modelWrapper,
      parentIdUrlName: 'substancia',
      parent: {
        field: 'elementos',
        persistence: parentodelWrapper
      }
    });
  }
  
}

module.exports = { CCQQuantidadeElementoRouter };