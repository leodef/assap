const {
  ModuloCrudRouter
} = require('../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDocumentCrudService
} = require('../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../commons/model-factory');
/**
 * Mapeamento de rotas para quantidade de elementos
 *
 * @name CCQElementoRouter
 * @module routes/api/modulo/calc-comp-quim/elemento
 * @category Routes
 * @subcategory Admin
*/
class CCQElementoRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
  */
  modulo() { return 'calcCompQuim'; }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    const modelWrapper = ModelFactory.getModel('CCQElemento');
    if(!modelWrapper) {
      throw new Error('Não foi possivel carregar a persistencia do modelo CCQElemento.');
    }
    return ModuloDocumentCrudService.getInstance({
      persistence: modelWrapper,
    });
  }
  
}

module.exports = { CCQElementoRouter };