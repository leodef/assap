const express =  require('express');
const { EntregaRouter } = require('./entrega');

/**
 * Mapeamento de rotas para o modulo Logistica
 * @namespace
 * @name calcCompQuimRoutes
 */
const routes = () => {
  const router = express.Router({mergeParams: true});

  /**
   * Mapeamento de rotas para gerenciamento de entrega
   * @memberof adminRoutes
  */
  router.use('/entrega', new EntregaRouter().router());

  /**
   * Rota padrão, caso nenhuma seja requisitada
   * @memberof adminRoutes
  */
  router.use('/', function(req, res){
    res.send('logistica');
  });

  return router;
};

module.exports = routes;