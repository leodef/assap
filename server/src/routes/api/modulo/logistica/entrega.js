const {
  ModuloCrudRouter,
} = require("../../../../abstracts/router/modulo/modulo-crud-router");

const {
  ModuloLogisticaEntregaService,
} = require("../../../../services/modulo/logistica/entrega");

/**
 * Mapeamento de rotas para entregas
 *
 * @name EntregaRouter
 * @module routes/api/modulo/logistica/entrega
 * @category Routes
 * @subcategory Admin
*/
class EntregaRouter extends ModuloCrudRouter {

  /**
   * Informa o nome do modulo em que os serviços estão situados
   *
   * @returns {String}
   *
  */
  modulo() {
    return 'logistica';
  }

  /**
   * Retorna os serviço para responder as requisições
   *
   * @returns {CrudService}
   *
   */
  service() {
    return ModuloLogisticaEntregaService.getInstance();
  }

  /**
   * Mapeia os serviços derivados
   *
   * @param {any} router Objeto de registro de mapemamento
   * 
   * @returns {any}
   *
  */
  loadChild(router) {
    const service = this.service();
    router.post.apply(router, [
      "/calculo-rota",
      ...this.getTaks(service.calculoRota.bind(service), "calculoRota"),
    ]);
    return router;
  }
}

module.exports = { EntregaRouter };
