const crud = require('../../../commons/crud');
  

function auth(req, res, service) {
  return true;
}

 /**
 * Serviço padrão para as operações de CRUD de um documento 
 * com a proteção padrão para serviços dentro de 'modules'
 * 
 * @name ModuloDocumentCrudService
 * @module abstracts/service/modulo
 * @category Abstrato
 * @subcategory Serviço
 */
class ModuloDocumentCrudService extends crud.DocumentCrudService {

    constructor(config) { super(config); }

    auth(req, res) { return auth(req, res, this); }

    static getInstance(config) { return new ModuloDocumentCrudService(config); }
}
 /**
 * Serviço padrão para as operações de CRUD de um documento
 * embutido dentro de outro documento.
 * com a proteção padrão para serviços dentro de 'modules'
 * 
 * @name ModuloNestedDocumentCrudService
 * @module abstracts/service/modulo
 * @category Abstrato
 * @subcategory Serviço
 */
class ModuloNestedDocumentCrudService extends crud.NestedDocumentCrudService {

  constructor(config) { super(config); }

  auth(req, res) { return auth(req, res, this); }

  static getInstance(config) { return new ModuloNestedDocumentCrudService(config); }
}
 /**
 * Serviço padrão para as operações de CRUD de referencias
 * de um documento por outro, através de uma lista de referencias.
 * com a proteção padrão para serviços dentro de 'modules'
 * 
 * @name ModuloReferencedDocumentCrudService
 * @module abstracts/service/modulo
 * @category Abstrato
 * @subcategory Serviço
 */
class ModuloReferencedDocumentCrudService extends crud.ReferencedDocumentCrudService {

  constructor(config) { super(config); }

  auth(req, res) { return auth(req, res, this); }

  static getInstance(config) { return new ModuloReferencedDocumentCrudService(config); }
}

module.exports = {
  ModuloDocumentCrudService,
  ModuloNestedDocumentCrudService,
  ModuloReferencedDocumentCrudService
};