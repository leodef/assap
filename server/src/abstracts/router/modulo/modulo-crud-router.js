const crud = require('../../../commons/crud');
const Auth = require('../../../config/auth');
const authTask = Auth.authTask();

 /**
 * Roteamento padrão para as operações de CRUD de um documento 
 * com a proteção padrão para rotas derivadas de 'modules'
 * 
 * @name ModuloCrudRouter
 * @module abstracts/router/modulo
 * @category Abstrato
 * @subcategory Roteamento
 */
class ModuloCrudRouter extends crud.CrudRouter {
/*##!!
  getAuthTask() {
    return authTask;
  }
*/
}

module.exports = { ModuloCrudRouter };
