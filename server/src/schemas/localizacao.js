const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Localização.
 * @typedef {Object} Localizacao
 * @property {String} titulo Titulo da localização.
 * @property {String} endereco Endereço completo.
 * @property {Number} lat Latidude.
 * @property {Number} long Longitude.
 * @property {String} pais Pais.
 * @property {String} estado Estado.
 * @property {String} cidade Cidade.
 * @property {String} area Area ou bairro.
 * @property {String} codigoPostal Codigo postal.
 * @property {String} numero Numero da residencia.
 * @property {String} complemento Dados adicionais.
*/

/**
 * Função para gerar o esquema
 * @returns{Localizacao}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      endereco: String,
      lat: { type: Number, min: -90 , max: 90 },
      long: { type: Number, min: -180 , max: 180 },
      pais: String,
      estado: String,
      cidade: String,
      area: String,
      codigoPostal: String,
      numero: String,
      complemento: String
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Localizacao';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};