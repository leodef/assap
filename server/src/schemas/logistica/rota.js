const mongoose = require('mongoose');
const ALGORITIMO_ROTA = require('../../enums/algoritimo-rota.enum');
const TIPO_LIMITE_ROTA = require('../../enums/tipo-limite-rota.enum');
const Schema = mongoose.Schema;
/**
 * Rota de entrega
 * @typedef {Object} Rota
 * @property {String} algoritimo Algoritimo utilizado no calculo da rota.
 * @property {Number} distancia Distância total da rota.
 * @property {Number} duracao Duração total da rota.
 * @property {Number} duracaoComTrafego Duração total da rota considerando tráfego.
 * @property {Boolean} calculo Calculo da rota realizado.
 * @property {Array<PontoRota>} pontos Pontos da rota.
 * @property {String} tipoLimite Origem do limite da entrega da entrega
 * @property {Limite} limite Limite maximo considerado para a rota.
 */

/**
 * Função para gerar o esquema
 * @returns {Rota}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      algoritimo: { type: String, enum: ALGORITIMO_ROTA },
      distancia: Number,
      duracao: Number,
      duracaoComTrafego: Number,
      calculo: Boolean,
      pontos: [params.depedencies.PontoRota.schema],
      tipoLimite: { type: String, enum: TIPO_LIMITE_ROTA },
      limite: params.depedencies.Limite.schema
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Rota';

/**
 * Dependencias do modelo
 */
const depedencies = [
  'PontoRota',
  'Limite'
];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};