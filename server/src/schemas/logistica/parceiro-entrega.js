const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Parceiro vinculado ao transporte
 * @typedef {Object} ParceiroEntrega
 * @property {Parceiro} parceiro Parceiro vinculado ao transporte.
 * @property {Contato} contato Contato do parceiro vinculado a entrega.
*/

/**
 * Função para gerar o esquema
 * @returns {ParceiroEntrega}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      parceiro: { type: Schema.Types.ObjectId, ref: 'Parceiro' },
      contato: params.depedencies.ContatoParceiro.schema
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'ParceiroEntrega';

/**
 * Dependencias do modelo
 */
const depedencies = ['ContatoParceiro'];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path:'parceiro', ref: 'Parceiro'}
  // {path:'contato', ref: 'ContatoParceiro'}
];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};