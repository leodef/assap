const mongoose = require('mongoose')
const moment = require('moment')
const STATUS_ENTREGA = require('../../enums/status-entrega.enum')
const TIPO_FORMACAO = require('../../enums/tipo-formacao.enum')
const { DTO } = require('../../commons/dto')
const cleanLabel = DTO.cleanLabel
const Schema = mongoose.Schema;
/**
 * Entrega
 * @typedef {Object} Entrega
 * @property {String} titulo Titulo da entrega
 * @property {String} tipoFormacaoTitulo Modo de geração do titulo da entrega, automatico ou manual
 * @property {String} desc Descrição da entrega
 * @property {String} tipoFormacaoDesc Modo de geração da descrição da entrega, automatico ou manual
 * @property {Date} data Data da entrega
 * @property {Rota} rota Rota da entrega
 * @property {String} status Status da entrega, iniciada ou concluida
 * @property {ParceiroEntrega} parceiroEntrega Informação sobre o parceiro relacionado da entrega
 * @property {Transporte} transporte Informação sobre o transporte da entrega
 * @property {FuncionarioEntrega} funcionarios Titulo da entrega
 */

/**
 * Função para gerar o esquema
 * @returns {Entrega}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      tipoFormacaoTitulo: { type: String, enum: TIPO_FORMACAO },
      desc: String,
      tipoFormacaoDesc: { type: String, enum: TIPO_FORMACAO }, 
      data: Date,
      produto: { type: Schema.Types.ObjectId, ref: 'Produto' },
      rota: params.depedencies.Rota.schema,
      status: { type: String, enum: STATUS_ENTREGA },
      parceiroEntrega: params.depedencies.ParceiroEntrega.schema,
      transporte: params.depedencies.Transporte.schema,
      funcionarios: [
        params.depedencies.FuncionarioEntrega.schema
      ],
      compromisso:  { type: Schema.Types.ObjectId, ref: 'Compromisso' },
      criarCompromisso: Boolean
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Entrega';

/**
 * Dependencias do modelo
 */
const depedencies = [
  'Rota',
  'ParceiroEntrega',
  'Transporte',
  'FuncionarioEntrega'
];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path:'produto', ref: 'Produto'},
  {path:'transporte.veiculo', ref: 'Veiculo'},
  {path:'transporte.carreta', ref: 'Carreta'},
  {path:'parceiroEntrega.parceiro', ref: 'Parceiro'},
  {path:'parceiroEntrega.contato.filial', ref: 'Filial'},
  {path:'funcionarios.funcionario', ref: 'Funcionario'},
  {path:'funcionarios.funcionario.filiais', ref: 'Filial'},
  {path:'rota.pontos.anterior', ref: 'PontoRota'},
  {path:'rota.pontos.parceiro', ref: 'Parceiro'},
  {path:'rota.pontos.filial', ref: 'Filial'},
  {path:'compromisso', ref: 'Compromisso'}
];

const generateTitulo= (obj) => {
  if(!obj) {
    return ''
  }
  const { parceiroEntrega, transporte, produto } = obj
  let result = [
    (
      produto && produto.titulo
        ? produto.titulo
        : null
    ),
    (
      parceiroEntrega && parceiroEntrega.parceiro && parceiroEntrega.parceiro.titulo
        ? parceiroEntrega.parceiro.titulo
        : null
      ),
    (
      obj.data ? moment(obj.data).format('DD/MM/YYYY') : null
    ),
    (
      transporte && transporte.veiculo
        ? transporte.veiculo.placa
        : null
    )
  ].filter((val) => Boolean(val)).join(' ')
  return cleanLabel(result)
}

const generateDesc = (obj) => {
  if(!obj) {
    return ''
  }
  const {
    rota,
    parceiroEntrega,
    transporte,
    funcionarios,
    compromisso
  } = obj;

  // pontoRota
  const pontoRotaDesc = (ponto, index) => {
    const parentSpace = '    '
    const space = '      '
    const comPontoAnterior = Boolean(ponto.distanciaDesc || ponto.distancia)
    return cleanLabel(`${
      parentSpace + 'Parada Rota '+ ponto.ordem + ':\n'
    }${
      ponto.localizacao &&  ponto.localizacao.titulo
        ? space + 'Localização: ' + ponto.localizacao.titulo + '\n'
        : ''
    }${
      comPontoAnterior 
        ? space + 'Trajeto da parada anterior até a atual:\n'
        : ''
    }${
      comPontoAnterior
        ? space + 'Distância: ' + ponto.distanciaDesc || ponto.distancia + '\n'
        : ''
    }${
      comPontoAnterior && (ponto.duracaoDesc || ponto.duracao)
        ? space + 'Duração: ' + ponto.duracaoDesc || ponto.duracao + '\n'
        : ''
    }${
      comPontoAnterior && (ponto.duracaoComTrafegoDesc || ponto.duracaoComTrafego)
        ? space + 'Duração com Tráfego: ' + ponto.duracaoComTrafegoDesc || ponto.duracaoComTrafego + '\n'
        : ''
    }${
      ponto.anterior
        ? space + 'Anterior: ' + ponto.anterior.map( (ponto) => ponto.titulo).join(', ') + '\n'
        : ''
    }${
      ponto.status
        ? space + 'Status: ' + ponto.status + '\n'
        : ''
    }${
        ponto._id || ponto._temp
        ? space + 'ID: ' + (ponto._id || ponto._temp) + '\n'
        : ''
    }
    `)
  }

  // data
  const dataDesc = obj.data? 'Data: '+moment(obj.data).format('DD/MM/YYYY') + '\n' : ''

  // rotaLimite
  const rotaLimiteSpace = '      '
  const rotaLimiteDesc = rota && rota.limite
    ? `${
        rota.limite.peso
          ? rotaLimiteSpace + 'Peso: ' + rota.limite.peso
          : ''
      }\n${
        rota.limite.volume
          ? rotaLimiteSpace + 'Volume: : ' + rota.limite.volume
          : ''
      }\n${
        rota.limite.tara
          ? rotaLimiteSpace + 'Peso de veículo: ' + rota.limite.tara
          : ''
      }\n`
    : ''

  // rota
  const rotaSpace = '    '
  const rotaPontoSpace = '      '
  const rotaComLimite = Boolean(rota.limite)
  const rotaDesc = rota
    ? `${
      'Rota:\n' 
    }${
        rota.algoritimo
          ? rotaSpace + 'Algoritimo: ' + rota.algoritimo + '\n'
          : ''
      }${
        rota.distancia
          ? rotaSpace + 'Distância: ' + rota.distancia + '\n'
          :  ''
      }${
        rota.duracao
          ? rotaSpace + 'Duração: ' + rota.duracao + '\n'
          : ''
      }${
        rota.duracaoComTrafego
          ? rotaSpace + 'Duração com Tráfego: ' + rota.duracaoComTrafego + '\n'
          : ''
      }${
        rota.calculo
            ? rotaSpace + 'Cálculo da rota realizado\n'
            : rotaSpace + 'Cálculo da rota não realizado\n'
      }${
        rota.pontos ? rotaSpace + 'Pontos: \n'
          + rotaPontoSpace
          +  '----\n'
          + (rota.pontos || [])
          .map(pontoRotaDesc)
          .join(rotaPontoSpace + '----\n') : ''
      }${
        rotaComLimite
          ? rotaSpace + 'Limite:' + '\n'
          : ''
      }${
        rotaComLimite && rota.tipo
          ? rotaLimiteSpace + 'Tipo: ' + rota.tipo + '\n'
          : ''
      }${
        rotaComLimite
          ? rotaLimiteDesc + '\n'
          : ''
      }`
    : ''
  
  const parceiroEntregaSpace = '    '
  const parceiroEntregaDesc = parceiroEntrega
    ? `${
        'Parceiro comercial envolvido na entrega:\n'
      }${
        parceiroEntrega && parceiroEntrega.parceiro && parceiroEntrega.parceiro.titulo
          ? parceiroEntregaSpace + 'Parceiro: ' + parceiroEntrega.parceiro.titulo + '\n'
          : ''
      }${
        parceiroEntrega && parceiroEntrega.contato && parceiroEntrega.contato.titulo
          ? parceiroEntregaSpace + 'Contato: ' + parceiroEntrega.contato.titulo + '\n'
          : ''
      }`
    : ''

  const transporteSpace = '    '
  const transporteDesc = transporte
    ? `${
        'Transporte:\n'
      }${
        transporte.veiculo && transporte.veiculo.titulo
          ? transporteSpace + 'Veículo: ' + transporte.veiculo.titulo + '\n'
          : ''
      }${
        transporte.carreta && transporte.carreta.titulo
          ? transporteSpace + 'Carreta: ' + transporte.carreta.titulo + '\n'
          : ''
      }`
    : ''
  
  const funcionariosSpace = '    '
  const funcionariosDesc = funcionarios
    ? `${
        'Funcionários:\n'
      }${
        funcionarios.map(
          (func) => `${funcionariosSpace}${func.tipo}: ${func.funcionario
            ? func.funcionario.nome
            : ''}`).join(', ') + '\n'
      }` : ''
  const compromissoDesc = compromisso && compromisso.titulo
    ? `${
      'Evento no calendário:\n'
    }${
      compromisso.titulo
        ? 'Compromisso: ' + compromisso.titulo + '\n'
        : ''}
  ` : ''
    
  const result = `${
      'Entrega:\n'
    }${
      obj.status
        ? 'Status: ' + obj.status + '\n'
        : ''
    }${
      obj.produto
        ? 'Produto: ' + (obj.produto.titulo || obj.produto) + '\n'
        : ''
    }${
      dataDesc
    }${
      rotaDesc
    }${
      parceiroEntregaDesc
    }${
      transporteDesc
    }${
      funcionariosDesc
    }${
      compromissoDesc
    }`
  return cleanLabel(result)
}

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
  config: {
    generateTitulo,
    generateDesc
  }
};