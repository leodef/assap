const mongoose = require('mongoose');
const TIPO_FUNCIONARIO_ENTREGA = require('../../enums/tipo-funcionario-entrega.enum');
const Schema = mongoose.Schema;
/**
 * Funcionario vinculado ao transporte
 * @typedef {Object} FuncionarioEntrega
 * @property {Funcionario} funcionario Funcionario vinculado ao transporte.
 * @property {String} tipo Tipo de interação do funcionário com a entrega. 'vendedor' ou 'motorista'.
*/
/**
 * Função para gerar o esquema
 * @returns FuncionarioEntrega
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      funcionario: { type: Schema.Types.ObjectId, ref: 'Funcionario' },
      tipo: { type: String, enum: TIPO_FUNCIONARIO_ENTREGA }
    }, {timestamps: true}); 

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'FuncionarioEntrega';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path:'funcionario', ref: 'Funcionario'}
];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};