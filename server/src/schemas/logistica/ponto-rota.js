const mongoose = require('mongoose')
const TIPO_FORMACAO = require('../../enums/tipo-formacao.enum')
const STATUS_ENTREGA = require('../../enums/status-entrega.enum')
const { DTO } = require('../../commons/dto')
const cleanLabel = DTO.cleanLabel
const Schema = mongoose.Schema;
/**
 * PontoRota.
 * @typedef {Object} PontoRota
 * @property {String} titulo - Titulo do ponto da rota.
 * @property {String} desc - Descrição do ponto da rota.
 * @property {Number} ordem - Ordem do ponto na rota.
 * @property {Number} distancia - Distância do ponto anterior até o ponto da rota.
 * @property {Number} duracao - Duração do trajeto do ponto anterior até o ponto da rota.
 * @property {Number} duracaoComTrafego - Duração com tráfego do trajeto do ponto anterior até o ponto da rota.
 * @property {String} distanciaDesc - Descrição formatada da distancia.
 * @property {String} duracaoDesc - Descrição formatada da duração.
 * @property {String} duracaoComTrafegoDes - Descrição formatada da duração com tráfego.
 * @property {Localizacao} localizacao - Localização do ponto da rota.
 * @property {Limite} limite - Atualização do consumo do limite da rota.
 * @property {Array<Number>} anterior - Ordem dos pontos que devem ser alcançados antes do atual.
 * @property {String} status - Statos do acesso ao ponto, se ja foi feito e como sucedeu.
 */

/**
 * Função para gerar o esquema
 * @returns {PontoRota}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      tipoFormacaoTitulo: { type: String, enum: TIPO_FORMACAO },
      desc: String,
      tipoFormacaoDesc: { type: String, enum: TIPO_FORMACAO },
      ordem: Number,
      distancia: Number,
      duracao: Number,
      duracaoComTrafego: Number,
      distanciaDesc: String,
      duracaoDesc: String,
      duracaoComTrafegoDesc: String,
      localizacao: params.depedencies.Localizacao.schema,
      limite: params.depedencies.Limite.schema,
      anterior: [{ type: Schema.Types.ObjectId, ref: 'PontoRota'} ],
      status: { type: String, enum: STATUS_ENTREGA },
      _temp: { type: String, required: false },
      usarFilial: Boolean,
      parceiro: { type: Schema.Types.ObjectId, ref: 'Parceiro' },
      filial: { type: Schema.Types.ObjectId, ref: 'Filial' },
    }, {timestamps: true});
    // Criar FilialPontoRota
    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'PontoRota';

/**
 * Dependencias do modelo
 */
const depedencies = [
  'Localizacao',
  'Limite'
];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

const generateTitulo = (obj) => {
  if(!obj) {
    return ''
  }
  const { localizacao, filial, parceiro } = obj
  const limiteTitulo = obj.limite && obj.limite.peso
    ? `${obj.limite.peso > 0 ? 'Carga' : 'Descarga' }: ${Math.abs(obj.limite.peso)}`
    : ''
  const parceiroTitulo = parceiro
    ? parceiro.titulo
    : null
  const filialTitulo = filial
    ? filial.titulo
    : null

  const localizacaoTitulo = localizacao
    ? localizacao.endereco || ` lat: ${localizacao.lat}, lng: ${localizacao.long} `
    : null
  const descTitulo = [
    parceiroTitulo,
    filialTitulo,
    limiteTitulo,
    localizacaoTitulo
  ].filter((val) => Boolean(val)).join(' ')

  return cleanLabel(`#${obj.ordem} ${descTitulo}`)
}

const generateDesc = (obj) => {
  if(!obj) {
    return ''
  }
  const {
    limite,
    localizacao,
    parceiro,
    filial
  } = obj;

  const parceiroFilialDesc =  parceiro || filial
    ? [
        (parceiro && parceiro.titulo
          ? 'Parceiro: ' + parceiro.titulo
          : ''),
        (filial && filial.titulo
          ? 'Filial: ' + filial.titulo
          : '')
      ].filter(val => Boolean(val)).join(', ')+'\n'
    : ''

  const cargaDescargaDesc = limite
    ? `${
        limite.peso > 0
          ? 'Carga'
          : 'Descarga'
      }( ${
        limite.peso
        ? 'Peso: ' + limite.peso
        : ''
      } ${
        limite.volume
          ? 'Volume: ' + limite.volume
          : ''
      })\n`
    : ''
  const trajetoAnteriorSpace = '  '
  const trajetoAnteriorDesc = (obj.distanciaDesc || obj.distancia)
    ? `${
      'Trajeto da parada anterior até a atual:\n'
      }${[
        (obj.distanciaDesc || obj.distancia
          ? trajetoAnteriorSpace + 'Distância: ' + (obj.distanciaDesc || obj.distancia)
          : null),
        (obj.duracaoDesc || obj.duracao
          ? trajetoAnteriorSpace + 'Duração: ' + (obj.duracaoDesc || obj.duracao)
          : null),
        (obj.duracaoComTrafegoDesc || obj.duracaoComTrafego
          ? trajetoAnteriorSpace + 'Duração com Tráfego: ' + (obj.duracaoComTrafegoDesc || obj.duracaoComTrafego) 
          : null)
      ].filter(val => Boolean(val)).join(', ')
    }`
    : ''
 
  const localizacaoSpace = '  '
  const localizacaoDesc = localizacao ? `${
      localizacaoSpace + 'Localização:\n'
    }${
      localizacao.endereco
      ? 'Endereço ' + localizacao.endereco
      : ''
    }\n${
      localizacao.lat || localizacao.long
        ? `Lat / Lng: ${localizacao.lat} / ${localizacao.long}`
        : ''
    }\n${
      localizacao.pais
      ? 'Pais: ' + localizacao.pais
      : ''
    }, ${
      localizacao.estado
        ? 'Estado: ' + localizacao.estado
        : ''
    }, ${
      localizacao.cidade
        ? 'Cidade: ' + localizacao.cidade
        : ''
    }\n${
      localizacao.area
        ? 'Bairro: ' + localizacao.area
        : ''
    }, ${
        localizacao.numero
          ? 'Número ' + localizacao.numero
          : ''
    }, ${
      localizacao.complemento
        ? 'Complemento: ' + localizacao.complemento
        : ''
    }\n${
      localizacao.codigoPostal
        ? 'Código Postal ' + localizacao.codigoPostal
        : ''
    }\n` : ''
  
  return cleanLabel(`${
      'Parada Rota #' + obj.ordem + ':\n'
    }${
      trajetoAnteriorDesc
    }${
      obj.anterior
        ? 'Anterior: ' + obj.anterior
        .map((ant) => ant ? ant.titulo : null)
        .filter(val => Boolean(val))
        .join(', ') + '\n' 
        : ''
    }${
      cargaDescargaDesc
    }${
      obj.status
        ? 'Status: ' + obj.status + '\n' 
        : ''
    }${
      obj._id || obj._temp
        ? 'ID: ' + (obj._id || obj._temp) + '\n' 
        : ''
    }${
      parceiroFilialDesc
    }${
      localizacaoDesc
    }`)
}

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
  config: {
    generateTitulo,
    generateDesc
  }
};