const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Informação de transporte de uma entrega
 * @typedef {Object} Transporte
 * @property {Veiculo} veiculo Trator do transporte.
 * @property {Carreta} carreta Carreta do transporte.
*/

/**
 * Função para gerar o esquema
 * @returns {Transporte}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      veiculo: { type: Schema.Types.ObjectId, ref: 'Veiculo' },
      carreta: { type: Schema.Types.ObjectId, ref: 'Veiculo' }
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Transporte';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path:'veiculo', ref: 'Veiculo'}
];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};