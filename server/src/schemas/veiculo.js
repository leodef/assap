const mongoose = require('mongoose');
const TIPO_FORMACAO = require('../enums/tipo-formacao.enum')
const TIPO_FUNCAO_VEICULO = require('../enums/tipo-funcao-veiculo.enum')
const { DTO } = require('../commons/dto')
const cleanLabel = DTO.cleanLabel
const Schema = mongoose.Schema;
/**
 * Veiculo da empresa
 * @typedef {Object} Veiculo
 * @property {String} titulo Nome do veiculo.
 * @property {String} desc Descrição do veiculo.
 * @property {String} placa Placa do veiculo.
 * @property {String} marca Marca do veiculo.
 * @property {String} modelo Modelo do veiculo.
 * @property {Limite} limite Capacidade de carga do veiculo.
 * @property {Boolean} autonomo Se pode ter carga sem o auxilio de outro automovel.
 * @property {Boolean} trator Se tem tração própria, é uma calado.
 * @property {Array<Veiculo>} compatibilidade Quais outros veiculos, esse pode carregar.
*/


/**
 * Função para gerar o esquema
 * @returns {Veiculo}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      titulo: String,
      tipoFormacaoTitulo: { type: String, enum: TIPO_FORMACAO },
      desc: String,
      tipoFormacaoDesc: { type: String, enum: TIPO_FORMACAO },
      placa: String,
      marca: String,
      modelo: String,
      tipo: String,
      ano: Number,
      limite: params.depedencies.Limite.schema,
      autonomo: Boolean, // Capaz de transportar carga sozinho
      trator: Boolean, // Com tração própria
      // Veiculos que pode carregar
      compatibilidade: [{ type: Schema.Types.ObjectId, ref: 'Veiculo' }],
      funcao: { type: String, enum: TIPO_FUNCAO_VEICULO },
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Veiculo';

/**
 * Dependencias do modelo
 */
const depedencies = ['Limite'];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path:'compatibilidade', ref: 'Veiculo'}
];

const generateTitulo = (obj) => {
  if(!obj) {
    return ''
  }
  const result = [
    (obj.placa || null),
    (obj.modelo || null),
    (obj.marca || null),
    (obj.ano || null)
  ].filter(val => Boolean(val)).join(', ')
  return cleanLabel(result)
}

const generateDesc = (obj) => {
  if(!obj) {
    return ''
  }
  const limitDesc = obj.limite ? [
      'Limite:\n',
      (obj.limite.tara
        ? 'Peso do veículo: ' + obj.limite.tara 
        : null),
      (obj.limite.peso
        ? 'Peso: ' + obj.limite.peso
        : null),
      (obj.limite.volume
        ? 'Volume: ' + obj.limite.volume
        : null)
  ].filter(val => Boolean(val)).join(', ') : ''
  const compatibilidadeDesc = obj.compatibilidade
  ? 'Carretas:\n' + obj.compatibilidade
    .map((c) => c ? '    '+c.titulo : null)
    .filter(val => Boolean(val))
    .join('\n') 
  : ''

  return `${
    'Veiculo:\n'
  }${
    obj.placa ? 'Placa: ' + obj.placa : ''
  } ${
    obj.marca ? 'Marca: ' + obj.marca : ''
  } ${
      obj.modelo ? 'Modelo: ' + obj.modelo : ''
  } ${
    obj.tipo ? 'Tipo: ' + obj.tipo : ''
  } ${
    obj.ano ? 'Ano: ' + obj.ano : ''
  }\n${
    obj.autonomo
      ? 'Veículo é autônomo para carga'
      : 'Veículo não é autônomo para carga'
  }\n${
    obj.trator 
      ? 'Veículo tem tração própria'
      : 'Veículo não tem tração própria'
  }\n${
    limitDesc
  }\n${
    compatibilidadeDesc
  }`
}

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
  config: {
    generateTitulo,
    generateDesc
  }
};