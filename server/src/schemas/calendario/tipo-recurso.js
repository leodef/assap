const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Tipo de recurso de compromisso no calendário
 * @typedef {Object} TipoRecursoCompromisso
 * @property {String} titulo Titulo do tipo de recurso do compromisso.
 * @property {String} campo Campo utilizado para idenficiação.
 */

/**
 * Função para gerar o esquema
 * @returns {TipoRecursoCompromisso}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      campo: String,
    }, {timestamps: true});
    return schema;
}

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'TipoRecursoCompromisso';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

module.exports = {
    key,
    schemaFunc,
    depedencies,
    populate
}