const mongoose = require('mongoose');
const TIPO_COMPROMISSO = require('../../enums/tipo-compromisso.enum');
const TIPO_FORMACAO = require('../../enums/tipo-formacao.enum');
const Schema = mongoose.Schema;
/**
 * Compromisso no calendário
 * @typedef {Object} Compromisso
 * @property {String} titulo Titulo do compromisso.
 * @property {String} desc Descrição do compromisso.
 * @property {Date} inicio Inicio do compromisso.
 * @property {Date} fim Fim do compromisso.
 * @property {Object} recursos Recursos do compromisso.
 * @property {Boolean} diaInteiro Indica se o compromisso é o dia inteiro.
 * @property {String} tipo Tipo do compromisso.
 */

/**
 * Função para gerar o esquema
 * @returns {Compromisso}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      tipoFormacaoTitulo: { type: String, enum: TIPO_FORMACAO },
      desc: String,
      tipoFormacaoDesc: { type: String, enum: TIPO_FORMACAO },
      inicio: Date,
      fim: Date,
      recursos: Object,
      diaInteiro: Boolean,
      tipo: { type: String, enum: TIPO_COMPROMISSO, default: 'geral' }
    }, {timestamps: true});
    return schema;
}

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Compromisso';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

module.exports = {
    key,
    schemaFunc,
    depedencies,
    populate
}