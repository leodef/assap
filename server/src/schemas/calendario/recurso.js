const mongoose = require('mongoose');
const TIPO_COMPROMISSO = require('../../../enums/tipo-compromisso');
const Schema = mongoose.Schema;
/**
 * Recurso de compromisso no calendário
 * @typedef {Object} Recurso
 * @property {String} titulo Titulo do recurso do compromisso.
 * @property {String} tipo Tipo do recurso do compromisso.
 */

/**
 * Função para gerar o esquema
 * @returns {Recurso}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      tipo: { type: Schema.Types.ObjectId, ref: 'TipoRecursoCompromisso' },
    }, {timestamps: true});
    return schema;
}

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Recurso';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path:'tipo', ref: 'TipoRecurso'}
];

module.exports = {
    key,
    schemaFunc,
    depedencies,
    populate
}