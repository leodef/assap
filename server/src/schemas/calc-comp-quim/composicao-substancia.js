
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Composição de substancias quimicas.
 * @typedef {Object} CCQComposicaoSubstancia
 * @property {String} titulo - Nome da composição de substância .
 * @property {String} desc - Descrição da composição de substância .
 * @property {Number} valor - Valor financeiro da valor.
 * @property {Array<CCQQuantidadeSubstancia>} substancias - Lista de substância que formam a composição.
 */

/**
 * Função para gerar o esquema
 * @returns {CCQComposicaoSubstancia}
 */
const schemaFunc = (params) => {

    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      desc: String,
      valor: Number,
      substancias: [params.depedencies.CCQQuantidadeSubstancia.schema]
    }, {timestamps: true});
    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'CCQComposicaoSubstancia';

/**
 * Dependencias do modelo
 */
const depedencies = ['CCQQuantidadeSubstancia'];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path: 'substancias.substancia', ref: 'CCQQuantidadeSubstancia'}
];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};