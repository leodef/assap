
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Substancia.
 * @typedef {Object} CCQSubstancia
 * @property {String} titulo Titulo da substancia.
 * @property {String} desc Descrição da substancia.
 * @property {Number} valor Valor financeiro da substancia.
 * @property {Array<CCQQuantidadeElemento>} elementos Quantidade de elementos na composição da substância
 */

/**
 * Função para gerar o esquema
 * @returns {CCQSubstancia}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      desc: String,
      valor: Number,
      elementos: [params.depedencies.CCQQuantidadeElemento.schema]
    }, {timestamps: true});
    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'CCQSubstancia';

/**
 * Dependencias do modelo
 */
const depedencies = ['CCQQuantidadeElemento'];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path: 'elementos.elemento', ref: 'CCQQuantidadeElemento'}
];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};