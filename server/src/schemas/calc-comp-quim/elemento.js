const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Elemento para compor substancias.
 * @typedef {Object} CCQElemento
 * @property {String} titulo Titulo do elemento.
 * @property {String} desc Descrição do elemento.
 */

/**
 * Função para gerar o esquema
 * @returns {CCQElemento}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      desc: String
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'CCQElemento';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];


module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};
