
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Quantidade de uma substância na composição.
 * @typedef {Object} CCQQuantidadeSubstancia
 * @property {CCQSubstancia} substancia Substancia.
 * @property {Number} quantidade Quantidade.
 */

/**
 * Função para gerar o esquema
 * @returns {CCQQuantidadeSubstancia}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      substancia: { type: Schema.Types.ObjectId, ref: 'CCQSubstancia' },
      quantidade: Number
    }, {timestamps: true});
    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'CCQQuantidadeSubstancia';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path: 'substancia', ref: 'CCQSubstancia'}
];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};