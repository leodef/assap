
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Quantidade de um elemento na substância.
 * @typedef {Object} CCQQuantidadeElemento
 * @property {CCQElemento} elemento Elemento.
 * @property {Number} quantidade Quantidade.
 */

/**
 * Função para gerar o esquema
 * @returns {CCQQuantidadeElemento}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      elemento: { type: Schema.Types.ObjectId, ref: 'CCQElemento' },
      quantidade: Number
    }, {timestamps: true});
    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'CCQQuantidadeElemento';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path:'elemento', ref: 'CCQElemento'}
];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};