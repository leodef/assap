const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Produto manuseado pela empresa.
 * @typedef {Object} Produto
 * @property {String} titulo Nome do produto.
 * @property {String} desc Descrição do produto.
 * @property {String} tipo Tipo do produto, para ser utilizado pela empresa.
 * @property {String} marca Empresa responsável pelo produto.
 * @property {Number} densidade Para tirar o volume a partir do peso.
 */

/**
 * Função para gerar o esquema
 * @returns {Produto}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      desc: String,
      tipo: String,
      marca: String,
      densidade: Number
    }, {timestamps: true});
    return schema;
}

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Produto';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

module.exports = {
    key,
    schemaFunc,
    depedencies,
    populate
}