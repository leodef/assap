const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Unidade de medida, ex: kilometros, metros, segundos, etç.
 * @typedef {Object} UnidadeMedida
 * @property {Grandeza} grandeza Grandeza da unidade de medida, Ex: Peso, Tamanho, Velocidade.
 * @property {String} titulo Nome da unidade de medida.
 * @property {String} desc Descrição da unidade de medida.
 * @property {String} valor Valor relativo da unidade de medida ex: 1km = 1000m.
 */

/**
 * Função para gerar o esquema
 * @returns {UnidadeMedida}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      grandeza: { type: Schema.Types.ObjectId, ref: 'Grandeza' },
      titulo: String,
      desc: String,
      valor: Number
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'UnidadeMedida';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [{path:'grandeza', ref: 'Grandeza'}];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
}