
    
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * DadosEmpresa.
 * @typedef {Object} DadosEmpresa
 * @property {Array<Filial>} filiais Filiais da empresa.
 * @property {Array<Parceiro>} parceiros Parceiros da empresa.
 * @property {Array<Veiculo>} veiculos Veiculos da empresa.
 * @property {Array<Produto>} produtos Produtos da empresa.
 * @property {Array<Entrega>} entregas Entregas da empresa.
 */

/**
 * Função para gerar o esquema
 * @returns {DadosEmpresa}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      filiais: [{
        type: Schema.Types.ObjectId, ref: 'Filial',
        required: true,
        default: []
      }],
      parceiros: [{
        type: Schema.Types.ObjectId, ref: 'Parceiro',
        required: true,
        default: []
      }],
      veiculos: [{
        type: Schema.Types.ObjectId, ref: 'Veiculo',
        required: true,
        default: []
      }],
      funcionarios: [{
        type: Schema.Types.ObjectId, ref: 'Funcionario',
        required: true,
        default: []
      }],
      produtos: [{
        type: Schema.Types.ObjectId, ref: 'Produto',
        required: true,
        default: []
      }],
      entregas: [{
        type: Schema.Types.ObjectId, ref: 'Entrega',
        required: true,
        default: []
      }],
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'DadosEmpresa';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path:'filiais', ref: 'Filial'},
  {path:'parceiros', ref: 'Parceiro'},
  {path:'veiculos', ref: 'Veiculo'},
  {path:'carretas', ref: 'Carreta'},
  {path:'funcionarios', ref: 'Funcionario'},
  {path:'produtos', ref: 'Produto'}
];


module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};