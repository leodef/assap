
    
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Empresa.
 * @typedef {Object} Empresa
 * @property {String} nomeFantasia Nome fantasia da empresa.
 * @property {String} razaoSocial Razão social da empresa.
 * @property {String} desc Descrição da empresa.
 * @property {String} identificacao Identificacao juridica, CNPJ.
 * @property {DadosEmpresa} dados Dados da empresa.
 */

/**
 * Função para gerar o esquema
 * @returns {Empresa}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      nomeFantasia: String,
      razaoSocial: String,
      desc: String,
      identificacao: String,
      dados: {
        type: Schema.Types.ObjectId,
        ref: 'DadosEmpresa'
      },
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Empresa';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

const generateDesc = (obj) => {
  if(!obj) {
    return ''
  }
  return `
Empresa:
  Nome Fantasia: ${obj.nomeFantasia}
  Razão Social: ${obj.razaoSocial}
  CNPJ: ${obj.identificacao}
  `
}

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
  config: {
    generateDesc
  }
};