const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Grandeza de uma unidade de medida, ex: peso, volume, distancia, etç.
 * @typedef {Object} Grandeza
 * @property {String} titulo Nome da grandeza.
 * @property {String} desc Descrição da grandeza.
*/

/**
 * Função para gerar o esquema
 * @returns {Grandeza}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      desc: String
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Grandeza';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};