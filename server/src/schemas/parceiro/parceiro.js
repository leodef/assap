const mongoose = require("mongoose");
const TIPO_PESSOA = require("../../enums/tipo-pessoa.enum");
const Schema = mongoose.Schema;
/**
 * Parceiro comercial.
 * @typedef {Object} Parceiro
 * @property {String} titulo Nome do parceiro.
 * @property {String} desc Descrição do parceiro.
 * @property {String} tipoPessoa Tipo de pessoa, Juridica ou Fisica.
 * @property {String} identificacao Identificacao juridica, CPF.
 * @property {Array<Filial>} filiais: Filiais do parceiro
 * @property {Array<ContatoParceiro>} contatos: Contatos disponiveis do parceiro
 */

/**
 * Função para gerar o esquema
 * @returns {Parceiro}
 */
const schemaFunc = (params) => {
  const schema = Schema(
    {
      titulo: String,
      desc: String,
      identificacao: String,
      tipoPessoa: { type: String, enum: TIPO_PESSOA },
      filiais: [{ type: Schema.Types.ObjectId, ref: 'Filial' }],
      contatos: [params.depedencies.ContatoParceiro.schema],
    },
    { timestamps: true }
  );

  return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = "Parceiro";

/**
 * Dependencias do modelo
 */
const depedencies = ["ContatoParceiro"];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  { path: "filiais", ref: "Filial" },
  { path: "contatos.filial", ref: "Filial" },
  { path: "contatos.contato.infos.tipo", ref: "TipoContatoInfo" }
  // { path: "contatos", ref: "ContatoParceiro" },
];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
};
