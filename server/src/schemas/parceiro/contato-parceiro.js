const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Contato de um parceiro
 * @typedef {Object} ContatoParceiro
 * @property {Filial} filial Filial do contato.
 * @property {Contato} contato Contato.
*/

/**
 * Função para gerar o esquema
 * @returns {ContatoParceiro}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      filial: { type: Schema.Types.ObjectId, ref: 'Filial' },
      contato: params.depedencies.Contato.schema
    }, {timestamps: true});
    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'ContatoParceiro';

/**
 * Dependencias do modelo
 */
const depedencies = [
  'Contato'
];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path:'filiais', ref: 'Filial'}
];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};