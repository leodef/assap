const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Filial da empresa
 * @typedef {Object} Filial
 * @property {String} titulo Nome da filial.
 * @property {String} desc Descrição da filial.
 * @property {Localizacao} localizacao Local da filial.
*/

/**
 * Função para gerar o esquema
 * @returns {Filial}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      _temp: { type: String, required: false },
      titulo: String,
      desc: String,
      localizacao: params.depedencies.Localizacao.schema
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Filial';

/**
 * Dependencias do modelo
 */
const depedencies = ['Localizacao'];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};