module.exports =  [
    
    //
    require('./grandeza'),
    require('./parceiro/parceiro'),
    require('./localizacao'),
    require('./limite'),
    require('./filial'),
    require('./funcionario'),
    require('./produto'),
    require('./unidade-medida'),
    require('./parceiro/contato-parceiro'),
    require('./veiculo'),
    
    // auth
    require('./auth/usuario'),
    require('./auth/auth-token'),
    require('./auth/token-usuario'),

    // calc-comp-quim
    require('./calc-comp-quim/elemento'),
    require('./calc-comp-quim/substancia'),
    require('./calc-comp-quim/composicao-substancia'),
    require('./calc-comp-quim/quantidade-elemento'),
    require('./calc-comp-quim/quantidade-substancia'),

    // calendario
    require('./calendario/compromisso'),

    // contato
    require('./contato/tipo-contato-info'),
    require('./contato/contato-info'),
    require('./contato/contato'),

    // empresa
    require('./empresa/dados-empresa'),
    require('./empresa/empresa'),

    // logistica
    require('./logistica/funcionario-entrega'),
    require('./logistica/parceiro-entrega'),
    require('./logistica/transporte'),
    require('./logistica/ponto-rota'),
    require('./logistica/rota'),
    require('./logistica/entrega')

   
];