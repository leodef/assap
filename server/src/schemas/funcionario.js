const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Funcionário.
 * @typedef {Object} Funcionario
 * @property {String} nome Nome do funcionário.
 * @property {String} identificacao CPF do funcionário.
 * @property {Array<String>} posicoes Posições do funcionário na empresa.
 * @property {Array<Filial>} filiais Filiais de atuação do funcionário.
 * @property {Empresa} empresa Empresa do funcionário.
*/

/**
 * Função para gerar o esquema
 * @returns {Funcionario}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      nome: String,
      identificacao: String,
      posicoes: [String],
      filiais: [{ type: Schema.Types.ObjectId, ref: 'Filial' }],
      empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
      criarUsuario: Boolean,
      usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Funcionario';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path:'filiais', ref: 'Filial'},
  {path:'empresa', ref: 'Empresa'},
  {
    path: 'usuario',
    ref: 'Usuario',
    select: {
      '_id': 1,
      'usuario': 1,
      'nome': 1,
      'tipo': 1,
      'status': 1,
      'email': 1
    }
  }
];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};