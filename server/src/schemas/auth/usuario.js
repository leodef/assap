const mongoose = require('mongoose');
const TIPO_USUARIO = require('../../enums/tipo-usuario.enum');
const Schema = mongoose.Schema;
/**
 * Usuario
 * @typedef {Object} Usuario
 * @property {String} usuario Nome para acesso do usuário
 * @property {String} senha Senha para acesso do usuário
 * @property {String} nome Nome para tratamento do usuário
 * @property {String} tipo Tipo de atuação do usuário no sistema
 * @property {String} status Status do usuário no sistema
 */

/**
 * Função para gerar o esquema
 * @returns {Usuario}
 */
const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId,
      usuario: String,
      senha: String,
      nome: String,
      tipo: { type: String, enum: TIPO_USUARIO },
      status: String,
      email: String
    },
    { timestamps: true }
  );
  return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Usuario';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};
