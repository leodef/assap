const mongoose = require('mongoose');
const { TIPO_TOKEN_USUARIO } = require('../../enums/tipo-token-usuario.enum');
const Schema = mongoose.Schema;
/**
 * Token do usuário
 * @typedef {Object} TokenUsuario
 * @property {Date} validade Data de vencimento do token
 * @property {String} tipo Finalidade do token, ex: troca de senha, ativação, etç
 * @property {Usuario} usuario Usuário dono do token
 */

/**
 * Função para gerar o esquema
 * @returns {TokenUsuario}
 */
const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId
      validade: Date,
      tipo: { type: String, enum: TIPO_TOKEN_USUARIO },
      usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    },
    { timestamps: true }
  );

  return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'TokenUsuario';

/**
 * Dependencias do modelo
 */
const depedencies = ['Usuario'];


/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = ['usuario'];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};
