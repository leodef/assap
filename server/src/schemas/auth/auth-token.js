const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Token de autenticação
 * @typedef {Object} AuthToken
 * @property {Date} validade Data de vencimento do token
 * @property {Usuario} usuario Usuário dono do token
 */

/**
 * Função para gerar o esquema
 * @returns {AuthToken}
 */
const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId
      validade: Date,
      usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
    },
    { timestamps: true }
  );

  return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'AuthToken';

/**
 * Dependencias do modelo
 */
const depedencies = ['Usuario'];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path: 'usuario', ref: 'Usuario'}
]

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};
