const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Define a quantidade possivel a ser transportada
 * @typedef {Object} Limite
 * @property {Number} peso Peso.
 * @property {Number} volume Volume.
 * @property {Number} tara Compensação, valo inicial do recepiente.
*/

/**
 * Função para gerar o esquema
 * @returns {Limite}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      peso: Number,
      volume: Number,
      tara: Number
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Limite';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};