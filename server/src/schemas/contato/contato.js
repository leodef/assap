
    
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Informações de contato.
 * @typedef {Object} Contato
 * @property {String} titulo Titulo do contato.
 * @property {String} desc Descrição do contato.
 * @property {Array<ContatoInfo>} infos Informações referente ao contato.
 */

/**
 * Função para gerar o esquema
 * @returns {Contato}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      desc: String,
      infos: [ params.depedencies.ContatoInfo.schema ]
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'Contato';

/**
 * Dependencias do modelo
 */
const depedencies = ['ContatoInfo'];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};