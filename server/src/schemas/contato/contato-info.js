
    
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Informação para contato.
 * @typedef {Object} ContatoInfo
 * @property {String} valor Informação.
 * @property {TipoContatoInfo} tipo Tipo da informação.
 */

/**
 * Função para gerar o esquema
 * @returns {ContatoInfo}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      valor: String,
      tipo: { type: Schema.Types.ObjectId, ref: 'TipoContatoInfo' },
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'ContatoInfo';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [
  {path:'tipo', ref: 'TipoContatoInfo'}
];


module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};