
    
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * Tipo da informação para contato.
 * @typedef {Object} TipoContatoInfo
 * @property {String} titulo Titulo do tipo de informação para contato.
 * @property {String} desc Descrição do tipo de informação para contato.
 */

/**
 * Função para gerar o esquema
 * @returns {TipoContatoInfo}
 */
const schemaFunc = (params) => {
    const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      desc: String,
      mask: String
    }, {timestamps: true});

    return schema;
};

/**
 * Chave para identificação do objeto de gerenciamendo de dados
 */
const key = 'TipoContatoInfo';

/**
 * Dependencias do modelo
 */
const depedencies = [];

/**
 * Campos a serem preenchidos na consulta de dados
 */
const populate = [];


module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};