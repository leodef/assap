const fs = require('fs-extra');
const mongoose = require('mongoose');


const cleanLabel = (result) => {
    result = result.trim()
    if(result.startsWith('-')) {
      result = result.substring(1).trim()
    }
    if(result.endsWith('-')) {
      result = result.substring(0, result.length-1).trim()
    }
    if(result.startsWith('/')) {
      result = result.substring(1).trim()
    }
    if(result.endsWith('/')) {
      result = result.substring(0, result.length-1).trim()
    }
    return result;
  }
  
  const generateId = () => {
    const id = mongoose.Types.ObjectId().toString();
    return id
  }

  const generateTempId = () => {
    return '_'+DTO.generateId()
  }
  
  class DTO {
    _id
    _temp
    __v
    createdAt
    updatedAt
  
    static cleanLabel (result) {
      return cleanLabel(result)
    }
  
    static generateId(){
      return generateId()
    }
  
    static generateTempId(){
      return generateTempId()
    }
  
    static apply (obj, method, params) {
      if (!obj || !obj[method]) {
        return null
      }
      return obj[method].apply(obj, params)
    }
  
    static fromJson (obj, json) {
      return DTO.apply(obj, 'fromJson', [json]) || JSON.parse(json)
    }
  
    static toJson (obj) {
      return DTO.apply(obj, 'toJson') || JSON.stringify(obj)
    }
  
    static fromForm (obj, json) {
      return DTO.apply(obj, 'fromForm', [json]) || JSON.parse(json)
    }
  
    static toForm (obj) {
      return DTO.apply(obj, 'toForm') || JSON.stringify(obj)
    }
  
    static isId(val) {
      return mongoose.Types.ObjectId.isValid(val)
    }
  
    assign (target, source, config) {
      target = target || this 
      // Object.assign(target, source)
      Object.keys(source).forEach(key => (target[key] = source[key]))
      return target
    }
  
    fromJson (json) {
      if (!json) { return this }
      try{
        json = (typeof json === 'string') ? JSON.parse(json) : json
        this.assign(this, json)
      } catch(err) {
        if(DTO.isId(json)) {
          this._id = json
        }
      }
      return this
    }
  
    toJson (value) {
      return JSON.stringify(value || this)
    }
  
    fromForm (form) {
      return this.fromJson(form)
    }
  
    toForm () {
      return this.toJson()
    }
  
    toDTO() {
      return _.clone(this) 
    }
  }
  

module.exports = {
  cleanLabel,
  generateId,
  generateTempId,
  DTO
};

