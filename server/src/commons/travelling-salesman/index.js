const _ = require("lodash");
const haversine = require('haversine')
 /**
 * Funções para encontrar a melhor rota
 * 
 * @name TravelingSalesman
 * @module commons/traveling-salesman
 * @category Commons
 * @subcategory TravelingSalesman
 */
class TravelingSalesman {

  static instance = null;

  /**
   * Retorna uma instancia da classe TravelingSalesman
   * @returns TravelingSalesman
  */
  static getInstance() {
    TravelingSalesman.instance =
        TravelingSalesman.instance ||
        new TravelingSalesman();

      return TravelingSalesman.instance;
  }

  //#region Utils
  /**
   * Pesquisa as rotas possiveis dentro de um ramo de
   *    possibilidades, para o algoritimo naive
   * @param {any} points  d
   * @param {any} distanceMatrix 
   * @param {any} config 
   * @param {any} history
   * @returns {any}
   */
  naiveSearch(points, distanceMatrix, config = {}, history = null) {
    const limit = config ? config.limit : null;
    let paths = null;
    history = history || {
      path: [],
      distancePath: [],
      countPath: [],
      distance: 0,
      count: null,
      points,
      options: points.map((value, index) => Number(index)),
      lastOption: null,
    };
    // history params
    let {
      path,
      distance,
      options,
      lastOption,
      count,
      distancePath,
      countPath
    } = history;
    if (options.length === 0) {
      return {
        path,
        distance,
        count,
        distancePath,
        countPath
      };
    }
    for (const index in options) {
      // point
      const option = options[index];
      const point = points[option];
      //const lastPoint = lastOption === null ? points[lastOption] : null;

      // distance
      const first = lastOption !== null
      const cDistance = first ? distanceMatrix[lastOption][option] : 0;

      //
      const cCount = this.count(point.count, count)
      if (
        cDistance === null ||
        !this.checkPrev(point, path) ||
        !this.checkLimit(cCount, limit) ) {
        continue;
      }
      // cOptions
      let cOptions = options.slice();
      cOptions.splice(index, 1);

      // cPath
      const cPath = this.naiveSearch(points, distanceMatrix, config, {
        // history
        path: [...path, option],
        distancePath: [ ...distancePath, (first ? cDistance : null) ],
        countPath: [ ...countPath, cCount ],
        distance: distance + cDistance,
        count: cCount,
        options: cOptions,
        lastOption: option,
      });

      if(cPath) { 
        paths = ((paths && paths.distance < cPath.distance) ? paths : cPath);
      }
    }
    return paths;
  }

  /**
   * Soma os valores acumulados ao vamor do ponto atual
   * @param {any} value 
   * @param {any} total
   * @returns {any}
   */
  count(value, total) {
    let result = null
    if(!value || !total) {
        result = _.clone(total || value)
        return result
    } else if(typeof total === 'object' && typeof value === 'object') {
      result = {}
      for(const valueKey in value) {
        result[valueKey] =  total[valueKey] + value[valueKey]
      }
    } else {
      result = (value || 0) + (total || 0)
    }
    return result
  }

  /**
   * Verifica se os valores acumulados na rota ultrapassam o limite
   * @param {any} count 
   * @param {any} limit
   * @returns {boolean}
   */
  checkLimit(count, limit) {
    let result = true
    if (
      !count ||
      !limit
    ) {
      result = true
    } else if (
      typeof count === 'object' &&
      typeof limit === 'object'
      ) {
      for (const countKey in count) {
        const countVal = count[countKey] || 0
        const limitVal = limit[countKey] || 0
        if(countVal > limitVal || countVal < 0) {
          result = false
        }
      }
    } else {
      result = (count < limit || count < 0)
    }
    return result;
  }

  /**
   * Verifica se os pontos que devem ser percorridos antes do ponto atual, ja estão na rota
   * @param {any} point 
   * @param {any} path
   * @returns {boolean}
   */
  checkPrev(point, path) {
    if (!point || !point.prev || point.prev.length === 0) {
      return true;
    }
    return this.includes(path, point.prev);
  }

  /**
   * Verifica se uma lista possui um objeto, ou todos os
   *   itens de outra lista
   * @param {any} path 
   * @param {any} obj
   * @returns {boolean}
   */
  includes(path, obj) {
    if (Array.isArray(obj)) {
      return obj.find((val) => path.includes(val)) !== undefined;
    } else {
      return path.includes(obj);
    }
  }

  /**
   * Calcula a distancia entre dois pontos
   * @param {any} a 
   * @param {any} b
   * @returns Number
   */
  getDistance(a, b) {
    const start = {
      latitude: a.x,
      longitude: a.y
    }
    
    const end = {
      latitude: b.x,
      longitude: b.y
    }
    // converte distancia das coodernadas para kms
    const distance =  haversine(start, end, {unit: 'meter'})
    // + 15% de curvas
    return (distance + ( (distance / 100) * 15) )
    /*
      return Math.sqrt(
        Math.pow(Math.abs((a.x || 0) - (b.x || 0)), 2) +
        Math.pow( Math.abs((a.y || 0) - (b.y || 0)), 2)
      );
    */
  }
  //#endregion Utils
  
  //#region Request / Response handle
  /**
   * Gera a matriz contento a distancia entre os pontos
   * @param {any} points 
   * @returns Array<Array<Number>>
   */
  getDistanceMatrix(points) {
    const distanceMatrix = [];
    for (let pointIndex in points) {
      pointIndex = Number(pointIndex);
      const point = points[pointIndex] || [];
      const prev = point.prev || [];
      for (let comparePointIndex in points) {
        comparePointIndex = Number(comparePointIndex);
        const point = points[pointIndex];
        const comparePoint = points[comparePointIndex];
        distanceMatrix[pointIndex] = distanceMatrix[pointIndex] || [];
        distanceMatrix[pointIndex][comparePointIndex] =
          pointIndex !== comparePointIndex && !prev.includes(comparePointIndex)
            ? Number(this.getDistance(point, comparePoint))
            : null;
      }
    }
    return distanceMatrix;
  }

  /**
   *  Subistiui index pelos velor correspondente ao index
   * @param {any} params Resultado do algoritimo
   * @returns {any}
   */
  fill(result) {
    let { path, points, distancePath } = result;
    path = (path || []).map( (pointIndex, index) => ({
      ...points[pointIndex],
      pos: (index + 1),
      distance: distancePath[index]
    }))
    return {
      ...result,
      path
    }
  }
  //#endregion Request / Response handle

  //#region Interface
  /**
   *  Executa o algoritimo Naive para procura da melhor rota
   * @param {any} points 
   * @param {any} pDistanceMatrix 
   * @param {any} config
   * @returns {any}
   */
  naive(points, pDistanceMatrix, config = {}) {
    const distanceMatrix = _.cloneDeep(
      pDistanceMatrix || this.getDistanceMatrix(points)
    );
    return this.fill({
      ...this.naiveSearch(points, distanceMatrix, config),
      points,
      config,
      algorithm: 'naive'
    });
  }

  /**
   *  Executa o algoritimo Nearest Neighbor para procura da melhor rota
   * @param {any} points
   * @param {any} pDistanceMatrix
   * @param {any} config
   * @returns {any}
   */
  nearestNeighbor(points, pDistanceMatrix, config = {}) {
    const limit = config ? config.limit : null;
    const distanceMatrix = _.cloneDeep(
      pDistanceMatrix || this.getDistanceMatrix(points)
    );
    const length = points.length;
    let distance = 0;
    // encontra o primeiro ponto sem nenhum obrigatoriamente anterior para iniciar a rota
    let first = points
      .map((point, index) => ({ point, index }))
      .find((value) => value.point && (!value.point.prev || value.point.prev.length === 0));
    if(!first || !first.point) {
      throw new Error('no-first-point-found');
    }
    let count = first.point.count || 0;
    let rowIndex = first.index;
    let path = [ first.index ];
    let distancePath = [ null ];
    let countPath = [ first.point.count ];
    while (path.length < length) {
      const row = distanceMatrix[rowIndex];
      let minCol = { index: null, value: null };
      for (let colIndex in row) {
        colIndex = Number(colIndex);
        let col = row[colIndex];
        if(col === null || colIndex === rowIndex) {
          continue;
        }
        col = Number(col)
        const point = points[colIndex];
        const cCount = this.count(point.count, count);
        if (
          this.checkPrev(point, path) &&
          this.checkLimit(cCount, limit) &&
          (minCol.index === null || col < minCol.value) &&
          !path.includes(colIndex)
        ) {
          minCol = {
            index: colIndex,
            value: col,
            count: cCount
          };
        }
      }
      count = minCol.count; 
      distance += minCol.value;
      rowIndex = minCol.index;
      distancePath.push(minCol.value);
      countPath.push(minCol.count);
      path.push(rowIndex);
    }
    return this.fill({
      distance,
      distancePath,
      countPath,
      path,
      count,
      points,
      config,
      algorithm: 'nearestNeighbor'
    });
  }

  /**
   *  Encontra o algoritimo para resolver uma requisição
   * @param {any} params Requisição recebida
   * @returns {any}
   */
  resolve(params) {
    if(!params) {
      throw new Error('no-params-for-resolve')

    }
    let {
      points,
      distanceMatrix,
      config
    } = params;
    points = this.loadPoints(points)

    if(!config) {
      throw new Error('no-config-for-resolve')

    }
    let { algorithm } = config
    let result = {};
    switch (algorithm) {
      case 'nearestNeighbor':
        result = this.nearestNeighbor(points, distanceMatrix, config);
        break;
      case 'naive':
        result = this.naive(points, distanceMatrix, config);
        break;
      default:
        result = this.nearestNeighbor(points, distanceMatrix, config);
        algorithm = 'nearestNeighbor'
        break;
    }
    return result
  }
  //#endregion Interface

  loadPoints(points) {
    return points.map( (point, pointIndex) => {
      const prev = (point.prev || [])
      for (let prevIndex in prev) {
        prevIndex = Number(prevIndex);
        let prevVal = prev[prevIndex];
        const prevPointIndex = points.findIndex( (val) => 
          (val && val.id && val.id === prevVal)
        )
        if(prevPointIndex >= 0) {
          prev[prevIndex] = prevPointIndex
        }
      }
      return ({
        ...point,
        prev
      })
    })
    
  }
}

const instance = TravelingSalesman.getInstance();

module.exports = {
    TravelingSalesman,
    instance
}