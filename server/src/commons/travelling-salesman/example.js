const {
  TravelingSalesman
} = require('../travelling-salesman')
const tsp = new TravelingSalesman();
const points = [
  {
    // A
    x: 1,
    y: 1,
    count: 5,
    value: "A"
  },
  {
    // B
    x: 2,
    y: 1,
    count: -3 ,
    value: "a",
    prev: [0],
  },
  {
    // C
    x: 1,
    y: 2,
    count: 10 ,
    value: "b",
  },
  {
    // D
    x: 2,
    y: 2,
    count:  -8,
    value: "B",
    prev: [2],
  },
];

// nearestNeighbor
console.info('\n --- nearestNeighbor --- \n')
const nearestNeighborResult =tsp.nearestNeighbor(points);
JSON.stringify(nearestNeighborResult);

// naive
console.info('\n --- naive --- \n')
const naiveResult = tsp.naive(points);
JSON.stringify(naiveResult);

/*
    Exemplo manual da pesquisa de rotas

    const points = [
        { x: 1, y: 2, value: 0, prev: [ 0,1,2 ] }
    ]
        ---------------------
        | x | a | b | c | d |
        |---|---|---|---|---|
        | a | x | 0 | 0 | 0 |
        |---|---|---|---|---|
        | b | 0 | x | 0 | 0 |
        |---|---|---|---|---|
        | c | 0 | 0 | x | 0 |
        |---|---|---|---|---|
        | d | 0 | 0 | 0 | x |
        ---------------------

    Paths
        abcd
        abdc
        acbd
        acdb
        adbc
        adcb
*/