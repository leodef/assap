const fs = require('fs-extra');
const mongoose = require('mongoose');

 /**
 * Gerencia objetos para persistencia na base de dados
 * 
 * @name ModelFactory
 * @module commons/model-factory
 * @category Commons
 * @subcategory ModelFactory
 */
class ModelFactory {

  /**
   * Retorna uma instancia da classe ModelFactory
   *
   * @param {any} config Configuração da instancia
   * 
   * @returns {ModelFactory}
   * 
   */
  static getInstance(config) {
    return (ModelFactory.instance =
      !!ModelFactory.instance && !config
        ? ModelFactory.instance
        : new ModelFactory(config));
  }

   /**
   * Retorna o objeto solicitado caso caso encontrado
   * 
   * @param {any} model Objeto solicitado
   * @param {boolean} throwError Indica se deve lançar um erro
   *                    caso o objeto não seja encontrado
   *
   * @returns {any}
   */
  static getModel(model, throwError = true) {
    const modelWrapper = !!ModelFactory.instance
      ? ModelFactory.instance.getModel(model)
      : null;
    if (!modelWrapper && throwError) {
      throw new Error(`Não foi possivel carregar a persistencia do modelo ${model}.`);
    }
    return modelWrapper;
  }

  /**
   * @constructor
   *
   * @param {any} args  Objeto de configuração da instancia
   * 
   * @returns ModelFactory
   */
  constructor(config) {
    this._models = {};
    this._waiting = {};
    this._depedencies = {};
    if (config.list) {
      this.loadList(config.list);
    }
    if (config.folder) {
      //this.loadFolder(config.folder);
    }
  }

  /**
   * Objetos carregados
   */
  get models() {
    return this.cleanObj(this._models);
  }

  /**
   * Objetos na fila de espera
   */
  get waiting() {
    return this.cleanObj(this._waiting);
  }

  /**
   * Dependencias
   */
  get depedencies() {
    return this.cleanObj(this._depedencies);
  }

  /**
   * Retira valores indesdejados de um objeto
   * 
   * @param {any} obj Objeto a ser limpado
   *
   * @returns {any}
   * @todo mover para uma classe mais genérica
   */
  cleanObj(obj) {
    const result = {};
    for (const index in obj) {
      let val = obj[index];
      if (!!val) {
        val = typeof val === 'object' ? this.cleanObj(val) : val;
        result[index] = val;
      }
    }
    return result;
  }

  /**
   * Retorna o objeto solicitado caso caso encontrado
   * 
   * @param {any} model Objeto solicitado
   *
   * @returns {any}
   */
  getModel(model) {
    return this._models[model.key || model];
  }

  /**
   * Carrega uma lista de esquemas no objeto de gerenciamento
   *    para depois ser carregado como objeto de persistencia
   * 
   * @param {any} param Esquemas a serem carregados
   *
   * @returns {any}
   */
  loadList(param) {
    for (let key in param) {
      const schema = param[key];
      key = Number.isNaN(key) ? key : schema.key;
      this.loadSchema({ ...schema, key });
    }
  }
  /**
   * Carrega um esquema no objeto de gerenciamento
   *    para depois ser carregado como objeto de persistencia
   * 
   * @param {any} schemaWrapper Esquema a ser carregado
   *
   * @returns {any}
   */
  loadSchema(schemaWrapper) {
    const {
      key,
      schemaFunc,
      depedencies,
      depedencieKeys,
      collectionName,
      populate,
      config
    } = schemaWrapper;
    const model = new ModelWrapper(
      key,
      schemaFunc,
      depedencies || depedencieKeys,
      collectionName,
      this,
      populate,
      config
    );
    this.loadModel(model);
  }

  /**
   * Se possivel converte o esquema em um objeto de persistencia,
   *   senão, coloca na fila de espera
   * 
   * @param {any} schemaWrapper Esquema a ser carregado
   *
   * @returns {any}
   */
  loadModel(model) {
    if (!this.checkAndLoadDepedencies(model)) {
      return;
    }
    return this.setModel(model);
  }

  /**
   * Converte o esquema em um objeto de persistencia
   * 
   * @param {any} model Esquema a ser carregado
   *
   * @returns {any}
   */
  setModel(model) {
    if (this._models[model.key]) {
      return;
    }
    model.init(this);
    this._waiting[model.key] = null;
    this._models[model.key] = model;
    const depedencies = this._depedencies[model.key];
    if (depedencies) {
      for (const key in depedencies) {
        const obj = depedencies[key];
        if (obj) {
          this.loadModel(obj);
        }
      }
    }
    return model;
  }

  /**
   * Carrega dependencias do esquema, e verifica se depois disso
   *   existe alguma
   * 
   * @param {any} model Esquema a ser carregado
   *
   *  @returns {boolean}
   */
  checkAndLoadDepedencies(model) {
    let result = true;
    for (const key in model.depedencies) {
      const obj = model.depedencies[key];
      if (!obj) {
        this._depedencies[key] = this._depedencies[key] || {};
        this._depedencies[key][model.key] = model;
        this._waiting[model.key] = model;
        result = false;
      }
    }
    return result;
  }
}

 /**
 * Encapsulamento de valores relacionados ao esquema
 * 
 * @name ModelWrapper
 * @module commons/model-factory
 * @category Commons
 * @subcategory ModelFactory
 */
class ModelWrapper {
  /**
   * @constructor
   *
   * @param {any} key Chave de indendificação do esquema
   * @param {any} schemaFunc Função que cria o esquema
   * @param {any} depedencieKeys Chave dos esquemas necessários para
   *   o carregamento deste
   * @param {any} collectionName Nome da coleção manipulada
   *  na base de dados
   * @param {ModelFactory} factory Objeto de gerenciamento que instanciou a classe 
   * @param {any} populate Dados para o preenchimento automático de 
   *   documentos embutidos
   * 
   * @returns ModelFactory
   */
  constructor(
    key,
    schemaFunc,
    depedencieKeys,
    collectionName,
    factory,
    populate,
    config
  ) {
    this.key = key;
    this.schemaFunc = schemaFunc;
    this.depedencieKeys = depedencieKeys;
    this.collectionName = collectionName;
    this.factory = factory;
    this.model = null;
    this.schema = null;
    this.populate = populate;
    this.config = config;
  }

  /**
   * CDependencias para a conversão do esquema
   * 
   */
  get depedencies() {
    const obj = {};
    const factory = this.factory;
    (this.depedencieKeys || []).forEach((key) => {
      obj[key] = factory._models[key];
    });
    return obj;
  }

  /**
   * Carrega e configura os objetos relacionados a base de dados
   *   de acordo com os dados da instancia
   *
   * @returns {any}
   */
  init() {
    const depedencies = this.depedencies;
    const key = this.key;
    const schema = this.schemaFunc({ ...this, depedencies });
    const populate = (this.populate = this.loadPopulate(this.populate, schema));
    const autoPopulate = this.getAutoPopulate(populate);
    if (autoPopulate) {
      schema.pre('findOne', autoPopulate).pre('find', autoPopulate);
    }
    this.schema = schema;
    const collectionName = this.collectionName;
    try {
      if (!!collectionName) {
        this.model = mongoose.model(key, schema, collectionName);
      } else {
        this.model = mongoose.model(key, schema);
      }
    } catch (err) {
      this.model = mongoose.model(key);
    } finally {
      const model = this.model
      this.collectionName =
        this.collectionName ||
        (model && model.collection) ?
          model.collection.collectionName :
          null
    }
    return this;
  }

  /**
   * Carrega o prenchimento automatico de documentos embutidos
   *   de acordo com os dados da instancia
   * 
   * @param {any} model Esquema a ser carregado
   *
   * @returns {any}
   */
  loadPopulate(populate, schema) {
    if (!populate) {
      return null;
    }
    const result = populate.map((obj) => {
      // se o objeto for um texto, esse texto refere ao caminho a ser populado
      if (typeof obj === 'string') {
        obj = { path: obj };
      }
      // se o objeto tiver o nome da collection, nao precisa ser alterado, pois ja tem o nome da coleção
      if (obj.collectionName) {
        return obj;
      }
      // se não for definido a referencia ele pega do schema
      const ref = obj.ref ||
        (schema.path(obj.path) ? schema.path(obj.path).options.ref : null);
      // Procura o wrapper que está sendo referenciado
      const wrapper = this.depedencies[ref];
      // adiciona o nome da coleção a dependencia 
      const collectionName = wrapper
        ? wrapper.model.collection.collectionName
        : null;
      return {
        ...obj,
        collectionName,
      };
    });

    return result;
  }

  /**
   * Gera os dados de prenchimento automatico de documentos embutidos
   *   pela chave 
   * 
   * @param {any} populate Dados do prrenchimento automatico
   *
   * @returns {any}
   */
  getAutoPopulate(populate) {
    if (!populate) {
      return null;
    }
    return function (next) {
      if (populate) {
        populate.forEach((obj) => {
          const  {
            ref,
            collectionName,
            ...params
          } = obj
          try {
            this.populate(params); // obj.path
          } catch (err) {}
        });
      }
      next();
    };
  }
}

module.exports = {
  ModelFactory,
  ModelWrapper,
};

/*
  Model.find(query, fields, options, callback)// fields and options can be omitted
  Model.find({}, 'first last', function (err, docs)
  Model.count(conditions, callback);
  Model.remove(conditions, callback);
  Model.distinct(field, conditions, callback);
  Model.update(conditions, update, options, callback);

  Model
  .where('age').gte(25)
  .where('tags').in(['movie', 'music', 'art'])
  .select('name', 'age', 'tags')
  .skip(20)
  .limit(10)
  .asc('age')
  .slaveOk()
  .hint({ age: 1, name: 1 })
  .exec(callback);

  Model.$where('this.firstname === this.lastname').exec(callback)

  var query = Model.find({});
  query.where('field', 5);query.limit(5);query.skip(100);
  query.exec(function (err, docs))
*/

/*
  async loadFolder(path){
    const resolvedPath = require.resolve(path);
    const files = await fs.readdir(resolvedPath);
    for(const fileName of files){
      const file = `${path}/${fileName}`;
      const resolvedFile = require.resolve(file);
      const stat = await fs.stat(resolvedFile);
      if(stat.isDirectory()){
        await this.loadFolder(file);
      } else {
        await this.loadSchema(require(file));
      }
    }
    return true;
  }
*/
