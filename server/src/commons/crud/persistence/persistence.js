const mongoose = require('mongoose');
 /**
 * Classe para operações genéricas de CRUD
 * 
 * @name Persistence
 * @module commons/crud/persistence
 * @category Commons
 * @subcategory Crud
 */
class Persistence {

  /**
   * @constructor
   *
   * @param {any} persistence  Objeto responsável por acionar a
   *                             interação com a base de dados
   * @param {any} queries  Scripts adicionais a serem executados
   *                             na base de dados
   * 
   * @returns Persistence
   */
  constructor(persistence, queries) {
    this.persistence = persistence;
    const {
      prevQuery,
      nextQuery,
      filterQuery,
      resolveItem
    } = queries || {};
    this.prevQuery = prevQuery;
    this.nextQuery = nextQuery;
    this.filterQuery = filterQuery;
    this.resolveItem = resolveItem;
  }

  /**
   * Converte o objeto para id caso necessário
   *
   * @param {any} val Objeto a ser convertido para id
   * @param {any} isId Indica se o objeto deve ser um id
   *
   * @returns {any}
   */
  id(val, isId = true) {
    return typeof val === 'string' && isId ? mongoose.Types.ObjectId(val) : val;
  }

  /**
   * Executa a agregação de scripts na base de dados
   *
   * @param {any} query Query a ser executada na base de dados
   *
   * @returns {any}
   */
  aggregate(query) {
    const persistence = this.persistence;
    const { model, populate } = persistence;
    return model.aggregate([query]);
  }

  /**
   * Converte um item antes de ser inserido
   * em tela caso seja necessário
   *
   * @param {any} item Item a ser convertido
   *
   * @returns {any}
   */
  loadItem(item) {
    const resolveItem = this.resolveItem;
    // const resolveListItem = this.resolveListItem;
    return Promise.resolve(
      resolveItem
        ? (
            Array.isArray(item)
              ? item.map(
                  (cItem, index) => resolveItem(cItem, this, index)
                )
              : resolveItem(item, this)

          )
        : item
      );
  }

  /**
   * Carrega uma query antes de ser executada na base de dados
   * caso seja nescessário
   *
   * @param {any} query Item a ser convertido
   * @param {any} params Parametros para a conversão da query
   * @param {any} prev Query a ser concatenada previamente
   *
   * @returns {any}
   */
  loadQuery(query, params, prev = []) {
    const prevQuery = this.getPrevQuery(params);
    const nextQuery = this.getNextQuery(params);
    const populateQuery = this.getPopulateQuery(params);
    return [
      ...prevQuery,
      ...prev,
      ...query,
      ...populateQuery,
      ...nextQuery];
  }

  /**
   * Gera a query para ser executada previamente a
   * execução de qualquer query na base de dados
   *
   * @param {any} params Parametros para a formação da query
   *
   * @returns {any}
   */
  getPrevQuery(params) {
    const prevQuery = this.prevQuery;
    return prevQuery ? prevQuery(params, this) || [] : [];
  }

  /**
   * Gera a query para ser executada posterior a
   * execução de qualquer query na base de dados
   *
   * @param {any} params Parametros para a formação da query
   *
   * @returns {any}
   */
  getNextQuery(params) {
    const nextQuery = this.nextQuery;
    return nextQuery ? nextQuery(params, this) || [] : [];
  }

  /**
   * Gera a query de filtro para a consulta de dados
   *
   * @param {any} filter Filtro em forma de objeto
   * @param {any} params Parametros para a formação da query
   * @param {Array<any>} params Query para ser concatenada previamente
   *
   * @returns {any}
   */
  getFilterQuery(filter, params, prev = []) {
    const filterQuery = this.filterQuery;
    return (
      (filterQuery
        ? filterQuery(filter, params, prev, this)
        : this.getDefaultFilterQuery(filter, params, prev)) || []
    );
  }

  /**
   * Gera a query de filtro padrão caso nao tenha alguma definida
   *
   * @param {any} filter Filtro em forma de objeto
   * @param {any} params Parametros para a formação da query
   * @param {Array<any>} params Query para ser concatenada previamente
   *
   * @returns {any}
   */
  getDefaultFilterQuery(filter, params, prev = []) {
    /*{ 
      fieldName: urlParamName,
      fieldName2: { _id: urlParamName }
    }*/
    if (!filter) {
      return [...(prev || [])];
    } else {
      const prevQuery = this.getPrevQuery(params);
      const nextQuery = this.getNextQuery(params);

      const filterQuery = Object.keys(filter).reduce((result, fieldName) => {
        const filterValue = filter[fieldName];
        if (!filterValue) {
          return null;
        }
        const isIdParam = filterValue._id;
        const urlParamName = isIdParam ? filterValue._id : filterValue;
        let urlParamValue = this.id(params[urlParamName], isIdParam);
        result[fieldName] = urlParamValue;
        return result;
      }, {});
      return [...(prev || []), { $match: filterQuery }];
    }
  }

  /**
   * Gera a query para popular dados de retorno
   *
   * @param {Array<any>} prevQuery Query para ser concatenada previamente
   *
   * @returns {any}
   */
  getPopulateQuery(prevQuery = []) {
    return [];
    /*
    const { schema, populate } = this.persistence;
    const query = populate ? populate
      .reduce( (prev, curr) => {
        let { path, collectionName } = curr;
 
        return [
          ...prev,
          {
            $lookup: {
                from: collectionName,
                localField: path,
                foreignField: '_id',
                as: path
              }
            },
            {
              $project: {
                [path]: {
                  $arrayElemAt: [ `$${path}`, 0 ]
                }
              }
            }]
      }, []) : []
    return [
      ...prevQuery,
      ...query
    ]
    */
  }

  /*
  getPopulateQueryFor(params) {
     if(Array.isArray()) {
        return params.map( (param) => this.getPopulateQueryFor(param));
     }
    const {collectionName, field} = params;
    return {
        $lookup: {
          from: model.collection.name, // root.key, //modelRoot.collectionName,
          localField: `${parentField}`,
          foreignField: '_id',
          as: `${parentField}`
        }
    }
  }
  */
}

module.exports = Persistence;
