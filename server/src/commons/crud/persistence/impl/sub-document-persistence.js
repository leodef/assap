const Persistence = require('../persistence');
const _ = require('lodash');

 /**
 * Classe para operações padrões de CRUD de um documento
 * embutido dentro de outro
 * 
 * @name SubDocumentPersistence
 * @module commons/crud/persistence/impl
 * @category Commons
 * @subcategory Crud
 */
class SubDocumentPersistence extends Persistence {
  static DOCTYPE_ID = 'id'
  static DOCTYPE_OBJECT = 'object'
  /**
   * @constructor
   *
   * @param {any} persistence  Objeto responsável por acionar a
   *                  interação com a base de dados do documento pai
   * @param {any} parentField  Campo do documento pai que contém a 
   *                  referencia para o documento
   * @param {any} childPersistence  Objeto responsável por acionar a
   *                             interação com a base de dados
   * @param {any} queries  Scripts adicionais a serem executados
   *                             na base de dados
   * 
   * @returns SubDocumentPersistence
   */
  constructor(
    persistence,
    parentField,
    childPersistence,
    queries,
    doctype = SubDocumentPersistence.DOCTYPE_OBJECT,// 'id' 'object'
    ) {
    super(
      persistence,
      queries);
    this.parentField = parentField;
    this.childPersistence = childPersistence;
    this.doctype = doctype || SubDocumentPersistence.DOCTYPE_OBJECT
  }

  /**
   * Insere um documento no campo filho
   * de um registro na base de dados
   *
   * @param {any} parentId Id para a consulta do documento pai
   * @param {any} queryId Documento a ser inserido no campo
   *
   * @returns {any}
   */
  create(parentId, queryId) {
    const { model, collectionName } = this.persistence;
    const parentField = this.parentField;
    const _parentId = this.id(parentId);
    queryId = this.id(queryId);
    // schemas
    const filterQuery = { _id: _parentId };
    return this.loadItem(queryId).then((loadItem) => {
      queryId = loadItem
      const updateQuery = { $push: {
        [`${parentField}`]: queryId
      } };
      return model.updateOne(
          // findOneAndUpdate
          filterQuery,
          updateQuery,
          { new: true }
        )
      })
      .then((result) => {
        // Retorna o objeto criado
        const lastItemQuery = this.getLastQuery(
          this.getFindQuery(
            parentId,
            {_id: queryId}),
          parentField
        );
        return model
          .aggregate(lastItemQuery)
          .then((resp) => (Array.isArray(resp) ? resp[0] : resp));
      });
  }

  /**
   * Atualiza um documento no campo filho
   * de um registro na base de dados
   *
   * @param {any} parentId Id para a consulta do documento pai
   * @param {any} query Script para a consulta do registro para edição
   * @param {any} body Dados para atualização do registro
   *
   * @returns {any}
   */
  update(parentId, query, child) {
    const { model } = this.persistence;
    const parentField = this.parentField;
    const _parentId = this.id(parentId);
    // schemas
    query = this.id(query);
    if (!!query && !!query._id) {
      query._id = this.id(query._id);
    }
    const filterQuery = { 
      _id: _parentId,
      [`${parentField}`]: { $elemMatch: query }
    }; // { _id,: _id, `${field}._id`: id},
    filterQuery;

    const updateSetQuery = {}; // { '$set': { `${field}.$`: body } }
    
    return this.loadItem(query).then((loadItem) => {
      query = loadItem
      for (const childKey in query) {
        updateSetQuery[`${parentField}.$.${childKey}`] = query[childKey];
      }
      const updateQuery = { $set: updateSetQuery };
      return model.updateOne(
          // findOneAndUpdate
          filterQuery,
          updateQuery,
          { new: true }
        )
    }).then((val) =>
      // Retorna o objeto alterado
      this.find(parentId, query)
    );
  }

  /**
   * Remove um documento no campo filho
   * de um registro na base de dados
   *
   * @param {any} parentId Id para a consulta do documento pai
   * @param {any} query Script para a consulta do registro para remoção
   *
   * @returns {any}
   */
  remove(parentId, query) {
    const { model } = this.persistence;
    const parentField = this.parentField;
    const _parentId = this.id(parentId);
    query = this.id(query);

    // schemas
    const filterQuery = { _id: _parentId }; // {_id,: _id, `${field}._id`: id},

    const updateQuery = {
      $pull: {
        [`${parentField}`]: query
      }
    };
    const findObj = this.find(_parentId, query);
    return model
      .updateOne(
        // findOneAndUpdate
        filterQuery,
        updateQuery,
        { new: true }
      )
      .then((resp) => findObj);
  }

  /**
   * Encontra um documento no campo filho
   * de um registro na base de dados
   *
   * @param {any} parentId Id para a consulta do documento pai
   * @param {any} query Script para a consulta do registro
   * @param {boolean} unique Indica se o resultado é um registro unico
   *
   * @returns {any}
   */
  find(parentId, query, unique = true) {
    const { model, collectionName } = this.persistence;
    const findQuery = this.getFindQuery(
      parentId,
      {_id: query});
    return model.aggregate(findQuery).then((result) => {
      return unique && Array.isArray(result) ? result[0] : result;
    });
  }

  /**
   * Realiza uma consulta no campo filho
   * de um registro na base de dados
   *
   * @param {any} parentId Id para a consulta do documento pai
   * @param {any} query Script para a consulta de dados
   *
   * @returns {any}
   */
  fetch(parentId, query) {
    return find(parentId, query, false);
  }

  /**
   * Substitui a referencia ao documentos pelo próprio documento
   *
   * @param {any} parentId Id para a consulta do documento pai
   * @param {any} child Id para consulta do documento filho
   *
   * @returns {any}
   */
  fill(parentId, child = null) {
    const childById = (!!child && !!child._id);
    // aggregate - retorna o resultado da consulta, se o resultado for unico, ou seja, pelo id, e retornado apenas o primeiro resultado
    const fillQuery = this.getFillQuery(parentId, child);
    return this.persistence.model
      .aggregate(fillQuery)
      .then((result) => (childById ? result[0] : result));
  }

  /**
   * Gera a query para consulta do ultimo registro criado
   *
   * @param {any} query Query para ser concatenada previamente
   * @param {any} parentField Campo do documento pai a ser referenciado
   *
   * @returns Array<any>
   */
  getLastQuery(query, parentField) {
      return [
        ...query,
        { $sort: { 'createdAt': 1 } },
        { $limit: 1 }
      ];
  }


  /**
   * Gera a query para pesquisa dentro de um campo
   * do documento pai
   *
   * @param {any} parentId Campo do documento pai a ser referenciado
   * @param {any} query Query para consulta do documento
   *
   * @returns Array<any>
   */
  getFindQuery(parentId, query = null) {
    const parentField = this.parentField;
    const _parentId = this.id(parentId);
    const doctype = this.doctype
    const { collectionName } = this.persistence;

    // schemas
    // 1 - matchIdSchema - Encontra o documento pai
    const matchParentIdSchema = ([{ $match: { _id: _parentId } }]);

    // 2 lookup - Encontra o item na coleção caso seja uma coleção de id
    const lookup = (
      (
        doctype === SubDocumentPersistence.DOCTYPE_ID
      ) ? [{
      $lookup: {
        from: collectionName, 
        localField: `${parentField}`, 
        foreignField: '_id', 
        as: `${parentField}`
      }
    }] : [])

    // 2 - unwind - Divide os documentos princiais, separando um para cada subdocumento dentro da array
    const unwind = ([{ $unwind: { path: `$${parentField}` } }]);

    // 3 - replaceRoot - Joga o documento do campo filho como documento principal
    const replaceRoot = ([{ $replaceRoot: { newRoot: `$${parentField}` } }]);

    // 4 - matchField - Caso seja passado um filtro para o filho
    let matchChild = ((!!query && !_.isEmpty(query)) ? [{ $match: query }] : []);

    // 5 - populateQuery - Popula campos filhos
    let populateQuery = this.getPopulateQuery();

    // aggregate input - Junta os campos para serem executados em sequencia, retirando os nulos
    return [
      ...matchParentIdSchema,
      ...lookup,
      ...unwind,
      ...replaceRoot,
      ...matchChild,
      ...populateQuery
    ];
  }

  /**
   * Gera a query para consulta dentro de um campo
   * do documento pai
   *
   * @param {any} parentId Campo do documento pai a ser referenciado
   * @param {any} query Parametros para a consulta de dados
   *
   * @returns {any}
   */
  getFetchQuery(parentId, query = null) {
    return this.getFindQuery(parentId, query);
  }

  /**
   * Gera a query para substitui a referencia ao
   * documentos pelo próprio documento
   *
   * @param {any} query Script para a consulta de dados
   *
   * @returns {any}
   */
  getFillQuery(parentId, query = null) {
    const { model } = this.childPersistence;
    const parentField = this.parentField;
    const _parentId = this.id(parentId);

    // schemas
    // 1 - matchIdSchema - Encontra o documento pai
    const matchParentIdSchema = ([{ $match:  { _id: _parentId } }]);

    // 2 - lookupSchema - Encontra os documentos filhos para os ids na array
    const lookupFiledArrayIdsSchema = ([
      {
          $lookup: {
            from: model.collection.name, // root.key, //modelRoot.collectionName,
            localField: `${parentField}`,
            foreignField: '_id',
            as: `${parentField}`
          }
      }]);

    // 3 - unwind - Divide os documentos princiais, separando um para cada subdocumento dentro da array
    const unwind = ([{ $unwind: { path: `$${parentField}` } }]);

    // 4 - replaceRoot - Joga o documento do campo filho como documento principal
    const replaceRoot = ([{ $replaceRoot: { newRoot: `$${parentField}` } }]);

    // 5 - matchField - Caso seja passado um filtro para o filho
    let matchChild = ((!!query && !_.isEmpty(query)) ? [{ $match: query }] : []);

    // 6 - populateQuery - Popula campos filhos
    let populateQuery = this.getPopulateQuery();

    // aggregate input - Junta os campos para serem executados em sequencia, retirando os nulos
    return [
      ...matchParentIdSchema,
      ...lookupFiledArrayIdsSchema,
      ...unwind,
      ...replaceRoot,
      ...matchChild,
      ...populateQuery
    ];
  }
}

module.exports = SubDocumentPersistence;
