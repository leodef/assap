const Persistence = require('../persistence');

 /**
 * Classe para operações padrões de CRUD de um documento
 * 
 * @name DocumentPersistence
 * @module commons/crud/persistence/impl
 * @category Commons
 * @subcategory Crud
 */
class DocumentPersistence extends Persistence {

  /**
   * @constructor
   *
   * @param {any} persistence  Objeto responsável por acionar a
   *                             interação com a base de dados
   * @param {any} queries  Scripts adicionais a serem executados
   *                             na base de dados
   * 
   * @returns DocumentPersistence
   */
  constructor(persistence, queries) {
    super(persistence, queries);
  }

  /**
   * Insere um registro na base de dados
   *
   * @param {any} body Item a ser inserido na base de dados
   *
   * @returns {any}
   */
  create(body) {
    const persistence = this.persistence;
    const { model } = persistence;
    return this.loadItem(body).then((loadItem) =>
      model.create(loadItem)
      )
  }

  /**
   * Atualiza um registro na base de dados
   *
   * @param {any} query Script para a consulta do registro para edição
   * @param {any} body Dados para atualização do registro
   *
   * @returns {any}
   */
  update(query, body) {
    const persistence = this.persistence;
    const { model } = persistence;
    
    return this.loadItem(body).then((loadItem) => {
      if(Array.isArray(loadItem) && Array.isArray(query)) {
        return Promise.all( query.map( (cQuery, index) => (
          model.findOneAndUpdate(cQuery, loadItem[index], { new: true })
        )))
      }
      return model.findOneAndUpdate(query, loadItem, { new: true })
    })
  }

  /**
   * Remove um registro na base de dados
   *
   * @param {any} query Script para a consulta do registro para remoção
   *
   * @returns {any}
   */
  remove(query) {
    const persistence = this.persistence;
    const { model } = persistence;
    return model.findOneAndDelete(query);
  }

  /**
   * Encontra um registro na base de dados
   *
   * @param {any} query Script para a consulta de dados
   * @param {boolean} unique Indica se o resultado é um registro unico
   *
   * @returns {any}
   */
  find(query, unique = true) {
    const persistence = this.persistence;
    const { model } = persistence;
    return (unique ? model.findOne(query) : model.find(query));
  }

  /**
   * Realiza uma consulta na base de dados
   *
   * @param {any} query Script para a consulta de dados
   *
   * @returns {any}
   */
  fetch(query) {
    return this.find(query, false);
  }

}
module.exports = DocumentPersistence;
