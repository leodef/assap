const express = require('express');
 /**
 * Classe para roteamento de serviços padrões para CRUD 
 * 
 * @name CrudRouter
 * @module commons/crud/router
 * @category Crud
 * @subcategory Roteamento
 */
class CrudRouter {
  /**
   * @constructor
   *
   * @param {any} routerConfig  Objeto de configuração do roteamento
   * 
   * @returns CrudRouter
   */
  constructor(routerConfig) {
    routerConfig = routerConfig || {};
    // por padrao se a nao for passado valor para mergeParams
    // para considerar padroes de outros mapeamentos
    if (!routerConfig.mergeParams && routerConfig.mergeParams !== false) {
      routerConfig.mergeParams = true;
    }
    this.routerConfig = routerConfig;
  }

  /**
   * Inicializa o objeto para registrar o roteamento
   *
   * @returns {any}
   */
  createRouter() {
    return express.Router(this.routerConfig);
  }

  /**
   * Retorna o objeto de rota inicializado e com as rotas carregadas
   *
   * @returns {any}
   */
  router() {
    return this.loadRouter();
  }


  /**
   * Retorna o serviço responsável por responder as requisições
   *
   * @returns {any}
   */
  service() {
    return null;
  }

  /**
   * Carrega rotas derivadas da rota do objeto
   *
   * @param {any} router  Objeto para o registro das rotas
   * 
   * @returns {any}
   */
  loadChild(router) {
    return router;
  }

  
  /**
   * Retorna as tarefas para serem executadas antes da
   * resposta do serviço
   *
   * @param {any} action  Ação a ser requisitadas
   * 
   * @returns {any}
   */
  getPrevTasks(action = null) {
    return [];
  }

  /**
   * Retorna as tarefas para serem executadas depois da
   * resposta do serviço
   *
   * @param {any} action  Ação a ser requisitadas
   * 
   * @returns {any}
   */
  getNextTasks(action = null) {
    return [];
  }

    /**
   * Retorna as tarefas para serem executadas para a
   * autenticação
   *
   * @param {any} action  Ação a ser requisitadas
   * 
   * @returns {any}
   */
  getAuthTask(action) {
    return [];
  }

  /**
   * Transforma o objeto em uma lista caso não seja
   *
   * @param {any} val  Valor a ser alterado
   * 
   * @returns {any}
   * 
   * @todo posicionar em uma classe mais genérica
   */
  getArray(val) {
    return Array.isArray(val) ? val : [val];
  }

  /**
   * Prepara as tarefas para serem registradas no roteador
   *
   * @param {any} tasks  Tarefas a serem complementadas
   * @param {any} action  Ação a ser requisitadas
   * 
   * @returns {any}
   */

  getTaks(tasks, action = null) {
    return [
      ...this.getArray(this.getAuthTask(action)),
      ...this.getArray(this.getPrevTasks(action)),
      ...this.getArray(tasks),
      ...this.getArray(this.getNextTasks(action)),
    ];
  }

  /**
   * Carrega rotas no objeto de registrar o roteamento
   *
   * @returns {any}
   */
  loadRouter() {
    const router = this._router || this.createRouter();
    const service = this.service();

    //find
    router.get.apply(router, [
      '/',
      ...this.getTaks(service.fetch.bind(service), 'fetch'),
    ]);
    //byId
    router.get.apply(router, [
      '/:id',
      ...this.getTaks(service.find.bind(service), 'find'),
    ]);
    //find
    router.post.apply(router, [
      '/fetch',
      ...this.getTaks(service.fetch.bind(service), 'fetch'),
    ]);
    // fetchOptions
    router.post.apply(router, [
      '/fetchOptions',
      ...this.getTaks(service.fetch.bind(service), 'fetch'),
    ]);
    //delete
    router.delete.apply(router, [
      '/:id',
      ...this.getTaks(service.remove.bind(service), 'remove'),
    ]);
    //create
    router.post.apply(router, [
      '/',
      ...this.getTaks(service.create.bind(service), 'create'),
    ]);
    //update
    router.put.apply(router, [
      '/:id',
      ...this.getTaks(service.update.bind(service), 'update'),
    ]);

    return this.loadChild(router);
  }
}

module.exports = CrudRouter;
