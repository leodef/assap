  
 /**
 * Funções genéricas para o tratamento de coleções de pesquisa com
 *   Ordenação
 *   Paginação
 *   Filtro
 *   Seleção de campos
 * 
 * @name CollectionUtils
 * @module commons/crud/utils
 * @category Crud
 * @subcategory Utils
 */
class CollectionUtils {

  /**
   * Insere o tratamendo de coleção em uma requisição
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} model Objeto para executar ações na base de dados
   *
   * @returns {any}
   */
  static resolveReq(req, model) {
    const config = CollectionUtils.loadConfig(req);
    const query = CollectionUtils.getCollectionQuery(config);
    return CollectionUtils.resolveCollectionQuery(query, model);
  }

  /**
   * Carrega a configuração para a paginação da requisição
   * 
   * @param {any} req Requisição ao servidor
   *
   * @returns {any}
   */
  static loadConfig(req) {
    let { params, query, body } = req;
    params = params || {};
    query = query || {};
    body = body || {};

    const config = {};

    // pagination
    const pagination = !!body && !!body.pagination ? body.pagination : {};
    pagination.limit = Number(
      pagination.limit || params.limit || query.limit || 0
    );
    pagination.page = Number(pagination.page || params.page || query.page || 1);
    config.pagination = pagination;

    // fields
    // 'campo1 campo2 campo3' =] => {'campo1': 1, 'campo2': 1, 'campo3': 1}
    // ['campo1', 'campo2', 'campo3'] => {'campo1': 1, 'campo2': 1, 'campo3': 1}
    config.fields = body.fields || params.fields || query.fields;
    // verifica se fields é um objeto
    if (config.fields) {
      try {
        // JSON
        config.fields = JSON.parse(config.fields);
      } catch (err) {
        config.fields = String(config.fields);
      }
      // verifica se fields e um array
      if (typeof config.fields === 'string') {
        // String
        config.fields = config.fields.split(/[ ,]+/);
      }
      if (Array.isArray(config.fields)) {
        // Array
        const fields = {};
        config.fields.forEach((field) => (fields[field] = 1));
        config.fields = fields;
      } else {
        // Object
        Object.keys(config.fields).forEach((key) => (config.fields[key] = 1));
      }
    }

    // filter
    // {'campo1': 1, 'campo2': 1, 'campo3': 1}' => {'campo1': 1, 'campo2': 1, 'campo3': 1}
    config.filter = body.filter || params.filter || query.filter;
    // verifica se filter é um objeto
    if (config.filter) {
      try {
        // JSON
        config.filter =
          typeof config.filter === 'string'
            ? JSON.parse(config.filter)
            : config.filter;
      } catch (err) {
        // String
        config.filter = null;
      }
    }

    // sort
    // 'campo1 campo2 campo3' =] => {'campo1': 1, 'campo2': 1, 'campo3': 1}
    // ['campo1', 'campo2', 'campo3'] => {'campo1': 1, 'campo2': 1, 'campo3': 1}
    config.sort = body.sort || params.sort || query.sort;
    if (config.sort) {
      try {
        // JSON
        config.sort = JSON.parse(config.sort);
      } catch (err) {}
      if (typeof config.sort === 'string') {
        // String
        config.sort = config.sort.split(/[ ,]+/);
      }
      if (Array.isArray(config.sort)) {
        // Array
        const sort = {};
        config.sort.forEach((field) => (sort[field] = 1));
        config.sort = sort;
      }
    }

    return config;
  }

  /**
   * Formatação do retorno da tarefa para o padrão
   * da resposta com coleção
   * 
   * @param {any} task Tarefa a ter o retorno formatado
   *
   * @returns {any}
   */
  static loadCollectionTaskResponse(task) {
    return task.then((response) =>
      CollectionUtils.loadCollectionResponse(response)
    );
  }

  /**
   * Formatação do objeto para o padrão da resposta com coleção
   * 
   * @param {any} response Objeto a ser formatado
   *
   * @returns {any}
   */
  static loadCollectionResponse(response) {
    response = Array.isArray(response) ? response[0] : response;
    return !response ? { items: [] } : response;
  }

  /**
   * Gera a query para ser concatenada a consulta para adaptar o
   *   retorno em forma coleção
   * 
   * @param {any} config Configuração para gerar a query
   * @param {any} prev Query para ser concatenada previamente
   *
   * @returns {any}
   */
  static getCollectionQuery(config, prev = []) {
    const { filter, pagination, fields, sort } = config || {};
    const { limit, page } = pagination || {};
    const paginate = Number(limit) > 0;
    // Se o filtro for um texto a procura e feitas pelo texto
    const filterQuery = !filter
      ? []
      : [
          // { $match: { $text: { $search: filter } } } :
          {
            $match: {
              $or: Object.keys(filter).map((key) => {
                const value = filter[key];
                const matchQuery = {};
                matchQuery[key] =
                  typeof value === 'string'
                    ? { $regex: value, $options: 'i' }
                    : value;
                return matchQuery;
              }),
            },
          },
        ];

    // Apenas pula os items para a pagina se tier limite
    const sortQuery = !sort ? [{ $sort: { createdAt: 1 } }] : [{ $sort: sort }];
    const skipQuery = !paginate ? [] : [{ $skip: limit * (page || 1) - limit }];
    const limitQuery = !paginate ? [] : [{ $limit: limit }];
    const fieldsQuery = !fields ? [] : [{ $project: fields }];
    const paginationValuesQuery = !paginate
      ? []
      : [
          {
            $group: {
              _id: null,
              count: {
                $sum: 1,
              },
            },
          },
          {
            $project: {
              count: 1,
              limit: {
                $literal: Number(limit),
              },
              page: {
                $literal: Number(page),
              },
            },
          },
          {
            $project: {
              _id: 0,
              count: 1,
              limit: 1,
              page: 1,
              pages: {
                $ceil: {
                  $divide: ['$count', '$limit'],
                },
              },
            },
          },
        ];
    const collectionResolveQuery = !paginate
      ? [
          {
            $facet: {
              items: [
                ...sortQuery,
                ...skipQuery,
                ...limitQuery,
                ...fieldsQuery,
              ],
            },
          },
        ]
      : [
          {
            $facet: {
              items: [
                ...sortQuery,
                ...skipQuery,
                ...limitQuery,
                ...fieldsQuery,
              ],
              pagination: [...paginationValuesQuery],
            },
          },
          {
            $project: {
              items: 1,
              pagination: {
                $arrayElemAt: ['$pagination', 0],
              },
            },
          },
        ];

    const aggregateQuery = [
      ...(prev || []),
      ...filterQuery,
      ...collectionResolveQuery,
    ];
    return aggregateQuery;
  }

  /**
   * Carrega a query para carregar a coleção e executa na base de dados
   * 
   * @param {any} query Query a ser executada previamente
   * @param {any} model Objeto para executar ações na base de dados
   *
   * @returns {any}
   */
  static resolveCollectionQuery(query, model) {
    const collectionQuery = this.getCollectionQuery(req);
    return model.aggregate(collectionQuery);
  }

}
module.exports = CollectionUtils;

/*
  async resolveReq(req, model) {
    const config = this.loadConfig(req);
    const { filter, pagination, fields } = config;
    const { limit, page } = pagination;
    // Quantidade total retornada pela pesquisa
    const count = await model.count(filter);
    // Apenas pula os items para a pagina se tier limite
    const skip = (Number(limit) === 0) ? ((limit * page) - limit) : 0
    if(typeof filter === 'string') {
      // Se o filtro for um texto a procura e feitas pelo texto
      filter = {$text: {$search: filter}}
    }
    const items = await model.find(filter, fields)
      // Pula as paginas anteriores
      .skip(skip)
      // Apenas limita consulta se tiver limite
      .limit(Number(limit) !== 0 ? limit : count );
    const pages = Math.ceil(count / limit)
    return {
      ...( (limit > 0 ) ?
        {
            pagination: {
            limit,
            page,
            count,
            pages
          }
        } : {}
      ),
      filter,
      fields
    }
  } */
