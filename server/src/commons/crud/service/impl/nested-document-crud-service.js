const CrudService = require('../crud-service');
const DocumentPersistence = require('../../persistence/impl/document-persistence');
const SubDocumentPersistence = require('../../persistence/impl/sub-document-persistence');
const CollectionUtils = require('../../utils/collection-utils');

 /**
 * Serviço com funções padrões para CRUD de um item embutido
 * em outro no sistema
 * 
 * @name NestedDocumentCrudService
 * @module commons/crud/service/impl
 * @category Crud
 * @subcategory Serviço
 */
class NestedDocumentCrudService extends CrudService {

  /**
   * @constructor
   *
   * @param {any} args  Objeto de configuração do serviço
   * 
   * @returns NestedDocumentCrudService
   */
  constructor(args) {
    super(args);
    const {
      parent,
      parentIdUrlName,
      parentField,
      persistence,
      ...queries
    } = args;
    this.parentIdUrlName = parentIdUrlName;
    // this.documentPersistence = new DocumentPersistence(persistence);
    this.parentDocumentPersistence = new SubDocumentPersistence(
      parent.persistence,
      parent.field,
      persistence,
      queries,
      SubDocumentPersistence.DOCTYPE_OBJECT
    );
  }

  /**
   * Resolve a requisição de criação de um item embutido em outro no sistema
   *   1- Encontra documento pai
   *   2- Encontra o campo do documento pai do tipo lista
   *   3- Insere o documento no campo do tipo lista do documento pai
   * POST /:parent/
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   */
  create(req, res, next) {
    req.logger.silly(`${typeof this} - create`);
    const parentIdUrlName = this.parentIdUrlName;
    const body = this.getReqBody(req);

    // user
    const user = this.auth(req, res, next);

    // _id
    const _parentId = this.searchReqParam(req, parentIdUrlName);

    if (!user) {
      return;
    }

    const action = 'create';
    return this.resolveTask(
      req,
      res,
      next,
      this.createItem(_parentId, body),
      action
    );
  }

  /**
   * Cria um item embutido em outro  no sistema
   *
   * @param {any} _parentId Id do objeto pai
   * @param {any} body Item a ser criado
   *
   * @returns {any}
   */
  createItem(_parentId, body) {
    const parentDocumentPersistence = this.parentDocumentPersistence;
    return parentDocumentPersistence.create(_parentId, body)
  }

  /**
   * Resolve a requisição de atualização de um item no sistema
   *   1- Encontra documento pai
   *   2- Encontra o campo do documento pai do tipo lista
   *   3- Atualiza o documento referente ao id informado
   * PUT /:parent/:id - POST /:parent/:id
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   */
  update(req, res, next) {
    req.logger.silly(`${typeof this} - update`);
    const parentIdUrlName = this.parentIdUrlName;
    const body = this.getReqBody(req);

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');
    const _parentId = this.searchReqParam(req, parentIdUrlName);

    const action = 'update';
    return this.resolveTask(
      req,
      res,
      next,
      this.updateItem(_parentId, _id, body),
      action
    );
  }
  
  /**
   * Atualiza um item no sistema
   *
   * @param {any} body Item a ser criado
   *
   * @returns {any}
   */
  updateItem(_parentId, _id, body) {
    const parentDocumentPersistence = this.parentDocumentPersistence;
    return parentDocumentPersistence.update(
      _parentId,
      { _id: parentDocumentPersistence.id(_id || body._id) },
      body
    );
  }

  /**
   * Resolve a requisição de remoção de um item no sistema
   *   1- Encontra documento pai
   *   2- Encontra o campo do documento pai do tipo lista
   *   3- Deleta o documento referente ao id informado
   * DELETE /:parent/:id
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   */
  remove(req, res, next) {
    req.logger.silly(`${typeof this} - remove`);
    const parentIdUrlName = this.parentIdUrlName;

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');
    const _parentId = this.searchReqParam(req, parentIdUrlName);

    const action = 'remove';
    return this.resolveTask(
      req,
      res,
      next,
      this.removeItem(_parentId, _id),
      action
    );
  }

  /**
   * Remove um item no sistema
   *
   * @param {any} body Item a ser criado
   *
   * @returns {any}
   */
  removeItem(_parentId, _id) {
    const parentDocumentPersistence = this.parentDocumentPersistence;
    return parentDocumentPersistence.remove(_parentId, {
      _id: parentDocumentPersistence.id(_id),
    });
  }

  /**
   * Resolve a requisição de consulta de um item no sistema
   *   1- Retorna documento encontrado na lista
   * GET /:parent/:id
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   */
  find(req, res, next) {
    req.logger.silly(`${typeof this} - find`);
    const parentIdUrlName = this.parentIdUrlName;
    const parentDocumentPersistence = this.parentDocumentPersistence;

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');
    const _parentId = this.searchReqParam(req, parentIdUrlName);

    const action = 'find';
    return this.resolveTask(
      req,
      res,
      next,
      parentDocumentPersistence.find(_parentId, {
        _id: parentDocumentPersistence.id(_id),
      }),
      action
    );
  }

  /**
   * Resolve a requisição de consulta no sistema
   *   1- Encontra documento pai
   *   2- Retorna o campo do documento pai do tipo lista
   * POST /:parent/fetch   - GET /:parent/
   *
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   */
  fetch(req, res, next) {
    req.logger.silly(`${typeof this} - fetch`);

    const { params } = req;

    const parentIdUrlName = this.parentIdUrlName;
    const parentDocumentPersistence = this.parentDocumentPersistence;

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _parentId = this.searchReqParam(req, parentIdUrlName);
    // query
    const fetchQuery = parentDocumentPersistence.getFetchQuery(_parentId);
    const query = documentPersistence.loadQuery(fetchQuery, params);
    const config = CollectionUtils.loadConfig(req);
    const collectionQuery = CollectionUtils.getCollectionQuery(config, query);
    const action = 'fetch';
    return this.resolveTask(
      req,
      res,
      next,
      CollectionUtils.loadCollectionTaskResponse(
        parentDocumentPersistence.aggregate(collectionQuery)
      ),
      action
    );
  }
}
module.exports = NestedDocumentCrudService;
