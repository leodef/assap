const CrudService = require('../crud-service');
const DocumentPersistence = require('../../persistence/impl/document-persistence');
const CollectionUtils = require('../../utils/collection-utils');

 /**
 * Serviço com funções padrões para CRUD no sistema
 * 
 * @name DocumentCrudService
 * @module commons/crud/service/impl
 * @category Crud
 * @subcategory Serviço
 */
class DocumentCrudService extends CrudService {
  /**
   * @constructor
   *
   * @param {any} args  Objeto de configuração do serviço
   * 
   * @returns DocumentCrudService
   */
  constructor(args) {
    super(args);
    const { persistence, ...queries } = args;
    if (persistence) {
      this.documentPersistence = new DocumentPersistence(
        persistence,
        queries
      );
    }
  }

  /**
   * Resolve a requisição de criação de um item no sistema
   * POST /
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   */
  create(req, res, next) {
    req.logger.silly(`${typeof this} - create`);
    const body = this.getReqBody(req);

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    const action = 'create';
    return this.resolveTask(
      req,
      res,
      next,
      this.createItem(body),
      action
    );
  }

  /**
   * Cria um item no sistema
   *
   * @param {any} body Item a ser criado
   *
   * @returns {any}
   */
  createItem(body) {
    const documentPersistence = this.documentPersistence;
    return documentPersistence.create(body)
  }


  /**
   * Resolve a requisição de atualização de um item no sistema
   * PUT /:id - POST /:parent/:id
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   */
  update(req, res, next) {
    req.logger.silly(`${typeof this} - update`);
    const body = this.getReqBody(req);

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');

    const action = 'update';
    return this.resolveTask(
      req,
      res,
      next,
      this.updateItem(_id, body),
      action
    );
  }

  /**
   * Atualiza um item no sistema
   *
   * @param {any} body Item a ser criado
   *
   * @returns {any}
   */
  updateItem(_id, body) {
    if(Array.isArray(_id)){
      return Promise.all(
        _id.map(
          (cId, index) => this.updateItem(cId, body[index])
        )
      )
    }
    _id = _id || body._id;
    const documentPersistence = this.documentPersistence;
    return documentPersistence.update({ _id }, body);
  }


  /**
   * Resolve a requisição de remoção de um item no sistema
   * DELETE /:id
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   */
  remove(req, res, next) {
    req.logger.silly(`${typeof this} - remove`);

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');

    const action = 'remove';
    return this.resolveTask(
      req,
      res,
      next,
      this.removeItem(_id),
      action
    );
  }

  /**
   * Remove um item no sistema
   *
   * @param {any} body Item a ser criado
   *
   * @returns {any}
   */
  removeItem(_id) {
    const documentPersistence = this.documentPersistence;
    if(Array.isArray(_id)){
      return Promise.all(_id.map( (cId) => this.removeItem(cId)))
    }
    return documentPersistence.remove({ _id });
  }


  /**
   * Resolve a requisição de consulta de um item no sistema
   * GET /:id
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   */
  find(req, res, next) {
    req.logger.silly(`${typeof this} - find`);
    const documentPersistence = this.documentPersistence;

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');

    const action = 'find';
    return this.resolveTask(
      req,
      res,
      next,
      documentPersistence.find({ _id }),
      action
    );
  }


  /**
   * Resolve a requisição de consulta no sistema
   * POST /:parent/fetch
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   */
  fetch(req, res, next) {
    req.logger.silly(`${typeof this} - all`);
    const documentPersistence = this.documentPersistence;

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }
    const { params } = req;
    const filterQuery = documentPersistence.getFilterQuery(
      this.getUrlFilters(),
      params
    );
    const config = CollectionUtils.loadConfig(req);
    const query = documentPersistence.loadQuery(filterQuery, params);
    const collectionQuery = CollectionUtils.getCollectionQuery(config, query);
    const action = 'fetch';
    return this.resolveTask(
      req,
      res,
      next,
      CollectionUtils.loadCollectionTaskResponse(
        documentPersistence.aggregate(collectionQuery)
      ),
      action
    );
  }
}
module.exports = DocumentCrudService;
