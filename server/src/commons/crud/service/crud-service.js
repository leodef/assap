
 /**
 * Serviço com funções genéricas para CRUD
 * 
 * @name CrudService
 * @module commons/crud/service
 * @category Crud
 * @subcategory Serviço
 */
class CrudService {
  urlFilters = {};

  /**
   * @constructor
   *
   * @param {any} args  Objeto de configuração do serviço
   * 
   * @returns CrudService
   */
  constructor(args) {
    const {
      urlFilters,
      loadBody,
      prevCallback,
      nextCallback
    } = (args || {});
    this.urlFilters = (urlFilters || {});
    this.loadBody = loadBody || ((body) => body);
    this.prevCallback = prevCallback;
    this.nextCallback = nextCallback;
  }

  /**
   * Verifica se existe alguma autenticação na sessão
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   *
   * @returns {any}
   * @todo Está retornando um valor fixo, deve ser alterado ao implementar
   *    a autenticação
   */
  auth(req, res, next) {
    return true;
  }

  /**
   * Procura um valor dentro da requisição
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} fieldName Nome do campo a ser encontrado
   *
   * @returns {any}
   * @todo Está retornando um valor fixo, deve ser alterado ao implementar
   *    a autenticação
   */
  searchReqParam(req, fieldName) {
    const { body, params, query } = req;
    if (body && body[fieldName]) {
      return body[fieldName]
    }
    if (params && params[fieldName]) {
      return params[fieldName]
    }
    if (query && query[fieldName]) {
      return query[fieldName]
    }
    return null;
  }


  /**
   * Concatena tarefas a serem executadas antes e depois da tarefa
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   * @param {any} task Tarefas a ser executada
   * @param {any} action Ação requisitada
   *
   * @returns {any}
   */
  resolveTask(req, res, next, task, action) {
    return this.getPrevCallback(req, res, next, action)
      .then((result) => task)
      .then((result) => this.getNextCallback(req, res, next, action, result))
      .then((result) => req.handleResponse(req, res, result))
      .catch((err) => req.handleError(req, res, err));
  }
  /**
   * Retorna os filtros padrões para a url
   * 
   * @returns {any}
   */
  getUrlFilters() {
    /*
      { 
        fieldName: urlParamName,
        fieldName2: { _id: { urlParamName } }
      }
    */
    return this.urlFilters;
  }
  /**
   * Retorna a corpo da requisição carregado
   * 
   * @param {any} req Requisição ao servidor
   *
   * @returns {any}
   */
  getReqBody(req) {
    const { body, params } = req;
    return this.loadBody(
      this.updateBodyWithUrlFilters(
        body,
        params
      ),
      params
    );
  }

  /**
   * Retorna o corpo da requisição atualizado com os parametros
   * 
   * @param {any} body Objeto a ser carregado
   * @param {any} param Parametros para carregar o objeto
   *
   * @returns {any}
   */
  updateBodyWithUrlFilters(body, params) {
    const urlFilters = this.getUrlFilters();
    Object.keys(urlFilters).forEach((fieldName) => {
      let urlParamName = urlFilters[fieldName];
      urlParamName = urlParamName._id || urlParamName;
      const urlVal = params[urlParamName];
      body[fieldName] = urlVal;
    });
    return body;
  }

  /**
   * Ações a serem executadas antes da resposta do serviço
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   * @param {any} action Ação requisitada
   *
   * @returns {any}
   */
  getPrevCallback(req, res, next, action) {
    const prevCallback = this.prevCallback;
    return prevCallback
      ? prevCallback(req, res, next, action)
      : Promise.resolve(action);
  }


  /**
   * Ações a serem executadas depois da resposta do serviço
   * 
   * @param {any} req Requisição ao servidor
   * @param {any} res Resposta do servidor
   * @param {any} next Comando para seguir a execução das proximas
   *                tarefas encadeadas
   * @param {any} action Ação requisitada
   *
   * @returns {any}
   */
  getNextCallback(req, res, next, action, result) {
    const nextCallback = this.nextCallback;
    return nextCallback
      ? nextCallback(req, res, next, action)
      : Promise.resolve(result);
  }
}
module.exports = CrudService;
