const http = require('http');
const tasks = require('./config/tasks');
const app = require('./config/express');
const logger = require('./config/logger');

/**
 * Classe principal do servidor, contem toda configuração e regra de negocios do servidor
 * @alias Server'
 * @typedef {object} Server'
 */

/**
 * Importa as configurações do ambiente de acordo com o perfil
 */
const env = app.get('env');

/**
 * Server
 */

/**
 * Inicializa o servidor HTTP
 * 
 * @type Server
 */
http.createServer(app).listen(
  env.port, // port
  // hostname
  () => { // callback
  logger.info(`BACKEND is running on port ${env.port}.`);
});
