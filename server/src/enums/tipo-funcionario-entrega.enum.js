/**
 * Enum de tipo de pessoa
 * 
 * @enum
 * @mame TIPO_FUNCIONARIO_ENTREGA
 */
const TIPO_FUNCIONARIO_ENTREGA = [
    'VENDEDOR',
    'MOTORISTA',
    'AUXILIAR',
    'GERAL'
  ];
  
  module.exports = TIPO_FUNCIONARIO_ENTREGA;