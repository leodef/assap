/**
 * Enum de opções de status de entrega
 * 
 * @enum
 * @mame STATUS_ENTREGA
 */
const STATUS_ENTREGA = [
  'PENDENTE',
  'INICIADO',
  'FINALIZADO'
];

module.exports = STATUS_ENTREGA;