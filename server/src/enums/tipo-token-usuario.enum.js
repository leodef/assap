/**
 * Enum de opções de tipo de token do usuário
 * 
 * @enum
 * @mame TIPO_TOKEN_USUARIO
 */
const TIPO_TOKEN_USUARIO = [
  'RESETAR_SENHA',
  'CONFIRMAR_CONTA'
];

module.exports = TIPO_TOKEN_USUARIO;
