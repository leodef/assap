/**
 * Enum de tipo de limite de entrega
 * 
 * @enum
 * @mame TIPO_LIMITE_ROTA
 */
const TIPO_LIMITE_ROTA = [
  'MANUAL',
  'TRANSPORTE',
  'INDEFINIDO'
];
module.exports = TIPO_LIMITE_ROTA;