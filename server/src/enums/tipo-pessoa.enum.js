
/**
 * Enum de tipo de pessoa
 * 
 * @enum
 * @mame TIPO_PESSOA
 */
const TIPO_PESSOA = [
  'FISICA',
  'JURIDICA'
];

module.exports = TIPO_PESSOA;