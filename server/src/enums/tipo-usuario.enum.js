/**
 * Enum de opções de tipo de token do usuário
 * 
 * @enum
 * @mame TIPO_USUARIO
 */
const TIPO_USUARIO = [
  'ADMIN',
  'ADMIN_EMPRESA',
  'FUNCIONARIO'
];

module.exports = TIPO_USUARIO;
