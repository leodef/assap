/**
 * Enum modo de formação de um valor, como ele foi gerado
 * 
 * @enum
 * @mame TIPO_FORMACAO
 */
const TIPO_FORMACAO = [
  'MANUAL',
  'AUTOMATIZADO',
  'INDEFINIDO'
];
module.exports = TIPO_FORMACAO;