/**
 * Enum de opções de algoritimo de rotas
 * 
 * @enum
 * @mame ALGORITIMO_ROTA
 */
const ALGORITIMO_ROTA = [
  'nearestNeighbor',
  'naive'
];

module.exports = ALGORITIMO_ROTA;
