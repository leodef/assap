
/**
 * Enum de tipo de pessoa
 * 
 * @enum
 * @mame TIPO_FUNCAO_VEICULO
 */
const TIPO_FUNCAO_VEICULO = [
  'CAVALO',
  'CARRETA'
];

module.exports = TIPO_FUNCAO_VEICULO;