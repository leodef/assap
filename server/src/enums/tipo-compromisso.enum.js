/**
 * Enum de tipos de compromisso
 * 
 * @enum
 * @mame TIPO_COMPROMISSO
 */
const TIPO_COMPROMISSO = [
  'geral',
  'entrega'
];

module.exports = TIPO_COMPROMISSO;
