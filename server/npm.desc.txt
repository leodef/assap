<#
  app-root-path
  bcrypt
  bluebird
  body-parser
  chai
  connect-multiparty
  cookie-parser
  cors
  express
  express-session
  formidable
  fs
  fs-extra
  generate-password
  helmet
  lodash
  method-override
  mocha
  moment
  mongo
  mongoose
  morgan
  node-cron
  nodemailer
  passport
  passport-http-bearer
  pm2
  winston
#>
npm i `
 app-root-path <# access application's root path from anywhere #> `
 bcrypt <# A library to help you hash passwords #> `
 bluebird <# Promises in node #> `
 body-parser <# HTTP Request Parser #> `
 connect-multiparty <# HTTP Multiparty Request #> `
 cookie-parser <# HTTP Request Cookie #> `
 cors <# Handle CORS Security #> `
 express <# App execution flow #> `
 express-session <# Sessions for Express lib #> `
 formidable <# uploading and encoding images and videos #> `
 fs <# File System #> `
 fs-extra <# adds file system methods that arent included in the native fs #> `
 generate-password <# Generate password #> `
 helmet <# Security app #> `
 lodash <# Common utils Obj Str Num etc... #> `
 method-override <# Override method for browser that not support it #> `
 moment <# Data utils #> `
 mongoose <# Connect with MongoDB #> `
 morgan <# HTTP request logger middlewar #> `
 nodemailer <# Send mails #> `
 passport <# Auth lib #> `
 passport-http-bearer <# Bearer token Auth lib #> `
 winston <# simple and universal logging library with support for multiple transports #> `
 bull <# Redis-based queue for Node #> `
 bull-board <# help you visualize your queues and their jobs #>

npm i -D `
 yarn <# Yarn caches every package it has downloaded #> `
 chai <# Chai is a BDD / TDD assertion library #> `
 mocha <# Test frameworkUnitel #> `
 nodemon <# restarting the node application when file changes #> `
 npm-run-all <# CLI tool to run multiple npm-scripts in parallel or sequential #> `
 sucrase <# Sucrase is an alternative to Babel that allows super-fast development builds #> `
 pm2 <# Advanced process manager for production Node.js applications #>