import React from 'react';
import {
  Table as TableMui,
  TableBody as TableBodyMui,
  TableCell,
  TableContainer,
  TableHead as TableHeadMui,
  TableRow,
  Paper,
  IconButton
} from '@material-ui/core';
import {
  Edit as EditIcon,
  Visibility as VisibilityIcon,
  Delete as DeleteIcon
} from '@material-ui/icons';
import './Table.scss';

function getFieldsObj(fields: Array<any>): Array<TablePropsField> {
	return fields
	  .filter( (val: TablePropsField | String ) => !!val )
	  .map( (val: TablePropsField | String ) => {
		if(val instanceof TablePropsField){
		  return val; 
		} else {
		  return new TablePropsField(val.toString());
		}
	  });
}
  
export class TablePropsField {
  constructor(
	public name: string,
	public label?: string,
	public type: string = 'text'
  ) {
	this.label = this.label || this.name;
  }
}


/**
 * Tabela genérica para exibir coleção de elementos
 *   Table
 * @param {TableProps} props Propriedades
 * @return {React.Component} Componentes com tabela genérica para exibir coleção de elementos
 */
export const Table = (props: any) =>  {
  const { config, items } = useContext(CollectionContext);
  const { label, align, fields, actionsLabel, actions } = config;
  const fieldsObj = useMemo(() => getFieldsObj(fields), [fields]);
  const showActions = useMemo( () => (
		!actions || (
			!actions.toRemove &&
			!actions.toEdit &&
			!actions.toShow
		)
	), [actions]);
  return (
    <TableContainer component={Paper}>
		<TableMui aria-label={label}>
			<TableHeadMui>
				<TableRow>
					{ /* Colunas*/ }
					{ fieldsObj.map( (field, fieldIndex) =>
					  (<TableCell align={align}>
						{row[field.name]}
					  </TableCell>)
					)}
					{ /* Coluna actions */ }
					{ (showActions) ? 
						(<TableCell className="actions-cell">
							<ActionColumn  {...props}/>
						</TableCell>) :
						null
					}
				</TableRow>
			
			</TableHeadMui>
	
            <TableBodyMui>
				{fieldsObj.map( (item: any, itemIndex: number) => (
					<TableRow key={itemIndex}>
						{ /* Colunas */ }
						{
							fieldsObj.map( (field: TablePropsField, fieldIndex: number) => (
							  <TableCell align={align}>
								{row[field.name]}
							  </TableCell>
							))
						}
						{ /* Coluna actions */ }
						{
							(showActions) ? 
							(
								<TableCell className="actions-cell">
								  <ActionColumn
									item={item}
									key={itemIndex} />
								</TableCell>
							) : null
						}
					</TableRow>)
				)}
			</TableBodyMui>

      </TableMui>
    </TableContainer>
  )
};

/**
 * Botões para linha da tabela genérica para exibir coleção de elementos
 *   Table.TableBody.Row.ActionColumn
 * @param {row: any, config: TableConfig, key: any } props Propriedades
 * @return {React.Component} Componente com botões para linha da tabela genérica para exibir coleção de elementos
 */
const ActionColumn = (props: RowProps) =>  {
  // CollectionContext
  const { config } = useContext(CollectionContext);
  const { actions } = config;
  // item 
  const { item, key } = props;
  
	// dispatch
	const dispatch = getDispatch();
	const setAction = useCallback( (action: ActionEnum, item: any) =>
		dispatch({ types: config.SET_ACTION_ITEM, payload: { action, item } })
	, [dispatch, item]);
	const toShow = useCallback( (item: any) => {
		setAction(ActionEnum.SHOW, item);
	}, [setAction]);
	const toEdit = useCallback( (item: any) => {
		setAction(ActionEnum.EDIT, item);
	}, [setAction]);
	const toRemove = useCallback( (item: any) => {
		setAction(ActionEnum.REMOVE, item);
	}, [setAction]);
  return (
    <React.Fragment>
      {
        !!actions.toShow ?
          (
          <IconButton
            aria-label="Visualizar"
            onClick={() =>  toShow(item)}>
            <VisibilityIcon />
          </IconButton>
          ) :
          null
      }
      {
        !!actions.toEdit ?
          (
          <IconButton
            aria-label="Editar"
            onClick={() => toEdit(item)}>
            <EditIcon />
          </IconButton>
          ) :
          null
      }
      {
        !!actions.toRemove ?
          (
          <IconButton
            aria-label="Remover"
            onClick={() => toRemove(item)}>
            <DeleteIcon />
          </IconButton>
          ) :
          null
      }
    </React.Fragment>
  )
};