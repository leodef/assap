
/**
 * Lista genérica para coleção de items 
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com lista genérica para coleção de items 
 */
export const List = (props: any) => {
	// CollectionContext
  const { config, items } = useContext(CollectionContext);
  const {dense, subheader } = config;
    return (
      <ListMui
        aria-labelledby="nested-list-subheader"
        subheader={
			!!subheader ? (
				<ListSubheaderMui component="div" id="nested-list-subheader">
				  {subheader}
				</ListSubheaderMui>) :
				null
			}
        dense={dense}>
          {items.map( (item: any, index: number) =>
            <ListItem item={item} key={index} />
          )}
      </ListMui>
    );
};

/**
 * Item da lista genérica para coleção de items
 *   List.ListItem
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com item da lista genérica para coleção de items 
 */
export const ListItem = (props: {item: any, key: number}) => {
	// CollectionContext
  const { config } = useContext(CollectionContext);
  const { getListItem } = config;
  // item 
  const { primary, secondary, avatar } = useMemo(() => getListItem(props.item, props.key), [props]);

  return (
    <ListItemMui key={key}>
		{
			!!avatar ? (
				<ListItemAvatarMui>
					<Avatar>
					  {avatar}
					</Avatar>
				</ListItemAvatarMui>
			) : null;
		}
      
      <ListItemText
        primary={primary}
        secondary={
          !!secondary ?
            secondary :
            null
        } />
      <ListItemActions
        {...props} />
    </ListItemMui> 
  );
};


/**
 * Menu customizado
 * @param {MenuProps} props Propriedades
 * @return {React.Component} Componente com menu customizado
 */
const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

/**
 * Fabrica de item de menu customizado
 * @param {any} theme Propriedades
 * @return {method} Componente com fabrica de item de menu customizado
 */
const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

/**
 * Botões do item da lista genérica para coleção de items
 *   List.ListItem.ListItemActions
 * @param {item: ListPropsItem, index: number, config: ListConfig} props Propriedades
 * @return {React.Component} Componente com botões do item da lista genérica para coleção de items 
 */
export const ListItemActions = (props: {item: any, key: number}) => {
  // CollectionContext
  const { config } = useContext(CollectionContext);
  const { actions } = config;
  // item 
  const { item, key } = props;
  
	// dispatch
	const dispatch = getDispatch();
	const setAction = useCallback( (action: ActionEnum, item: any) =>
		dispatch({ types: config.SET_ACTION_ITEM, payload: { action, item } })
	, [dispatch, item]);
	const toShow = useCallback( (item: any) => {
		setAction(ActionEnum.SHOW, item);
	}, [setAction]);
	const toEdit = useCallback( (item: any) => {
		setAction(ActionEnum.EDIT, item);
	}, [setAction]);
	const toRemove = useCallback( (item: any) => {
		setAction(ActionEnum.REMOVE, item);
	}, [setAction]);

  // Menu control
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };


  if (!actions || (
	!actions.toShow &&
	!actions.toEdit &&
	!actions.toRemove)
	) {
    return null;
  }

  return (
    <ListItemSecondaryAction>
      <div>
        <IconButton
            aria-label="more"
            aria-controls="long-menu"
            aria-haspopup="true"
            onClick={handleClick}
          >
            <MoreVertIcon />
        </IconButton>
        <StyledMenu
          id="customized-menu"
          anchorEl={anchorEl}
          keepMounted
          open={!!anchorEl}
          onClose={handleClose} >
          {
            !!actions.toShow ?
              (<StyledMenuItem onClick={() => toShow(item)}>
                <ListItemIcon>
                  <VisibilityIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary="Visualizar" />
              </StyledMenuItem>) :
              null
          }
          {
            !!actions.toEdit ?
              (<StyledMenuItem onClick={() => toEdit(item)}>
                <ListItemIcon>
                  <EditIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary="Editar" />
              </StyledMenuItem>) :
              null
          }
          {
            !!actions.toRemove ?
              (<StyledMenuItem onClick={() => toRemove(item)}>
                  <ListItemIcon>
                    <DeleteIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText primary="Remover" />
                </StyledMenuItem>) :
              null
          }
        </StyledMenu>
      </div>
    </ListItemSecondaryAction>
  );
};
