// Backend
	Query
		var name="john";
		UserSchema.find({name: { $regex: '.*' + name + '.*' } }).limit(5);
		
				

		db.articles.find( { $text: { $search: "coffee" } } )



	Pagination
		MyModel.find(query, fields, { skip: 10, limit: 5 }, function(err, results) { ... })

		Event.find()
			.select('name')
			.limit(limit)
			.skip(limit * page)
			.sort({
				name: 'asc'
			})
			.exec(function(err, events) {
				Event.count().exec(function(err, count) {
					res.render('events', {
						events: events,
						page: page,
						pages: count / perPage
					})
				})
			})

// Frontend

// containers/Iem
// impoty { type } from '...';
export const Item = (props: any) => {

	// CrudContext
	const getState = useCallback(((state: any) => state.item));
	const actions = {toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true};
	const crudContextValue = useMemo( () => { getState, actions, types: type }, [getState, actions, type]);
	// values
	const collectionType = useMemo(() => 'TABLE');
	const action = useSelector( (state: any) => getState(state).action );
	const dispatch = useDispatch();
	const toList = useCallback( ()=> dispatch({type: type.SET_ACTION_ITEM, payload: {action: ActionTypeEnum.LIST} }), [action])
	const clear = useCallback( () =>
			dispatch({ type: type.CLEAR_ITEM, payload: location}),
			[dispatch, location] );
	// dialog
	const onClose = (event) => toList();
	const isShowModalOpen = useMemo(() => (action === ActionTypeEnum.SHOW), [action]);
	const isFormModalOpen = useMemo(() => (action === ActionTypeEnum.NEW || action === ActionTypeEnum.EDIT), [action]);
	const isRemoveDialogOpen = useMemo(() => (action === ActionTypeEnum.REMOVE), [action]);
	// lifecycle
	useEffect(()=> {
		return () => clear();
	}, [ clear ]);
	// render
	return(<CrudContext.Provider value={crudContextValue}>
		{/* Collection */}
		<Collection type={collectionType}/>
		{/* Form */}
		<Modal
		  open={isShowModalOpen}
		  onClose={onClose}
		  aria-labelledby="show-modal-title"
		  aria-describedby="show-modal-description">
		  <div>
			<Form />
		  </div>
		</Modal>
		{/* Show */}
		<Modal
		  open={isFormModalOpen}
		  onClose={onClose}
		  aria-labelledby="show-modal-title"
		  aria-describedby="show-modal-description">
		  <div>
			<Show />
		  </div>
		</Modal>
	</CrudContext.Provider);
}

// componentes/Item/Collection
export const Collection = (props: any) => {
	// props
	const type = useMemo(() => props.type, [props]);
	// CrudContext
	const { getState, actions, types } = useContext(CrudContext);
	// selector
	const { filter, items } = useSelector( (state: any) => getState(state));
	// dispatch
	const dispatch = useDispatch();
	const fetch = useCallback( () => dispatch({ types: type.FETCH_ITEM, payload: { filter }}), [filter] );
	// lifecqycle
	useEffect(()=> {
		fetch();
	}, [fetch, filter]);
	// CollectionContext
	const collectionContextValue = useMemo(() => {items, filter}, [items, filter]);
	const collectionContent = useMemo( () => type === CollectionTypeEnum.TABLE ? (<Table />) : (<List />), [type]);
	return(<CollectionContext.Provider value={}>{collectionContent}</CollectionContext.Provider);
}

// componentes/Item/Table
// import { Table as SharedTable } from 'componens/shared/Table';
export const Table = (props: any) => {
	
	// CrudContext
	const { actions } = useContext(CrudContext);

	// CollectionContext
	const context = useContext(CollectionContext);
	const collectionContextValue = useMemo( () => {
		return {
			...context,
			config: {
				label: 'Grandeza',
				align: 'right',
				fields: [
				  new TablePropsField('titulo', 'Título'),
				  new TablePropsField('desc', 'Descrição')
				],
				actions
			}
		}
	}, [context, actions]);
	return(
		<CollectionContext.Provider value={collectionContextValue}>
			<SharedTable />
		</CollectionContext.Provider>
	);
	
}

// componentes/Item/List
// import { List as SharedList } from 'componens/shared/List';
export const List  = (props: any) => {
	// CrudContext
	const { actions } = useContext(CrudContext);

	// CollectionContext
	const context = useContext(CollectionContext);
	const getListItem(value: any, index: number) {
		return {
			primary: (value.titulo ? value.titulo.toString() : ''),
			secondary: null,
			avatar: (!!value.titulo ? value.titulo.substr(2) : (<AssignmentIcon />))
		}
	}
	const collectionContextValue = useMemo( () => {
		return {
			...context,
			config: {
				dense: true,
				subheader: 'Grandeza',
				getListItem,
				actions
			}
		}
	}, [context, actions]);
	return(
		<CollectionContext.Provider value={collectionContextValue}>
			<SharedList />
		</CollectionContext.Provider>
	);
	
}

// componentes/Item/Show
export const Show = (props: any) => {
	const classes = useStyles();
	// CrudContext
	const { getState, actions, types } = useContext(CrudContext);
	// selector
	const item = useSelector( (state: any) => getState(state).item);

	return (
		<Card className={classes.root}>
		  <CardContent>
			<Typography className={classes.title} color="textSecondary" gutterBottom>
			  Grandeza
			</Typography>
			<Typography variant="h5" component="h2">
			  {item.titulo}
			</Typography>
			<Typography variant="body2" component="p">
			  {item.desc}
			</Typography>
		  </CardContent>
		  <CardActions>
			<ShowActions config={config} />
		  </CardActions>
		</Card>
	);
}

const ShowActions = (props: any) => {
	// CrudContext
	const { getState, actions, types } = useContext(CrudContext);
	// selector
	const item = useSelector( (state: any) => getState(state).item);
	// dispatch
	const dispatch = useDispatch();
	const setAction = useCallback( (action: ActionTypeEnum, item: any) =>
		dispatch({ types: config.SET_ACTION_ITEM, payload: { action, item } })
	, [dispatch, item]);
	const toEdit = useCallback( (item: any) => {
		setAction(ActionTypeEnum.EDIT, item);
	}, [setAction]);
	const toRemove = useCallback( (item: any) => {
		setAction(ActionTypeEnum.REMOVE, item);
	}, [setAction]);
	const back = useCallback( () => {
		setAction(ActionTypeEnum.LIST, null);
	}, [setAction]);
  return (
    <React.Fragment>       
    {
      actions.toEdit ?
        (
        <IconButton
          aria-label="Editar"
          onClick={() => toEdit(item) }>
          <EditIcon />
        </IconButton>
        ) :
        null
    }
    {
      actions.toRemove ?
        (
        <IconButton
          aria-label="Remover"
          onClick={() => toRemove(item) }>
          <DeleteIcon />
        </IconButton>
        ) :
        null
    } {
      actions.toBack ?
        (
        <IconButton
          aria-label="Voltar"
          onClick={() => back() }>
          <ArrowBackIcon />
        </IconButton>
        ) :
        null
    }
    </React.Fragment>
  );
})

// componentes/Item/RemoveDialog
export const RemoveDialog = (props: any) => {

	// CrudContext
	const { getState, actions, types } = useContext(CrudContext);
	// selector
	const item = useSelector( (state: any) => getState(state).item );
	// dispatch
	const dispatch = useDispatch();
	const setAction = useCallback( (action: ActionTypeEnum, item: any) =>
		dispatch({ types: config.SET_ACTION_ITEM, payload: { action, item } })
	, [dispatch, item]);
	const remove = useCallback( (item: any) =>
		dispatch({ types: config.DELETE_ITEM, payload: { item } })
	, [dispatch]);
	const back = useCallback( () => {
		setAction(ActionTypeEnum.LIST, null);
	}, [setAction]);
	
    return (<React.Fragment>
      <DialogTitle id="remove-dialog-title">{"Deseja realmente remover?"}</DialogTitle>
      <DialogActions>
        <Button onClick={() => back()} color="primary">
          Não
        </Button>
        <Button onClick={() => remove(item)} color="primary" autoFocus>
          Sim
        </Button>
      </DialogActions>
    </React.Fragment>);
}

// componentes/Item/Form
export const Form = (props: any) => {
	const dispatch = useDispatch();
	// CrudContext
	const { getState, actions, types } = useContext(CrudContext);
	// selector
	const item = useSelector( (state: any) => getState(state).item);
	// dispatch
	const dispatch = useDispatch();
	const save = useCallback( (item: any) =>
		dispatch({ types: config.SAVE_ITEM, payload: { item } })
	, [dispatch, item]);

	// submit
    const onFormSubmit = (values: any, formikHelpers: FormikHelpers<any>) => {
      const { setSubmitting } = formikHelpers;
      setSubmitting(false);
      save(values);
    }
	return(
        <Formik
          initialValues={item || initialValues}
          validationSchema={GrandezaSchema}
          onSubmit={onFormSubmit}
          render={(formProps: FormikProps<any>) => (
			<FormikForm>
			  <Fieldset form={formProps} />
			  <FormActions form={formProps} />
			</FormikForm>
			)}
        />);
	
}

const FormActions = (props: any) => {
	// CrudContext
	const { getState, actions, types } = useContext(CrudContext);
	// selector
	const item = useSelector( (state: any) => getState(state).item);
	// dispatch
	const dispatch = useDispatch();
	const setAction = useCallback( (action: ActionTypeEnum, item: any) =>
		dispatch({ types: config.SET_ACTION_ITEM, payload: { action, item } })
	, [dispatch, item]);
	const toRemove = useCallback( (item: any) => {
		setAction(ActionTypeEnum.REMOVE, item);
	}, [setAction]);
	const back = useCallback( () => {
		setAction(ActionTypeEnum.LIST, null);
	}, [setAction]);

	// render
	return (
		<Grid container>
		  <Grid item>
			<Button
			  type="submit"
			  variant="contained"
			  color="primary">
			  Salvar
			</Button>
		  </Grid>
		{  actions.toRemove ?
		  (<Grid item>
			<Button
			  variant="contained"
			  color="primary"
			  onClick={(event: any) => toRemove(item) }>
			  Remover
			</Button>
		  </Grid>) : null
		}
		{ actions.toBack ?
		  (<Grid item>
			<Button
			  variant="contained"
			  color="primary"
			  onClick={(event: any) => back()}>
			  Voltar
			</Button>
		  </Grid>) : null
		}
		</Grid>
	 );
}

// componentes/Item/Fieldset
export const Fieldset = React.memo((props: any) => {
	const fields = {
      titulo: {
        name: getFieldName('titulo', props)
      },
      desc: {
        name: getFieldName('desc', props)
      }
    }

    return (
      <Grid container spacing={2} component={Fieldset}>
        <Grid item xs={12} sm={6}>
          <Field
            component={UpperCasingTextField}
            name={fields.titulo.name}
            type="text"
            label="Titulo"
          />
        </Grid>
        <Grid item xs={12}>
          <Field
            component={TextField}
            name={fields.desc.name}
            type="text"
            label="Descrição"
            multiline
          />
        </Grid>
      </Grid>
    );
});


//Shared


// componentes/Shared/List
//** mudar para pegar configuração do contexto
export const SharedList = (props: any) => {
	return(<React.Fragment></React.Fragment);
}

// componentes/Shared/Table
//** mudar para pegar configuração do contexto
export const SharedTable = (props: any) => {
	return(<React.Fragment></React.Fragment);	
}

