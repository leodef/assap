field = schema
wrapper = null
pullPath = ''
paths = paths.map( (obj, index) => {
	const isLast = (index < paths.length - 1);
	const isFirst = (index === 0);
	let { path, ref, collectionName, foreignField } = obj;
	foreignField = (foreignField || '_id');
	fullPath = (fullPath+'.'+path);
	nextPath = (fullPath+'.'+path[index+1]);
	field = field[path];
	isList = Array.isArray(field);
	field = (isList ? field[0] : field);
	ref = (ref || field.options.ref);
	wrapper = (this.depedencies[ref] || wrapper.depedencies[ref]);
	collectionName = (collectionName || wrapper.model.collection.collectionName);
	return {
		isLast,
		isFirst,
		path,
		ref,
		collectionName,
		foreignField,
		fullPath,
		nextPath,
		isList
	}
})
paths.map((obj, index) => {
	const nextCollectionName = paths[index+1];
	return {
		nextCollectionName,
		...obj
	}
})

paths.reduce( (prev, curr) => {
	if(isLast){ return prev }
	const {
		isLast,
		isFirst,
		path,
		ref,
		collectionName,
		foreignField,
		fullPath,
		nextPath,
		isList
	} = curr
	const groupPathPush = {};
	const groupPathPush[`${path}`] = {'$push': `$${path}`};
	
	return [
		... prev,
		// unwind path
		...(isList ? [
			{
				'$unwind': `$${path}`
			}
		] : []),
		// lookup nextPath
		{
			'$lookup': {
			'from': collectionName,
			'localField': nextPath,
			'foreignField': foreignField, 
			'as': nextPath
			}
		},
		// unwind nextPath
		{
			'$unwind': `$${nextPath}`,
		},
		// group path
		{
			'$group': {
			  '_id': '$_id', 
			  'root': {
				'$mergeObjects': '$$ROOT'
			  }, 
			  groupPathPush
			}
		},
		// replaceRoot root
		{
			'$replaceRoot': {
			  'newRoot': {
				'$mergeObjects': [
				  '$root', '$$ROOT'
				]
			  }
			}
		},
		// project root
		{
			'$project': {
			  'root': 0
			}
		}
	]
})

[
  {
    '$unwind': '$elementos'
  }, {
    '$lookup': {
      'from': 'ccqelementos', 
      'localField': 'elementos.elemento', 
      'foreignField': '_id', 
      'as': 'elementos.elemento'
    }
  }, {
    '$unwind': '$elementos.elemento'
  }, {
    '$group': {
      '_id': '$_id', 
      'root': {
        '$mergeObjects': '$$ROOT'
      }, 
      'elementos': {
        '$push': '$elementos'
      }
    }
  }, {
    '$replaceRoot': {
      'newRoot': {
        '$mergeObjects': [
          '$root', '$$ROOT'
        ]
      }
    }
  }, {
    '$project': {
      'root': 0
    }
  }
]