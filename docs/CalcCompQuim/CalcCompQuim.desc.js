/*
	- Quando alterado o filtro atualizar dados de paginação com os items filtrodos, voltar para pagina 1
	- Implementar ParentCrudContext, os dados do parentes devem ser passados para o serviço, o servio escolhe a path
		da api de acordo com os parents envolvidos
	- CalcCompQuim - Gerenciamento de quantidade de substancias por composição de substancias
		
*/
	Elemento
	Substancia
	QuantidadeElemento
	ComposicaoSubstancia
	QuantidadeSubstancia
Telas

CRUD Elemento ADMIN
CRUD Substancia ADMIN
	CRUD QuantidadeElemento
CRUD	ComposiçãoSubstancia CLIENTE
	CRUD QuantidadeSubstancia (pesquisar subistancias por elementos)


Serviços

CRUD elemento/
CRUD substancia /:substancia /quantidade-elemento
CRUD substancia /:substancia /quantidade-elemento



Schemas
/*
Elemento
    titulo: string
    desc: string
*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var autoPopulate = function(next) {next(); };

const schemaFunc = (params) => {
	const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      desc: String
	}, {timestamps: true});
	schema.
		pre('findOne', autoPopulate).
		pre('find', autoPopulate);

	return schema;
}
const key = 'CCQElemento';
const depedencies = [];

module.exports = {
	key,
	schemaFunc,
	depedencies
}

/*
QuantidadeElemento
    elemento: Elemento
    quantidade: number (%)
*/

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var autoPopulate = function(next) {next(); };

const schemaFunc = (params) => {
	const schema = Schema({
      //_id: Schema.Types.ObjectId,
      elemento: { type: Schema.Types.ObjectId, ref: 'CCQElemento' },
      quantidade: Number
	}, {timestamps: true});
	schema.
		pre('findOne', autoPopulate).
		pre('find', autoPopulate);

	return schema;
}
const key = 'CCQQuantidadeElemento';
const depedencies = ['CCQElemento'];

module.exports = {
	key,
	schemaFunc,
	depedencies
}

/*
Substancia
    titulo: string
    desc: string
    valor: number
    elementos: Array<QuantidadeElemento>
*/

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var autoPopulate = function(next) {next(); };

const schemaFunc = (params) => {
	const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      desc: String,
      valor: Number,
      elementos: [params.depedencies.CCQQuantidadeElemento.schema]
	}, {timestamps: true});
	schema.
		pre('findOne', autoPopulate).
		pre('find', autoPopulate);

	return schema;
}
const key = 'CCQSubstancia';
const depedencies = ['CCQQuantidadeElemento'];

module.exports = {
	key,
	schemaFunc,
	depedencies
}
   
   /*
QuantidadeSubstancia
    substancia:Substancia
    quantidade: number
*/

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var autoPopulate = function(next) {next(); };

const schemaFunc = (params) => {
	const schema = Schema({
      //_id: Schema.Types.ObjectId,
      substancia: { type: Schema.Types.ObjectId, ref: 'CCQSubstancia' },
      quantidade: Number
	}, {timestamps: true});
	schema.
		pre('findOne', autoPopulate).
		pre('find', autoPopulate);

	return schema;
}
const key = 'CCQQuantidadeSubstancia';
const depedencies = ['CCQSubstancia'];

module.exports = {
	key,
	schemaFunc,
	depedencies
}

    /*
ComposiçãoSubstancia
    titulo: string
    desc: string
	valor: number
    substancias: Array<QuantidadeSubstancia>
*/

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var autoPopulate = function(next) {next(); };

const schemaFunc = (params) => {
	const schema = Schema({
      //_id: Schema.Types.ObjectId,
      titulo: String,
      desc: String,
      valor: Number,7
      substancias: [params.depedencies.CCQQuantidadeSubstancia.schema]
	}, {timestamps: true});
	schema.
		pre('findOne', autoPopulate).
		pre('find', autoPopulate);

	return schema;
}
const key = 'CCQComposiçãoSubstancia';
const depedencies = ['CCQQuantidadeSubstancia'];

module.exports = {
	key,
	schemaFunc,
	depedencies
}

/*
	
	IncompatibilidadeSubstancias
	substancias: Array<Substancia>

*/

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var autoPopulate = function(next) {next(); };

const schemaFunc = (params) => {
	const schema = Schema({
      //_id: Schema.Types.ObjectId,
      substancias: [{ type: Schema.Types.ObjectId, ref: 'CCQSubstancia' }]
	}, {timestamps: true});
	schema.
		pre('findOne', autoPopulate).
		pre('find', autoPopulate);

	return schema;
}
const key = 'CCQIncompatibilidadeSubstancias';
const depedencies = ['CCQSubstancia'];

module.exports = {
	key,
	schemaFunc,
	depedencies
}
