const moment = require('moment')

const cleanLabel = (result) => {
  result = result.trim()
  if(result.startsWith('-')) {
    result = result.substring(1).trim()
  }
  if(result.endsWith('-')) {
    result = result.substring(0, result.length-1).trim()
  }
  if(result.startsWith('/')) {
    result = result.substring(1).trim()
  }
  if(result.endsWith('/')) {
    result = result.substring(0, result.length-1).trim()
  }
  while(result.includes('  ')) {
    result = result.replace('  ', ' ');
  }
  return result 
}

const limitStr = (val, limit, points = false) => {
 val && val > limit
  ? val.substr(0, limit - 3) + (points ? '...' : '')
  : val
}

// entrega
const generateTituloEntrega = (obj) => {
  if(!obj) {
    return ''
  }
  const { parceiroEntrega, transporte, produto } = obj
  let result = [
    (
      produto && produto.titulo
        ? produto.titulo
        : null
    ),
    (
      parceiroEntrega && parceiroEntrega.parceiro && parceiroEntrega.parceiro.titulo
        ? parceiroEntrega.parceiro.titulo
        : null
      ),
    (
      obj.data ? moment(obj.data).format('DD/MM/YYYY') : null
    ),
    (
      transporte && transporte.veiculo
        ? transporte.veiculo.placa
        : null
    )
  ].filter((val) => Boolean(val)).join(' ')
  return cleanLabel(result)
}

const generateDescEntrega = (obj) => {
  if(!obj) {
    return ''
  }
  const {
    rota,
    parceiroEntrega,
    transporte,
    funcionarios,
    compromisso
  } = obj;

  // pontoRota
  const pontoRotaDesc = (ponto, index) => {
    const parentSpace = '    '
    const space = '      '
    const comPontoAnterior = Boolean(ponto.distanciaDesc || ponto.distancia)
    return cleanLabel(`${
      parentSpace + 'Parada Rota '+ ponto.ordem + ':\n'
    }${
      ponto.localizacao &&  ponto.localizacao.titulo
        ? space + 'Localização: ' + ponto.localizacao.titulo + '\n'
        : ''
    }${
      comPontoAnterior 
        ? space + 'Trajeto da parada anterior até a atual:\n'
        : ''
    }${
      comPontoAnterior
        ? space + 'Distância: ' + ponto.distanciaDesc || ponto.distancia + '\n'
        : ''
    }${
      comPontoAnterior && (ponto.duracaoDesc || ponto.duracao)
        ? space + 'Duração: ' + ponto.duracaoDesc || ponto.duracao + '\n'
        : ''
    }${
      comPontoAnterior && (ponto.duracaoComTrafegoDesc || ponto.duracaoComTrafego)
        ? space + 'Duração com Tráfego: ' + ponto.duracaoComTrafegoDesc || ponto.duracaoComTrafego + '\n'
        : ''
    }${
      ponto.anterior
        ? space + 'Anterior: ' + ponto.anterior.map( (ponto) => ponto.titulo).join(', ') + '\n'
        : ''
    }${
      ponto.status
        ? space + 'Status: ' + ponto.status + '\n'
        : ''
    }${
        ponto._id || ponto._temp
        ? space + 'ID: ' + (ponto._id || ponto._temp) + '\n'
        : ''
    }
    `)
  }

  // data
  const dataDesc = obj.data? 'Data: '+moment(obj.data).format('DD/MM/YYYY') + '\n' : ''

  // rotaLimite
  const rotaLimiteSpace = '      '
  const rotaLimiteDesc = rota && rota.limite
    ? `${
        rota.limite.peso
          ? rotaLimiteSpace + 'Peso: ' + rota.limite.peso
          : ''
      }\n${
        rota.limite.volume
          ? rotaLimiteSpace + 'Volume: : ' + rota.limite.volume
          : ''
      }\n${
        rota.limite.tara
          ? rotaLimiteSpace + 'Peso de veículo: ' + rota.limite.tara
          : ''
      }\n`
    : ''

  // rota
  const rotaSpace = '    '
  const rotaPontoSpace = '      '
  const rotaComLimite = Boolean(rota.limite)
  const rotaDesc = rota
    ? `${
      'Rota:\n' 
    }${
        rota.algoritimo
          ? rotaSpace + 'Algoritimo: ' + rota.algoritimo + '\n'
          : ''
      }${
        rota.distancia
          ? rotaSpace + 'Distância: ' + rota.distancia + '\n'
          :  ''
      }${
        rota.duracao
          ? rotaSpace + 'Duração: ' + rota.duracao + '\n'
          : ''
      }${
        rota.duracaoComTrafego
          ? rotaSpace + 'Duração com Tráfego: ' + rota.duracaoComTrafego + '\n'
          : ''
      }${
        rota.calculo
            ? rotaSpace + 'Cálculo da rota realizado\n'
            : rotaSpace + 'Cálculo da rota não realizado\n'
      }${
        rota.pontos ? rotaSpace + 'Pontos: \n'
          + rotaPontoSpace
          +  '----\n'
          + (rota.pontos || [])
          .map(pontoRotaDesc)
          .join(rotaPontoSpace + '----\n') : ''
      }${
        rotaComLimite
          ? rotaSpace + 'Limite:' + '\n'
          : ''
      }${
        rotaComLimite && rota.tipo
          ? rotaLimiteSpace + 'Tipo: ' + rota.tipo + '\n'
          : ''
      }${
        rotaComLimite
          ? rotaLimiteDesc + '\n'
          : ''
      }`
    : ''
  
  const parceiroEntregaSpace = '    '
  const parceiroEntregaDesc = parceiroEntrega
    ? `${
        'Parceiro comercial envolvido na entrega:\n'
      }${
        parceiroEntrega && parceiroEntrega.parceiro && parceiroEntrega.parceiro.titulo
          ? parceiroEntregaSpace + 'Parceiro: ' + parceiroEntrega.parceiro.titulo + '\n'
          : ''
      }${
        parceiroEntrega && parceiroEntrega.contato && parceiroEntrega.contato.titulo
          ? parceiroEntregaSpace + 'Contato: ' + parceiroEntrega.contato.titulo + '\n'
          : ''
      }`
    : ''

  const transporteSpace = '    '
  const transporteDesc = transporte
    ? `${
        'Transporte:\n'
      }${
        transporte.veiculo && transporte.veiculo.titulo
          ? transporteSpace + 'Veículo: ' + transporte.veiculo.titulo + '\n'
          : ''
      }${
        transporte.carreta && transporte.carreta.titulo
          ? transporteSpace + 'Carreta: ' + transporte.carreta.titulo + '\n'
          : ''
      }`
    : ''
  
  const funcionariosSpace = '    '
  const funcionariosDesc = funcionarios
    ? `${
        'Funcionários:\n'
      }${
        funcionarios.map(
          (func) => `${funcionariosSpace}${func.tipo}: ${func.funcionario
            ? func.funcionario.nome
            : ''}`).join(', ') + '\n'
      }` : ''
  const compromissoDesc = compromisso && compromisso.titulo
    ? `${
      'Evento no calendário:\n'
    }${
      compromisso.titulo
        ? 'Compromisso: ' + compromisso.titulo + '\n'
        : ''}
  ` : ''
    
  const result = `${
      'Entrega:\n'
    }${
      obj.status
        ? 'Status: ' + obj.status + '\n'
        : ''
    }${
      obj.produto
        ? 'Produto: ' + (obj.produto.titulo || obj.produto) + '\n'
        : ''
    }${
      dataDesc
    }${
      rotaDesc
    }${
      parceiroEntregaDesc
    }${
      transporteDesc
    }${
      funcionariosDesc
    }${
      compromissoDesc
    }`
  return cleanLabel(result)
}

const entrega = {
  "_id": "5fae0772f9c82d54b8b72129",
  "titulo": "Entrega teste",
  "tipoFormacaoTitulo": "MANUAL",
  "desc": "",
  "tipoFormacaoDesc": "AUTOMATIZADO",
  "status": "PENDENTE",
  "data": "2012-11-14T02:00:00.000Z",
  "produto": {
    "_id": "5f870eb908cdd24ddcf7a6f8",
    "titulo": "Ácido clorídrico 34%",
    "desc": "Ácido clorídrico ou ácido muriático é um sistema químico inorgânico incolor com a fórmula H₂O:HCl. O ácido clorídrico tem um cheiro característico e pungente",
    "tipo": "Produto Quimico",
    "marca": "Própria",
    "densidade": 1.169,
    "createdAt": "2020-10-14T14:44:09.629Z",
    "updatedAt": "2020-10-14T14:44:09.629Z",
    "__v": 0
  },
  "rota": {
    "_id": "5fae0772f9c82d54b8b7212a",
    "pontos": [
      {
        "anterior": [],
        "_id": "5fae0772f9c82d54b8b7212c",
        "_temp": null,
        "createdAt": "2020-11-13T04:11:30.443Z",
        "updatedAt": "2020-11-13T04:11:30.443Z",
        "titulo": "Carregamento",
        "tipoFormacaoTitulo": "MANUAL",
        "desc": "",
		    "tipoFormacaoDesc": "AUTOMATIZADO",
        "ordem": 1,
        "distancia": null,
        "duracao": null,
        "duracaoComTrafego": null,
        "distanciaDesc": null,
        "duracaoDesc": null,
        "duracaoComTrafegoDesc": null,
        "localizacao": {
          "_id": "5fa364cde85d1026940e847b",
          "createdAt": "2020-11-05T02:34:53.267Z",
          "updatedAt": "2020-11-05T02:34:53.267Z",
          "titulo": "D B R Química Com e Transportes de Produtos Químicos - Pedro machado e ilha, Contenda - PR, Brasil",
          "endereco": "Rodovia do Xisto BR 476 km 170 n°2979 - Pedro machado e ilha, Contenda - PR, 83730-000, Brazil",
          "lat": -25.674374,
          "long": -49.514709,
          "pais": "Brazil",
          "estado": "Paraná",
          "cidade": "Contenda",
          "area": "Pedro machado e ilha",
          "codigoPostal": "83730-000",
          "numero": "",
          "complemento": ""
        },
        "limite": {
          "_id": "5fae0772f9c82d54b8b7212e",
          "peso": 12000,
          "volume": 14028,
          "tara": 0,
          "createdAt": "2020-11-13T04:11:30.443Z",
          "updatedAt": "2020-11-13T04:11:30.443Z"
        },
        "status": "PENDENTE",
        "usarFilial": true,
        "filial": {
          "_id": "5fa364cde85d1026940e847a",
          "_temp": "",
          "titulo": "Fabrica",
          "desc": "",
          "localizacao": {
            "_id": "5fa364cde85d1026940e847b",
            "titulo": "D B R Química Com e Transportes de Produtos Químicos - Pedro machado e ilha, Contenda - PR, Brasil",
            "endereco": "Rodovia do Xisto BR 476 km 170 n°2979 - Pedro machado e ilha, Contenda - PR, 83730-000, Brazil",
            "lat": -25.674374,
            "long": -49.514709,
            "pais": "Brazil",
            "estado": "Paraná",
            "cidade": "Contenda",
            "area": "Pedro machado e ilha",
            "codigoPostal": "83730-000",
            "numero": "",
            "complemento": "",
            "createdAt": "2020-11-05T02:34:53.267Z",
            "updatedAt": "2020-11-05T02:34:53.267Z"
          },
          "createdAt": "2020-11-05T02:34:53.267Z",
          "updatedAt": "2020-11-05T02:34:53.267Z",
          "__v": 0
        }
      },
      {
        "anterior": [],
        "_id": "5fae0772f9c82d54b8b72132",
        "_temp": null,
        "createdAt": "2020-11-13T04:11:30.443Z",
        "updatedAt": "2020-11-13T04:11:30.443Z",
        "titulo": "#3 Supermercado Boa Vista  - Seg Loja - Descarga: 4000 - Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
        "tipoFormacaoTitulo": "AUTOMATIZADO",
        "desc": "",
		    "tipoFormacaoDesc": "AUTOMATIZADO",
        "ordem": 2,
        "distancia": 12535,
        "duracao": 1400,
        "duracaoComTrafego": 1278,
        "distanciaDesc": "12,5 km",
        "duracaoDesc": "23 minutos",
        "duracaoComTrafegoDesc": "21 minutos",
        "localizacao": {
          "_id": "5f9ee17229d6477e18f818c7",
          "createdAt": "2020-11-01T19:00:28.326Z",
          "updatedAt": "2020-11-01T19:00:28.326Z",
          "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
          "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
          "lat": -25.5008981,
          "long": -49.22838369999999,
          "pais": "Brazil",
          "estado": "Paraná",
          "cidade": "Curitiba",
          "area": "Boqueirão",
          "codigoPostal": "81730-070",
          "numero": "1894",
          "complemento": ""
        },
        "limite": {
          "_id": "5fae0772f9c82d54b8b72134",
          "peso": -4000,
          "volume": -4676,
          "tara": 0,
          "createdAt": "2020-11-13T04:11:30.443Z",
          "updatedAt": "2020-11-13T04:11:30.443Z"
        },
        "status": "PENDENTE",
        "usarFilial": true,
        "parceiro": {
          "filiais": [
            {
              "_id": "5f9e170714cc488580744f28",
              "_temp": null,
              "titulo": "Matriz",
              "desc": "Principal",
              "localizacao": {
                "_id": "5f9e170714cc488580744f29",
                "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                "lat": -25.4105754,
                "long": -49.2571317,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Juvevê",
                "codigoPostal": "80540-180",
                "numero": "143",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.325Z",
                "updatedAt": "2020-11-01T19:00:28.325Z"
              },
              "createdAt": "2020-11-01T02:01:43.891Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            },
            {
              "_id": "5f9ee17229d6477e18f818c4",
              "_temp": null,
              "titulo": "Loja",
              "desc": "Loja principal",
              "localizacao": {
                "_id": "5f9ee17229d6477e18f818c5",
                "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, Brasil",
                "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "lat": -25.5008981,
                "long": -49.22838369999999,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Boqueirão",
                "codigoPostal": "81730-070",
                "numero": "1894",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.326Z",
                "updatedAt": "2020-11-01T19:00:28.326Z"
              },
              "createdAt": "2020-11-01T16:25:22.804Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            },
            {
              "_id": "5f9ee17229d6477e18f818c6",
              "_temp": null,
              "titulo": "Seg Loja",
              "desc": "Seg Loja Desc",
              "localizacao": {
                "_id": "5f9ee17229d6477e18f818c7",
                "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "lat": -25.5008981,
                "long": -49.22838369999999,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Boqueirão",
                "codigoPostal": "81730-070",
                "numero": "1894",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.326Z",
                "updatedAt": "2020-11-01T19:00:28.326Z"
              },
              "createdAt": "2020-11-01T16:25:22.804Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            }
          ],
          "_id": "5f9e170714cc488580744f2c",
          "titulo": "Supermercado Boa Vista ",
          "desc": "Supermercado Boa Vista, venda deALT",
          "identificacao": "84.682.149/0001-98",
          "tipoPessoa": "JURIDICA",
          "contatos": [
            {
              "_id": "5f9e170714cc488580744f32",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9e170714cc488580744f28",
                "_temp": null,
                "titulo": "Matriz",
                "desc": "Principal",
                "localizacao": {
                  "_id": "5f9e170714cc488580744f29",
                  "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                  "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                  "lat": -25.4105754,
                  "long": -49.2571317,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Juvevê",
                  "codigoPostal": "80540-180",
                  "numero": "143",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.325Z",
                  "updatedAt": "2020-11-01T19:00:28.325Z"
                },
                "createdAt": "2020-11-01T02:01:43.891Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9e170714cc488580744f33",
                "infos": [
                  {
                    "_id": "5f9e170714cc488580744f34",
                    "tipo": {
                      "_id": "5f94caf4ae527551705bd904",
                      "titulo": "E-MAIL",
                      "desc": "Endereço eletronico",
                      "mask": "[a-z|0-9]@[a-z|0-9].com",
                      "createdAt": "2020-10-25T00:46:44.932Z",
                      "updatedAt": "2020-10-25T00:46:44.932Z",
                      "__v": 0
                    },
                    "valor": "paulo_almeida@email.com",
                    "createdAt": "2020-11-01T02:01:43.944Z",
                    "updatedAt": "2020-11-01T02:01:43.944Z"
                  },
                  {
                    "_id": "5f9e170714cc488580744f35",
                    "tipo": {
                      "_id": "5f94cba7ae527551705bd906",
                      "titulo": "PAGINA WEB",
                      "desc": "website",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:43.148Z",
                      "updatedAt": "2020-10-25T00:49:43.148Z",
                      "__v": 0
                    },
                    "valor": "pauloalmeida.com.br",
                    "createdAt": "2020-11-01T02:01:43.945Z",
                    "updatedAt": "2020-11-01T02:01:43.945Z"
                  }
                ],
                "createdAt": "2020-11-01T02:01:43.945Z",
                "updatedAt": "2020-11-01T02:01:43.945Z",
                "titulo": "Paulo Almeida",
                "desc": "Gerente"
              }
            },
            {
              "_id": "5f9ee17229d6477e18f818ce",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9e170714cc488580744f28",
                "_temp": null,
                "titulo": "Matriz",
                "desc": "Principal",
                "localizacao": {
                  "_id": "5f9e170714cc488580744f29",
                  "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                  "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                  "lat": -25.4105754,
                  "long": -49.2571317,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Juvevê",
                  "codigoPostal": "80540-180",
                  "numero": "143",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.325Z",
                  "updatedAt": "2020-11-01T19:00:28.325Z"
                },
                "createdAt": "2020-11-01T02:01:43.891Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9ee17229d6477e18f818cf",
                "infos": [
                  {
                    "_id": "5f9ee17229d6477e18f818d0",
                    "tipo": {
                      "_id": "5f94cba7ae527551705bd906",
                      "titulo": "PAGINA WEB",
                      "desc": "website",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:43.148Z",
                      "updatedAt": "2020-10-25T00:49:43.148Z",
                      "__v": 0
                    },
                    "valor": "paginaweb.com/lucassilva"
                  }
                ],
                "titulo": "Lucas Silva",
                "desc": "Vendedor"
              }
            },
            {
              "_id": "5f9ee17229d6477e18f818d1",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9ee17229d6477e18f818c6",
                "_temp": null,
                "titulo": "Seg Loja",
                "desc": "Seg Loja Desc",
                "localizacao": {
                  "_id": "5f9ee17229d6477e18f818c7",
                  "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                  "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                  "lat": -25.5008981,
                  "long": -49.22838369999999,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Boqueirão",
                  "codigoPostal": "81730-070",
                  "numero": "1894",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.326Z",
                  "updatedAt": "2020-11-01T19:00:28.326Z"
                },
                "createdAt": "2020-11-01T16:25:22.804Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9ee17229d6477e18f818d2",
                "infos": [
                  {
                    "_id": "5f9ee17229d6477e18f818d3",
                    "tipo": {
                      "_id": "5f94cb89ae527551705bd905",
                      "titulo": "CELULAR",
                      "desc": "numero de celular",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:13.912Z",
                      "updatedAt": "2020-10-25T00:49:13.912Z",
                      "__v": 0
                    },
                    "valor": "41998876755"
                  }
                ],
                "titulo": "Almeida",
                "desc": "desc Almeida"
              }
            }
          ],
          "createdAt": "2020-11-01T02:01:43.945Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "__v": 0
        },
        "filial": {
          "_id": "5f9ee17229d6477e18f818c6",
          "_temp": null,
          "titulo": "Seg Loja",
          "desc": "Seg Loja Desc",
          "localizacao": {
            "_id": "5f9ee17229d6477e18f818c7",
            "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
            "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
            "lat": -25.5008981,
            "long": -49.22838369999999,
            "pais": "Brazil",
            "estado": "Paraná",
            "cidade": "Curitiba",
            "area": "Boqueirão",
            "codigoPostal": "81730-070",
            "numero": "1894",
            "complemento": "",
            "createdAt": "2020-11-01T19:00:28.326Z",
            "updatedAt": "2020-11-01T19:00:28.326Z"
          },
          "createdAt": "2020-11-01T16:25:22.804Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "__v": 0
        }
      },
      {
        "anterior": [],
        "_id": "5fae0772f9c82d54b8b7212f",
        "_temp": "_5fb861e93cb8d3000063422d",
        "createdAt": "2020-11-13T04:11:30.443Z",
        "updatedAt": "2020-11-13T04:11:30.443Z",
        "titulo": "#2 Supermercado Boa Vista  - Matriz - Descarga: 2000 - Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
        "tipoFormacaoTitulo": "AUTOMATIZADO",
        "desc": "",
        "tipoFormacaoDesc": "AUTOMATIZADO",
        "ordem": 3,
        "distancia": 46695,
        "duracao": 3529,
        "duracaoComTrafego": 3217,
        "distanciaDesc": "46,7 km",
        "duracaoDesc": "59 minutos",
        "duracaoComTrafegoDesc": "54 minutos",
        "localizacao": {
          "_id": "5f9e170714cc488580744f29",
          "createdAt": "2020-11-01T19:00:28.325Z",
          "updatedAt": "2020-11-01T19:00:28.325Z",
          "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
          "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
          "lat": -25.4105754,
          "long": -49.2571317,
          "pais": "Brazil",
          "estado": "Paraná",
          "cidade": "Curitiba",
          "area": "Juvevê",
          "codigoPostal": "80540-180",
          "numero": "143",
          "complemento": ""
        },
        "limite": {
          "_id": "5fae0772f9c82d54b8b72131",
          "peso": -2000,
          "volume": -2338,
          "tara": 0,
          "createdAt": "2020-11-13T04:11:30.443Z",
          "updatedAt": "2020-11-13T04:11:30.443Z"
        },
        "status": "PENDENTE",
        "usarFilial": true,
        "parceiro": {
          "filiais": [
            {
              "_id": "5f9e170714cc488580744f28",
              "_temp": null,
              "titulo": "Matriz",
              "desc": "Principal",
              "localizacao": {
                "_id": "5f9e170714cc488580744f29",
                "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                "lat": -25.4105754,
                "long": -49.2571317,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Juvevê",
                "codigoPostal": "80540-180",
                "numero": "143",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.325Z",
                "updatedAt": "2020-11-01T19:00:28.325Z"
              },
              "createdAt": "2020-11-01T02:01:43.891Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            },
            {
              "_id": "5f9ee17229d6477e18f818c4",
              "_temp": null,
              "titulo": "Loja",
              "desc": "Loja principal",
              "localizacao": {
                "_id": "5f9ee17229d6477e18f818c5",
                "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, Brasil",
                "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "lat": -25.5008981,
                "long": -49.22838369999999,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Boqueirão",
                "codigoPostal": "81730-070",
                "numero": "1894",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.326Z",
                "updatedAt": "2020-11-01T19:00:28.326Z"
              },
              "createdAt": "2020-11-01T16:25:22.804Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            },
            {
              "_id": "5f9ee17229d6477e18f818c6",
              "_temp": null,
              "titulo": "Seg Loja",
              "desc": "Seg Loja Desc",
              "localizacao": {
                "_id": "5f9ee17229d6477e18f818c7",
                "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "lat": -25.5008981,
                "long": -49.22838369999999,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Boqueirão",
                "codigoPostal": "81730-070",
                "numero": "1894",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.326Z",
                "updatedAt": "2020-11-01T19:00:28.326Z"
              },
              "createdAt": "2020-11-01T16:25:22.804Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            }
          ],
          "_id": "5f9e170714cc488580744f2c",
          "titulo": "Supermercado Boa Vista ",
          "desc": "Supermercado Boa Vista, venda deALT",
          "identificacao": "84.682.149/0001-98",
          "tipoPessoa": "JURIDICA",
          "contatos": [
            {
              "_id": "5f9e170714cc488580744f32",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9e170714cc488580744f28",
                "_temp": null,
                "titulo": "Matriz",
                "desc": "Principal",
                "localizacao": {
                  "_id": "5f9e170714cc488580744f29",
                  "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                  "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                  "lat": -25.4105754,
                  "long": -49.2571317,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Juvevê",
                  "codigoPostal": "80540-180",
                  "numero": "143",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.325Z",
                  "updatedAt": "2020-11-01T19:00:28.325Z"
                },
                "createdAt": "2020-11-01T02:01:43.891Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9e170714cc488580744f33",
                "infos": [
                  {
                    "_id": "5f9e170714cc488580744f34",
                    "tipo": {
                      "_id": "5f94caf4ae527551705bd904",
                      "titulo": "E-MAIL",
                      "desc": "Endereço eletronico",
                      "mask": "[a-z|0-9]@[a-z|0-9].com",
                      "createdAt": "2020-10-25T00:46:44.932Z",
                      "updatedAt": "2020-10-25T00:46:44.932Z",
                      "__v": 0
                    },
                    "valor": "paulo_almeida@email.com",
                    "createdAt": "2020-11-01T02:01:43.944Z",
                    "updatedAt": "2020-11-01T02:01:43.944Z"
                  },
                  {
                    "_id": "5f9e170714cc488580744f35",
                    "tipo": {
                      "_id": "5f94cba7ae527551705bd906",
                      "titulo": "PAGINA WEB",
                      "desc": "website",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:43.148Z",
                      "updatedAt": "2020-10-25T00:49:43.148Z",
                      "__v": 0
                    },
                    "valor": "pauloalmeida.com.br",
                    "createdAt": "2020-11-01T02:01:43.945Z",
                    "updatedAt": "2020-11-01T02:01:43.945Z"
                  }
                ],
                "createdAt": "2020-11-01T02:01:43.945Z",
                "updatedAt": "2020-11-01T02:01:43.945Z",
                "titulo": "Paulo Almeida",
                "desc": "Gerente"
              }
            },
            {
              "_id": "5f9ee17229d6477e18f818ce",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9e170714cc488580744f28",
                "_temp": null,
                "titulo": "Matriz",
                "desc": "Principal",
                "localizacao": {
                  "_id": "5f9e170714cc488580744f29",
                  "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                  "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                  "lat": -25.4105754,
                  "long": -49.2571317,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Juvevê",
                  "codigoPostal": "80540-180",
                  "numero": "143",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.325Z",
                  "updatedAt": "2020-11-01T19:00:28.325Z"
                },
                "createdAt": "2020-11-01T02:01:43.891Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9ee17229d6477e18f818cf",
                "infos": [
                  {
                    "_id": "5f9ee17229d6477e18f818d0",
                    "tipo": {
                      "_id": "5f94cba7ae527551705bd906",
                      "titulo": "PAGINA WEB",
                      "desc": "website",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:43.148Z",
                      "updatedAt": "2020-10-25T00:49:43.148Z",
                      "__v": 0
                    },
                    "valor": "paginaweb.com/lucassilva"
                  }
                ],
                "titulo": "Lucas Silva",
                "desc": "Vendedor"
              }
            },
            {
              "_id": "5f9ee17229d6477e18f818d1",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9ee17229d6477e18f818c6",
                "_temp": null,
                "titulo": "Seg Loja",
                "desc": "Seg Loja Desc",
                "localizacao": {
                  "_id": "5f9ee17229d6477e18f818c7",
                  "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                  "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                  "lat": -25.5008981,
                  "long": -49.22838369999999,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Boqueirão",
                  "codigoPostal": "81730-070",
                  "numero": "1894",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.326Z",
                  "updatedAt": "2020-11-01T19:00:28.326Z"
                },
                "createdAt": "2020-11-01T16:25:22.804Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9ee17229d6477e18f818d2",
                "infos": [
                  {
                    "_id": "5f9ee17229d6477e18f818d3",
                    "tipo": {
                      "_id": "5f94cb89ae527551705bd905",
                      "titulo": "CELULAR",
                      "desc": "numero de celular",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:13.912Z",
                      "updatedAt": "2020-10-25T00:49:13.912Z",
                      "__v": 0
                    },
                    "valor": "41998876755"
                  }
                ],
                "titulo": "Almeida",
                "desc": "desc Almeida"
              }
            }
          ],
          "createdAt": "2020-11-01T02:01:43.945Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "__v": 0
        },
        "filial": {
          "_id": "5f9e170714cc488580744f28",
          "_temp": null,
          "titulo": "Matriz",
          "desc": "Principal",
          "localizacao": {
            "_id": "5f9e170714cc488580744f29",
            "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
            "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
            "lat": -25.4105754,
            "long": -49.2571317,
            "pais": "Brazil",
            "estado": "Paraná",
            "cidade": "Curitiba",
            "area": "Juvevê",
            "codigoPostal": "80540-180",
            "numero": "143",
            "complemento": "",
            "createdAt": "2020-11-01T19:00:28.325Z",
            "updatedAt": "2020-11-01T19:00:28.325Z"
          },
          "createdAt": "2020-11-01T02:01:43.891Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "__v": 0
        }
      },
      {
        "anterior": [],
        "_id": "5fae0772f9c82d54b8b72135",
        "_temp": null,
        "createdAt": "2020-11-13T04:11:30.443Z",
        "updatedAt": "2020-11-13T04:11:30.443Z",
        "titulo": "#2 Supermercado Boa Vista  - Matriz - Descarga: 4000 - Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
        "tipoFormacaoTitulo": "AUTOMATIZADO",
        "desc": "",
        "tipoFormacaoDesc": "AUTOMATIZADO",
        "ordem": 4,
        "distancia": 13448,
        "duracao": 1509,
        "duracaoComTrafego": 1402,
        "distanciaDesc": "13,4 km",
        "duracaoDesc": "25 minutos",
        "duracaoComTrafegoDesc": "23 minutos",
        "localizacao": {
          "_id": "5f9e170714cc488580744f29",
          "createdAt": "2020-11-01T19:00:28.325Z",
          "updatedAt": "2020-11-01T19:00:28.325Z",
          "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
          "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
          "lat": -25.4105754,
          "long": -49.2571317,
          "pais": "Brazil",
          "estado": "Paraná",
          "cidade": "Curitiba",
          "area": "Juvevê",
          "codigoPostal": "80540-180",
          "numero": "143",
          "complemento": ""
        },
        "limite": {
          "_id": "5fae0772f9c82d54b8b72137",
          "peso": -4000,
          "volume": -4676,
          "tara": 0,
          "createdAt": "2020-11-13T04:11:30.443Z",
          "updatedAt": "2020-11-13T04:11:30.443Z"
        },
        "status": "PENDENTE",
        "usarFilial": true,
        "parceiro": {
          "filiais": [
            {
              "_id": "5f9e170714cc488580744f28",
              "_temp": null,
              "titulo": "Matriz",
              "desc": "Principal",
              "localizacao": {
                "_id": "5f9e170714cc488580744f29",
                "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                "lat": -25.4105754,
                "long": -49.2571317,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Juvevê",
                "codigoPostal": "80540-180",
                "numero": "143",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.325Z",
                "updatedAt": "2020-11-01T19:00:28.325Z"
              },
              "createdAt": "2020-11-01T02:01:43.891Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            },
            {
              "_id": "5f9ee17229d6477e18f818c4",
              "_temp": null,
              "titulo": "Loja",
              "desc": "Loja principal",
              "localizacao": {
                "_id": "5f9ee17229d6477e18f818c5",
                "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, Brasil",
                "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "lat": -25.5008981,
                "long": -49.22838369999999,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Boqueirão",
                "codigoPostal": "81730-070",
                "numero": "1894",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.326Z",
                "updatedAt": "2020-11-01T19:00:28.326Z"
              },
              "createdAt": "2020-11-01T16:25:22.804Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            },
            {
              "_id": "5f9ee17229d6477e18f818c6",
              "_temp": null,
              "titulo": "Seg Loja",
              "desc": "Seg Loja Desc",
              "localizacao": {
                "_id": "5f9ee17229d6477e18f818c7",
                "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "lat": -25.5008981,
                "long": -49.22838369999999,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Boqueirão",
                "codigoPostal": "81730-070",
                "numero": "1894",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.326Z",
                "updatedAt": "2020-11-01T19:00:28.326Z"
              },
              "createdAt": "2020-11-01T16:25:22.804Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            }
          ],
          "_id": "5f9e170714cc488580744f2c",
          "titulo": "Supermercado Boa Vista ",
          "desc": "Supermercado Boa Vista, venda deALT",
          "identificacao": "84.682.149/0001-98",
          "tipoPessoa": "JURIDICA",
          "contatos": [
            {
              "_id": "5f9e170714cc488580744f32",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9e170714cc488580744f28",
                "_temp": null,
                "titulo": "Matriz",
                "desc": "Principal",
                "localizacao": {
                  "_id": "5f9e170714cc488580744f29",
                  "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                  "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                  "lat": -25.4105754,
                  "long": -49.2571317,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Juvevê",
                  "codigoPostal": "80540-180",
                  "numero": "143",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.325Z",
                  "updatedAt": "2020-11-01T19:00:28.325Z"
                },
                "createdAt": "2020-11-01T02:01:43.891Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9e170714cc488580744f33",
                "infos": [
                  {
                    "_id": "5f9e170714cc488580744f34",
                    "tipo": {
                      "_id": "5f94caf4ae527551705bd904",
                      "titulo": "E-MAIL",
                      "desc": "Endereço eletronico",
                      "mask": "[a-z|0-9]@[a-z|0-9].com",
                      "createdAt": "2020-10-25T00:46:44.932Z",
                      "updatedAt": "2020-10-25T00:46:44.932Z",
                      "__v": 0
                    },
                    "valor": "paulo_almeida@email.com",
                    "createdAt": "2020-11-01T02:01:43.944Z",
                    "updatedAt": "2020-11-01T02:01:43.944Z"
                  },
                  {
                    "_id": "5f9e170714cc488580744f35",
                    "tipo": {
                      "_id": "5f94cba7ae527551705bd906",
                      "titulo": "PAGINA WEB",
                      "desc": "website",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:43.148Z",
                      "updatedAt": "2020-10-25T00:49:43.148Z",
                      "__v": 0
                    },
                    "valor": "pauloalmeida.com.br",
                    "createdAt": "2020-11-01T02:01:43.945Z",
                    "updatedAt": "2020-11-01T02:01:43.945Z"
                  }
                ],
                "createdAt": "2020-11-01T02:01:43.945Z",
                "updatedAt": "2020-11-01T02:01:43.945Z",
                "titulo": "Paulo Almeida",
                "desc": "Gerente"
              }
            },
            {
              "_id": "5f9ee17229d6477e18f818ce",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9e170714cc488580744f28",
                "_temp": null,
                "titulo": "Matriz",
                "desc": "Principal",
                "localizacao": {
                  "_id": "5f9e170714cc488580744f29",
                  "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                  "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                  "lat": -25.4105754,
                  "long": -49.2571317,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Juvevê",
                  "codigoPostal": "80540-180",
                  "numero": "143",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.325Z",
                  "updatedAt": "2020-11-01T19:00:28.325Z"
                },
                "createdAt": "2020-11-01T02:01:43.891Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9ee17229d6477e18f818cf",
                "infos": [
                  {
                    "_id": "5f9ee17229d6477e18f818d0",
                    "tipo": {
                      "_id": "5f94cba7ae527551705bd906",
                      "titulo": "PAGINA WEB",
                      "desc": "website",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:43.148Z",
                      "updatedAt": "2020-10-25T00:49:43.148Z",
                      "__v": 0
                    },
                    "valor": "paginaweb.com/lucassilva"
                  }
                ],
                "titulo": "Lucas Silva",
                "desc": "Vendedor"
              }
            },
            {
              "_id": "5f9ee17229d6477e18f818d1",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9ee17229d6477e18f818c6",
                "_temp": null,
                "titulo": "Seg Loja",
                "desc": "Seg Loja Desc",
                "localizacao": {
                  "_id": "5f9ee17229d6477e18f818c7",
                  "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                  "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                  "lat": -25.5008981,
                  "long": -49.22838369999999,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Boqueirão",
                  "codigoPostal": "81730-070",
                  "numero": "1894",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.326Z",
                  "updatedAt": "2020-11-01T19:00:28.326Z"
                },
                "createdAt": "2020-11-01T16:25:22.804Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9ee17229d6477e18f818d2",
                "infos": [
                  {
                    "_id": "5f9ee17229d6477e18f818d3",
                    "tipo": {
                      "_id": "5f94cb89ae527551705bd905",
                      "titulo": "CELULAR",
                      "desc": "numero de celular",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:13.912Z",
                      "updatedAt": "2020-10-25T00:49:13.912Z",
                      "__v": 0
                    },
                    "valor": "41998876755"
                  }
                ],
                "titulo": "Almeida",
                "desc": "desc Almeida"
              }
            }
          ],
          "createdAt": "2020-11-01T02:01:43.945Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "__v": 0
        },
        "filial": {
          "_id": "5f9e170714cc488580744f28",
          "_temp": null,
          "titulo": "Matriz",
          "desc": "Principal",
          "localizacao": {
            "_id": "5f9e170714cc488580744f29",
            "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
            "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
            "lat": -25.4105754,
            "long": -49.2571317,
            "pais": "Brazil",
            "estado": "Paraná",
            "cidade": "Curitiba",
            "area": "Juvevê",
            "codigoPostal": "80540-180",
            "numero": "143",
            "complemento": "",
            "createdAt": "2020-11-01T19:00:28.325Z",
            "updatedAt": "2020-11-01T19:00:28.325Z"
          },
          "createdAt": "2020-11-01T02:01:43.891Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "__v": 0
        }
      },
      {
        "anterior": [],
        "_id": "5fb862b1e2defb9064e533c8",
        "_temp": "_5fb8621f3cb8d3000063422e",
        "titulo": "Barracão parceiro",
        "tipoFormacaoTitulo": "MANUAL",
        "desc": "",
        "tipoFormacaoDesc": "AUTOMATIZADO",
        "ordem": 5,
        "distancia": 9767,
        "duracao": 1121,
        "duracaoComTrafego": 979,
        "distanciaDesc": "9,8 km",
        "duracaoDesc": "19 minutos",
        "duracaoComTrafegoDesc": "16 minutos",
        "localizacao": {
          "_id": "5fb862b1e2defb9064e533c9",
          "titulo": "Rua Francisco Wanke, 123 - Osasco, Colombo - PR, Brasil",
          "endereco": "R. Francisco Wanke, 123 - Osasco, Colombo - PR, 83403-220, Brazil",
          "lat": -25.3464276,
          "long": -49.2151163,
          "pais": "Brazil",
          "estado": "Paraná",
          "cidade": "Colombo",
          "area": "Osasco",
          "codigoPostal": "83403-220",
          "numero": "123",
          "complemento": ""
        },
        "limite": {
          "_id": "5fb862b1e2defb9064e533ca",
          "peso": -2000,
          "volume": -2338,
          "tara": 0
        },
        "status": "PENDENTE",
        "usarFilial": false
      }
    ],
    "createdAt": "2020-11-21T00:43:29.920Z",
    "updatedAt": "2020-11-21T00:43:29.920Z",
    "algoritimo": "nearestNeighbor",
    "calculo": true,
    "distancia": 82445,
    "duracao": 7559,
    "duracaoComTrafego": 6876,
    "limite": {
      "_id": "5fae0772f9c82d54b8b7212b",
      "createdAt": "2020-11-13T04:11:30.443Z",
      "updatedAt": "2020-11-13T04:11:30.443Z",
      "peso": 23300,
      "volume": 40000,
      "tara": 16700
    },
    "tipoLimite": "TRANSPORTE"
  },
  "parceiroEntrega": {
    "_id": "5fae0772f9c82d54b8b72138",
    "createdAt": "2020-11-21T00:43:29.920Z",
    "updatedAt": "2020-11-21T00:43:29.920Z",
    "parceiro": {
      "filiais": [
        {
          "_id": "5f9e170714cc488580744f28",
          "_temp": null,
          "titulo": "Matriz",
          "desc": "Principal",
          "localizacao": {
            "_id": "5f9e170714cc488580744f29",
            "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
            "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
            "lat": -25.4105754,
            "long": -49.2571317,
            "pais": "Brazil",
            "estado": "Paraná",
            "cidade": "Curitiba",
            "area": "Juvevê",
            "codigoPostal": "80540-180",
            "numero": "143",
            "complemento": "",
            "createdAt": "2020-11-01T19:00:28.325Z",
            "updatedAt": "2020-11-01T19:00:28.325Z"
          },
          "createdAt": "2020-11-01T02:01:43.891Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "__v": 0
        },
        {
          "_id": "5f9ee17229d6477e18f818c4",
          "_temp": null,
          "titulo": "Loja",
          "desc": "Loja principal",
          "localizacao": {
            "_id": "5f9ee17229d6477e18f818c5",
            "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, Brasil",
            "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
            "lat": -25.5008981,
            "long": -49.22838369999999,
            "pais": "Brazil",
            "estado": "Paraná",
            "cidade": "Curitiba",
            "area": "Boqueirão",
            "codigoPostal": "81730-070",
            "numero": "1894",
            "complemento": "",
            "createdAt": "2020-11-01T19:00:28.326Z",
            "updatedAt": "2020-11-01T19:00:28.326Z"
          },
          "createdAt": "2020-11-01T16:25:22.804Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "__v": 0
        },
        {
          "_id": "5f9ee17229d6477e18f818c6",
          "_temp": null,
          "titulo": "Seg Loja",
          "desc": "Seg Loja Desc",
          "localizacao": {
            "_id": "5f9ee17229d6477e18f818c7",
            "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
            "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
            "lat": -25.5008981,
            "long": -49.22838369999999,
            "pais": "Brazil",
            "estado": "Paraná",
            "cidade": "Curitiba",
            "area": "Boqueirão",
            "codigoPostal": "81730-070",
            "numero": "1894",
            "complemento": "",
            "createdAt": "2020-11-01T19:00:28.326Z",
            "updatedAt": "2020-11-01T19:00:28.326Z"
          },
          "createdAt": "2020-11-01T16:25:22.804Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "__v": 0
        }
      ],
      "_id": "5f9e170714cc488580744f2c",
      "titulo": "Supermercado Boa Vista ",
      "desc": "Supermercado Boa Vista, venda deALT",
      "identificacao": "84.682.149/0001-98",
      "tipoPessoa": "JURIDICA",
      "contatos": [
        {
          "_id": "5f9e170714cc488580744f32",
          "createdAt": "2020-11-01T19:00:28.341Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "filial": {
            "_id": "5f9e170714cc488580744f28",
            "_temp": null,
            "titulo": "Matriz",
            "desc": "Principal",
            "localizacao": {
              "_id": "5f9e170714cc488580744f29",
              "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
              "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
              "lat": -25.4105754,
              "long": -49.2571317,
              "pais": "Brazil",
              "estado": "Paraná",
              "cidade": "Curitiba",
              "area": "Juvevê",
              "codigoPostal": "80540-180",
              "numero": "143",
              "complemento": "",
              "createdAt": "2020-11-01T19:00:28.325Z",
              "updatedAt": "2020-11-01T19:00:28.325Z"
            },
            "createdAt": "2020-11-01T02:01:43.891Z",
            "updatedAt": "2020-11-01T19:00:28.341Z",
            "__v": 0
          },
          "contato": {
            "_id": "5f9e170714cc488580744f33",
            "infos": [
              {
                "_id": "5f9e170714cc488580744f34",
                "tipo": {
                  "_id": "5f94caf4ae527551705bd904",
                  "titulo": "E-MAIL",
                  "desc": "Endereço eletronico",
                  "mask": "[a-z|0-9]@[a-z|0-9].com",
                  "createdAt": "2020-10-25T00:46:44.932Z",
                  "updatedAt": "2020-10-25T00:46:44.932Z",
                  "__v": 0
                },
                "valor": "paulo_almeida@email.com",
                "createdAt": "2020-11-01T02:01:43.944Z",
                "updatedAt": "2020-11-01T02:01:43.944Z"
              },
              {
                "_id": "5f9e170714cc488580744f35",
                "tipo": {
                  "_id": "5f94cba7ae527551705bd906",
                  "titulo": "PAGINA WEB",
                  "desc": "website",
                  "mask": "",
                  "createdAt": "2020-10-25T00:49:43.148Z",
                  "updatedAt": "2020-10-25T00:49:43.148Z",
                  "__v": 0
                },
                "valor": "pauloalmeida.com.br",
                "createdAt": "2020-11-01T02:01:43.945Z",
                "updatedAt": "2020-11-01T02:01:43.945Z"
              }
            ],
            "createdAt": "2020-11-01T02:01:43.945Z",
            "updatedAt": "2020-11-01T02:01:43.945Z",
            "titulo": "Paulo Almeida",
            "desc": "Gerente"
          }
        },
        {
          "_id": "5f9ee17229d6477e18f818ce",
          "createdAt": "2020-11-01T19:00:28.341Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "filial": {
            "_id": "5f9e170714cc488580744f28",
            "_temp": null,
            "titulo": "Matriz",
            "desc": "Principal",
            "localizacao": {
              "_id": "5f9e170714cc488580744f29",
              "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
              "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
              "lat": -25.4105754,
              "long": -49.2571317,
              "pais": "Brazil",
              "estado": "Paraná",
              "cidade": "Curitiba",
              "area": "Juvevê",
              "codigoPostal": "80540-180",
              "numero": "143",
              "complemento": "",
              "createdAt": "2020-11-01T19:00:28.325Z",
              "updatedAt": "2020-11-01T19:00:28.325Z"
            },
            "createdAt": "2020-11-01T02:01:43.891Z",
            "updatedAt": "2020-11-01T19:00:28.341Z",
            "__v": 0
          },
          "contato": {
            "_id": "5f9ee17229d6477e18f818cf",
            "infos": [
              {
                "_id": "5f9ee17229d6477e18f818d0",
                "tipo": {
                  "_id": "5f94cba7ae527551705bd906",
                  "titulo": "PAGINA WEB",
                  "desc": "website",
                  "mask": "",
                  "createdAt": "2020-10-25T00:49:43.148Z",
                  "updatedAt": "2020-10-25T00:49:43.148Z",
                  "__v": 0
                },
                "valor": "paginaweb.com/lucassilva"
              }
            ],
            "titulo": "Lucas Silva",
            "desc": "Vendedor"
          }
        },
        {
          "_id": "5f9ee17229d6477e18f818d1",
          "createdAt": "2020-11-01T19:00:28.341Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "filial": {
            "_id": "5f9ee17229d6477e18f818c6",
            "_temp": null,
            "titulo": "Seg Loja",
            "desc": "Seg Loja Desc",
            "localizacao": {
              "_id": "5f9ee17229d6477e18f818c7",
              "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
              "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
              "lat": -25.5008981,
              "long": -49.22838369999999,
              "pais": "Brazil",
              "estado": "Paraná",
              "cidade": "Curitiba",
              "area": "Boqueirão",
              "codigoPostal": "81730-070",
              "numero": "1894",
              "complemento": "",
              "createdAt": "2020-11-01T19:00:28.326Z",
              "updatedAt": "2020-11-01T19:00:28.326Z"
            },
            "createdAt": "2020-11-01T16:25:22.804Z",
            "updatedAt": "2020-11-01T19:00:28.341Z",
            "__v": 0
          },
          "contato": {
            "_id": "5f9ee17229d6477e18f818d2",
            "infos": [
              {
                "_id": "5f9ee17229d6477e18f818d3",
                "tipo": {
                  "_id": "5f94cb89ae527551705bd905",
                  "titulo": "CELULAR",
                  "desc": "numero de celular",
                  "mask": "",
                  "createdAt": "2020-10-25T00:49:13.912Z",
                  "updatedAt": "2020-10-25T00:49:13.912Z",
                  "__v": 0
                },
                "valor": "41998876755"
              }
            ],
            "titulo": "Almeida",
            "desc": "desc Almeida"
          }
        }
      ],
      "createdAt": "2020-11-01T02:01:43.945Z",
      "updatedAt": "2020-11-01T19:00:28.341Z",
      "__v": 0
    },
    "contato": {
      "_id": "5f9e170714cc488580744f32",
      "createdAt": "2020-11-01T19:00:28.341Z",
      "updatedAt": "2020-11-01T19:00:28.341Z",
      "filial": {
        "_id": "5f9e170714cc488580744f28",
        "_temp": null,
        "titulo": "Matriz",
        "desc": "Principal",
        "localizacao": {
          "_id": "5f9e170714cc488580744f29",
          "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
          "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
          "lat": -25.4105754,
          "long": -49.2571317,
          "pais": "Brazil",
          "estado": "Paraná",
          "cidade": "Curitiba",
          "area": "Juvevê",
          "codigoPostal": "80540-180",
          "numero": "143",
          "complemento": "",
          "createdAt": "2020-11-01T19:00:28.325Z",
          "updatedAt": "2020-11-01T19:00:28.325Z"
        },
        "createdAt": "2020-11-01T02:01:43.891Z",
        "updatedAt": "2020-11-01T19:00:28.341Z",
        "__v": 0
      },
      "contato": {
        "_id": "5f9e170714cc488580744f33",
        "infos": [
          {
            "_id": "5f9e170714cc488580744f34",
            "tipo": "5f94caf4ae527551705bd904",
            "valor": "paulo_almeida@email.com",
            "createdAt": "2020-11-01T02:01:43.944Z",
            "updatedAt": "2020-11-01T02:01:43.944Z"
          },
          {
            "_id": "5f9e170714cc488580744f35",
            "tipo": "5f94cba7ae527551705bd906",
            "valor": "pauloalmeida.com.br",
            "createdAt": "2020-11-01T02:01:43.945Z",
            "updatedAt": "2020-11-01T02:01:43.945Z"
          }
        ],
        "createdAt": "2020-11-01T02:01:43.945Z",
        "updatedAt": "2020-11-01T02:01:43.945Z",
        "titulo": "Paulo Almeida",
        "desc": "Gerente"
      }
    }
  },
  "transporte": {
    "_id": "5fae0772f9c82d54b8b72139",
    "createdAt": "2020-11-21T00:43:29.920Z",
    "updatedAt": "2020-11-21T00:43:29.920Z",
    "veiculo": {
      "compatibilidade": [
        {
          "compatibilidade": [],
          "_id": "5f9da2f9630e988764e477cf",
          "titulo": "CARRETA1",
          "tipoFormacaoTitulo": "MANUAL",
          "desc": "Carreta para o CAVALO",
          "tipoFormacaoDesc": "MANUAL",
          "placa": "",
          "marca": "Marca Carreta",
          "modelo": "Modelo Carreta",
          "tipo": "Tipo Carreta",
          "ano": 2012,
          "limite": {
            "_id": "5f9da2f9630e988764e477d0",
            "peso": 23400,
            "volume": 40000,
            "tara": 3400,
            "createdAt": "2020-10-31T18:47:55.237Z",
            "updatedAt": "2020-10-31T18:47:55.237Z"
          },
          "autonomo": true,
          "trator": false,
          "funcao": "CARRETA",
          "createdAt": "2020-10-31T17:46:33.276Z",
          "updatedAt": "2020-10-31T18:47:55.237Z",
          "__v": 0
        },
        {
          "compatibilidade": [],
          "_id": "5f9da345630e988764e477d1",
          "titulo": "CARRETA2",
          "tipoFormacaoTitulo": "MANUAL",
          "desc": "Carreta para o CAVALO",
          "tipoFormacaoDesc": "MANUAL",
          "placa": "AAA-1234",
          "marca": "Marca Carreta2",
          "modelo": "Modelo Carreta2",
          "tipo": "Tipo Carreta2",
          "ano": 2013,
          "limite": {
            "_id": "5f9da345630e988764e477d2",
            "peso": 41300,
            "volume": 50000,
            "tara": 21300,
            "createdAt": "2020-10-31T17:47:49.422Z",
            "updatedAt": "2020-10-31T17:47:49.422Z"
          },
          "autonomo": true,
          "trator": false,
          "funcao": "CARRETA",
          "createdAt": "2020-10-31T17:47:49.422Z",
          "updatedAt": "2020-10-31T17:47:49.422Z",
          "__v": 0
        }
      ],
      "_id": "5f9da276630e988764e477cd",
      "titulo": "CAV-3110 / Modelo Cavalo / Marca Cavalo  / 2012",
      "tipoFormacaoTitulo": "Automatizado",
      "desc": "",
      "tipoFormacaoDesc": "MANUAL",
      "placa": "CAV-3110",
      "marca": "Marca Cavalo",
      "modelo": "Modelo Cavalo",
      "tipo": "Tipo Cavalo",
      "ano": 2012,
      "limite": {
        "_id": "5f9da276630e988764e477ce",
        "peso": 23300,
        "volume": 30000,
        "tara": 13300,
        "createdAt": "2020-11-07T22:32:54.085Z",
        "updatedAt": "2020-11-07T22:32:54.085Z"
      },
      "autonomo": false,
      "trator": true,
      "funcao": "CAVALO",
      "createdAt": "2020-10-31T17:44:22.113Z",
      "updatedAt": "2020-11-07T22:32:54.085Z",
      "__v": 0
    },
    "carreta": {
      "compatibilidade": [],
      "_id": "5f9da2f9630e988764e477cf",
      "titulo": "CARRETA1",
      "tipoFormacaoTitulo": "MANUAL",
      "desc": "Carreta para o CAVALO",
      "tipoFormacaoDesc": "MANUAL",
      "placa": "",
      "marca": "Marca Carreta",
      "modelo": "Modelo Carreta",
      "tipo": "Tipo Carreta",
      "ano": 2012,
      "limite": {
        "_id": "5f9da2f9630e988764e477d0",
        "peso": 23400,
        "volume": 40000,
        "tara": 3400,
        "createdAt": "2020-10-31T18:47:55.237Z",
        "updatedAt": "2020-10-31T18:47:55.237Z"
      },
      "autonomo": true,
      "trator": false,
      "funcao": "CARRETA",
      "createdAt": "2020-10-31T17:46:33.276Z",
      "updatedAt": "2020-10-31T18:47:55.237Z",
      "__v": 0
    }
  },
  "funcionarios": [
    {
      "_id": "5fae0772f9c82d54b8b7213a",
      "createdAt": "2020-11-21T00:43:29.920Z",
      "updatedAt": "2020-11-21T00:43:29.920Z",
      "funcionario": {
        "posicoes": [
          "CEO"
        ],
        "filiais": [],
        "_id": "5f87196b08cdd24ddcf7a6fa",
        "nome": "Marcio Americo",
        "identificacao": "231.698.630-70",
        "empresa": {
          "_id": "5f7e94b594cb094d00882e6d",
          "nomeFantasia": "Distribuidora Gracia",
          "razaoSocial": "Distribuidora Garcia Ltda",
          "desc": "Supermecardo Irmaos Garcia Ltda",
          "identificacao": "99372050000167",
          "dados": "5f7e94b594cb094d00882e6c",
          "createdAt": "2020-10-08T04:25:25.868Z",
          "updatedAt": "2020-10-08T04:25:25.868Z",
          "__v": 0
        },
        "createdAt": "2020-10-14T15:29:47.551Z",
        "updatedAt": "2020-10-31T19:17:48.657Z",
        "__v": 0
      },
      "tipo": "GERAL"
    }
  ],
  "compromisso": {
    "tipo": "entrega",
    "_id": "5fb00edd95131599e40367a5",
    "titulo": "",
    "inicio": "2020-11-20T17:04:32.321Z",
    "fim": "2020-11-20T17:07:32.321Z",
    "recursos": null,
    "diaInteiro": true,
    "createdAt": "2020-11-14T17:07:41.725Z",
    "updatedAt": "2020-11-21T00:43:29.854Z",
    "__v": 0
  },
  "criarCompromisso": true,
  "createdAt": "2020-11-13T04:11:30.444Z",
  "updatedAt": "2020-11-21T00:43:29.920Z",
  "__v": 0
}

const tituloEntrega = generateTituloEntrega(entrega)
console.log(' --- entrega.titulo ---', tituloEntrega)
const descEntrega = generateDescEntrega(entrega)
console.log(' --- entrega.desc ---', descEntrega)

// ponto-rota

const generateTituloPontoRota = (obj) => {
  if(!obj) {
    return ''
  }
  const { localizacao, filial, parceiro } = obj
  const limiteTitulo = obj.limite && obj.limite.peso
    ? `${obj.limite.peso > 0 ? 'Carga' : 'Descarga' }: ${Math.abs(obj.limite.peso)}`
    : ''
  const parceiroTitulo = parceiro
    ? parceiro.titulo
    : null
  const filialTitulo = filial
    ? filial.titulo
    : null

  const localizacaoTitulo = localizacao
    ? localizacao.endereco || ` lat: ${localizacao.lat}, lng: ${localizacao.long} `
    : null
  const descTitulo = [
    parceiroTitulo,
    filialTitulo,
    limiteTitulo,
    localizacaoTitulo
  ].filter((val) => Boolean(val)).join(' ')

  return cleanLabel(`#${obj.ordem} ${descTitulo}`)
}

const generateDescPontoRota = (obj) => {
  if(!obj) {
    return ''
  }
  const {
    limite,
    localizacao,
    parceiro,
    filial
  } = obj;

  const parceiroFilialDesc =  parceiro || filial
    ? [
        (parceiro && parceiro.titulo
          ? 'Parceiro: ' + parceiro.titulo
          : ''),
        (filial && filial.titulo
          ? 'Filial: ' + filial.titulo
          : '')
      ].filter(val => Boolean(val)).join(', ')+'\n'
    : ''

  const cargaDescargaDesc = limite
    ? `${
        limite.peso > 0
          ? 'Carga'
          : 'Descarga'
      }( ${
        limite.peso
        ? 'Peso: ' + limite.peso
        : ''
      } ${
        limite.volume
          ? 'Volume: ' + limite.volume
          : ''
      })\n`
    : ''
  const trajetoAnteriorSpace = '  '
  const trajetoAnteriorDesc = (obj.distanciaDesc || obj.distancia)
    ? `${
      'Trajeto da parada anterior até a atual:\n'
      }${[
        (obj.distanciaDesc || obj.distancia
          ? trajetoAnteriorSpace + 'Distância: ' + (obj.distanciaDesc || obj.distancia)
          : null),
        (obj.duracaoDesc || obj.duracao
          ? trajetoAnteriorSpace + 'Duração: ' + (obj.duracaoDesc || obj.duracao)
          : null),
        (obj.duracaoComTrafegoDesc || obj.duracaoComTrafego
          ? trajetoAnteriorSpace + 'Duração com Tráfego: ' + (obj.duracaoComTrafegoDesc || obj.duracaoComTrafego) 
          : null)
      ].filter(val => Boolean(val)).join(', ')
    }`
    : ''
 
  const localizacaoSpace = '  '
  const localizacaoDesc = localizacao ? `${
      localizacaoSpace + 'Localização:\n'
    }${
      localizacao.endereco
      ? 'Endereço ' + localizacao.endereco
      : ''
    }\n${
      localizacao.lat || localizacao.long
        ? `Lat / Lng: ${localizacao.lat} / ${localizacao.long}`
        : ''
    }\n${
      localizacao.pais
      ? 'Pais: ' + localizacao.pais
      : ''
    }, ${
      localizacao.estado
        ? 'Estado: ' + localizacao.estado
        : ''
    }, ${
      localizacao.cidade
        ? 'Cidade: ' + localizacao.cidade
        : ''
    }\n${
      localizacao.area
        ? 'Bairro: ' + localizacao.area
        : ''
    }, ${
        localizacao.numero
          ? 'Número ' + localizacao.numero
          : ''
    }, ${
      localizacao.complemento
        ? 'Complemento: ' + localizacao.complemento
        : ''
    }\n${
      localizacao.codigoPostal
        ? 'Código Postal ' + localizacao.codigoPostal
        : ''
    }\n` : ''
  
  return cleanLabel(`${
      'Parada Rota #' + obj.ordem + ':\n'
    }${
      trajetoAnteriorDesc
    }${
      obj.anterior
        ? 'Anterior: ' + obj.anterior
        .map((ant) => ant ? ant.titulo : null)
        .filter(val => Boolean(val))
        .join(', ') + '\n' 
        : ''
    }${
      cargaDescargaDesc
    }${
      obj.status
        ? 'Status: ' + obj.status + '\n' 
        : ''
    }${
      obj._id || obj._temp
        ? 'ID: ' + (obj._id || obj._temp) + '\n' 
        : ''
    }${
      parceiroFilialDesc
    }${
      localizacaoDesc
    }`)
}

const pontoRota = {
        "anterior": [
          {
            "anterior": [],
            "_id": "5fae0772f9c82d54b8b7212c",
            "_temp": null,
            "createdAt": "2020-11-13T04:11:30.443Z",
            "updatedAt": "2020-11-13T04:11:30.443Z",
            "titulo": "Carregamento",
            "tipoFormacaoTitulo": "MANUAL",
            "desc": "",
            "tipoFormacaoDesc": "AUTOMATIZADO",
            "ordem": 1,
            "distancia": null,
            "duracao": null,
            "duracaoComTrafego": null,
            "distanciaDesc": null,
            "duracaoDesc": null,
            "duracaoComTrafegoDesc": null,
            "localizacao": {
              "_id": "5fa364cde85d1026940e847b",
              "createdAt": "2020-11-05T02:34:53.267Z",
              "updatedAt": "2020-11-05T02:34:53.267Z",
              "titulo": "D B R Química Com e Transportes de Produtos Químicos - Pedro machado e ilha, Contenda - PR, Brasil",
              "endereco": "Rodovia do Xisto BR 476 km 170 n°2979 - Pedro machado e ilha, Contenda - PR, 83730-000, Brazil",
              "lat": -25.674374,
              "long": -49.514709,
              "pais": "Brazil",
              "estado": "Paraná",
              "cidade": "Contenda",
              "area": "Pedro machado e ilha",
              "codigoPostal": "83730-000",
              "numero": "",
              "complemento": ""
            },
            "limite": {
              "_id": "5fae0772f9c82d54b8b7212e",
              "peso": 12000,
              "volume": 14028,
              "tara": 0,
              "createdAt": "2020-11-13T04:11:30.443Z",
              "updatedAt": "2020-11-13T04:11:30.443Z"
            },
            "status": "PENDENTE",
            "usarFilial": true,
            "filial": {
              "_id": "5fa364cde85d1026940e847a",
              "_temp": "",
              "titulo": "Fabrica",
              "desc": "",
              "localizacao": {
                "_id": "5fa364cde85d1026940e847b",
                "titulo": "D B R Química Com e Transportes de Produtos Químicos - Pedro machado e ilha, Contenda - PR, Brasil",
                "endereco": "Rodovia do Xisto BR 476 km 170 n°2979 - Pedro machado e ilha, Contenda - PR, 83730-000, Brazil",
                "lat": -25.674374,
                "long": -49.514709,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Contenda",
                "area": "Pedro machado e ilha",
                "codigoPostal": "83730-000",
                "numero": "",
                "complemento": "",
                "createdAt": "2020-11-05T02:34:53.267Z",
                "updatedAt": "2020-11-05T02:34:53.267Z"
              },
              "createdAt": "2020-11-05T02:34:53.267Z",
              "updatedAt": "2020-11-05T02:34:53.267Z",
              "__v": 0
            }
          }
        ],
        "_id": "5fae0772f9c82d54b8b7212f",
        "_temp": "_5fb861e93cb8d3000063422d",
        "createdAt": "2020-11-13T04:11:30.443Z",
        "updatedAt": "2020-11-13T04:11:30.443Z",
        "titulo": "#2 Supermercado Boa Vista  - Matriz - Descarga: 2000 - Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
        "tipoFormacaoTitulo": "AUTOMATIZADO",
        "desc": "",
        "tipoFormacaoDesc": "AUTOMATIZADO",
        "ordem": 3,
        "distancia": 46695,
        "duracao": 3529,
        "duracaoComTrafego": 3217,
        "distanciaDesc": "46,7 km",
        "duracaoDesc": "59 minutos",
        "duracaoComTrafegoDesc": "54 minutos",
        "localizacao": {
          "_id": "5f9e170714cc488580744f29",
          "createdAt": "2020-11-01T19:00:28.325Z",
          "updatedAt": "2020-11-01T19:00:28.325Z",
          "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
          "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
          "lat": -25.4105754,
          "long": -49.2571317,
          "pais": "Brazil",
          "estado": "Paraná",
          "cidade": "Curitiba",
          "area": "Juvevê",
          "codigoPostal": "80540-180",
          "numero": "143",
          "complemento": ""
        },
        "limite": {
          "_id": "5fae0772f9c82d54b8b72131",
          "peso": -2000,
          "volume": -2338,
          "tara": 0,
          "createdAt": "2020-11-13T04:11:30.443Z",
          "updatedAt": "2020-11-13T04:11:30.443Z"
        },
        "status": "PENDENTE",
        "usarFilial": true,
        "parceiro": {
          "filiais": [
            {
              "_id": "5f9e170714cc488580744f28",
              "_temp": null,
              "titulo": "Matriz",
              "desc": "Principal",
              "localizacao": {
                "_id": "5f9e170714cc488580744f29",
                "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                "lat": -25.4105754,
                "long": -49.2571317,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Juvevê",
                "codigoPostal": "80540-180",
                "numero": "143",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.325Z",
                "updatedAt": "2020-11-01T19:00:28.325Z"
              },
              "createdAt": "2020-11-01T02:01:43.891Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            },
            {
              "_id": "5f9ee17229d6477e18f818c4",
              "_temp": null,
              "titulo": "Loja",
              "desc": "Loja principal",
              "localizacao": {
                "_id": "5f9ee17229d6477e18f818c5",
                "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, Brasil",
                "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "lat": -25.5008981,
                "long": -49.22838369999999,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Boqueirão",
                "codigoPostal": "81730-070",
                "numero": "1894",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.326Z",
                "updatedAt": "2020-11-01T19:00:28.326Z"
              },
              "createdAt": "2020-11-01T16:25:22.804Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            },
            {
              "_id": "5f9ee17229d6477e18f818c6",
              "_temp": null,
              "titulo": "Seg Loja",
              "desc": "Seg Loja Desc",
              "localizacao": {
                "_id": "5f9ee17229d6477e18f818c7",
                "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                "lat": -25.5008981,
                "long": -49.22838369999999,
                "pais": "Brazil",
                "estado": "Paraná",
                "cidade": "Curitiba",
                "area": "Boqueirão",
                "codigoPostal": "81730-070",
                "numero": "1894",
                "complemento": "",
                "createdAt": "2020-11-01T19:00:28.326Z",
                "updatedAt": "2020-11-01T19:00:28.326Z"
              },
              "createdAt": "2020-11-01T16:25:22.804Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "__v": 0
            }
          ],
          "_id": "5f9e170714cc488580744f2c",
          "titulo": "Supermercado Boa Vista ",
          "desc": "Supermercado Boa Vista, venda deALT",
          "identificacao": "84.682.149/0001-98",
          "tipoPessoa": "JURIDICA",
          "contatos": [
            {
              "_id": "5f9e170714cc488580744f32",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9e170714cc488580744f28",
                "_temp": null,
                "titulo": "Matriz",
                "desc": "Principal",
                "localizacao": {
                  "_id": "5f9e170714cc488580744f29",
                  "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                  "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                  "lat": -25.4105754,
                  "long": -49.2571317,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Juvevê",
                  "codigoPostal": "80540-180",
                  "numero": "143",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.325Z",
                  "updatedAt": "2020-11-01T19:00:28.325Z"
                },
                "createdAt": "2020-11-01T02:01:43.891Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9e170714cc488580744f33",
                "infos": [
                  {
                    "_id": "5f9e170714cc488580744f34",
                    "tipo": {
                      "_id": "5f94caf4ae527551705bd904",
                      "titulo": "E-MAIL",
                      "desc": "Endereço eletronico",
                      "mask": "[a-z|0-9]@[a-z|0-9].com",
                      "createdAt": "2020-10-25T00:46:44.932Z",
                      "updatedAt": "2020-10-25T00:46:44.932Z",
                      "__v": 0
                    },
                    "valor": "paulo_almeida@email.com",
                    "createdAt": "2020-11-01T02:01:43.944Z",
                    "updatedAt": "2020-11-01T02:01:43.944Z"
                  },
                  {
                    "_id": "5f9e170714cc488580744f35",
                    "tipo": {
                      "_id": "5f94cba7ae527551705bd906",
                      "titulo": "PAGINA WEB",
                      "desc": "website",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:43.148Z",
                      "updatedAt": "2020-10-25T00:49:43.148Z",
                      "__v": 0
                    },
                    "valor": "pauloalmeida.com.br",
                    "createdAt": "2020-11-01T02:01:43.945Z",
                    "updatedAt": "2020-11-01T02:01:43.945Z"
                  }
                ],
                "createdAt": "2020-11-01T02:01:43.945Z",
                "updatedAt": "2020-11-01T02:01:43.945Z",
                "titulo": "Paulo Almeida",
                "desc": "Gerente"
              }
            },
            {
              "_id": "5f9ee17229d6477e18f818ce",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9e170714cc488580744f28",
                "_temp": null,
                "titulo": "Matriz",
                "desc": "Principal",
                "localizacao": {
                  "_id": "5f9e170714cc488580744f29",
                  "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
                  "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
                  "lat": -25.4105754,
                  "long": -49.2571317,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Juvevê",
                  "codigoPostal": "80540-180",
                  "numero": "143",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.325Z",
                  "updatedAt": "2020-11-01T19:00:28.325Z"
                },
                "createdAt": "2020-11-01T02:01:43.891Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9ee17229d6477e18f818cf",
                "infos": [
                  {
                    "_id": "5f9ee17229d6477e18f818d0",
                    "tipo": {
                      "_id": "5f94cba7ae527551705bd906",
                      "titulo": "PAGINA WEB",
                      "desc": "website",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:43.148Z",
                      "updatedAt": "2020-10-25T00:49:43.148Z",
                      "__v": 0
                    },
                    "valor": "paginaweb.com/lucassilva"
                  }
                ],
                "titulo": "Lucas Silva",
                "desc": "Vendedor"
              }
            },
            {
              "_id": "5f9ee17229d6477e18f818d1",
              "createdAt": "2020-11-01T19:00:28.341Z",
              "updatedAt": "2020-11-01T19:00:28.341Z",
              "filial": {
                "_id": "5f9ee17229d6477e18f818c6",
                "_temp": null,
                "titulo": "Seg Loja",
                "desc": "Seg Loja Desc",
                "localizacao": {
                  "_id": "5f9ee17229d6477e18f818c7",
                  "titulo": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                  "endereco": "Rua O Brasil para Cristo, 1894 - Boqueirão, Curitiba - PR, 81730-070, Brazil",
                  "lat": -25.5008981,
                  "long": -49.22838369999999,
                  "pais": "Brazil",
                  "estado": "Paraná",
                  "cidade": "Curitiba",
                  "area": "Boqueirão",
                  "codigoPostal": "81730-070",
                  "numero": "1894",
                  "complemento": "",
                  "createdAt": "2020-11-01T19:00:28.326Z",
                  "updatedAt": "2020-11-01T19:00:28.326Z"
                },
                "createdAt": "2020-11-01T16:25:22.804Z",
                "updatedAt": "2020-11-01T19:00:28.341Z",
                "__v": 0
              },
              "contato": {
                "_id": "5f9ee17229d6477e18f818d2",
                "infos": [
                  {
                    "_id": "5f9ee17229d6477e18f818d3",
                    "tipo": {
                      "_id": "5f94cb89ae527551705bd905",
                      "titulo": "CELULAR",
                      "desc": "numero de celular",
                      "mask": "",
                      "createdAt": "2020-10-25T00:49:13.912Z",
                      "updatedAt": "2020-10-25T00:49:13.912Z",
                      "__v": 0
                    },
                    "valor": "41998876755"
                  }
                ],
                "titulo": "Almeida",
                "desc": "desc Almeida"
              }
            }
          ],
          "createdAt": "2020-11-01T02:01:43.945Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "__v": 0
        },
        "filial": {
          "_id": "5f9e170714cc488580744f28",
          "_temp": null,
          "titulo": "Matriz",
          "desc": "Principal",
          "localizacao": {
            "_id": "5f9e170714cc488580744f29",
            "titulo": "Avenida Anita Garibaldi, 143 - Juvevê, Curitiba - PR, Brasil",
            "endereco": "Av. Anita Garibaldi, 143 - Juvevê, Curitiba - PR, 80540-180, Brazil",
            "lat": -25.4105754,
            "long": -49.2571317,
            "pais": "Brazil",
            "estado": "Paraná",
            "cidade": "Curitiba",
            "area": "Juvevê",
            "codigoPostal": "80540-180",
            "numero": "143",
            "complemento": "",
            "createdAt": "2020-11-01T19:00:28.325Z",
            "updatedAt": "2020-11-01T19:00:28.325Z"
          },
          "createdAt": "2020-11-01T02:01:43.891Z",
          "updatedAt": "2020-11-01T19:00:28.341Z",
          "__v": 0
        }
      }

const tituloPontoRota = generateTituloPontoRota(pontoRota)
console.log(' --- pontoRota.titulo ---', tituloPontoRota)
const descPontoRota = generateDescPontoRota(pontoRota)
console.log(' --- pontoRota.desc ---', descPontoRota)

// veiculo
const generateTituloVeiculo = (obj) => {
  if(!obj) {
    return ''
  }
  const result = [
    (obj.placa || null),
    (obj.modelo || null),
    (obj.marca || null),
    (obj.ano || null)
  ].filter(val => Boolean(val)).join(', ')
  return cleanLabel(result)
}

const generateDescVeiculo = (obj) => {
  if(!obj) {
    return ''
  }
  const limitDesc = obj.limite ? [
      'Limite:\n',
      (obj.limite.tara
        ? 'Peso do veículo: ' + obj.limite.tara 
        : null),
      (obj.limite.peso
        ? 'Peso: ' + obj.limite.peso
        : null),
      (obj.limite.volume
        ? 'Volume: ' + obj.limite.volume
        : null)
  ].filter(val => Boolean(val)).join(', ') : ''
  const compatibilidadeDesc = obj.compatibilidade
  ? 'Carretas:\n' + obj.compatibilidade
    .map((c) => c ? '    '+c.titulo : null)
    .filter(val => Boolean(val))
    .join('\n') 
  : ''

  return `${
    'Veiculo:\n'
  }${
    obj.placa ? 'Placa: ' + obj.placa : ''
  } ${
    obj.marca ? 'Marca: ' + obj.marca : ''
  } ${
      obj.modelo ? 'Modelo: ' + obj.modelo : ''
  } ${
    obj.tipo ? 'Tipo: ' + obj.tipo : ''
  } ${
    obj.ano ? 'Ano: ' + obj.ano : ''
  }\n${
    obj.autonomo
      ? 'Veículo é autônomo para carga'
      : 'Veículo não é autônomo para carga'
  }\n${
    obj.trator 
      ? 'Veículo tem tração própria'
      : 'Veículo não tem tração própria'
  }\n${
    limitDesc
  }\n${
    compatibilidadeDesc
  }`
}

const veiculo = {
  "compatibilidade": [
    {
      "compatibilidade": [],
      "_id": "5f9da2f9630e988764e477cf",
      "titulo": "CARRETA1",
      "tipoFormacaoTitulo": "MANUAL",
      "desc": "Carreta para o CAVALO",
      "tipoFormacaoDesc": "MANUAL",
      "placa": "",
      "marca": "Marca Carreta",
      "modelo": "Modelo Carreta",
      "tipo": "Tipo Carreta",
      "ano": 2012,
      "limite": {
        "_id": "5f9da2f9630e988764e477d0",
        "peso": 23400,
        "volume": 40000,
        "tara": 3400,
        "createdAt": "2020-10-31T18:47:55.237Z",
        "updatedAt": "2020-10-31T18:47:55.237Z"
      },
      "autonomo": true,
      "trator": false,
      "funcao": "CARRETA",
      "createdAt": "2020-10-31T17:46:33.276Z",
      "updatedAt": "2020-10-31T18:47:55.237Z",
      "__v": 0
    },
    {
      "compatibilidade": [],
      "_id": "5f9da345630e988764e477d1",
      "titulo": "CARRETA2",
      "tipoFormacaoTitulo": "MANUAL",
      "desc": "Carreta para o CAVALO",
      "tipoFormacaoDesc": "MANUAL",
      "placa": "AAA-1234",
      "marca": "Marca Carreta2",
      "modelo": "Modelo Carreta2",
      "tipo": "Tipo Carreta2",
      "ano": 2013,
      "limite": {
        "_id": "5f9da345630e988764e477d2",
        "peso": 41300,
        "volume": 50000,
        "tara": 21300,
        "createdAt": "2020-10-31T17:47:49.422Z",
        "updatedAt": "2020-10-31T17:47:49.422Z"
      },
      "autonomo": true,
      "trator": false,
      "funcao": "CARRETA",
      "createdAt": "2020-10-31T17:47:49.422Z",
      "updatedAt": "2020-10-31T17:47:49.422Z",
      "__v": 0
    }
  ],
  "_id": "5f9da276630e988764e477cd",
  "titulo": "",
  "tipoFormacaoTitulo": "Automatizado",
  "desc": "",
  "tipoFormacaoDesc": "MANUAL",
  "placa": "CAV-3110",
  "marca": "Marca Cavalo",
  "modelo": "Modelo Cavalo",
  "tipo": "Tipo Cavalo",
  "ano": 2012,
  "limite": {
    "_id": "5f9da276630e988764e477ce",
    "peso": 23300,
    "volume": 30000,
    "tara": 13300,
    "createdAt": "2020-11-07T22:32:54.085Z",
    "updatedAt": "2020-11-07T22:32:54.085Z"
  },
  "autonomo": false,
  "trator": true,
  "funcao": "CAVALO",
  "createdAt": "2020-10-31T17:44:22.113Z",
  "updatedAt": "2020-11-07T22:32:54.085Z",
  "__v": 0
}
const tituloVeiculo = generateTituloVeiculo(veiculo)
console.log(' --- veiculo.titulo ---', tituloVeiculo)
const descVeiculo = generateDescVeiculo(veiculo)
console.log(' --- veiculo.desc ---', descVeiculo)