

const _ = require('lodash'); 
class TravelingSalesman {
	/*
	const points = [
		{ x: 1, y: 2, value: 0, prev: [ 0,1,2 ] }
	]
		---------------------
		| x | a | b | c | d |
		|---|---|---|---|---|
		| a | x | 0 | 0 | 0 |
		|---|---|---|---|---|
		| b | 0 | x | 0 | 0 |
		|---|---|---|---|---|
		| c | 0 | 0 | x | 0 |
		|---|---|---|---|---|
		| d | 0 | 0 | 0 | x |
		---------------------

	Paths
		abcd
		abdc
		acbd
		acdb
		adbc
		adcb

	*/
	// [ {x, y, prev } ]
	search(points, distanceMatrix, history){
    let paths = [];
		history = history || {
        path: [],
        distance: 0,
        options: points.map((value, index) => Number(index)),
        lastOption: null
      };
    // history params
		let {
      path,
      distance,
      options,
      lastOption
    } = history;
		if(options.length === 0) {
			return [{ distance, path }]
		}
		for(const index in options) {
			// point
      const option = options[index];
      const point = points[option];
			const lastPoint =  (lastOption === null) ?
        points[lastOption] :
        null;

			// distance
			const cDistance = (lastOption !== null) ?  distanceMatrix[lastOption][option] : 0;

			if(cDistance === null) {
				continue;
			}
			
			// cOptions
			let cOptions = options.slice();
			cOptions.splice(index, 1)
			
			// cPath
			const cPath = this.search(
					points,
					distanceMatrix,
					{ // history
						path: [ ...path, option ],
						distance: ( distance + cDistance),
						options: cOptions,
						lastOption: option
					}
				)
			
				paths = [
					...paths,
					...cPath
				];
			}
			return paths;
	}


	getDistance(a, b) {
		return Math.sqrt(
			Math.pow(
				Math.abs(a.x - b.x),
				2
			) +
			Math.pow(
				Math.abs(a.y - b.y),
				2
			)
		)
	
  }

	getDistanceMatrix(points) {
		const distanceMatrix = [];
		for(const pointIndex in points) {
			const point = points[pointIndex] || [];
			const prev = (point.prev || [])
			for(const comparePointIndex in points) {
				const point = points[pointIndex];
				const comparePoint = points[comparePointIndex];
        distanceMatrix[pointIndex] = distanceMatrix[pointIndex] || [];
				distanceMatrix[pointIndex][comparePointIndex] = (
					pointIndex  !== comparePointIndex &&
					!prev.includes(comparePointIndex)
				) ?
					this.getDistance(point, comparePoint) :
					null
			}
		}
    return distanceMatrix;
	}
	
	naive(points, pDistanceMatrix) {
		const distanceMatrix = _.cloneDeep(pDistanceMatrix || this.getDistanceMatrix(points));
		return  this.search(points, distanceMatrix);
	}
	
	nearestNeighbor(points, pDistanceMatrix){
		const distanceMatrix = _.cloneDeep(pDistanceMatrix || this.getDistanceMatrix(points));
		let rowIndex = 0;
		let distance = 0;
		let path = [ 0 ];
		let keep = true;
		while(end) {
			const row  = distanceMatrix[rowIndex];
			let minCol = { index: null, value: null };
			for(const colIndex in row) {
				const col  = row[colIndex];
				if( (minCol.index === null || col < minCol.value) && distanceMatrix[colIndex]){
					minCol = { index: colIndex, value: col };
				}
			}
			if(!minCol || minCol.index) {
				keep = false;
				break;
			}
			path.push(minCol.index);
			distance += minCol.value;
			distanceMatrix[rowIndex] = null;
			rowIndex = minCol.index;
		} 
		return { distance, path };
	}

}

const tsp = new TravelingSalesman();
const points = [
    { // A
      x: 1,
      y: 1,
      value: 'a' 
    },
    { // B
      x: 2,
      y: 1,
      value: 'b'
    },
    { // C
      x: 1,
      y: 2,
      value: 'c'
    },
    { // D
      x: 2,
      y: 2,
      value: 'd'
    }
];
// const result = tsp.naive(points);
const result = tsp.nearestNeighbor(points);


JSON.stringify(result)