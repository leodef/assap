import React, { useContext, useCallback, useMemo } from 'react'
import {
  Button,
  DialogActions,
  DialogTitle
} from '@material-ui/core'
import { CrudContext } from '../../../../../contexts/CrudContext'
import { useSelector, useDispatch } from 'react-redux'
import { ActionTypeEnum } from '../../../../../types/Crud'
import './RemoveDialog.scss'
import { LoadingButton } from '../../../../Shared/Utils/LoadingButton/LoadingButton'

/**
 * Dialogo para remoção do item
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com dialogo para remoção do item
 */
export const RemoveDialog = (props: any) => {
  // CrudContext
  const { getState, types } = useContext(CrudContext)
  // selector
  const { item, deleteLoading } = useSelector((state: any) => getState(state))

  const loading = useMemo(() => (deleteLoading),
    [deleteLoading])

  // dispatch
  const dispatch = useDispatch()
  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({ type: types.SET_ACTION_ITEM, payload: { action, item } })
  , [dispatch, types])
  const remove = useCallback((item: any) =>
    dispatch({ type: types.DELETE_ITEM, payload: { item } })
  , [dispatch, types])
  const back = useCallback(() => {
    setAction(ActionTypeEnum.LIST, null)
  }, [setAction])

  const dialogTitle = 'Deseja realmente remover?'

  const button = useMemo(() => {
    return {
      onClick: () => remove(item),
      color: 'primary',
      autoFocus: true
    }
  }, [remove, item])

  return (<React.Fragment>
    <DialogTitle id="remove-dialog-title">{dialogTitle}</DialogTitle>
    <DialogActions>
      <Button onClick={() => back()} color="primary">
          Não
      </Button>
      <LoadingButton
        button={button}
        loading={loading}>
        Sim
      </LoadingButton>
    </DialogActions>
  </React.Fragment>)
}
