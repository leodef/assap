import React, { useCallback, useContext, useMemo, useEffect } from 'react'
import {
  Grid,
  Button,
  Card,
  CardActions,
  CardContent,
  Typography,
  makeStyles
} from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
// eslint-disable-next-line no-unused-vars
import { FormikHelpers, withFormik } from 'formik'
import { Fieldset } from '../Fieldset/Fieldset'
import { GrandezaSchema, initialValues } from '../../../../../types/Modulo/Admin/Grandeza'
import { CrudContext } from '../../../../../contexts/CrudContext'
import { ActionTypeEnum } from '../../../../../types/Crud'
import './Form.scss'
import _ from 'lodash'
import { LoadingButton } from '../../../../Shared/Utils/LoadingButton/LoadingButton'

const useStyles = makeStyles({
  root: {
    minWidth: 275
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
})

/**
 * Formulário para Grandeza
 * @param {FormProps} props Propriedades
 * @return {React.Component} Componente com formulário para Grandeza
 */
export const Form = (props: any) => {
  const dispatch = useDispatch()
  // CrudContext
  const { getState, types } = useContext(CrudContext)

  // selector
  const item = useSelector((state: any) => getState(state).item)

  // dispatch
  const save = useCallback((item: any) => {
    const type = item._id ? types.UPDATE_ITEM : types.CREATE_ITEM
    dispatch({ type, payload: { item } })
  }, [dispatch, types])

  const WithFormik = withFormik({
    mapPropsToValues: (props: any) => _.defaults(props.item, initialValues),
    handleSubmit: (values: any, formikHelpers: FormikHelpers<any>) => {
      const { setSubmitting } = formikHelpers
      setSubmitting(false)
      save(values)
    },
    validationSchema: GrandezaSchema
  })(FormBody)
  return (<WithFormik item={item} />)
}

const FormBody = (props: any) => {
  const classes = useStyles()
  return (<form onSubmit={props.handleSubmit}>
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          Grandeza
        </Typography>
        { /* props.children */}
        <Fieldset />
      </CardContent>
      <CardActions>
        <FormActions />
      </CardActions>
    </Card>
  </form>)
}

const FormActions = (props: any) => {
  // CrudContext
  const { getState, actions, types } = useContext(CrudContext)
  // selector
  const { item, action, createLoading, updateLoading } = useSelector((state: any) => getState(state))

  const loading = useMemo(() =>
    (createLoading || updateLoading),
  [createLoading, updateLoading])

  // dispatch
  const dispatch = useDispatch()
  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({ type: types.SET_ACTION_ITEM, payload: { action, item } })
  , [dispatch, types])

  const toRemove = useCallback((item: any) => {
    setAction(ActionTypeEnum.REMOVE, item)
  }, [setAction])

  const back = useCallback(() => {
    setAction(ActionTypeEnum.LIST, null)
  }, [setAction])

  // Metodo de consulta
  const find = useCallback((item: any) =>
    dispatch({ type: types.FIND_ITEM, payload: { item } }),
  [dispatch, types])

  // lifecycle
  // Executar metodo de consulta
  useEffect(() => {
    if (action === ActionTypeEnum.EDIT || action === ActionTypeEnum.NEW) {
      find(item)
    }
  }, [item._id, action])

  const removeButton = useMemo(() => {
    return actions.toRemove
      ? (<Button
        variant="contained"
        color="primary"
        onClick={(event: any) => toRemove(item)}>
        Remover
      </Button>) : null
  }, [actions, toRemove, item])

  const submitButton = useMemo(() => {
    const button = {
      type: 'submit',
      variant: 'contained',
      color: 'primary'
    }
    return (<LoadingButton
      loading={loading}
      button={button}>
        Salvar
    </LoadingButton>)
  }, [loading])

  const backButton = useMemo(() => {
    return actions.toBack
      ? (<Button
        variant="contained"
        color="primary"
        onClick={(event: any) => back()}>
          Voltar
      </Button>) : null
  }, [actions, back])
  // render
  return (
    <Grid container>
      <Grid item>
        {removeButton}
      </Grid>
      <Grid item>
        {submitButton}
      </Grid>
      <Grid item>
        {backButton}
      </Grid>
    </Grid>
  )
}
