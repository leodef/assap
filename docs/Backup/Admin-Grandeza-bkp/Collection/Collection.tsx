import React, { useMemo, useContext, useCallback, useEffect } from 'react'
import { Table } from '../../../../../components/Modulo/Admin/Grandeza/Table/Table'
import { List } from '../../../../../components/Modulo/Admin/Grandeza/List/List'
import { CrudContext } from '../../../../../contexts/CrudContext'
import { useSelector, useDispatch } from 'react-redux'
import { CollectionTypeEnum, ActionTypeEnum } from '../../../../../types/Crud'
import { CollectionContext } from '../../../../../contexts/CollectionContext'
import * as _ from 'lodash'
import './Collection.scss'

function contains (item: any, filter: any) {
  for (const index in item) {
    const obj = item[index]
    if (!obj) {
      continue
    }
    if (obj.toString().toUpperCase().contains(filter)) {
      return true
    }
  }
  return false
}

function applyPagination (items: Array<any>, pagination: any): Array<any> {
  if (!pagination) {
    return items
  }
  const { page, limit } = pagination
  if (!limit) {
    return items
  }
  const start = ((page || 1) - 1) * limit
  const end = start + limit
  return items.slice(start, end)
}

function applyFilter (items: Array<any>, filter: any): Array<any> {
  filter = filter ? filter.toString().toUpperCase() : null
  if (!filter || filter === '') {
    return items
  }
  return items.filter(item => contains(item, filter))
}
function getItems (items: Array<any>, filter: any, pagination: any) {
  return applyFilter(
    applyPagination(
      items, pagination
    ), filter
  )
}

/**
 * Coleção de item do tipo Grandeza
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com coleção de item do tipo Grandeza
 */
export const Collection = (props: any) => {
  // prop
  const { type } = props

  // CrudContext
  const { getState, actions, types } = useContext(CrudContext)

  // selector
  const { filter, pagination, items, action, fetchLoading } = useSelector((state: any) => getState(state))

  // dispatch
  const dispatch = useDispatch()

  // Metodo de consulta
  const fetch = useCallback((filt: any, pag: any) =>
    dispatch({ type: types.FETCH_ITEM, payload: { filter: filt, pagination: pag } }),
  [dispatch, types])

  // Metodo para atualizar informações do filtro
  const setFilter = useCallback((filt: any) =>
    dispatch({ type: types.SET_FILTER_ITEM, payload: { filter: filt } }),
  [dispatch, types])

  // Metodo para atualizar informações da paginação
  const setPagination = useCallback((pag) => {
    dispatch({
      type: types.SET_PAGINATION_ITEM,
      payload: {
        pagination: _.defaults(pag, pagination)
      }
    })
  },
  [dispatch, types, pagination])

  // lifecycle
  // Executar metodo de consulta
  useEffect(() => {
    if (action === ActionTypeEnum.LIST) {
      fetch(filter, pagination)
    }
  }, [fetch, filter, pagination, action])

  const completeFilter = useMemo(
    () => (filter || props.filter),
    [filter, props.filter])

  const completePagination = useMemo(
    () => _.defaults(pagination, props.pagination),
    [pagination, props.pagination])

  const filteredItems = useMemo(
    () => getItems(items, completeFilter, completePagination),
    [items, completeFilter, completePagination])

  // CollectionContext
  const collectionContextValue = useMemo(() => {
    return {
      actions,
      types,
      items: filteredItems,
      filter: completeFilter,
      pagination: completePagination,
      setFilter,
      setPagination,
      length: items.length,
      loading: fetchLoading
    } as any
  }, [
    actions,
    types,
    filteredItems,
    completeFilter,
    completePagination,
    setFilter,
    setPagination,
    items,
    fetchLoading
  ])
  const collectionContent = useMemo(() =>
    type === CollectionTypeEnum.TABLE
      ? (<Table />)
      : (<List />),
  [type])
  return (
    <CollectionContext.Provider
      value={collectionContextValue}>
      {collectionContent}
    </CollectionContext.Provider>)
}
