import React, { useContext, useCallback, useMemo, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Card,
  CardActions,
  CardContent,
  Typography,
  IconButton,
  Grid
} from '@material-ui/core'
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  ArrowBack as ArrowBackIcon
} from '@material-ui/icons'
import { CrudContext } from '../../../../../contexts/CrudContext'
import { useSelector, useDispatch } from 'react-redux'
import { ActionTypeEnum } from '../../../../../types/Crud'
import './Show.scss'
import { Resume } from '../Resume/Resume'

const useStyles = makeStyles({
  root: {
    minWidth: 275
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
})

/**
 * Visualização de item do tipo Grandeza
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Grandeza
 */
export const Show = (props: any) => {
  const classes = useStyles()
  // CrudContext
  const { getState, types } = useContext(CrudContext)
  // selector
  const { item, action } = useSelector((state: any) => getState(state))
  const title = useMemo(() => { return 'Grandeza' }, [])

  // dispatch
  const dispatch = useDispatch()

  // Metodo de consulta
  const find = useCallback((item: any) =>
    dispatch({ type: types.FIND_ITEM, payload: { item } }),
  [dispatch, types])

  const itemId = useMemo(() => item ? item._id : null, [item])
  // lifecycle
  // Executar metodo de consulta
  useEffect(() => {
    if (action === ActionTypeEnum.SHOW && itemId) {
      find(item)
    }
  }, [itemId, action])

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          {title}
        </Typography>
        <Resume item={item} />
      </CardContent>
      <CardActions>
        <ShowActions />
      </CardActions>
    </Card>
  )
}

const ShowActions = (props: any) => {
  // CrudContext
  const { getState, actions, types } = useContext(CrudContext)
  // selector
  const item = useSelector((state: any) => getState(state).item)
  // dispatch
  const dispatch = useDispatch()

  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({ type: types.SET_ACTION_ITEM, payload: { action, item } })
  , [dispatch, types])

  const toEdit = useCallback((item: any) => {
    setAction(ActionTypeEnum.EDIT, item)
  }, [setAction])

  const toRemove = useCallback((item: any) => {
    setAction(ActionTypeEnum.REMOVE, item)
  }, [setAction])

  const back = useCallback(() => {
    setAction(ActionTypeEnum.LIST, null)
  }, [setAction])

  const deleteButton = useMemo(() => {
    return actions.toRemove
      ? (
        <IconButton
          aria-label="Remover"
          onClick={() => toRemove(item)}>
          <DeleteIcon />
        </IconButton>
      )
      : null
  }, [actions, toRemove, item])

  const editButton = useMemo(() => {
    return actions.toEdit
      ? (
        <IconButton
          aria-label="Editar"
          onClick={() => toEdit(item)}>
          <EditIcon />
        </IconButton>
      )
      : null
  }, [actions, toEdit, item])

  const backButton = useMemo(() => {
    return actions.toBack
      ? (
        <IconButton
          aria-label="Voltar"
          onClick={() => back()}>
          <ArrowBackIcon />
        </IconButton>
      )
      : null
  }, [actions, back])

  return (
    <Grid container>
      <Grid item>
        {deleteButton}
      </Grid>
      <Grid item>
        {editButton}
      </Grid>
      <Grid item>
        {backButton}
      </Grid>
    </Grid>
  )
}
