import React, { useMemo, useContext } from 'react'
import { Table as SharedTable, TablePropsField } from '../../../../Shared/Collection/Table/Table'
import { CollectionContext } from '../../../../../contexts/CollectionContext'
import './Table.scss'

/**
 * Tabela de item do tipo Grandeza
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com tabela de item do tipo Grandeza
 */
export const Table = (props: any) => {
  // CollectionContext
  const context = useContext(CollectionContext)
  const collectionContextValue = useMemo(() => {
    return {
      ...context,
      config: {
        label: 'Grandeza',
        align: 'center',
        actionsLabel: 'Ações',
        fields: [
          new TablePropsField('titulo', 'Título'),
          new TablePropsField('desc', 'Descrição')
        ]
      }
    }
  }, [context])
  return (
    <CollectionContext.Provider value={collectionContextValue}>
      <SharedTable />
    </CollectionContext.Provider>
  )
}
