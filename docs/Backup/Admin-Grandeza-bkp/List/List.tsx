import React, { useMemo, useContext } from 'react'
import AssignmentIcon from '@material-ui/icons/Assignment'
import {
  List as SharedList
} from '../../../../Shared/Collection/List/List'
import { CollectionContext } from '../../../../../contexts/CollectionContext'
import './List.scss'

/**
 * Lista de items do tipo Grandeza
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com lista de items do tipo Grandeza
 */
export const List = (props: any) => {
  // CollectionContext
  const context = useContext(CollectionContext)
  const getListItem = (value: any, index: number) => {
    return {
      primary: (value.titulo ? value.titulo.toString() : ''),
      secondary: null,
      avatar: (value.titulo ? value.titulo.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
    }
  }
  const collectionContextValue = useMemo(() => {
    return {
      ...context,
      config: {
        dense: true,
        subheader: 'Grandeza',
        getListItem
      }
    }
  }, [context])
  return (
    <CollectionContext.Provider value={collectionContextValue}>
      <SharedList />
    </CollectionContext.Provider>
  )
}
