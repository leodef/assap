import * as Yup from 'yup';
//##!!
export const ProdutoSchema = Yup.object().shape({
  nome: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  desc: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  tipo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  marca: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required')
});

export const initialValues = {
  nome: '',
  desc: '',
  tipo: '',
  marca: ''
}

/*
    duracaoProducao: params.depedencies.Quantidade.schema,
    nome: String,
    desc: String,
    tipo: String,
    marca: String
*/