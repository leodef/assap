import * as Yup from 'yup';

export const FinalidadeUnidadeMedidaSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  desc: Yup.string()
    .min(2, 'Too Short!')
    .max(250, 'Too Long!')
    .required('Required')
});

export const initialValues = {
  titulo: '',
  desc: ''
}