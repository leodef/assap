import * as Yup from 'yup';

export const VeiculoSchema = Yup.object().shape({
  modelo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
    placa: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
    tipo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
    marca: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required')
});

export const initialValues = {
  modelo: '',
  placa: '',
  tipo: '',
  marca: ''
}

/*
    capacidade: params.depedencies.Quantidade.schema,
    modelo: String,
    placa: String,
    tipo: String,
    marca: String
  */