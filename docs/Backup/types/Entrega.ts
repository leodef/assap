import * as Yup from 'yup';
import moment from 'moment';
//##!!
export const EntregaSchema = Yup.object().shape({
  /*
  tipo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  */
});

export enum StatusEntrega {
  ABERTO = 'ABERTO',
  FECHADO = 'FECHADO',
  INICIADO = 'INICIADO',
  FINALIZADO = 'FINALIZADO'
}

export const initialValues = {
  criador: {},
  filialTransportadora: {}, // 'Filial'
  transportadora: {}, // 'Empresa'
  veiculo: {}, // 'Veiculo'
  trajeto: {
    pontos: [] // localizacao posicao
  }, // 'Trajeto' 
  acordos: [
      // criador 'Empresa'
      // remetente 'Filial'
      // destinatario 'Filial'
      // produto 'Produto'
      // quantidade: params.depedencies.Quantidade.schema
  ], // 'Acordo'
  dataFechamento: moment().add(1, 'day').format('YYYY-MM-DD'),
  status: StatusEntrega.ABERTO
}
/*

// Express my moment as a string. I happen to have chosen YYYY-MM-DD. You don't know why.
myMoemnt.format("YYYY-MM-DD");

// Express my moment as an ISO date string. You can expect that consumers of this string 
// are expecting an ISO formatted string.
myMoemtn.toISODateString();

I would probably end up with a solution like this to achieve readable code:

var ISO_DATE_FORMAT = 'YYYY-MM-DD";
myMoemtn.format(ISO_DATE_FORMAT);

*/