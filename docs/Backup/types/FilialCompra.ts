import * as Yup from 'yup';
//##!!
export const FilialCompraSchema = Yup.object().shape({
  /*
  tipo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  */
});

export const initialValues = {
  compra: '',
  transportadora: '',
  veiculo: '',
  trajeto: '',
  quantidade: {},
  data: Date
}

/*
    compra: { type: Schema.Types.ObjectId, ref: 'Compra' },
    transportadora: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    veiculo: { type: Schema.Types.ObjectId, ref: 'Veiculo' },
    trajeto: { type: Schema.Types.ObjectId, ref: 'Trajeto' },
    quantidade: params.depedencies.Quantidade.schema,
    data: Date
*/