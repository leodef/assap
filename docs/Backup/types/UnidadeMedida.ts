import * as Yup from 'yup';
//##!!
export const UnidadeMedidaSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  desc: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  finalidade: Yup.object()
    .required('Required')
});

export const initialValues = {
  finalidade: '',
  titulo: '',
  desc: '',
  valor: 0
}

/*
    finalidade: { type: Schema.Types.ObjectId, ref: 'FinalidadeUnidadeMedida' },
    titulo: String,
    desc: String,
    valor: Number
  */