import * as Yup from 'yup';
//##!!
export const TipoUsuarioSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  desc: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  posicao: Yup.number()
    .min(0, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required')
});

export const initialValues = {
  titulo: '',
  desc: '',
  posicao: 0
}

/*
    titulo: String,
    desc: String,
    posicao: Number
*/