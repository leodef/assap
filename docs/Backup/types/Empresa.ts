import * as Yup from 'yup';
//##!!
export const EmpresaSchema = Yup.object().shape({
  /*
  tipo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  */
});

export const initialValues = {
  nome: '',
  cnpj: '',
  tipo: '',
  tipoProducao: [],
  veiculo: [],
  contato: { info: [] }
  
}

/*
    contato: params.depedencies.Contato.schema,
    veiculo: [{ type: Schema.Types.ObjectId, ref: 'Veiculo' }],
    tipo: { type: Schema.Types.ObjectId, ref: 'TipoEmpresa' },
    tipoProducao: [{ type: Schema.Types.ObjectId, ref: 'TipoProducao' }],
    nome: String,
    cnpj: String
*/