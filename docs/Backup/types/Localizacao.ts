import * as Yup from 'yup';
//##!!
export const LocalizacaoSchema = Yup.object().shape({
  endereco: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  latitude: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  longitude: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required')
});

export const initialValues = {
  endereco: '',
  latitude: '',
  longitude: ''
}

/*
    endereco: String,
    latitude: String,
    longitude: String
*/