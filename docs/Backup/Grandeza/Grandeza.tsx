import React, { useEffect, useMemo} from 'react';
import { useDispatch, useSelector  } from 'react-redux';
import {
  makeStyles,
  Theme,
  createStyles
} from '@material-ui/core/styles';
import {
  Fab,
  Modal,
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
} from '@material-ui/core';
import {
  Add as AddIcon
} from '@material-ui/icons';
import { CrudType, CollectionTypeEnum, ActionTypeEnum } from '../../../../types/Crud';
import './Grandeza.scss';
import { Show } from '../../../../components/Modulo/Admin/Grandeza/Show/Show';
import { Form } from '../../../../components/Modulo/Admin/Grandeza/Form/Form';
import { Table } from '../../../../components/Modulo/Admin/Grandeza/Table/Table';
import { List } from '../../../../components/Modulo/Admin/Grandeza/List/List';
import Filter from '../../../../components/Shared/Filter/Filter';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
  }),
);

/**
 * Tela de gerenciameto de Grandeza
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Grandeza
 */
export function Grandeza(props: any) {
  const classes = useStyles();  
  const dispatch = useDispatch();
  const onSubmit = (values: any) => save(values);
  const collectionType = useMemo(()=> props.collectionType, [props.collectionType])
  const isFormModalOpen = useMemo(
    () => (action === ActionTypeEnum.EDIT || action === ActionTypeEnum.NEW ), 
    [action]);
  const isShowModalOpen = useMemo(
    () => (action === ActionTypeEnum.SHOW ),
    [action]);
  const isRemoveModalOpen = useMemo(
    () => (action === ActionTypeEnum.REMOVE ),
    [action]);
  // Executar pesquisa sempre que o filtro for alterado
  useEffect(() => {
      if(!!fetch && action === ActionTypeEnum.LIST) {
        fetch(filter)
      }
      console.log('update')
    // Reseta ao desmontar o componente
    return () => {
      console.log('ummont');
      if(!!reset) {
        reset()
      }
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <div className={classes.root}>
        <Filter
          filter={filter}
          setFilter={setFilter}
        />
        {
          (!collectionType || collectionType === CollectionType.TABLE) ?
          (<Table
            items={items}
            toShow={toShow}
            toEdit={toEdit}
            toRemove={toRemove}
          />) :
          (<List
            items={items}
            toShow={toShow}
            toEdit={toEdit}
            toRemove={toRemove}
          />)
        }
        <Fab color="secondary" aria-label="edit">
          <AddIcon onClick={() => toNew()} />
        </Fab>
      </div>
    
	  { /* Form */ }
    <Modal
      open={isFormModalOpen}
      onClose={() => toList()}
      aria-labelledby="form-modal-title"
      aria-describedby="form-modal-description">
        <div>
      <Form
        onSubmit={onSubmit}
        toRemove={toRemove}
        back={back}
        item={item}
      /></div>
    </Modal>

	  { /* Show */ }
    <Modal
      open={isShowModalOpen}
      onClose={() => toList()}
      aria-labelledby="show-modal-title"
      aria-describedby="show-modal-description">
      <div>
        <Show
          toRemove={toRemove}
          toEdit={toEdit}
          back={back}
          item={item} />
        </div>
  </Modal>

	  { /* Remove */ }
    <Dialog
        open={isRemoveModalOpen}
        onClose={() => toList()}
        aria-labelledby="remove-dialog-title"
        aria-describedby="remove-dialog-description">

    </Dialog>
    </div>
  );
}

/*

  applyFilter(): Array<any> {
    const { items, filter } = this.props;
    if(!items){
      return [];
    }
    if (!filter) { return items || []; }
    return items.filter((item: any) => {
        return (!item.titulo || item.titulo.indexOf(filter.text) >= 0) ||
        (!item.desc || item.desc.indexOf(filter.text) >= 0)
    });
  }
*/
