import React, { useEffect, useMemo, useCallback, useContext } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  Modal,
  Dialog,
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core'
import {
  CollectionTypeEnum,
  ActionTypeEnum
} from '../../../../types/Crud'
import { Show } from '../../../../components/Modulo/Admin/Grandeza/Show/Show'
import { Form } from '../../../../components/Modulo/Admin/Grandeza/Form/Form'
import { CrudContext } from '../../../../contexts/CrudContext'
import { Collection } from '../../../../components/Modulo/Admin/Grandeza/Collection/Collection'
import { RemoveDialog } from '../../../../components/Modulo/Admin/Grandeza/RemoveDialog/RemoveDialog'
import * as _ from 'lodash'
import './Grandeza.scss'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      minWidth: 200,
      minHeight: 200,
      // boxShadow: theme.shadows[5],
      // padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    }
  })
)

/**
 * Tela de gerenciameto de Grandeza
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Grandeza
 */
export const Grandeza = (props: any) => {
  const classes = useStyles()
  // CrudContext
  const { getState, types } = useContext(CrudContext)
  // values
  const collection = useMemo(() => {
    const result = {
      type: CollectionTypeEnum.LIST,
      limit: 10
    }
    if (props.collection) {
      return _.defaults(props.collection, result)
    }
    return result
  }, [props.collection])
  const action = useSelector(
    (state: any) => getState(state).action)
  const dispatch = useDispatch()
  const toList = useCallback(() =>
    dispatch({ type: types.SET_ACTION_ITEM, payload: { action: ActionTypeEnum.LIST } }),
  [dispatch, types])
  const clear = useCallback(() =>
    dispatch({ type: types.CLEAR_ITEM }),
  [dispatch, types])
  // dialog
  const onClose = (event: any) => toList()
  const isShowModalOpen = useMemo(() =>
    (action === ActionTypeEnum.SHOW), [action])
  const isFormModalOpen = useMemo(() =>
    (action === ActionTypeEnum.NEW || action === ActionTypeEnum.EDIT), [action])
  const isRemoveDialogOpen = useMemo(() =>
    (action === ActionTypeEnum.REMOVE), [action])
  // lifecycle
  useEffect(() => {
    return function () { clear() }
  }, [clear])
  // render
  return (<React.Fragment>
    {/* Collection */}
    <Collection {...collection} />
    {/* Form */}
    <Modal
      open={isFormModalOpen}
      onClose={onClose}
      aria-labelledby="show-modal-title"
      aria-describedby="show-modal-description">
      <div className={classes.paper}>
        <Form />
      </div>
    </Modal>
    {/* Show */}
    <Modal
      open={isShowModalOpen}
      onClose={onClose}
      aria-labelledby="show-modal-title"
      aria-describedby="show-modal-description">
      <div className={classes.paper}>
        <Show />
      </div>
    </Modal>
    <Dialog
      open={isRemoveDialogOpen}
      aria-labelledby="Remover grandeza"
      aria-describedby="Remover grandeza">
      <RemoveDialog />
    </Dialog>
  </React.Fragment>)
}



import React, { useEffect, useMemo, useCallback, useContext } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  Modal,
  Dialog,
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core'
import {
  CollectionTypeEnum,
  ActionTypeEnum
} from '../../../../types/Crud'
import { Show } from '../../../../components/Modulo/Admin/Grandeza/Show/Show'
import { Form } from '../../../../components/Modulo/Admin/Grandeza/Form/Form'
import { CrudContext } from '../../../../contexts/CrudContext'
import { Collection } from '../../../../components/Modulo/Admin/Grandeza/Collection/Collection'
import { RemoveDialog } from '../../../../components/Modulo/Admin/Grandeza/RemoveDialog/RemoveDialog'
import * as _ from 'lodash'
import './Grandeza.scss'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      minWidth: 200,
      minHeight: 200,
      // boxShadow: theme.shadows[5],
      // padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    }
  })
)

/**
 * Tela de gerenciameto de Grandeza
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Grandeza
 */
export const Grandeza = (props: any) => {
  const classes = useStyles()
  // CrudContext
  const { getState, types } = useContext(CrudContext)
  // values
  const collection = useMemo(() => {
    const result = {
      type: CollectionTypeEnum.LIST,
      limit: 10
    }
    if (props.collection) {
      return _.defaults(props.collection, result)
    }
    return result
  }, [props.collection])
  const action = useSelector(
    (state: any) => getState(state).action)
  const dispatch = useDispatch()
  const toList = useCallback(() =>
    dispatch({ type: types.SET_ACTION_ITEM, payload: { action: ActionTypeEnum.LIST } }),
  [dispatch, types])
  const clear = useCallback(() =>
    dispatch({ type: types.CLEAR_ITEM }),
  [dispatch, types])
  // dialog
  const onClose = (event: any) => toList()
  const isShowModalOpen = useMemo(() =>
    (action === ActionTypeEnum.SHOW), [action])
  const isFormModalOpen = useMemo(() =>
    (action === ActionTypeEnum.NEW || action === ActionTypeEnum.EDIT), [action])
  const isRemoveDialogOpen = useMemo(() =>
    (action === ActionTypeEnum.REMOVE), [action])
  // lifecycle
  useEffect(() => {
    return function () { clear() }
  }, [clear])
  // render
  return (<React.Fragment>
    {/* Collection */}
    <Collection {...collection} />
    {/* Form */}
    <Modal
      open={isFormModalOpen}
      onClose={onClose}
      aria-labelledby="show-modal-title"
      aria-describedby="show-modal-description">
      <div className={classes.paper}>
        <Form />
      </div>
    </Modal>
    {/* Show */}
    <Modal
      open={isShowModalOpen}
      onClose={onClose}
      aria-labelledby="show-modal-title"
      aria-describedby="show-modal-description">
      <div className={classes.paper}>
        <Show />
      </div>
    </Modal>
    <Dialog
      open={isRemoveDialogOpen}
      aria-labelledby="Remover grandeza"
      aria-describedby="Remover grandeza">
      <RemoveDialog />
    </Dialog>
  </React.Fragment>)
}
