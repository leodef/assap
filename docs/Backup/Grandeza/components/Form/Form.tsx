import React, { useCallback } from 'react';
import {
  Grid,
  Button
} from '@material-ui/core';
import { Formik, FormikProps, FormikHelpers, Form as FormikForm } from 'formik';
import { Fieldset } from '../Fieldset/Fieldset';
import './Form.scss';
import { GrandezaSchema, initialValues, Grandeza } from '../../../../../types/Modulo/Admin/Grandeza';
import { useDispatch, useSelector } from 'react-redux';

export class FormProps {
  item: Grandeza = new Grandeza();
}
/**
 * Formulário para Grandeza
 * @param {FormProps} props Propriedades
 * @return {React.Component} Componente com formulário para Grandeza
 */
export const Form = ( (props: any) => {
    const dispatch = useDispatch();

    const onFormSubmit = (values: any, formikHelpers: FormikHelpers<any>) => {
      const { setSubmitting } = formikHelpers;
      setSubmitting(false);
      onSubmit(values);
    }
    return (
        <Formik
          initialValues={item || initialValues}
          validationSchema={GrandezaSchema}
          onSubmit={onFormSubmit}
          render={(formProps: FormikProps<any>) =>
            <FormBody
              form={formProps}/>}
        />);
})

/**
 * Corpo do formulário para Grandeza
 * @param {form: FormikProps<any>, config: FormConfig} props Propriedades
 * @return {React.Component} Componente com formulário para Grandeza
 */

export const FormBody = ( (props: {
  form: FormikProps<any>,
  config: FormConfig
}) => {
  // onSubmit={handleSubmit}
  const { form, config } = props;
  
  return (
    <FormikForm>
      <Fieldset form={form} />
      <FormBodyActions config={config} />
    </FormikForm>
  );
});

/**
 * Botões do formulário para Grandeza
 * @param {config: FormConfig} props Propriedades
 * @return {React.Component} Botões do formulário para Grandeza
 */
export const FormBodyActions = ( (props: {
  config: FormConfig
}) => {
  const { config } = props;
  return (
    <Grid container>
    { !!save ?
      (<Grid item>
        <Button
          type="submit"
          variant="contained"
          color="primary">
          Salvar
        </Button>
      </Grid>) : null
    }
    {  !!toRemove ?
      (<Grid item>
        <Button
          variant="contained"
          color="primary"
          onClick={(event: any) => toRemove(item) }>
          Remover
        </Button>
      </Grid>) : null
    }
    { !!showBtnBack ?
      (<Grid item>
        <Button
          variant="contained"
          color="primary"
          onClick={(event: any) => back()}>
          Voltar
        </Button>
      </Grid>) : null
    }
    </Grid>
  );
})