import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Card,
  CardActions,
  CardContent,
  Typography,
  IconButton
} from '@material-ui/core';
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  ArrowBack as ArrowBackIcon
} from '@material-ui/icons'
import {
  Grandeza
} from '../../../../../types/Modulo/Admin/Grandeza';
import './Show.scss';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export class ShowProps {
  item: Grandeza = new Grandeza();
  toEdit?: ((item: any) => void);
  toRemove?: ((item: any) => void);
  back?: (() => void);
}
export class ShowConfig extends ShowProps {

  get showBtnEdit() {
    return !!this.toEdit;
  }
  get showBtnRemove() {
    return !!this.toRemove;
  }
  get showBtnBack() {
    return !!this.back;
  }
  get showActions() {
    return (
      this.showBtnEdit ||
      this.showBtnRemove ||
      this.showBtnBack
    );
  }
  constructor(obj?: any) {
    super();
    if(!!obj) {
      Object.assign(this, obj);
    }
  }
}
/**
 * Visualização de item do tipo Grandeza
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Grandeza
 */
export const Show = ( (props: ShowProps) => {
  const classes = useStyles();
  const { item } = props;
  const config = new ShowConfig(props);

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          Grandeza
        </Typography>
        <Typography variant="h5" component="h2">
          {item.titulo}
        </Typography>
        <Typography variant="body2" component="p">
          {item.desc}
        </Typography>
      </CardContent>
      <CardActions>
        <ShowActions config={config} />
      </CardActions>
    </Card>
  );
})
/**
 * Ações na visualização de item do tipo Grandeza
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com ações na visualização de item do tipo Grandeza
 */

export const ShowActions = ( (props: {config: ShowConfig}) => {
  const { config } = props;
  const {
    showBtnEdit,
    showBtnBack,
    showBtnRemove,
    toEdit,
    back,
    toRemove,
    item
  } = config;
  return (
    <React.Fragment>       
    {
      showBtnEdit ?
        (
        <IconButton
          aria-label="Editar"
          onClick={() => !!toEdit ? toEdit(item) : null}>
          <EditIcon />
        </IconButton>
        ) :
        null
    }
    {
      showBtnRemove ?
        (
        <IconButton
          aria-label="Remover"
          onClick={() => !!toRemove ? toRemove(item) : null}>
          <DeleteIcon />
        </IconButton>
        ) :
        null
    } {
      showBtnBack ?
        (
        <IconButton
          aria-label="Voltar"
          onClick={() => !!back ? back() : null}>
          <ArrowBackIcon />
        </IconButton>
        ) :
        null
    }
    </React.Fragment>
  );
})