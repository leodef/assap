import React, {useMemo} from 'react';
import AssignmentIcon from '@material-ui/icons/Assignment';
import {
  List as AppList,
  ListProps as SharedListProps,
  ListPropsItem
} from '../../../../Shared/List/List'; 
import { Grandeza } from '../../../../../types/Modulo/Admin/Grandeza';
import './List.scss';

export interface ListProps {
  items: Array<any>;
  dense?: boolean | undefined;
  subheader?: string | undefined;
}
export class AppListProps implements SharedListProps {
  items: Array<ListPropsItem> = new Array<ListPropsItem>();
  dense?: boolean | undefined;
  subheader?: string | undefined;

  constructor(props: ListProps) {
    this.dense = true;
    this.subheader = props.subheader || 'Grandeza';
    this.toShow = props.toShow;
    this.toEdit = props.toEdit;
    this.toRemove = props.toRemove;
    this.items = props.items
      .filter(item => !!item)
      .map( item => new AppListPropsItem(item as Grandeza));
  }
}
export class AppListPropsItem extends ListPropsItem {
  constructor(grandeza: Grandeza) {
    super(
      (grandeza.titulo ?
        grandeza.titulo.toString() :
        ''),
      null,
      (!!grandeza.titulo ?
        grandeza.titulo.substr(2) :
        (<AssignmentIcon />))
    );
  }
}

/**
 * Lista de items do tipo Grandeza
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com lista de items do tipo Grandeza
 */
export const List = ( (props: ListProps) => {
  const appListProps = useMemo(() => new AppListProps(props), [props]);
    return (
      <AppList {...appListProps} />
    );
});
