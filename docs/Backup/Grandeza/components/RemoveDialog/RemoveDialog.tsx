import React, { useMemo } from 'react';
import {
  Button,
  DialogActions,
  DialogTitle,
} from '@material-ui/core';
import './Table.scss';

/**
 * Dialogo para remoção do item
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com dialogo para remoção do item
 */
export const RemoveDialog = ( (props: any) => {

    return (<React.Fragment>
      <DialogTitle id="remove-dialog-title">{"Deseja realmente remover?"}</DialogTitle>
      <DialogActions>
        <Button onClick={() => back()} color="primary">
          Não
        </Button>
        <Button onClick={() => remove(item)} color="primary" autoFocus>
          Sim
        </Button>
      </DialogActions>
    </React.Fragment>);
});