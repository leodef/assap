import React from 'react';
import { Field } from 'formik';
import {
  Grid,
  TextField
} from '@material-ui/core';
import
  { getFieldName, UpperCasingTextField }
  from '../../../../Shared/FormikMaterialUIField/FormikMaterialUIField';
import './Fieldset.scss';

/**
 * Area de inputs para Grandeza
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Grandeza
 */
export const Fieldset = React.memo( (props: any) => {
    const fields = {
      titulo: {
        name: getFieldName('titulo', props)
      },
      desc: {
        name: getFieldName('desc', props)
      }
    }

    return (
      <Grid container spacing={2} component={Fieldset}>
        <Grid item xs={12} sm={6}>
          <Field
            component={UpperCasingTextField}
            name={fields.titulo.name}
            type="text"
            label="Titulo"
          />
        </Grid>
        <Grid item xs={12}>
          <Field
            component={TextField}
            name={fields.desc.name}
            type="text"
            label="Descrição"
            multiline
          />
        </Grid>
      </Grid>
    );
});
