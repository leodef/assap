import React from 'react';
import ReactDOM from 'react-dom';
import { Fieldset } from './Fieldset';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Fieldset />, div);
  ReactDOM.unmountComponentAtNode(div);
});
