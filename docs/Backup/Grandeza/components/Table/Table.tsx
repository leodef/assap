import React, { useMemo } from 'react';
import { Table as AppTable, TableProps as SharedTableProps, TablePropsField} from '../../../../Shared/Table/Table'
import './Table.scss';

export class AppTableProps implements SharedTableProps {
  
  fields: Array<String | TablePropsField> = [];
  label?: string | undefined;
  align: 'inherit' | 'left' | 'center' | 'right' | 'justify';
  items: any[];
  
  constructor(props: any) {
    this.items = props.items;
    this.label  = 'Grandeza';
    this.align  = 'right';
    this.fields = [
      new TablePropsField('titulo', 'Título'),
      new TablePropsField('desc', 'Descrição')
    ];
  }
}
/**
 * Tabela de item do tipo Grandeza
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com tabela de item do tipo Grandeza
 */
export const Table = ( (props: any) => {
  const appTableProps = useMemo(() => new AppTableProps(props), [props]);
    return (
      <AppTable {...appTableProps} />
    );
});