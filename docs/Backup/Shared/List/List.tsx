import React , { useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  List as ListMui,
  ListItem as ListItemMui,
  ListItemAvatar as ListItemAvatarMui,
  ListSubheader as ListSubheaderMui, 
  ListItemText,
  Avatar,
  IconButton,
  ListItemSecondaryAction,
  Menu,
  MenuItem,
  ListItemIcon,
  MenuProps
} from '@material-ui/core';
import {
  MoreVert as MoreVertIcon,
  Visibility as VisibilityIcon,
  Edit as EditIcon,
  Delete as DeleteIcon
} from '@material-ui/icons';
import './List.scss';

export interface ListProps {
  items: Array<ListPropsItem>;
  dense?: boolean;
  subheader?: string;
}

export interface ListItemProps extends ListProps {
  item: ListPropsItem;
  key: number;
}

export class ListPropsItem {
  
  constructor(
    public primary: string,
    public secondary: string | null  = null,
    public avatar: any = null) {}
}

/**
 * Lista genérica para coleção de items 
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com lista genérica para coleção de items 
 */
export const List = React.memo( (props: ListProps) => {
  const { items, dense, subheader} = props;
    return (
      <ListMui
        aria-labelledby="nested-list-subheader"
        subheader={<ListSubheader subheader={subheader} />}
        dense={dense}>
          {items.map( (item: any, index: number) =>
            <ListItem
              item={item}
              key={index}
              {...props} />
          )}
      </ListMui>
    );
});

/**
 * Cabeçalho para lista genérica para coleção de items
 *   List.ListSubheader
 * @param {config: ListConfig} props Propriedades
 * @return {React.Component} Componente com cabeçalho para lista genérica para coleção de items 
 */
export const ListSubheader = React.memo( (props: any) => {
  const { subheader } = props;
  return !!subheader ? (
    <ListSubheaderMui component="div" id="nested-list-subheader">
      {subheader}
    </ListSubheaderMui>) : null
});

/**
 * Item da lista genérica para coleção de items
 *   List.ListItem
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com item da lista genérica para coleção de items 
 */
export const ListItem = React.memo( (props: ListItemProps) => {
  const { item, key } = props;
  const { primary, secondary } = item;
  return (
    <ListItemMui key={key}>
      <ListItemAvatar {...props} />
      <ListItemText
        primary={primary}
        secondary={
          !!secondary ?
            secondary :
            null
        } />
      <ListItemActions
        {...props} />
    </ListItemMui> 
  );
});

/**
 * Avatar do item da lista genérica para coleção de items
 *   List.ListItem.ListItemAvatar
 * @param {item: ListPropsItem, index: number} props Propriedades
 * @return {React.Component} Componente com avatar do item da lista genérica para coleção de items 
 */
export const ListItemAvatar = React.memo( (props: ListItemProps) => {
  const { item } = props;
  const { avatar } = item;
  return !!avatar ? (
    <ListItemAvatarMui>
      <Avatar>
        {avatar}
      </Avatar>
    </ListItemAvatarMui>
      ) : null;
});


/**
 * Menu customizado
 * @param {MenuProps} props Propriedades
 * @return {React.Component} Componente com menu customizado
 */
const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

/**
 * Fabrica de item de menu customizado
 * @param {any} theme Propriedades
 * @return {method} Componente com fabrica de item de menu customizado
 */
const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

/**
 * Botões do item da lista genérica para coleção de items
 *   List.ListItem.ListItemActions
 * @param {item: ListPropsItem, index: number, config: ListConfig} props Propriedades
 * @return {React.Component} Componente com botões do item da lista genérica para coleção de items 
 */
export const ListItemActions = React.memo( (props: ListItemProps) => {
  const { item, toShow, toEdit, toRemove } = props;
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  if(!toShow && !toEdit && !toRemove) {
    return null;
  }
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <ListItemSecondaryAction>
      <div>
        <IconButton
            aria-label="more"
            aria-controls="long-menu"
            aria-haspopup="true"
            onClick={handleClick}
          >
            <MoreVertIcon />
        </IconButton>
        <StyledMenu
          id="customized-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose} >
          {
            !!toShow ?
              (<StyledMenuItem onClick={() => toShow(item)}>
                <ListItemIcon>
                  <VisibilityIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary="Visualizar" />
              </StyledMenuItem>) :
              null
          }
          {
            !!toEdit ?
              (<StyledMenuItem onClick={() => toEdit(item)}>
                <ListItemIcon>
                  <EditIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary="Editar" />
              </StyledMenuItem>) :
              null
          }
          {
            !!toRemove ?
              (<StyledMenuItem onClick={() => toRemove(item)}>
                  <ListItemIcon>
                    <DeleteIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText primary="Remover" />
                </StyledMenuItem>) :
              null
          }
        </StyledMenu>
      </div>
    </ListItemSecondaryAction>
  );
});
