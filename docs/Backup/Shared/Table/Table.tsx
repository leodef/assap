import React from 'react';
import {
  Table as TableMui,
  TableBody as TableBodyMui,
  TableCell,
  TableContainer,
  TableHead as TableHeadMui,
  TableRow,
  Paper,
  IconButton
} from '@material-ui/core';
import {
  Edit as EditIcon,
  Visibility as VisibilityIcon,
  Delete as DeleteIcon
} from '@material-ui/icons';
import './Table.scss';

export interface TableProps {
  items: Array<any>;
  fields: Array<TablePropsField | String>;
  actionsLabel?: string;
  label?: string;
  align: "inherit" | "left" | "center" | "right" | "justify";
  fieldsObj: Array<TablePropsField>;
}
export class TablePropsField {
  constructor(
    public name: string,
    public label?: string,
    public type: string = 'text'
  ) {
    this.label = this.label || this.name;
  }
}
export class TablePropsImpl implements TableProps {
  items: any[] = [];
  fields: Array<TablePropsField | String> = [];
  actionsLabel = "Opções";
  label?: string;
  align: "inherit" | "left" | "center" | "right" | "justify" = "right" ;

  get fieldsObj(): Array<TablePropsField> {
    return this.fields
      .filter( (val: TablePropsField | String ) => !!val )
      .map( (val: TablePropsField | String ) => {
        if(val instanceof TablePropsField){
          return val; 
        } else {
          return new TablePropsField(val.toString());
        }
      })
  }
  set fieldsObj(fieldsObj: Array<TablePropsField>) {}

  constructor(obj?: any) {
    if(!!obj) {
      Object.assign(this, obj);
    }
  }
}
export interface RowProps extends TableProps {
  row: any;
  key: number;
}

/**
 * Tabela genérica para exibir coleção de elementos
 *   Table
 * @param {TableProps} props Propriedades
 * @return {React.Component} Componentes com tabela genérica para exibir coleção de elementos
 */
export const Table = React.memo((props: TableProps) =>  {
  props = new TablePropsImpl(props);
  const { label } = props;
  return (
    <TableContainer component={Paper}>
      <TableMui aria-label={label}>
        <TableHead {...props} />
        <TableBody {...props} />
      </TableMui>
    </TableContainer>
  )
});

/**
 * Cabeçalho para tabela genérica para exibir coleção de elementos
 *   Table.Tablehead
 * @param {config: TableConfig} props Propriedades
 * @return {React.Component} Componente com cabeçalho para tabela genérica para exibir coleção de elementos
 */
const TableHead = React.memo((props: TableProps) =>  {
  const { align, fieldsObj, actionsLabel, toRemove, toEdit, toShow } = props;
  return (
    <TableHeadMui>
      <TableRow>
        {
          fieldsObj.map( (field, fieldIndex) => (
            <TableCell component="th" scope="row" align={align} key={fieldIndex}>
              {field.label}
            </TableCell>
          ))
        }
        {
          (!!toRemove || !!toEdit || !!toShow) ? 
            (
              <TableCell className="actions-cell">
                {actionsLabel}
              </TableCell>
            ) : null
        }
      </TableRow>
    </TableHeadMui>
  );
});

/**
 * Conteudp da tabela genérica para exibir coleção de elementos
 *   Table.TableBody
 * @param {config: TableConfig} props Propriedades
 * @return {React.Component} Componente com conteudo da tabela genérica para exibir coleção de elementos
 */
const TableBody = React.memo((props: TableProps) =>  {
  const { fieldsObj } = props;
  return (
    <TableBodyMui>
      {fieldsObj.map( (row: any, index: number) => (
        <Row
          row={row}
          key={index}
          {...props}
        />
      ))}
    </TableBodyMui>
  )
});

/**
 * Linha da tabela genérica para exibir coleção de elementos
 *   Table.TableBody.Row
 * @param {row: any, config: TableConfig, key: any } props Propriedades
 * @return {React.Component} Componente com linha da tabela genérica para exibir coleção de elementos
 */
const Row = React.memo((props: RowProps) =>  {
  const {
    row,
    key,
    align,
    fieldsObj,
    toRemove,
    toEdit,
    toShow
  } = props;
  return (
    <TableRow key={key}>
      {
        fieldsObj.map( (field, fieldIndex) => (
          <TableCell align={align}>
            {row[field.name]}
          </TableCell>
        ))
      }
      {
        (!!toRemove || !!toEdit || !!toShow) ? 
          (
            <TableCell className="actions-cell">
              <ActionColumn  {...props}/>
            </TableCell>
          ) : null
      }
    </TableRow>
  );
});

/**
 * Botões para linha da tabela genérica para exibir coleção de elementos
 *   Table.TableBody.Row.ActionColumn
 * @param {row: any, config: TableConfig, key: any } props Propriedades
 * @return {React.Component} Componente com botões para linha da tabela genérica para exibir coleção de elementos
 */
const ActionColumn = React.memo((props: RowProps) =>  {
  const {
    row,
    toShow,
    toEdit,
    toRemove
  } = props;
  return (
    <React.Fragment>
      {
        !!toShow ?
          (
          <IconButton
            aria-label="Visualizar"
            onClick={() =>  toShow(row)}>
            <VisibilityIcon />
          </IconButton>
          ) :
          null
      }
      {
        !!toEdit ?
          (
          <IconButton
            aria-label="Editar"
            onClick={() => toEdit(row)}>
            <EditIcon />
          </IconButton>
          ) :
          null
      }
      {
        !!toRemove ?
          (
          <IconButton
            aria-label="Remover"
            onClick={() => toRemove(row)}>
            <DeleteIcon />
          </IconButton>
          ) :
          null
      }
    </React.Fragment>
  )
});
